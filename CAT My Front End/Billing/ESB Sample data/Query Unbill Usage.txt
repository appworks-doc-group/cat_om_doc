Query Unbill Usage
--- unbilled usage 

select 
    nvl(sum(amount),0)/100 
 from 
    CDR_UNBILLED a
    , cdr_data b 
where 
    a.msg_id = b.msg_id 
    and a.msg_id2 = b.msg_id2
    and a.msg_id_serv = b.msg_id_serv 
    and a.split_row_num = b.split_row_num
    and a.cdr_data_partition_key = b.cdr_data_partition_key 
    and a.account_no = xxxxx
    and b.no_bill=0
	
xxxxx = account_no 	