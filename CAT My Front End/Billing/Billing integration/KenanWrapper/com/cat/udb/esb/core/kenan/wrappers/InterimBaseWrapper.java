package com.cat.udb.esb.core.kenan.wrappers;

import org.apache.log4j.Logger;

import cat.ibacss.bean.InterimBillBean;

import com.cat.udb.esb.core.utils.DebugUtil;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
//import com.csgsystems.aruba.connection.BSDMSessionContext;
//import com.csgsystems.aruba.connection.Connection;
import commonj.sdo.DataObject;

public abstract class InterimBaseWrapper {
	
	private static Logger logger = Logger.getLogger(InterimBaseWrapper.class);
	
	protected InterimBillBean request = null;
	protected InterimBillBean response = null;
	protected DataObject requestObj;
	protected String PATH_LOG;
	//private Class CLASS = this.getClass();
	
//	protected InterimBaseWrapper(String path_log) {
//		PATH_LOG = path_log;
//	}
	
	public DataObject execute(DataObject requestObj ) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "###### Start Executing Service ######", CLASS);
		logger.debug("###### Start Executing Service ######");
		
		DataObject responseObj = null;
		//Throwable error = null;
		
		try {
			this.requestObj = requestObj;
			
			//init connection (we may use KenanConnectionManager from KenanUtil)
//				System.out.println("#### Connect to Kenan ####");
			
//				BSDMSettings settings = BSDMSettings.getDefault();
//				context = BSDMSessionContext.getDefaultContext();

			//Init Connection
//				connection = ConnectionManager.getConnection();
			
			//Begin Transaction
//				connection.beginTransaction(150, 0);
	
			executeService();
			
//				System.out.println("#### Response from Kenan API ####");
//				System.out.println(response.toString());
			//LoggerUtil.debug(PATH_LOG, "#### Response Bean ####", CLASS);
			logger.debug("#### Response Bean ####");
			//LoggerUtil.debug(PATH_LOG, response.toString(), CLASS);
			logger.debug(response.toString());
					
			//End Transaction
//				connection.endTransaction(0);
			
			responseObj = prepareResponseObj(requestObj, response, null);
		}
		catch (Throwable e) {
			//e.printStackTrace();
			logger.error(DebugUtil.getStackTrace(e));
			responseObj = prepareResponseObj(requestObj, response, e);
			//LoggerUtil.debug(PATH_LOG, "###### Catch - " + e + " ######", CLASS);
		}
		finally {
			//LoggerUtil.debug(PATH_LOG, "###### Finally - Done ######", CLASS);
			logger.debug("###### Finally - Done ######");
		}
		
		return responseObj; 
	}

	protected abstract DataObject prepareResponseObj(DataObject reqObj, InterimBillBean response, Throwable error) throws Exception;
	protected abstract void executeService() throws Throwable;
}
