package com.cat.udb.esb.wrappers;

import java.util.HashMap;

import com.cat.udb.esb.core.kenan.utils.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;
import com.cat.ibacss.esb.bo.accountinfo.kenan.wrapper.AddAccountAndUserAccount;
import com.cat.ibacss.esb.bo.accountinfo.kenan.wrapper.impl.AddAccountAndUserAccountImpl;
import com.cat.ibacss.esb.bo.convert.AccountConverter;
import com.cat.ibacss.esb.bo.convert.impl.AccountConverterUDBImpl;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.BaseWrapper;

import com.csgsystems.bali.connection.ApiMappings;
import commonj.sdo.DataObject;
import java.util.Date;

import org.apache.log4j.Logger;

public class CreateAccountWrapper extends BaseWrapper {
	
	private static Logger logger = Logger.getLogger(CreateAccountWrapper.class);
	
	//private static final String PATH_LOG = Constant.LOG4j_PROPERITES_PATH_UDB_A1;
	//private Class CLASS = this.getClass();
	
//	public CreateAccountWrapper() {
//		super(Constant.LOG4j_PROPERITES_PATH_UDB_A1);
//	}
	
	@Override
	protected void executeService() throws Throwable {
		//LoggerUtil.debug(PATH_LOG, "###### CreateAccountWrapper - executeService START ######", CLASS);
		logger.debug("###### CreateAccountWrapper - executeService START ######");
		HashMap mapCreateAccount = null;
		request = new HashMap();
		mapCreateAccount = mapToRequestServiceHashMap(requestObj);
		request.put("Account", mapCreateAccount);
		HashMap requestExt = (HashMap)request.get("Account");
		requestExt.put("ExtendedData", MessageUtil.getExtendedData(requestExt, Constant.ACCOUNT_EXTDATA));
		//LoggerUtil.debugHashMap(PATH_LOG, "== request ==", CLASS, request);
		if (logger.isDebugEnabled())
			logger.debug("== request == " + DebugUtil.getStringBuffer(request).toString());
		
		HashMap accountCreateResponse = new HashMap();
		
		if (request != null){
			//Call Kenan API : "AccountCreate"
			//LoggerUtil.debug(PATH_LOG, "###### Call Kenan API : AccountCreate ######", CLASS);
			logger.debug("###### Call Kenan API : AccountCreate ######");
			HashMap tempAPIresponse = new HashMap();
			tempAPIresponse = connection.call(context, ApiMappings.getCallName("AccountCreate"), request);
			//LoggerUtil.debugHashMap(PATH_LOG, "tempAPIresponse", CLASS, tempAPIresponse);
			if (logger.isDebugEnabled())
				logger.debug("tempAPIresponse " + DebugUtil.getStringBuffer(tempAPIresponse).toString());
			accountCreateResponse.put("Account", (HashMap) tempAPIresponse.get("Account"));
			//LoggerUtil.debugHashMap(PATH_LOG, "accountCreateResponse", CLASS, accountCreateResponse);
			if (logger.isDebugEnabled())
				logger.debug("accountCreateResponse " + DebugUtil.getStringBuffer(accountCreateResponse).toString());
			
			//Call Kenan API : "AccountIdCreate"
			HashMap accountIdCreateRequest = new HashMap();
			accountIdCreateRequest = createAccountIDRequest(accountCreateResponse);
			//LoggerUtil.debug(PATH_LOG, "###### Call Kenan API : AccountIdCreate ######", CLASS);
			logger.debug("###### Call Kenan API : AccountIdCreate ######");
			HashMap accountIdCreateResponse = connection.call(context, ApiMappings.getCallName("AccountIdCreate"), accountIdCreateRequest);
			((HashMap)accountCreateResponse.get("Account")).put("AccountId", accountIdCreateResponse.get("AccountId"));
			//LoggerUtil.debugHashMap(PATH_LOG, "accountCreateResponse", CLASS, accountCreateResponse);
			if (logger.isDebugEnabled())
				logger.debug("accountCreateResponse " + DebugUtil.getStringBuffer(accountCreateResponse).toString());
			
			//Payment Profile, Call Kenan API : "PaymentProfileCreate" and Call Kenan API : "AccountUpdate"
			accountCreateResponse = getPaymentProfile (accountCreateResponse);
			
			//Customer Service Center Create
			((HashMap)request.get("Account")).put("Key", ((HashMap)accountCreateResponse.get("Account")).get("Key"));
			HashMap [] customerServiceCenterUpdateResponseList = updateAccountDetail((HashMap)request.get("Account"));
			((HashMap)accountCreateResponse.get("Account")).put("CustomerServiceCenters", customerServiceCenterUpdateResponseList);

			((HashMap)accountCreateResponse.get("Account")).put("ServerId", 3);
		}
		
		if (((HashMap)accountCreateResponse.get("Account")).containsKey("ExtendedData")){
			MessageUtil.setExtendedData(((HashMap)accountCreateResponse.get("Account")));
			((HashMap)accountCreateResponse.get("Account")).remove("ExtendedData");
		}
		
		//LoggerUtil.debug(PATH_LOG, "###### CreateAccountWrapper - executeService END ######", CLASS);
		logger.debug("###### CreateAccountWrapper - executeService END ######");
		response = accountCreateResponse;
		return;
	}
	
	public HashMap mapToRequestServiceHashMap(DataObject requestObj) throws Exception {
		HashMap mapCreateAccount_Rsp = null;
		try {
			AccountConverter reConverter = new AccountConverterUDBImpl();
			mapCreateAccount_Rsp = reConverter.dataObject2HashMap(requestObj);
		}
		catch (Exception e) {
			//LoggerUtil.error(PATH_LOG, "Exception : Map mapCreateAccountObject --------> " + e, CLASS);
			logger.error(DebugUtil.getStackTrace(e));
			throw e;
		}
		return mapCreateAccount_Rsp;
	}
	
	@Override
	protected DataObject prepareResponseObj(DataObject reqObj, HashMap response, Throwable error) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "$$$$$$$$$ prepareResponseObj $$$$$$$$$", CLASS);
		MessageUtil msgHelper = new MessageUtil();
		HashMap result = msgHelper.getResult(error);
		String transactionLogId = msgHelper.getTransactionLogId(reqObj);
		DataObject responseObj = mapToResponseServiceDataObj(response, result, transactionLogId);
		
		//LoggerUtil.debug(PATH_LOG, "$$$$$$$$$ UserAccountMessage $$$$$$$$$", CLASS);
		logger.debug("$$$$$$$$$ UserAccountMessage $$$$$$$$$");
		DataObject userAccountMessageObj = null;
		//userAccountMessageObj.createDataObject("UserAccountMessage");
		//responseObj.setDataObject("UserAccountMessage", userAccountMessageObj);
		
		AddAccountAndUserAccount wrapper = new AddAccountAndUserAccountImpl();
		requestObj.setDataObject("UserAccountMessage", null);
		userAccountMessageObj = wrapper.addAccountEBPP(requestObj);
		if (userAccountMessageObj != null){
			//LoggerUtil.debug(PATH_LOG, "$$$$$$$$$ set UserAccountMessage $$$$$$$$$", CLASS);
			logger.debug("$$$$$$$$$ set UserAccountMessage $$$$$$$$$");
			responseObj.setDataObject("UserAccountMessage", userAccountMessageObj);
		}
		
		//responseObj.setDataObject("UserAccountMessage", null);
		
		return responseObj;
	}
	
	private DataObject mapToResponseServiceDataObj(HashMap response, 
			HashMap result, String transactionLogId) throws Exception {
		
		HashMap responseHashMap = new HashMap();
		DataObject responseObj;
		
		if (response != null){
			responseHashMap.put("Account", (HashMap)response.get("Account"));
		}
		else {
			responseHashMap.put("Account", request);
		}
		
		responseHashMap.put("Result", result);
		responseHashMap.put("TransactionLogId", transactionLogId);
		responseHashMap.put("UserAccountMessage", null);
		
		//LoggerUtil.debug(PATH_LOG, "### mapToResponseServiceDataObj: ", CLASS);
		logger.debug("### mapToResponseServiceDataObj: ");
		//LoggerUtil.debugHashMap(PATH_LOG, "responseHashMap", CLASS, responseHashMap);
		if (logger.isDebugEnabled())
			logger.debug("responseHashMap " + DebugUtil.getStringBuffer(responseHashMap).toString());
		
		AccountConverter createAccountConvert = new AccountConverterUDBImpl();
		DataObject requestObj = createAccountConvert.hashMap2DataObject(responseHashMap);
		
		return requestObj;
	}
	
	private HashMap createAccountIDRequest (HashMap accountCreateResponse) throws Exception {
		try {
			HashMap accountIdKeyTemp = new HashMap();
			accountIdKeyTemp.put("AccountInternalId", Integer.parseInt(((HashMap)((HashMap)accountCreateResponse.get("Account")).get("Key")).get("AccountInternalId").toString()));
			accountIdKeyTemp.put("AccountExternalId", ((HashMap)request.get("Account")).get("CardId").toString());
			accountIdKeyTemp.put("AccountExternalIdType", Integer.parseInt(((HashMap)request.get("Account")).get("CardType").toString()));
			accountIdKeyTemp.put("ActiveDate", (Date)((HashMap)request.get("Account")).get("DateActive"));
			HashMap accountIDKey = new HashMap();
			accountIDKey.put("Key", accountIdKeyTemp.clone());
			
			HashMap accountIdCreateRequest = new HashMap();
			accountIdCreateRequest.put("AccountId", accountIDKey.clone());
			//LoggerUtil.debugHashMap(PATH_LOG, "accountIdCreateRequest", CLASS, accountIdCreateRequest);
			if (logger.isDebugEnabled())
				logger.debug("accountIdCreateRequest " + DebugUtil.getStringBuffer(accountIdCreateRequest).toString());
			
			return accountIdCreateRequest;
		}
		catch (Exception e) {
			//LoggerUtil.error(PATH_LOG, "Exception CreateAccountWrapper createAccountIDRequest : "+e.getMessage(), CLASS);
			logger.error(DebugUtil.getStackTrace(e));
			throw e;
		}	
	}

	protected HashMap [] updateAccountDetail (HashMap reqAccount) throws Exception {	
		//LoggerUtil.debug(PATH_LOG, "----------- CreateAccountWrapper - updateAccountDetail ----------", CLASS);
		logger.debug("----------- CreateAccountWrapper - updateAccountDetail ----------");
		//LoggerUtil.debugHashMap(PATH_LOG, "reqAccount", CLASS, reqAccount);
		if (logger.isDebugEnabled())
			logger.debug("reqAccount " + DebugUtil.getStringBuffer(reqAccount).toString());
		HashMap [] customerServiceCenterUpdateListResponse = null;
		
		try {
			int csvSize = ((HashMap[])reqAccount.get("CustomerServiceCenters")).length;
			if (csvSize > 0){
				customerServiceCenterUpdateListResponse = new HashMap [csvSize];
				customerServiceCenterUpdateListResponse = updateCustomerService(reqAccount, customerServiceCenterUpdateListResponse, csvSize);
				//LoggerUtil.debug(PATH_LOG, "----------- CreateAccountWrapper - customerServiceCenterUpdateListRequest Completed----------", CLASS);
				logger.debug("----------- CreateAccountWrapper - customerServiceCenterUpdateListRequest Completed----------");
				return customerServiceCenterUpdateListResponse;
			}
			else {
				return null;
			}			
		}
		catch (Exception e) {
			//LoggerUtil.error(PATH_LOG, "Exception CreateAccountWrapper updateAccountDetail : "+e.getMessage(), CLASS);
			logger.error(DebugUtil.getStackTrace(e));
			throw e;
		}		
	}
	
	private HashMap [] updateCustomerService (HashMap reqAccount, HashMap [] customerServiceCenterUpdateListResponse, int csvSize) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "###### UpdateAccountWrapper - updateCustomerService ######", CLASS);
		logger.debug("###### UpdateAccountWrapper - updateCustomerService ######");
		
		try {
			HashMap [] customerServiceCenterUpdate = (HashMap[])reqAccount.get("CustomerServiceCenters");
			HashMap customerServiceCenterUpdateResponse = new HashMap();
			//LoggerUtil.debug(PATH_LOG, "----- Loop call API : CustomerServiceCenterUpdate, loop count : "+csvSize+" ------ ", CLASS);
			logger.debug("----- Loop call API : CustomerServiceCenterUpdate, loop count : " + csvSize + " ------ ");
			
			for (int i=0; i<csvSize; i++){
				//LoggerUtil.debug(PATH_LOG, "Loop : "+i, CLASS);
				logger.debug("Loop : " + i);
				//LoggerUtil.debug(PATH_LOG, "((HashMap)reqAccount.get(Key)).get(AccountInternalId): "+((HashMap)reqAccount.get("Key")).get("AccountInternalId"), CLASS);
				logger.debug("((HashMap)reqAccount.get(Key)).get(AccountInternalId): "+((HashMap)reqAccount.get("Key")).get("AccountInternalId"));
				//LoggerUtil.debugHashMap(PATH_LOG, "((HashMap)customerServiceCenterUpdate[i].get(Key))", CLASS, ((HashMap) ((HashMap)customerServiceCenterUpdate[i].get("CustomerServiceCenter")).get("Key")));
				if (logger.isDebugEnabled())
					logger.debug("((HashMap)customerServiceCenterUpdate[i].get(Key)) " + DebugUtil.getStringBuffer(((HashMap) ((HashMap)customerServiceCenterUpdate[i].get("CustomerServiceCenter")).get("Key"))).toString());
				
				((HashMap) ((HashMap)customerServiceCenterUpdate[i].get("CustomerServiceCenter")).get("Key")).put("AccountInternalId", ((HashMap)reqAccount.get("Key")).get("AccountInternalId"));
				//LoggerUtil.debugHashMap(PATH_LOG, "customerServiceCenterUpdate[i]", CLASS, customerServiceCenterUpdate[i]);
				if (logger.isDebugEnabled())
					logger.debug("customerServiceCenterUpdate[i] " + DebugUtil.getStringBuffer(customerServiceCenterUpdate[i]).toString());
				customerServiceCenterUpdateResponse = connection.call(context, ApiMappings.getCallName("CustomerServiceCenterUpdate"), customerServiceCenterUpdate[i]);
				//LoggerUtil.debugHashMap(PATH_LOG, " ### customerServiceCenterUpdateResponse ###", CLASS, customerServiceCenterUpdateResponse);
				if (logger.isDebugEnabled())
					logger.debug(" ### customerServiceCenterUpdateResponse ### " + DebugUtil.getStringBuffer(customerServiceCenterUpdateResponse).toString());
				
				customerServiceCenterUpdateListResponse[i] = new HashMap();
				customerServiceCenterUpdateListResponse[i].put("CustomerServiceCenter", customerServiceCenterUpdateResponse.get("CustomerServiceCenter"));
				//LoggerUtil.debugHashMap(PATH_LOG, "customerServiceCenterUpdateListResponse["+i+"]", CLASS, customerServiceCenterUpdateListResponse[i]);
				if (logger.isDebugEnabled())
					logger.debug("customerServiceCenterUpdateListResponse["+i+"] " + DebugUtil.getStringBuffer(customerServiceCenterUpdateListResponse[i]).toString());
				customerServiceCenterUpdateResponse = null;
			}
			return customerServiceCenterUpdateListResponse;
		}
		catch (Exception e) {
			//LoggerUtil.error(PATH_LOG, "Exception CreateAccountWrapper updateCustomerService : "+e.getMessage(), CLASS);
			logger.error(DebugUtil.getStackTrace(e));
			throw e;
		}	
	}
	
	private HashMap getPaymentProfile (HashMap accountCreateResponse) throws Exception {
		try {
			HashMap[] paymentProfileCreateListResponse = getPaymentProfileCreateList((HashMap[])((HashMap)request.get("Account")).get("PaymentProfiles"),((HashMap)accountCreateResponse.get("Account")));
			
			if(paymentProfileCreateListResponse != null){
				((HashMap)accountCreateResponse.get("Account")).put("PaymentProfiles", paymentProfileCreateListResponse);
				
				String defaultPaymentProfile = getDefaultPaymentProfile(paymentProfileCreateListResponse);
				if(defaultPaymentProfile != null){
					((HashMap)accountCreateResponse.get("Account")).put("PaymentProfileId",defaultPaymentProfile);
					HashMap accountUpdate = new HashMap();
					accountUpdate.put("Key", ((HashMap)accountCreateResponse.get("Account")).get("Key"));
					accountUpdate.put("PaymentProfileId", defaultPaymentProfile);
					
					//Call Kenan API : "AccountUpdate"
					//LoggerUtil.debug(PATH_LOG, "###### Call Kenan API : AccountUpdate ######", CLASS);
					logger.debug("###### Call Kenan API : AccountUpdate ######");
					HashMap accountUpdateResponse = connection.call(context, ApiMappings.getCallName("AccountUpdate"), accountUpdate);
					accountCreateResponse.put("Account", (HashMap) accountUpdateResponse.get("Account"));
					//LoggerUtil.debugHashMap(PATH_LOG, "accountCreateResponse", CLASS, accountCreateResponse);
					if (logger.isDebugEnabled())
						logger.debug("accountCreateResponse " + DebugUtil.getStringBuffer(accountCreateResponse).toString());
				}
			}
			
			return accountCreateResponse;
		}
		catch (Exception e) {
			//LoggerUtil.error(PATH_LOG, "Exception : getPaymentProfile -- " + e, CLASS);
			logger.error(DebugUtil.getStackTrace(e));
			throw e;
		}
	}

	private HashMap [] getPaymentProfileCreateList (HashMap[] paymentProfileList, HashMap accountRequest) throws Exception {
		
		try {
			
			boolean hasPaymentProfileCreateList = (paymentProfileList != null);
			if(hasPaymentProfileCreateList){
				int size = paymentProfileList.length;
				for(int i =0;i<size;i++){
					HashMap paymentProfileCreate = (HashMap)paymentProfileList[i];
					
					//Call Kenan API : "PaymentProfileCreate"
					//LoggerUtil.debug(PATH_LOG, "###### Call Kenan API : PaymentProfileCreate ######", CLASS);
					logger.debug("###### Call Kenan API : PaymentProfileCreate ######");
					paymentProfileCreate.put("AccountInternalId", ((HashMap)accountRequest.get("Key")).get("AccountInternalId")); 
					HashMap paymentProfileCreateResponse = connection.call(context, ApiMappings.getCallName("PaymentProfileCreate"), paymentProfileCreate);
					//LoggerUtil.debugHashMap(PATH_LOG, "paymentProfileCreateResponse", CLASS, paymentProfileCreateResponse);
					if (logger.isDebugEnabled())
						logger.debug("paymentProfileCreateResponse " + DebugUtil.getStringBuffer(paymentProfileCreateResponse).toString());
					
					paymentProfileList[i] = paymentProfileCreateResponse;
					paymentProfileCreate = null;
					paymentProfileCreateResponse = null;
				}
				return paymentProfileList;
			}
			else{
				return null;
			}	
		}
		catch (Exception e) {
			//LoggerUtil.error(PATH_LOG, "Exception CreateAccountWrapper getpaymentProfileCreateList : "+e.getMessage(), CLASS);
			logger.error(DebugUtil.getStackTrace(e));
			throw e;
		}	
	}
	
	private String getDefaultPaymentProfile(HashMap[] paymentProfileCreateListResponse) throws Exception{
		for(int i=0;i<paymentProfileCreateListResponse.length;i++){
			HashMap paymentProfileObject = (HashMap)paymentProfileCreateListResponse[i];
			if(Boolean.parseBoolean(paymentProfileObject.get("IsDefault").toString()) ){
				return (((HashMap)paymentProfileObject.get("Key")).get("ProfileId")).toString();
			}
		}
		return null;
	}
	
}
