package com.cat.udb.esb.wrappers.common;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cat.udb.esb.core.kenan.utils.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.udb.esb.core.utils.DebugUtil;

//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.csgsystems.aruba.connection.Connection;

public class UpdateServiceComponentImpl {

	private static Logger logger = Logger.getLogger(UpdateServiceComponentImpl.class);
	
//	public static HashMap OverrideService(HashMap serviceReq, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Throwable{
	public static HashMap OverrideService(HashMap serviceReq, Connection connection, BSDMSessionContext context) throws Throwable{
		//LoggerUtil.debug(PATH_LOG, "#*-*#*-*#*-*# BEGIN UpdateServiceComponentImpl : OverrideService #*-*#*-*#*-*#", UpdateServiceComponentImpl.class);
		logger.debug("#*-*#*-*#*-*# BEGIN UpdateServiceComponentImpl : OverrideService #*-*#*-*#*-*#");
		HashMap request = new HashMap();
		request.put("Account", (HashMap)serviceReq.get("Account"));
		
		//Add extended Data
		HashMap requestExt = (HashMap)request.get("Account");
		requestExt.put("ExtendedData", MessageUtil.getExtendedData(requestExt, Constant.ACCOUNT_EXTDATA));
		//LoggerUtil.debugHashMap(PATH_LOG, "== request data ==", UpdateServiceComponentImpl.class, request);
		if (logger.isDebugEnabled())
			logger.debug("== request data == " + DebugUtil.getStringBuffer(request).toString());
	
		// 0. Initial accountRequest
		HashMap accountRequest = new HashMap();
		accountRequest = (HashMap)request.get("Account");
		
		// 1. OrderCreate
		//LoggerUtil.debug(PATH_LOG, "== 1. OrderCreate ==", UpdateServiceComponentImpl.class);
		logger.debug("== 1. OrderCreate ==");
		HashMap order = new HashMap();
		order = (HashMap)accountRequest.get("Order");
		HashMap orderCreateResponse = new HashMap();
		//orderCreateResponse = OrderedServiceImpl.createOrder(order, connection, context, PATH_LOG);
		orderCreateResponse = OrderedServiceImpl.createOrder(order, connection, context);
		if (orderCreateResponse != null){
			String orderid = ((HashMap)((HashMap)orderCreateResponse.get("Order")).get("Key")).get("OrderId").toString();
			//LoggerUtil.debug(PATH_LOG, "== New Order id : =="+orderid , UpdateServiceComponentImpl.class);
			logger.debug("== New Order id : ==" + orderid);
			((HashMap)request.get("Account")).put("Order", orderCreateResponse);
		}
		orderCreateResponse = null;
		
		//	2. Group of Service-level Modification
		//LoggerUtil.debug(PATH_LOG, "== 2. Group of Service-level Modification ==", UpdateServiceComponentImpl.class);
		logger.debug("== 2. Group of Service-level Modification ==");
		HashMap orderedServiceList [] = null;
		if (accountRequest.containsKey("Services")) {
			orderedServiceList = (HashMap[])accountRequest.get("Services");
		}
		
		if (orderedServiceList != null && orderedServiceList.length > 0) {
			HashMap orderedService = new HashMap();			
			HashMap serviceRequestField = new HashMap();
			HashMap orderedComponentList [] = null;
			
			//LoggerUtil.debug(PATH_LOG, "== Loop Order Service List ==", UpdateServiceComponentImpl.class);
			logger.debug("== Loop Order Service List ==");
			for (int i=0; i<orderedServiceList.length; i++) {
				orderedService = (HashMap)orderedServiceList[i];
				serviceRequestField = (HashMap)orderedService.get("Service");
				orderedComponentList = (HashMap[])serviceRequestField.get("OrderedComponents");
				
				orderedService.put("Order", ((HashMap)((HashMap)request.get("Account")).get("Order")).get("Order"));
				orderedService.put("FindExistingSO", true);
				serviceRequestField.put("ExtendedData", MessageUtil.getExtendedData(serviceRequestField, Constant.SERVICE_EXTDATA));
				
				// 2.1 Service-level
				//HashMap serviceLevelProcessResponse = OrderedServiceImpl.serviceLevelProcess(orderedService, connection, context, PATH_LOG);
				HashMap serviceLevelProcessResponse = OrderedServiceImpl.serviceLevelProcess(orderedService, connection, context);
				if(serviceLevelProcessResponse != null){
					//LoggerUtil.debugHashMap(PATH_LOG, "$$$ serviceLevelProcessResponse $$$", UpdateServiceComponentImpl.class, serviceLevelProcessResponse);
					if (logger.isDebugEnabled())
						logger.debug("$$$ serviceLevelProcessResponse $$$");
					orderedService.put("Service", (HashMap)serviceLevelProcessResponse.get("Service"));
				}
				// 2.2  Component-level (TODO: NO HAVE UNIT TEST)
//				HashMap componentLevelProcessResponse [] = OrderedServiceImpl.componentLevelProcess((HashMap)((HashMap)request.get("Account")).get("Order"), orderedService, orderedComponentList, connection, context, PATH_LOG);
				HashMap componentLevelProcessResponse [] = OrderedServiceImpl.componentLevelProcess((HashMap)((HashMap)request.get("Account")).get("Order"), orderedService, orderedComponentList, connection, context);
				if(componentLevelProcessResponse != null){
					orderedService.put("OrderedComponents", componentLevelProcessResponse);
				}
				// 3.3  Set OrderedService to OrderedServiceList
				//LoggerUtil.debug(PATH_LOG, "== 3.3  Set OrderedService to OrderedServiceList ==", UpdateServiceComponentImpl.class);
				logger.debug("== 3.3  Set OrderedService to OrderedServiceList ==");
				orderedServiceList[i] = orderedService;
			}
			((HashMap)request.get("Account")).put("Services",orderedServiceList);
		}
		
		// 3. OrderCommit
		//LoggerUtil.debug(PATH_LOG, "== 3. OrderCommit ==", UpdateServiceComponentImpl.class);
		logger.debug("== 3. OrderCommit ==");
		order = new HashMap();
		order = (HashMap)accountRequest.get("Order");
		HashMap orderCommitResponse = new HashMap();
		//orderCommitResponse = OrderedServiceImpl.commitOrder(order, connection, context, PATH_LOG);
		orderCommitResponse = OrderedServiceImpl.commitOrder(order, connection, context);
		if (orderCommitResponse != null){
			((HashMap)request.get("Account")).put("Order", orderCommitResponse);
		}
		orderCommitResponse = null;
		
		//LoggerUtil.debug(PATH_LOG, "#*-*#*-*#*-*# END UpdateServiceComponentImpl : OverrideService #*-*#*-*#*-*#", UpdateServiceComponentImpl.class);
		logger.debug("#*-*#*-*#*-*# END UpdateServiceComponentImpl : OverrideService #*-*#*-*#*-*#");
		
		return request;
	}
	
	@SuppressWarnings("unchecked")
	//public static HashMap changeComponent(HashMap serviceReq, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Exception, Throwable{
	public static HashMap changeComponent(HashMap serviceReq, Connection connection, BSDMSessionContext context) throws Exception, Throwable{
		//LoggerUtil.debug(PATH_LOG, "#*-*#*-*#*-*# BEGIN UpdateServiceComponentImpl : changeComponent #*-*#*-*#*-*#", UpdateServiceComponentImpl.class);
		logger.debug("#*-*#*-*#*-*# BEGIN UpdateServiceComponentImpl : changeComponent #*-*#*-*#*-*#");
		HashMap request = new HashMap();
		request.put("Account", (HashMap)serviceReq.get("Account"));
		
		//Add extended Data
		HashMap requestExt = (HashMap)request.get("Account");
		requestExt.put("ExtendedData", MessageUtil.getExtendedData(requestExt, Constant.ACCOUNT_EXTDATA));
		//LoggerUtil.debugHashMap(PATH_LOG, "== request data ==", UpdateServiceComponentImpl.class, request);
		if (logger.isDebugEnabled())
			logger.debug("== request data == " + DebugUtil.getStringBuffer(request).toString());
	
		// 0. Initial accountRequest
		HashMap accountRequest = new HashMap();
		accountRequest = (HashMap)request.get("Account");
		
		// 1. OrderCreate
		//LoggerUtil.debug(PATH_LOG, "== 1. OrderCreate ==", UpdateServiceComponentImpl.class);
		logger.debug("== 1. OrderCreate ==");
		HashMap order = new HashMap();
		//order.put("Order", (HashMap)accountRequest.get("Order"));
		order = (HashMap)accountRequest.get("Order");
		HashMap orderCreateResponse = new HashMap();
		//orderCreateResponse = OrderedServiceImpl.createOrder(order, connection, context, PATH_LOG);
		orderCreateResponse = OrderedServiceImpl.createOrder(order, connection, context);
		if (orderCreateResponse != null){
			String orderid = ((HashMap)((HashMap)orderCreateResponse.get("Order")).get("Key")).get("OrderId").toString();
			//LoggerUtil.debug(PATH_LOG, "== New Order id : =="+orderid , UpdateServiceComponentImpl.class);
			logger.debug("== New Order id : ==" + orderid);
			((HashMap)request.get("Account")).put("Order", orderCreateResponse);
		}
		orderCreateResponse = null;
		
		// 2. Group of Account-level Modification
		//LoggerUtil.debug(PATH_LOG, "== 2. Group of Account-level Modification ==", UpdateServiceComponentImpl.class);
		logger.debug("== 2. Group of Account-level Modification ==");
		// 2.1 OrderedContract Modification at Account-level
		//LoggerUtil.debug(PATH_LOG, "== 2.1 OrderedContract Modification at Account-level ==", UpdateServiceComponentImpl.class);
		logger.debug("== 2.1 OrderedContract Modification at Account-level ==");
		HashMap[] orderedContractList = (HashMap[])((HashMap)request.get("Account")).get("OrderedContracts");
		if(orderedContractList != null){
//			Object[] orderedContracts= OrderedContractImpl.modifyOrderedContract(orderedContractList, (HashMap)((HashMap)request.get("Account")).get("Order"), connection, context, PATH_LOG);
			Object[] orderedContracts= OrderedContractImpl.modifyOrderedContract(orderedContractList, (HashMap)((HashMap)request.get("Account")).get("Order"), connection, context);
			((HashMap)request.get("Account")).put("OrderedContracts", orderedContracts);
			//LoggerUtil.debugHashMap(PATH_LOG, "== request : After OrderedContract Mod ==", UpdateServiceComponentImpl.class, request);
			if (logger.isDebugEnabled())
				logger.debug("== request : After OrderedContract Mod == " + DebugUtil.getStringBuffer(request).toString());
		}		
		
		// 2.2 OrderedNrc Modification at Account-level
		//LoggerUtil.debug(PATH_LOG, "== 2.2 OrderedNrc Modification at Account-level ==", UpdateServiceComponentImpl.class);
		logger.debug("== 2.2 OrderedNrc Modification at Account-level ==");
		HashMap[] orderNrcsList = (HashMap[])((HashMap)request.get("Account")).get("OrderedNrcs");
		if(orderNrcsList != null){
			//LoggerUtil.debug(PATH_LOG, "== orderNrcsList != null ==", UpdateServiceComponentImpl.class);
			logger.debug("== orderNrcsList != null ==");
//			Object[] orderedNrcs = OrderedNrcImpl.modifyOrderedNrc(orderNrcsList,null,(HashMap)((HashMap)request.get("Account")).get("Order"),connection, context ,PATH_LOG);
			Object[] orderedNrcs = OrderedNrcImpl.modifyOrderedNrc(orderNrcsList,null,(HashMap)((HashMap)request.get("Account")).get("Order"),connection, context);
			((HashMap)request.get("Account")).put("OrderedNrcs", orderedNrcs);
			//LoggerUtil.debugHashMap(PATH_LOG, "== request : After OrderedNrc Mod ==", UpdateServiceComponentImpl.class, request);
			if (logger.isDebugEnabled())
				logger.debug("== request : After OrderedNrc Mod == " + DebugUtil.getStringBuffer(request).toString());
		}		
		
		// 3. Group of Service-level Modification
		//LoggerUtil.debug(PATH_LOG, "== 3. Group of Service-level Modification ==", UpdateServiceComponentImpl.class);
		logger.debug("== 3. Group of Service-level Modification ==");
		HashMap orderedServiceList [] = null;
		HashMap orderedComponentList [] = null;
		if (accountRequest.containsKey("Services")) {
			orderedServiceList = (HashMap[])accountRequest.get("Services");
		}
		if (orderedServiceList != null && orderedServiceList.length > 0) {
			HashMap orderedService = null;
			
			//LoggerUtil.debug(PATH_LOG, "== Loop Order Service List ==", UpdateServiceComponentImpl.class);
			logger.debug("== Loop Order Service List ==");
			for (int i=0; i<orderedServiceList.length; i++) {
				orderedService = new HashMap();
				orderedService = (HashMap)orderedServiceList[i];
				HashMap serviceRequestField = (HashMap)orderedService.get("Service");
				Object currencyCode = serviceRequestField.get("CurrencyCode");
				serviceRequestField.put("ExtendedData", MessageUtil.getExtendedData(serviceRequestField, Constant.SERVICE_EXTDATA));
				//LoggerUtil.debugHashMap(PATH_LOG, " == serviceRequestField == ", UpdateServiceComponentImpl.class, serviceRequestField);
				if (logger.isDebugEnabled())
					logger.debug(" == serviceRequestField == " + DebugUtil.getStringBuffer(serviceRequestField));
				
				// 3.3 OrderedComponent Modification
				//LoggerUtil.debug(PATH_LOG, "== 3.3 OrderedComponent Modification ==", UpdateServiceComponentImpl.class);
				logger.debug("== 3.3 OrderedComponent Modification ==");
				orderedComponentList = null;
				orderedComponentList = (HashMap[])serviceRequestField.get("OrderedComponents");
				if (orderedComponentList != null && orderedComponentList.length > 0) {
//					HashMap orderedComponentResponseList [] = ComponentLevelImpl.modifyComponentLevel(orderedComponentList, orderedService, (HashMap)((HashMap)request.get("Account")).get("Order"), connection, context, PATH_LOG);
					HashMap orderedComponentResponseList [] = ComponentLevelImpl.modifyComponentLevel(orderedComponentList, orderedService, (HashMap)((HashMap)request.get("Account")).get("Order"), connection, context, currencyCode);
					if(orderedComponentResponseList != null){
						((HashMap)orderedService.get("Service")).put("OrderedComponents", orderedComponentResponseList);
					}
					orderedComponentResponseList = null;	
				}
								
				// 3.4 OrderedNrc Modification
				//LoggerUtil.debug(PATH_LOG, "== 3.4 OrderedNrc Modification ==", UpdateServiceComponentImpl.class);
				logger.debug("== 3.4 OrderedNrc Modification ==");
				
				orderNrcsList = (HashMap[])serviceRequestField.get("OrderedNrcs");
				if(orderNrcsList != null){
					//Object[] orderedNrcs = OrderedNrcImpl.modifyOrderedNrc(orderNrcsList, orderedService, (HashMap)((HashMap)request.get("Account")).get("Order"), connection, context ,PATH_LOG);
					Object[] orderedNrcs = OrderedNrcImpl.modifyOrderedNrc(orderNrcsList, orderedService, (HashMap)((HashMap)request.get("Account")).get("Order"), connection, context);
					serviceRequestField.put("OrderedNrcs",orderedNrcs);
				}
				
				// 3.5  Set OrderedServiceList to Account
				//LoggerUtil.debug(PATH_LOG, "== 3.5  Set OrderedServiceList to Account ==", UpdateServiceComponentImpl.class);
				logger.debug("== 3.5  Set OrderedServiceList to Account ==");
				orderedServiceList[i] = orderedService;
			}
			((HashMap)request.get("Account")).put("Services",orderedServiceList);
		}
		
		// 4. OrderCommit
		//LoggerUtil.debug(PATH_LOG, "== 4. OrderCommit ==", UpdateServiceComponentImpl.class);
		logger.debug("== 4. OrderCommit ==");
		order = new HashMap();
		//order = (HashMap)accountRequest.get("Order");
		order = (HashMap)((HashMap)request.get("Account")).get("Order");
		HashMap orderCommitResponse = new HashMap();
//		orderCommitResponse = OrderedServiceImpl.commitOrder(order, connection, context, PATH_LOG);
		orderCommitResponse = OrderedServiceImpl.commitOrder(order, connection, context);
		if (orderCommitResponse != null){
			((HashMap)request.get("Account")).put("Order", orderCommitResponse);
		}
		orderCommitResponse = null;
		
		//LoggerUtil.debug(PATH_LOG, "#*-*#*-*#*-*# END UpdateServiceComponentImpl : changeComponent #*-*#*-*#*-*#", UpdateServiceComponentImpl.class);
		logger.debug("#*-*#*-*#*-*# END UpdateServiceComponentImpl : changeComponent #*-*#*-*#*-*#");
		
		return request;
	}
	
	@SuppressWarnings("unchecked")
//	public static HashMap updateService(HashMap serviceReq, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Exception, Throwable {
	public static HashMap updateService(HashMap serviceReq, Connection connection, BSDMSessionContext context) throws Exception, Throwable {
		//LoggerUtil.debug(PATH_LOG, "#*-*#*-*#*-*# BEGIN UpdateServiceComponentImpl : updateService #*-*#*-*#*-*#", UpdateServiceComponentImpl.class);
		logger.debug("#*-*#*-*#*-*# BEGIN UpdateServiceComponentImpl : updateService #*-*#*-*#*-*#");
		HashMap request = new HashMap();
		request.put("Account", (HashMap)serviceReq.get("Account"));
		
		//Add extended Data
		HashMap requestExt = (HashMap)request.get("Account");
		requestExt.put("ExtendedData", MessageUtil.getExtendedData(requestExt, Constant.ACCOUNT_EXTDATA));
		//LoggerUtil.debugHashMap(PATH_LOG, "== request data ==", UpdateServiceComponentImpl.class, request);
		if (logger.isDebugEnabled())
			logger.debug("== request data == " + DebugUtil.getStringBuffer(request).toString());
		
		HashMap serviceUpdateList[] = (HashMap[])((HashMap)request.get("Account")).get("Services");
		HashMap serviceUpdateListResponse[] = null;
		HashMap serviceData = null;
		
		HashMap serviceUpdateResponse = null;
		//LoggerUtil.debug(PATH_LOG, "== Begin Loop update service ==", UpdateServiceComponentImpl.class);
		logger.debug("== Begin Loop update service ==");
		
		for(int i=0; i<serviceUpdateList.length;i++){
			// init variable
			serviceData = new HashMap();
			serviceUpdateResponse = new HashMap();
			serviceUpdateListResponse[i] = new HashMap();
			
			// process update servise call kenan api ServiceUpdate
			serviceData = (HashMap)serviceUpdateList[i];
//			serviceUpdateResponse = OrderedServiceImpl.updateService (serviceData, connection, context, PATH_LOG);
			serviceUpdateResponse = OrderedServiceImpl.updateService (serviceData, connection, context);
			//LoggerUtil.debugHashMap(PATH_LOG, "== serviceUpdateResponse loop "+ i +" ==", UpdateServiceComponentImpl.class, serviceUpdateResponse);
			if (logger.isDebugEnabled())
				logger.debug("== serviceUpdateResponse loop "+ i +" == " + DebugUtil.getStringBuffer(serviceUpdateResponse).toString());
			serviceUpdateListResponse[i].put("Service", serviceUpdateResponse);
		}
		//LoggerUtil.debug(PATH_LOG, "== End Loop update service ==", UpdateServiceComponentImpl.class);
		logger.debug("== End Loop update service ==");

		((HashMap)request.get("Account")).put("Services", serviceUpdateListResponse);
		//LoggerUtil.debug(PATH_LOG, "#*-*#*-*#*-*# END UpdateServiceComponentImpl : updateService #*-*#*-*#*-*#", UpdateServiceComponentImpl.class);
		logger.debug("#*-*#*-*#*-*# END UpdateServiceComponentImpl : updateService #*-*#*-*#*-*#");
		
		return request;
	}
	
}
