package com.cat.udb.esb.wrappers;

import java.util.HashMap;

import org.apache.log4j.Logger;

//import util.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.BaseWrapper;
import com.cat.udb.esb.wrappers.common.RetrieveAccountImpl;

import com.cat.ibacss.esb.bo.convert.RetrieveAccountConverter;
import com.cat.ibacss.esb.bo.convert.impl.RetrieveAccountConverterImpl;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.bali.connection.ApiMappings;
import commonj.sdo.DataObject;

public class RetrieveAccountWrapper extends BaseWrapper {
	
	private static Logger logger = Logger.getLogger(RetrieveAccountWrapper.class);
	
//	private Class CLASS = this.getClass();
//	
//	public RetrieveAccountWrapper() {
//		super(Constant.LOG4j_PROPERITES_PATH_UDB_A23);
//	}
	
	@Override
	protected void executeService() throws Throwable {

		//LoggerUtil.debug(PATH_LOG, "###### RetrieveAccountWrapper - executeService START ######", CLASS);
		logger.debug("###### RetrieveAccountWrapper - executeService START ######");
		
		HashMap mapRetrieveAccount = null;
		mapRetrieveAccount = mapToRequestServiceHashMap(requestObj);
		request = new HashMap();
		request.put("Account", (HashMap)mapRetrieveAccount);

		HashMap APIresponse = new HashMap();
		//APIresponse = RetrieveAccountImpl.accountGetDetail(request, connection, context, PATH_LOG);
		APIresponse = RetrieveAccountImpl.accountGetDetail(request, connection, context);
		
		response = APIresponse;
		//LoggerUtil.debug(PATH_LOG, "###### RetrieveAccountWrapper - executeService END ######", CLASS);
		logger.debug("###### RetrieveAccountWrapper - executeService END ######");
		
		return;
	}

	@Override
	protected DataObject prepareResponseObj(DataObject reqObj, HashMap response, Throwable error) throws Exception {
		
		MessageUtil msgHelper = new MessageUtil();
		HashMap result = msgHelper.getResult(error);
		String transactionLogId = msgHelper.getTransactionLogId(reqObj);
		DataObject responseObj = mapToResponseServiceDataObj(response, result, transactionLogId);
		return responseObj;
	}
	
	private HashMap mapToRequestServiceHashMap(DataObject requestObj) throws Exception{
		
		//Return Account
		RetrieveAccountConverter retrieveAccountConvert = new RetrieveAccountConverterImpl();
		HashMap requestHashMap = retrieveAccountConvert.dataObject2HashMap(requestObj);

		return requestHashMap;
	}
	
	private DataObject mapToResponseServiceDataObj(HashMap response, 
			HashMap result, String transactionLogId) throws Exception {
		
		HashMap responseHashMap = new HashMap();
		DataObject responseObj;
		
		if (response != null){
			responseHashMap.put("Account", (HashMap)response.get("Account"));
		}
		else{
			responseHashMap.put("Account", request);
		}
		
		responseHashMap.put("Result", result);
		responseHashMap.put("TransactionLogId", transactionLogId);
		
		//LoggerUtil.debug(PATH_LOG, "### mapToResponseServiceDataObj: ", CLASS);
		logger.debug("### mapToResponseServiceDataObj: ");
		//LoggerUtil.debugHashMap(PATH_LOG, "responseHashMap", CLASS, responseHashMap);
		if (logger.isDebugEnabled())
			logger.debug("responseHashMap " + DebugUtil.getStringBuffer(responseHashMap).toString());
		
		RetrieveAccountConverter retrieveAccountConvert = new RetrieveAccountConverterImpl();
		DataObject requestObj = retrieveAccountConvert.hashMap2DataObject(responseHashMap);
		return requestObj;
	}
	
	
}
