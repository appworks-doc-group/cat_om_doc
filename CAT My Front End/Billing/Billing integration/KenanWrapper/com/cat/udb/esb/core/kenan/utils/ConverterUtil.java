package com.cat.udb.esb.core.kenan.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import com.ibm.icu.text.SimpleDateFormat;

public class ConverterUtil {
	private static char vChar[] = new char[10];
	
	public static String toDateString(Date dt){
		SimpleDateFormat dateFormat = new SimpleDateFormat();
		String datePattern = "yyyy-MM-dd hh:mm:ss";
		
		dateFormat.applyPattern(datePattern);
		return dateFormat.format(dt);
	}
	
	public static HashMap[] toHashMapArray(List lt){
		HashMap[] array = new HashMap[lt.size()];
		for (int i = 0; i < lt.size(); i++) {
			array[i]= (HashMap)lt.get(i);
		}
		return array;
	}
	
	public static List getObjectList(HashMap rspObjList,String objListName,String ObjName) throws Exception {
		Object[] obj = null;
		HashMap[] ret = null;
		List responseList = new ArrayList();
		HashMap objOut = null;
		try{
			obj = (Object[]) rspObjList.get(objListName);
			ret = new HashMap[obj.length];
			objOut = new HashMap();
			for(int i=0;i<obj.length;i++){
				ret[i] = (HashMap) obj[i];
				objOut.put(ObjName, (HashMap)ret[i].clone());
				responseList.add((HashMap)objOut.clone());
			}
			obj = null;
			ret = null;
			objOut.clear();
		}catch (Exception e) {
			throw e;
		}
		return responseList;
	}
	
	public static HashMap[] getHashMapArray(HashMap rspObjList,String objListName,String ObjName) throws Exception {
		Object[] obj = null;
		HashMap[] responseList = null;
		HashMap objOut = null;
		try{
			obj = (Object[]) rspObjList.get(objListName);
			responseList = new HashMap[obj.length];
			objOut = new HashMap();
			for(int i=0;i<obj.length;i++){
				objOut.put(ObjName, ((HashMap) obj[i]).clone());
				responseList[i] = (HashMap)objOut.clone();
			}
			return responseList;
		}catch (Exception e) {
			throw e;
		}finally{
			obj = null;
			responseList = null;
			objOut = null;
		}
	}
	
	public static String frDate(String strName, Date dt) throws Exception{
		String datePattern[] = new String[1];
		SimpleDateFormat dateFormat = new SimpleDateFormat();
		datePattern[0] = "yyyy-MM-dd hh:mm:ss";
		try{
			for(int i=0;i<datePattern.length;){
				dateFormat.applyPattern(datePattern[i]);
				return dateFormat.format(dt);
			}
		}catch(Exception e){
			throw new Exception("Cannot convert " + strName + " which value is " + dt + "to String");
		}
		throw new Exception("Cannot convert " + strName + "'s value is " + dt + " to String");
	}
	
	public static Integer toInteger(String strName ,String  object) throws Throwable{
		vChar[0] = (char)48;
		vChar[1] = (char)49;
		vChar[2] = (char)50;
		vChar[3] = (char)51;
		vChar[4] = (char)52;
		vChar[5] = (char)53;
		vChar[6] = (char)54;
		vChar[7] = (char)55;
		vChar[8] = (char)56;
		vChar[9] = (char)57;
		if(StringUtils.containsOnly(object.trim(), vChar)) return Integer.valueOf(object);
		else throw new Throwable("Cannot convert " + strName + "'s value is " + object + " to Integer");
	}
}
