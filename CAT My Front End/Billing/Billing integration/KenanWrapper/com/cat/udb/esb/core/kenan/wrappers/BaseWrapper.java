package com.cat.udb.esb.core.kenan.wrappers;

import org.apache.log4j.Logger;

import java.util.HashMap;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.utils.ConnectionManager;

import com.csgsystems.aruba.connection.BSDMSessionContext;
//import com.csgsystems.aruba.connection.BSDMSettings;
import com.csgsystems.aruba.connection.Connection;

import commonj.sdo.DataObject;

public abstract class BaseWrapper {
	
	private static Logger logger = Logger.getLogger(BaseWrapper.class); 
	
	protected Connection connection = null;
	protected BSDMSessionContext context = null;
	protected HashMap request = null;
	protected HashMap response = null;
	protected DataObject requestObj;
	//protected String PATH_LOG;
	//private Class CLASS = this.getClass();
	
	//protected BaseWrapper(String path_log) {
	//	PATH_LOG = path_log;
	//}

	public DataObject execute(DataObject requestObj) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "###### Start Executing Service ######", CLASS);
		logger.debug("###### Start Executing Service ######");
		
		DataObject responseObj = null;
		//Throwable error = null;
		
		try {
			this.requestObj = requestObj;
			
			//init connection (we may use KenanConnectionManager from KenanUtil)
			//LoggerUtil.debug(PATH_LOG, "###### Connect to Kenan ######", CLASS);
			logger.debug("###### Connect to Kenan ######");
			
			//BSDMSettings settings = BSDMSettings.getDefault();
			context = BSDMSessionContext.getDefaultContext();
			
			//Init Connection
			connection = ConnectionManager.getConnection();
			
			//Begin Transaction
			connection.beginTransaction(150, 0);
	
			executeService();
			
			//End Transaction
			connection.endTransaction(0);
			
			responseObj = prepareResponseObj(requestObj, response, null);		
		}
		catch (Throwable e){
			//e.printStackTrace();
			logger.error(DebugUtil.getStackTrace(e));
			
			if (connection != null){
				try {
					connection.abortTransaction(0);
				}
				catch (Exception e1) {
					//e.printStackTrace();
					logger.warn(DebugUtil.getStackTrace(e1));
				}
				logger.debug("###### Catch - transaction aborted ######");
			}
			
			if (response == null) {
				response = new HashMap();
			}
			
			logger.debug("###### Catch - build response ######");
			responseObj = prepareResponseObj(requestObj, response, e);
		}
		finally {
			try {
				if (connection != null) {
					connection.close();
					//LoggerUtil.debug(PATH_LOG, "###### Finally - close connection ######", CLASS);
					logger.debug("###### Finally - connection closed ######");
				}
			}
			catch (Throwable t) {
				logger.warn(DebugUtil.getStackTrace(t));
			}
		}
		
		return responseObj; 
	}

	protected abstract DataObject prepareResponseObj(DataObject reqObj, HashMap response, Throwable error) throws Exception;
	protected abstract void executeService() throws Throwable;
}
