package com.cat.udb.esb.wrappers;

import com.cat.udb.esb.wrappers.common.RetrieveAccountImpl;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

//import util.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.BaseWrapper;

import com.cat.ibacss.esb.bo.convert.RetrieveAccountConverter;
import com.cat.ibacss.esb.bo.convert.impl.RetrieveAccountConverterImpl;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import commonj.sdo.DataObject;

public class RetrieveAccountListWrapper extends BaseWrapper {
	
	private static Logger logger = Logger.getLogger(RetrieveAccountListWrapper.class);
	
	//private static final String PATH_LOG = Constant.LOG4j_PROPERITES_PATH_UDB_A23;
//	private Class CLASS = this.getClass();
//	
//	public RetrieveAccountListWrapper() {
//		super(Constant.LOG4j_PROPERITES_PATH_UDB_A23);
//	}
	
	@Override
	protected void executeService() throws Throwable {
		
		//LoggerUtil.debug(PATH_LOG, "###### RetrieveAccountListWrapper - executeService START ######", CLASS);
		logger.debug("###### RetrieveAccountListWrapper - executeService START ######");
		request = new HashMap();
		response = new HashMap();
		HashMap requestTmp = new HashMap();
		
		HashMap APIresponse = new HashMap();
		HashMap mapRetrieveAccountList = new HashMap();
		mapRetrieveAccountList = mapToRequestServiceHashMap(requestObj);
		mapRetrieveAccountList.put("Fetch", true);
		Iterator iterator = (mapRetrieveAccountList.keySet()).iterator();
		
		while(iterator.hasNext()) {
			String key = iterator.next().toString();
			String value = "ServiceExternalId";
			String value2 = "ServiceExternalIdType";
			String value3 = "BillRefResets"; 
			String value4 = "BillRefNo";
			
			if(value.equals(key) || value2.equals(key)){	
				request.put("CustomerIdEquipMap", mapRetrieveAccountList);
				//LoggerUtil.debugHashMap(PATH_LOG, "###### request CustomerIdEquipMap ######", CLASS, request);
				if (logger.isDebugEnabled())
					logger.debug("###### request CustomerIdEquipMap ###### " + DebugUtil.getStringBuffer(request).toString());
//				APIresponse = RetrieveAccountImpl.retrieveAccountByMobileNumber(request, connection, context, PATH_LOG);
				APIresponse = RetrieveAccountImpl.retrieveAccountByMobileNumber(request, connection, context);
				break;
			} 
			
			if(value3.equals(key) || value4.equals(key)){
				requestTmp.put("Key", mapRetrieveAccountList);
				requestTmp.put("Fetch", true);
				request.put("Invoice", requestTmp);
				//LoggerUtil.debugHashMap(PATH_LOG, "###### request Invoice ######", CLASS, request);
				if (logger.isDebugEnabled())
					logger.debug("###### request Invoice ###### " + DebugUtil.getStringBuffer(request).toString());
//				APIresponse = RetrieveAccountImpl.retrieveAccountByInvoiceNumber(request, connection, context, PATH_LOG);
				APIresponse = RetrieveAccountImpl.retrieveAccountByInvoiceNumber(request, connection, context);
				break;
			}
			
			if(!value3.equals(key) && !value.equals(key) && !"Fetch".equals(key) && !value2.equals(key) && !value4.equals(key)){
				request.put("AccountLocate", mapRetrieveAccountList);
				//LoggerUtil.debugHashMap(PATH_LOG, "###### request AccountLocate ######", CLASS, request);
				if (logger.isDebugEnabled())
					logger.debug("###### request AccountLocate ###### " + DebugUtil.getStringBuffer(request).toString());
//				APIresponse = RetrieveAccountImpl.retrieveAccountList(request, connection, context, PATH_LOG);
				APIresponse = RetrieveAccountImpl.retrieveAccountList(request, connection, context);
				break;
			}
		}
		
		//LoggerUtil.debugHashMap(PATH_LOG, "###### APIresponse ######", CLASS, APIresponse);
		if (logger.isDebugEnabled())
			logger.debug("###### APIresponse ###### " + DebugUtil.getStringBuffer(APIresponse).toString());
		//LoggerUtil.debug(PATH_LOG, "###### RetrieveAccountListWrapper - executeService END ######", CLASS);
		logger.debug("###### RetrieveAccountListWrapper - executeService END ######");
		response = APIresponse;
		
		return;
	}
	
	@Override
	protected DataObject prepareResponseObj(DataObject reqObj, HashMap response, Throwable error) throws Exception {
		MessageUtil msgHelper = new MessageUtil();
		HashMap result = msgHelper.getResult(error);
		String transactionLogId = msgHelper.getTransactionLogId(reqObj);
		DataObject responseObj = mapToResponseServiceDataObj(response, result, transactionLogId);
		return responseObj;
	}
	
	private HashMap mapToRequestServiceHashMap(DataObject requestObj) throws Exception {
		RetrieveAccountConverter reListConverter = new RetrieveAccountConverterImpl();
		HashMap requestHashMap = reListConverter.dataObject2HashMapList(requestObj);

		return requestHashMap;
	}
	
	private DataObject mapToResponseServiceDataObj(HashMap response, HashMap result, String transactionLogId) throws Exception {
		
		HashMap responseHashMap = new HashMap();
		DataObject responseObj = null;
		
		if (response != null){
			responseHashMap = response;
		}
		else{
			responseHashMap.put("AccountLocate", request.get("AccountLocate"));
		}
		
		responseHashMap.put("Result", result);
		responseHashMap.put("TransactionLogId", transactionLogId);
		
		//LoggerUtil.debugHashMap(PATH_LOG, "responseHashMap", CLASS, responseHashMap);
		if (logger.isDebugEnabled())
			logger.debug("responseHashMap " + DebugUtil.getStringBuffer(responseHashMap).toString());
		
		RetrieveAccountConverter retrieveAccountlistConvert = new RetrieveAccountConverterImpl();
		if(responseHashMap != null){
			if (retrieveAccountlistConvert.checkList(responseHashMap)) {
				responseObj = retrieveAccountlistConvert.hashMapList2DataObject(responseHashMap);
			}
			else {
				responseObj = retrieveAccountlistConvert.hashMapSearch2DataObject(responseHashMap);
			}
		}
		
		return responseObj;
	}
}
