package com.cat.udb.esb.core.kenan.utils;

//import java.util.ResourceBundle;

//import com.cat.ibacss.esb.config.key.UDBKeyBundle;

public final class Constant {
	
	//private static final String ROOT_PATH = com.cat.ibacss.esb.bo.constant.Constant.ROOT_PATH;
	//public static final String INTERIM_BILL_URL_PROVIDER = ResourceBundle.getBundle("init_config_UDB").getString(UDBKeyBundle.KEY_INTERIM_BILL_URL.toString()); //prod
	
	/**
	 *  Extended Data Type
	 */
	public static final String ACCOUNT_EXTDATA = "ACCOUNT_EXTDATA";
	public static final String SERVICE_EXTDATA = "SERVICE_EXTDATA";
	
	/**
	 *  Extended Data File
	 */
	public static final String API_EXTDATA= "api_extdata.properties";
	
	/**
	 *  UDB_A2_CreateService
	 */
	//public static final String LOG4j_PROPERITES_PATH_UDB_A2 = ROOT_PATH + "UDB_A2_log4j.xml";
	
	/**
	 *  UDB_A23_RetrieveAccount
	 */
	//public static final String LOG4j_PROPERITES_PATH_UDB_A23 = ROOT_PATH+"UDB_A23_log4j.xml";
	
	/**
	 *  UDB_A14_UpdateAccount
	 */
	//public static final String LOG4j_PROPERITES_PATH_UDB_A14 = ROOT_PATH+"UDB_A14_log4j.xml";	
	
	/**
	 * UDB_A37_UpdateInventoryStatus
	 */
	//public static final String LOG4j_PROPERTIES_PATH_UDB_A37 = ROOT_PATH+"UDB_A37_log4j.xml";
	
	/**
	 * UDB_A45_CreateInterimBill
	 */
	//public static final String Log4j_PROPERTIES_PATH_UDB_A45 = ROOT_PATH + "UDB_A45_log4j.xml";
	
	/**
	 * UDB_C13_RetrieveInvoice
	 */
	//public static final String LOG4j_PROPERTIES_PATH_UDB_C13 = ROOT_PATH + "UDB_C13_log4j.xml";
	
	/**
	 * UDB_F10_RetrieveInvoice
	 */
	//public static final String LOG4j_PROPERTIES_PATH_UDB_F10 = ROOT_PATH + "UDB_F10_log4j.xml";
	
	/**
	 *  UDB_A1_CreateAccount
	 */
	//public static final String LOG4j_PROPERITES_PATH_UDB_A1 = ROOT_PATH+"UDB_A1_log4j.xml";
	
	/**
	 *  UDB_A15_UpdatePaymentMethod
	 */
	//public static final String LOG4j_PROPERITES_PATH_UDB_A15 = ROOT_PATH+"UDB_A15_log4j.xml";	
	
	/**
	 * UDB_A8_UpdateCiem
	 */
	//public static final String LOG4j_PROPERTIES_PATH_UDB_A8 = ROOT_PATH + "UDB_A8_log4j.xml";

	/**
	 * UDB_A3_SuspendService
	 */
	//public static final String LOG4j_PROPERTIES_PATH_UDB_A3 = ROOT_PATH + "UDB_A3_log4j.xml";
	
	/**
	 * UDB_A5_TerminateService
	 */
	//public static final String LOG4j_PROPERTIES_PATH_UDB_A5 = ROOT_PATH + "UDB_A5_log4j.xml";
	
	/**
	 * UDB_A4_ReconnectService
	 */
	//public static final String LOG4j_PROPERTIES_PATH_UDB_A4 = ROOT_PATH + "UDB_A4_log4j.xml";
	
	/**
	 * UDB_A42_CancelOrder
	 */
	//public static final String LOG4j_PROPERTIES_PATH_UDB_A42 = ROOT_PATH + "UDB_A42_log4j.xml";
	
	/**
	 * UDB_A7_UpdateServicePackage
	 */
	//public static final String LOG4j_PROPERTIES_PATH_UDB_A7 = ROOT_PATH + "UDB_A7_log4j.xml";

	/**
	 * UDB_A38_UpdateServiceComponent
	 */
	//public static final String LOG4j_PROPERTIES_PATH_UDB_A38 = ROOT_PATH + "UDB_A38_log4j.xml";
	
	/**
	 * UDB_F14_RetrievePayment
	 */
	//public static final String LOG4j_PROPERTIES_PATH_UDB_F14 = ROOT_PATH + "UDB_F14_log4j.xml";
	
	/**
	 * UDB_F4_CreatePayment
	 */
	//public static final String LOG4j_PROPERTIES_PATH_UDB_F4 = ROOT_PATH + "UDB_F4_log4j.xml";
}

