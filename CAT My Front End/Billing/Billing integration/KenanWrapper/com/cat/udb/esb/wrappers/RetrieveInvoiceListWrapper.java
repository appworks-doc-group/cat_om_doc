package com.cat.udb.esb.wrappers;

import java.util.HashMap;

import org.apache.log4j.Logger;

//import util.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.BaseWrapper;

import com.cat.ibacss.esb.bo.convert.InvoiceConverter;
import com.cat.ibacss.esb.bo.convert.impl.InvoiceConverterImpl;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.bali.connection.ApiMappings;
import commonj.sdo.DataObject;

public class RetrieveInvoiceListWrapper extends BaseWrapper {
	
	private static Logger logger = Logger.getLogger(RetrieveInvoiceListWrapper.class);
//	private Class CLASS = this.getClass();
//	
//	public RetrieveInvoiceListWrapper(){
//		super(Constant.LOG4j_PROPERTIES_PATH_UDB_F10);
//	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void executeService() throws Throwable {
		//LoggerUtil.debug(PATH_LOG, "#### start executeService() ####", CLASS);
		logger.debug("#### start executeService() ####");
		
		HashMap requestHashMap = new HashMap();
		HashMap responseHashMap = new HashMap();
		HashMap invoiceList = new HashMap();
		
		requestHashMap = mapToRequestServiceHashMap(requestObj);
		
		HashMap returnInvoice = (HashMap)requestHashMap.get("Invoice");
		invoiceList.put("Invoice", (HashMap)returnInvoice.clone());
		
		responseHashMap = connection.call(context, ApiMappings.getCallName("InvoiceFind"), invoiceList);
		response = responseHashMap;
		
		//LoggerUtil.debug(PATH_LOG, "#### end executeService() ####", CLASS);
		logger.debug("#### end executeService() ####");
	}

	@SuppressWarnings("unchecked")
	@Override
	protected DataObject prepareResponseObj(DataObject reqObj,HashMap response, Throwable error) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "#### start prepareResponseObj() ####", CLASS);
		logger.debug("#### start prepareResponseObj() ####");
		HashMap result = MessageUtil.getResult(error);
		response.put("Result", result);
		response.put("TransactionLogId", MessageUtil.getTransactionLogId(reqObj));
		DataObject responseObj;
		responseObj = mapToResponseServiceDataObject(response);
		//LoggerUtil.debug(PATH_LOG, "#### end prepareResponseObj() ####", CLASS);
		logger.debug("#### end prepareResponseObj() ####");
		
		return responseObj;
	}
	
	private HashMap mapToRequestServiceHashMap(DataObject requestObj) throws Exception{
		//LoggerUtil.debug(PATH_LOG, "#### start mapToRequestServiceHashMap() ####", CLASS);
		logger.debug("#### start mapToRequestServiceHashMap() ####");
		
		HashMap response;
		InvoiceConverter converter = new InvoiceConverterImpl();
		response = converter.dataObject2HashMapInvoiceList(requestObj);
		//LoggerUtil.debugHashMap(PATH_LOG, "RetrieveInvoiceListRequest", CLASS, response);
		if (logger.isDebugEnabled())
			logger.debug("RetrieveInvoiceListRequest " + DebugUtil.getStringBuffer(response).toString());
		
		//LoggerUtil.debug(PATH_LOG, "#### end mapToRequestServiceHashMap() ####", CLASS);
		logger.debug("#### end mapToRequestServiceHashMap() ####");
		
		return response;
	}
	
	private DataObject mapToResponseServiceDataObject(HashMap responseHashMap) throws Exception{
		//LoggerUtil.debug(PATH_LOG, "#### start mapToResponseServiceDataObject() ####", CLASS);
		logger.debug("#### start mapToResponseServiceDataObject() ####");
		
		DataObject response;
		
		//LoggerUtil.debugHashMap(PATH_LOG, "RetrieveInvoiceListResponse", CLASS, responseHashMap);
		if (logger.isDebugEnabled())
			logger.debug("RetrieveInvoiceListResponse " + DebugUtil.getStringBuffer(responseHashMap).toString());
		
		InvoiceConverter converter = new InvoiceConverterImpl();
		response = converter.hashMap2DataObjectInvoiceList(responseHashMap);
		
		//LoggerUtil.debug(PATH_LOG, "#### end mapToResponseServiceDataObject() ####", CLASS);
		logger.debug("#### end mapToResponseServiceDataObject() ####");
		
		return response;
	}

}
