package com.cat.udb.esb.wrappers;

import java.util.HashMap;

import org.apache.log4j.Logger;

//import util.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.BaseWrapper;

import com.cat.ibacss.esb.bo.convert.UnbillingUsageConverter;
import com.cat.ibacss.esb.bo.convert.impl.UnbillingUsageConverterImpl;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.bali.connection.ApiMappings;
import commonj.sdo.DataObject;

public class RetrieveBillingWrapper extends BaseWrapper {
	
	private static Logger logger = Logger.getLogger(RetrieveBillingWrapper.class);
//	protected Class CLASS = this.getClass();
//	
//	public RetrieveBillingWrapper(){
//		super(Constant.LOG4j_PROPERTIES_PATH_UDB_A18);
//	}

	@SuppressWarnings("unchecked")
	@Override
	protected void executeService() throws Throwable {
		//LoggerUtil.debug(PATH_LOG, "#### Start executeService ####", CLASS);
		logger.debug("#### Start executeService ####");
		request = mapToRequestServiceHashMap(requestObj);
		HashMap unbilling = new HashMap();
		unbilling.put("UnbilledUsage", (HashMap)request.get("UnbilledUsage"));
		response = connection.call(context, ApiMappings.getCallName("UnbilledUsageFind"), unbilling);
		
		//LoggerUtil.debug(PATH_LOG, "#### End executeService ####", CLASS);
		logger.debug("#### End executeService ####");
	}

	@SuppressWarnings("unchecked")
	@Override
	protected DataObject prepareResponseObj(DataObject reqObj, HashMap response, Throwable error) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "#### Start prepareResponseObj ####", CLASS);
		logger.debug("#### Start prepareResponseObj ####");
		DataObject responseObj = null;
		HashMap result = MessageUtil.getResult(error);
		String transactionLogId = MessageUtil.getTransactionLogId(reqObj);
		response.put("Result", result);
		response.put("TransactionLogId", transactionLogId);
		responseObj = mapToResponseServiceObj(response);
		//LoggerUtil.debug(PATH_LOG, "#### End prepareResponseObj ####", CLASS);
		logger.debug("#### End prepareResponseObj ####");
		
		return responseObj;
	}
	
	private HashMap mapToRequestServiceHashMap(DataObject requestObj) throws Exception{
		//LoggerUtil.debug(PATH_LOG, "#### Start mapToRequestServiceHasMap ####", CLASS);
		logger.debug("#### Start mapToRequestServiceHasMap ####");
		HashMap respHashMap = null;
		UnbillingUsageConverter converter = new UnbillingUsageConverterImpl();
		respHashMap = converter.dataObjectRetrieveUnbilling2HashMap(requestObj);
		//LoggerUtil.debugHashMap(PATH_LOG, "RetrieveBillingRequest", CLASS, respHashMap);
		if (logger.isDebugEnabled())
			logger.debug("RetrieveBillingRequest " + DebugUtil.getStringBuffer(respHashMap).toString());
		//LoggerUtil.debug(PATH_LOG, "#### End mapToRequestServiceHasMap ####", CLASS);
		logger.debug("#### End mapToRequestServiceHasMap ####");
		
		return respHashMap;
	}
	
	private DataObject mapToResponseServiceObj(HashMap responseHashmap) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "#### Start mapToResponseServiceObj ####", CLASS);
		logger.debug("#### Start mapToResponseServiceObj ####");
		//LoggerUtil.debugHashMap(PATH_LOG, "RetrieveBillingResponse", CLASS, responseHashmap);
		if (logger.isDebugEnabled())
			logger.debug("RetrieveBillingResponse " + DebugUtil.getStringBuffer(responseHashmap).toString());
		DataObject respObj = null;
		UnbillingUsageConverter converter = new UnbillingUsageConverterImpl();
		respObj = converter.HashMapRetrieveUnbillingList2dataObject(responseHashmap);
		//LoggerUtil.debug(PATH_LOG, "#### End mapToResponseServiceObj ####", CLASS);
		logger.debug("#### End mapToResponseServiceObj ####");
		
		return respObj;
	}
}
