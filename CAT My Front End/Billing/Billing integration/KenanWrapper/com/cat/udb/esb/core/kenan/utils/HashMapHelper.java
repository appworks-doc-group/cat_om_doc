package com.cat.udb.esb.core.kenan.utils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
//import commonj.sdo.DataObject;

//import com.cat.ibacss.esb.bo.convert.ServiceChangeConverter;
//import com.cat.ibacss.esb.bo.convert.impl.ServiceChangeConverterImpl;
import com.csgsystems.aruba.connection.ServiceException;

public class HashMapHelper {
	public static HashMap<String, Object> ConvertObjectToMap(Object obj) throws 
    IllegalAccessException, 
    IllegalArgumentException, 
    InvocationTargetException {
        Class<?> pomclass = obj.getClass();
        pomclass = obj.getClass();
        Method[] methods = obj.getClass().getMethods();


        HashMap<String, Object> map = new HashMap<String, Object>();
        for (Method m : methods) {
           if (m.getName().startsWith("get") && !m.getName().startsWith("getClass")) {
              Object value = (Object) m.invoke(obj);
              System.out.println("name=" + m.getName().substring(3) + ", value=" + value);
              map.put(m.getName().substring(3), (Object) value);
           }
        }
        return map;
	} 
	
	public static void printHashMap(Map map) {
		try {
	//		 Create a BufferedWriter
			BufferedWriter bw = new BufferedWriter(new PrintWriter(System.out));
	//		 Pass the map to the ServiceException object's print() method
			ServiceException.printMap(bw, map, 2);
	//		 Flush the BufferedWriter
			bw.flush();
		}catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
