package com.cat.udb.esb.wrappers.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.cat.udb.esb.core.kenan.utils.ConverterUtil;
//import util.HashMapHelper;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;
import com.cat.udb.esb.core.utils.DebugUtil;

//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.csgsystems.aruba.connection.Connection;
import com.csgsystems.bali.connection.ApiMappings;

public class ProductNameOverrideImpl {
	
	private static Logger logger = Logger.getLogger(ProductNameOverrideImpl.class);

	public static HashMap updateProductExtendedData(HashMap product, Object[] extentedData, 
		Connection connection, BSDMSessionContext context) throws Throwable{
		
		//Init request
		
		/*
		HashMap override = new HashMap();
		override.put("ParamName", "ProductOverrideName");
		override.put("ParamId", 30001);
		override.put("ParamValue", "TestName");
		*/
		HashMap productUpdate = new HashMap();
		HashMap requestProduct = new HashMap();
		
		productUpdate.put("Key", (HashMap)product.get("Key"));
		productUpdate.put("ExtendedData", extentedData);
		requestProduct.put("Product", productUpdate);
		
		//Call API
		if (logger.isDebugEnabled())
			logger.debug("###### ProductExtendedDataUpdate Request ###### " + DebugUtil.getStringBuffer(requestProduct).toString());
		HashMap productUpdateResponse = connection.call(context, ApiMappings.getCallName("ProductUpdate"), requestProduct);
		if (logger.isDebugEnabled())
			logger.debug("###### ProductExtendedDataUpdate Response ###### " + DebugUtil.getStringBuffer(productUpdateResponse).toString());
		
		return productUpdateResponse;
	}	
}
