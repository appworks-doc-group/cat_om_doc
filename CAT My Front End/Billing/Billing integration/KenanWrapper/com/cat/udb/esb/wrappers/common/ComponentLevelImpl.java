package com.cat.udb.esb.wrappers.common;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cat.udb.esb.core.kenan.utils.ConverterUtil;
import com.cat.udb.esb.core.utils.DebugUtil;

//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.csgsystems.aruba.connection.Connection;

public class ComponentLevelImpl {
	
	private static Logger logger = Logger.getLogger(ComponentLevelImpl.class);
	
	//public static HashMap[] createComponentLevel(HashMap[] componentList, HashMap orderedServiceCreateResponse, HashMap order, 
	//		HashMap[] orderedPackages,Connection connection, BSDMSessionContext context,String PATH_LOG) throws Throwable{
	public static HashMap[] createComponentLevel(HashMap[] componentList, HashMap orderedServiceCreateResponse, HashMap order, 
			HashMap[] orderedPackages,Connection connection, BSDMSessionContext context ,Object currencyCode) throws Throwable{
		
		ArrayList componentResponselist = new ArrayList();
		for (HashMap component : componentList) {
			
			HashMap componentField = (HashMap)component.get("Component");

			if (componentField.containsKey("Corridor")){
				//Create Corridor
				//LoggerUtil.debug(PATH_LOG, "###### CorridorCreate ######", ComponentLevelImpl.class);
				logger.debug("###### CorridorCreate ######");
				//HashMap corridor = CorridorImpl.createCorridor(componentField, orderedServiceCreateResponse,connection, context,PATH_LOG);
				HashMap corridor = CorridorImpl.createCorridor(componentField, orderedServiceCreateResponse,connection, context);
				componentField.putAll(corridor);
				componentResponselist.add(component);
			}
			else {
				//Create Component
				//LoggerUtil.debug(PATH_LOG, "###### OrderedComponentCreate ######", ComponentLevelImpl.class);
				logger.debug("###### OrderedComponentCreate ######");
//				HashMap componentOrderedCreateResponse = OrderedComponentImpl.createOrderedComponent(component, orderedServiceCreateResponse, 
//						order, orderedPackages, connection, context,PATH_LOG);
				HashMap componentOrderedCreateResponse = OrderedComponentImpl.createOrderedComponent(component, orderedServiceCreateResponse, 
						order, orderedPackages, connection, context,currencyCode);
				
				componentResponselist.add(componentOrderedCreateResponse);
			}						
		}
		return ConverterUtil.toHashMapArray(componentResponselist);
	} 
	
	//public static HashMap[] modifyComponentLevel(HashMap[] componentList, HashMap orderedServiceModifyResponse, HashMap order, 
	//		Connection connection, BSDMSessionContext context,String PATH_LOG) throws Throwable{
	public static HashMap[] modifyComponentLevel(HashMap[] componentList, HashMap orderedServiceModifyResponse, HashMap order, 
			Connection connection, BSDMSessionContext context, Object currencyCode) throws Throwable{
		
		//LoggerUtil.debugHashMap(PATH_LOG, " == orderedServiceModifyResponse == ", ComponentLevelImpl.class, orderedServiceModifyResponse);
		if (logger.isDebugEnabled())
			logger.debug(" == orderedServiceModifyResponse == " + DebugUtil.getStringBuffer(orderedServiceModifyResponse).toString());
		ArrayList componentResponselist = new ArrayList();
		HashMap serviceRequestField = (HashMap)orderedServiceModifyResponse.get("Service");
		
		for (HashMap orderComponent  : componentList) {
			HashMap component = (HashMap)orderComponent.get("OrderedComponent");
			//LoggerUtil.debugHashMap(PATH_LOG, " == component in modifyComponentLevel == ", ComponentLevelImpl.class, component);
			if (logger.isDebugEnabled())
				logger.debug(" == component in modifyComponentLevel == " + DebugUtil.getStringBuffer(component).toString());
			
			HashMap componentField = (HashMap)component.get("Component");			
			if(component.get("ActionType").equals("CREATE")){
				
				if(componentField.containsKey("Corridor")){					
					//Create Corridor
					//LoggerUtil.debug(PATH_LOG, "###### CorridorCreate ######", ComponentLevelImpl.class);
					logger.debug("###### CorridorCreate ######");
					//HashMap corridor = CorridorImpl.createCorridor(componentField, null, connection, context,PATH_LOG);
					HashMap corridor = CorridorImpl.createCorridor(componentField, null, connection, context);
					componentField.putAll(corridor);
					componentResponselist.add(component);
				}
				else{					
					//Create Component
					//LoggerUtil.debug(PATH_LOG, "###### OrderedComponentCreate ######", ComponentLevelImpl.class);
					logger.debug("###### OrderedComponentCreate ######");
					component.put("CurrencyCode", Integer.parseInt(serviceRequestField.get("CurrencyCode").toString()));
//					HashMap componentOrderedCreateResponse_tmp = OrderedComponentImpl.createOrderedComponent(component, null, 
//							order, null, connection, context,PATH_LOG);
					HashMap componentOrderedCreateResponse_tmp = OrderedComponentImpl.createOrderedComponent(component, null, 
							order, null, connection, context,currencyCode);
					HashMap componentOrderedCreateResponse = new HashMap();
					componentOrderedCreateResponse.put("OrderedComponent", componentOrderedCreateResponse_tmp);
					componentResponselist.add(componentOrderedCreateResponse);
				}	
			}
			else if(component.get("ActionType").equals("DISCONNECT")){

				if (componentField.containsKey("Corridor")){					
					//Update Corridor
					//LoggerUtil.debug(PATH_LOG, "###### CorridorUpdate ######", ComponentLevelImpl.class);
					logger.debug("###### CorridorUpdate ######");
//					CorridorImpl.updateCorridor(componentField, orderedServiceModifyResponse,connection, context,PATH_LOG);
					CorridorImpl.updateCorridor(componentField, orderedServiceModifyResponse,connection, context);
					componentResponselist.add(component);
				}
				else {					
					//Disconnect Component
					//LoggerUtil.debug(PATH_LOG, "###### OrderedComponentDisconnect ######", ComponentLevelImpl.class);
					logger.debug("###### OrderedComponentDisconnect ######");
//					HashMap componentOrderedDisconnectResponse = OrderedComponentImpl.disconnectOrderedComponent(orderComponent, orderedServiceModifyResponse, 
//							order, connection, context,PATH_LOG);
					HashMap componentOrderedDisconnectResponse = OrderedComponentImpl.disconnectOrderedComponent(orderComponent, orderedServiceModifyResponse, 
							order, connection, context);
					componentResponselist.add(componentOrderedDisconnectResponse);
				}
			}
			else
				throw new Exception("OrderedProduct.ActionType's value not match.");
		}
		return ConverterUtil.toHashMapArray(componentResponselist);
	} 
}
