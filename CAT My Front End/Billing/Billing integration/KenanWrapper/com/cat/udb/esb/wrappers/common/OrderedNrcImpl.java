package com.cat.udb.esb.wrappers.common;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cat.udb.esb.core.kenan.utils.ConverterUtil;
import com.cat.udb.esb.core.utils.DebugUtil;

//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.csgsystems.aruba.connection.Connection;
import com.csgsystems.bali.connection.ApiMappings;

public class OrderedNrcImpl {
	
	private static Logger logger = Logger.getLogger(OrderedNrcImpl.class);

	//public static HashMap[] createOrderedNrc(HashMap[] orderedNrcList, HashMap service, 
	//		HashMap order, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Throwable{
	public static HashMap[] createOrderedNrc(HashMap[] orderedNrcList, HashMap service, HashMap order, Connection connection, BSDMSessionContext context) throws Throwable {
		if (logger.isDebugEnabled())
			logger.debug("createOrderedNrc(orderedNrcList, service, order, connection, context)...");
		
		ArrayList orderedNrcResponseList = new ArrayList();
		
		Object orderField = order.get("Order");
		
		for (HashMap orderNrcs : orderedNrcList){
			HashMap orderNrcsField = (HashMap) orderNrcs.get("OrderedNrc");
			
			//Init request
			orderNrcsField.put("Order", orderField);
			orderNrcsField.put("FindExistingSO", true);
			
			if (service != null) {
				HashMap serviceField = (HashMap)service.get("Service");
				((HashMap)orderNrcsField.get("Nrc")).put("ParentServiceInternalId", serviceField.get("ServiceInternalId"));
				((HashMap)orderNrcsField.get("Nrc")).put("ParentServiceInternalIdResets", serviceField.get("ServiceInternalIdResets"));
			}
			
			//Call API
			//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedNrcCreate Request ######" , OrderedNrcImpl.class, orderNrcsField);
			if (logger.isDebugEnabled())
				logger.debug("###### OrderedNrcCreate Request ###### " + DebugUtil.getStringBuffer(orderNrcsField).toString());
			HashMap orderedNrcCreateResponse = connection.call(context, ApiMappings.getCallName("OrderedNrcCreate"), orderNrcsField);
			
			//Set ServiceLineID & SourceLineItemId
			HashMap orderedNrcResponse = new HashMap();
			HashMap orderNrcElement = new HashMap();
			orderNrcElement.put("Nrc", ((Object[])orderedNrcCreateResponse.get("NrcList"))[0]);
			orderNrcElement.put("SourceLineItemId", orderNrcsField.get("SourceLineItemId"));
			
			orderedNrcResponse.put("OrderedNrc", orderNrcElement);
			
			//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedNrcCreate Response ######" , OrderedNrcImpl.class, orderedNrcResponse);
			if (logger.isDebugEnabled())
				logger.debug("###### OrderedNrcCreate Response ###### " + DebugUtil.getStringBuffer(orderedNrcResponse).toString());
			orderedNrcResponseList.add(orderedNrcResponse);
		}
		return ConverterUtil.toHashMapArray(orderedNrcResponseList);
	}
	
//	public static HashMap[] modifyOrderedNrc(HashMap[] orderedNrcList, HashMap service, 
//			HashMap order, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Throwable{
	public static HashMap[] modifyOrderedNrc(HashMap[] orderedNrcList, HashMap service, 
			HashMap order, Connection connection, BSDMSessionContext context) throws Throwable{
		
		if (logger.isDebugEnabled())
			logger.debug("modifyOrderedNrc(orderedNrcList, service, order, connection, context)...");
		
		ArrayList orderedNrcResponseList = new ArrayList();

		Object orderField = order.get("Order");
		//int i = 0;
		
		for (HashMap orderNrcs : orderedNrcList) {
			//i += 1;
			
			HashMap orderNrcsField = (HashMap)orderNrcs.get("OrderedNrc");
			HashMap orderNrcElement = new HashMap();
			
			//Init request
			orderNrcsField.put("Order", orderField);
			/* Commented out on 22 Sep 2014 by Kittiwut P. to make the message structure to be the same as the original IBACSS interface
			orderNrcsField.put("FindExistingSO", true);
			*/
			
			//logger.debug("i = " + i);
			//if (i != 1) {
			orderNrcsField.put("FindExistingSO", true);
			//}
			
			if (service != null) {
				HashMap serviceField = (HashMap)service.get("Service");
				((HashMap)orderNrcsField.get("Nrc")).put("ParentServiceInternalId", serviceField.get("ServiceInternalId"));
				((HashMap)orderNrcsField.get("Nrc")).put("ParentServiceInternalIdResets", serviceField.get("ServiceInternalIdResets"));
			}
			
			//Call API
			HashMap orderedNrcModifyAPIResponse = null;
			
			if (orderNrcsField.get("ActionType").equals("CREATE")) {
				if (logger.isDebugEnabled())
					logger.debug("###### OrderedNrcCreate Request ###### " + DebugUtil.getStringBuffer(orderNrcsField).toString());
				orderedNrcModifyAPIResponse = connection.call(context, ApiMappings.getCallName("OrderedNrcCreate"), orderNrcsField);
				orderedNrcModifyAPIResponse.put("SourceLineItemId", orderNrcsField.get("SourceLineItemId"));
				orderNrcElement.put("Nrc", ((Object[])orderedNrcModifyAPIResponse.get("NrcList"))[0]);
			}
			
			HashMap orderedNrcResponse = new HashMap();
			orderNrcElement.put("SourceLineItemId", orderNrcsField.get("SourceLineItemId"));
			orderedNrcResponse.put("OrderedNrc", orderNrcElement);
			
			if (logger.isDebugEnabled())
				logger.debug("###### OrderedNrcCreate Response ###### " + DebugUtil.getStringBuffer(orderedNrcResponse).toString());
			orderedNrcResponseList.add(orderedNrcResponse);
		}
		
		for (HashMap orderNrcs : orderedNrcList) {			
			HashMap orderNrcsField = (HashMap)orderNrcs.get("OrderedNrc");
			HashMap orderNrcElement = new HashMap();
			
			//Init request
			orderNrcsField.put("Order", orderField);
			/* Commented out on 22 Sep 2014 by Kittiwut P. to make the message structure to be the same as the original IBACSS interface
			orderNrcsField.put("FindExistingSO", true);
			*/
			
			//logger.debug("i = " + i);
			//if (i != 1) {
			orderNrcsField.put("FindExistingSO", true);
			//}
			
			if (service != null) {
				HashMap serviceField = (HashMap)service.get("Service");
				((HashMap)orderNrcsField.get("Nrc")).put("ParentServiceInternalId", serviceField.get("ServiceInternalId"));
				((HashMap)orderNrcsField.get("Nrc")).put("ParentServiceInternalIdResets", serviceField.get("ServiceInternalIdResets"));
			}
			
			//Call API
			HashMap orderedNrcModifyAPIResponse = null;
			
			if (orderNrcsField.get("ActionType").equals("DISCONNECT")) {				
				//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedNrcDelete Request ######" , OrderedNrcImpl.class, orderNrcsField);
				if (logger.isDebugEnabled())
					logger.debug("###### OrderedNrcDelete Request ###### " + DebugUtil.getStringBuffer(orderNrcsField).toString());
				orderedNrcModifyAPIResponse = connection.call(context, ApiMappings.getCallName("OrderedNrcDelete"), orderNrcsField);
				orderedNrcModifyAPIResponse.put("SourceLineItemId", orderNrcsField.get("SourceLineItemId"));
				orderNrcElement.put("Nrc", orderedNrcModifyAPIResponse);
			}
			
			HashMap orderedNrcResponse = new HashMap();
			orderNrcElement.put("SourceLineItemId", orderNrcsField.get("SourceLineItemId"));
			orderedNrcResponse.put("OrderedNrc", orderNrcElement);
			
			//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedNrcCreate Response ######" , OrderedNrcImpl.class, orderedNrcResponse);
			if (logger.isDebugEnabled())
				logger.debug("###### OrderedNrcDelete Response ###### " + DebugUtil.getStringBuffer(orderedNrcResponse).toString());
			orderedNrcResponseList.add(orderedNrcResponse);
		}
		
		return ConverterUtil.toHashMapArray(orderedNrcResponseList);
	}
}
