package com.cat.udb.esb.wrappers.common;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cat.udb.esb.core.kenan.utils.ConverterUtil;
//import util.HashMapHelper;
import com.cat.udb.esb.core.utils.DebugUtil;

//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.csgsystems.aruba.connection.Connection;
import com.csgsystems.bali.connection.ApiMappings;

public class OrderedPackageImpl {
	
	private static Logger logger = Logger.getLogger(OrderedPackageImpl.class);

	//public static HashMap[] createOrderedPackage(HashMap[] orderedPackageList, HashMap order, Connection connection, BSDMSessionContext context,String PATH_LOG) throws Throwable{
	public static HashMap[] createOrderedPackage(HashMap[] orderedPackageList, HashMap order, Connection connection, BSDMSessionContext context) throws Throwable{
		ArrayList orderedPackageResponseList = new ArrayList();
		Object orderField = order.get("Order");
		
		for (HashMap productPackage : orderedPackageList) {
			//Init request
			productPackage.put("Order", orderField);
			productPackage.put("FindExistingSO", true);
			productPackage.put("VerboseResponse", false);
			
			//Call API
			//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedPackageCreate Request ######" , OrderedPackageImpl.class, productPackage);
			if (logger.isDebugEnabled())
				logger.debug("###### OrderedPackageCreate Request ###### " + DebugUtil.getStringBuffer(productPackage).toString());
			HashMap orderedPackageCreateResponse = connection.call(context, ApiMappings.getCallName("OrderedPackageCreate"), productPackage);
			
			//Set ServiceLineID & SourceLineItemId
			HashMap productPackageField = (HashMap)productPackage.get("ProductPackage");
			((HashMap)(orderedPackageCreateResponse.get("ProductPackage"))).put("ServiceLineId", productPackageField.get("ServiceLineId"));
			((HashMap)(orderedPackageCreateResponse.get("ProductPackage"))).put("SourceLineItemId", productPackageField.get("SourceLineItemId"));
			
			//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedPackageCreate Response ######" , OrderedPackageImpl.class, orderedPackageCreateResponse);
			if (logger.isDebugEnabled())
				logger.debug("###### OrderedPackageCreate Response ###### " + DebugUtil.getStringBuffer(orderedPackageCreateResponse).toString());
			orderedPackageResponseList.add(orderedPackageCreateResponse);
		}
		return ConverterUtil.toHashMapArray(orderedPackageResponseList); 
	}

	//public static HashMap[] disconnectOrderedPackage(HashMap[] orderedPackageList, HashMap order, 
	//		Connection connection, BSDMSessionContext context,String PATH_LOG) throws Throwable{
	public static HashMap[] disconnectOrderedPackage(HashMap[] orderedPackageList, HashMap order, Connection connection, BSDMSessionContext context) throws Throwable{
		
		ArrayList orderedPackageResponseList = new ArrayList();
		Object orderField = order.get("Order");
		
		for (HashMap productPackage : orderedPackageList) {
			
			//Init request
			productPackage.put("Order", orderField);
			productPackage.put("FindExistingSO", true);
			productPackage.put("VerboseResponse", false);
			
			//Call API
			//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedPackageDisconnect Request ######" , OrderedPackageImpl.class, productPackage);
			if (logger.isDebugEnabled())
				logger.debug("###### OrderedPackageDisconnect Request ###### " + DebugUtil.getStringBuffer(productPackage).toString());
			HashMap orderedPackageDisconnectResponse = connection.call(context, ApiMappings.getCallName("OrderedPackageDisconnect"), productPackage);
			
			//Set ServiceLineID & SourceLineItemId
			HashMap productPackageField = (HashMap)productPackage.get("ProductPackage");
			orderedPackageDisconnectResponse.put("ServiceLineId", productPackageField.get("ServiceLineId"));
			orderedPackageDisconnectResponse.put("SourceLineItemId", productPackageField.get("SourceLineItemId"));
			
			//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedPackageDisconnect Response ######" , OrderedPackageImpl.class, orderedPackageDisconnectResponse);
			if (logger.isDebugEnabled())
				logger.debug("###### OrderedPackageDisconnect Response ###### " + DebugUtil.getStringBuffer(orderedPackageDisconnectResponse).toString());
			orderedPackageResponseList.add(orderedPackageDisconnectResponse);
		}
		return ConverterUtil.toHashMapArray(orderedPackageResponseList); 
	}
}
