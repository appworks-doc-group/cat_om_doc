package com.cat.udb.esb.wrappers;

import java.util.HashMap;

import org.apache.log4j.Logger;

//import util.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.BaseWrapper;

import com.cat.ibacss.esb.bo.convert.PaymentConverter;
import com.cat.ibacss.esb.bo.convert.impl.PaymentConverterImpl;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.bali.connection.ApiMappings;
import commonj.sdo.DataObject;

public class RetrievePaymentWrapper extends BaseWrapper {
	
	private static Logger logger = Logger.getLogger(RetrievePaymentWrapper.class);
//	private Class CLASS = this.getClass();
//	
//	public RetrievePaymentWrapper(){
//		super(Constant.LOG4j_PROPERTIES_PATH_UDB_F14);
//	}

	@SuppressWarnings("unchecked")
	@Override
	protected void executeService() throws Throwable {
		//LoggerUtil.debug(PATH_LOG, "#### Start executeService ####", CLASS);
		logger.debug("#### Start executeService ####");
		
		request = mapToRequestServiceHashMap(requestObj);
		
		HashMap returnPayment = (HashMap)request.get("Payment");
		HashMap requestPaymentHashMap = new HashMap();
		
		returnPayment.put("Key", returnPayment.get("Key"));
		requestPaymentHashMap.put("Payment", (HashMap)returnPayment.clone());
		//LoggerUtil.debugHashMap(PATH_LOG, "PaymentGetRequest", CLASS, requestPaymentHashMap);
		if (logger.isDebugEnabled())
			logger.debug("PaymentGetRequest " + DebugUtil.getStringBuffer(requestPaymentHashMap).toString());
		response = connection.call(context, ApiMappings.getCallName("PaymentGet"), (HashMap)requestPaymentHashMap.clone());
		HashMap paymentResponse = (HashMap)response.get("Payment");
		String action = getAction(paymentResponse);
		paymentResponse.put("Action", action);
		response.put("Payment", paymentResponse);
		//LoggerUtil.debug(PATH_LOG, "#### End executeService ####", CLASS);
		logger.debug("#### End executeService ####");
	}

	@SuppressWarnings("unchecked")
	@Override
	protected DataObject prepareResponseObj(DataObject reqObj, HashMap response, Throwable error) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "#### Start prepareResponseObj ####", CLASS);
		logger.debug("#### Start prepareResponseObj ####");
		
		DataObject responseObj;
		HashMap result = MessageUtil.getResult(error);
		String transactionLogId = MessageUtil.getTransactionLogId(reqObj);
		response.put("Result", result);
		response.put("TransactionLogId", transactionLogId);
		responseObj = mapToResponseServiceObj(response);
		
		//LoggerUtil.debug(PATH_LOG, "#### End prepareResponseObj ####", CLASS);
		logger.debug("#### End prepareResponseObj ####");
		
		return responseObj;
	}
	
	@SuppressWarnings("unchecked")
	public HashMap mapToRequestServiceHashMap(DataObject dataObject) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "#### Start mapToRequestServiceHashMap ####", CLASS);
		logger.debug("#### Start mapToRequestServiceHashMap ####");
		HashMap responseHashMap = null;
		PaymentConverter reConverter = new PaymentConverterImpl();
		responseHashMap = reConverter.dataObject2HashMap(dataObject);
		//LoggerUtil.debugHashMap(PATH_LOG, "RetrievePaymentRequest", CLASS, responseHashMap);
		if (logger.isDebugEnabled())
			logger.debug("RetrievePaymentRequest " + DebugUtil.getStringBuffer(responseHashMap));
		
		//LoggerUtil.debug(PATH_LOG, "#### End mapToRequestServiceHashMap ####", CLASS);
		logger.debug("#### End mapToRequestServiceHashMap ####");
		
		return responseHashMap;
	}
	
	private DataObject mapToResponseServiceObj(HashMap responseHashMap) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "#### Start mapToResponseServiceObj ####", CLASS);
		logger.debug("#### Start mapToResponseServiceObj ####");
		
		//LoggerUtil.debugHashMap(PATH_LOG, "RetrievePaymentResponse", CLASS, responseHashMap);
		if (logger.isDebugEnabled())
			logger.debug("RetrievePaymentResponse " + DebugUtil.getStringBuffer(responseHashMap));
		DataObject responseObj;
		PaymentConverter reConverter = new PaymentConverterImpl();
		responseObj = reConverter.hashMapRetrieve2DataObject(responseHashMap);
		
		//LoggerUtil.debug(PATH_LOG, "#### End mapToResponseServiceObj ####", CLASS);
		logger.debug("#### End mapToResponseServiceObj ####");
		
		return responseObj;
	}
	
	private String getAction(HashMap payment) throws Throwable{
		String action = "NO-ACTION";
		Integer billRefNo = Integer.parseInt(payment.get("BillRefNo").toString());
		String actionCode = payment.get("ActionCode").toString();
		if(billRefNo == 0){
			Integer transCategory = Integer.parseInt(payment.get("TransCategory").toString());
			if(transCategory == 1){
				if(actionCode.trim().equals("DEL")){
					action = "UN-REV-DEL";
				}
				else{
					action = "DEL";
				}
			}
			else if(transCategory == 2){
				action = "UN-REV-DEL";
			}
			else {
			}
		}
		else if(billRefNo != 0){
			if(actionCode.trim().equals("REV")){
				action = "UN-REV-DEL";
			}
			else{
				action = "REV";
			}
		}
		else{
			
		}
		return action;
	}
}
