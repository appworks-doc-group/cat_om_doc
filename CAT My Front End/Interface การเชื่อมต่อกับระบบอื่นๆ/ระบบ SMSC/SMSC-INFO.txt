PROTOCOL:SMPP

OLD HOST
IP:10.99.1.100
PORT:5020

NEW HOST:
IP:10.99.210.48
PORT:5019
==============================================
JSMPP

smppSession.submitShortMessage("CMT", TypeOfNumber.ALPHANUMERIC, NumberingPlanIndicator.UNKNOWN, fromMDN, 
		        		   TypeOfNumber.INTERNATIONAL, NumberingPlanIndicator.UNKNOWN, toMDN, esmClass, (byte)0, (byte)1,  
		        		   null, null, new RegisteredDelivery(SMSCDeliveryReceipt.DEFAULT), (byte)0, 
		        		   new GeneralDataCoding(false, true, MessageClass.CLASS1, Alphabet.ALPHA_UCS2), (byte)0, segmentData[i].getBytes("UTF-16BE"), 
		        		   sarMsgRefNum, sarSegmentSeqnum, sarTotalSegments);
                