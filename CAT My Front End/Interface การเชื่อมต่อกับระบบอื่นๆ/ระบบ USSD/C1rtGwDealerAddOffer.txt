----------------------------
ID: 160
Address: http://test.c1rtgw.cattelecom.com/C1rtGw/api/subscribers/66864926022/offer/dealer/add
Encoding: UTF-8
Http-Method: PUT
Content-Type: application/json
Headers: {Accept=[application/json], Authorization=[Basic d2VwYXktaW50ZXJuZXQ6d2VwYXktcHdk], connection=[close], Content-Length=[120], content-type=[application/json], Host=[test.c1rtgw.cattelecom.com], User-Agent=[Apache-HttpClient/4.5.2 (Java/1.8.0_191)], X-Forwarded-For=[172.16.194.194]}
Payload: {"offerId":"51005617","refTransId":"de54b451-6012-4b8e-b329-ffa7dccd8c4e","clientTransDate":"2018-12-12T00:00:00+07:00"}
--------------------------------------


---------------------------
ID: 160
Response-Code: 400
Content-Type: application/json;charset=utf-8
Headers: {Content-Type=[application/json;charset=utf-8], Date=[Wed, 12 Dec 2018 07:33:02 GMT]}
Payload: {"errorMessage":"Transaction already confirmed","errorCode":"VAL0106","errorDescription":"Transaction already confirmed"}
--------------------------------------
