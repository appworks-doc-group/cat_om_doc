#!/bin/ksh
# Oracle Env.
export ORACLE_HOME=/oracle/product/10.2.0/client_1
export ORACLE_BASE=/oracle
export ORACLE_TERM=xterm
export ORACLE_SID=CRMDEVWP
export TNS_ADMIN=$ORACLE_HOME/network/admin
export NLS_LANG=AMERICAN_AMERICA.UTF8

DIR_APP="/arborbin/FX/mai/MNP_DEALER"

. $DIR_APP/parameter.ctl


date=`date +%Y%m%d`
date_file=`date +%y%m%d%H%M`
mkdir /arborbin/FX/mai/MNP_DEALER/$date/prepaid

export batch_status_tmp=11
export batch_status_complete=4.5
export batch_status_select=4

echo $CONNECTION_DEV

### Spool File ####
$ORACLE_HOME/bin/sqlplus -s $CONNECTION_DB<<EOF
Spool on

-----------------------1+2. Begin :BATCH_CUSTOMER_ACCOUNT+BATCH_BILLING_ACCOUNT----------------------------------------------
set pagesize 0
set trimspool on  
set headsep off 
set feedback off
set echo off 
set verify off
set timing off
set linesize 4000

-- Create an sql file that will create the individual result files
SET DEFINE OFF

update BATCH_CUSTOMER_ACCOUNT
set BATCH_STATUS=${batch_status_tmp},LAST_UPDATE_BY='GENDATA_APP',LAST_UPDATE_DATE=sysdate,BATCH_ERROR_MESSAGE='BATCHID '||'${date_file}'
where   BATCH_STATUS=${batch_status_select} and CAT_SVC_TYPE_LKP =150 ;

commit;
/*
update BATCH_BILLING_ACCOUNT
set BATCH_STATUS=${batch_status_tmp},LAST_UPDATE_BY='GENDATA_APP',LAST_UPDATE_DATE=sysdate
where  CUSTOMER_ACCOUNT_ID in (select b.CUSTOMER_ACCOUNT_ID from BATCH_CUSTOMER_ACCOUNT b where BATCH_STATUS=${batch_status_tmp})
;
commit;
*/
SPOOL /arborbin/FX/mai/MNP_DEALER/$date/prepaid/DEALER_CNO_N_${date_file}_1_CABA.txt

select c.customer_account_id||'|'||c.CUSTOMER_ID||'|'||''||'|'||''||'|'||decode(c.CUSTOMER_TYPE,'individual','3','organization','1',null)||'|'||c.full_name||'|'||c.CAT_THAI_TITLE ||'|'||
c.FIRST_NAME||'|'||c.LAST_NAME||'|'||c.CAT_CARD_NUMBER||'|'||c.CAT_CARD_TYPE||'|'||
'' ||'|'||
 ''||'|'||1||'|'||''||'|'||''||'|'|| ''||'|'||''||'|'||''||'|'||''||'|'||''||'|'||''||'|'||
 ''||'|'||''||'|'||''||'|'||c.CAT_HOUSE_NUMBER||'|'||c.CAT_MOO||'|'||c.CAT_VILLAGE||'|'||c.CAT_MORE_INFO||'|'||c.CAT_TROK_SOI||'|'||
c.CAT_ROAD||'|'||c.CAT_KWANG||'|'||c.CAT_KHET||'|'||c.CAT_PROVINCE||'|'||c.POSTAL_CODE||'|'||c.COUNTRY_CODE_ADDRESS||'|'||to_char(c.CREATE_DATE ,'YYYYMMDD HH24:MI:SS')  ||'|'||''||'|'||
c.CAT_TAX_REGISTER_NUM||'|'||'CRM'
FROM BATCH_CUSTOMER_ACCOUNT C WHERE C.BATCH_STATUS=${batch_status_tmp} and C.CAT_SVC_TYPE_LKP='150'
;

SPOOL OFF


-----------------------END   1+2. BATCH_BILLING_ACCOUNT+BATCH_BILLING_ACCOUNT----------------------------------------------
-----------------------3. Begin :BATCH_SERVICE----------------------------------------------
set pagesize 0
set trimspool on  
set headsep off 
set feedback off
set echo off 
set verify off
set timing off
set linesize 4000


-- Create an sql file that will create the individual result files
SET DEFINE OFF

update BATCH_SERVICE
set BATCH_STATUS=${batch_status_tmp},LAST_UPDATE_BY='GENDATA_APP',LAST_UPDATE_DATE=sysdate
where BATCH_STATUS=${batch_status_select} and CAT_SVC_TYPE_LKP=150
;
commit;

spool  /arborbin/FX/mai/MNP_DEALER/$date/prepaid/DEALER_CNO_N_${date_file}_1_SI.txt

select replace(DATA_DETAIL,  CHR(13) || CHR(10), '') from (
select  SERVICE_ID||'|'||CUSTOMER_ACCOUNT_ID||'|'||		BILLING_ACCOUNT_ID||'|'||		CAT_SVC_TYPE_LKP||'|'||		PROPERTY_ONE||'|'||		
            PROPERTY_TWO||'|'||to_char(START_DATE,'YYYYMMDD HH24:MI:SS')||'|'||		SAP_COST_CENTER||'|'||		DONOR_OPERATOR||'|'||		DEALER_CODE||'|'||		CRM_USER_ID||'|'||		
            decode(CAT_SVC_TYPE_LKP,150,'CRM',110,'CRMBILLING')  DATA_DETAIL
          from BATCH_SERVICE where BATCH_STATUS= ${batch_status_tmp}
 	and CAT_SVC_TYPE_LKP=150
)
;

SPOOL OFF


-----------------------END   3. BATCH_SERVICE----------------------------------------------
-----------------------4. Begin :BATCH_PACKAGE----------------------------------------------
set pagesize 0
set trimspool on  
set headsep off 
set feedback off
set echo off 
set verify off
set timing off
set linesize 4000



-- Create an sql file that will create the individual result files
SET DEFINE OFF

update BATCH_PACKAGE
set BATCH_STATUS=${batch_status_tmp},LAST_UPDATE_BY='GENDATA_APP',LAST_UPDATE_DATE=sysdate
where BATCH_STATUS=${batch_status_select} and SERVICE_ID in (select b.SERVICE_ID from BATCH_SERVICE b where BATCH_STATUS=${batch_status_tmp} and CAT_SVC_TYPE_LKP=150)
;
commit;

spool  /arborbin/FX/mai/MNP_DEALER/$date/prepaid/DEALER_CNO_N_${date_file}_1_PKG.txt
select PACKAGE_ID||'|'||SERVICE_ID||'|'||CAT_PKG_ID||'|'||PACKAGE_NAME||'|'||to_char(START_DATE,'YYYYMMDD HH24:MI:SS')||'|'||CAT_PRODUCT_ID  DATA_DETAIL
          from BATCH_PACKAGE where BATCH_STATUS= ${batch_status_tmp}
 	and SERVICE_ID in (select s.SERVICE_ID from BATCH_SERVICE s where  s.CAT_SVC_TYPE_LKP=150)
;



SPOOL OFF


-----------------------END   4. BATCH_PACKAGE----------------------------------------------
-----------------------5. Begin :BATCH_COMPONENT----------------------------------------------
set pagesize 0
set trimspool on  
set headsep off 
set feedback off
set echo off 
set verify off
set timing off
set linesize 4000



-- Create an sql file that will create the individual result files
SET DEFINE OFF

update BATCH_COMPONENT
set BATCH_STATUS=${batch_status_tmp},LAST_UPDATE_BY='GENDATA_APP',LAST_UPDATE_DATE=sysdate
where BATCH_STATUS=${batch_status_select} and SERVICE_ID in (select b.SERVICE_ID from BATCH_SERVICE b 
where BATCH_STATUS=${batch_status_tmp} and CAT_SVC_TYPE_LKP=150);
commit;

spool  /arborbin/FX/mai/MNP_DEALER/$date/prepaid/DEALER_CNO_N_${date_file}_1_COMP.txt

/*
select COMPONENT_ID||'|'||SERVICE_ID||'|'||PACKAGE_ID||'|'||CAT_COMP_ID||'|'||to_char(START_DATE,'YYYYMMDD HH24:MI:SS')||'|'||CAT_PRODUCT_ID||'|'||COMPONENT_NAME  DATA_DETAIL
          from BATCH_COMPONENT where BATCH_STATUS=${batch_status_tmp}
	and SERVICE_ID in (select s.SERVICE_ID from BATCH_SERVICE s where  s.CAT_SVC_TYPE_LKP=150)
union
 select ${date_file}||'|'||${date_file}||'|'||${date_file}||'|'||${date_file}||'|'||to_char(sysdate,'YYYYMMDD HH24:MI:SS')||'|'||${date_file}||'|'||${date_file}  DATA_DETAIL
	from dual
;
*/
   select distinct ${date_file}||'|'||${date_file}||'|'||${date_file}||'|'||${date_file}||'|'||to_char(sysdate,'YYYYMMDD HH24:MI:SS')||'|'||${date_file}||'|'||${date_file}  DATA_DETAIL
	from BATCH_CUSTOMER_ACCOUNT where  BATCH_STATUS=${batch_status_tmp} and CAT_SVC_TYPE_LKP =150
;

SPOOL OFF


-----------------------END   5. BATCH_COMPONENT----------------------------------------------
-----------------------BATCH_SERVICE_TERMINATE----------------------------------------------
set pagesize 0
set trimspool on  
set headsep off 
set feedback off
set echo off 
set verify off
set timing off
set linesize 4000



-- Create an sql file that will create the individual result files
SET DEFINE OFF
update BATCH_SERVICE_TERMINATE
set BATCH_STATUS=${batch_status_tmp},LAST_UPDATE_BY='GENDATA_APP',LAST_UPDATE_DATE=sysdate
where BATCH_STATUS=${batch_status_select} --and TARGET_SYSTEM='CRMBILLING'
;
commit;

spool  /arborbin/FX/mai/MNP_DEALER/$date/prepaid/MNP_TNO_N_${date_file}_1_NA.txt


select SERVICE_TERMINATE_ID||'|'||REFERENCE_ORDER_ID||'|'||CAT_SVC_TYPE_LKP||'|'||PROPERTY_ONE||'|'||SUBSCR_NO||'|'||to_char(END_DATE,'YYYYMMDD HH24:MI:SS')||'|'||RECIPIENT_OPERATOR  ||'|'||ESB_SVC_ID||'|'||ACCOUNT_INTERNAL_ID||'|'||CAT_BILL_ACCT_NUMBER||'|'||TARGET_SYSTEM  DATA_DETAIL
          from BATCH_SERVICE_TERMINATE where BATCH_STATUS= ${batch_status_tmp} and CAT_SVC_TYPE_LKP=150 
;

SPOOL OFF



-----------------------END BATCH_SERVICE_TERMINATE----------------------------------------------

-----------------------UPDATE BATCH STATUS TEMP----------------------------------------------
--1
update BATCH_CUSTOMER_ACCOUNT
set BATCH_STATUS=${batch_status_complete}
where BATCH_STATUS=${batch_status_tmp};
commit;

--2
update BATCH_BILLING_ACCOUNT
set BATCH_STATUS=${batch_status_complete}
where BATCH_STATUS=${batch_status_tmp};
commit;

--3
update BATCH_SERVICE
set BATCH_STATUS=${batch_status_complete}
where BATCH_STATUS=${batch_status_tmp};
commit;

--4
update BATCH_PACKAGE
set BATCH_STATUS=${batch_status_complete}
where BATCH_STATUS=${batch_status_tmp}
;
commit;

--5
update BATCH_COMPONENT
set BATCH_STATUS=${batch_status_complete}
where BATCH_STATUS=${batch_status_tmp}
;
commit;

--6
update BATCH_SERVICE_TERMINATE
set BATCH_STATUS=${batch_status_complete}
where BATCH_STATUS=${batch_status_tmp};
commit;
-----------------------END UPDATE BATCH STATUS TEMP------------------------------------------
------------------------------------------------------------------------------------------
--------                              END SQLPLUS                               ----------
------------------------------------------------------------------------------------------
exit;
EOF

cd /arborbin/FX/mai/MNP_DEALER/${date}/prepaid
find /arborbin/FX/mai/MNP_DEALER/${date}/prepaid -type f -size 0 -exec rm {} \;

ftp -n 10.32.23.165  << EOF 
user wasusr wasusr  
ascii
lcd /arborbin/FX/mai/MNP_DEALER/$date/prepaid
cd /omapp/batches/loader/files/TMO
mput MNP_TNO_N_${date_file}_*
exit
quit
EOF

ftp -n 10.32.23.165  << EOF 
user wasusr wasusr  
lcd /arborbin/FX/mai/MNP_DEALER/$date/prepaid
cd /omapp/batches/loader/files/CNO
mput DEALER_CNO_N_${date_file}_*
exit
quit
EOF





ftp -n 10.44.48.34  << EOF 
user pragaimas-s pragaimas-s123
ascii
lcd /arborbin/FX/mai/MNP_DEALER/$date/prepaid
mkdir /MNP_DEALER/$date
cd /MNP_DEALER/$date
mput MNP_TNO_N_${date_file}_*
lcd /arborbin/FX/mai/MNP_DEALER/$date/prepaid
cd /MNP_DEALER/$date
mput DEALER_CNO_N_${date_file}_*
exit
quit
EOF

cd /arborbin/FX/mai/MNP_DEALER/$date/prepaid
#rm DEALER_CNO_N_${date_file}_*
#rm MNP_TNO_N_${date_file}_*
#rm run_all_${date_file}_*
#rm generate_one_${date_file}_*

echo "###########FINISH###############"

exit 0



!