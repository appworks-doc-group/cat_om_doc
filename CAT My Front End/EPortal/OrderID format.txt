OrderID = Prefix + Date + Running Number

Prefix = SMD, MFE, USS, MNP, DAP, WOM
Date = YYYYMMDD
Running Number = 0000000000 (9 digits, no reset)