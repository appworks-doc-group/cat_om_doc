#!/bin/ksh

# Oracle Env.
ORACLE_BASE=/oracle; export ORACLE_BASE
ORACLE_HOME=$ORACLE_BASE/product/10.2.0/client_1; export ORACLE_HOME
ORACLE_TERM=xterm; export ORACLE_TERM
PATH=$ORACLE_HOME/bin:$PATH; export PATH
LD_LIBRARY_PATH=$ORACLE_HOME/lib; export LD_LIBRARY_PATH
NLS_LANG=AMERICAN_AMERICA.TH8TISASCII; export NLS_LANG
#========================================================================

cur_date=`date +"%y%m%d_%H%M"`

HOME_PATH=/payment/home/itbnpg05/projects; export HOME_PATH
WORK_PATH=$HOME_PATH/AUTO_RECON; export WORK_PATH
SCRIPT_PATH=$WORK_PATH/Script; export SCRIPT_PATH
DATA_PATH=$WORK_PATH/data; export DATA_PATH
BACKUP_PATH=$WORK_PATH/backup; export BACKUP_PATH
SPOOL_PATH=$WORK_PATH/Spool; export SPOOL_PATH

#================ Gen data ============
sqlplus -s /nolog << !
connect arbor/xxxxxxxx@catpcu1
@$SCRIPT_PATH/cre_reconn_debt.sql 
exit
!

# =============== Spool my ==============
name=RECON_DEBT_MY_WEBBASE_$cur_date.TXT
f_name=$SPOOL_PATH/$name

cd $SCRIPT_PATH

rm -rf $DATA_PATH/*.TXT > /dev/null

sqlplus -s /nolog << !
connect arbor/xxxxxxxx@catpcu1
set verify off

define file_name=$f_name
define file_recon=$name

@$SCRIPT_PATH/spool_reconn_my.sql 

exit
!

cp -pr $f_name $BACKUP_PATH
mv $f_name $DATA_PATH

# =============== Spool NET ==============
DATA_PATH=$HOME_PATH/AUTO_RECON_ALL/data
BACKUP_PATH=$HOME_PATH/AUTO_RECON_ALL/backup

name=RECON_DEBT_NET_WEBBASE_$cur_date.TXT
f_name=$SPOOL_PATH/$name

cd $SCRIPT_PATH

rm -rf $DATA_PATH/*.TXT > /dev/null

sqlplus -s /nolog << !
connect arbor/xxxxxxxx@catpcu1
set verify off

define file_name=$f_name
define file_recon=$name

@$SCRIPT_PATH/spool_reconn_net.sql 

exit
!

cp -pr $f_name $BACKUP_PATH
mv $f_name $DATA_PATH

