select
    equip.EXTERNAL_ID AS MSISDN
    ,bal.BILL_REF_NO AS billInvoice
    ,c.MKT_CODE
    ,bal.BALANCE_DUE
    ,a5.sc_name AS collectionUnit
from
    service sv
    join cmf_balance bal
        on sv.PARENT_ACCOUNT_NO = bal.ACCOUNT_NO
    join cmf c
        on sv.PARENT_ACCOUNT_NO = c.ACCOUNT_NO
    join customer_id_equip_map equip
        on sv.SUBSCR_NO = equip.SUBSCR_NO
        and equip.IS_CURRENT = 1
        and equip.EXTERNAL_ID_TYPE = 17
    join customer_service_centers a4
        on bal.ACCOUNT_NO = a4.ACCOUNT_NO
        and a4.service_center_type = 4
    join service_centers a5
        on a4.service_center_id = a5.service_center_id
WHERE
    equip.EXTERNAL_ID = '<MSISDN>'
    and length(bal.BILL_REF_NO) > 6
    and sv.SERVICE_INACTIVE_DT is null
