select
    ciam.EXTERNAL_ID as billingAccount
    ,equip.EXTERNAL_ID as MSISDN
    ,bal.BILL_REF_NO as billInvoice
    ,c.MKT_CODE
    ,bal.BALANCE_DUE
    ,a5.sc_name as collectionUnit
from
    customer_id_equip_map equip
    join service sv
        on equip.SUBSCR_NO = sv.SUBSCR_NO
        and equip.IS_CURRENT = 1
        and equip.EXTERNAL_ID_TYPE = 17
    join CUSTOMER_ID_ACCT_MAP ciam
        on sv.PARENT_ACCOUNT_NO = ciam.ACCOUNT_NO
        and ciam.EXTERNAL_ID_TYPE = 10005
    join cmf_balance bal
        on sv.PARENT_ACCOUNT_NO = bal.ACCOUNT_NO
    join cmf c
        on sv.PARENT_ACCOUNT_NO = c.ACCOUNT_NO
    join customer_service_centers a4
        on c.ACCOUNT_NO = a4.ACCOUNT_NO
        and a4.service_center_type = 4
    join service_centers a5
        on a4.service_center_id = a5.service_center_id
where
    equip.EXTERNAL_ID = '<MSISDN>'
    and length(bal.BILL_REF_NO) > 6
    and sv.SERVICE_INACTIVE_DT IS null