-- ตรวจสอบว่า ต้องไม่ใช่ bill group 67 (ผู้บริหาร)
--    -> ถ้าเจอให้เอาออก
/*
select
  distinct m.MKT_CODE,
  m.DISPLAY_VALUE,
  m.SHORT_DISPLAY,
  a.CAT_BILL_ACCT_NUMBER,
  CAT_PARAM_ONE,
  CAT_PARAM_TWO
from
  catbilling_prov_cat3g_4billing a,
  cmf c,
  MKT_CODE_VALUES m
where
  batch_id like '20190605%'
  and a.INT_ACCOUNT_NO = c.ACCOUNT_NO
  and c.MKT_CODE = m.MKT_CODE
  and SHORT_DISPLAY = '67';
*/

select
    equip.EXTERNAL_ID
from
  cmf c,
  MKT_CODE_VALUES m,
  service sv,
  customer_id_equip_map equip
where
  c.MKT_CODE = m.MKT_CODE
  and c.ACCOUNT_NO = sv.PARENT_ACCOUNT_NO
  and sv.SUBSCR_NO = equip.SUBSCR_NO
  and equip.IS_CURRENT = 1
  and equip.EXTERNAL_ID_TYPE = 17
  and m.SHORT_DISPLAY = '67';