-- ตรวจสอบก่อนจะส่งไป suspend กรณีมีลูกค้ามาชำระค่าใช้บริการแล้ว
--   -> ถ้าเจอให้เอาออก
/*
update
  catbilling_prov_cat3g_4billing a
set
  prov_status = '4:NOT_PROV',
  NOTE1 = 'pay a debt (Billing)'
where
  substr(batch_id, 1, 8) = to_char($1)
  and prov_status = '3:PROV_COMPLETED'
  and ORDER_NUMBER is null
  and BILLING_STATUS = '0:REQUEST'
  and CRM_STATUS = '0:REQUEST'
  and not exists(
    select
      1
    from
      cmf_balance bal
    where
      bal.BALANCE_DUE > 0
      and length(bal.BILL_REF_NO) > 6
      and to_char(bal.PPDD_DATE, 'YYYYMM') <= to_char(add_months(sysdate, -2), 'YYYYMM')
      and bal.ACCOUNT_NO = a.INT_ACCOUNT_NO
  );
*/

select
    equip.EXTERNAL_ID
from
    cmf_balance bal,
    service sv,
    customer_id_equip_map equip
where
    bal.ACCOUNT_NO = sv.PARENT_ACCOUNT_NO
    and sv.SUBSCR_NO = equip.SUBSCR_NO
    and equip.IS_CURRENT = 1
    and equip.EXTERNAL_ID_TYPE = 17
    and bal.BALANCE_DUE > 0
    and length(bal.BILL_REF_NO) > 6
    and to_char(bal.PPDD_DATE, 'YYYYMM') <= to_char(add_months(sysdate, -2), 'YYYYMM');
