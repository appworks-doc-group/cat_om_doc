dest.setServiceOperation("CBSInterfaceBusinessMgrService","querySubHomeZone")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs12ws.subscriber.querysubhomezone.io.QuerySubHomeZoneRequest"

def destMidVar = destArgs1.subAccessCode

def srcMidVar = srcArgs0.QuerySubHomeZoneRequestMsg.QuerySubHomeZoneRequest

destMidVar.primaryIdentity = srcMidVar.SubscriberNo

def srcMidVar0 = srcArgs0.QuerySubHomeZoneRequestMsg.RequestHeader

destArgs0.beId = srcMidVar0.TenantId

destArgs0.additionInfo = srcMidVar0.additionInfo

destArgs0.belToAreaId = srcMidVar0.BelToAreaID

destArgs0.commandId = srcMidVar0.CommandId

destArgs0.currentCell = srcMidVar0.currentCell

destArgs0.interFrom = srcMidVar0.InterFrom

destArgs0.interMedi = srcMidVar0.InterMedi

destArgs0.interMode = srcMidVar0.InterMode

destArgs0.operatorId = srcMidVar0.OperatorID

destArgs0.partnerId = srcMidVar0.PartnerID

destArgs0.partnerOperId = srcMidVar0.PartnerOperID

destArgs0.remark = srcMidVar0.Remark

destArgs0.requestType = srcMidVar0.RequestType

destArgs0.reserve2 = srcMidVar0.Reserve2

destArgs0.reserve3 = srcMidVar0.Reserve3

destArgs0.sequenceId = srcMidVar0.SequenceId

destArgs0.messageSeq = srcMidVar0.SerialNo

def srcMidVar1 = srcArgs0.QuerySubHomeZoneRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar1.Name

destArgs0.password = srcMidVar1.Password

destArgs0.remoteAddress = srcMidVar1.RemoteAddress

destArgs0.thirdPartyId = srcMidVar0.ThirdPartyID

destArgs0.tradePartnerId = srcMidVar0.TradePartnerID

destArgs0.transactionId = srcMidVar0.TransactionId

destArgs0.version = srcMidVar0.Version

destArgs0.visitArea = srcMidVar0.visitArea
