def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar0 = destReturn.QueryAccountNotifyLevelResultMsg.ResultHeader

def destMidVar1 = destReturn.QueryAccountNotifyLevelResultMsg.QueryAccountNotifyLevelResult

def srcMidVar0 = srcReturn.resultHeader

def srcMidVar1 = srcReturn.resultBody

destMidVar1.SubscriberNo = srcMidVar1.subscriberNo

def destMidVar2 = destMidVar1.AccountLevelInfo

def srcMidVar2 = srcMidVar1.accountLevelInfo

destMidVar2.ObjectType = srcMidVar2.objectType

destMidVar2.AccountObject = srcMidVar2.accountObject

def listMapping0
listMapping0 = 
{
    src,dest  ->

	dest.SequenceNo = src.sequenceNo
	dest.Level = src.levelLeft	
}

mappingList(srcMidVar2.levelList,destMidVar2.LevelList,listMapping0)

destMidVar0.CommandId = srcMidVar0.commandId
destMidVar0.OperationTime = srcMidVar0.operationTime 
destMidVar0.OrderId = srcMidVar0.orderId
destMidVar0.ResultCode = srcMidVar0.resultCode

destMidVar0.ResultDesc = srcMidVar0.resultDesc

destMidVar0.SequenceId = srcMidVar0.sequenceId

destMidVar0.Version = srcMidVar0.version

destMidVar0.TransactionId = srcMidVar0.transactionId




