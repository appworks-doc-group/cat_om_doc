dest.setServiceOperation("CustomerService","modifyConsumptionLimitUsage")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.modifyconsumptionlimitusage.io.ModifyConsumptionLimitUsageRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping1

listMapping1 =
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping2

listMapping2 =
{
    src,dest  ->
	
	listMapping0.call(src.CustAccessCode,dest.custAccessCode)
	
	listMapping1.call(src.SubAccessCode,dest.subAccessCode)
}

listMapping3 =
{
	src,dest  ->

		dest.code = src.Code

		dest.value = src.Value

}

def listMapping4

listMapping4 =
{
    src,dest  ->

	dest.businessCode = src.BusinessCode
	
	def srcMidVar0 = src.OperatorInfo
	
	dest.channelId = srcMidVar0.ChannelID
	
	def srcMidVar1 = src.AccessSecurity
	
	dest.loginSystem = srcMidVar1.LoginSystemCode
	
	dest.messageSeq = src.MessageSeq
	
	dest.msgLanguageCode = src.MsgLanguageCode
	
	dest.operatorId = srcMidVar0.OperatorID
	
	dest.password = srcMidVar1.Password
	
	def destMidVar = dest.simpleProperty[0]
	
	def srcMidVar2 = src.AdditionalProperty[0]
	
	destMidVar.value = srcMidVar2.Value
	
	def srcMidVar3 = src.TimeFormat
	
	dest.timeType = srcMidVar3.TimeType
	
	dest.timeZoneId = srcMidVar3.TimeZoneID
	
	dest.version = src.Version
	
	dest.remoteAddress = srcMidVar1.RemoteIP
	
	mappingList(src.AdditionalProperty,dest.simpleProperty,listMapping3)
	
	def srcMidVar4 = src.OwnershipInfo
	
	dest.beId = srcMidVar4.BEID
	
	dest.brId = srcMidVar4.BRID
	
	dest.interMode = src.AccessMode	
}


def listMapping5

listMapping5 =
{
   src,dest  ->

   dest.limitType = src.LimitType
   
   dest.adjustType = src.AdjustType
   
   dest.adjustValue = src.AdjustValue
     
}

def srcMidVar = srcArgs0.ModifyConsumptionLimitUsageRequestMsg.RequestHeader

listMapping4.call(srcMidVar,destArgs0)

def srcMidVar5 = srcArgs0.ModifyConsumptionLimitUsageRequestMsg.ModifyConsumptionLimitUsageRequest

listMapping2.call(srcMidVar5.LimitObj,destArgs1.anyAccessCode);

mappingList(srcMidVar5.ConsumptionLimitChangeInfo,destArgs1.consumptionLimitChangeRequestInfos,listMapping5)



