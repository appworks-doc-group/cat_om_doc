import com.huawei.ngcbs.bm.common.common.Constant4Model
dest.setServiceOperation("SubscriberService","changeSubIdentity")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.changesubiden.io.ChangeSubIdentityRequest"

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.businessCode = src.BusinessCode
	
	def srcMidVar0 = src.OperatorInfo
	
	dest.channelId = srcMidVar0.ChannelID
	
	def srcMidVar1 = src.AccessSecurity
	
	dest.loginSystem = srcMidVar1.LoginSystemCode
	
	dest.messageSeq = src.MessageSeq
	
	dest.msgLanguageCode = src.MsgLanguageCode
	
	dest.operatorId = srcMidVar0.OperatorID
	
	dest.password = srcMidVar1.Password
	
	def srcMidVar2 = src.TimeFormat
	
	dest.timeType = srcMidVar2.TimeType
	
	dest.timeZoneId = srcMidVar2.TimeZoneID
	
	dest.version = src.Version
	
	dest.remoteAddress = srcMidVar1.RemoteIP
	
	mappingList(src.AdditionalProperty,dest.simpleProperty,listMapping1)
	
	def srcMidVar3 = src.OwnershipInfo
	
	dest.beId = srcMidVar3.BEID
	
	dest.brId = srcMidVar3.BRID
	
	dest.interMode = src.AccessMode
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.subIden = src.SubIdentity
	
	dest.subIdenType = src.SubIdentityType
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.subIden = src.SubIdentity
	
	dest.subIdenType = src.SubIdentityType
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.newSubIden = src.NewSubIdentity
	
	dest.oldSubIden = src.OldSubIdentity
	
	dest.oldSubIdenType = src.OldSubIdentityType
	
}
def listMapping8

listMapping8 = 
{
    src,dest  ->
	dest._class="com.huawei.ngcbs.cm.subscriber.createsubscriber.io.SubIdenInfoExt"
	dest.subIden = src.SubIdentity
	
	dest.subIdenType = src.SubIdentityType
	
	dest.relatedSubIdentity = src.RelatedSubIdentity
}
def listMapping2

listMapping2 = 
{
    src,dest  ->

	listMapping3.call(src.SubAccessCode,dest.subAccessCode)
	
	def destMidVar = dest.addSubIdenInfo
	
	mappingList(src.AddSubIdentity,destMidVar.addSubIdens,listMapping8)
	
	def destMidVar0 = dest.delSubIdenInfo
	
	mappingList(src.DelSubIdentity,destMidVar0.delSubIdens,listMapping5)
	
	def destMidVar1 = dest.modifySubIdenInfo
	
	mappingList(src.ModifySubIdentity,destMidVar1.modifySubIdens,listMapping6)
	
	def srcMidVar4 = src.EffectiveTime

  dest.effMode = srcMidVar4.Mode
	
	dest.effDate=parseDate(srcMidVar4.Time,Constant4Model.DATE_FORMAT)
	
	dest.expDate=parseDate(src.ExpirationTime,Constant4Model.DATE_FORMAT)
	
}

def srcMidVar = srcArgs0.ChangeSubIdentityRequestMsg

listMapping0.call(srcMidVar.RequestHeader,destArgs0)

listMapping2.call(srcMidVar.ChangeSubIdentityRequest,destArgs1)

def srcMidVar7 = srcMidVar.ChangeSubIdentityRequest

destArgs1.handlingChargeFlag = srcMidVar7.HandlingChargeFlag

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

mappingList(srcMidVar7.AdditionalProperty,destArgs1.simplePropertyList,listMapping7)