import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReqDocMsg = src.payload._args[0]
def destMsgHeader = dest.payload._args[0]
def destReqData = dest.payload._args[1]
def srcReqHeaderDoc = srcReqDocMsg.ChangeAcctBillCycleRequestMsg.RequestHeader

destMsgHeader._class = "com.huawei.ngcbs.cm.ocs33ws.core.bo.Ocs33MessageHeader"
destReqData._class = "com.huawei.ngcbs.cm.ocs33ws.subscriber.submgrt.io.OCS33ChangeAcctBillCycleRequest"

destMsgHeader.operatorId = srcReqHeaderDoc.OperatorID
destMsgHeader.additionInfo = srcReqHeaderDoc.additionInfo
destMsgHeader.commandId = srcReqHeaderDoc.CommandId
destMsgHeader.transactionId = srcReqHeaderDoc.TransactionId
destMsgHeader.interMedi = srcReqHeaderDoc.InterMedi
destMsgHeader.reserve2 = srcReqHeaderDoc.Reserve2
destMsgHeader.thirdPartyId = srcReqHeaderDoc.ThirdPartyID
destMsgHeader.reserve3 = srcReqHeaderDoc.Reserve3
destMsgHeader.sequenceId = srcReqHeaderDoc.SequenceId
destMsgHeader.visitArea = srcReqHeaderDoc.visitArea
destMsgHeader.belToAreaId = srcReqHeaderDoc.BelToAreaID
destMsgHeader.currentCell = srcReqHeaderDoc.currentCell
destMsgHeader.partnerId = srcReqHeaderDoc.PartnerID
destMsgHeader.tradePartnerId = srcReqHeaderDoc.TradePartnerID
destMsgHeader.partnerOperId = srcReqHeaderDoc.PartnerOperID
destMsgHeader.version = srcReqHeaderDoc.Version
destMsgHeader.remark = srcReqHeaderDoc.Remark
destMsgHeader.loginSystem = srcReqHeaderDoc.SessionEntity.Name
destMsgHeader.password = srcReqHeaderDoc.SessionEntity.Password
destMsgHeader.remoteAddress = srcReqHeaderDoc.SessionEntity.RemoteAddress
destMsgHeader.interFrom = srcReqHeaderDoc.InterFrom
destMsgHeader.requestType = srcReqHeaderDoc.RequestType
destMsgHeader.messageSeq = srcReqHeaderDoc.SerialNo
destMsgHeader.performanceStatCmd="ChangeAcctBillCycle"


def srcReqDataDoc = srcReqDocMsg.ChangeAcctBillCycleRequestMsg.ChangeAcctBillCycleRequest
destReqData.subscriberNo = srcReqDataDoc.SubscriberNo
destReqData.accountCode = srcReqDataDoc.AccountCode
destReqData.newBillCycleType = srcReqDataDoc.NewBillCycleType
destReqData.validMode = srcReqDataDoc.ValidMode
