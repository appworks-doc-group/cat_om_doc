def srcReturn = src.payload._return   

def destReturn = dest.payload._return    

def srcResultHeader = srcReturn.resultHeader

def srcResultBody = srcReturn.resultBody

def destMidVar = destReturn.IntegrationQueryResponse.IntegrationQueryResult.ResultMessage.MessageHeader

def destMessageBody = destReturn.IntegrationQueryResponse.IntegrationQueryResult.ResultMessage.MessageBody

destMidVar.CommandId = srcResultHeader.commandId
destMidVar.Version = srcResultHeader.version
destMidVar.TransactionId = srcResultHeader.transactionId
destMidVar.SequenceId = srcResultHeader.sequenceId
destMidVar.ResultCode = srcResultHeader.resultCode
destMidVar.ResultDesc = srcResultHeader.resultDesc
destMidVar.TenantId = srcResultHeader.tenantId
destMidVar.Language = srcResultHeader.language

def srcMidVar2 = srcResultBody.subscirberState

destMessageBody.ValidStopDate = formatDate(srcMidVar2.activeStop, "yyyyMMddHHmmss")
destMessageBody.SuspendStopDate = formatDate(srcMidVar2.suspendStop, "yyyyMMddHHmmss")
destMessageBody.DisableStopDate = formatDate(srcMidVar2.disableStop, "yyyyMMddHHmmss")
destMessageBody.Status = srcMidVar2.strLifeCycleState
destMessageBody.FirstActDate = formatDate(srcMidVar2.firstActiveDate, "yyyyMMddHHmmss")

def listMapping0

listMapping0 = 
{
    src,dest  ->

    dest.BalanceDescLang1 = src.balanceDesc

    dest.Fee.Value = src.balance
	
	dest.Fee.MinMeasureId = src.minMeasureId
	
    dest.UnitType = src.unitType

    dest.AccountKey = src.accountKey
	
    dest.AccountType = src.accountType

}

mappingList(srcResultBody.balanceRecordList,destMessageBody.BalanceRecordList.Balance,listMapping0)

destMessageBody.PreviousStatus = srcMidVar2.previousStatus
destMessageBody.ChangeDate = formatDate(srcMidVar2.changeDate, "yyyyMMdd")

destMessageBody.HomeZoneNo = srcResultBody.homeZoneNo
destMessageBody.PromoBalance = srcResultBody.promoBalance
destMessageBody.ManageStatus = srcResultBody.manageStatus
destMessageBody.RealMainBalance = srcResultBody.realMainBalance
destMessageBody.RechargeTime = srcResultBody.rechargeTime
destMessageBody.RechargeAmount = srcResultBody.rechargeAmount

def srcMidVar1 = srcResultBody.subscirberInfo.subscriberBasic

def listMapping2

listMapping2 = 
{
    src,dest  ->

    dest.Id = src.code

    dest.Value = src.value

}

mappingList(srcMidVar1.simplePropertyList,destMessageBody.SubscriberInfo.Subscriber.SimpleProperty,listMapping2)

def listMapping1

listMapping1 = 
{
    src,dest  ->

    dest.Id = src.id

    dest.ProductOrderKey = src.productOrderKey
	
	dest.CurCycleStartTime = formatDate(src.curCycleStartTime, "yyyyMMddHHmmss")

    dest.CurCycleEndTime = formatDate(src.curCycleEndTime, "yyyyMMddHHmmss")
	
	dest.BillStatus = src.billStatus
	
	mappingList(src.simplePropertyList,dest.SimpleProperty,listMapping2)
	
}

def srcMidVar0 = srcResultBody.subscirberInfo
mappingList(srcMidVar0.productList,destMessageBody.SubscriberInfo.Product,listMapping1)

def listMapping3

listMapping3 = 
{
    src,dest  ->

    dest.CumulateID = src.cumulateId

    dest.AccumulateName = src.accumulateName

    dest.CumulateBeginTimes = formatDate(src.cumulateBeginTime, "yyyyMMddHHmmss")
     
    dest.CumulateEndTime = formatDate(src.cumulateEndTime, "yyyyMMddHHmmss")

    dest.CumulativeAmt = src.cumulativeAmt

}

mappingList(srcResultBody.cumulativeItemList,destMessageBody.CumulativeItemList.CumulativeItem,listMapping3)
