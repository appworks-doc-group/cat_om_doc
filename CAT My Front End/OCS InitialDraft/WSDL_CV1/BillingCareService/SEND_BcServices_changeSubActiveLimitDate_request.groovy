import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("SubscriberService","changeSubActiveLimitDate")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.changesubactivelimitdate.io.ChangeSubActiveLimitDateRequest"

def listMapping1

listMapping1 =
		{
			src,dest  ->

				dest.code = src.Code

				dest.value = src.Value

		}

def listMapping0

listMapping0 =
		{
			src,dest  ->

				dest.businessCode = src.BusinessCode

				def srcMidVar0 = src.OperatorInfo

				dest.channelId = srcMidVar0.ChannelID

				def srcMidVar1 = src.AccessSecurity

				dest.loginSystem = srcMidVar1.LoginSystemCode

				dest.messageSeq = src.MessageSeq

				dest.msgLanguageCode = src.MsgLanguageCode

				dest.operatorId = srcMidVar0.OperatorID

				dest.password = srcMidVar1.Password

				def srcMidVar2 = src.TimeFormat

				dest.timeType = srcMidVar2.TimeType

				dest.timeZoneId = srcMidVar2.TimeZoneID

				dest.version = src.Version

				dest.remoteAddress = srcMidVar1.RemoteIP

				mappingList(src.AdditionalProperty,dest.simpleProperty,listMapping1)

				def srcMidVar3 = src.OwnershipInfo

				dest.beId = srcMidVar3.BEID

				dest.brId = srcMidVar3.BRID

				dest.interMode = src.AccessMode

		}

def listMapping3

listMapping3 =
		{
			src,dest  ->

				dest.primaryIdentity = src.PrimaryIdentity

				dest.subscriberKey = src.SubscriberKey

		}



def listMapping2

listMapping2 =
		{
			src,dest  ->

				listMapping3.call(src.SubAccessCode,dest.subAccessCode)

				dest.newActiveLimitDate = parseDate(src.NewActiveLimitDate,Constant4Model.DATE_FORMAT)

		}

def srcMidVar = srcArgs0.ChangeSubActiveLimitDateRequestMsg

listMapping0.call(srcMidVar.RequestHeader,destArgs0)

listMapping2.call(srcMidVar.ChangeSubActiveLimitDateRequest,destArgs1)
