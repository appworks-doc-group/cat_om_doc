import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.TaxCode = src.taxCode
	
	dest.TaxAmount = src.taxAmt
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->
    
  dest.EffectiveTime=formatDate(src.effectiveTime,Constant4Model.DATE_FORMAT)
	
	dest.ExpirationTime=formatDate(src.expirationTime,Constant4Model.DATE_FORMAT)
	
	def destMidVar1 = dest.OfferingKey
	
	def srcMidVar1 = src.offeringKey
  srcMidVar1._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
    
	destMidVar1.OfferingID = srcMidVar1.oId
	
  destMidVar1.OfferingCode = srcMidVar1.oCode
  
	destMidVar1.PurchaseSeq = srcMidVar1.pSeq
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.ChargeAmt = src.chargeAmt
	
	dest.ChargeCode = src.chargeCode
	
	mappingList(src.taxInfo4WSList,dest.Tax,listMapping2)
	
	listMapping3.call(src.penaltyOfferingInst,dest.PenaltyOfferingInst)
	
}

def destMidVar = destReturn.PenaltyCalculationResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.Version = srcMidVar.version

destMidVar.MessageSeq = srcMidVar.messageSeq

mappingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def srcMidVar0 = srcReturn.penaltyCalculationResultInfo

def destMidVar0 = destReturn.PenaltyCalculationResultMsg.PenaltyCalculationResult

mappingList(srcMidVar0.penaltyChargeList,destMidVar0.PenaltyChargeList,listMapping1)
