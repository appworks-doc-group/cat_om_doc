dest.setServiceOperation("WorkOrder4BaasService","workOrder4Baas")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Name = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destArgs0.WorkOrder4BaasRequest.RequestHeader

def srcMidVar = srcArgs0.workOrderHeader

destMidVar.Version = srcMidVar.version

destMidVar.TransactionId = srcMidVar.transactionId

destMidVar.SessionId = srcMidVar.sessionId

destMidVar.ProcessTime = srcMidVar.processTime

destMidVar.Language = srcMidVar.language

destMidVar.ChannelId = srcMidVar.channelId

destMidVar.TechnicalChannelId = srcMidVar.technicalChannelId

destMidVar.TenantId = srcMidVar.beId

destMidVar.AccessUser = srcMidVar.accessUser

destMidVar.AccessPwd = srcMidVar.accessPwd

destMidVar.OperatorId = srcMidVar.operatorId

destMidVar.OperatorPwd = srcMidVar.operatorPwd

def destMidVar0 = destArgs0.WorkOrder4BaasRequest.WorkOrder4BaaSIn

destMidVar0.SubscriberID = srcArgs0.subscriberID

destMidVar0.OrderType = srcArgs0.orderType

def destMidVar1 = destArgs0.WorkOrder4BaasRequest.WorkOrder4BaaSIn.ExtParamList
mappingList(srcArgs0.simplePropList,destMidVar1.ParameterInfo,listMapping0)