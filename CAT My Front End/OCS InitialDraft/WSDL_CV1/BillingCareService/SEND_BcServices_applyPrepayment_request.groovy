dest.setServiceOperation("AccountService","applyPrepayment")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.account.applyprepayment.io.ApplyPrepaymentRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.ApplyPrepaymentRequestMsg.ApplyPrepaymentRequest

mappingList(srcMidVar.AdditionalProperty,destArgs1.simplePropertyList,listMapping0)

destArgs1.amount = srcMidVar.Amount

def srcMidVar0 = srcArgs0.ApplyPrepaymentRequestMsg.ApplyPrepaymentRequest.ApplyObj

def destMidVar = destArgs1.anyAccessCode

listMapping1.call(srcMidVar0.CustAccessCode,destMidVar.custAccessCode)

listMapping2.call(srcMidVar0.SubAccessCode,destMidVar.subAccessCode)

listMapping3.call(srcMidVar0.SubGroupAccessCode,destMidVar.groupAccessCode)

destArgs1.extTransID = srcMidVar.ExtTransID

destArgs1.contractID = srcMidVar.ContractID

destArgs1.currencyID = srcMidVar.CurrencyID

def destMidVar0 = destArgs1.offeringKey

destMidVar0._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"

def srcMidVar1 = srcArgs0.ApplyPrepaymentRequestMsg.ApplyPrepaymentRequest.OfferingKey

destMidVar0.oId = srcMidVar1.OfferingID

destMidVar0.oCode = srcMidVar1.OfferingCode

destMidVar0.pSeq = srcMidVar1.PurchaseSeq

destArgs1.repayMode = srcMidVar.RepayMode

def srcMidVar2 = srcArgs0.ApplyPrepaymentRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar2.AccessMode

def srcMidVar3 = srcArgs0.ApplyPrepaymentRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar3.LoginSystemCode

destArgs0.password = srcMidVar3.Password

destArgs0.remoteAddress = srcMidVar3.RemoteIP

mappingList(srcMidVar2.AdditionalProperty,destArgs0.simpleProperty,listMapping4)

destArgs0.businessCode = srcMidVar2.BusinessCode

destArgs0.messageSeq = srcMidVar2.MessageSeq

destArgs0.msgLanguageCode = srcMidVar2.MsgLanguageCode

def srcMidVar4 = srcArgs0.ApplyPrepaymentRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar4.ChannelID

destArgs0.operatorId = srcMidVar4.OperatorID

def srcMidVar5 = srcArgs0.ApplyPrepaymentRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar5.BEID

destArgs0.brId = srcMidVar5.BRID

def srcMidVar6 = srcArgs0.ApplyPrepaymentRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar6.TimeType

destArgs0.timeZoneId = srcMidVar6.TimeZoneID

destArgs0.version = srcMidVar2.Version
