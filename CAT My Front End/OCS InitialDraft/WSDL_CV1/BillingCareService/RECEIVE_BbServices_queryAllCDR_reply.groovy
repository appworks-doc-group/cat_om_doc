import com.huawei.ngcbs.bm.common.common.Constant4Model
def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.TaxCode = src.taxCode
	
	dest.TaxAmt = src.taxAmt
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.ChargeAmount = src.chargeAmount
	
	dest.ChargeCode = src.chargeCodeId
	
	dest.ChargeCodeName = src.chargeCodeName
	
	dest.CurrencyID = src.currencyId
	
	dest.OfferingID = src.offeringId
	
	dest.PayAcctKey = src.payAcctKey
	
	dest.PayObjClass = src.payObjClass
	
	dest.PayObjID = src.payObjId
	
	dest.PayObjType = src.payObjType
	
	dest.PlanID = src.planId
	
	mappingList(src.tax,dest.Tax,listMapping4)
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.TaxAmt = src.taxAmt
	
	dest.TaxCode = src.taxCode
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.TaxAmt = src.taxAmt
	
	dest.TaxCode = src.taxCode
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.ActualChargeAmt = src.actualChargeAmt
	
	mappingList(src.actualTaxList,dest.ActualTax,listMapping6)
	
	dest.CurrencyID = src.currencyId
	
	dest.FreeChargeAmt = src.freeChargeAmt
	
	mappingList(src.freeTaxList,dest.FreeTax,listMapping7)
	
}

def listMapping9

listMapping9 = 
{
    src,dest  ->

	dest.Amount = src.amount
	
	dest.FreeUnitType = src.freeUnitType
	
}

def listMapping11

listMapping11 = 
{
    src,dest  ->

	dest.OfferingID = src.offerId
	
	dest.PurchaseSeq = src.purchaseSeq
	
}

def listMapping10

listMapping10 = 
{
    src,dest  ->

	dest.FreeVolume = src.freeVolume
	
	listMapping11.call(src.offeringKey,dest.Offering)
	
}

def listMapping8

listMapping8 = 
{
    src,dest  ->

	dest.ActualVolume = src.actualVolume
	
	mappingList(src.freeUnits,dest.FreeUnitList,listMapping9)
	
	dest.FreeVolume = src.freeVolume
	
	mappingList(src.freeVolumeList,dest.FreeVolumeList,listMapping10)
	
	dest.MeasureUnit = src.measureUnit
	
	dest.RatingVolume = src.ratingVolume
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.AccountKey = src.acctKey
	
	mappingList(src.additionalProperty,dest.AdditionalProperty,listMapping2)
	
	dest.BillCycleID = src.billCycleId
	
	dest.CdrSeq = src.cdrSeq
	
	mappingList(src.chargeDetailList,dest.ChargeDetail,listMapping3)
	
	dest.EndTime=formatDate(src.endTime, Constant4Model.DATE_FORMAT)
	
	dest.FlowType = src.flowType
	
	dest.OtherNumber = src.otherNumber
	
	dest.PrimaryIdentity = src.subIdentity
	
	dest.SeriveType = src.seriveType
	
	dest.ServiceCategory = src.serviceCategory
	
	dest.ServiceTypeName = src.serviceTypeName
	
	dest.StartTime=formatDate(src.startTime, Constant4Model.DATE_FORMAT)
	
	dest.SubKey = src.subKey
	
	listMapping5.call(src.totalChargeInfo,dest.TotalChargeInfo)
	
	listMapping8.call(src.volumeInfo,dest.VolumeInfo)
	
	dest.PreBalance = src.preBalance
	
	dest.changeamount = src.changeamount
	
}

def listMapping13

listMapping13 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping15

listMapping15 = 
{
    src,dest  ->

	dest.TaxAmt = src.taxAmt
	
	dest.TaxCode = src.taxCode
	
}

def listMapping16

listMapping16 = 
{
    src,dest  ->

	dest.TaxAmt = src.taxAmt
	
	dest.TaxCode = src.taxCode
	
}

def listMapping14

listMapping14 = 
{
    src,dest  ->

	dest.ActualChargeAmt = src.actualChargeAmt
	
	mappingList(src.actualTaxList,dest.ActualTax,listMapping15)
	
	dest.CurrencyID = src.currencyId
	
	dest.FreeChargeAmt = src.freeChargeAmt
	
	mappingList(src.freeTaxList,dest.FreeTax,listMapping16)
	
}

def listMapping18

listMapping18 = 
{
    src,dest  ->

	dest.Amount = src.amount
	
	dest.FreeUnitType = src.freeUnitType
	
}

def listMapping17

listMapping17 = 
{
    src,dest  ->

	dest.ActualVolume = src.actualVolume
	
	mappingList(src.freeUnits,dest.FreeUnitList,listMapping18)
	
	dest.FreeVolume = src.freeVolume
	
	dest.MeasureUnit = src.measureUnit
	
	dest.RatingVolume = src.ratingVolume
	
}

def listMapping12

listMapping12 = 
{
    src,dest  ->

	mappingList(src.cdrSummaryDataInfo.additionalProperty,dest.AdditionalProperty,listMapping13)
	
	dest.BillCycleID = src.cdrSummaryQueryInfo.billCycleId
	
	dest.FlowType = src.cdrSummaryQueryInfo.flowType
	
	dest.RoamFlag = src.cdrSummaryQueryInfo.roamFlag
	
	dest.SeriveType = src.cdrSummaryQueryInfo.seriveType
	
	dest.ServiceCategory = src.cdrSummaryQueryInfo.serviceCategory
	
	dest.ServiceTypeName = src.cdrSummaryQueryInfo.serviceTypeName
	
	mappingList(src.cdrSummaryDataInfo.totalChargeInfos,dest.TotalChargeInfo,listMapping14)
	
	mappingList(src.cdrSummaryDataInfo.volumeInfos,dest.VolumeInfo,listMapping17)
	
}

def destMidVar = destReturn.QueryAllCDRResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.Version = srcMidVar.version

destMidVar.MessageSeq = srcMidVar.messageSeq

mappingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def destMidVar0 = destReturn.QueryAllCDRResultMsg.QueryAllCDRResult

def srcMidVar0 = srcReturn.queryCDRResultInfo

destMidVar0.BeginRowNum = srcMidVar0.beginRowNum

destMidVar0.FetchRowNum = srcMidVar0.fetchRowNum

mappingList(srcMidVar0.CDRInfoList,destMidVar0.CDRInfo,listMapping1)

mappingList(srcMidVar0.summaryCDRInfos,destMidVar0.CDRSummary,listMapping12)

destMidVar0.TotalCDRNum = srcMidVar0.totalCDRNum
