import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destReturn.QueryCallHuntingResponseMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.Version = srcMidVar.version

destMidVar.MessageSeq = srcMidVar.messageSeq

mappingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def destMidVar0 = destReturn.QueryCallHuntingResponseMsg.QueryCallHuntingResponse

destMidVar0.EffectiveTime = formatDate(srcReturn.effectiveTime, Constant4Model.DATE_FORMAT)

destMidVar0.ExpireTime = formatDate(srcReturn.expireTime, Constant4Model.DATE_FORMAT)

def destMidVar1 = destReturn.QueryCallHuntingResponseMsg.QueryCallHuntingResponse.SubGroupAccessCode

def srcMidVar0 = srcReturn.groupAccessCode

destMidVar1.SubGroupCode = srcMidVar0.groupCode

destMidVar1.SubGroupKey = srcMidVar0.groupKey

destMidVar0.HuntingMainNumber = srcReturn.huntingMainNumber

destMidVar0.HuntingNumber = srcReturn.huntingNumber

destMidVar0.Priority = srcReturn.priority

destMidVar0.UserState = srcReturn.userState
