def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcReturn.CheckCashRegisterResultMsg.CheckCashRegisterResult

destReturn.batchNo = srcMidVar.BatchNo

destReturn.currencyId = srcMidVar.CurrencyID

destReturn.operId = srcMidVar.OperID

def srcMidVar0 = srcReturn.CheckCashRegisterResultMsg.ResultHeader

def destMidVar = destReturn.resultHeader

mappingList(srcMidVar0.AdditionalProperty,destMidVar.simpleProperty,listMapping0)

destMidVar.version = srcMidVar0.Version

destMidVar.msgLanguageCode = srcMidVar0.MsgLanguageCode

destMidVar.resultCode = srcMidVar0.ResultCode

destMidVar.resultDesc = srcMidVar0.ResultDesc

destReturn._class = "com.huawei.ngcbs.cm.common.ws.cashregister.query.io.CheckCashRegisterResult"
