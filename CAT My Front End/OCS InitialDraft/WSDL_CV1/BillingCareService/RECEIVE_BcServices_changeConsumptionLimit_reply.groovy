import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.ChangeConsumptionLimitResultMsg

def destMidVarResultHeader = destMidVar.ResultHeader

destMidVarResultHeader.MsgLanguageCode = srcReturn.resultHeader.msgLanguageCode

destMidVarResultHeader.ResultCode = srcReturn.resultHeader.resultCode

destMidVarResultHeader.ResultDesc = srcReturn.resultHeader.resultDesc

destMidVarResultHeader.Version = srcReturn.resultHeader.version

destMidVarResultHeader.MessageSeq = srcReturn.resultHeader.messageSeq

def destMidVar0 = destReturn.ChangeConsumptionLimitResultMsg.ResultHeader.AdditionalProperty[0]

def srcMidVar = srcReturn.resultHeader.simpleProperty[0]

destMidVar0.Code = srcMidVar.code

destMidVar0.Value = srcMidVar.value


def destMidVar1 = destMidVar.ChangeConsumptionLimitResult

def srcMidVar1 = srcReturn.changeConsumptionLimitResultInfo

def listMapping0

listMapping0 =
{
    src,dest  ->

        dest.LimitType = src.limitType

        dest.EffectiveTime = formatDate(src.effDate, Constant4Model.DATE_FORMAT)

        dest.ExpirationTime = formatDate(src.expDate, Constant4Model.DATE_FORMAT)

}

def listMapping1

listMapping1 =
{
    src,dest  ->

        dest.LimitType = src.limitType

        dest.EffectiveTime = formatDate(src.effDate, Constant4Model.DATE_FORMAT)

        dest.ExpirationTime = formatDate(src.expDate, Constant4Model.DATE_FORMAT)

}

mappingList(srcMidVar1.addConsumptionLimitLimitResultInfoList,destMidVar1.AddLimit,listMapping0)

mappingList(srcMidVar1.delConsumptionLimitLimitResultInfoList,destMidVar1.DelLimit,listMapping1)

