import com.huawei.ngcbs.bm.common.common.Constant4Model;

dest.setServiceOperation("CBSInterfaceBusinessMgrService","changeMainProd");

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs12ws.product.changemainprod.io.ChangeMainProdRequestOcs12Base"

def listMapping1

listMapping1 = 
{
    src,dest  ->

	def destMidVar5 = dest.property
	
	destMidVar5.propCode = src.Id
	
	destMidVar5.value = src.Value
	
}

def listMapping0

listMapping0 = 
{
   src,dest  ->

	def destMidVar4 = dest.offeringInst.offeringKey
	destMidVar4._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
	
	destMidVar4.oCode = src.Id
	
	mappingList(src.SimpleProperty,dest.properties,listMapping1)
	
}

def listMapping2

listMapping2 = 
{
  src,dest  ->
  dest._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
	dest.oCode = src.Id
	
	dest.pSeq = src.ProductOrderKey
	
}

def srcMidVar = srcArgs0.ChangeMainProdRequestMsg.RequestHeader

destArgs0.beId = srcMidVar.TenantId

destArgs0.additionInfo = srcMidVar.additionInfo

destArgs0.belToAreaId = srcMidVar.BelToAreaID

destArgs0.commandId = srcMidVar.CommandId

destArgs0.currentCell = srcMidVar.currentCell

destArgs0.interFrom = srcMidVar.InterFrom

destArgs0.interMedi = srcMidVar.InterMedi

destArgs0.interMode = srcMidVar.InterMode

destArgs0.operatorId = srcMidVar.OperatorID

destArgs0.partnerId = srcMidVar.PartnerID

destArgs0.partnerOperId = srcMidVar.PartnerOperID

destArgs0.remark = srcMidVar.Remark

destArgs0.requestType = srcMidVar.RequestType

destArgs0.reserve2 = srcMidVar.Reserve2

destArgs0.reserve3 = srcMidVar.Reserve3

destArgs0.sequenceId = srcMidVar.SequenceId

destArgs0.messageSeq = srcMidVar.SerialNo

def srcMidVar0 = srcArgs0.ChangeMainProdRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar0.Name

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteAddress

destArgs0.thirdPartyId = srcMidVar.ThirdPartyID

destArgs0.tradePartnerId = srcMidVar.TradePartnerID

destArgs0.transactionId = srcMidVar.TransactionId

destArgs0.version = srcMidVar.Version

destArgs0.visitArea = srcMidVar.visitArea

def srcMidVar1 = srcArgs0.ChangeMainProdRequestMsg.ChangeMainProdRequest

destArgs1.bcChangeMode = srcMidVar1.BCChangeMode

destArgs1.custID = srcMidVar1.CustID

destArgs1.handlingChargeFlag = srcMidVar1.HandlingChargeFlag

destArgs1.imsi = srcMidVar1.IMSI

destArgs1.localID = srcMidVar1.LocalID

destArgs1.newAcctID = srcMidVar1.newAcctID

destArgs1.newBillcycleType = srcMidVar1.newBillcycleType

destArgs1.newCustID = srcMidVar1.newCustID

destArgs1.newSubID = srcMidVar1.newSubID

destArgs1.posAcctCredit = srcMidVar1.posAcctCredit

destArgs1.posAcctInitBal = srcMidVar1.posAcctInitBal

destArgs1.ppsAcctCredit = srcMidVar1.ppsAcctCredit

destArgs1.ppsAcctInitBal = srcMidVar1.ppsAcctInitBal

def destMidVar = destArgs1.changeSubOfferingInfo.subBrand

destMidVar.brand = srcMidVar1.NewBrandId

def destMidVar0 = destArgs1.subAccessCode

destMidVar0.primaryIdentity = srcMidVar1.SubscriberNo

destArgs1.changeSubOfferingInfo.newPrimaryOffering.effDate=parseDate(srcMidVar1.EffectiveDate,Constant4Model.DAY_FORMAT)

def destMidVar1 = destArgs1.changeSubOfferingInfo.newPrimaryOffering

destMidVar1.effMode = srcMidVar1.ValidMode

def destMidVar2 = destArgs1.changeSubOfferingInfo.newPrimaryOffering.offeringInst.offeringKey
  
destMidVar2._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
    
destMidVar2.oCode = srcMidVar1.NewMainProductId

def destMidVar3 = destArgs1.changeSubOfferingInfo

mappingList(srcMidVar1.Product,destMidVar3.addSuppOfferings,listMapping0)

mappingList(srcMidVar1.RemovedProduct,destMidVar3.delSuppOfferings,listMapping2)
