def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destMidVar = destArgs0.ManageGroupAttendRequestMsg.RequestHeader

def srcMidVar = srcArgs0.omRequestHeader

destMidVar.additionInfo = srcMidVar.belToAreaId

destMidVar.CommandId = srcMidVar.commandId

destMidVar.currentCell = srcMidVar.currentCell

destMidVar.InterFrom = srcMidVar.interFrom

destMidVar.InterMedi = srcMidVar.interMedi

destMidVar.InterMode = srcMidVar.interMode

destMidVar.OperatorID = srcMidVar.operatorId

destMidVar.PartnerID = srcMidVar.partnerId

destMidVar.PartnerOperID = srcMidVar.partnerOperId

destMidVar.Remark = srcMidVar.remark

destMidVar.RequestType = srcMidVar.requestType

destMidVar.SequenceId = srcMidVar.sequenceId

destMidVar.SerialNo = srcMidVar.serialNo

def destMidVar0 = destArgs0.ManageGroupAttendRequestMsg.RequestHeader.SessionEntity

def srcMidVar0 = srcArgs0.omRequestHeader.sessionEntity

destMidVar0.Name = srcMidVar0.name

destMidVar0.Password = srcMidVar0.password

destMidVar0.RemoteAddress = srcMidVar0.remoteAddress

destMidVar.TenantID = srcMidVar.tenantId

destMidVar.ThirdPartyID = srcMidVar.thirdPartyId

destMidVar.TradePartnerID = srcMidVar.tradePartnerId

destMidVar.Version = srcMidVar.version

destMidVar.visitArea = srcMidVar.visitArea

destMidVar.TransactionId = srcMidVar.transactionId

def destMidVar1 = destArgs0.ManageGroupAttendRequestMsg.ManageGroupAttendRequest

destMidVar1.EffectiveTime = formatDate(srcArgs0.effectiveTime, "yyyyMMddHHmmss")

destMidVar1.ExpireTime = formatDate(srcArgs0.expireTime, "yyyyMMddHHmmss")

destMidVar1.GroupNumber = srcArgs0.groupNumber

destMidVar1.GrpAttendantNo = srcArgs0.grpAttendantNo

destMidVar1.OperationType = srcArgs0.operationType
