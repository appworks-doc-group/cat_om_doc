def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.notifyFileExecuteResultResponse.NotifyFileExecuteResultReply

destMidVar.beId = srcReturn.beId

destMidVar.retCode = srcReturn.retCode

destMidVar.retDesc = srcReturn.retDesc
