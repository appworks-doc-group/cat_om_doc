dest.setServiceOperation("CBSInterfaceContentMgrService","manageBillDesc");

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destArgs1 = dest.payload._args[1]

destArgs0._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12MessageHeader"

destArgs1._class = "com.huawei.ngcbs.cm.content.managebilldesc.io.ManageBillDescRequest"

def srcMidVar1 = srcArgs0.ManageBillDescRequestMsg.ManageBillDescRequest

destArgs1.operationType = srcMidVar1.OperationType

destArgs1.billDescCode = srcMidVar1.BillDescCode

destArgs1.descLang1 = srcMidVar1.DescLang1

destArgs1.descLang2 = srcMidVar1.DescLang2

destArgs1.descLang3 = srcMidVar1.DescLang3

destArgs1.descLang4 = srcMidVar1.DescLang4

destArgs1.descLang5 = srcMidVar1.DescLang5

def srcMidVar = srcArgs0.ManageBillDescRequestMsg.RequestHeader

destArgs0.beId = srcMidVar.TenantId

destArgs0.msgLanguageCode = srcMidVar.Language

destArgs0.operatorId = srcMidVar.OperatorID

destArgs0.additionInfo = srcMidVar.additionInfo

destArgs0.commandId = srcMidVar.CommandId

destArgs0.interMedi = srcMidVar.InterMedi

destArgs0.transactionId = srcMidVar.TransactionId

destArgs0.reserve2 = srcMidVar.Reserve2

destArgs0.thirdPartyId = srcMidVar.ThirdPartyID

destArgs0.reserve3 = srcMidVar.Reserve3

destArgs0.interMode = srcMidVar.InterMode

destArgs0.sequenceId = srcMidVar.SequenceId

destArgs0.visitArea = srcMidVar.visitArea

destArgs0.belToAreaId = srcMidVar.BelToAreaID

destArgs0.currentCell = srcMidVar.currentCell

destArgs0.partnerId = srcMidVar.PartnerID

destArgs0.partnerOperId = srcMidVar.PartnerOperID

destArgs0.version = srcMidVar.Version

destArgs0.remark = srcMidVar.Remark

destArgs0.requestType = srcMidVar.RequestType

def srcMidVar0 = srcArgs0.ManageBillDescRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar0.Name

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteAddress

destArgs0.interFrom = srcMidVar.InterFrom

destArgs0.tradePartnerId = srcMidVar.TradePartnerID

destArgs0.messageSeq = srcMidVar.SerialNo