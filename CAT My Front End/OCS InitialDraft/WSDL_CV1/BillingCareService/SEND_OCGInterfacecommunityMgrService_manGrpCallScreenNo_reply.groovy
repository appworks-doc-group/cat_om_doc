def omResultDoc = src.payload._return
def bmResult = dest.payload._return
bmResult._class = "com.huawei.ngcbs.cm.common.ws.client.io.om.OMCommonResult"

def omResultHeader = omResultDoc.ManGrpCallScreenNoResultMsg.ResultHeader
def bmResultHeader = bmResult.resultHeader

bmResultHeader.version = omResultHeader.Version
bmResultHeader.resultCode = omResultHeader.ResultCode
bmResultHeader.resultDesc = omResultHeader.ResultDesc
