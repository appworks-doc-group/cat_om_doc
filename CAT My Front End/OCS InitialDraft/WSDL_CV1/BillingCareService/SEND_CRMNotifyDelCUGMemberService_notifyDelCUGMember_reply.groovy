def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.paraName = src.Name
	
	dest.paraValue = src.Value
	
}

def destMidVar = destReturn.workOrderHeader

def srcMidVar = srcReturn.NotifyDelCUGMemberResponse.RequestHeader

destMidVar.accessPwd = srcMidVar.AccessPwd

destMidVar.accessUser = srcMidVar.AccessUser

destMidVar.beId = srcMidVar.BEId

destMidVar.channelId = srcMidVar.ChannelId

def srcMidVar0 = srcReturn.NotifyDelCUGMemberResponse.RequestHeader.ExtParamList

mappingList(srcMidVar0.ParameterInfo,destMidVar.extParamList,listMapping0)

destMidVar.language = srcMidVar.Language

destMidVar.operatorId = srcMidVar.OperatorId

destMidVar.operatorPwd = srcMidVar.OperatorPwd

destMidVar.processTime = srcMidVar.ProcessTime

destMidVar.sessionId = srcMidVar.SessionId

destMidVar.testFlag = srcMidVar.TestFlag

destMidVar.timeType = srcMidVar.TimeType

destMidVar.timeZoneId = srcMidVar.TimeZoneID

destMidVar.transactionId = srcMidVar.TransactionId

destMidVar.version = srcMidVar.Version

def srcMidVar1 = srcReturn.NotifyDelCUGMemberResponse

destReturn.retCode = srcMidVar1.RetCode

destReturn.retMsg = srcMidVar1.RetMsg

destReturn._class = "com.huawei.ngcbs.cm.reverse.notifydelcugmember.NotifyDelCUGMemberResult"
