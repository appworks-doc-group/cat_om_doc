def srcReturn = src.payload._return

def destReturn = dest.payload._return

def srcResultHeader = srcReturn.resultHeader

def srcResultBody = srcReturn.resultBody

def destResultHeader = destReturn.ProductQueryResponse.ProductQueryResult.ResultMessage.MessageHeader

destResultHeader.CommandId = srcResultHeader.commandId

destResultHeader.Version = srcResultHeader.version

destResultHeader.TransactionId = srcResultHeader.transactionId

destResultHeader.SequenceId = srcResultHeader.sequenceId

destResultHeader.ResultCode = srcResultHeader.resultCode

destResultHeader.ResultDesc = srcResultHeader.resultDesc

destResultHeader.TenantId = srcResultHeader.tenantId

destResultHeader.Language = srcResultHeader.language

def destResultBody = destReturn.ProductQueryResponse.ProductQueryResult.ResultMessage.MessageBody

destResultBody.TotalNumbers = srcResultBody.totalNumbers

def listMapping0

listMapping0 = 
{
    src,dest  ->

    dest.ProductID = src.productId

    dest.ProductOrderKey = src.productOrderKey

    dest.EffectiveTime = formatDate(src.effEctiveDate, "yyyyMMddHHmmss")
     
    dest.ExpireTime = formatDate(src.expiredDate, "yyyyMMddHHmmss")

    dest.PolicyId = src.policyId

    dest.CurCycleStartTime = formatDate(src.curCycleStartTime, "yyyyMMddHHmmss")

    dest.CurCycleEndTime = formatDate(src.curCycleEndTime, "yyyyMMddHHmmss")
    
    dest.NextSpringTime = formatDate(src.nextSpringTime, "yyyyMMddHHmmss")
}

mappingList(srcResultBody.productOrderList,destResultBody.ProductOrderList.ProductOrder,listMapping0)