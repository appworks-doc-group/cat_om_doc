def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping_fuChg

listMapping_fuChg = 
{
    src,dest  ->

	dest.FreeUnitInstanceID = src.freeUnitInstanceId
	
	dest.FreeUnitType = src.freeUnitType
	
	dest.FreeUnitTypeName = src.freeUnitTypeName
	
	dest.MeasureUnit = src.measureUnit
	
	dest.MeasureUnitName = src.measureUnitName
	
	dest.OldAmt = src.oldAmt
	
	dest.NewAmt = src.newAmt
	
}

def listMapping_balChg

listMapping_balChg = 
{
    src,dest  ->

	dest.BalanceType = src.balanceType
	
	dest.BalanceID = src.balanceId
	
	dest.BalanceTypeName = src.balanceTypeName
	
	dest.OldBalanceAmt = src.oldBalanceAmt
	
	dest.NewBalanceAmt = src.newBalanceAmt
	
	dest.CurrencyID = src.currencyId
	
}

def listMapping_oldLifeChgor

listMapping_oldLifeChgor = 
{
    src,dest  ->

	dest.StatusName = src.statusName
	
	dest.StatusIndex = src.statusIndex
	
	dest.StatusExpireTime = src.statusExpireTimeStr
	
}

def listMapping_newLifeChgor

listMapping_newLifeChgor = 
{
    src,dest  ->

	dest.StatusName = src.statusName
	
	dest.StatusIndex = src.statusIndex
	
	dest.StatusExpireTime = src.statusExpireTimeStr
	
}

def listMapping_oldLifeChgee

listMapping_oldLifeChgee = 
{
    src,dest  ->

	dest.StatusName = src.statusName
	
	dest.StatusIndex = src.statusIndex
	
	dest.StatusExpireTime = src.statusExpireTimeStr
	
}

def listMapping_newLifeChgee

listMapping_newLifeChgee = 
{
    src,dest  ->

	dest.StatusName = src.statusName
	
	dest.StatusIndex = src.statusIndex
	
	dest.StatusExpireTime = src.statusExpireTimeStr
	
}

def listMapping_transfereeInfo

listMapping_transfereeInfo = 
{
    src,dest  ->

	dest.SubKey = src.subKey
	
	dest.PrimaryIdentity = src.primaryIdentity
	
  mappingList(src.lifeCycleChgInfo.oldLifeCycleStatusList, dest.LifeCycleChgInfo.OldLifeCycleStatus, listMapping_oldLifeChgee)

  mappingList(src.lifeCycleChgInfo.newLifeCycleStatusList, dest.LifeCycleChgInfo.NewLifeCycleStatus, listMapping_newLifeChgee)
  
  dest.LifeCycleChgInfo.ChgValidity = src.lifeCycleChgInfo.chgValidity
	
	dest.RemainNumTransInMonth = src.remainNumTransInMonth
	
}

def listMapping_transferValidityInfo

listMapping_transferValidityInfo = 
{
    src,dest  ->

	dest.ResultCode = src.resultCode
	
	dest.ResultDesc = src.resultDesc
	
	dest.TradeTime = src.tradeTime
	
	dest.AcctKey = src.acctKey
	
	dest.SubKey = src.subKey
	
	dest.PrimaryIdentity = src.primaryIdentity
	
	dest.TransferChannelID = src.transferChannelID
	
	dest.TransID = src.transID
	
	dest.TransferDays = src.transferDays
	
	dest.HandlingChargeAmt = src.handlingChargeAmt
	
	mappingList(src.balanceChgInfoList, dest.BalanceChgInfo, listMapping_balChg)
	
	dest.CurrencyID = src.currencyID
	
  mappingList(src.freeUnitChgInfoList, dest.FreeUnitChgInfo, listMapping_fuChg)
	
  mappingList(src.lifeCycleChgInfo.oldLifeCycleStatusList, dest.LifeCycleChgInfo.OldLifeCycleStatus, listMapping_oldLifeChgor)

  mappingList(src.lifeCycleChgInfo.newLifeCycleStatusList, dest.LifeCycleChgInfo.NewLifeCycleStatus, listMapping_newLifeChgor)
  
  dest.LifeCycleChgInfo.ChgValidity = src.lifeCycleChgInfo.chgValidity
	
	dest.RemainNumTransOutMonth = src.remainNumTransOutMonth
	
	listMapping_transfereeInfo.call(src.transfereeInfo, dest.TransfereeInfo)
	
}

def destMidVar1 = destReturn.QueryTransferValidityLogResultMsg.QueryTransferValidityLogResult

def srcMidVar1 = srcReturn.queryTransferValidityLogResultInfo

mappingList(srcMidVar1.transferValidityInfoList, destMidVar1.TransferValidityInfo,listMapping_transferValidityInfo)

destMidVar1.TotalRowNum = srcMidVar1.totalNum

destMidVar1.BeginRowNum = srcMidVar1.beginRowNum

destMidVar1.FetchRowNum = srcMidVar1.fetchRowNum


def destMidVar = destReturn.QueryTransferValidityLogResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.Version = srcMidVar.version

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.MessageSeq = srcMidVar.messageSeq

addingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)