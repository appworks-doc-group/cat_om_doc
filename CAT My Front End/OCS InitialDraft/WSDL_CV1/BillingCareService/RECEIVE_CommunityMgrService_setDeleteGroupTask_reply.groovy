import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.SetDeleteGroupTaskResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.CommandId = srcMidVar.commandId

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.TransactionId = srcMidVar.transactionId

destMidVar.SequenceId = srcMidVar.sequenceId

destMidVar.Version = srcMidVar.version

destMidVar.OperationTime = srcMidVar.operationTime

destMidVar.OrderId = srcMidVar.orderId

def destMidVar1 = destReturn.SetDeleteGroupTaskResultMsg.SetDeleteGroupTaskResult

def srcMidVar1 = srcReturn.ocs33SetDeleteGroupTaskResultInfo

destMidVar1.TaskName = srcMidVar1.taskName

destMidVar1.TaskRunTime = formatDate(srcMidVar1.taskRunTime, Constant4Model.DATE_FORMAT)
