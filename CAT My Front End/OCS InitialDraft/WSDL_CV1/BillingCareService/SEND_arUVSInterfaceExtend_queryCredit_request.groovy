import com.huawei.ngcbs.bm.common.common.Constant4Model;

dest.setServiceOperation("arUVSInterfaceExtend","queryCredit");

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs11ws.core.bo.Ocs11MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs11ws.io.querycredit.QueryCreditRequestOcs12Base"

def listMapping1

listMapping1 =
        {
            src,dest  ->

                dest.acctId = src.AcctId

                dest.acctCode = src.AcctCode

        }

def srcMidVar = srcArgs0.QueryCredit.QueryCreditRequest.RequestMessage.MessageHeader

destArgs0.requestType = srcMidVar.RequestType

destArgs0.tenantId = srcMidVar.TenantId

destArgs0.sequenceId = srcMidVar.SequenceId

destArgs0.transactionId = srcMidVar.TransactionId

destArgs0.version = srcMidVar.Version

destArgs0.commandId = srcMidVar.CommandId

destArgs0.language = srcMidVar.Language

def srcMidVar1 = srcArgs0.QueryCredit.QueryCreditRequest.RequestMessage.MessageBody.QueryCreditRequestList

destArgs0.interFrom = srcMidVar1.QueryCreditRequestValue.AcctId

mappingList(srcMidVar1.QueryCreditRequestValue,destArgs1.queryCreditRequestListType,listMapping1)

def srcMidVar2 = srcArgs0.QueryCredit.QueryCreditRequest.RequestMessage.MessageBody

destArgs0.interMode = srcMidVar2.AccessMethod

def srcSessionEntity = srcArgs0.QueryCredit.SessionEntity

destArgs0.sessionEntity.userID = srcSessionEntity.userID

destArgs0.sessionEntity.password = srcSessionEntity.password

destArgs0.sessionEntity.locale = srcSessionEntity.locale

destArgs0.sessionEntity.loginVia = srcSessionEntity.loginVia

destArgs0.sessionEntity.remoteAddr = srcSessionEntity.remoteAddr

destArgs0.sessionEntity.uploadRoot = srcSessionEntity.uploadRoot

destArgs0.loginSystem = srcSessionEntity.userID

destArgs0.password = srcSessionEntity.password


