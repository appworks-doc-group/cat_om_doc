import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.ChangeSubscriberBasicInforResultMsg.ResultHeader

destMidVar.CommandId = srcReturn.commandId

destMidVar.TransactionId = srcReturn.transactionId

destMidVar.ResultCode = srcReturn.resultCode

destMidVar.Version = srcReturn.version

destMidVar.ResultDesc = srcReturn.resultDesc

destMidVar.SequenceId = srcReturn.sequenceId

destMidVar.OrderId = srcReturn.orderId

destMidVar.OperationTime = srcReturn.operationTime