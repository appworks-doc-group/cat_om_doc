def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destArgs1 = dest.payload._args[1]

def destMidVar = destArgs1.subAccessCode

def srcMidVar = srcArgs0.DisclaimMissingRequestMsg.DisclaimMissingRequest

destMidVar.primaryIdentity = srcMidVar.SubscriberNo

def srcMidVar0 = srcArgs0.DisclaimMissingRequestMsg.RequestHeader

destArgs0.beId = srcMidVar0.TenantId

destArgs0.operatorId = srcMidVar0.OperatorID

destArgs0.additionInfo = srcMidVar0.additionInfo

destArgs0.commandId = srcMidVar0.CommandId

destArgs0.interMedi = srcMidVar0.InterMedi

destArgs0.transactionId = srcMidVar0.TransactionId

destArgs0.reserve2 = srcMidVar0.Reserve2

destArgs0.thirdPartyId = srcMidVar0.ThirdPartyID

destArgs0.reserve3 = srcMidVar0.Reserve3

destArgs0.interMode = srcMidVar0.InterMode

destArgs0.sequenceId = srcMidVar0.SequenceId

destArgs0.visitArea = srcMidVar0.visitArea

destArgs0.messageSeq = srcMidVar0.SerialNo

destArgs0.belToAreaId = srcMidVar0.BelToAreaID

destArgs0.partnerId = srcMidVar0.PartnerID

destArgs0.remark = srcMidVar0.Remark

destArgs0.tradePartnerId = srcMidVar0.TradePartnerID

destArgs0.version = srcMidVar0.Version

destArgs0.partnerOperId = srcMidVar0.PartnerOperID

destArgs0.requestType = srcMidVar0.RequestType

def srcMidVar1 = srcArgs0.DisclaimMissingRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar1.Name

destArgs0.password = srcMidVar1.Password

destArgs0.remoteAddress = srcMidVar1.RemoteAddress

destArgs0.interFrom = srcMidVar0.InterFrom

def destMidVar0 = destArgs1.changeSubStatusInfo

destMidVar0.opType = 20
