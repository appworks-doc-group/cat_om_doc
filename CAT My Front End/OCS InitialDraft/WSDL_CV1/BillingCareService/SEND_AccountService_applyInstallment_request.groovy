def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.account.applyinstallment.io.ApplyInstallmentRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.value = src.Value
	
	dest.code = src.Code
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
	
}

def srcMidVar = srcArgs0.ApplyInstallmentRequestMsg.RequestHeader

destArgs0.version = srcMidVar.Version

def srcMidVar0 = srcArgs0.ApplyInstallmentRequestMsg.RequestHeader.TimeFormat

destArgs0.timeZoneId = srcMidVar0.TimeZoneID

destArgs0.timeType = srcMidVar0.TimeType

def srcMidVar1 = srcArgs0.ApplyInstallmentRequestMsg.RequestHeader.OwnershipInfo

destArgs0.brId = srcMidVar1.BRID

destArgs0.beId = srcMidVar1.BEID

def srcMidVar2 = srcArgs0.ApplyInstallmentRequestMsg.RequestHeader.OperatorInfo

destArgs0.operatorId = srcMidVar2.OperatorID

destArgs0.channelId = srcMidVar2.ChannelID

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.businessCode = srcMidVar.BusinessCode

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

def srcMidVar3 = srcArgs0.ApplyInstallmentRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar3.LoginSystemCode

destArgs0.password = srcMidVar3.Password

destArgs0.remoteAddress = srcMidVar3.RemoteIP

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar4 = srcArgs0.ApplyInstallmentRequestMsg.ApplyInstallmentRequest

mappingList(srcMidVar4.AdditionalProperty,destArgs1.simplePropertyList,listMapping1)

def srcMidVar5 = srcArgs0.ApplyInstallmentRequestMsg.ApplyInstallmentRequest.ApplyObj

def destMidVar = destArgs1.anyAccessCode

listMapping2.call(srcMidVar5.CustAccessCode,destMidVar.custAccessCode)

listMapping3.call(srcMidVar5.SubAccessCode,destMidVar.subAccessCode)

listMapping4.call(srcMidVar5.SubGroupAccessCode,destMidVar.groupAccessCode)

destArgs1.chargeCode = srcMidVar4.ChargeCode

destArgs1.contractID = srcMidVar4.ContractID

destArgs1.cycleLength = srcMidVar4.CycleLength

destArgs1.cycleRefDate = srcMidVar4.CycleRefDate

destArgs1.cycleType = srcMidVar4.CycleType

def srcMidVar6 = srcArgs0.ApplyInstallmentRequestMsg.ApplyInstallmentRequest.InatallmentPlan

destArgs1.finalCycleAmount = srcMidVar6.FinalCycleAmount

destArgs1.firstCycleAmount = srcMidVar6.FirstCycleAmount

destArgs1.planType = srcMidVar6.PlanType

destArgs1.totalAmount = srcMidVar4.TotalAmount

destArgs1.totalCycle = srcMidVar4.TotalCycle

destArgs1.currencyID = srcMidVar4.CurrencyID
