import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	mappingList(src.propList,dest.PropList,listMapping3)
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	mappingList(src.additionalProperty,dest.AdditionalProperty,listMapping5)
	
	dest.ChargeAmount = src.chargeAmount
	
	dest.CurrencyID = src.currencyId
	
	dest.OfferingID = src.offeringId
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	mappingList(src.additionalProperty,dest.AdditionalProperty,listMapping7)
	
	dest.ChargeAmount = src.chargeAmount
	
	dest.CurrencyID = src.currencyId
	
	dest.OfferingID = src.offeringId
	
}

def listMapping9

listMapping9 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping8

listMapping8 = 
{
    src,dest  ->

	mappingList(src.additionalProperty,dest.AdditionalProperty,listMapping9)
	
	dest.ChargeAmount = src.chargeAmount
	
	dest.ChargeCode = src.chargeCode
	
	dest.CurrencyID = src.currencyId
	
	dest.SubCategory = src.subCategory
	
}

def listMapping11

listMapping11 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping10

listMapping10 = 
{
    src,dest  ->

	mappingList(src.additionalProperty,dest.AdditionalProperty,listMapping11)
	
	dest.ChargeAmount = src.chargeAmount
	
	dest.CurrencyID = src.currencyId
	
	dest.OfferingID = src.offeringId
	
}

def listMapping13

listMapping13 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping12

listMapping12 = 
{
    src,dest  ->

	mappingList(src.additionalProperty,dest.AdditionalProperty,listMapping13)
	
	dest.CurrencyID = src.currencyId
	
	dest.TaxAmount = src.taxAmount
	
	dest.TaxCode = src.taxCode
	
}

def listMapping15

listMapping15 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping14

listMapping14 = 
{
    src,dest  ->

	dest.ActualVolume = src.actualVolume
	
	mappingList(src.additionalProperty,dest.AdditionalProperty,listMapping15)
	
	dest.ChargeAmount = src.chargeAmount
	
	dest.ChargeCode = src.chargeCode
	
	dest.CurrencyID = src.currencyId
	
	dest.FreeVolume = src.freeVolume
	
	dest.MeasureUnit = src.measureUnit
	
	dest.RatingDiscVolume = src.ratingDiscVolume
	
	dest.RatingVolume = src.ratingVolume
	
	dest.SubCategory = src.subCategory
	
}

def listMapping18

listMapping18 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping17

listMapping17 = 
{
    src,dest  ->

	mappingList(src.additionalProperty,dest.AdditionalProperty,listMapping18)
	
	dest.ChargeAmount = src.chargeAmount
	
	dest.ChargeCode = src.chargeCode
	
	dest.ChargeTime=formatDate(src.chargeTime, Constant4Model.DATE_FORMAT)
	
	dest.CurrencyID = src.currencyId
	
	dest.SubCategory = src.subCategory
	
}

def listMapping20

listMapping20 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping19

listMapping19 = 
{
    src,dest  ->

	mappingList(src.additionalProperty,dest.AdditionalProperty,listMapping20)
	
	dest.ChargeAmount = src.chargeAmount
	
	dest.CurrencyID = src.currencyId
	
	dest.OfferingID = src.offeringId
	
	dest.SubCategory = src.subCategory
	
}

def listMapping22

listMapping22 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping21

listMapping21 = 
{
    src,dest  ->

	mappingList(src.additionalProperty,dest.AdditionalProperty,listMapping22)
	
	dest.AdjustAmount = src.adjustAmount
	
	dest.CurrencyID = src.currencyId
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	listMapping2.call(src.customizedCharge,dest.CustomizedCharge)
	
	listMapping4.call(src.discount,dest.Discount)
	
	listMapping6.call(src.minCommitmentCharge,dest.MinCommitmentCharge)
	
	listMapping8.call(src.nRecurringCharge,dest.NRecurringCharge)
	
	listMapping10.call(src.recurringCharge,dest.RecurringCharge)
	
	listMapping12.call(src.tax,dest.Tax)
	
	listMapping14.call(src.usageCharge,dest.UsageCharge)
	
	listMapping17.call(src.nRecCharge,dest.NRecurringCharge)
	
	listMapping19.call(src.billMediumCharge,dest.BillMediumCharge)
	
	listMapping21.call(src.adjustmentInfo,dest.Adjustment)
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.AcctKey = src.acctKey
	
	listMapping1.call(src.aggregation,dest.AggregationInfo)
	
	dest.BillChargeID = src.billChargeId
	
	dest.BillCycleID = src.billCycleId
	
	dest.Category = src.category
	
	dest.CustKey = src.custKey
	
	dest.ObjKey = src.objKey
	
	dest.ObjType = src.objType
	
	dest.PrimaryIdentity = src.primaryIdentity
	
}

def listMapping16

listMapping16 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def srcMidVar = srcReturn.aggregationAmountList

def destMidVar = destReturn.QueryAggregationAmountResultMsg.QueryAggregationAmountResult

mappingList(srcMidVar.aggregationAmountList,destMidVar.AggregationAmountList,listMapping0)

def srcMidVar0 = srcReturn.resultHeader

def destMidVar0 = destReturn.QueryAggregationAmountResultMsg.ResultHeader

mappingList(srcMidVar0.simpleProperty,destMidVar0.AdditionalProperty,listMapping16)

destMidVar0.MsgLanguageCode = srcMidVar0.msgLanguageCode

destMidVar0.ResultCode = srcMidVar0.resultCode

destMidVar0.ResultDesc = srcMidVar0.resultDesc

destMidVar0.Version = srcMidVar0.version

destMidVar0.MessageSeq = srcMidVar0.messageSeq
