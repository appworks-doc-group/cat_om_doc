def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.resultHeader

def srcMidVar = srcReturn.RechargeResultMsg.ResultHeader

destMidVar.resultDesc = srcMidVar.ResultDesc

destMidVar.resultCode = srcMidVar.ResultCode

def listMapping0

listMapping0 = 
{
    src0,dest0  ->

    dest0.value = src0.Value
    
    dest0.code = src0.Code
    
}

addingList(srcMidVar.AdditionalProperty,destMidVar.simpleProperty,listMapping0)

destMidVar.msgLanguageCode = srcMidVar.MsgLanguageCode

destMidVar.version = srcMidVar.Version

destReturn._class = "com.huawei.ngcbs.cm.common.cm2ar.bo.DefaultAr2CmResponse"