dest.setServiceOperation("SubscriberService","batchChangeScatteredSubOwnership")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.batch.changescatteredsubownership.io.BatchChangeScatteredSubOwnershipRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.BatchChangeScatteredSubOwnershipRequestMsg.BatchChangeScatteredSubOwnershipRequest

destArgs1.requestFileName = srcMidVar.FileName

destArgs1.prepaidBillCycleType = srcMidVar.PrepaidBillCycleType

destArgs1.custKey = srcMidVar.CustKey

mappingList(srcMidVar.ControlProperty,destArgs1.controlProperties,listMapping0)

def srcMidVar0 = srcArgs0.BatchChangeScatteredSubOwnershipRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar0.AccessMode

def srcMidVar1 = srcArgs0.BatchChangeScatteredSubOwnershipRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar1.LoginSystemCode

destArgs0.password = srcMidVar1.Password

destArgs0.remoteAddress = srcMidVar1.RemoteIP

mappingList(srcMidVar0.AdditionalProperty,destArgs0.simpleProperty,listMapping1)

destArgs0.businessCode = srcMidVar0.BusinessCode

destArgs0.messageSeq = srcMidVar0.MessageSeq

destArgs0.msgLanguageCode = srcMidVar0.MsgLanguageCode

def srcMidVar2 = srcArgs0.BatchChangeScatteredSubOwnershipRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar2.ChannelID

destArgs0.operatorId = srcMidVar2.OperatorID

def srcMidVar3 = srcArgs0.BatchChangeScatteredSubOwnershipRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar3.BEID

destArgs0.brId = srcMidVar3.BRID

def srcMidVar4 = srcArgs0.BatchChangeScatteredSubOwnershipRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar4.TimeType

destArgs0.timeZoneId = srcMidVar4.TimeZoneID

destArgs0.version = srcMidVar0.Version
