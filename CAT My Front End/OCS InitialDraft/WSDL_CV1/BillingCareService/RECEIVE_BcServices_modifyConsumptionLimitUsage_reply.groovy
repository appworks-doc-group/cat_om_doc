import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 = 
{
    src,dest  ->
	
	dest.LimitType = src.limitType

	dest.LimitTypeName = src.limitTypeName

	dest.Amount = src.amount

	dest.UsageAmount = src.usageAmount

	dest.LimitCtrlType = src.limitCtrlType

	dest.CurrencyID = src.currencyID

	dest.MeasureType = src.measureType

	dest.MeasureID = src.measureID

	dest.MeasureType = src.measureType

	dest.BeginTime = formatDate(src.beginTime, Constant4Model.DATE_FORMAT)
	
	dest.EndTime = formatDate(src.endTime, Constant4Model.DATE_FORMAT)
}

def srcMidVar0 = srcReturn.resultHeader

def destMidVar0 = destReturn.ModifyConsumptionLimitUsageResultMsg.ResultHeader

destMidVar0.ResultDesc = srcMidVar0.resultDesc

destMidVar0.ResultCode = srcMidVar0.resultCode

destMidVar0.MsgLanguageCode = srcMidVar0.msgLanguageCode

destMidVar0.Version = srcMidVar0.version

destMidVar0.MessageSeq = srcMidVar0.messageSeq

def destMidVar1 = destReturn.ModifyConsumptionLimitUsageResultMsg.ModifyConsumptionLimitUsageResult

mappingList(srcReturn.limitUsageList,destMidVar1.LimitUsageList,listMapping1)




