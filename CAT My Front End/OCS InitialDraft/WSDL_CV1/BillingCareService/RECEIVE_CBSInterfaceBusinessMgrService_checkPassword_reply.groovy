def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.CheckPasswordResultMsg.ResultHeader

destMidVar.Version = srcReturn.version

destMidVar.TransactionId = srcReturn.transactionId

destMidVar.SequenceId = srcReturn.sequenceId

destMidVar.ResultCode = srcReturn.resultCode

destMidVar.ResultDesc = srcReturn.resultDesc

destMidVar.CommandId = srcReturn.commandId
