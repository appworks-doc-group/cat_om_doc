def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Amount = src.amount
	
	dest.CurrencyID = src.currencyId
	
	dest.LimitInstID = src.limitInstId
	
	dest.LimitType = src.limitType
	
	dest.MeasureID = src.measureId
	
	dest.PayRelationKey = src.payRelationKey
	
	def destMidVar1 = dest.PayRelExtRule
	
	def srcMidVar0 = src.payRelExtRule
	
	destMidVar1.ChargeCode = srcMidVar0.chargeCode
	
	destMidVar1.ControlRule = srcMidVar0.controlRule
	
	def destMidVar2 = dest.PayRelExtRule.OfferingKey
	
	def srcMidVar1 = src.payRelExtRule.offeringKey
	
	destMidVar2.OfferingID = srcMidVar1.oId
	
	destMidVar2.PurchaseSeq = srcMidVar1.pSeq
	
	dest.Priority = src.priority
	
	dest.UsageAmount = src.usageAmount
	
}

def destMidVar = destReturn.QueryPaymentLimitUsageResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.Version = srcMidVar.version

mappingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def destMidVar0 = destReturn.QueryPaymentLimitUsageResultMsg.QueryPaymentLimitUsageResult

mappingList(srcReturn.queryPaymentLimitUsageResultBody,destMidVar0.LimitUsageList,listMapping1)
