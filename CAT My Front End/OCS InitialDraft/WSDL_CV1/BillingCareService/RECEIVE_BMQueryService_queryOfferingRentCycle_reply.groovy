import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.MsgLanguageCode = src.msgLanguageCode
	
	dest.ResultCode = src.resultCode
	
	dest.ResultDesc = src.resultDesc
	
	mappingList(src.simpleProperty,dest.AdditionalProperty,listMapping1)
	
	dest.Version = src.version
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.AccountCode = src.accoutCode
	
	dest.AccountKey = src.accoutKey
	
	dest.PayType = src.payType
	
	dest.PrimaryIdentity = src.primaryIdentity
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.CustomerCode = src.customerCode
	
	dest.CustomerKey = src.customerKey
	
	dest.PrimaryIdentity = src.primaryIdentity
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.SubGroupCode = src.groupCode
	
	dest.SubGroupKey = src.groupKey
	
}

def listMapping8

listMapping8 = 
{
    src,dest  ->

	dest.PrimaryIdentity = src.primaryIdentity
	
	dest.SubscriberKey = src.subscriberKey
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	listMapping5.call(src.acctAccessCode,dest.AcctAccessCode)
	
	listMapping6.call(src.custAccessCode,dest.CustAccessCode)
	
	listMapping7.call(src.groupAccessCode,dest.SubGroupAccessCode)
	
	listMapping8.call(src.subAccessCode,dest.SubAccessCode)
	
}

def listMapping9

listMapping9 = 
{
    src,dest  ->

	dest.OfferingID = src.oId
	
	dest.PurchaseSeq = src.pSeq
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	listMapping4.call(src.anyAccessCode,dest.OfferingOwner)
	
	dest.CurrencyID = src.currencyId
	
	//import com.huawei.ngcbs.bm.common.common.Constant4Model
	dest.EndDay=formatDate(src.endDay, Constant4Model.DATE_FORMAT)
	
	listMapping9.call(src.offeringKeyInfo,dest.OfferingKey)
	
	dest.OpenDay=formatDate(src.openDay, Constant4Model.DATE_FORMAT)
	
	dest.ProcessedCycleNum = src.processedCycleNum
	
	dest.RentAmount = src.rentAmount
	
	dest.SuccessCycleNum = src.successCycleNum
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	mappingList(src.queryOfferingRecentCycleDataList,dest.OfferingRentCycle,listMapping3)
	
}

def destMidVar = destReturn.QueryOfferingRentCycleResultMsg

listMapping0.call(srcReturn.resultHeader,destMidVar.ResultHeader)

listMapping2.call(srcReturn.resultInfo,destMidVar.QueryOfferingRentCycleResult)
