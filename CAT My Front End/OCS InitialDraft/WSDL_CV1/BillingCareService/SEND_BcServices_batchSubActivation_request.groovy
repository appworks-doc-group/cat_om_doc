import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("SubscriberService","batchSubActivation")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.batch.subactivation.io.BatchSubActivationRequest"

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.businessCode = src.BusinessCode
	
	def srcMidVar0 = src.OperatorInfo
	
	dest.channelId = srcMidVar0.ChannelID
	
	def srcMidVar1 = src.AccessSecurity
	
	dest.loginSystem = srcMidVar1.LoginSystemCode
	
	dest.messageSeq = src.MessageSeq
	
	dest.msgLanguageCode = src.MsgLanguageCode
	
	dest.operatorId = srcMidVar0.OperatorID
	
	dest.password = srcMidVar1.Password
	
	def srcMidVar2 = src.TimeFormat
	
	dest.timeType = srcMidVar2.TimeType
	
	dest.timeZoneId = srcMidVar2.TimeZoneID
	
	dest.version = src.Version
	
	def srcMidVar3 = src.OwnershipInfo
	
	dest.beId = srcMidVar3.BEID
	
	dest.brId = srcMidVar3.BRID
	
	mappingList(src.AdditionalProperty,dest.simpleProperty,listMapping1)
	
	dest.remoteAddress = srcMidVar1.RemoteIP
	
	dest.interMode = src.AccessMode
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	def destMidVar = dest.subscriberInfo
	
	destMidVar.ivrLang = src.IVRLang
	
	destMidVar.subPassword = src.SubPassword
	
	destMidVar.writtenLang = src.WrittenLang
	
	mappingList(src.SubProperty,dest.properties,listMapping4)
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.expirationTime=parseDate(src.ExpirationTime, Constant4Model.DATE_FORMAT)
	
	dest.trialEndTime=parseDate(src.TrialEndTime, Constant4Model.DATE_FORMAT)
	
	dest.trialStartTime=parseDate(src.TrialStartTime, Constant4Model.DATE_FORMAT)
	
	def destMidVar0 = dest.offeringKey
	
	destMidVar0._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
	
	destMidVar0.oId = src.OfferingID
	
  destMidVar0.oCode = src.OfferingCode
  
  destMidVar0.pSeq = src.PurchaseSeq
	
	dest.OwnerType = src.OwnerType
	
	dest.activeTime=parseDate(src.ActiveTime, Constant4Model.DATE_FORMAT)
	
	dest.activeTimeLimit=parseDate(src.ActiveTimeLimit, Constant4Model.DATE_FORMAT)
	
	dest.ownerType = src.OwnerType
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.requestFileName = src.FileName
	
	listMapping3.call(src.SubBasicInfo,dest.subActivationInfo)
	
	mappingList(src.OfferingInst,dest.activeOfferingInstInfos,listMapping5)
	
}

def srcMidVar = srcArgs0.BatchSubActivationRequestMsg

listMapping0.call(srcMidVar.RequestHeader,destArgs0)

listMapping2.call(srcMidVar.BatchSubActivationRequest,destArgs1)
