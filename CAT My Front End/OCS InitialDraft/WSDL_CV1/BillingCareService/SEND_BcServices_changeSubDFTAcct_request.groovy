import com.huawei.ngcbs.bm.common.common.Constant4Model


dest.setServiceOperation("SubscriberService","changeSubDftAccount")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.changesubdftacct.io.ChangeSubDftAcctRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.tpAcctKey = src.AcctKey
	
	def destMidVar0 = dest.paymentRelation
	
	destMidVar0.finalFlag = src.OnlyPayRelFlag
	
	destMidVar0.paymentLimitKey = src.PaymentLimitKey
	
	destMidVar0.paymentRelationKey = src.PayRelationKey
	
	destMidVar0.priority = src.Priority
	
	def srcMidVar6 = src.PayRelExtRule
	
	destMidVar0.chargeCode = srcMidVar6.ChargeCode
	
	destMidVar0.extRuleCode = srcMidVar6.ControlRule
	
	def destMidVar1 = dest.paymentRelation.offeringKey
    destMidVar1._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
    
	def srcMidVar7 = src.PayRelExtRule.OfferingKey
	
	destMidVar1.oId = srcMidVar7.OfferingID
    destMidVar1.oCode = srcMidVar7.OfferingCode
	destMidVar1.pSeq = srcMidVar7.PurchaseSeq
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.paymentRelationKey = src.PayRelationKey
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	def srcMidVar8 = src.PaymentLimitInfo.Limit
	
	dest.limitMeasureUnit = srcMidVar8.LimitMeasureUnit
	
	dest.limitType = srcMidVar8.LimitType
	
	dest.limitValue = srcMidVar8.LimitValue
	
	dest.limitValueType = srcMidVar8.LimitValueType
	
	def srcMidVar9 = src.PaymentLimitInfo
	
	dest.limitCycleType = srcMidVar9.LimitCycleType
	
	dest.limitRule = srcMidVar9.LimitRule
	
	dest.limitKey = src.PaymentLimitKey
	
	dest.cBonusFlag = srcMidVar9.CBonusFlag
	
	dest.currencyId = srcMidVar9.CurrencyID
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.value = src.Value
	
	dest.key = src.Code
	
}

def listMapping5

listMapping5 =
{
	src,dest  ->

	dest.code = src.Code

	dest.value = src.Value

}

def srcMidVar = srcArgs0.ChangeSubDFTAcctRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar.LoginSystemCode

destArgs0.password = srcMidVar.Password

destArgs0.remoteAddress = srcMidVar.RemoteIP

def srcMidVar0 = srcArgs0.ChangeSubDFTAcctRequestMsg.RequestHeader

mappingList(srcMidVar0.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar0.BusinessCode

destArgs0.messageSeq = srcMidVar0.MessageSeq

destArgs0.msgLanguageCode = srcMidVar0.MsgLanguageCode

def srcMidVar1 = srcArgs0.ChangeSubDFTAcctRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.ChangeSubDFTAcctRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.ChangeSubDFTAcctRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar0.Version

def srcMidVar4 = srcArgs0.ChangeSubDFTAcctRequestMsg.ChangeSubDFTAcctRequest

mappingList(srcMidVar4.ControlProperty,destArgs1.controlPropertys,listMapping4)

mappingList(srcMidVar4.AdditionalProperty, destArgs1.simplePropertyList, listMapping5)

def srcMidVar5 = srcArgs0.ChangeSubDFTAcctRequestMsg.ChangeSubDFTAcctRequest.DFTPayRelation

mappingList(srcMidVar5.AddPayRelation,destArgs1.addPaymentRelations,listMapping1)

mappingList(srcMidVar5.DelPayRelation,destArgs1.delPaymentRelationInfos,listMapping2)

addingList(srcMidVar5.PaymentLimit,destArgs1.paymentLimits,listMapping3)

destArgs1.time= parseDate(srcArgs0.ChangeSubDFTAcctRequestMsg.ChangeSubDFTAcctRequest.EffectiveTime.Time, Constant4Model.DATE_FORMAT)

def srcMidVar10 = srcArgs0.ChangeSubDFTAcctRequestMsg.ChangeSubDFTAcctRequest.EffectiveTime

destArgs1.mode = srcMidVar10.Mode

def destMidVar2 = destArgs1.subAccessCode

def srcMidVar11 = srcArgs0.ChangeSubDFTAcctRequestMsg.ChangeSubDFTAcctRequest.SubAccessCode

destMidVar2.primaryIdentity = srcMidVar11.PrimaryIdentity

destMidVar2.subscriberKey = srcMidVar11.SubscriberKey

def destMidVar3 = destArgs1.changeSubDftAcctInfo.newSubDftAcct

def srcMidVar12 = srcArgs0.ChangeSubDFTAcctRequestMsg.ChangeSubDFTAcctRequest.SubDFTAccount.NewDFTAcct

destMidVar3.dftAcctKey = srcMidVar12.DFTAcctKey

destMidVar3.postpaidAcctKey = srcMidVar12.PostPaidAcctKey

destMidVar3.prepaidAcctKey = srcMidVar12.PrePaidAcctKey

def destMidVar4 = destArgs1.changeSubDftAcctInfo.oldSubDftAcct

def srcMidVar13 = srcArgs0.ChangeSubDFTAcctRequestMsg.ChangeSubDFTAcctRequest.SubDFTAccount.OldDFTAcct

destMidVar4.dftAcctKey = srcMidVar13.DFTAcctKey

destMidVar4.postpaidAcctKey = srcMidVar13.PostPaidAcctKey

destMidVar4.prepaidAcctKey = srcMidVar13.PrePaidAcctKey

destArgs0.interMode = srcMidVar0.AccessMode
