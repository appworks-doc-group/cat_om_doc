
def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->
	
	dest.ParentGroupNumber = src.pGroupNumber
	
	dest.GroupNumber = src.groupNumber

	dest.GroupType = src.groupType
	if(isNotNull(src))
	{
		if(isNull(src.groupType))
		{
			dest.GroupType  =''
		}
	}

	dest.GroupName = src.groupName
	if(isNotNull(src))
	{
		if(isNull(src.groupName))
		{
			dest.GroupName  =''
		}
	}

	dest.GroupCustomerCode = src.groupCustomerCode
	
}

def srcMidVar = srcReturn.resultHeader

def destMidVar = destReturn.QueryAllSubGroupResultMsg.ResultHeader

destMidVar.CommandId = srcMidVar.commandId

destMidVar.Version = srcMidVar.version

destMidVar.TransactionId = srcMidVar.transactionId

destMidVar.SequenceId = srcMidVar.sequenceId

destMidVar.TenantId = srcMidVar.beId

destMidVar.Language = srcMidVar.msgLanguageCode

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.OperationTime = srcMidVar.operationTime

destMidVar.OrderId = srcMidVar.orderId

def destMidVar0 = destReturn.QueryAllSubGroupResultMsg.QueryAllSubGroupResult

def srcMidVar0 = srcReturn.resultBody

mappingList(srcMidVar0.subGroupList,destMidVar0.SubGroup,listMapping0)