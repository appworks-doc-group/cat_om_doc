dest.setServiceOperation("AccountService","querySumInstallment")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.account.querysuminstallment.io.QuerySumInstallmentRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping3

listMapping3 =
{
    src,dest  ->

	dest.code = src.Code

	dest.value = src.Value

}

def listMapping1

listMapping1 =
{
	src,dest  ->

	dest.status = src.Status

}

def srcMidVar0 = srcArgs0.QuerySumInstallmentRequestMsg.QuerySumInstallmentRequest.QueryObj

def destMidVar0 = destArgs1.anyAccessCode

listMapping0.call(srcMidVar0.CustAccessCode,destMidVar0.custAccessCode)

def srcMidVar1 = srcArgs0.QuerySumInstallmentRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar1.AccessMode

def srcMidVar2 = srcArgs0.QuerySumInstallmentRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar2.LoginSystemCode

destArgs0.password = srcMidVar2.Password

destArgs0.remoteAddress = srcMidVar2.RemoteIP

mappingList(srcMidVar1.AdditionalProperty,destArgs0.simpleProperty,listMapping3)

destArgs0.businessCode = srcMidVar1.BusinessCode

destArgs0.messageSeq = srcMidVar1.MessageSeq

destArgs0.msgLanguageCode = srcMidVar1.MsgLanguageCode

def srcMidVar3 = srcArgs0.QuerySumInstallmentRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar3.ChannelID

destArgs0.operatorId = srcMidVar3.OperatorID

def srcMidVar4 = srcArgs0.QuerySumInstallmentRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar4.BEID

destArgs0.brId = srcMidVar4.BRID

def srcMidVar5 = srcArgs0.QuerySumInstallmentRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar5.TimeType

destArgs0.timeZoneId = srcMidVar5.TimeZoneID

destArgs0.version = srcMidVar1.Version

def srcMidVar6 = srcArgs0.QuerySumInstallmentRequestMsg.QuerySumInstallmentRequest

destArgs1.installmentObjType = srcMidVar6.InstallmentObjType

mappingList(srcMidVar6.StatusList,destArgs1.statusList,listMapping1)
