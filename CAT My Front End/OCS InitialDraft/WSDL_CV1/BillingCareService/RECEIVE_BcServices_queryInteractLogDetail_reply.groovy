import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def srcMidVar0 = srcReturn.resultHeader

def destMidVar0 = destReturn.QueryInteractLogDetailResultMsg.ResultHeader

mappingList(srcMidVar0.simpleProperty,destMidVar0.AdditionalProperty,listMapping0)

destMidVar0.ResultDesc = srcMidVar0.resultDesc

destMidVar0.ResultCode = srcMidVar0.resultCode

destMidVar0.MsgLanguageCode = srcMidVar0.msgLanguageCode

destMidVar0.Version = srcMidVar0.version

destMidVar0.MessageSeq = srcMidVar0.messageSeq

def srcMidVar1 = srcReturn.queryInteractLogDetailInfo

def destMidVar1 = destReturn.QueryInteractLogDetailResultMsg.QueryInteractLogDetailResult.InteractLogDetail

def destMidVar2 = destMidVar1.MsgDetail

destMidVar1.RecordID = srcMidVar1.recordID

destMidVar1.RequestSeq = srcMidVar1.requestSeq

destMidVar1.RequestSysCode = srcMidVar1.requestSysCode

destMidVar1.RequestSysIP = srcMidVar1.requestsysip

destMidVar1.InteractMode = srcMidVar1.interactmode

destMidVar1.CustID = srcMidVar1.custid

destMidVar1.OperationCode = srcMidVar1.operationcode

destMidVar1.OperationObjectType = srcMidVar1.operationobjecttype

destMidVar1.OperationObjectCode = srcMidVar1.operationobjectcode

destMidVar1.PrimaryIdentity = srcMidVar1.primaryidentity

destMidVar1.BeginTime = formatDate(srcMidVar1.begintime, Constant4Model.DATE_FORMAT)

destMidVar1.EndTime = formatDate(srcMidVar1.endtime, Constant4Model.DATE_FORMAT)

destMidVar1.Status = srcMidVar1.status

destMidVar1.ResultCode = srcMidVar1.resultcode

destMidVar1.ResultDescription = srcMidVar1.resultdescription

destMidVar1.OperatorID = srcMidVar1.operatorid

destMidVar1.DepartmentID = srcMidVar1.departmentid

mappingList(srcMidVar1.simpleProperty,destMidVar1.AdditionalProperty,listMapping0)

destMidVar2.RequestMsg = srcMidVar1.requestMsg

destMidVar2.ResponseMsg = srcMidVar1.responseMsg







