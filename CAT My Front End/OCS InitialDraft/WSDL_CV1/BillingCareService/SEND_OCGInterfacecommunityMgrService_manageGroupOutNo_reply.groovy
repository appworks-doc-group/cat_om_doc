def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.resultCode = src.ResultCode
	
	dest.resultDesc = src.ResultDesc
	
	dest.version = src.Version
	
}

def srcMidVar = srcReturn.ManageGroupOutNoResultMsg

listMapping0.call(srcMidVar.ResultHeader,destReturn.resultHeader)

destReturn._class = "com.huawei.ngcbs.cm.common.ws.client.io.om.OMCommonResult"
