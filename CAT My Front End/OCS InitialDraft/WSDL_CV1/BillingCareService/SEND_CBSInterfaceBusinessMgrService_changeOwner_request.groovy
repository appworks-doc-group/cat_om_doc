import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("CBSInterfaceBusinessMgrService","changeOwner")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs12ws.subscriber.changeowner.ChangeOwnerRequest12Base"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Id
	
	dest.value = src.Value
	
}

def destMidVar = destArgs1.addressInfo

def srcMidVar = srcArgs0.ChangeOwnerRequestMsg.ChangeOwnerRequest

destMidVar.addr1 = srcMidVar.Address

def destMidVar0 = destArgs1.changeCustBasicInfo.individualInfo

destMidVar0.birthday = parseDate(srcMidVar.Birthday,Constant4Model.DAY_FORMAT)

destArgs1.changeMode = srcMidVar.ChangeMode

destMidVar0.email = srcMidVar.Email

destMidVar0.gender = srcMidVar.Gender

def destMidVar1 = destArgs1.changeCustBasicInfo.customerInfo

destMidVar1.custLevel = srcMidVar.Grade

destMidVar0.idNumber = srcMidVar.IdCode

destMidVar0.idType = srcMidVar.IdType

destMidVar0.middleName = srcMidVar.Name

def destMidVar2 = destArgs1.changeCustBasicInfo

mappingList(srcMidVar.SimpleProperty,destMidVar2.indvProperties,listMapping0)

def destMidVar3 = destArgs1.custAccessCode

destMidVar3.primaryIdentity = srcMidVar.SubscriberNo

destMidVar.postCode = srcMidVar.ZipCode

def srcMidVar0 = srcArgs0.ChangeOwnerRequestMsg.RequestHeader

destArgs0.beId = srcMidVar0.TenantId

destArgs0.additionInfo = srcMidVar0.additionInfo

destArgs0.belToAreaId = srcMidVar0.BelToAreaID

destArgs0.commandId = srcMidVar0.CommandId

destArgs0.currentCell = srcMidVar0.currentCell

destArgs0.interFrom = srcMidVar0.InterFrom

destArgs0.interMedi = srcMidVar0.InterMedi

destArgs0.interMode = srcMidVar0.InterMode

destArgs0.operatorId = srcMidVar0.OperatorID

destArgs0.partnerId = srcMidVar0.PartnerID

destArgs0.partnerOperId = srcMidVar0.PartnerOperID

destArgs0.remark = srcMidVar0.Remark

destArgs0.requestType = srcMidVar0.RequestType

destArgs0.reserve2 = srcMidVar0.Reserve2

destArgs0.reserve3 = srcMidVar0.Reserve3

destArgs0.sequenceId = srcMidVar0.SequenceId

destArgs0.messageSeq = srcMidVar0.SerialNo

def srcMidVar1 = srcArgs0.ChangeOwnerRequestMsg.RequestHeader.SessionEntity

destArgs0.remoteAddress = srcMidVar1.RemoteAddress

destArgs0.password = srcMidVar1.Password

destArgs0.loginSystem = srcMidVar1.Name

destArgs0.thirdPartyId = srcMidVar0.ThirdPartyID

destArgs0.tradePartnerId = srcMidVar0.TradePartnerID

destArgs0.transactionId = srcMidVar0.TransactionId

destArgs0.version = srcMidVar0.Version

destArgs0.visitArea = srcMidVar0.visitArea
