import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.account.batch.changeacctoffering.io.BatchChangeAcctOfferingRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.code = src.SubPropCode
	
	dest.value = src.Value
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.effDate=parseDate(src.EffectiveTime, Constant4Model.DATE_FORMAT)
	
	dest.expDate=parseDate(src.ExpirationTime, Constant4Model.DATE_FORMAT)
	
	def destMidVar1 = dest.property
	
	destMidVar1.propCode = src.PropCode
	
	destMidVar1.complexFlag = src.PropType
	
	destMidVar1.value = src.Value
	
	mappingList(src.SubPropInst,destMidVar1.subProps,listMapping5)
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping9

listMapping9 = 
{
    src,dest  ->

	dest.value = src.Value
	
	dest.code = src.SubPropCode
	
}

def listMapping8

listMapping8 = 
{
    src,dest  ->

	dest.effDate=parseDate(src.EffectiveTime, Constant4Model.DATE_FORMAT)
	
	dest.expDate=parseDate(src.ExpirationTime, Constant4Model.DATE_FORMAT)
	
	def destMidVar3 = dest.property
	
	destMidVar3.propCode = src.PropCode
	
	destMidVar3.complexFlag = src.PropType
	
	destMidVar3.value = src.Value
	
	mappingList(src.SubPropInst,destMidVar3.subProps,listMapping9)
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	def destMidVar2 = dest.productInst
	
	destMidVar2.networkType = src.NetworkType
	
	destMidVar2.packageFlag = src.PackageFlag
	
	destMidVar2.parentProdId = src.ParentProdID
	
	destMidVar2.primaryFlag = src.PrimaryFlag
	
	destMidVar2.prodId = src.ProductID
	
	destMidVar2.productType = src.ProductType
	
	mappingList(src.PInstProperty,dest.properties,listMapping8)
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	def destMidVar0 = dest.offeringInst
	
	def srcMidVar6 = src.ActivationTime
	
	destMidVar0.activeMode = srcMidVar6.Mode
	
	destMidVar0.activeTime=parseDate(srcMidVar6.ActiveTime, Constant4Model.DATE_FORMAT)
	
	destMidVar0.activeTimeLimit=parseDate(srcMidVar6.ActiveTimeLimit, Constant4Model.DATE_FORMAT)
	
	destMidVar0.bundleFlag = src.BundledFlag
	
	def srcMidVar7 = src.EffectiveTime
	
	dest.effMode = srcMidVar7.Mode
	
	dest.effDate=parseDate(srcMidVar7.Time, Constant4Model.DATE_FORMAT)
	
	dest.expDate=parseDate(src.ExpirationTime, Constant4Model.DATE_FORMAT)
	
	destMidVar0.offeringClass = src.OfferingClass
	
	destMidVar0.primaryFlag = "S"
	
	listMapping2.call(src.OfferingKey,destMidVar0.offeringKey)
	
	listMapping3.call(src.ParentOfferingKey,destMidVar0.parentOfferingKey)
	
	mappingList(src.OInstProperty,dest.properties,listMapping4)
	
	listMapping6.call(src.RelGOfferingKey,destMidVar0.relGOfferingKey)
	
	destMidVar0.status = src.Status
	
	destMidVar0.trialEndTime=parseDate(src.TrialEndTime, Constant4Model.DATE_FORMAT)
	
	destMidVar0.trialStartTime=parseDate(src.TrialStartTime, Constant4Model.DATE_FORMAT)
	
	mappingList(src.ProductInst,dest.productInsts,listMapping7)
	
}

def listMapping11

listMapping11 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping10

listMapping10 = 
{
    src,dest  ->

	listMapping11.call(src.OfferingKey,dest.offeringKey)
	
	dest.expMode = "I"
	
}

def listMapping13

listMapping13 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping12

listMapping12 = 
{
    src,dest  ->

	dest.effDate=parseDate(src.NewEffectiveTime, Constant4Model.DATE_FORMAT)
	
	def srcMidVar8 = src.NewExpirationTime
	
	dest.expMode = srcMidVar8.Mode
	
	dest.expDate=parseDate(srcMidVar8.Time, Constant4Model.DATE_FORMAT)
	
	listMapping13.call(src.OfferingKey,dest.offeringKey)
	
}

def srcMidVar = srcArgs0.BatchChangeAcctOfferingRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.BatchChangeAcctOfferingRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.BatchChangeAcctOfferingRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.BatchChangeAcctOfferingRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

destArgs0.version = srcMidVar.Version

def srcMidVar3 = srcArgs0.BatchChangeAcctOfferingRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

def srcMidVar4 = srcArgs0.BatchChangeAcctOfferingRequestMsg.BatchChangeAcctOfferingRequest

destArgs1.requestFileName = srcMidVar4.FileName

def srcMidVar5 = srcArgs0.BatchChangeAcctOfferingRequestMsg.BatchChangeAcctOfferingRequest.AcctOffering

def destMidVar = destArgs1.changeAcctOfferingReqeust

mappingList(srcMidVar5.AddOffering,destMidVar.addOfferingInstInfo,listMapping1)

mappingList(srcMidVar5.DelOffering,destMidVar.delOfferingInstInfo,listMapping10)

mappingList(srcMidVar5.ModifyOffering,destMidVar.modifyOfferingInstInfo,listMapping12)
