import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.ProductName = src.productName
	
	dest.ProductCode = src.productCode
	
	dest.ProductId = src.productId
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.Name = src.name
	
	dest.Id = src.id
	
	dest.Code = src.code
	
	dest.Description = src.description
	
}

def srcMidVar = srcReturn.resultHeader

def destMidVar = destReturn.QueryOfferingInfoResultMsg.ResultHeader

addingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.Version = srcMidVar.version

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.MessageSeq = srcMidVar.messageSeq

def destMidVar0 = destReturn.QueryOfferingInfoResultMsg.QueryOfferingInfoResult

def srcMidVar0 = srcReturn.queryOfferingInfoResult

destMidVar0.OfferingID = srcMidVar0.realOfferingId

destMidVar0.OfferingName = srcMidVar0.offeringName

destMidVar0.OfferingCode = srcMidVar0.offeringCode

destMidVar0.Description = srcMidVar0.description

destMidVar0.PrimaryFlag = srcMidVar0.primaryFlag

destMidVar0.BundleFlag = srcMidVar0.bundleFlag

destMidVar0.PaymentMode = srcMidVar0.paymentMode

destMidVar0.EffDate = formatDate(srcMidVar0.effDate, Constant4Model.DATE_FORMAT)

destMidVar0.ExpDate = formatDate(srcMidVar0.expDate, Constant4Model.DATE_FORMAT)

destMidVar0.ProductList = srcMidVar0.productList

def srcMidVar1 = srcReturn.queryOfferingInfoResult.productList

def destMidVar1 = destReturn.QueryOfferingInfoResultMsg.QueryOfferingInfoResult.ProductList

addingList(srcMidVar1.productList,destMidVar1.Product,listMapping1)

destMidVar0.PricePlanList = srcMidVar0.pricePlanList

def srcMidVar2 = srcReturn.queryOfferingInfoResult.pricePlanList

def destMidVar2 = destReturn.QueryOfferingInfoResultMsg.QueryOfferingInfoResult.PricePlanList

addingList(srcMidVar2.pricePlanList,destMidVar2.PricePlan,listMapping2)
