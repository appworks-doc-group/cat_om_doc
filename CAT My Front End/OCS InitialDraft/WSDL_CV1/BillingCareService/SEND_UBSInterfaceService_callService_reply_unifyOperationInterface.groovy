def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.keyOfEntity = src.key
	
	dest.valueOfEntity = src.value
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.valueOfItem = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.keyOfEntity = src.key
	
	dest.valueOfEntity = src.value
	
	mappingList(src.entity,dest.entityListOfEntity,listMapping3)
	
	mappingList(src.item,dest.itemListOfEntity,listMapping4)
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.valueOfItem = src.value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	mappingList(src.entity,dest.entityListOfEntity,listMapping1)
	
	mappingList(src.item,dest.itemListOfEntity,listMapping2)
	
	dest.keyOfEntity = src.key
	
	dest.valueOfEntity = src.value
	
}

def destMidVar = destReturn.resultHeader

def srcMidVar = srcReturn.UnifyOperationResult.resultHeader

destMidVar.beId = srcMidVar.beId

destReturn.resultCode = srcMidVar.resultCode

destReturn.resultDesc = srcMidVar.resultDesc

destMidVar.transactionId = srcMidVar.transactionId

destMidVar.version = srcMidVar.version

def srcMidVar0 = srcReturn.UnifyOperationResult.resultBody

mappingList(srcMidVar0.param,destReturn.resultBody,listMapping0)

destReturn._class = "com.huawei.ngcbs.cm.common.ws.client.io.ucm.UnifyOperationWSResult"
