def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.NewGroupResultMsg.ResultHeader

destMidVar.CommandId = srcReturn.resultHeader.commandId

destMidVar.TransactionId = srcReturn.resultHeader.transactionId

destMidVar.ResultCode = srcReturn.resultHeader.resultCode

destMidVar.Version = srcReturn.resultHeader.version

destMidVar.ResultDesc = srcReturn.resultHeader.resultDesc

destMidVar.SequenceId = srcReturn.resultHeader.sequenceId

destMidVar.OrderId = srcReturn.resultHeader.orderId

destMidVar.OperationTime = srcReturn.resultHeader.operationTime