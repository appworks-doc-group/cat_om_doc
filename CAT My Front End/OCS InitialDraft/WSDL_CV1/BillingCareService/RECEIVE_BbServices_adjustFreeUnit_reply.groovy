def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.FreeUnitInstanceID = src.freeUnitInstanceId
	
	dest.FreeUnitType = src.freeUnitType
	
	dest.NewAmount = src.newAmount
	
	dest.OldAmount = src.oldAmount
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.MsgLanguageCode = src.msgLanguageCode
	
	dest.ResultCode = src.resultCode
	
	dest.ResultDesc = src.resultDesc
	
	dest.Version = src.version

	dest.MessageSeq = src.messageSeq
	
	mappingList(src.simpleProperty,dest.AdditionalProperty,listMapping2)
	
}

def srcMidVar = srcReturn.freeUnitItem

def destMidVar = destReturn.AdjustFreeUnitResultMsg.AdjustFreeUnitResult

mappingList(srcMidVar.freeUnitItem,destMidVar.FreeUnitItemDetail,listMapping0)

def destMidVar0 = destReturn.AdjustFreeUnitResultMsg

listMapping1.call(srcReturn.resultHeader,destMidVar0.ResultHeader)
