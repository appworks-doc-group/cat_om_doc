import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.ParamCode = src.paramCode
	
	dest.ParamValue = src.paramValue
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.LimitType = src.limitType
	
	dest.LimitTypeName = src.limitTypeName
	
	dest.Amount = src.amount
	
	dest.EndDay = formatDate(src.endDay, Constant4Model.DATE_FORMAT)
	
	dest.BeginDate = formatDate(src.beginDate, Constant4Model.DATE_FORMAT)
	
	dest.MeasureType = src.measureType
	
	dest.MeasureID = src.measureId
	
	dest.CurrencyID = src.currencyId
	
	dest.LimitCtrlType = src.limitCtrlType
	
	dest.UsageAmount = src.usageAmount

	dest.EffectiveTime = formatDate(src.effectiveTime, Constant4Model.DATE_FORMAT)

	dest.ExpireTime = formatDate(src.expireTime, Constant4Model.DATE_FORMAT)

	dest.LimitCtrlAction = src.limitCtrlAction

	addingList(src.limitParam,dest.LimitParam,listMapping2)
	
}

def destMidVar = destReturn.QueryConsumptionLimitResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.Version = srcMidVar.version

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.MessageSeq = srcMidVar.messageSeq

addingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def srcMidVar0 = srcReturn.limitUsageList

def destMidVar0 = destReturn.QueryConsumptionLimitResultMsg.QueryConsumptionLimitResult

addingList(srcMidVar0.limitUsageList,destMidVar0.LimitUsageList,listMapping1)
