def srcReturn = src.payload._return
def destReturn = dest.payload._return

def srcResultHeader = srcReturn.resultHeader

def destMidVar = destReturn.queryPricePlanResponse.QueryPricePlanResult.ResultMessage.MessageHeader
def destMessageBody = destReturn.queryPricePlanResponse.QueryPricePlanResult.ResultMessage.MessageBody

destMidVar.CommandId = srcResultHeader.commandId
destMidVar.Version = srcResultHeader.version
destMidVar.TransactionId = srcResultHeader.transactionId
destMidVar.SequenceId = srcResultHeader.sequenceId
destMidVar.ResultCode = srcResultHeader.resultCode
destMidVar.ResultDesc = srcResultHeader.resultDesc

def listMapping0

listMapping0 =
{
    src,dest  ->

    dest.ID = src.id
    
    dest.Name = src.name
	
	dest.PricePlanType = src.pricePlanType
	
	dest.SubscribeType = src.subscribeType
}

mappingList(srcReturn.resultBody.ocs11QueryPricePlan,destMessageBody.PricePlanList.PricePlan,listMapping0)