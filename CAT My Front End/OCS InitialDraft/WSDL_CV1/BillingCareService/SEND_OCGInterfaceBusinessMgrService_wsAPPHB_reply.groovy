def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.resultHeader

def srcMidVar = srcReturn.WSAPPHBResponseMsg.ResultHeader

destMidVar.resultCode = srcMidVar.ResultCode

destMidVar.resultDesc = srcMidVar.ResultDesc

destMidVar.version = srcMidVar.Version

def srcMidVar0 = srcReturn.WSAPPHBResponseMsg.WSAPPHBResponse

def destMidVar0 = destReturn.wsapphbResponse

destMidVar0.resultCode = srcMidVar0.ResultCode

destMidVar0.resultInfo = srcMidVar0.ResultInfo

destReturn._class = "com.huawei.ngcbs.cm.common.ws.client.io.om.OMApphbResult"
