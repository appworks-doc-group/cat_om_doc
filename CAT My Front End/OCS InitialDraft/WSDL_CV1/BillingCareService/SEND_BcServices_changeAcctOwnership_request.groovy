import com.huawei.ngcbs.bm.common.common.Constant4Model


dest.setServiceOperation("SubscriberService","changeAcctOwnershipWithResult")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.changeownership.io.ChangeAcctOwnershipRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.postCode = src.PostCode
	
	dest.addr1 = src.Address1
	
	dest.addr10 = src.Address10
	
	dest.addr11 = src.Address11
	
	dest.addr12 = src.Address12
	
	dest.addr2 = src.Address2
	
	dest.addr3 = src.Address3
	
	dest.addr4 = src.Address4
	
	dest.addr5 = src.Address5
	
	dest.addr6 = src.Address6
	
	dest.addr7 = src.Address7
	
	dest.addr8 = src.Address8
	
	dest.addr9 = src.Address9
	
	dest.tpAddrKey = src.AddressKey
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.newObjKey = src.NewPayRelationKey
	
	dest.oldObjKey = src.OldPayRelationKey
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	def destMidVar2 = dest.newObjKey
	
	def srcMidVar11 = src.NewOfferingKey
	
	destMidVar2.oId = srcMidVar11.OfferingID
	
	destMidVar2.oCode = srcMidVar11.OfferingCode
	
	destMidVar2.pSeq = srcMidVar11.PurchaseSeq
	
	def destMidVar3 = dest.oldObjKey
	
	def srcMidVar12 = src.OldOfferingKey
	
	destMidVar3.oId = srcMidVar12.OfferingID
	
	destMidVar3.oCode = srcMidVar12.OfferingCode
	
	destMidVar3.pSeq = srcMidVar12.PurchaseSeq
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	def srcMidVar13 = src.OfferingKey
	
	dest.oId = srcMidVar13.OfferingID
	
		dest.oCode = srcMidVar13.OfferingCode
	
	dest.pSeq = srcMidVar13.PurchaseSeq
	
}

def listMapping11

listMapping11 = 
{
    src,dest  ->

	dest.newObjKey = src.NewAutoPayChannelKey
	
	dest.oldObjKey = src.OldAutoPayChannelKey
	
}

def listMapping12

listMapping12 = 
{
    src,dest  ->

	dest.channelType = src.ChannelType
	
	dest.noticeType = src.NoticeType
	
	dest.subNoticeType = src.SubNoticeType
	
}

def listMapping13

listMapping13 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping14

listMapping14 = 
{
    src,dest  ->

		dest.birthday=parseDate(src.Birthday,Constant4Model.DATE_FORMAT)
	
	dest.addrKey = src.HomeAddressKey
	
	dest.education = src.Education
	
	dest.email = src.Email
	
	dest.fax = src.Fax
	
	dest.firstName = src.FirstName
	
	dest.gender = src.Gender
	
	dest.homePhone = src.HomePhone
	
	dest.idNumber = src.IDNumber
	
	dest.idType = src.IDType
	
	dest.idValidity=parseDate(src.IDValidity,Constant4Model.DATE_FORMAT)
	
	dest.lastName = src.LastName
	
	dest.marriedStatus = src.MaritalStatus
	
	dest.middleName = src.MiddleName
	
	dest.mobilePhone = src.MobilePhone
	
	dest.nationality = src.Nationality
	
	dest.nativePlace = src.NativePlace
	
	dest.occupation = src.Occupation
	
	dest.officePhone = src.OfficePhone
	
	dest.race = src.Race
	
	dest.salary = src.Salary
	
	dest.title = src.Title
	
}

def listMapping15

listMapping15 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping16

listMapping16 = 
{
    src,dest  ->

	dest.idNumber = src.IDNumber
	
	dest.addrKey = src.OrgAddressKey
	
	dest.idType = src.IDType
	
	dest.idValidity = src.IDValidity
	
	dest.industry = src.Industry
	
	dest.orgEmail = src.OrgEmail
	
	dest.orgFax = src.OrgFaxNumber
	
	dest.orgLevel = src.OrgLevel
	
	dest.orgName = src.OrgName
	
	dest.orgPhone = src.OrgPhoneNumber
	
	dest.orgSName = src.OrgShortName
	
	dest.sizeLevel = src.OrgSize
	
	dest.orgType = src.OrgType
	
	dest.orgWeb = src.OrgWebSite
	
	dest.subIndustry = src.SubIndustry
	
}

def listMapping17

listMapping17 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping18

listMapping18 = 
{
    src,dest  ->

	dest.channelType = src.ChannelType
	
	dest.noticeType = src.NoticeType
	
	dest.subNoticeType = src.SubNoticeType
	
}

def listMapping19

listMapping19 = 
{
    src,dest  ->

	dest.custLevel = src.CustLevel
	
	dest.custLoyalty = src.CustLoyalty
	
	dest.custSegment = src.CustSegment
	
	dest.billCycleType = src.DFTBillCycleType
	
	dest.currencyId = src.DFTCurrencyID
	
	dest.custPLang = src.DFTIVRLang
	
	dest.custPwd = src.DFTPwd
	
	dest.custWLang = src.DFTWrittenLang
	
	dest.dunningFlag = src.DunningFlag
	
}

def listMapping20

listMapping20 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping21

listMapping21 = 
{
    src,dest  ->

	dest.education = src.Education
	
	dest.email = src.Email
	
	dest.fax = src.Fax
	
	dest.firstName = src.FirstName
	
	dest.gender = src.Gender
	
	dest.homePhone = src.HomePhone
	
	dest.idNumber = src.IDNumber
	
	dest.idType = src.IDType
	
	dest.lastName = src.LastName
	
	dest.middleName = src.MiddleName
	
	dest.mobilePhone = src.MobilePhone
	
	dest.nationality = src.Nationality
	
	dest.nativePlace = src.NativePlace
	
	dest.occupation = src.Occupation
	
	dest.officePhone = src.OfficePhone
	
	dest.race = src.Race
	
	dest.title = src.Title
	
	dest.salary = src.Salary
	
	dest.addrKey = src.HomeAddressKey
	
		dest.birthday=parseDate(src.Birthday,Constant4Model.DATE_FORMAT)
	
	dest.idValidity=parseDate(src.IDValidity,Constant4Model.DATE_FORMAT)
	
	dest.marriedStatus = src.MaritalStatus
	
}

def listMapping22

listMapping22 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping23

listMapping23 = 
{
    src,dest  ->

	dest.idNumber = src.IDNumber
	
	dest.idType = src.IDType
	
	dest.industry = src.Industry
	
	dest.orgEmail = src.OrgEmail
	
	dest.orgLevel = src.OrgLevel
	
	dest.orgName = src.OrgName
	
	dest.orgType = src.OrgType
	
	dest.subIndustry = src.SubIndustry
	
	dest.orgWeb = src.OrgWebSite
	
	dest.sizeLevel = src.OrgSize
	
	dest.orgSName = src.OrgShortName
	
	dest.orgPhone = src.OrgPhoneNumber
	
	dest.orgFax = src.OrgFaxNumber
	
	dest.addrKey = src.OrgAddressKey
	
	dest.idValidity=parseDate(src.IDValidity,Constant4Model.DATE_FORMAT)
	
}

def listMapping24

listMapping24 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping27

listMapping27 = 
{
    src,dest  ->

	dest.code = src.SubPropCode
	
	dest.value = src.Value
	
}

def listMapping26

listMapping26 = 
{
    src,dest  ->

	dest.effDate=parseDate(src.EffectiveTime,Constant4Model.DATE_FORMAT)
	
	dest.expDate=parseDate(src.ExpirationTime,Constant4Model.DATE_FORMAT)
	
	def destMidVar15 = dest.property
	
	destMidVar15.propCode = src.PropCode
	
	destMidVar15.complexFlag = src.PropType
	
	destMidVar15.value = src.Value
	
	mappingList(src.SubPropInst,destMidVar15.subProps,listMapping27)
	
}

def listMapping30

listMapping30 = 
{
    src,dest  ->

	dest.code = src.SubPropCode
	
	dest.value = src.Value
	
}

def listMapping29

listMapping29 = 
{
    src,dest  ->

	dest.effDate=parseDate(src.EffectiveTime,Constant4Model.DATE_FORMAT)
	
	dest.expDate=parseDate(src.ExpirationTime,Constant4Model.DATE_FORMAT)
	
	def destMidVar17 = dest.property
	
	destMidVar17.propCode = src.PropCode
	
	destMidVar17.complexFlag = src.PropType
	
	destMidVar17.value = src.Value
	
	mappingList(src.SubPropInst,destMidVar17.subProps,listMapping30)
	
}

def listMapping28

listMapping28 = 
{
    src,dest  ->

	def destMidVar16 = dest.productInst
	
	destMidVar16.networkType = src.NetworkType
	
	destMidVar16.packageFlag = src.PackageFlag
	
	destMidVar16.parentProdId = src.ParentProdID
	
	destMidVar16.primaryFlag = src.PrimaryFlag
	
	destMidVar16.prodId = src.ProductID
	
	destMidVar16.productType = src.ProductType
	
	mappingList(src.PInstProperty,dest.properties,listMapping29)
	
}

def listMapping25

listMapping25 = 
{
    src,dest  ->

	def srcMidVar25 = src.EffectiveTime
	
	dest.effMode = srcMidVar25.Mode
	
	dest.effDate=parseDate(srcMidVar25.Time,Constant4Model.DATE_FORMAT)
	
	dest.expDate=parseDate(src.ExpirationTime,Constant4Model.DATE_FORMAT)
	
	dest.offeringInst.activeTime=parseDate(src.ActivationTime.ActiveTime,Constant4Model.DATE_FORMAT)
	
	dest.offeringInst.activeTimeLimit=parseDate(src.ActivationTime.ActiveTimeLimit,Constant4Model.DATE_FORMAT)
	
	def destMidVar11 = dest.offeringInst
	
	def srcMidVar26 = src.ActivationTime
	
	destMidVar11.activeMode = srcMidVar26.Mode
	
	destMidVar11.bundleFlag = src.BundledFlag
	
	destMidVar11.offeringClass = src.OfferingClass
	
	def destMidVar12 = dest.offeringInst.offeringKey
	
	def srcMidVar27 = src.OfferingKey
	
	destMidVar12.oId = srcMidVar27.OfferingID
	
	destMidVar12.oCode = srcMidVar27.OfferingCode
	
	destMidVar12.pSeq = srcMidVar27.PurchaseSeq
	
	def destMidVar13 = dest.offeringInst.parentOfferingKey
	
	def srcMidVar28 = src.ParentOfferingKey
	
	destMidVar13.oId = srcMidVar28.OfferingID
	
		destMidVar13.oCode = srcMidVar28.OfferingCode
	
	destMidVar13.pSeq = srcMidVar28.PurchaseSeq
	
	def destMidVar14 = dest.offeringInst.relGOfferingKey
	
	def srcMidVar29 = src.RelGOfferingKey
	
	destMidVar14.oId = srcMidVar29.OfferingID
	
	destMidVar14.oCode = srcMidVar29.OfferingCode
	
	destMidVar14.pSeq = srcMidVar29.PurchaseSeq
	
	destMidVar11.status = src.Status
	
	destMidVar11.trialEndTime=parseDate(src.TrialEndTime,Constant4Model.DATE_FORMAT)
	
	destMidVar11.trialStartTime=parseDate(src.TrialStartTime,Constant4Model.DATE_FORMAT)
	
	mappingList(src.OInstProperty,dest.properties,listMapping26)
	
	mappingList(src.ProductInst,dest.productInsts,listMapping28)
	
}

def srcMidVar = srcArgs0.ChangeAcctOwnershipRequestMsg.RequestHeader

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

def srcMidVar0 = srcArgs0.ChangeAcctOwnershipRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.ChangeAcctOwnershipRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.ChangeAcctOwnershipRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.ChangeAcctOwnershipRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar.Version

def destMidVar = destArgs1.subAccessCode

def srcMidVar4 = srcArgs0.ChangeAcctOwnershipRequestMsg.ChangeAcctOwnershipRequest.OldOwnership

destMidVar.subscriberKey = srcMidVar4.SubscriberKey

def srcMidVar5 = srcArgs0.ChangeAcctOwnershipRequestMsg.ChangeAcctOwnershipRequest.NewOwnership

mappingList(srcMidVar5.ControlProperty,destArgs1.controlProperties,listMapping1)

def destMidVar0 = destArgs1.changeAcctOwnershipInfo.oriSubDftAcct

def srcMidVar6 = srcArgs0.ChangeAcctOwnershipRequestMsg.ChangeAcctOwnershipRequest.OldOwnership.Account

destMidVar0.postpaidAcctKey = srcMidVar6.PostpaidAcctKey

destMidVar0.prepaidAcctKey = srcMidVar6.PrepaidAcctKey

def srcMidVar7 = srcArgs0.ChangeAcctOwnershipRequestMsg.ChangeAcctOwnershipRequest.NewOwnership.RegisterCustomer

destArgs1.opType = srcMidVar7.OpType

mappingList(srcMidVar5.AddressInfo,destArgs1.addresses,listMapping2)

def destMidVar1 = destArgs1.changeSubOwnershipInfo

def srcMidVar8 = srcArgs0.ChangeAcctOwnershipRequestMsg.ChangeAcctOwnershipRequest.NewOwnership.Subscriber

destMidVar1.newSubKey = srcMidVar8.SubscriberKey

destMidVar1.userCustKey = srcMidVar8.UserCustomerKey

def srcMidVar9 = srcArgs0.ChangeAcctOwnershipRequestMsg.ChangeAcctOwnershipRequest.NewOwnership.Subscriber.SubPayRelation

mappingList(srcMidVar9.ShiftPayRelation,destMidVar1.shiftPayRelations,listMapping3)

def srcMidVar10 = srcArgs0.ChangeAcctOwnershipRequestMsg.ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering

mappingList(srcMidVar10.ShiftOffering,destMidVar1.shiftSuppOfferings,listMapping4)

mappingList(srcMidVar10.DelOffering,destMidVar1.delSuppOfferings,listMapping5)

destMidVar1.addSuppOfferings[0].offeringInst.activeTime=parseDate(srcMidVar5.SupplementaryOffering.AddOffering[0].ActivationTime.ActiveTime,Constant4Model.DATE_FORMAT)

destMidVar1.addSuppOfferings[0].offeringInst.activeTimeLimit=parseDate(srcMidVar5.SupplementaryOffering.AddOffering[0].ActivationTime.ActiveTimeLimit,Constant4Model.DATE_FORMAT)

destMidVar1.addSuppOfferings[0].effDate=parseDate(srcMidVar5.SupplementaryOffering.AddOffering[0].EffectiveTime.Time,Constant4Model.DATE_FORMAT)

destMidVar1.addSuppOfferings[0].expDate=parseDate(srcMidVar5.SupplementaryOffering.AddOffering[0].ExpirationTime,Constant4Model.DATE_FORMAT)

destMidVar1.addSuppOfferings[0].offeringInst.trialEndTime=parseDate(srcMidVar5.SupplementaryOffering.AddOffering[0].TrialEndTime,Constant4Model.DATE_FORMAT)

destMidVar1.addSuppOfferings[0].offeringInst.trialStartTime=parseDate(srcMidVar5.SupplementaryOffering.AddOffering[0].TrialStartTime,Constant4Model.DATE_FORMAT)

def destMidVar4 = destArgs1.changeSubOwnershipInfo.primaryOffering

def srcMidVar14 = srcArgs0.ChangeAcctOwnershipRequestMsg.ChangeAcctOwnershipRequest.NewOwnership.PrimaryOffering.NewOfferingKey

destMidVar4.oId = srcMidVar14.OfferingID

destMidVar4.oCode = srcMidVar14.OfferingCode

destMidVar4.pSeq = srcMidVar14.PurchaseSeq

def destMidVar5 = destArgs1.changeAcctOwnershipInfo.newSubDftAcct

def srcMidVar15 = srcArgs0.ChangeAcctOwnershipRequestMsg.ChangeAcctOwnershipRequest.NewOwnership.Account

destMidVar5.postpaidAcctKey = srcMidVar15.PostpaidAcctKey

destMidVar5.prepaidAcctKey = srcMidVar15.PrepaidAcctKey

def destMidVar6 = destArgs1.changeAcctOwnershipInfo

mappingList(srcMidVar15.AutoPayChannel,destMidVar6.shiftAutoPayChannels,listMapping11)

def srcMidVar16 = srcArgs0.ChangeAcctOwnershipRequestMsg.ChangeAcctOwnershipRequest.NewOwnership.RegisterCustomer.CustInfo

def destMidVar7 = destArgs1.regCustomerInfo

mappingList(srcMidVar16.NoticeSuppress,destMidVar7.noticeSuppresses,listMapping12)

def destMidVar8 = destArgs1.regCustomerInfo.customerInfo

def srcMidVar17 = srcArgs0.ChangeAcctOwnershipRequestMsg.ChangeAcctOwnershipRequest.NewOwnership.RegisterCustomer.CustInfo.CustBasicInfo

destMidVar8.custLevel = srcMidVar17.CustLevel

destMidVar8.custLoyalty = srcMidVar17.CustLoyalty

mappingList(srcMidVar17.CustProperty,destMidVar7.custProperties,listMapping13)

destMidVar8.custSegment = srcMidVar17.CustSegment

destMidVar8.billCycleType = srcMidVar17.DFTBillCycleType

destMidVar8.currencyId = srcMidVar17.DFTCurrencyID

destMidVar8.custPLang = srcMidVar17.DFTIVRLang

destMidVar8.custPwd = srcMidVar17.DFTPwd

destMidVar8.custWLang = srcMidVar17.DFTWrittenLang

destMidVar8.dunningFlag = srcMidVar17.DunningFlag

destMidVar8.custClass = srcMidVar16.CustClass

destMidVar8.custCode = srcMidVar16.CustCode

destMidVar8.custNodeType = srcMidVar16.CustNodeType

destMidVar8.custType = srcMidVar16.CustType

destMidVar8.parentCustKey = srcMidVar16.ParentCustKey

destMidVar8.tpCustKey = srcMidVar7.CustKey

listMapping14.call(srcMidVar7.IndividualInfo,destMidVar7.individualInfo)

def srcMidVar18 = srcArgs0.ChangeAcctOwnershipRequestMsg.ChangeAcctOwnershipRequest.NewOwnership.RegisterCustomer.IndividualInfo

mappingList(srcMidVar18.IndividualProperty,destMidVar7.indvProperties,listMapping15)

listMapping16.call(srcMidVar7.OrgInfo,destMidVar7.orgInfo)

def srcMidVar19 = srcArgs0.ChangeAcctOwnershipRequestMsg.ChangeAcctOwnershipRequest.NewOwnership.RegisterCustomer.OrgInfo

mappingList(srcMidVar19.OrgProperty,destMidVar7.orgProperties,listMapping17)

def srcMidVar20 = srcArgs0.ChangeAcctOwnershipRequestMsg.ChangeAcctOwnershipRequest.NewOwnership.UserCustomer.CustInfo

def destMidVar9 = destArgs1.userCustomerInfo

mappingList(srcMidVar20.NoticeSuppress,destMidVar9.noticeSuppresses,listMapping18)

listMapping19.call(srcMidVar20.CustBasicInfo,destMidVar9.customerInfo)

def srcMidVar21 = srcArgs0.ChangeAcctOwnershipRequestMsg.ChangeAcctOwnershipRequest.NewOwnership.UserCustomer.CustInfo.CustBasicInfo

mappingList(srcMidVar21.CustProperty,destMidVar9.custProperties,listMapping20)

def destMidVar10 = destArgs1.userCustomerInfo.customerInfo

destMidVar10.custCode = srcMidVar20.CustCode

destMidVar10.custClass = srcMidVar20.CustClass

destMidVar10.custNodeType = srcMidVar20.CustNodeType

destMidVar10.custType = srcMidVar20.CustType

destMidVar10.parentCustKey = srcMidVar20.ParentCustKey

def srcMidVar22 = srcArgs0.ChangeAcctOwnershipRequestMsg.ChangeAcctOwnershipRequest.NewOwnership.UserCustomer

destMidVar10.tpCustKey = srcMidVar22.CustKey

listMapping21.call(srcMidVar22.IndividualInfo,destMidVar9.individualInfo)

def srcMidVar23 = srcArgs0.ChangeAcctOwnershipRequestMsg.ChangeAcctOwnershipRequest.NewOwnership.UserCustomer.IndividualInfo

mappingList(srcMidVar23.IndividualProperty,destMidVar9.indvProperties,listMapping22)

listMapping23.call(srcMidVar22.OrgInfo,destMidVar9.orgInfo)

def srcMidVar24 = srcArgs0.ChangeAcctOwnershipRequestMsg.ChangeAcctOwnershipRequest.NewOwnership.UserCustomer.OrgInfo

mappingList(srcMidVar24.OrgProperty,destMidVar9.orgProperties,listMapping24)

destArgs0.interMode = srcMidVar.AccessMode

mappingList(srcMidVar10.AddOffering,destMidVar1.addSuppOfferings,listMapping25)

def destMidVar18 = destArgs1.changeSubBasicInfo.subscriberInfo

destMidVar18.ivrLang = srcMidVar8.IVRLang

destMidVar18.subPassword = srcMidVar8.SubPassword

destMidVar18.writtenLang = srcMidVar8.WrittenLang
