def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.resultHeader

def srcMidVar = srcReturn.QueryAttachedResultMsg.ResultHeader

destMidVar.resultCode = srcMidVar.ResultCode

destMidVar.resultDesc = srcMidVar.ResultDesc

destMidVar.version = srcMidVar.Version

def srcMidVar0 = srcReturn.QueryAttachedResultMsg.QueryAttachedResult

destReturn.displayNumber = srcMidVar0.DisplayNumber

destReturn.fraudState = srcMidVar0.FraudState

destReturn.fraudTimes = srcMidVar0.FraudTimes

destReturn.marketClass = srcMidVar0.MarketClass

destReturn.phoneType = srcMidVar0.PhoneType

destReturn.subscriberNo = srcMidVar0.SubscriberNo

destReturn.timeEnterFraud = srcMidVar0.TimeEnterFraud

destReturn.vipFlag = srcMidVar0.VIPFlag

destReturn._class = "com.huawei.ngcbs.cm.common.ws.client.io.om.OMQueryAttachedResult"
