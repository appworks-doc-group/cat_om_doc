import com.huawei.ngcbs.bm.common.common.Constant4Model

def destMsgHeader = dest.payload._args[0]
destMsgHeader._class = "com.huawei.ngcbs.cm.ocs33ws.core.bo.Ocs33MessageHeader"

def destReqData = dest.payload._args[1]
destReqData._class = "com.huawei.ngcbs.cm.ocs33ws.subscriber.subscribe.io.OCS33ChangeOptionalOfferRequest"

def srcReqDocMsg = src.payload._args[0]
def srcReqHeaderDoc = srcReqDocMsg.ChangeOptionalOfferRequestMsg.RequestHeader
def srcReqDataDoc = srcReqDocMsg.ChangeOptionalOfferRequestMsg.ChangeOptionalOfferRequest

destMsgHeader.commandId = srcReqHeaderDoc.CommandId
destMsgHeader.version = srcReqHeaderDoc.Version
destMsgHeader.transactionId = srcReqHeaderDoc.TransactionId
destMsgHeader.sequenceId = srcReqHeaderDoc.SequenceId
destMsgHeader.requestType = srcReqHeaderDoc.RequestType
destMsgHeader.loginSystem = srcReqHeaderDoc.SessionEntity.Name
destMsgHeader.password = srcReqHeaderDoc.SessionEntity.Password
destMsgHeader.remoteAddress = srcReqHeaderDoc.SessionEntity.RemoteAddress
destMsgHeader.interFrom = srcReqHeaderDoc.InterFrom
destMsgHeader.interMedi = srcReqHeaderDoc.InterMedi
destMsgHeader.interMode = srcReqHeaderDoc.InterMode
destMsgHeader.visitArea = srcReqHeaderDoc.visitArea
destMsgHeader.currentCell = srcReqHeaderDoc.currentCell
destMsgHeader.additionInfo = srcReqHeaderDoc.additionInfo
destMsgHeader.thirdPartyId = srcReqHeaderDoc.ThirdPartyID
destMsgHeader.partnerId = srcReqHeaderDoc.PartnerID
destMsgHeader.operatorId = srcReqHeaderDoc.OperatorID
destMsgHeader.tradePartnerId = srcReqHeaderDoc.TradePartnerID
destMsgHeader.partnerOperId = srcReqHeaderDoc.PartnerOperID
destMsgHeader.belToAreaId = srcReqHeaderDoc.BelToAreaID
destMsgHeader.reserve2 = srcReqHeaderDoc.Reserve2
destMsgHeader.reserve3 = srcReqHeaderDoc.Reserve3
destMsgHeader.messageSeq = srcReqHeaderDoc.SerialNo
destMsgHeader.remark = srcReqHeaderDoc.Remark
destMsgHeader.performanceStatCmd = "ChangeOptionalOffer"

destReqData.subscriberNo = srcReqDataDoc.SubscriberNo
destReqData.handlingChargeFlag = srcReqDataDoc.HandleChargeFlag
def listOfferOrderPropMapping = 
{
	src,dest  ->
	
	dest.id = src.Id
	dest.value = src.Value
}

def listOptionalOfferMapping = 
{
	src,dest  ->
	
	dest.offerId = src.Id
	dest.operationType = src.OperationType
	dest.offerOrderKey = src.OfferOrderKey
	dest.validMode.mode = src.ValidMode.Mode
	dest.validMode.effectiveDate = parseDate(src.ValidMode.EffectiveDate, Constant4Model.DATE_FORMAT)
	dest.validMode.expireDate = parseDate(src.ValidMode.ExpireDate, Constant4Model.DATE_FORMAT)
	dest.status = src.Status
	mappingList(src.SimpleProperty, dest.prodOrderProps, listOfferOrderPropMapping)
}

mappingList(srcReqDataDoc.OptionalOffer, destReqData.offerOrders, listOptionalOfferMapping)
