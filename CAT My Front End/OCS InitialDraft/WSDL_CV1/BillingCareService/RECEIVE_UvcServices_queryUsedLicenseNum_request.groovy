def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destMidVar = destArgs0.QueryUsedLicenseNumRequestMsg.RequestHeader.AccessSecurity

def srcMidVar = srcArgs0.messageHeader

destMidVar.LoginSystemCode = srcMidVar.loginSystem

destMidVar.Password = srcMidVar.password

destMidVar.RemoteIP = srcMidVar.remoteAddress

def destMidVar0 = destArgs0.QueryUsedLicenseNumRequestMsg.RequestHeader

destMidVar0.BusinessCode = srcMidVar.businessCode

destMidVar0.MessageSeq = srcMidVar.messageSeq

destMidVar0.MsgLanguageCode = srcMidVar.msgLanguageCode

def destMidVar1 = destArgs0.QueryUsedLicenseNumRequestMsg.RequestHeader.OperatorInfo

destMidVar1.ChannelID = srcMidVar.channelId

destMidVar1.OperatorID = srcMidVar.operatorId

def destMidVar2 = destArgs0.QueryUsedLicenseNumRequestMsg.RequestHeader.OwnershipInfo

destMidVar2.BEID = srcMidVar.beId

destMidVar2.BRID = srcMidVar.brId

def destMidVar3 = destArgs0.QueryUsedLicenseNumRequestMsg.RequestHeader.TimeFormat

destMidVar3.TimeType = srcMidVar.timeType

destMidVar3.TimeZoneID = srcMidVar.timeZoneId

destMidVar0.Version = srcMidVar.version

def destMidVar4 = destArgs0.QueryUsedLicenseNumRequestMsg.QueryUsedLicenseNumRequest

destMidVar4.LicenseItemCode = srcArgs0.licenseItemCode

