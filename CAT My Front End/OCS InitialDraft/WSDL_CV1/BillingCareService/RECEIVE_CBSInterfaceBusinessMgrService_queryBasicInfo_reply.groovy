def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Id = src.code
	
	dest.Value = src.value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Address = src.address
	
	dest.BillcycleType = src.billCycleType
	
	dest.Code = src.code
	
	dest.Name = src.name
	
	dest.RelationCustomer = src.relationCustomer
	
	mappingList(src.simplePropertys,dest.SimpleProperty,listMapping1)
	
	dest.Title = src.title

	def createSubscriberNoList = { localdest ->
	return { val,i  ->
		localdest.putAt(i,val.subscriberNo)
			}
	}

	forEachList(src.subscriberNoList, createSubscriberNoList(dest.SubscriberNo))
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.Id = src.code
	
	dest.Value = src.value
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.Id = src.code
	
	dest.Value = src.value
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.BillStatus = src.billStatus
	
	dest.CurCycleEndTime=formatDate(src.curCycleEndTime, "yyyyMMddHHmmss")
	
	dest.CurCycleStartTime=formatDate(src.curCycleStartTime, "yyyyMMddHHmmss")
	
	dest.EffectiveDate=formatDate(src.effectiveDate, "yyyyMMddHHmmss")
	
	dest.ExpiredDate=formatDate(src.expireDate, "yyyyMMddHHmmss")
	
	dest.Id = src.id
	
	dest.ProductOrderKey = src.productOrderKey
	
	addingList(src.simpleProperty,dest.SimpleProperty,listMapping4)
	
	dest.status = src.status
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.Id = src.code
	
	dest.Value = src.value
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.Id = src.id
	
	dest.RegistrationTime=formatDate(src.registrationTime, "yyyyMMddHHmmss")
	
	mappingList(src.simplePropertyList,dest.SimpleProperty,listMapping6)
	
	dest.Status = src.status
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.Id = src.code
	
	dest.Value = src.value
	
}

def listMapping8

listMapping8 = 
{
    src,dest  ->

	dest.Id = src.code
	
	dest.Value = src.value
	
}

def srcMidVar = srcReturn.resultHeader

srcMidVar._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12ResultHeader"

def destMidVar = destReturn.QueryBasicInfoResultMsg.ResultHeader

destMidVar.CommandId = srcMidVar.commandId

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.SequenceId = srcMidVar.sequenceId

destMidVar.SerialNo = srcMidVar.serialNo

destMidVar.TransactionId = srcMidVar.transactionId

destMidVar.Version = srcMidVar.version

def srcMidVar0 = srcReturn.resultBody

srcMidVar0._class = "com.huawei.ngcbs.cm.ocs12ws.subscriber.querybasicinfo.io.QueryBasicInfoResult"

def destMidVar0 = destReturn.QueryBasicInfoResultMsg.QueryBasicInfoResult

mappingList(srcMidVar0.accountInfos,destMidVar0.Account,listMapping0)

def destMidVar1 = destReturn.QueryBasicInfoResultMsg.QueryBasicInfoResult.Customer

def srcMidVar1 = srcReturn.resultBody.customerInfo

destMidVar1.Address = srcMidVar1.address

destMidVar1.BelToAreaID = srcMidVar1.beToAreaID

destMidVar1.Birthday = srcMidVar1.birthday

destMidVar1.Code = srcMidVar1.code

destMidVar1.Country = srcMidVar1.country

destMidVar1.CreditAmount = srcMidVar1.creditAmount

destMidVar1.CreditGrade = srcMidVar1.creditGrade

destMidVar1.CustomerState = srcMidVar1.customerState

destMidVar1.CustomerType = srcMidVar1.customerType

destMidVar1.Education = srcMidVar1.educaion

destMidVar1.Email = srcMidVar1.email

destMidVar1.Gender = srcMidVar1.gender

destMidVar1.Grade = srcMidVar1.grade

destMidVar1.IdCode = srcMidVar1.idCode

destMidVar1.IdType = srcMidVar1.idType

destMidVar1.JobType = srcMidVar1.jobType

destMidVar1.MaritalStatus = srcMidVar1.maritalStatus

destMidVar1.Name = srcMidVar1.name

destMidVar1.NationType = srcMidVar1.nationType

destMidVar1.NativePlace = srcMidVar1.nativePlace

destMidVar1.RegistrationTime = srcMidVar1.registrationTime

mappingList(srcMidVar1.simplePropertyList,destMidVar1.SimpleProperty,listMapping2)

destMidVar1.Skill = srcMidVar1.skill

destMidVar1.SocialNo = srcMidVar1.socialNo

destMidVar1.ZipCode = srcMidVar1.zipCode

def srcMidVar2 = srcReturn.resultBody.subscriberInfo

def destMidVar2 = destReturn.QueryBasicInfoResultMsg.QueryBasicInfoResult.Subscriber

mappingList(srcMidVar2.productList,destMidVar2.Product,listMapping3)

mappingList(srcMidVar2.serviceList,destMidVar2.Service,listMapping5)

def destMidVar3 = destReturn.QueryBasicInfoResultMsg.QueryBasicInfoResult.Subscriber.Subscriber

def srcMidVar3 = srcReturn.resultBody.subscriberInfo.subscriberBasic

destMidVar3.BelToAreaID = srcMidVar3.belToAreaId

destMidVar3.BrandId = srcMidVar3.brandId

destMidVar3.Code = srcMidVar3.code

destMidVar3.IMSI = srcMidVar3.imsi

destMidVar3.InitialCredit = srcMidVar3.initialCredit

destMidVar3.Lang = srcMidVar3.lang

destMidVar3.MainProductID = srcMidVar3.mainProductId

destMidVar3.PaidMode = srcMidVar3.paidMode

destMidVar2.Subscriber.RegistrationTime=formatDate(srcMidVar3.registrationTime, "yyyyMMddHHmmss")

mappingList(srcMidVar3.simplePropertyList,destMidVar3.SimpleProperty,listMapping7)

destMidVar3.SMSLang = srcMidVar3.smsLang

destMidVar3.USSDLang = srcMidVar3.ussdLang

def destMidVar4 = destReturn.QueryBasicInfoResultMsg.QueryBasicInfoResult.SubscriberAccount

def srcMidVar4 = srcReturn.resultBody.subscriberAccount

destMidVar4.Address = srcMidVar4.address

destMidVar4.BillcycleType = srcMidVar4.billCycleType

destMidVar4.Code = srcMidVar4.code

destMidVar4.Name = srcMidVar4.name

destMidVar4.RelationCustomer = srcMidVar4.relationCustomer

mappingList(srcMidVar4.simplePropertys,destMidVar4.SimpleProperty,listMapping8)

destMidVar4.Title = srcMidVar4.title
