dest.setServiceOperation("CRMForBSS","changeOfferingStatus")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.oss.webservice.bss.services.ChangeOfferingStatusRequestDocument"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Name = src.paraName
	
	dest.Value = src.paraValue
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->
	dest.Name = src.paraName
	
	dest.Value = src.paraValue
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.Name = src.paraName
	
	dest.Value = src.paraValue
	
}

def destMidVar = destArgs0.ChangeOfferingStatusRequest.RequestHeader

def srcMidVar = srcArgs0.workOrderHeader

destMidVar.AccessPwd = srcMidVar.accessPwd

destMidVar.AccessUser = srcMidVar.accessUser

destMidVar.BEId = srcMidVar.beId

destMidVar.ChannelId = srcMidVar.channelId

def destMidVar0 = destArgs0.ChangeOfferingStatusRequest.RequestHeader.ExtParamList

mappingList(srcMidVar.extParamList,destMidVar0.ParameterInfo,listMapping0)

destMidVar.Language = srcMidVar.language

destMidVar.OperatorId = srcMidVar.operatorId

destMidVar.OperatorPwd = srcMidVar.operatorPwd

destMidVar.ProcessTime = srcMidVar.processTime

destMidVar.SessionId = srcMidVar.sessionId

destMidVar.TestFlag = srcMidVar.testFlag

destMidVar.TimeType = srcMidVar.timeType

destMidVar.TimeZoneID = srcMidVar.timeZoneId

destMidVar.TransactionId = srcMidVar.transactionId

destMidVar.Version = srcMidVar.version

def destMidVar1 = destArgs0.ChangeOfferingStatusRequest.ChangeOfferingStatus

def srcMidVar0 = srcArgs0.changeOfferingStatus

destMidVar1.ChangeResone = srcMidVar0.changeResone

destMidVar1.ChangeType = srcMidVar0.changeType

mappingList(srcMidVar0.extParamList,destMidVar1.ExtParamList,listMapping1)

destMidVar1.SubscriberID = srcMidVar0.subId

destMidVar1.Msisdn = srcMidVar0.subscriberNo

def destMidVar3 = destArgs0.ChangeOfferingStatusRequest.ChangeOfferingStatus.OfferInfo

def srcMidVar2 = srcArgs0.changeOfferingStatus.offeringInfo

destMidVar3.TrialStartTime = srcMidVar2.trialStartTime

destMidVar3.TrialEndTime = srcMidVar2.trialEndTime

def destMidVar4 = destArgs0.ChangeOfferingStatusRequest.ChangeOfferingStatus.OfferInfo.OfferingID

destMidVar4.OfferingID = srcMidVar2.offeringId

destMidVar4.PurchaseSeq = srcMidVar2.purchaseSeq

destMidVar3.ActiveTime = srcMidVar2.activeDate

def destMidVar5 = destArgs0.ChangeOfferingStatusRequest.ChangeOfferingStatus.OfferInfo.ExtParamList

mappingList(srcMidVar2.extParmalist,destMidVar5.AdditionInfo,listMapping2)

destMidVar3.ExpireTime = srcMidVar2.expireTime

destMidVar3.EffectiveTime = srcMidVar2.effectiveTime
