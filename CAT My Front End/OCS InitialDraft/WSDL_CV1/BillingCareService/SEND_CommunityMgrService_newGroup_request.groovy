import com.huawei.ngcbs.bm.common.common.Constant4Model


dest.setServiceOperation("CommunityMgrService","newGroup")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs33ws.core.bo.Ocs33MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs33ws.community.newgroup.io.OCS33NewGroupRequest"

def srcReqHeaderDoc = srcArgs0.NewGroupRequestMsg.RequestHeader

destArgs0.operatorId = srcReqHeaderDoc.OperatorID

destArgs0.additionInfo = srcReqHeaderDoc.additionInfo

destArgs0.commandId = srcReqHeaderDoc.CommandId

destArgs0.transactionId = srcReqHeaderDoc.TransactionId

destArgs0.interMedi = srcReqHeaderDoc.InterMedi

destArgs0.reserve2 = srcReqHeaderDoc.Reserve2

destArgs0.thirdPartyId = srcReqHeaderDoc.ThirdPartyID

destArgs0.reserve3 = srcReqHeaderDoc.Reserve3

destArgs0.sequenceId = srcReqHeaderDoc.SequenceId

destArgs0.visitArea = srcReqHeaderDoc.visitArea

destArgs0.belToAreaId = srcReqHeaderDoc.BelToAreaID

destArgs0.currentCell = srcReqHeaderDoc.currentCell

destArgs0.partnerId = srcReqHeaderDoc.PartnerID

destArgs0.tradePartnerId = srcReqHeaderDoc.TradePartnerID

destArgs0.partnerOperId = srcReqHeaderDoc.PartnerOperID

destArgs0.version = srcReqHeaderDoc.Version

destArgs0.remark = srcReqHeaderDoc.Remark

destArgs0.loginSystem = srcReqHeaderDoc.SessionEntity.Name

destArgs0.password = srcReqHeaderDoc.SessionEntity.Password

destArgs0.remoteAddress = srcReqHeaderDoc.SessionEntity.RemoteAddress

destArgs0.interFrom = srcReqHeaderDoc.InterFrom

destArgs0.requestType = srcReqHeaderDoc.RequestType

destArgs0.messageSeq = srcReqHeaderDoc.SerialNo

def NewGroupRequestGroupCustomerSrc = srcArgs0.NewGroupRequestMsg.NewGroupRequest.GroupCustomer

def NewGroupRequestGroupAccountSrc = srcArgs0.NewGroupRequestMsg.NewGroupRequest.GroupAccount

def NewGroupRequestGroupOfferSrc = srcArgs0.NewGroupRequestMsg.NewGroupRequest.GroupOffer

def NewGroupRequestBASrc = srcArgs0.NewGroupRequestMsg.NewGroupRequest.BA

def listMapping10

listMapping10 = 
{
    src,dest  ->
	
	dest.id = src.Id
	
	dest.value = src.Value
	

}

def listMapping11

listMapping11 = 
{
    src,dest  ->
	
	dest.callingCellID = src.CallingCellID
}

def listMapping12

listMapping12 = 
{
    src,dest  ->
	
	dest.ratingGroup = src.RatingGroup
}

def listMapping311

listMapping311 = 
{
    src,dest  ->
	
	dest.id = src.Id
	
	mappingList(src.SimpleProperty,dest.simpleProperty,listMapping10)
}

def listMapping31

listMapping31 = 
{
    src,dest  ->
	
	dest.id = src.Id
	
	mappingList(src.Service,dest.service,listMapping311)
	
	mappingList(src.SimpleProperty,dest.simpleProperty,listMapping10)
}

def listMapping3

listMapping3 = 
{
    src,dest  ->
	
	dest.id = src.Id
	
	mappingList(src.Product,dest.product,listMapping31)
	
	mappingList(src.simpleProperty,dest.simpleProperty,listMapping10)
}


def listMapping4

listMapping4 = 
{
    src,dest  ->
	
	dest.grpMemberNo = src.GrpMemberNo
	
	dest.scenId = src."SCEN_ID"
	
	dest.applytime =  parseDate(src.ApplyTime,Constant4Model.DAY_FORMAT)
	
	dest.expireTime = parseDate(src.ExpireTime,Constant4Model.DAY_FORMAT)
	
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->
	
	dest.mediumID = src.MediumID
	
	dest.billLang = src.BillLang
	
	dest.applyTime =  parseDate(src.ApplyTime,Constant4Model.DAY_FORMAT)
	
	dest.expireTime = parseDate(src.ExpireTime,Constant4Model.DAY_FORMAT)
	
	
}

destArgs1.groupCustomer.parentGroupNumber = NewGroupRequestGroupCustomerSrc.ParentGroupNumber

destArgs1.groupCustomer.groupNumber = NewGroupRequestGroupCustomerSrc.GroupNumber

destArgs1.groupCustomer.groupType = NewGroupRequestGroupCustomerSrc.GroupType

destArgs1.groupCustomer.groupName = NewGroupRequestGroupCustomerSrc.GroupName

destArgs1.groupCustomer.groupCustomerCode = NewGroupRequestGroupCustomerSrc.GroupCustomerCode

destArgs1.groupCustomer.groupCustomerID = NewGroupRequestGroupCustomerSrc.GroupCustomerID

destArgs1.groupCustomer.registerNo = NewGroupRequestGroupCustomerSrc.RegisterNo

destArgs1.groupCustomer.maxCountOfCug = NewGroupRequestGroupCustomerSrc.MaxCountOfCug

destArgs1.groupCustomer.maxCountOfGroupOut = NewGroupRequestGroupCustomerSrc.MaxCountOfGroupOut

destArgs1.groupCustomer.maxHuntingList = NewGroupRequestGroupCustomerSrc.MaxHuntingList

destArgs1.groupCustomer.quotaAmount = NewGroupRequestGroupCustomerSrc.QuotaAmount

destArgs1.groupCustomer.belToAreaID = NewGroupRequestGroupCustomerSrc.BelToAreaID

destArgs1.groupCustomer.maxMemNumber = NewGroupRequestGroupCustomerSrc.MaxMemNumber

mappingList(NewGroupRequestGroupCustomerSrc.SimpleProperty,destArgs1.groupCustomer.simpleList,listMapping10)

mappingList(NewGroupRequestGroupCustomerSrc.RatingGroupList,destArgs1.groupCustomer.ratingGroupList,listMapping12)

mappingList(NewGroupRequestGroupCustomerSrc.OfficeZoneInfo,destArgs1.groupCustomer.officeZoneInfoList,listMapping11)

destArgs1.groupAccount.firstName = NewGroupRequestGroupAccountSrc.FirstName

destArgs1.groupAccount.middleName = NewGroupRequestGroupAccountSrc.MiddleName

destArgs1.groupAccount.lastName = NewGroupRequestGroupAccountSrc.LastName

destArgs1.groupAccount.accountId = NewGroupRequestGroupAccountSrc.AccountID

destArgs1.groupAccount.accountCode = NewGroupRequestGroupAccountSrc.AccountCode

destArgs1.groupAccount.paidMode = NewGroupRequestGroupAccountSrc.PaidMode

destArgs1.groupAccount.paymentMethod = NewGroupRequestGroupAccountSrc.PaymentMethod

destArgs1.groupAccount.billFlag = NewGroupRequestGroupAccountSrc.BillFlag

destArgs1.groupAccount.title = NewGroupRequestGroupAccountSrc.Title

destArgs1.groupAccount.billAddress1 = NewGroupRequestGroupAccountSrc.BillAddress1

destArgs1.groupAccount.billAddress2 = NewGroupRequestGroupAccountSrc.BillAddress2

destArgs1.groupAccount.billAddress3 = NewGroupRequestGroupAccountSrc.BillAddress3

destArgs1.groupAccount.billAddress4 = NewGroupRequestGroupAccountSrc.BillAddress4

destArgs1.groupAccount.billAddress5 = NewGroupRequestGroupAccountSrc.BillAddress5

destArgs1.groupAccount.zipCode = NewGroupRequestGroupAccountSrc.ZipCode

destArgs1.groupAccount.billLang = NewGroupRequestGroupAccountSrc.BillLang

destArgs1.groupAccount.emailBillAddr = NewGroupRequestGroupAccountSrc.EmailBillAddr

destArgs1.groupAccount.smsBillLang = NewGroupRequestGroupAccountSrc.SMSBillLang

destArgs1.groupAccount.smsBillAddr = NewGroupRequestGroupAccountSrc.SMSBillAddr

destArgs1.groupAccount.bankAcctNo = NewGroupRequestGroupAccountSrc.BankAcctNo

destArgs1.groupAccount.bankId = NewGroupRequestGroupAccountSrc.BankID

destArgs1.groupAccount.bankName = NewGroupRequestGroupAccountSrc.BankName

destArgs1.groupAccount.bankAcctName = NewGroupRequestGroupAccountSrc.BankAcctName

destArgs1.groupAccount.bankAcctActiveDate = parseDate(NewGroupRequestGroupAccountSrc.BankAcctActiveDate,Constant4Model.DAY_FORMAT)

destArgs1.groupAccount.cardExpiryDate = parseDate(NewGroupRequestGroupAccountSrc.CardExpiryDate,Constant4Model.DAY_FORMAT)

destArgs1.groupAccount.sfid = NewGroupRequestGroupAccountSrc.SFID

destArgs1.groupAccount.spid = NewGroupRequestGroupAccountSrc.SPID

destArgs1.groupAccount.ccGroup = NewGroupRequestGroupAccountSrc.CCGroup

destArgs1.groupAccount.ccSubGroup = NewGroupRequestGroupAccountSrc.CCSubGroup

destArgs1.groupAccount.vatNumber = NewGroupRequestGroupAccountSrc.VATNumber

destArgs1.groupAccount.printVATNo = NewGroupRequestGroupAccountSrc.PrintVATNo

destArgs1.groupAccount.dueDate = NewGroupRequestGroupAccountSrc.DueDate

destArgs1.groupAccount.ppsAcctInitBal = NewGroupRequestGroupAccountSrc.PPSAcctInitBal

destArgs1.groupAccount.ppsAcctCredit = NewGroupRequestGroupAccountSrc.PPSAcctCredit

destArgs1.groupAccount.posAcctInitBal = NewGroupRequestGroupAccountSrc.POSAcctInitBal

destArgs1.groupAccount.posAcctCredit = NewGroupRequestGroupAccountSrc.POSAcctCredit

destArgs1.groupAccount.contactTel = NewGroupRequestGroupAccountSrc.ContactTel

destArgs1.groupAccount.dcCallForward = NewGroupRequestGroupAccountSrc.DCCallForward

destArgs1.groupAccount.billCycleCredit = NewGroupRequestGroupAccountSrc.BillCycleCredit

destArgs1.groupAccount.creditCtrlMode = NewGroupRequestGroupAccountSrc.CreditCtrlMode

destArgs1.groupAccount.billCycleType = NewGroupRequestGroupAccountSrc.BillCycleType

mappingList(NewGroupRequestGroupAccountSrc.SimpleProperty,destArgs1.groupAccount.simpleProperty,listMapping10)

mappingList(NewGroupRequestGroupOfferSrc,destArgs1.groupOffer,listMapping3)

destArgs1.ba.baCode = NewGroupRequestBASrc.BACode

destArgs1.ba.billAddr.title = NewGroupRequestBASrc.BillAddr.Title

destArgs1.ba.billAddr.firstName = NewGroupRequestBASrc.BillAddr.FirstName

destArgs1.ba.billAddr.middleName = NewGroupRequestBASrc.BillAddr.MiddleName

destArgs1.ba.billAddr.lastName = NewGroupRequestBASrc.BillAddr.LastName

destArgs1.ba.billAddr.billAddress1 = NewGroupRequestBASrc.BillAddr.BillAddress1

destArgs1.ba.billAddr.billAddress2 = NewGroupRequestBASrc.BillAddr.BillAddress2

destArgs1.ba.billAddr.billAddress3 = NewGroupRequestBASrc.BillAddr.BillAddress3

destArgs1.ba.billAddr.billAddress4 = NewGroupRequestBASrc.BillAddr.BillAddress4

destArgs1.ba.billAddr.billAddress5 = NewGroupRequestBASrc.BillAddr.BillAddress5

destArgs1.ba.billAddr.city = NewGroupRequestBASrc.BillAddr.City

destArgs1.ba.billAddr.zipCode = NewGroupRequestBASrc.BillAddr.ZipCode

destArgs1.ba.billAddr.smsBillAddr = NewGroupRequestBASrc.BillAddr.SMSBillAddr

destArgs1.ba.billAddr.billLang = NewGroupRequestBASrc.BillAddr.BillLang

destArgs1.ba.billAddr.emailBillAddr = NewGroupRequestBASrc.BillAddr.EmailBillAddr

destArgs1.ba.applyTime =  parseDate(NewGroupRequestBASrc.ApplyTime,Constant4Model.DAY_FORMAT)

destArgs1.ba.expireTime = parseDate(NewGroupRequestBASrc.ExpireTime,Constant4Model.DAY_FORMAT)

mappingList(destArgs1.ba.BAScenario,destArgs1.ba.baSenario,listMapping4)

mappingList(destArgs1.ba.BillDelivery,destArgs1.ba.billDelivery,listMapping5)

destArgs1.cbpid = srcArgs0.NewGroupRequestMsg.CBPID

destArgs1.validMode = srcArgs0.NewGroupRequestMsg.ValidMode

destArgs1.effectiveDate = parseDate(srcArgs0.NewGroupRequestMsg.EffectiveDate,Constant4Model.DAY_FORMAT)

