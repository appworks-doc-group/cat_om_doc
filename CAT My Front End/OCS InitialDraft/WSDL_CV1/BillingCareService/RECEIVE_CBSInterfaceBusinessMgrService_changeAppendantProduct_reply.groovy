def srcReturnData = src.payload._return
def destReturnDoc = dest.payload._return

def destHeader = destReturnDoc.ChangeAppendantProductResultMsg.ResultHeader
def srcHeader = srcReturnData.resultHeader

destHeader.CommandId = srcHeader.commandId
destHeader.ResultCode = srcHeader.resultCode
destHeader.ResultDesc = srcHeader.resultDesc
destHeader.SequenceId = srcHeader.sequenceId
destHeader.Version = srcHeader.version
destHeader.TransactionId = srcHeader.transactionId


def srcBusinessData = srcReturnData.resultBody
def destReturnData = destReturnDoc.ChangeAppendantProductResultMsg.ChangeAppendantProductResult

def listProdOrderMapping = 
{
  src,dest  ->

	dest.ProductID = src.productId
	dest.ProductOrderKey = src.productOrderKey		
	dest.EffectiveDate = src.effectiveDate	
	dest.ExpireDate = src.expireDate
	dest.AutoType = src.autoType	
}

mappingList(srcBusinessData.prodOrderInfoList, destReturnData.ProductOrderInfo, listProdOrderMapping)
