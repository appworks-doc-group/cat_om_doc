dest.setServiceOperation("MDSInterfaceBatchCallbackService","notifyFileExecuteResult")
def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.common.provision.ws.io.BatchSyncProCallbackRequest"

def srcMidVar = srcArgs0.notifyFileExecuteResult.NotifyFileExecuteResultRequest

destArgs0.requestFileName = srcMidVar.reqFileName

destArgs0.responseFileName = srcMidVar.repFileName

def destMidVar = destArgs0.requestHeader

def srcMidVar0 = srcArgs0.notifyFileExecuteResult.NotifyFileExecuteResultRequest.MessageHeader

destMidVar.accessChannel = srcMidVar0.accessChannel

destMidVar.beId = srcMidVar0.beId

destMidVar.operatorCode = srcMidVar0.operatorCode

destMidVar.password = srcMidVar0.password
