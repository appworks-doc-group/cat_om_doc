dest.setServiceOperation("BMQueryService", "queryLastXTransaction")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.query.querylastxtransaction.io.QueryLastXTransactionRequest"

def listMapping0

listMapping0 =
{
    src, dest ->

    dest.code = src.Code

    dest.Value = src.value
}

def srcMidVar = srcArgs0.QueryLastXTransactionRequestMsg.QueryLastXTransactionRequest

def destMidVar = destArgs1.subAccessCode

def srcMidVar0 = srcMidVar.SubAccessCode

destMidVar.primaryIdentity = srcMidVar0.PrimaryIdentity

destMidVar.subscriberKey = srcMidVar0.SubscriberKey

def srcMidVar1 = srcArgs0.QueryLastXTransactionRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar1.AccessMode

def srcMidVar2 = srcMidVar1.AccessSecurity

destArgs0.loginSystem = srcMidVar2.LoginSystemCode

destArgs0.password = srcMidVar2.Password

destArgs0.remoteAddress = srcMidVar2.RemoteIP

destArgs0.businessCode = srcMidVar2.BusinessCode

destArgs0.messageSeq = srcMidVar1.MessageSeq

destArgs0.msgLanguageCode = srcMidVar1.MsgLanguageCode

def srcMidVar3 = srcMidVar1.OperatorInfo

destArgs0.channelId = srcMidVar3.ChannelID

destArgs0.operatorId = srcMidVar3.OperatorID

def srcMidVar4 = srcMidVar1.OwnershipInfo

destArgs0.beId = srcMidVar4.BEID

destArgs0.brId = srcMidVar4.BRID

def srcMidVar5 = srcMidVar1.TimeFormat

destArgs0.timeType = srcMidVar5.TimeType

destArgs0.timeZoneId = srcMidVar5.TimeZoneID

destArgs0.version = srcMidVar1.Version

mappingList(srcMidVar0.AdditionalProperty, destArgs0.simpleProperty, listMapping0)