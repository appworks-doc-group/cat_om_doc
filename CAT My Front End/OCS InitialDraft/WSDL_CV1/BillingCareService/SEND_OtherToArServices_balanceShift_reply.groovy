def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def destMidVar = destReturn.resultHeader

def srcMidVar = srcReturn.BalanceShiftResultMsg.ResultHeader

destMidVar.msgLanguageCode = srcMidVar.MsgLanguageCode

destMidVar.resultCode = srcMidVar.ResultCode

destMidVar.resultDesc = srcMidVar.ResultDesc

destMidVar.version = srcMidVar.Version

mappingList(srcMidVar.AdditionalProperty,destMidVar.simpleProperty,listMapping0)

listMapping1 =
{
	src,dest  ->

	dest.balanceId = src.BalanceID

	dest.balanceType = src.BalanceType

	dest.balanceTypeName = src.BalanceTypeName

	dest.currencyId = src.CurrencyID

	dest.newBalanceAmt = src.NewBalanceAmt

	dest.oldBalanceAmt = src.OldBalanceAmt

}

def srcMidVar1 = srcReturn.BalanceShiftResultMsg.BalanceShiftResult.Transferor

def destMidVar1 = destReturn.transferor

mappingList(srcMidVar1.BalanceChgInfo, destMidVar1.balanceChgInfoList, listMapping1)

listMapping2 =
{
	src,dest  ->

	dest.balanceId = src.BalanceID

	dest.balanceType = src.BalanceType

	dest.balanceTypeName = src.BalanceTypeName

	dest.currencyId = src.CurrencyID

	dest.newBalanceAmt = src.NewBalanceAmt

	dest.oldBalanceAmt = src.OldBalanceAmt

}

def srcMidVar2 = srcReturn.BalanceShiftResultMsg.BalanceShiftResult.Transferee

def destMidVar2 = destReturn.transferee

mappingList(srcMidVar2.BalanceChgInfo, destMidVar2.balanceChgInfoList, listMapping2)

destReturn._class = "com.huawei.ngcbs.cm.common.ws.client.io.ar.balanceshift.BalanceShiftResult"
