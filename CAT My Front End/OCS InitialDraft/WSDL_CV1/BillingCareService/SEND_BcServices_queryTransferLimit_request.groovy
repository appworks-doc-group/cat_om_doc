dest.setServiceOperation("BMQueryService","queryTransferLimit")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.query.querytransferlimit.io.QueryTransferLimitRequest"

def srcMidVar5 = srcArgs0.QueryTransferLimitRequestMsg.QueryTransferLimitRequest

destArgs1.transferLimitClass = srcMidVar5.TransferLimitClass

destArgs1.transferLimitType = srcMidVar5.TransferLimitType

destArgs1.subAccessCode.primaryIdentity = srcMidVar5.SubAccessCode.PrimaryIdentity

destArgs1.subAccessCode.subscriberKey = srcMidVar5.SubAccessCode.SubscriberKey

def srcMidVar = srcArgs0.QueryTransferLimitRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.QueryTransferLimitRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode



