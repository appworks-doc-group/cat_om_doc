import com.huawei.ngcbs.bm.common.common.Constant4Model
dest.setServiceOperation("OCS33OrderMgrService","newSubscriberExtend")

def srcArgs0 = src.payload._args[0]
def srcMessageHeader = srcArgs0.NewSubscriberExtendRequestMsg.RequestHeader
def srcMessageBody = srcArgs0.NewSubscriberExtendRequestMsg.NewSubscriberRequest

def destArgs0 = dest.payload._args[0]
def destArgs1 = dest.payload._args[1]
destArgs0._class = "com.huawei.ngcbs.cm.ocs33ws.core.bo.Ocs33MessageHeader"
destArgs1._class = "com.huawei.ngcbs.cm.ocs33ws.subscriber.newsubscriber.io.OCS33NewSubscriberExtendRequest"

def listMappingSimple
listMappingSimple = 
{
    src,dest  ->
	dest.id = src.Id
	dest.value = src.Value	
}

def listMappingCust
listMappingCust = 
{
	src,dest  ->
	dest.firstName = src.FirstName
	dest.middleName = src.MiddleName
	dest.lastName = src.LastName
	dest.customerID = src.CustomerID
	dest.customerCode = src.CustomerCode
	dest.idType = src.IdType
	dest.idCode = src.IdCode
	dest.gender = src.Gender
	dest.birthday = src.Birthday
	dest.title = src.Title
	dest.salary = src.Salary
	dest.homeAddress = src.HomeAddress
	dest.grade = src.Grade
	dest.email = src.Email
	dest.zipCode = src.ZipCode
	dest.country = src.Country
	dest.nativePlace = src.NativePlace
	dest.nationType = src.NationType
	dest.jobType = src.JobType
	dest.education = src.Education
	dest.maritalStatus = src.MaritalStatus
	dest.skill = src.Skill
	dest.socialNo = src.SocialNo
	dest.creditGrade = src.CreditGrade
	dest.creditAmount = src.CreditAmount
	dest.customerState = src.CustomerState
	dest.contactNumber1 = src.ContactNumber1
	dest.contactNumber2 = src.ContactNumber2
	dest.contactNumber3 = src.ContactNumber3
	dest.contactNumber4 = src.ContactNumber4
	dest.contactNumber5 = src.ContactNumber5
	dest.systemNofityNumber = src.SystemNofityNumber
	dest.homePhone = src.HomePhone
	dest.workPhone = src.WorkPhone
	dest.belToAreaID = src.BelToAreaID
	dest.postAddress1 = src.PostAddress1
	dest.postAddress2 = src.PostAddress2
	dest.postAddress3 = src.PostAddress3
	dest.postAddress4 = src.PostAddress4
	dest.postAddress5 = src.PostAddress5
	dest.remark = src.Remark
	dest.langType = src.LangType
	dest.timeZone = src.TimeZone
	dest.operationType = src.OperationType
	mappingList(src.SimpleProperty,dest.simplePropertys,listMappingSimple)
}

def listMappingHomeZoneList
listMappingHomeZoneList = 
{
    src,dest  ->
	dest.homeZoneID = src.HomeZoneID
	dest.effectiveDate = src.effectiveDate
	dest.expireDate = src.expireDate 
}


def listMappingAcct
listMappingAcct = 
{
    src,dest  ->
	dest.firstName = src.FirstName
	dest.middleName = src.MiddleName
	dest.lastName = src.LastName
	dest.accountID = src.AccountID
	dest.accountCode = src.AccountCode
	dest.paidMode = src.PaidMode
	dest.paymentMethod = src.PaymentMethod
	dest.billFlag = src.BillFlag
	dest.title = src.Title
	dest.billAddress1 = src.BillAddress1
	dest.billAddress2 = src.BillAddress2
	dest.billAddress3 = src.BillAddress3
	dest.billAddress4 = src.BillAddress4
	dest.billAddress5 = src.BillAddress5
	dest.zipCode = src.ZipCode
	dest.billLang = src.BillLang
	dest.emailBillAddr = src.EmailBillAddr
	dest.sMSBillLang = src.SMSBillLang
	dest.sMSBillAddr = src.SMSBillAddr
	dest.bankAcctNo = src.BankAcctNo
	dest.bankID = src.BankID
	dest.bankName = src.BankName
	dest.bankAcctName = src.BankAcctName
	dest.bankAccType = src.BankAccType
	dest.bankAcctActiveDate = src.BankAcctActiveDate
	dest.cardExpiryDate = src.CardExpiryDate
	dest.SFID = src.SFID
	dest.SPID = src.SPID
	dest.cCGroup = src.CCGroup
	dest.cCSubGroup = src.CCSubGroup
	dest.vATNumber = src.VATNumber
	dest.printVATNo = src.PrintVATNo
	dest.dueDate = src.DueDate
	dest.pPSAcctInitBal = src.PPSAcctInitBal
	dest.pPSAcctCredit = src.PPSAcctCredit
	dest.pOSAcctInitBal = src.POSAcctInitBal
	dest.pOSAcctCredit = src.POSAcctCredit
	dest.contactTel = src.ContactTel
	dest.dCCallForward = src.DCCallForward
	dest.billCycleCredit = src.BillCycleCredit
	dest.creditCtrlMode = src.CreditCtrlMode
	dest.staffID = src.StaffID
	dest.operationType = src.OperationType
	dest.billCycleType = src.BillCycleType
	mappingList(src.SimpleProperty,dest.simplePropertys,listMappingSimple)
}

def listMappingOfferOrderIdentify
listMappingOfferOrderIdentify = 
{
    src,dest  ->
	dest.offerKey = src.OfferKey
	dest.extOfferCode = src.ExtOfferCode
	dest.extOfferOrderCode = src.ExtOfferOrderCode
}
def listMappingValidMode
listMappingValidMode = 
{
    src,dest  ->
	dest.mode = src.Mode
	dest.validateFlag = src.ValidateFlag
	dest.effectiveDate = dest.effectiveDate = parseDate(src.EffectiveDate,Constant4Model.DATE_FORMAT)
	dest.expireDate = parseDate(src.ExpireDate,Constant4Model.DATE_FORMAT)
}
def listMappingFamilyList
listMappingFamilyList = 
{
    src,dest  ->
	dest.familyNo1 = src.FamilyNo
	dest.phoneNoOrder = src.phoneNoOrder
	dest.subGroupType = src.subGroupType
	dest.effectiveDate = src.effectiveDate
	dest.expireDate = src.expireDate
}
def listMappingFamily
listMappingFamily = 
{
    src,dest  ->
	mappingList(src.FamilyNoList,dest.familyNoLists,listMappingFamilyList)
}
def listMappingCallScreenList
listMappingCallScreenList = 
{
    src,dest  ->
	dest.callScreenNo = src.callScreenNo
	dest.effectiveDate = src.effectiveDate
	dest.expireDate = src.expireDate
	dest.weekStart = src.weekStart
	dest.weekStop = src.weekStop
	dest.routeNumber = src.routeNumber
	dest.iRRouteFlag = src.IRRouteFlag
	dest.routingMethod = src.RoutingMethod
}

def listMappingCallScreen
listMappingCallScreen = 
{
    src,dest  ->
	dest.callScreenType = src.CallScreenType
	mappingList(src.CallScreenNoInfoList,dest.callScreenNoInfoLists,listMappingCallScreenList)
}


def listMappingHomeZone
listMappingHomeZone = 
{
    src,dest  ->
	dest.homeZoneChange = src.HomeZoneChange
	dest.homeZonePromptMode = src.HomeZonePromptMode
	mappingList(src.HomeZoneList,dest.homeZoneLists,listMappingHomeZoneList)
}
def listMappingSpecialProperty
listMappingSpecialProperty = 
{
    src,dest  ->
	listMappingFamily.call(src.FamilyNo,dest.familyNo)
	listMappingCallScreen.call(src.CallScreen,dest.callScreen)
	listMappingHomeZone.call(src.HomeZone,dest.homeZone)
}

def listMappingSubscriber
listMappingSubscriber = 
{
    src,dest  ->
	dest.subscriberNo = src.SubscriberNo
	dest.subscriberCode = src.SubscriberCode
	dest.belToAreaID = src.BelToAreaID
	dest.iMSI = src.IMSI
	dest.password = src.Password
	mappingList(src.SimpleProperty,dest.simplePropertys,listMappingSimple)
}

def listMappingPrimaryOfferOrders
listMappingPrimaryOfferOrders = 
{
    src,dest  ->
	listMappingOfferOrderIdentify.call(src.OfferOrderIdentify,dest.offerOrderIdentify)
	listMappingValidMode.call(src.ValidMode,dest.validMode)
	mappingList(src.SpecialProperty,dest.specialPropertys,listMappingSpecialProperty)
	mappingList(src.SimpleProperty,dest.simplePropertys,listMappingSimple)
	listMappingSubscriber.call(src.Subscriber,dest.subscriber)
	dest.accountCode = src.AccountCode
	dest.handleChargeFlag = src.HandleChargeFlag
}

def listMappingOptionalOfferOrders
listMappingOptionalOfferOrders = 
{
    src,dest  ->
	listMappingOfferOrderIdentify.call(src.OfferOrderIdentify,dest.offerOrderIdentify)
	listMappingValidMode.call(src.ValidMode,dest.validMode)
	mappingList(src.SpecialProperty,dest.specialPropertys,listMappingSpecialProperty)
	mappingList(src.SimpleProperty,dest.simplePropertys,listMappingSimple)
	dest.accountCode = src.SubscriberNo
	dest.accountCode = src.AccountCode
	dest.handleChargeFlag = src.HandleChargeFlag
}












destArgs0.commandId = srcMessageHeader.CommandId
destArgs0.version = srcMessageHeader.Version
destArgs0.transactionId = srcMessageHeader.TransactionId
destArgs0.sequenceId = srcMessageHeader.SequenceId
destArgs0.requestType = srcMessageHeader.RequestType
destArgs0.beId = srcMessageHeader.TenantId
destArgs0.msgLanguageCode = srcMessageHeader.Language
destArgs0.loginSystem = srcMessageHeader.SessionEntity.Name
destArgs0.password = srcMessageHeader.SessionEntity.Password
destArgs0.remoteAddress = srcMessageHeader.SessionEntity.RemoteAddress
destArgs0.interFrom = srcMessageHeader.InterFrom
destArgs0.interMedi = srcMessageHeader.InterMedi
destArgs0.interMode = srcMessageHeader.InterMode
destArgs0.visitArea = srcMessageHeader.visitArea
destArgs0.currentCell = srcMessageHeader.currentCell
destArgs0.additionInfo = srcMessageHeader.additionInfo
destArgs0.thirdPartyId = srcMessageHeader.ThirdPartyID
destArgs0.partnerId = srcMessageHeader.PartnerID
destArgs0.operatorId = srcMessageHeader.OperatorID
destArgs0.tradePartnerId = srcMessageHeader.TradePartnerID
destArgs0.partnerOperId = srcMessageHeader.PartnerOperID
destArgs0.belToAreaId = srcMessageHeader.BelToAreaID
destArgs0.reserve2 = srcMessageHeader.Reserve2
destArgs0.reserve3 = srcMessageHeader.Reserve3
destArgs0.messageSeq = srcMessageHeader.SerialNo
destArgs0.remark = srcMessageHeader.Remark



def srcHandleObj = srcMessageBody.HandleObj
def destHandleObj = destArgs1.handleObj
destHandleObj._class = "com.huawei.ngcbs.cm.ocs33ws.offer.chgpriofr.io.HandleObj"

destHandleObj.subscriberNo = srcHandleObj.SubscriberNo
destHandleObj.accountCode = srcHandleObj.AccountCode
destHandleObj.customerCode = srcHandleObj.CustomerCode


def srcCust = srcMessageBody.Customer
def destCust = destArgs1.customer
listMappingCust.call(srcCust,destCust)


mappingList(srcMessageBody.Account,destArgs1.accounts,listMappingAcct)


def srcPrimaryOrders = srcMessageBody.PrimaryOfferOrder
def destPrimaryOffOrders = destArgs1.primaryOfferOrders
mappingList(srcPrimaryOrders,destPrimaryOffOrders,listMappingPrimaryOfferOrders)


def srcOptionalOrders = srcMessageBody.OptionalOfferOrder
def destOptionalOffOrders = destArgs1.optionalOfferOrders
mappingList(srcOptionalOrders,destOptionalOffOrders,listMappingOptionalOfferOrders)

