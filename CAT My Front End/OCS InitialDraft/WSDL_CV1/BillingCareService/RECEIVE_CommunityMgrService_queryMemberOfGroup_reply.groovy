def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.QueryMemberOfGroupResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.CommandId = srcMidVar.commandId

destMidVar.Version = srcMidVar.version

destMidVar.TransactionId = srcMidVar.transactionId

destMidVar.SequenceId = srcMidVar.sequenceId

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.OperationTime = srcMidVar.operationTime

destMidVar.OrderId = srcMidVar.orderId

def destMidVar0 = destReturn.QueryMemberOfGroupResultMsg.QueryMemberOfGroupResult

def srcMidVar0 = srcReturn.ocs33GroupMemberList

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.ParentGroupNumber = src.parentGroupNumber
	
	dest.TopGroupNumber = src.topGroupNumber
	
	dest.GrpMemberNo = src.grpMemberNo
	
	dest.GrpMemberShortNo = src.grpMemberShortNo
	
	if(isNull(src.grpMemberShortNo)) 
	{ 
		dest.GrpMemberShortNo =''
	} 

	dest.GrpMemberType = src.grpMemberType
	
	dest.GrpMemberStatus = src.grpMemberStatus
	
}

mappingList(srcMidVar0,destMidVar0.GroupMember,listMapping0)