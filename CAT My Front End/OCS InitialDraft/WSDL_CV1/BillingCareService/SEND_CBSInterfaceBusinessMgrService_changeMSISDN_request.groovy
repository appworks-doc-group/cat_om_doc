dest.setServiceOperation("CBSInterfaceBusinessMgrService","changeMSISDN");

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destArgs1 = dest.payload._args[1]

def srcMidVar = srcArgs0.ChangeMSISDNRequestMsg.RequestHeader

destArgs0.beId = srcMidVar.TenantId

destArgs0.thirdPartyId = srcMidVar.ThirdPartyID

destArgs0.commandId = srcMidVar.CommandId

destArgs0.interMedi = srcMidVar.InterMedi

destArgs0.additionInfo = srcMidVar.additionInfo

destArgs0.belToAreaId = srcMidVar.BelToAreaID

destArgs0.interFrom = srcMidVar.InterFrom

destArgs0.interMode = srcMidVar.InterMode

destArgs0.partnerId = srcMidVar.PartnerID

destArgs0.partnerOperId = srcMidVar.PartnerOperID

destArgs0.remark = srcMidVar.Remark

destArgs0.requestType = srcMidVar.RequestType

destArgs0.reserve2 = srcMidVar.Reserve2

destArgs0.reserve3 = srcMidVar.Reserve3

destArgs0.sequenceId = srcMidVar.SequenceId

def srcMidVar0 = srcArgs0.ChangeMSISDNRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar0.Name

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteAddress

destArgs0.tradePartnerId = srcMidVar.TradePartnerID

destArgs0.transactionId = srcMidVar.TransactionId

destArgs0.version = srcMidVar.Version

destArgs0.visitArea = srcMidVar.visitArea

def srcMidVar1 = srcArgs0.ChangeMSISDNRequestMsg.ChangeMSISDNRequest

destArgs1.handlingChargeFlag = srcMidVar1.HandlingChargeFlag

def destMidVar = destArgs1.modifySubIdenInfo.modifySubIdens[0]

destMidVar.newSubIden = srcMidVar1.NewSubscriberNo

destMidVar.oldSubIden = srcMidVar1.SubscriberNo

def destMidVar0 = destArgs1.subAccessCode

destMidVar0.primaryIdentity = srcMidVar1.SubscriberNo

destArgs0.messageSeq = srcMidVar.SerialNo

destArgs0.operatorId = srcMidVar.OperatorID

destArgs0.businessCode = "ChangeMSISDN"
