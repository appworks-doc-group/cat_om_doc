import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.supplementprofile.io.SupplementProfileRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.postCode = src.PostCode
	
	dest.addr1 = src.Address1
	
	dest.addr10 = src.Address10
	
	dest.addr11 = src.Address11
	
	dest.addr12 = src.Address12
	
	dest.addr2 = src.Address2
	
	dest.addr3 = src.Address3
	
	dest.addr4 = src.Address4
	
	dest.addr5 = src.Address5
	
	dest.addr6 = src.Address6
	
	dest.addr7 = src.Address7
	
	dest.addr8 = src.Address8
	
	dest.addr9 = src.Address9
	
	dest.tpAddrKey = src.AddressKey
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->
  dest._class = "com.huawei.ngcbs.cm.common.common.io.creditlimit.CreditLimitExtInfo"
	dest.limitType = src.LimitType
	
	def destMidVar1 = dest.valueInfo
	
	destMidVar1.amount = src.LimitValue
	
	dest.limitPlanCode = src.LimitPlanCode
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.email = src.Email
	
	dest.fax = src.Fax
	
	dest.firstName = src.FirstName
	
	dest.homePhone = src.HomePhone
	
	dest.lastName = src.LastName
	
	dest.middleName = src.MiddleName
	
	dest.mobilePhone = src.MobilePhone
	
	dest.officePhone = src.OfficePhone
	
	dest.title = src.Title
	
	dest.addrKey = src.AddressKey
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.code = src.BMCode
	
	dest.type = src.BMType
	
}

def listMapping8

listMapping8 = 
{
    src,dest  ->

	dest.autoRechargeAmt = src.AutoRechargeAmt
	
	dest.autoRechargeDate = src.AutoRechargeDate
	
	dest.periodType = src.PeriodType
	
}

def listMapping9

listMapping9 = 
{
    src,dest  ->

	dest.autoPayDate = src.AutoPayDate
	
	dest.autoPayMaxAmt = src.AutoPayMaxAmt
	
}

def listMapping10

listMapping10 = 
{
    src,dest  ->

	dest.controlPeriodType = src.ControlPeriodType
	
	dest.autoRechargeAmt = src.AutoRechargeAmt
	
	dest.balanceThreshold = src.BalanceThreshold
	
	dest.maxTimes = src.MaxTimes
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.payType = src.AutoPayType
	
	listMapping8.call(src.TimeSchema,dest.timeSchema)
	
	listMapping9.call(src.BillCycleSchema,dest.billCycleSchema)
	
	listMapping10.call(src.LowBalanceSchema,dest.lowBalanceSchema)
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	def destMidVar2 = dest.paymentChannelInfo
	
	destMidVar2.paymentChannelKey = src.AutoPayChannelKey
	
	def srcMidVar7 = src.AutoPayChannelInfo
	
	destMidVar2.bankAcctName = srcMidVar7.AcctName
	
	destMidVar2.bankAcctNo = srcMidVar7.AcctNo
	
	destMidVar2.bankAcctType = srcMidVar7.AcctType
	
	destMidVar2.bankBranchCode = srcMidVar7.BankBranchCode
	
	destMidVar2.bankCode = srcMidVar7.BankCode
	
	destMidVar2.creditCardType = srcMidVar7.CreditCardType
	
	destMidVar2.cvvNumber = srcMidVar7.CVVNumber
	
	destMidVar2.priority = srcMidVar7.Priority
	
	dest.chargeCodes = srcMidVar7.ChargeCode
	
	mappingList(srcMidVar7.PaymentPlan,dest.paymentPlanInfos,listMapping7)
	
	destMidVar2.bankAcctExpDate = srcMidVar7.ExpDate
	
}

def listMapping11

listMapping11 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping12

listMapping12 = 
{
    src,dest  ->

	dest.education = src.Education
	
	dest.email = src.Email
	
	dest.fax = src.Fax
	
	dest.firstName = src.FirstName
	
	dest.gender = src.Gender
	
	dest.homePhone = src.HomePhone
	
	dest.idNumber = src.IDNumber
	
	dest.idType = src.IDType
	
	dest.lastName = src.LastName
	
	dest.middleName = src.MiddleName
	
	dest.mobilePhone = src.MobilePhone
	
	dest.nationality = src.Nationality
	
	dest.nativePlace = src.NativePlace
	
	dest.occupation = src.Occupation
	
	dest.officePhone = src.OfficePhone
	
	dest.race = src.Race
	
	dest.title = src.Title
	
	dest.salary = src.Salary
	
	dest.idValidity=parseDate(src.IDValidity,Constant4Model.DATE_FORMAT)
	
	dest.birthday=parseDate(src.Birthday,Constant4Model.DATE_FORMAT)
	
	dest.marriedStatus = src.MaritalStatus
	
	dest.addrKey = src.HomeAddressKey
	
}

def listMapping13

listMapping13 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping14

listMapping14 = 
{
    src,dest  ->

	dest.idNumber = src.IDNumber
	
	dest.idType = src.IDType
	
	dest.industry = src.Industry
	
	dest.orgEmail = src.OrgEmail
	
	dest.orgLevel = src.OrgLevel
	
	dest.orgName = src.OrgName
	
	dest.orgType = src.OrgType
	
	dest.subIndustry = src.SubIndustry
	
	dest.addrKey = src.OrgAddressKey
	
	dest.idValidity=parseDate(src.IDValidity,Constant4Model.DATE_FORMAT)
	
	dest.orgFax = src.OrgFaxNumber
	
	dest.orgPhone = src.OrgPhoneNumber
	
	dest.orgSName = src.OrgShortName
	
	dest.sizeLevel = src.OrgSize
	
	dest.orgWeb = src.OrgWebSite
	
}

def listMapping15

listMapping15 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping16

listMapping16 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping17

listMapping17 = 
{
    src,dest  ->

	dest.channelType = src.ChannelType
	
	dest.noticeType = src.NoticeType
	
	dest.subNoticeType = src.SubNoticeType
	
}

def listMapping18

listMapping18 = 
{
    src,dest  ->

	dest.education = src.Education
	
	dest.email = src.Email
	
	dest.fax = src.Fax
	
	dest.firstName = src.FirstName
	
	dest.gender = src.Gender
	
	dest.homePhone = src.HomePhone
	
	dest.idNumber = src.IDNumber
	
	dest.idType = src.IDType
	
	dest.lastName = src.LastName
	
	dest.middleName = src.MiddleName
	
	dest.mobilePhone = src.MobilePhone
	
	dest.nationality = src.Nationality
	
	dest.nativePlace = src.NativePlace
	
	dest.occupation = src.Occupation
	
	dest.officePhone = src.OfficePhone
	
	dest.race = src.Race
	
	dest.title = src.Title
	
	dest.birthday=parseDate(src.Birthday,Constant4Model.DATE_FORMAT)
	
	dest.addrKey = src.HomeAddressKey
	
	dest.marriedStatus = src.MaritalStatus
	
	dest.idValidity=parseDate(src.IDValidity,Constant4Model.DATE_FORMAT)
	
	dest.salary = src.Salary
	
}

def listMapping19

listMapping19 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping20

listMapping20 = 
{
    src,dest  ->

	dest.idNumber = src.IDNumber
	
	dest.idType = src.IDType
	
	dest.industry = src.Industry
	
	dest.orgEmail = src.OrgEmail
	
	dest.orgLevel = src.OrgLevel
	
	dest.orgName = src.OrgName
	
	dest.orgType = src.OrgType
	
	dest.subIndustry = src.SubIndustry
	
	dest.idValidity=parseDate(src.IDValidity,Constant4Model.DATE_FORMAT)
	
	dest.addrKey = src.OrgAddressKey
	
	dest.orgSName = src.OrgShortName
	
	dest.sizeLevel = src.OrgSize
	
	dest.orgWeb = src.OrgWebSite
	
	dest.orgPhone = src.OrgPhoneNumber
	
	dest.orgFax = src.OrgFaxNumber
	
}

def listMapping21

listMapping21 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping22

listMapping22 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.SupplementProfileRequestMsg.RequestHeader

destArgs0.version = srcMidVar.Version

def srcMidVar0 = srcArgs0.SupplementProfileRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar0.TimeType

destArgs0.timeZoneId = srcMidVar0.TimeZoneID

def srcMidVar1 = srcArgs0.SupplementProfileRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar1.BEID

destArgs0.brId = srcMidVar1.BRID

def srcMidVar2 = srcArgs0.SupplementProfileRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar2.ChannelID

destArgs0.operatorId = srcMidVar2.OperatorID

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar3 = srcArgs0.SupplementProfileRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar3.LoginSystemCode

destArgs0.password = srcMidVar3.Password

destArgs0.remoteAddress = srcMidVar3.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

def srcMidVar4 = srcArgs0.SupplementProfileRequestMsg.SupplementProfileRequest

mappingList(srcMidVar4.AddressInfo,destArgs1.addressInfo,listMapping1)

def srcMidVar5 = srcArgs0.SupplementProfileRequestMsg.SupplementProfileRequest.Account[0].AcctBasicInfo

def destMidVar = destArgs1.changeAcctBasicInfoInfo[0]

mappingList(srcMidVar5.AcctProperty,destMidVar.properties,listMapping2)

def destMidVar0 = destArgs1.changeAcctBasicInfoInfo[0].accountInfo

destMidVar0.billLang = srcMidVar5.BillLang

destMidVar0.dunningFlag = srcMidVar5.DunningFlag

destMidVar0.latePaymentFlag = srcMidVar5.LateFeeChargeable

destMidVar.redlistFlag = srcMidVar5.RedlistFlag

def srcMidVar6 = srcArgs0.SupplementProfileRequestMsg.SupplementProfileRequest.Account[0]

destMidVar0.acctKey = srcMidVar6.AcctKey

destMidVar0.acctPaymentMethod = srcMidVar6.AcctPayMethod

destMidVar0.currencyId = srcMidVar6.CurrencyID

destMidVar.initBalance = srcMidVar6.InitBalance

destMidVar.billCycleType = srcMidVar6.BillCycleType

mappingList(srcMidVar6.CreditLimit,destMidVar.creditLimits,listMapping3)

listMapping4.call(srcMidVar5.ContactInfo,destMidVar.contactInfo)

mappingList(srcMidVar5.FreeBillMedium,destMidVar0.bmInfos,listMapping5)

mappingList(srcMidVar6.AutoPayChannel,destMidVar.paymentChannels,listMapping6)

destMidVar0.acctName = srcMidVar5.AcctName

def destMidVar3 = destArgs1.newOwnership

def srcMidVar8 = srcArgs0.SupplementProfileRequestMsg.SupplementProfileRequest.NewOwnership

destMidVar3.acctKey = srcMidVar8.AccountKey

destMidVar3.custKey = srcMidVar8.CustomerKey

def destMidVar4 = destArgs1.regCustomerInfo.customerInfo

def srcMidVar9 = srcArgs0.SupplementProfileRequestMsg.SupplementProfileRequest.RegisterCustomer.CustBasicInfo

destMidVar4.custLevel = srcMidVar9.CustLevel

destMidVar4.custLoyalty = srcMidVar9.CustLoyalty

def destMidVar5 = destArgs1.regCustomerInfo

mappingList(srcMidVar9.CustProperty,destMidVar5.custProperties,listMapping11)

destMidVar4.custSegment = srcMidVar9.CustSegment

destMidVar4.billCycleType = srcMidVar9.DFTBillCycleType

destMidVar4.currencyId = srcMidVar9.DFTCurrencyID

destMidVar4.dunningFlag = srcMidVar9.DunningFlag

destMidVar4.custWLang = srcMidVar9.DFTWrittenLang

destMidVar4.custPwd = srcMidVar9.DFTPwd

destMidVar4.custPLang = srcMidVar9.DFTIVRLang

def srcMidVar10 = srcArgs0.SupplementProfileRequestMsg.SupplementProfileRequest.RegisterCustomer

listMapping12.call(srcMidVar10.IndividualInfo,destMidVar5.individualInfo)

def destMidVar6 = destArgs1.subAccessCode

def srcMidVar11 = srcArgs0.SupplementProfileRequestMsg.SupplementProfileRequest.SubAccessCode

destMidVar6.primaryIdentity = srcMidVar11.PrimaryIdentity

destMidVar6.subscriberKey = srcMidVar11.SubscriberKey

def srcMidVar12 = srcArgs0.SupplementProfileRequestMsg.SupplementProfileRequest.RegisterCustomer.IndividualInfo

mappingList(srcMidVar12.IndividualProperty,destMidVar5.indvProperties,listMapping13)

listMapping14.call(srcMidVar10.OrgInfo,destMidVar5.orgInfo)

def srcMidVar13 = srcArgs0.SupplementProfileRequestMsg.SupplementProfileRequest.RegisterCustomer.OrgInfo

mappingList(srcMidVar13.OrgProperty,destMidVar5.orgProperties,listMapping15)

def destMidVar7 = destArgs1.userCustomerInfo.customerInfo

def srcMidVar14 = srcArgs0.SupplementProfileRequestMsg.SupplementProfileRequest.UserCustomer.CustInfo.CustBasicInfo

destMidVar7.custLevel = srcMidVar14.CustLevel

destMidVar7.custLoyalty = srcMidVar14.CustLoyalty

def destMidVar8 = destArgs1.userCustomerInfo

mappingList(srcMidVar14.CustProperty,destMidVar8.custProperties,listMapping16)

destMidVar7.custSegment = srcMidVar14.CustSegment

destMidVar7.billCycleType = srcMidVar14.DFTBillCycleType

destMidVar7.currencyId = srcMidVar14.DFTCurrencyID

destMidVar7.custPLang = srcMidVar14.DFTIVRLang

destMidVar7.custPwd = srcMidVar14.DFTPwd

destMidVar7.custWLang = srcMidVar14.DFTWrittenLang

destMidVar7.dunningFlag = srcMidVar14.DunningFlag

def srcMidVar15 = srcArgs0.SupplementProfileRequestMsg.SupplementProfileRequest.UserCustomer.CustInfo

destMidVar7.custClass = srcMidVar15.CustClass

destMidVar7.custCode = srcMidVar15.CustCode

destMidVar7.custNodeType = srcMidVar15.CustNodeType

destMidVar7.custType = srcMidVar15.CustType

destMidVar7.parentCustKey = srcMidVar15.ParentCustKey

mappingList(srcMidVar15.NoticeSuppress,destMidVar8.noticeSuppresses,listMapping17)

def srcMidVar16 = srcArgs0.SupplementProfileRequestMsg.SupplementProfileRequest.UserCustomer

destMidVar7.tpCustKey = srcMidVar16.CustKey

listMapping18.call(srcMidVar16.IndividualInfo,destMidVar8.individualInfo)

def srcMidVar17 = srcArgs0.SupplementProfileRequestMsg.SupplementProfileRequest.UserCustomer.IndividualInfo

mappingList(srcMidVar17.IndividualProperty,destMidVar8.indvProperties,listMapping19)

listMapping20.call(srcMidVar16.OrgInfo,destMidVar8.orgInfo)

def srcMidVar18 = srcArgs0.SupplementProfileRequestMsg.SupplementProfileRequest.UserCustomer.OrgInfo

mappingList(srcMidVar18.OrgProperty,destMidVar8.orgProperties,listMapping21)

def srcMidVar19 = srcArgs0.SupplementProfileRequestMsg.SupplementProfileRequest.SubBasicInfo

def destMidVar9 = destArgs1.changeSubBasicInfo

mappingList(srcMidVar19.SubProperty,destMidVar9.subProperties,listMapping22)

def destMidVar10 = destArgs1.changeSubBasicInfo.subscriberInfo

destMidVar10.dunningFlag = srcMidVar19.DunningFlag

destMidVar10.ivrLang = srcMidVar19.IVRLang

destMidVar10.subLevel = srcMidVar19.SubLevel

destMidVar10.writtenLang = srcMidVar19.WrittenLang
