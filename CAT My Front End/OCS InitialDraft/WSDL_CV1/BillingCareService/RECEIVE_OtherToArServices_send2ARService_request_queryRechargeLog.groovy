dest.setServiceOperation("OtherToArServices","queryRechargeLog")

def srcArgs0 = src.payload._args[0]

def srcArgs1 = src.payload._args[1]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = { src,dest  ->
    
    dest.Code = src.code
    
    dest.Value = src.value
}

def destMidVar = destArgs0.QueryRechargeLogRequestMsg.RequestHeader

destMidVar.Version = srcArgs0.version

def destMidVar0 = destArgs0.QueryRechargeLogRequestMsg.RequestHeader.TimeFormat

destMidVar0.TimeZoneID = srcArgs0.timeZoneId

destMidVar0.TimeType = srcArgs0.timeType

def destMidVar1 = destArgs0.QueryRechargeLogRequestMsg.RequestHeader.OwnershipInfo

destMidVar1.BRID = srcArgs0.brId

destMidVar1.BEID = srcArgs0.beId

def destMidVar2 = destArgs0.QueryRechargeLogRequestMsg.RequestHeader.OperatorInfo

destMidVar2.OperatorID = srcArgs0.operatorId

destMidVar2.ChannelID = srcArgs0.channelId

destMidVar.MsgLanguageCode = srcArgs0.msgLanguageCode

destMidVar.MessageSeq = srcArgs0.messageSeq

destMidVar.BusinessCode = srcArgs0.commandCode

mappingList(srcArgs0.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def destMidVar3 = destArgs0.QueryRechargeLogRequestMsg.RequestHeader.AccessSecurity

destMidVar3.RemoteIP = srcArgs0.remoteAddress

destMidVar3.Password = srcArgs0.password

destMidVar3.LoginSystemCode = srcArgs0.loginSystem

destMidVar.AccessMode = srcArgs0.interMode

def destMidVar4 = destArgs0.QueryRechargeLogRequestMsg.QueryRechargeLogRequest

destMidVar4.BeginRowNum = srcArgs1.beginRowNum

def destMidVar5 = destArgs0.QueryRechargeLogRequestMsg.QueryRechargeLogRequest.CustAccessCode

def srcMidVar = srcArgs1.custAccessCode

destMidVar5.CustomerCode = srcMidVar.customerCode

destMidVar5.CustomerKey = srcMidVar.customerKey

destMidVar4.EndTime=formatDate(srcArgs1.endTime, "yyyyMMddHHmmss")

destMidVar4.FetchRowNum = srcArgs1.fetchRowNum

def destMidVar6 = destArgs0.QueryRechargeLogRequestMsg.QueryRechargeLogRequest.QueryObj.AcctAccessCode

def srcMidVar0 = srcArgs1.queryObj.acctAccessCode

destMidVar6.AccountCode = srcMidVar0.accoutCode

destMidVar6.AccountKey = srcMidVar0.accoutKey

destMidVar6.PayType = srcMidVar0.payType

destMidVar6.PrimaryIdentity = srcMidVar0.primaryIdentity

def destMidVar7 = destArgs0.QueryRechargeLogRequestMsg.QueryRechargeLogRequest.QueryObj.SubAccessCode

def srcMidVar1 = srcArgs1.queryObj.subAccessCode

destMidVar7.PrimaryIdentity = srcMidVar1.primaryIdentity

destMidVar7.SubscriberKey = srcMidVar1.subscriberKey

destMidVar4.StartTime=formatDate(srcArgs1.startTime, "yyyyMMddHHmmss")

destMidVar4.TotalRowNum = srcArgs1.totalRowNum
