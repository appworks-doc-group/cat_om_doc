dest.setServiceOperation("CBSInterfaceAccountMgrService","exchangeAccount")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs12ws.account.exchangeaccount.io.ExchangeAccountRequest"

def srcMidVar = srcArgs0.ExchangeAccountRequestMsg.ExchangeAccountRequest

destArgs1.ammount = srcMidVar.Ammount

destArgs1.custID = srcMidVar.CustID

destArgs1.desAccountKey = srcMidVar.DesAccountKey

destArgs1.desAccountType = srcMidVar.DesAccountType

destArgs1.handlingChargeFlag = srcMidVar.HandlingChargeFlag

destArgs1.logId = srcMidVar.LogId

destArgs1.oriAccountKey = srcMidVar.OriAccountKey

destArgs1.oriAccountType = srcMidVar.OriAccountType

def destMidVar = destArgs1.subAccessCode

destMidVar.primaryIdentity = srcMidVar.SubscriberNo

def srcMidVar0 = srcArgs0.ExchangeAccountRequestMsg.RequestHeader

destArgs0.additionInfo = srcMidVar0.additionInfo

destArgs0.belToAreaId = srcMidVar0.BelToAreaID

destArgs0.commandId = srcMidVar0.CommandId

destArgs0.currentCell = srcMidVar0.currentCell

destArgs0.interFrom = srcMidVar0.InterFrom

destArgs0.interMedi = srcMidVar0.InterMedi

destArgs0.interMode = srcMidVar0.InterMode

destArgs0.operatorId = srcMidVar0.OperatorID

destArgs0.partnerId = srcMidVar0.PartnerID

destArgs0.partnerOperId = srcMidVar0.PartnerOperID

destArgs0.remark = srcMidVar0.Remark

destArgs0.requestType = srcMidVar0.RequestType

destArgs0.reserve2 = srcMidVar0.Reserve2

destArgs0.reserve3 = srcMidVar0.Reserve3

destArgs0.sequenceId = srcMidVar0.SequenceId

destArgs0.messageSeq = srcMidVar0.SerialNo

destArgs0.thirdPartyId = srcMidVar0.ThirdPartyID

destArgs0.tradePartnerId = srcMidVar0.TradePartnerID

destArgs0.transactionId = srcMidVar0.TransactionId

destArgs0.version = srcMidVar0.Version

destArgs0.visitArea = srcMidVar0.visitArea

def srcMidVar1 = srcArgs0.ExchangeAccountRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar1.Name

destArgs0.password = srcMidVar1.Password

destArgs0.remoteAddress = srcMidVar1.RemoteAddress
