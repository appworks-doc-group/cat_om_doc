import com.huawei.ngcbs.bm.common.common.Constant4Model

def omResultDoc = src.payload._return
def bmResult = dest.payload._return
bmResult._class = "com.huawei.ngcbs.cm.common.ws.client.io.om.OMQueryGrpCallScreenNoResult"

def omResultHeader = omResultDoc.QueryGrpCallScreenNoResultMsg.ResultHeader
def bmResultHeader = bmResult.resultHeader

bmResultHeader.version = omResultHeader.Version
bmResultHeader.resultCode = omResultHeader.ResultCode
bmResultHeader.resultDesc = omResultHeader.ResultDesc

def omResultBody = omResultDoc.QueryGrpCallScreenNoResultMsg.QueryGrpCallScreenNoResult

def listMappingCallScreenNoInfo
listMappingCallScreenNoInfo = 
{
    src,dest  ->
	dest.callScreenNo = src.CallScreenNo
	dest.screenNoType = src.ScreenNoType
	dest.effectiveDate = parseDate(src.EffectiveDate,Constant4Model.DATE_FORMAT)
	dest.expireDate = parseDate(src.ExpireDate,Constant4Model.DATE_FORMAT)
	dest.weekStart = src.WeekStart
	dest.weekStop = src.WeekStop
	dest.timeStart = src.TimeStart
	dest.timeStop = src.TimeStop
	dest.routeNumber = src.RouteNumber
	dest.routingMethod = src.RoutingMethod
}
mappingList(omResultBody.CallScreenNoInfo,bmResult.callScreenNoInfos,listMappingCallScreenNoInfo)
