dest.setServiceOperation("AccountService","batchChangeScatteredAcctInfo")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.account.batch.changescatteredacctinfo.io.BatchChangeScatteredAcctInfoRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	
}


def srcMidVar = srcArgs0.BatchChangeScatteredAcctInfoRequestMsg.BatchChangeScatteredAcctInfoRequest

destArgs1.requestFileName = srcMidVar.FileName

def srcMidVar0 = srcArgs0.BatchChangeScatteredAcctInfoRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar0.BEID

destArgs0.brId = srcMidVar0.BRID

def srcMidVar1 = srcArgs0.BatchChangeScatteredAcctInfoRequestMsg.RequestHeader

destArgs0.businessCode = srcMidVar1.BusinessCode

def srcMidVar2 = srcArgs0.BatchChangeScatteredAcctInfoRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar2.ChannelID

destArgs0.messageSeq = srcMidVar1.MessageSeq

destArgs0.msgLanguageCode = srcMidVar1.MsgLanguageCode

destArgs0.operatorId = srcMidVar2.OperatorID

def srcMidVar3 = srcArgs0.BatchChangeScatteredAcctInfoRequestMsg.RequestHeader.AccessSecurity

destArgs0.password = srcMidVar3.Password

def destMidVar = destArgs0.simpleProperty[0]

def srcMidVar4 = srcArgs0.BatchChangeScatteredAcctInfoRequestMsg.RequestHeader.AdditionalProperty[0]

destMidVar.code = srcMidVar4.Code

destMidVar.value = srcMidVar4.Value

def srcMidVar5 = srcArgs0.BatchChangeScatteredAcctInfoRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar5.TimeType

destArgs0.timeZoneId = srcMidVar5.TimeZoneID

destArgs0.version = srcMidVar1.Version

destArgs0.remoteAddress = srcMidVar3.RemoteIP

destArgs0.loginSystem = srcMidVar3.LoginSystemCode

mappingList(srcMidVar1.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.interMode = srcMidVar1.AccessMode
