import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	mappingList(src.simpleProperty,dest.AdditionalProperty,listMapping1)
	
	dest.Version = src.version
	
	dest.ResultDesc = src.resultDesc
	
	dest.ResultCode = src.resultCode
	
	dest.MsgLanguageCode = src.msgLanguageCode
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.NewEffectiveTime=formatDate(src.effDate, Constant4Model.DATE_FORMAT)
	
	dest.NewExpirationTime=formatDate(src.expDate, Constant4Model.DATE_FORMAT)
	
	def destMidVar0 = dest.OfferingKey
	
	def srcMidVar = src.offeringKey
	
	destMidVar0.OfferingID = srcMidVar.oId
	
	destMidVar0.PurchaseSeq = srcMidVar.pSeq
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	mappingList(src.modifyOfferingInfos,dest.ModifyOffering,listMapping3)
	
}

def destMidVar = destReturn.ChangeCustOfferingResultMsg

listMapping0.call(srcReturn.resultHeader,destMidVar.ResultHeader)

listMapping2.call(srcReturn.changeCustOfferingResultBody,destMidVar.ChangeCustOfferingResult)
