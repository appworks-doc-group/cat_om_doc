def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.BelToAreaID = src.belToAreaId
	
	dest.CommandId = src.commandId
	
	dest.currentCell = src.currentCell
	
	dest.InterFrom = src.interFrom
	
	dest.InterMedi = src.interMedi
	
	dest.InterMode = src.interMode
	
	dest.OperatorID = src.operatorId
	
	dest.Remark = src.remark
	
	dest.SequenceId = src.sequenceId
	
	dest.SerialNo = src.serialNo
	
	def destMidVar0 = dest.SessionEntity
	
	def srcMidVar = src.sessionEntity
	
	destMidVar0.Name = srcMidVar.name
	
	destMidVar0.Password = srcMidVar.password
	
	destMidVar0.RemoteAddress = srcMidVar.remoteAddress
	
	dest.TenantID = src.tenantId
	
	dest.TransactionId = src.transactionId
	
	dest.Version = src.version
	
	dest.visitArea = src.visitArea
	
	dest.TradePartnerID = src.tradePartnerId
	
	dest.ThirdPartyID = src.thirdPartyId
	
	dest.RequestType = src.requestType
	
	dest.PartnerID = src.partnerId
	
	dest.PartnerOperID = src.partnerOperId
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Value = src.value
	
	dest.Id = src.code
	
}

def destMidVar = destArgs0.ModifyGroupBasicInfoRequestMsg

listMapping0.call(srcArgs0.omRequestHeader,destMidVar.RequestHeader)

def destMidVar1 = destArgs0.ModifyGroupBasicInfoRequestMsg.ModifyGroupBasicInfoRequest

destMidVar1.GroupNumber = srcArgs0.groupNumber

def destMidVar2 = destArgs0.ModifyGroupBasicInfoRequestMsg.ModifyGroupBasicInfoRequest.GroupBasicInfo

destMidVar2.GroupName = srcArgs0.groupName

destMidVar2.NoAnswerTime = srcArgs0.noAnswerTime

destMidVar2.SpeedDialPrefix = srcArgs0.speedDialPrefix

mappingList(srcArgs0.simplePropertyList,destMidVar2.SimpleProperty,listMapping1)
