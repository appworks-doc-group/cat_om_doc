dest.setServiceOperation("CBSInterfaceAccountMgrService","activeFirst")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destArgs1 = dest.payload._args[1]

def srcMidVar = srcArgs0.ActiveFirstRequestMsg.RequestHeader

destArgs0.beId = srcMidVar.TenantId

destArgs0.operatorId = srcMidVar.OperatorID

destArgs0.additionInfo = srcMidVar.additionInfo

destArgs0.commandId = srcMidVar.CommandId

destArgs0.interMedi = srcMidVar.InterMedi

destArgs0.transactionId = srcMidVar.TransactionId

destArgs0.reserve2 = srcMidVar.Reserve2

destArgs0.thirdPartyId = srcMidVar.ThirdPartyID

destArgs0.reserve3 = srcMidVar.Reserve3

destArgs0.interMode = srcMidVar.InterMode

destArgs0.sequenceId = srcMidVar.SequenceId

destArgs0.visitArea = srcMidVar.visitArea

destArgs0.belToAreaId = srcMidVar.BelToAreaID

destArgs0.currentCell = srcMidVar.currentCell

destArgs0.partnerId = srcMidVar.PartnerID

destArgs0.partnerOperId = srcMidVar.PartnerOperID

destArgs0.version = srcMidVar.Version

destArgs0.remark = srcMidVar.Remark

destArgs0.requestType = srcMidVar.RequestType

def srcMidVar0 = srcArgs0.ActiveFirstRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar0.Name

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteAddress

destArgs0.interFrom = srcMidVar.InterFrom

destArgs0.tradePartnerId = srcMidVar.TradePartnerID

destArgs0.messageSeq = srcMidVar.SerialNo

def srcMidVar1 = srcArgs0.ActiveFirstRequestMsg.ActiveFirstRequest

destArgs1.rechargeAmount = srcMidVar1.RechargeAmount

destArgs1.cardPinNumber = srcMidVar1.CardPINNumber

def destMidVar = destArgs1.subAccessCode

destMidVar.primaryIdentity = srcMidVar1.SubscriberNo

def destMidVar0 = destArgs1.subActivationInfo.subscriberInfo

destMidVar0.ivrLang = srcMidVar1.IVRLang

destMidVar0.writtenLang = srcMidVar1.SMSLang
