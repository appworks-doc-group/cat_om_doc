def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.offering.changeproductstatus.io.ChangeProductStatusRequest"

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.businessCode = src.BusinessCode
	
	def srcMidVar0 = src.OperatorInfo
	
	dest.channelId = srcMidVar0.ChannelID
	
	def srcMidVar1 = src.AccessSecurity
	
	dest.loginSystem = srcMidVar1.LoginSystemCode
	
	dest.messageSeq = src.MessageSeq
	
	dest.msgLanguageCode = src.MsgLanguageCode
	
	dest.operatorId = srcMidVar0.OperatorID
	
	dest.password = srcMidVar1.Password
	
	def srcMidVar2 = src.TimeFormat
	
	dest.timeType = srcMidVar2.TimeType
	
	dest.timeZoneId = srcMidVar2.TimeZoneID
	
	dest.version = src.Version
	
	dest.remoteAddress = srcMidVar1.RemoteIP
	
	mappingList(src.AdditionalProperty,dest.simpleProperty,listMapping1)
	
	def srcMidVar3 = src.OwnershipInfo
	
	dest.beId = srcMidVar3.BEID
	
	dest.brId = srcMidVar3.BRID
	
	dest.interMode = src.AccessMode
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.accoutCode = src.AccountCode
	
	dest.accoutKey = src.AccountKey
	
	dest.payType = src.PayType
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping11

listMapping11 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	listMapping5.call(src.AcctAccessCode,dest.acctAccessCode)
	
	listMapping6.call(src.CustAccessCode,dest.custAccessCode)
	
	listMapping7.call(src.SubAccessCode,dest.subAccessCode)
	
	listMapping11.call(src.SubGroupAccessCode,dest.groupAccessCode)
	
}

def listMapping9

listMapping9 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping10

listMapping10 = 
{
    src,dest  ->

	dest.newStatus = src.NewStatus
	
	dest.oldStatus = src.OldStatus
	
	dest.opType = src.OpType
	
	dest.productId = src.ProductID
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	listMapping4.call(src.OfferingOwner,dest.anyAccessCode)
	
	def destMidVar = dest.changeProductStatusInfo
	
	listMapping9.call(src.OfferingKey,destMidVar.offeringKey)
	
	mappingList(src.ProductInst,destMidVar.productStatusInfos,listMapping10)
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	mappingList(src.OfferingInst,dest.changeProductStatusInfoList,listMapping3)
	
}


def srcMidVar = srcArgs0.ChangeProductStatusRequestMsg

listMapping0.call(srcMidVar.RequestHeader,destArgs0)

listMapping2.call(srcMidVar.ChangeProductStatusRequest,destArgs1)
