def srcArgs0 = src.payload._args[0]

def srcArgs1 = src.payload._args[1]

def destArgs0 = dest.payload._args[0]

def destMidVar = destArgs0.DeleteSubscriberRequestMsg.RequestHeader

destMidVar.BelToAreaID = srcArgs0.belToAreaId

destMidVar.CommandId = srcArgs0.commandId

destMidVar.currentCell = srcArgs0.currentCell

destMidVar.InterFrom = srcArgs0.interFrom

destMidVar.InterMedi = srcArgs0.interMedi

destMidVar.InterMode = srcArgs0.interMode

destMidVar.OperatorID = srcArgs0.operatorId

destMidVar.PartnerID = srcArgs0.partnerId

destMidVar.PartnerOperID = srcArgs0.partnerOperId

destMidVar.Remark = srcArgs0.remark

destMidVar.RequestType = srcArgs0.requestType

destMidVar.SequenceId = srcArgs0.sequenceId

destMidVar.SerialNo = srcArgs0.serialNo

destMidVar.TenantID = srcArgs0.tenantId

destMidVar.ThirdPartyID = srcArgs0.thirdPartyId

destMidVar.TradePartnerID = srcArgs0.tradePartnerId

destMidVar.TransactionId = srcArgs0.transactionId

destMidVar.Version = srcArgs0.version

destMidVar.visitArea = srcArgs0.visitArea

def destMidVar0 = destArgs0.DeleteSubscriberRequestMsg.DeleteSubscriberRequest.SubscriberNo

destMidVar0.SubscriberNo = srcArgs1.subscriberNo

def destMidVar1 = destArgs0.DeleteSubscriberRequestMsg.RequestHeader.SessionEntity

def srcMidVar = srcArgs0.sessionEntity

destMidVar1.Name = srcMidVar.name

destMidVar1.Password = srcMidVar.password

destMidVar1.RemoteAddress = srcMidVar.remoteAddress
