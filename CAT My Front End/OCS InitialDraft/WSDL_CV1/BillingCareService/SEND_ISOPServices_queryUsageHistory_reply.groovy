def srcReturn = src.payload._return

def destReturn = dest.payload._return

def srcMidVar = srcReturn.queryUsageHistoryResponse.return

destReturn.resultCode = srcMidVar.resultCode

destReturn.resultMessage = srcMidVar.resultMessage

def listMapping1

listMapping1 =
{
    src, dest ->

    dest.attrMapKey = src.key

    dest.attrMapValue = src.value

}

def listMapping0

listMapping0 =
{
    src, dest ->

    dest.clickUrl = src.clickUrl

    dest.desc = src.desc

    dest.displayMode = src.displayMode

    dest.offerId = src.offerId

    dest.offerName = src.offerName

    dest.offerType = src.offerType

    dest.priority = src.priority

    dest.url = src.url

    mappingList(src.offerAttrMap, dest.offerAttrMap, listMapping1)
}

def listMapping3

listMapping3 =
{
    src, dest ->

    dest.dateType = src.dateType

    dest.businessType = src.businessType

    dest.statisDate = src.statisDate

    dest.volume = src.volume

    dest.quota = src.quota
}

def listMapping2

listMapping2 =
{
    src, dest ->

    dest.usageType = src.usageType

    dest.offeringClass = src.offeringClass

    mappingList(src.businessVolumeList, dest.businessVolumeList, listMapping3)

    mappingList(src.offerList, dest.offerList, listMapping0)
}

mappingList(srcMidVar.usageList, destReturn.usageList, listMapping2)

destReturn._class = "com.huawei.ngcbs.cm.common.ws.client.io.isop.QueryUsageHistoryResult"