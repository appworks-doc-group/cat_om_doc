def srcReturn = src.payload._return

def destReturn = dest.payload._return

def srcMidVar = srcReturn.resultHeader

def destMidVar = destReturn.CumulateEnquiryResultMsg.ResultHeader

destMidVar.CommandId = srcMidVar.commandId

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.SequenceId = srcMidVar.sequenceId

destMidVar.SerialNo = srcMidVar.serialNo

destMidVar.TransactionId = srcMidVar.transactionId

destMidVar.Version = srcMidVar.version


def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.CumulateID = src.accmTypeID
	
	dest.CumulateBeginTime=formatDate(src.beginDate, "yyyyMMddHHmmss")
	
	dest.CumulateEndTime=formatDate(src.endDate, "yyyyMMddHHmmss")
	
	dest.CumulativeAmt = src.amount

	dest.CumulativePreAmt = src.cumulativePreAmt

	dest.CumulateMeasureId = src.measureID
	
	dest.CumulateDescription = src.accmTypeName
	
}

def destMidVar1 = destReturn.CumulateEnquiryResultMsg.CumulateEnquiryResult

mappingList(srcReturn.resultBody,destMidVar1.CumulativeItem,listMapping0)
