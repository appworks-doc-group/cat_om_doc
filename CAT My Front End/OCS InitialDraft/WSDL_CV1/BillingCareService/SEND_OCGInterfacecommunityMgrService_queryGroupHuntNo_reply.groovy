def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.resultHeader

def srcMidVar = srcReturn.QueryGroupHuntNoResultMsg.ResultHeader

destMidVar.resultCode = srcMidVar.ResultCode

destMidVar.resultDesc = srcMidVar.ResultDesc

destMidVar.version = srcMidVar.Version

def srcMidVar0 = srcReturn.QueryGroupHuntNoResultMsg.QueryGroupHuntNoResult

destReturn.effectiveTime = srcMidVar0.EffectiveTime

destReturn.expireTime = srcMidVar0.ExpireTime

destReturn.groupNumber = srcMidVar0.GroupNumber

destReturn.huntingMainNumber = srcMidVar0.HuntingMainNumber

destReturn.huntingNumber = srcMidVar0.HuntingNumber

destReturn.priority = srcMidVar0.Priority

destReturn.userState = srcMidVar0.UserState

destReturn._class = "com.huawei.ngcbs.cm.common.ws.client.io.om.QueryGroupHuntNoResult"
