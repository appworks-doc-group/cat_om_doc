dest.setServiceOperation("OtherToArServices","adjustment")

def srcArgs0 = src.payload._args[0]

def srcArgs1 = src.payload._args[1]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.AdjustmentAmt = src.adjustAmt
	
	dest.AdjustmentType = src.adjustmentType
	
	dest.BalanceID = src.balanceId
	
	dest.BalanceType = src.balanceType
	
	dest.CurrencyID = src.currencyId
	
	dest.EffectiveTime = src.effectiveTime
	
	dest.ExpireTime = src.expireTime
	
	dest.ExtendDays = src.extendDays
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.AdjustmentAmt = src.adjustmentAmt
	
	dest.AdjustmentType = src.adjustmentType
	
	dest.EffectiveTime = src.effectiveTime
	
	dest.ExpireTime = src.expireTime
	
	dest.ExtendDays = src.extendDays
	
	dest.FreeUnitInstanceID = src.freeUnitInstanceID
	
	dest.FreeUnitType = src.freeUnitType
	
}

def destMidVar = destArgs0.AdjustmentRequestMsg.RequestHeader

destMidVar.AccessMode = srcArgs0.interMode

def destMidVar0 = destArgs0.AdjustmentRequestMsg.RequestHeader.AccessSecurity

destMidVar0.LoginSystemCode = srcArgs0.loginSystem

destMidVar0.Password = srcArgs0.password

destMidVar0.RemoteIP = srcArgs0.remoteAddress

mappingList(srcArgs0.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

destMidVar.BusinessCode = srcArgs0.businessCode

destMidVar.MessageSeq = srcArgs0.messageSeq

destMidVar.MsgLanguageCode = srcArgs0.msgLanguageCode

def destMidVar1 = destArgs0.AdjustmentRequestMsg.RequestHeader.OperatorInfo

destMidVar1.ChannelID = srcArgs0.channelId

destMidVar1.OperatorID = srcArgs0.operatorId

def destMidVar2 = destArgs0.AdjustmentRequestMsg.RequestHeader.OwnershipInfo

destMidVar2.BEID = srcArgs0.beId

destMidVar2.BRID = srcArgs0.brId

def destMidVar3 = destArgs0.AdjustmentRequestMsg.RequestHeader.TimeFormat

destMidVar3.TimeType = srcArgs0.timeType

destMidVar3.TimeZoneID = srcArgs0.timeZoneId

destMidVar.Version = srcArgs0.version

def destMidVar4 = destArgs0.AdjustmentRequestMsg.AdjustmentRequest.AdjustmentObj.AcctAccessCode

def srcMidVar = srcArgs1.acctAccessCode

destMidVar4.AccountCode = srcMidVar.accoutCode

destMidVar4.AccountKey = srcMidVar.accoutKey

destMidVar4.PayType = srcMidVar.payType

destMidVar4.PrimaryIdentity = srcMidVar.primaryIdentity

def destMidVar5 = destArgs0.AdjustmentRequestMsg.AdjustmentRequest

destMidVar5.AdjustmentReasonCode = srcArgs1.adjustmentReasonCode

def destMidVar6 = destArgs0.AdjustmentRequestMsg.AdjustmentRequest.AdjustmentObj.SubAccessCode

def srcMidVar0 = srcArgs1.subAccessCode

destMidVar6.PrimaryIdentity = srcMidVar0.primaryIdentity

destMidVar6.SubscriberKey = srcMidVar0.subscriberKey

mappingList(srcArgs1.balanceAdjustments,destMidVar5.AdjustmentInfo,listMapping1)

mappingList(srcArgs1.freeUnitAdjustments,destMidVar5.FreeUnitAdjustmentInfo,listMapping2)

destMidVar5.OpType = srcArgs1.opType
