import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("SubscriberService","changeOfferingRelation")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.changeofferingrelation.io.ChangeOfferingRelationRequest"

def listMapping0

listMapping0 =
{
    src,dest  ->

	dest.code = src.Code

	dest.value = src.Value

}

def listMapping2

listMapping2 =
{
    src,dest  ->

	dest.oId = src.OfferingID

	dest.pSeq = src.PurchaseSeq

}

def listMapping4

listMapping4 =
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity

	dest.subscriberKey = src.SubscriberKey

}

def listMapping5

listMapping5 =
{
    src,dest  ->

	dest.limitCycleType = src.LimitCycleType

	dest.limitValue = src.LimitValue

	dest.measureUnit = src.MeasureUnit
	
	dest.rscRelationLimitKey = src.RscRelationLimitKey
	
	dest.limitValueExpr = src.LimitValueExpr

}

def listMapping1

listMapping1 =
{
    src,dest  ->

	dest.effectiveTime.time = parseDate(src.EffectiveTime.Time, Constant4Model.DATE_FORMAT)
	
	dest.effectiveTime.mode = src.EffectiveTime.Mode
	
	dest.expirationTime.time = parseDate(src.ExpirationTime.Time, Constant4Model.DATE_FORMAT)
	
	dest.expirationTime.mode = src.ExpirationTime.Mode

	listMapping2.call(src.OfferingKey,dest.offeringKey)
	
	if(!src.OfferingKey.isNull()){
	
	dest.offeringKey._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
	
	dest.offeringKey.oCode = src.OfferingKey.OfferingCode
	
	}
	
	listMapping4.call(src.DestSubIdentify,dest.destSubIdentify)

	listMapping5.call(src.FreeUnitShareLimit,dest.freeUnitShareLimit)

    dest.relationKey = src.RelationKey
    
}

def listMapping6

listMapping6 =
{
    src,dest  ->

	dest.relationKey = src.RelationKey
	
}

def listMapping13

listMapping13 =
{
    src,dest  ->

	dest.limitCycleType = src.LimitCycleType

	dest.limitValue = src.LimitValue

	dest.measureUnit = src.MeasureUnit
	
	dest.rscRelationLimitKey = src.RscRelationLimitKey
	
	dest.limitValueExpr = src.LimitValueExpr

}


def listMapping10

listMapping10 =
{
    src,dest  ->
	
	dest.relationKey = src.RelationKey

	def srcMidVar2 = src.NewRelation

	listMapping2.call(srcMidVar2.OfferingKey,dest.offeringKey)
	
		if(!srcMidVar2.OfferingKey.isNull()){
		dest.offeringKey._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
		dest.offeringKey.oCode = srcMidVar2.OfferingKey.OfferingCode
	}

	listMapping13.call(srcMidVar2.FreeUnitShareLimit,dest.freeUnitShareLimit)

	dest.effectiveTime.time = parseDate(srcMidVar2.EffectiveTime.Time, Constant4Model.DATE_FORMAT)
	
	dest.effectiveTime.mode = srcMidVar2.EffectiveTime.Mode
	
	dest.expirationTime.time = parseDate(srcMidVar2.ExpirationTime.Time, Constant4Model.DATE_FORMAT)
	
	dest.expirationTime.mode = srcMidVar2.ExpirationTime.Mode

}


def srcMidVar = srcArgs0.ChangeOfferingRelationRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.ChangeOfferingRelationRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.ChangeOfferingRelationRequestMsg.ChangeOfferingRelationRequest.OfferingRelation

mappingList(srcMidVar1.DelRelation,destArgs1.delRelations,listMapping6)

mappingList(srcMidVar1.AddRelation,destArgs1.addRelations,listMapping1)

listMapping10.call(srcMidVar1.ModRelation,destArgs1.modRelation)

def srcMidVar4 = srcArgs0.ChangeOfferingRelationRequestMsg.ChangeOfferingRelationRequest

listMapping4.call(srcMidVar4.SubAccessCode,destArgs1.subAccessCode)

def srcMidVar5 = srcArgs0.ChangeOfferingRelationRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar5.ChannelID

destArgs0.operatorId = srcMidVar5.OperatorID

def srcMidVar6 = srcArgs0.ChangeOfferingRelationRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar6.BEID

destArgs0.brId = srcMidVar6.BRID

def srcMidVar7 = srcArgs0.ChangeOfferingRelationRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar7.TimeType

destArgs0.timeZoneId = srcMidVar7.TimeZoneID

destArgs0.version = srcMidVar.Version
