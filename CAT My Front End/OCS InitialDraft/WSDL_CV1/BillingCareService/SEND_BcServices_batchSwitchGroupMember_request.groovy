import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("BMGroupService","batchSwitchGroupMember")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.group.batch.switchgroupmember.io.BatchSwitchGroupMemberRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.code = src.SubPropCode
	
	dest.value = src.Value
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.effDate = parseDate(src.EffectiveTime,"yyyyMMddHHmmss")
	
	dest.expDate = parseDate(src.ExpirationTime,"yyyyMMddHHmmss")
	
	def destMidVar2 = dest.property
	
	destMidVar2.propCode = src.PropCode
	
	destMidVar2.complexFlag = src.PropType
	
	mappingList(src.SubPropInst,destMidVar2.subProps,listMapping3)
	
	destMidVar2.value = src.Value
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.code = src.SubPropCode
	
	dest.value = src.Value
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.effDate = parseDate(src.EffectiveTime,"yyyyMMddHHmmss")
	
	dest.expDate = parseDate(src.ExpirationTime,"yyyyMMddHHmmss")
	
	def destMidVar5 = dest.property
	
	destMidVar5.propCode = src.PropCode
	
	destMidVar5.complexFlag = src.PropType
	
	mappingList(src.SubPropInst,destMidVar5.subProps,listMapping6)
	
	destMidVar5.value = src.Value
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	def destMidVar4 = dest.productInst
	
	destMidVar4.networkType = src.NetworkType
	
	destMidVar4.packageFlag = src.PackageFlag
	
	destMidVar4.parentProdId = src.ParentProdID
	
	mappingList(src.PInstProperty,dest.properties,listMapping5)
	
	destMidVar4.primaryFlag = src.PrimaryFlag
	
	destMidVar4.prodId = src.ProductID
	
	destMidVar4.productType = src.ProductType
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	def destMidVar0 = dest.offeringInst
	
	destMidVar0.bundleFlag = src.BundledFlag
	
	destMidVar0.offeringClass = src.OfferingClass
	
	def destMidVar1 = dest.offeringInst.offeringKey
	
	def srcMidVar6 = src.OfferingKey
	
	destMidVar1.oId = srcMidVar6.OfferingID
	
	destMidVar1.pSeq = srcMidVar6.PurchaseSeq
	
	mappingList(src.OInstProperty,dest.properties,listMapping2)
	
	def destMidVar3 = dest.offeringInst.parentOfferingKey
	
	def srcMidVar7 = src.ParentOfferingKey
	
	destMidVar3.oId = srcMidVar7.OfferingID
	
	destMidVar3.pSeq = srcMidVar7.PurchaseSeq
	
	mappingList(src.ProductInst,dest.productInsts,listMapping4)
	
	def destMidVar6 = dest.offeringInst.relGOfferingKey
	
	def srcMidVar8 = src.RelGOfferingKey
	
	destMidVar6.oId = srcMidVar8.OfferingID
	
	destMidVar6.pSeq = srcMidVar8.PurchaseSeq
	
	destMidVar0.status = src.Status
	
	destMidVar0.trialEndTime = parseDate(src.TrialEndTime,"yyyyMMddHHmmss")
	
	destMidVar0.trialStartTime = parseDate(src.TrialStartTime,"yyyyMMddHHmmss")
	
}

def srcMidVar = srcArgs0.BatchSwitchGroupMemberRequestMsg.RequestHeader

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.BatchSwitchGroupMemberRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

def srcMidVar1 = srcArgs0.BatchSwitchGroupMemberRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.BatchSwitchGroupMemberRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.BatchSwitchGroupMemberRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar.Version

def srcMidVar4 = srcArgs0.BatchSwitchGroupMemberRequestMsg.BatchSwitchGroupMemberRequest.EffectiveTime

destArgs1.effMode = srcMidVar4.Mode

destArgs1.effDate = parseDate(srcMidVar4.Time,"yyyyMMddHHmmss")

def srcMidVar5 = srcArgs0.BatchSwitchGroupMemberRequestMsg.BatchSwitchGroupMemberRequest

destArgs1.requestFileName = srcMidVar5.FileName

def destMidVar = destArgs1.subOfferingInfo

mappingList(srcMidVar5.GroupMemberOffering,destMidVar.addSuppOfferings,listMapping1)

def destMidVar7 = destArgs1.groupAccessCode

def srcMidVar9 = srcArgs0.BatchSwitchGroupMemberRequestMsg.BatchSwitchGroupMemberRequest.SubGroupAccessCode

destMidVar7.groupCode = srcMidVar9.SubGroupCode

destMidVar7.groupKey = srcMidVar9.SubGroupKey

def destOldGroupAccessCode = destArgs1.oldGroupAccessCode

def srcOldGroupAccessCode = srcArgs0.BatchSwitchGroupMemberRequestMsg.BatchSwitchGroupMemberRequest.OldGroup

destOldGroupAccessCode.groupCode = srcOldGroupAccessCode.SubGroupCode

destOldGroupAccessCode.groupKey = srcOldGroupAccessCode.SubGroupKey

def listMappingControlProperty

listMappingControlProperty =
{
	src,dest  ->

	dest.code = src.Code

	dest.value = src.Value

}
mappingList(srcArgs0.BatchSwitchGroupMemberRequestMsg.BatchSwitchGroupMemberRequest.ControlProperty,destArgs1.simplePropertyList,listMappingControlProperty)

def listMapping7

listMapping7 =
{
	src,dest  ->

	dest.oId = src.OfferingID

	dest.pSeq = src.PurchaseSeq

}

def listMapping8

listMapping8 =
{
	src,dest  ->

	dest.limitCycleType = src.LimitCycleType

	dest.limitValue = src.LimitValue

	dest.measureUnit = src.MeasureUnit

}


def listMapping9

listMapping9 =
{
src,dest  ->

	dest.effDate=parseDate(src.StartTime, Constant4Model.DATE_FORMAT)

	def destMidVar1 = dest.rscRelationInfo

	listMapping7.call(src.OfferingKey,destMidVar1.offeringKey)

	if(!src.OfferingKey.isNull()){

	destMidVar1.offeringKey._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"

	destMidVar1.offeringKey.oCode = src.OfferingKey.OfferingCode

	}

	listMapping8.call(src.ShareLimit,dest.shareLimitInfo)

	destMidVar1.shareRule = src.ShareRule

	destMidVar1.freeUnitType = src.FreeUnitType

	dest.expDate=parseDate(src.EndTime, Constant4Model.DATE_FORMAT)

	destMidVar1.freeUnitLimitTypeCode = src.ShareLimit.FreeUnitLimitType

	dest.priority = src.Priority
}

def srcMidVar6 = srcArgs0.BatchSwitchGroupMemberRequestMsg.BatchSwitchGroupMemberRequest.RscRelation

mappingList(srcMidVar6.AddRscRelation,destArgs1.addRelations,listMapping9)
