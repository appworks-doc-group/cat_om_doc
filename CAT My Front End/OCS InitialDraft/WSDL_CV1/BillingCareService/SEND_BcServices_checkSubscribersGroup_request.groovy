dest.setServiceOperation("BMGroupService","checkSubscribersGroup")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.group.checksubscribersgroup.io.CheckSubscribersGroupRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.CheckSubscribersGroupRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.CheckSubscribersGroupRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.CheckSubscribersGroupRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.CheckSubscribersGroupRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.CheckSubscribersGroupRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar.Version

def srcMidVar4 = srcArgs0.CheckSubscribersGroupRequestMsg.CheckSubscribersGroupRequest

destArgs1.groupType = srcMidVar4.GroupType

def destMidVar = destArgs1.peerSubAccessCode

def srcMidVar5 = srcArgs0.CheckSubscribersGroupRequestMsg.CheckSubscribersGroupRequest.PeerSubAccessCode

destMidVar.primaryIdentity = srcMidVar5.PrimaryIdentity

destMidVar.subscriberKey = srcMidVar5.SubscriberKey

def destMidVar0 = destArgs1.subAccessCode

def srcMidVar6 = srcArgs0.CheckSubscribersGroupRequestMsg.CheckSubscribersGroupRequest.SubAccessCode

destMidVar0.primaryIdentity = srcMidVar6.PrimaryIdentity

destMidVar0.subscriberKey = srcMidVar6.SubscriberKey
