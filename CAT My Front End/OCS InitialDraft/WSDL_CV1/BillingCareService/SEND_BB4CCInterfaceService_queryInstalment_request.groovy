dest.setServiceOperation("OCS33BB4CCInterfaceService", "queryInstalment")

def srcArgs0 = src.payload._args[0]
def srcMidVar0 = srcArgs0.QueryInstalmentRequestMsg.RequestHeader
def srcMidVar2 = srcArgs0.QueryInstalmentRequestMsg.QueryInstalmentRequest

def destArgs0 = dest.payload._args[0]
def destArgs1 = dest.payload._args[1]

destArgs0._class = "com.huawei.ngcbs.cm.ocs33ws.charge2Billing.ws.bo.BB4CCMessageHeader"
destArgs1._class = "com.huawei.ngcbs.cm.ocs33ws.instalment.queryinstalment.io.OCS33QueryInstalmentRequest"


def listMapping0
listMapping0 = 
{
    src,dest  ->
    
	dest.name = src.name
	
	dest.value = src.value
}




def listMapping1
listMapping1 = 
{
    src,dest  ->
    
	dest.otherId = src.OtherId
	
	dest.acctId = src.AcctId	
	
	dest.subId = src.SubId
	
	dest.adjustId = src.AdjustId

}

destArgs0.messageSeq = srcMidVar0.SerialNo
destArgs0.serialNo = srcMidVar0.SerialNo
destArgs0.requestFrom = srcMidVar0.RequestFrom
destArgs0.operatorId = srcMidVar0.OperatorId
destArgs0.departmentId = srcMidVar0.DepartmentId
destArgs0.version = srcMidVar0.Version
destArgs0.remark = srcMidVar0.Remark
mappingList(srcMidVar0.Param, destArgs0.param, listMapping0)
destArgs0.tenantId = srcMidVar0.TenantId
destArgs0.msgLanguageCode = srcMidVar0.Language

destArgs1.status = srcMidVar2.Status

mappingList(srcMidVar2.QueryCondList.QueryCondValue, destArgs1.queryCondList, listMapping1)
