def srcReturn = src.payload._return

def destReturn = dest.payload._return

def srcMidVar = srcReturn.resultHeader

srcMidVar._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12ResultHeader"

def destMidVar = destReturn.ExchangeAccountResultMsg.ResultHeader

destMidVar.CommandId = srcMidVar.commandId

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.SequenceId = srcMidVar.sequenceId

destMidVar.SerialNo = srcMidVar.serialNo

destMidVar.TransactionId = srcMidVar.transactionId

destMidVar.Version = srcMidVar.version

def srcMidVar0 = srcReturn.resultBody

srcMidVar0._class = "com.huawei.ngcbs.cm.ocs12ws.account.exchangeaccount.io.ExchangeAccountResult"

def destMidVar0 = destReturn.ExchangeAccountResultMsg.ExchangeAccountResult

destMidVar0.ExchangeHandlingCharge = srcMidVar0.exchangeHandlingCharge

def destMidVar1 = destReturn.ExchangeAccountResultMsg.ExchangeAccountResult.OriAcctChgRec

def srcMidVar1 = srcReturn.resultBody.oriAcctChgRec

destMidVar1.CurrAcctBal = srcMidVar1.currAcctBal

destMidVar1.ChgAcctBal = srcMidVar1.chgAcctBal

destMidVar1.CurrExpTime = srcMidVar1.currExpTime

destMidVar1.ChgExpTime = srcMidVar1.chgExpTime

destMidVar1.AccountType = srcMidVar1.accountType

destMidVar1.AccountID = srcMidVar1.accountId

destMidVar1.MinMeasureId = srcMidVar1.minMeasureId

def destMidVar2 = destReturn.ExchangeAccountResultMsg.ExchangeAccountResult.DesAcctChgRec

def srcMidVar2 = srcReturn.resultBody.desAcctChgRec

destMidVar2.CurrAcctBal = srcMidVar2.currAcctBal

destMidVar2.ChgAcctBal = srcMidVar2.chgAcctBal

destMidVar2.CurrExpTime = srcMidVar2.currExpTime

destMidVar2.ChgExpTime = srcMidVar2.chgExpTime

destMidVar2.AccountType = srcMidVar2.accountType

destMidVar2.AccountID = srcMidVar2.accountId

destMidVar2.MinMeasureId = srcMidVar2.minMeasureId
