def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.ZoneID = src.id
	
	dest.ZoneName = src.name
	
	dest.ZoneType = src.type
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def srcMidVar = srcReturn.zoneList

def destMidVar = destReturn.QueryZoneInfoResultMsg.QueryZoneInfoResult

mappingList(srcMidVar.zones,destMidVar.ZoneList,listMapping0)

def srcMidVar0 = srcReturn.resultHeader

def destMidVar0 = destReturn.QueryZoneInfoResultMsg.ResultHeader

mappingList(srcMidVar0.simpleProperty,destMidVar0.AdditionalProperty,listMapping1)

destMidVar0.ResultDesc = srcMidVar0.resultDesc

destMidVar0.ResultCode = srcMidVar0.resultCode

destMidVar0.MsgLanguageCode = srcMidVar0.msgLanguageCode

destMidVar0.Version = srcMidVar0.version
