dest.setServiceOperation("arUVSInterfaceExtend","uniDebitFee")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destArgs1 = dest.payload._args[1]

destArgs0._class = "com.huawei.ngcbs.cm.ocs11ws.core.bo.Ocs11MessageHeader"

destArgs1._class = "com.huawei.ngcbs.cm.ocs11ws.subscriber.unidebitfee.io.UniDebitFeeRequest"

def srcMessageHeader = srcArgs0.UniDebitFee.UniDebitFeeRequest.RequestMessage.MessageHeader

def srcMessageBody = srcArgs0.UniDebitFee.UniDebitFeeRequest.RequestMessage.MessageBody

def srcSessionEntity = srcArgs0.UniDebitFee.SessionEntity


destArgs0.loginSystem = srcSessionEntity.userID

destArgs0.password = srcSessionEntity.password


destArgs0.sessionEntity.userID = srcSessionEntity.userID

destArgs0.sessionEntity.password = srcSessionEntity.password

destArgs0.sessionEntity.locale = srcSessionEntity.locale

destArgs0.sessionEntity.loginVia = srcSessionEntity.loginVia

destArgs0.sessionEntity.remoteAddr = srcSessionEntity.remoteAddr

destArgs0.sessionEntity.uploadRoot = srcSessionEntity.uploadRoot

destArgs0.commandId = srcMessageHeader.CommandId

destArgs0.version = srcMessageHeader.Version

destArgs0.transactionId = srcMessageHeader.TransactionId

destArgs0.sequenceId = srcMessageHeader.SequenceId

destArgs0.requestType = srcMessageHeader.RequestType

destArgs0.tenantId = srcMessageHeader.TenantId

destArgs0.language = srcMessageHeader.Language


destArgs0.messageSeq = srcMessageBody.LogID

destArgs0.interMode = srcMessageBody.AccessMethod

destArgs1.subscriberNo = srcMessageBody.SubscriberNo

destArgs1.acctId = srcMessageBody.AcctID

destArgs1.logId = srcMessageBody.LogID

destArgs1.operId = srcMessageBody.OperID

destArgs1.deptId = srcMessageBody.DeptID

destArgs1.areaCode = srcMessageBody.AreaCode




def srcMidVar0 = srcMessageBody.FeeInfoList
def destMidVar0 = destArgs1.feeInfoList


def listMapping3
listMapping3 = 
{
    src,dest  ->
	dest.paraID = src.paraName
	  dest.paraName = src.paraName
	  dest.paraValue = src.paraValue
}

def listMapping2
listMapping2 = 
{
    src,dest  ->
    dest.taxCode = src.TaxCode
	  dest.taxAmt.value = src.TaxAmt.Value
	  dest.taxAmt.minMeasureId = src.TaxAmt.MinMeasureId
}

def listMapping1
listMapping1 = 
{
    src,dest  ->
    dest.productID = src.ProductID
	  dest.productOrderID = src.ProductOrderID
	  dest.salesProdDefID = src.salesProdDefID
	  
	  def src3 = src.ProParaList.ProPara
	  def dest3 = dest.proParaList.proPara 
	  mappingList(src3,dest3,listMapping3)

}

def listMapping0
listMapping0 = 
{
    src,dest  ->
	  dest.feeType = src.FeeType
	  dest.feeAmt.value = src.FeeAmt.Value
	  dest.feeAmt.minMeasureId = src.FeeAmt.MinMeasureId
	  
	  def dest1 = dest.productList.productItem 
	  def src1 = src.ProductList.ProductItem
	  mappingList(src1,dest1,listMapping1)
	  
	  dest.addCount = src.AddCount	
	  dest.modifyCount = src.ModifyCount
	  dest.queryCount = src.QueryCount	
	  dest.deleteCount = src.DeleteCount
	  dest.operationID = src.OperationID	
	  dest.opreationType = src.OperationType
	  dest.actionType = src.ActionType	
	  
	  def dest2 = dest.taxList.taxRecord 
	  def src2 = src.TaxList.TaxRecord
	   mappingList(src2,dest2,listMapping2)
	  	  
	  dest.additionalInfo = src.AdditionalInfo	
	  dest.cugType = src.CugType
	  dest.cugId = src.CugId	
	  	
}
mappingList(srcMidVar0.FeeInfoRecord,destMidVar0.feeInfoRecord,listMapping0)