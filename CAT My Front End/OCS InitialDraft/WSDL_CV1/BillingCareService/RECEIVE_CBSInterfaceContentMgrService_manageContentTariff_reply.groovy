def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.ManageContentTariffResultMsg.ResultHeader

def srcReturnHeader = srcReturn.resultHeader

destMidVar.TransactionId = srcReturnHeader.transactionId

destMidVar.SequenceId = srcReturnHeader.sequenceId

destMidVar.ResultCode = srcReturnHeader.resultCode

destMidVar.ResultDesc = srcReturnHeader.resultDesc

destMidVar.Version = srcReturnHeader.version

destMidVar.CommandId = srcReturnHeader.commandId

def destMidVar1 = destReturn.ManageContentTariffResultMsg.ManageContentTariffResult

def listMapping0

listMapping0 =
        {
            src, dest ->
                dest.ProductID = src.productID
                dest.TariffGroup = src.tariffGroup
                dest.TariffName = src.tariffName
        }


mappingList(srcReturn.productInfoList, destMidVar1.ProductInfo, listMapping0)

def listMapping1

listMapping1 =
        {
            src, dest ->
                dest.TariffType = src.tariffType
                dest.TariffTypeDesc = src.tariffTypeDesc
        }

mappingList(srcReturn.tariffTypesList, destMidVar1.TariffTypes, listMapping1)

def listMapping3

listMapping3 =
        {
            src, dest ->
                dest.Amount = src.amount
        }

def listMapping4

listMapping4 =
        {
            src, dest ->
                dest.Unit = src.unit
                dest.UnitMeasure = src.unitMeasure
                dest.Ratio = src.ratio
                dest.RatioMeasure = src.ratioMeasure
        }

def listMapping2

listMapping2 =
        {
            src, dest ->
                dest.TariffID = src.tariffID
                mappingList(src.rentList, dest.Rent, listMapping3)
                listMapping4.call(src.feeRateList, dest.FeeRate)
        }
mappingList(srcReturn.priceInfoList, destMidVar1.PriceInfo, listMapping2)

def listMapping6

listMapping6 =
        {
            src, dest ->
                dest.TariffType = src.tariffType
                dest.TariffID = src.tariffID
                dest.SpecialUsageFlag = src.specialUsageFlag
                dest.GamblingOfferFlag = src.gamblingOfferFlag
                dest.ContentFreeFlag = src.contentFreeFlag
        }

def listMapping5

listMapping5 =
        {
            src, dest ->
                dest.ProductID = src.productID
                dest.TariffGroup = src.tariffGroup
                dest.ServiceType = src.serviceType
                dest.CustomType = src.customType
                dest.IsDefault = src.isDefault
                mappingList(src.tariffInfoList, dest.TariffInfo, listMapping6)
                dest.TariffName = src.tariffName
        }

mappingList(srcReturn.tariffList, destMidVar1.Tariff, listMapping5)