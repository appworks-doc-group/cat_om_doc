def srcReturn = src.payload._return

def destReturn = dest.payload._return

def srcMidVar = srcReturn.notifyBulkLoadResultResponse.CommResult

destReturn.resultCode = srcMidVar.RetCode

destReturn.resultDesc = srcMidVar.RetDesc

destReturn._class = "com.huawei.ngcbs.bm.framework.batch.io.BulkLoadResultMessage"
