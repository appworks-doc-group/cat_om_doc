def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcReturn.UpdateCashRegisterResultMsg.ResultHeader

def destMidVar = destReturn.resultHeader

mappingList(srcMidVar.AdditionalProperty,destMidVar.simpleProperty,listMapping0)

destMidVar.version = srcMidVar.Version

destMidVar.resultCode = srcMidVar.ResultCode

destMidVar.resultDesc = srcMidVar.ResultDesc

destMidVar.msgLanguageCode = srcMidVar.MsgLanguageCode

destReturn._class = "com.huawei.ngcbs.cm.common.ws.cashregister.update.io.UpdateCashRegisterResult"
