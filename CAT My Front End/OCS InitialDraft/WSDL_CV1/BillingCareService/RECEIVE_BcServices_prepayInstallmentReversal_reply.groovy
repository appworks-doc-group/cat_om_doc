import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 =
        {
            src, dest ->

                dest.Code = src.code

                dest.Value = src.value

        }

def listMapping1

listMapping1 =
        {
            src, dest ->

                dest.CycleSequence = src.cycleSequence

                dest.InitialAmount = src.initialAmount

                dest.CycleAmount = src.cycleAmount

                dest.CycleClass = src.cycleClass

                dest.Status = src.cycleStatus

                dest.RepayDueDate = formatDate(src.repayDueDate, Constant4Model.DATE_FORMAT)

                dest.CreateDate = formatDate(src.createDate, Constant4Model.DATE_FORMAT)

                dest.ExpDate = formatDate(src.expDate, Constant4Model.DATE_FORMAT)

                dest.DelayFlag = src.paymentDelayFlag

                dest.NewFlag = src.newFlag

                dest.PrevCycleAmount = src.prevCycleAmount

                dest.PrevInitialAmount = src.prevInitialAmount

                dest.PrevCycleStatus = src.prevCycleStatus

                dest.PrevRepayDueDate = formatDate(src.prevRepayDueDate, Constant4Model.DATE_FORMAT)

        }

def listMapping2

listMapping2 =
        {
            src, dest ->
                dest.CycleSequence = src.cycleSequence
                dest.PrepayAmount = src.prevAmount
        }


def destMidVar = destReturn.PrepayInstallmentReversalResultMsg.ResultHeader

def srcMidVar0 = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar0.msgLanguageCode

destMidVar.ResultCode = srcMidVar0.resultCode

destMidVar.ResultDesc = srcMidVar0.resultDesc

destMidVar.Version = srcMidVar0.version

destMidVar.MessageSeq = srcMidVar0.messageSeq

mappingList(srcMidVar0.simpleProperty, destMidVar.AdditionalProperty, listMapping0)


def srcMidVar1 = srcReturn.installmentReversalDetailList

def destMidVar1 = destReturn.PrepayInstallmentReversalResultMsg.PrepayInstallmentReversalResult

mappingList(srcMidVar1.installmentReversalDetail,destMidVar1.InstallmentChgDetailList,listMapping1)

def srcMidVar2 = srcReturn.unReversalTransDetailList
mappingList(srcMidVar2.unReversalTransDetailList,destMidVar1.UnReversalTransDetailList,listMapping2)
