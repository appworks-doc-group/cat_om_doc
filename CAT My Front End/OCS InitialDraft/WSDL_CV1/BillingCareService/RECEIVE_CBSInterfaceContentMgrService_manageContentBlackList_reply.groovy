def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.ManageContentBlackListResultMsg.ResultHeader

def srcReturnHeader = srcReturn.resultHeader

destMidVar.TransactionId = srcReturnHeader.transactionId

destMidVar.SequenceId = srcReturnHeader.sequenceId

destMidVar.ResultCode = srcReturnHeader.resultCode

destMidVar.ResultDesc = srcReturnHeader.resultDesc

destMidVar.Version = srcReturnHeader.version

destMidVar.CommandId = srcReturnHeader.commandId

def listMapping0

listMapping0 =
        {
            src, dest ->
                dest.ServiceType = src.serviceType
                dest.Source = src.source
                dest.Reason = src.reason
        }


def destMidVar1 = destReturn.ManageContentBlackListResultMsg.ManageContentBlackListResult

mappingList(srcReturn.blackListInfoList,destMidVar1.BlackList,listMapping0)


