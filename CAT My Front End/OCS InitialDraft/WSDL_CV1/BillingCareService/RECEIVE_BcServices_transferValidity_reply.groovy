def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping_fuChg

listMapping_fuChg = 
{
    src,dest  ->

	dest.FreeUnitInstanceID = src.freeUnitInstanceId
	
	dest.FreeUnitType = src.freeUnitType
	
	dest.FreeUnitTypeName = src.freeUnitTypeName
	
	dest.MeasureUnit = src.measureUnit
	
	dest.MeasureUnitName = src.measureUnitName
	
	dest.OldAmt = src.oldAmt
	
	dest.NewAmt = src.newAmt
	
}

def listMapping_balChg

listMapping_balChg = 
{
    src,dest  ->

	dest.BalanceType = src.balanceType
	
	dest.BalanceID = src.balanceId
	
	dest.BalanceTypeName = src.balanceTypeName
	
	dest.OldBalanceAmt = src.oldBalanceAmt
	
	dest.NewBalanceAmt = src.newBalanceAmt
	
	dest.CurrencyID = src.currencyId
	
}

def listMapping_oldLifeChgor

listMapping_oldLifeChgor = 
{
    src,dest  ->

	dest.StatusName = src.statusName
	
	dest.StatusIndex = src.statusIndex
	
	dest.StatusExpireTime = src.statusExpireTimeStr
	
}

def listMapping_newLifeChgor

listMapping_newLifeChgor = 
{
    src,dest  ->

	dest.StatusName = src.statusName
	
	dest.StatusIndex = src.statusIndex
	
	dest.StatusExpireTime = src.statusExpireTimeStr
	
}

def listMapping_oldLifeChgee

listMapping_oldLifeChgee = 
{
    src,dest  ->

	dest.StatusName = src.statusName
	
	dest.StatusIndex = src.statusIndex
	
	dest.StatusExpireTime = src.statusExpireTimeStr
	
}

def listMapping_newLifeChgee

listMapping_newLifeChgee = 
{
    src,dest  ->

	dest.StatusName = src.statusName
	
	dest.StatusIndex = src.statusIndex
	
	dest.StatusExpireTime = src.statusExpireTimeStr
	
}

def destMidVar = destReturn.TransferValidityResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.Version = srcMidVar.version

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.MessageSeq = srcMidVar.messageSeq

addingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def destMidVar1 = destReturn.TransferValidityResultMsg.TransferValidityResult


def destMidVar1_transferor = destMidVar1.Transferor
def srcMidVar1 = srcReturn.transferValidityResultInfo

srcMidVar1_transferor = srcMidVar1.transferor

mappingList(srcMidVar1_transferor.balanceChgInfoList, destMidVar1_transferor.BalanceChgInfo, listMapping_balChg)

mappingList(srcMidVar1_transferor.freeUnitChgInfoList, destMidVar1_transferor.FreeUnitChgInfo, listMapping_fuChg)

destMidVar1_transferor.HandlingChargeAmt = srcMidVar1_transferor.handlingChargeAmt

destMidVar1_transferor.CurrencyID = srcMidVar1_transferor.currencyID

destMidVar1_transferor.RemainNumTransOutMonth = srcMidVar1_transferor.remainNumTransOutMonth

mappingList(srcMidVar1_transferor.lifeCycleChgInfo.oldLifeCycleStatusList, destMidVar1_transferor.LifeCycleChgInfo.OldLifeCycleStatus, listMapping_oldLifeChgor)

mappingList(srcMidVar1_transferor.lifeCycleChgInfo.newLifeCycleStatusList, destMidVar1_transferor.LifeCycleChgInfo.NewLifeCycleStatus, listMapping_newLifeChgor)

destMidVar1_transferor.LifeCycleChgInfo.ChgValidity = srcMidVar1_transferor.lifeCycleChgInfo.chgValidity


def destMidVar1_transferee = destMidVar1.Transferee
srcMidVar1_transferee = srcMidVar1.transferee

destMidVar1_transferee.RemainNumTransInMonth = srcMidVar1_transferee.remainNumTransInMonth

mappingList(srcMidVar1_transferee.lifeCycleChgInfo.oldLifeCycleStatusList, destMidVar1_transferee.LifeCycleChgInfo.OldLifeCycleStatus, listMapping_oldLifeChgee)

mappingList(srcMidVar1_transferee.lifeCycleChgInfo.newLifeCycleStatusList, destMidVar1_transferee.LifeCycleChgInfo.NewLifeCycleStatus, listMapping_newLifeChgee)

destMidVar1_transferee.LifeCycleChgInfo.ChgValidity = srcMidVar1_transferee.lifeCycleChgInfo.chgValidity