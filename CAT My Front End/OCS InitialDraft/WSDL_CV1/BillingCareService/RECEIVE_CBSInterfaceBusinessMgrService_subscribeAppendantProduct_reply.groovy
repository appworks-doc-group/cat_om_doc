def srcReturn = src.payload._return

def destReturn = dest.payload._return

def srcMidVar = srcReturn.resultHeader

srcMidVar._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12ResultHeader"

def destMidVar = destReturn.SubscribeAppendantProductResultMsg.ResultHeader

destMidVar.CommandId = srcMidVar.commandId

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.SequenceId = srcMidVar.sequenceId

destMidVar.TransactionId = srcMidVar.transactionId

destMidVar.Version = srcMidVar.version

def srcMidVar0 = srcReturn.resultBody

srcMidVar0._class = "com.huawei.ngcbs.cm.ocs12ws.subscriber.changeappprods.io.ChangeAppProdsResultOcs12Base"

def destMidVar0 = destReturn.SubscribeAppendantProductResultMsg.SubscribeAppendantProductResult

def srcMidVar1 = srcReturn.resultBody

def listProdOrderMapping = 
{
  src,dest  ->

  dest.AutoType = src.autoType
  dest.EffectiveDate = src.effectiveDate
  dest.ExpireDate = src.expireDate
  dest.ProductID = src.productId
  dest.ProductOrderKey = src.productOrderKey
}

mappingList(srcMidVar1.prodOrderInfoList, destMidVar0.ProductOrderInfo, listProdOrderMapping)
