def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.ManSubFamilyNoResultMsg.ResultHeader

destMidVar.CommandId = srcReturn.commandId

destMidVar.ResultCode = srcReturn.resultCode

destMidVar.ResultDesc = srcReturn.resultDesc

destMidVar.SequenceId = srcReturn.sequenceId


destMidVar.TransactionId = srcReturn.transactionId

destMidVar.Version = srcReturn.version
