dest.setServiceOperation("EventAccessService","getRecommendedOffer")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.key = src.attrMapKey
	
	dest.value = src.attrMapValue
	
}

def destMidVar = destArgs0.getRecommendedOffer.requestHeader

def srcMidVar = srcArgs0.requestHeader

destMidVar.accessChannel = srcMidVar.accessChannel

destMidVar.beId = srcMidVar.beId

destMidVar.language = srcMidVar.language

destMidVar.operator = srcMidVar.operator

destMidVar.password = srcMidVar.password

destMidVar.transactionId = srcMidVar.transactionId

def destMidVar0 = destArgs0.getRecommendedOffer.eventBody

def srcMidVar0 = srcArgs0.eventBody

destMidVar0.msisdn = srcMidVar0.msisdn

destMidVar0.eventCode = srcMidVar0.eventCode

mappingList(srcMidVar0.eventAttrMap,destMidVar0.eventAttrMap,listMapping0)
