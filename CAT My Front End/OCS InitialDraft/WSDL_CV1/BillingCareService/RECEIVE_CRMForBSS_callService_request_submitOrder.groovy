import com.huawei.ngcbs.bm.common.common.Constant4Model
dest.setServiceOperation("HuaweiCRM","submitOrder")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.crm.service.SubmitOrderRequestDocument"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.ParamName = src.paraName
	
	dest.ParamValue = src.paraValue
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.ParamName = src.paraName
	
	dest.ParamValue = src.paraValue
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.ParamName = src.paraName
	
	dest.ParamValue = src.paraValue
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	def destMidVar9 = dest.ExtParamList
	
	mappingList(src.extParmalist,destMidVar9.ParameterInfo,listMapping5)
	
	dest.ProductId = src.productId
	
	dest.SelectFlag = "2"
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.ActiveMode = src.activeMode
	
	dest.EffectMode = src.effectiveMode
	
	dest.EffectiveTime=formatDate(src.effectiveTime, Constant4Model.DATE_FORMAT)
	
	dest.ExpiredTime=formatDate(src.expireTime, Constant4Model.DATE_FORMAT)
	
	dest.ActionType = "1"
	
	def destMidVar6 = dest.ExtParamList
	
	mappingList(src.extParmalist,destMidVar6.ParameterInfo,listMapping3)
	
	dest.LatestActiveDate = src.latestActiveDate
	
	def destMidVar7 = dest.OfferingId
	
	destMidVar7.OfferingId = src.offeringId
	
	destMidVar7.PurchaseSeq = src.purchaseSeq
	
	dest.OwnerType = src.ownerType
	
	dest.Status = src.statu
	
	def destMidVar8 = dest.ProductList
	
	mappingList(src.productInstInfoList,destMidVar8.ProductInfo,listMapping4)
	
	dest.ParentOfferingId = src.parentOfferingId
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	def destMidVar3 = dest.Subscriber
	
	destMidVar3.SubscriberId = src.tpSubKey
	
	def destMidVar4 = dest.OrderItemInfo
	
	destMidVar4.OrderItemType = "CO025"
	
	destMidVar4.IsCustomerNotification = "0"
	
	destMidVar4.IsPartnerNotification = "0"
	
	def destMidVar5 = dest.Subscriber.SupplementaryOfferingList
	
	mappingList(src.offeringInfoList,destMidVar5.OfferingInfo,listMapping2)
	
}

def destMidVar = destArgs0.SubmitOrderRequest.RequestHeader

def srcMidVar = srcArgs0.workOrderHeader

destMidVar.AccessPwd = srcMidVar.accessPwd

destMidVar.AccessUser = srcMidVar.accessUser

destMidVar.TenantId = srcMidVar.beId

destMidVar.ChannelId = srcMidVar.channelId

def destMidVar0 = destArgs0.SubmitOrderRequest.RequestHeader.ExtParamList

mappingList(srcMidVar.extParamList,destMidVar0.ParameterInfo,listMapping0)

destMidVar.Language = srcMidVar.language

destMidVar.OperatorId = srcMidVar.operatorId

destMidVar.OperatorPwd = srcMidVar.operatorPwd

destMidVar.ProcessTime=srcMidVar.processTime

destMidVar.SessionId = srcMidVar.sessionId

destMidVar.TestFlag = srcMidVar.testFlag

destMidVar.TransactionId = srcMidVar.transactionId

destMidVar.Version = srcMidVar.version

def destMidVar1 = destArgs0.SubmitOrderRequest.SubmitRequestBody.Order

destMidVar1.CustomerId = srcArgs0.tprCustKey

destMidVar1.OrderType = "CO002"

def destMidVar2 = destArgs0.SubmitOrderRequest.SubmitRequestBody.OrderItems

mappingList(srcArgs0.changeOfferingList,destMidVar2.OrderItem,listMapping1)

destMidVar.TechnicalChannelId = srcMidVar.technicalChannelId

destMidVar.ExternalOrderId = srcMidVar.externalOrderId

destMidVar.OrderId = srcMidVar.orderId
