dest.setServiceOperation("SubmgrtMgrService","changeCustBasicInfor")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destArgs1 = dest.payload._args[1]

destArgs0._class = "com.huawei.ngcbs.cm.ocs33ws.core.bo.Ocs33MessageHeader"

destArgs1._class = "com.huawei.ngcbs.cm.ocs33ws.customer.custinfo.io.OCS33ChangeCustBasicInforRequest"

def destArgs2 = destArgs1.customer

def srcMessageHeader = srcArgs0.ChangeCustBasicInforRequestMsg.RequestHeader

def srcMessageBody = srcArgs0.ChangeCustBasicInforRequestMsg.ChangeCustBasicInforRequest

def srcCust = srcMessageBody.Customer

destArgs0.beId  = srcMessageHeader.TenantId
destArgs0.msgLanguageCode  = srcMessageHeader.Language
destArgs0.commandId = srcMessageHeader.CommandId
destArgs0.version = srcMessageHeader.Version
destArgs0.transactionId = srcMessageHeader.TransactionId
destArgs0.sequenceId = srcMessageHeader.SequenceId
destArgs0.requestType = srcMessageHeader.RequestType
destArgs0.loginSystem = srcMessageHeader.SessionEntity.Name
destArgs0.password = srcMessageHeader.SessionEntity.Password
destArgs0.remoteAddress = srcMessageHeader.SessionEntity.RemoteAddress
destArgs0.interFrom = srcMessageHeader.InterFrom
destArgs0.interMedi = srcMessageHeader.InterMedi
destArgs0.interMode = srcMessageHeader.InterMode
destArgs0.visitArea = srcMessageHeader.visitArea
destArgs0.currentCell = srcMessageHeader.currentCell
destArgs0.additionInfo = srcMessageHeader.additionInfo
destArgs0.thirdPartyId = srcMessageHeader.ThirdPartyID
destArgs0.partnerId = srcMessageHeader.PartnerID
destArgs0.operatorId = srcMessageHeader.OperatorID
destArgs0.tradePartnerId = srcMessageHeader.TradePartnerID
destArgs0.partnerOperId = srcMessageHeader.PartnerOperID
destArgs0.belToAreaId = srcMessageHeader.BelToAreaID
destArgs0.reserve2 = srcMessageHeader.Reserve2
destArgs0.reserve3 = srcMessageHeader.Reserve3
destArgs0.messageSeq = srcMessageHeader.SerialNo
destArgs0.remark = srcMessageHeader.Remark
destArgs0.performanceStatCmd = "ChangeCustBasicInfor"

destArgs2.firstName = srcCust.FirstName
destArgs2.middleName = srcCust.MiddleName
destArgs2.lastName = srcCust.LastName
destArgs2.customerID = srcCust.CustomerID
destArgs2.customerCode = srcCust.CustomerCode
destArgs2.idType = srcCust.IdType
destArgs2.idCode = srcCust.IdCode
destArgs2.gender = srcCust.Gender
destArgs2.birthday = srcCust.Birthday
destArgs2.title = srcCust.Title
destArgs2.salary = srcCust.Salary
destArgs2.homeAddress = srcCust.HomeAddress
destArgs2.grade = srcCust.Grade
destArgs2.email = srcCust.Email
destArgs2.zipCode = srcCust.ZipCode
destArgs2.country = srcCust.Country
destArgs2.nativePlace = srcCust.NativePlace
destArgs2.nationType = srcCust.NationType
destArgs2.jobType = srcCust.JobType
destArgs2.education = srcCust.Education
destArgs2.maritalStatus = srcCust.MaritalStatus
destArgs2.skill = srcCust.Skill
destArgs2.socialNo = srcCust.SocialNo
destArgs2.creditGrade = srcCust.CreditGrade
destArgs2.creditAmount = srcCust.CreditAmount
destArgs2.customerState = srcCust.CustomerState
destArgs2.contactNumber1 = srcCust.ContactNumber1
destArgs2.contactNumber2 = srcCust.ContactNumber2
destArgs2.contactNumber3 = srcCust.ContactNumber3
destArgs2.contactNumber4 = srcCust.ContactNumber4
destArgs2.contactNumber5 = srcCust.ContactNumber5
destArgs2.systemNofityNumber = srcCust.SystemNofityNumber
destArgs2.homePhone = srcCust.HomePhone
destArgs2.workPhone = srcCust.WorkPhone
destArgs2.belToAreaID = srcCust.BelToAreaID
destArgs2.postAddress1 = srcCust.PostAddress1
destArgs2.postAddress2 = srcCust.PostAddress2
destArgs2.postAddress3 = srcCust.PostAddress3
destArgs2.postAddress4 = srcCust.PostAddress4
destArgs2.postAddress5 = srcCust.PostAddress5
destArgs2.remark = srcCust.Remark
destArgs2.langType = srcCust.LangType
destArgs2.timeZone = srcCust.TimeZone
def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.id = src.Id
	
	dest.value = src.Value
	
}
mappingList(srcCust.SimpleProperty,destArgs2.simplePropertys,listMapping0)

destArgs1.subscriberNo = srcMessageBody.SubscriberNo

destArgs1.customerCode = srcMessageBody.CustomerCode

destArgs1.accountCode = srcMessageBody.AccountCode




