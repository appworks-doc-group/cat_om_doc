def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.ChangeConsumptionLimitResultMsg.ResultHeader

destMidVar.MsgLanguageCode = srcReturn.msgLanguageCode

destMidVar.ResultCode = srcReturn.resultCode

destMidVar.ResultDesc = srcReturn.resultDesc

destMidVar.Version = srcReturn.version

def destMidVar0 = destReturn.ChangeConsumptionLimitResultMsg.ResultHeader.AdditionalProperty[0]

def srcMidVar = srcReturn.simpleProperty[0]

destMidVar0.Code = srcMidVar.code

destMidVar0.Value = srcMidVar.value
