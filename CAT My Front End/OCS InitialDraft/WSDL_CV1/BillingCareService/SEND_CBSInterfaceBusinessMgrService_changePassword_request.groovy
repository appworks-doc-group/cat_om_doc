dest.setServiceOperation("CBSInterfaceBusinessMgrService","changePassword");

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destArgs1 = dest.payload._args[1]

def srcMidVar = srcArgs0.ChangePasswordRequestMsg.RequestHeader

destArgs0.beId = srcMidVar.TenantId

destArgs0.operatorId = srcMidVar.OperatorID

destArgs0.additionInfo = srcMidVar.additionInfo

destArgs0.commandId = srcMidVar.CommandId

destArgs0.interMedi = srcMidVar.InterMedi

destArgs0.transactionId = srcMidVar.TransactionId

destArgs0.reserve2 = srcMidVar.Reserve2

destArgs0.thirdPartyId = srcMidVar.ThirdPartyID

destArgs0.reserve3 = srcMidVar.Reserve3

destArgs0.sequenceId = srcMidVar.SequenceId

destArgs0.visitArea = srcMidVar.visitArea

destArgs0.messageSeq = srcMidVar.SerialNo

destArgs0.interMode = srcMidVar.InterMode

destArgs0.belToAreaId = srcMidVar.BelToAreaID

destArgs0.currentCell = srcMidVar.currentCell

destArgs0.partnerId = srcMidVar.PartnerID

destArgs0.remark = srcMidVar.Remark

destArgs0.tradePartnerId = srcMidVar.TradePartnerID

destArgs0.version = srcMidVar.Version

destArgs0.partnerOperId = srcMidVar.PartnerOperID

def srcMidVar0 = srcArgs0.ChangePasswordRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar0.Name

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteAddress

destArgs0.requestType = srcMidVar.RequestType

destArgs0.interFrom = srcMidVar.InterFrom

def destMidVar = destArgs1.subAccessCode

def srcMidVar1 = srcArgs0.ChangePasswordRequestMsg.ChangePasswordRequest

destMidVar.primaryIdentity = srcMidVar1.SubscriberNo

destArgs1.handlingChargeFlag = srcMidVar1.HandlingChargeFlag

def destMidVar0 = destArgs1.changeSubPwdInfo

destMidVar0.oldPassword = srcMidVar1.OldPassword

destMidVar0.newPassword = srcMidVar1.NewPassword

destMidVar0.opType = 2
