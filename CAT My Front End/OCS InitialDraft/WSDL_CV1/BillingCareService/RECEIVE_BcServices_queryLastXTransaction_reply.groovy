import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 =
{
    src, dest ->

    dest.Code = src.code

    dest.Value = src.value
}

def listMapping1

listMapping1 =
{
    src, dest ->

    dest.PrimaryIdentity = src.primaryIdentity

    dest.ServiceType = src.serviceType

    dest.SubServiceType = src.subServiceType

    dest.RoamingType = src.roamingType

    dest.OppositeNumber = src.oppositeNumber

    dest.StartTime = formatDate(src.startTime, Constant4Model.DATE_FORMAT)

    dest.ServiceUsage = src.serviceUsage

    dest.ChargeAmount = src.chargeAmount

    dest.CurrencyID = src.currencyID
}

def destMidVar = destReturn.QueryLastXTransactionResultMsg.ResultHeader

def srcMidVar0 = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar0.msgLanguageCode

destMidVar.ResultCode = srcMidVar0.resultCode

destMidVar.ResultDesc = srcMidVar0.resultDesc

destMidVar.Version = srcMidVar0.version

destMidVar.MessageSeq = srcMidVar0.messageSeq

mappingList(srcMidVar0.simpleProperty, destMidVar.AdditionalProperty, listMapping0)

def destMidVar1 = destReturn.QueryLastXTransactionResultMsg.QueryLastXTransactionResult

def srcMidVar1 = srcReturn.lastXRecords

mappingList(srcMidVar1.lastXRecord, destMidVar1.LastXRecords, listMapping1)