def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.query.queryappendsubiden.io.QueryAppendSubIdentityRequest"

def srcMidVar = srcArgs0.QueryAppendSubIdentityRequestMsg.RequestHeader

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.QueryAppendSubIdentityRequestMsg.RequestHeader.OperatorInfo

destArgs0.operatorId = srcMidVar0.OperatorID

destArgs0.channelId = srcMidVar0.ChannelID

def listMapping0

listMapping0 = 
{
    src0,dest0  ->

	dest0.value = src0.Value
	
	dest0.code = src0.Code
	
}

addingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.QueryAppendSubIdentityRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar1.LoginSystemCode

destArgs0.password = srcMidVar1.Password

destArgs0.remoteAddress = srcMidVar1.RemoteIP

destArgs0.version = srcMidVar.Version

def srcMidVar2 = srcArgs0.QueryAppendSubIdentityRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.QueryAppendSubIdentityRequestMsg.RequestHeader.TimeFormat

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.timeType = srcMidVar3.TimeType

def destMidVar = destArgs1.subAccessCode

def srcMidVar4 = srcArgs0.QueryAppendSubIdentityRequestMsg.QueryAppendSubIdentityRequest

destMidVar.primaryIdentity = srcMidVar4.PrimaryIdentity
