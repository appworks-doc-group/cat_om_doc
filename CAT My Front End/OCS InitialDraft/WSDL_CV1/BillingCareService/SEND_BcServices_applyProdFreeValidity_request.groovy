dest.setServiceOperation("SubscriberService","applyProdFreeValidity")
def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.applyprodfreevalidity.io.ApplyProdFreeValidityRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.ApplyProdFreeValidityRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar.BEID

def srcMidVar0 = srcArgs0.ApplyProdFreeValidityRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar0.AccessMode

def srcMidVar1 = srcArgs0.ApplyProdFreeValidityRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar1.LoginSystemCode

destArgs0.password = srcMidVar1.Password

destArgs0.remoteAddress = srcMidVar1.RemoteIP

mappingList(srcMidVar0.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar0.BusinessCode

destArgs0.messageSeq = srcMidVar0.MessageSeq

destArgs0.msgLanguageCode = srcMidVar0.MsgLanguageCode

def srcMidVar2 = srcArgs0.ApplyProdFreeValidityRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar2.ChannelID

destArgs0.operatorId = srcMidVar2.OperatorID

destArgs0.brId = srcMidVar.BRID

def srcMidVar3 = srcArgs0.ApplyProdFreeValidityRequestMsg.RequestHeader.TimeFormat

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar0.Version

destArgs0.timeType = srcMidVar3.TimeType

def srcMidVar4 = srcArgs0.ApplyProdFreeValidityRequestMsg.ApplyProdFreeValidityRequest

destArgs1.extendDays = srcMidVar4.ExtendDays

def destMidVar = destArgs1.offeringKey

def srcMidVar5 = srcArgs0.ApplyProdFreeValidityRequestMsg.ApplyProdFreeValidityRequest.OfferingKey

destMidVar.oCode = srcMidVar5.OfferingCode

destMidVar.purchaseSeq = srcMidVar5.PurchaseSeq

destMidVar.oId = srcMidVar5.OfferingID

destMidVar.pSeq = srcMidVar5.PurchaseSeq

def destMidVar0 = destArgs1.subAccessCode

def srcMidVar6 = srcArgs0.ApplyProdFreeValidityRequestMsg.ApplyProdFreeValidityRequest.SubAccessCode

destMidVar0.primaryIdentity = srcMidVar6.PrimaryIdentity

destMidVar0.subscriberKey = srcMidVar6.SubscriberKey
