import com.huawei.ngcbs.bm.common.common.Constant4Model

def destReturnDoc = dest.payload._return
def destReturnData = destReturnDoc.ChangeOptionalOfferResultMsg.ChangeOptionalOfferResult
def destHeader = destReturnDoc.ChangeOptionalOfferResultMsg.ResultHeader

def srcReturnData = src.payload._return
def srcHeader = srcReturnData.resultHeader
def srcBusinessData = srcReturnData.resultBody

destHeader.CommandId = srcHeader.commandId
destHeader.ResultCode = srcHeader.resultCode
destHeader.ResultDesc = srcHeader.resultDesc
destHeader.SequenceId = srcHeader.sequenceId
destHeader.Version = srcHeader.version
destHeader.TransactionId = srcHeader.transactionId
destHeader.OrderId = srcHeader.orderId
destHeader.OperationTime = srcHeader.operationTime

srcBusinessData._class = "com.huawei.ngcbs.cm.ocs33ws.subscriber.subscribe.io.OCS33ChangeOptionalOfferResult"

def listOfferOrderPropMapping = 
{
	src,dest  ->
	
	dest.Id = src.id
	dest.Value = src.value
}

def listOfferOrderMapping = 
{
  src,dest  ->

	dest.OfferId = src.offerId
	dest.OfferOrderKey = src.offerOrderKey		
	dest.EffectiveDate = src.effectiveDate	
	dest.ExpireDate = src.expireDate
	dest.AutoType = src.autoType
    dest.OfferCode = src.offerCode
    mappingList(src.properties, dest.SimpleProperty, listOfferOrderPropMapping)
}

mappingList(srcBusinessData.offerOrderInfos, destReturnData.OfferOrderInfo, listOfferOrderMapping)
