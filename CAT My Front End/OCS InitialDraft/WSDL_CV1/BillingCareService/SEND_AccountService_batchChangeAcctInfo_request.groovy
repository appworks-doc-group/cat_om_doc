def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.account.batch.changeacctinfo.io.BatchChangeAcctInfoRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.BatchChangeAcctInfoRequestMsg.RequestHeader

destArgs0.version = srcMidVar.Version

def srcMidVar0 = srcArgs0.BatchChangeAcctInfoRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar0.TimeType

destArgs0.timeZoneId = srcMidVar0.TimeZoneID

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.BatchChangeAcctInfoRequestMsg.RequestHeader.OperatorInfo

destArgs0.operatorId = srcMidVar1.OperatorID

destArgs0.channelId = srcMidVar1.ChannelID

def srcMidVar2 = srcArgs0.BatchChangeAcctInfoRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar2.LoginSystemCode

destArgs0.password = srcMidVar2.Password

destArgs0.interMode = srcMidVar.AccessMode

destArgs0.remoteAddress = srcMidVar2.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar.BusinessCode

def srcMidVar3 = srcArgs0.BatchChangeAcctInfoRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar3.BEID

destArgs0.brId = srcMidVar3.BRID

def destMidVar = destArgs1.changeAcctBasicInfoInfo.accountInfo

def srcMidVar4 = srcArgs0.BatchChangeAcctInfoRequestMsg.BatchChangeAcctInfoRequest.AcctBasicInfo

destMidVar.acctName = srcMidVar4.AcctName

def destMidVar0 = destArgs1.changeAcctBasicInfoInfo

mappingList(srcMidVar4.AcctProperty,destMidVar0.propertyInfos,listMapping1)

destMidVar.billLang = srcMidVar4.BillLang

def destMidVar1 = destArgs1.changeAcctBasicInfoInfo.contactInfo

def srcMidVar5 = srcArgs0.BatchChangeAcctInfoRequestMsg.BatchChangeAcctInfoRequest.AcctBasicInfo.ContactInfo

destMidVar1.addrKey = srcMidVar5.AddressKey

destMidVar1.email = srcMidVar5.Email

destMidVar1.fax = srcMidVar5.Fax

destMidVar1.firstName = srcMidVar5.FirstName

destMidVar1.homePhone = srcMidVar5.HomePhone

destMidVar1.lastName = srcMidVar5.LastName

destMidVar1.middleName = srcMidVar5.MiddleName

destMidVar1.mobilePhone = srcMidVar5.MobilePhone

destMidVar1.officePhone = srcMidVar5.OfficePhone

destMidVar1.title = srcMidVar5.Title

destMidVar.dunningFlag = srcMidVar4.DunningFlag

def srcMidVar6 = srcArgs0.BatchChangeAcctInfoRequestMsg.BatchChangeAcctInfoRequest

destArgs1.requestFileName = srcMidVar6.FileName

destMidVar0.redlistFlag = srcMidVar4.RedlistFlag

destMidVar.latePaymentFlag = srcMidVar4.LateFeeChargeable

def destMidVar2 = destArgs1.changeAcctBasicInfoInfo.accountInfo.bmInfos[0]

def srcMidVar7 = srcArgs0.BatchChangeAcctInfoRequestMsg.BatchChangeAcctInfoRequest.AcctBasicInfo.FreeBillMedium[0]

destMidVar2.code = srcMidVar7.BMCode

destMidVar2.type = srcMidVar7.BMType
