dest.setServiceOperation("SubscriberService","batchScatteredSubActivation")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.batch.scatteredsubactivation.io.BatchScatteredSubActivationRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.BatchScatteredSubActivationRequestMsg.BatchScatteredSubActivationRequest.SubBasicInfo

def destMidVar = destArgs1.subActivationInfo

mappingList(srcMidVar.SubProperty,destMidVar.properties,listMapping0)

def destMidVar0 = destArgs1.subActivationInfo.subscriberInfo

destMidVar0.writtenLang = srcMidVar.WrittenLang

destMidVar0.subPassword = srcMidVar.SubPassword

destMidVar0.ivrLang = srcMidVar.IVRLang

def srcMidVar0 = srcArgs0.BatchScatteredSubActivationRequestMsg.BatchScatteredSubActivationRequest

destArgs1.requestFileName = srcMidVar0.FileName

def srcMidVar1 = srcArgs0.BatchScatteredSubActivationRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar1.LoginSystemCode

destArgs0.password = srcMidVar1.Password

destArgs0.remoteAddress = srcMidVar1.RemoteIP

def srcMidVar2 = srcArgs0.BatchScatteredSubActivationRequestMsg.RequestHeader

mappingList(srcMidVar2.AdditionalProperty,destArgs0.simpleProperty,listMapping1)

destArgs0.messageSeq = srcMidVar2.MessageSeq

destArgs0.businessCode = srcMidVar2.BusinessCode

def srcMidVar3 = srcArgs0.BatchScatteredSubActivationRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar3.ChannelID

destArgs0.operatorId = srcMidVar3.OperatorID

def srcMidVar4 = srcArgs0.BatchScatteredSubActivationRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar4.BEID

destArgs0.brId = srcMidVar4.BRID

def srcMidVar5 = srcArgs0.BatchScatteredSubActivationRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar5.TimeType

destArgs0.timeZoneId = srcMidVar5.TimeZoneID

destArgs0.version = srcMidVar2.Version

destArgs0.interMode = srcMidVar2.AccessMode

destArgs0.msgLanguageCode = srcMidVar2.MsgLanguageCode
