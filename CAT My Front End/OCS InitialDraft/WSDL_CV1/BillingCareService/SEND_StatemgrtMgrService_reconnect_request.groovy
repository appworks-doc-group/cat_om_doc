def srcReqDocMsg = src.payload._args[0]
def destMsgHeader = dest.payload._args[0]
def destReqData = dest.payload._args[1]
def srcReqHeaderDoc = srcReqDocMsg.ReconnectRequestMsg.RequestHeader

destMsgHeader._class = "com.huawei.ngcbs.cm.ocs33ws.core.bo.Ocs33MessageHeader"
destReqData._class = "com.huawei.ngcbs.cm.ocs33ws.subscriber.reconnect.io.OCS33ReconnectRequest"

destMsgHeader.commandId = srcReqHeaderDoc.CommandId
destMsgHeader.version = srcReqHeaderDoc.Version
destMsgHeader.transactionId = srcReqHeaderDoc.TransactionId
destMsgHeader.sequenceId = srcReqHeaderDoc.SequenceId
destMsgHeader.requestType = srcReqHeaderDoc.RequestType
destMsgHeader.loginSystem = srcReqHeaderDoc.SessionEntity.Name
destMsgHeader.password = srcReqHeaderDoc.SessionEntity.Password
destMsgHeader.remoteAddress = srcReqHeaderDoc.SessionEntity.RemoteAddress
destMsgHeader.interFrom = srcReqHeaderDoc.InterFrom
destMsgHeader.interMedi = srcReqHeaderDoc.InterMedi
destMsgHeader.interMode = srcReqHeaderDoc.InterMode
destMsgHeader.visitArea = srcReqHeaderDoc.visitArea
destMsgHeader.currentCell = srcReqHeaderDoc.currentCell
destMsgHeader.additionInfo = srcReqHeaderDoc.additionInfo
destMsgHeader.thirdPartyId = srcReqHeaderDoc.ThirdPartyID
destMsgHeader.partnerId = srcReqHeaderDoc.PartnerID
destMsgHeader.operatorId = srcReqHeaderDoc.OperatorID
destMsgHeader.tradePartnerId = srcReqHeaderDoc.TradePartnerID
destMsgHeader.partnerOperId = srcReqHeaderDoc.PartnerOperID
destMsgHeader.belToAreaId = srcReqHeaderDoc.BelToAreaID
destMsgHeader.reserve2 = srcReqHeaderDoc.Reserve2
destMsgHeader.reserve3 = srcReqHeaderDoc.Reserve3
destMsgHeader.messageSeq = srcReqHeaderDoc.SerialNo
destMsgHeader.remark = srcReqHeaderDoc.Remark
destMsgHeader.performanceStatCmd = "Reconnect"

def srcReqDataDoc = srcReqDocMsg.ReconnectRequestMsg.ReconnectRequest
destReqData.subAccessCode.primaryIdentity = srcReqDataDoc.SubscriberNo
destReqData.changeSubStatusInfo.opType = srcReqDataDoc.OperateType