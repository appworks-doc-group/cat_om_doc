def srcArgs0 = src.payload._args[0]
def destArgs0 = dest.payload._args[0]

def destMidVar = destArgs0.getRecommendedOffer.requestHeader
destMidVar.accessChannel = srcArgs0.requestHeader.accessChannel
destMidVar.beId = srcArgs0.requestHeader.beId
destMidVar.language = srcArgs0.requestHeader.language
destMidVar.operator = srcArgs0.requestHeader.operator
destMidVar.password = srcArgs0.requestHeader.password
destMidVar.transactionId = srcArgs0.requestHeader.transactionId

def listMapping0
listMapping0 =
{
    src, dest  ->

        dest.key = src.attrMapKey

        dest.value = src.attrMapValue

}

def destMidVar0 = destArgs0.getRecommendedOffer.eventBody
destMidVar0.eventCode = srcArgs0.eventBody.eventCode
destMidVar0.msisdn = srcArgs0.eventBody.msisdn
mappingList(srcArgs0.eventBody.eventAttrMap, destMidVar0.eventAttrMap, listMapping0)


