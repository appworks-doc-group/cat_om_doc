def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.AutoType = src.autoType
	
	dest.EffectiveDate = formatDate(src.effDate, "yyyyMMddHHmmss")
	
	dest.ExpireDate = formatDate(src.expDate, "yyyyMMddHHmmss")
	
	def srcMidVar0 = src.offeringKey
	srcMidVar0._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
	dest.ProductID = srcMidVar0.oCode
	
	dest.ProductOrderKey = srcMidVar0.pSeq
	
}

def destMidVar = destReturn.ChangeMainProdResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.CommandId = srcMidVar.commandId

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.SequenceId = srcMidVar.sequenceId

destMidVar.TransactionId = srcMidVar.transactionId

destMidVar.Version = srcMidVar.version

def destMidVar0 = destReturn.ChangeMainProdResultMsg.ChangeMainProdResult

mappingList(srcReturn.modifyOfferings,destMidVar0.ProductOrderInfo,listMapping0)
