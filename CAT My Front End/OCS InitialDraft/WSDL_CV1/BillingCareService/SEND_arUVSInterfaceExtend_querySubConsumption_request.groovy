import com.huawei.ngcbs.bm.common.common.Constant4Model;

dest.setServiceOperation("arUVSInterfaceExtend","querySubConsumption");

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs11ws.core.bo.Ocs11MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs11ws.io.querysubconsumption.QuerySubConsumptionRequestOcs12Base"

def srcMidVar = srcArgs0.QuerySubConsumption.QuerySubConsumptionRequest.RequestMessage.MessageHeader

def srcMidVar1 = srcArgs0.QuerySubConsumption.QuerySubConsumptionRequest.RequestMessage.MessageBody

destArgs0.requestType = srcMidVar.RequestType

destArgs0.tenantId = srcMidVar.TenantId

destArgs0.sequenceId = srcMidVar.SequenceId

destArgs0.transactionId = srcMidVar.TransactionId

destArgs0.version = srcMidVar.Version

destArgs0.commandId = srcMidVar.CommandId

destArgs0.language = srcMidVar.Language

destArgs1.subscriberNo = srcMidVar1.SubscriberNo

destArgs1.subscriberID = srcMidVar1.SubscriberID

destArgs1.acctType = srcMidVar1.AcctType

destArgs1.consumptionMode = srcMidVar1.ConsumptionMode

def srcSessionEntity = srcArgs0.QuerySubConsumption.SessionEntity

destArgs0.sessionEntity.userID = srcSessionEntity.userID

destArgs0.sessionEntity.password = srcSessionEntity.password

destArgs0.sessionEntity.locale = srcSessionEntity.locale

destArgs0.sessionEntity.loginVia = srcSessionEntity.loginVia

destArgs0.sessionEntity.remoteAddr = srcSessionEntity.remoteAddr

destArgs0.sessionEntity.uploadRoot = srcSessionEntity.uploadRoot

destArgs0.loginSystem = srcSessionEntity.userID

destArgs0.password = srcSessionEntity.password

