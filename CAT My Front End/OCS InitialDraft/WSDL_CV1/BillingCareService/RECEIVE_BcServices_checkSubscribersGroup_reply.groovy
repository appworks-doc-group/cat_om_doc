def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.ActiveFlg = src.activeFlg
	
	dest.GroupID = src.groupId
	
	dest.GroupName = src.groupName
	
}

def destMidVar = destReturn.CheckSubscribersGroupResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.Version = srcMidVar.version

destMidVar.MessageSeq = srcMidVar.messageSeq

mappingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def destMidVar0 = destReturn.CheckSubscribersGroupResultMsg.CheckSubscribersGroupResult

def srcMidVar0 = srcReturn.resultInfo

destMidVar0.GroupRelation = srcMidVar0.groupRelation

mappingList(srcMidVar0.userGroupList,destMidVar0.UserGroup,listMapping1)
