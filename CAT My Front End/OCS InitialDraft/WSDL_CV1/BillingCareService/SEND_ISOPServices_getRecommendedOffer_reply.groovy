def srcReturn = src.payload._return

def destReturn = dest.payload._return

def srcMidVar = srcReturn.getRecommendedOfferResponse.return

destReturn.resultCode = srcMidVar.reresultCode

destReturn.resultMessage = srcMidVar.resultMessage

destReturn._class = "com.huawei.ngcbs.cm.common.ws.client.io.isop.RecommendedOfferResult"
