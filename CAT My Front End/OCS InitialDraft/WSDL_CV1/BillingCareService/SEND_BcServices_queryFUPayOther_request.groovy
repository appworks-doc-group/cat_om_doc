import com.huawei.ngcbs.bm.common.common.Constant4Model;

dest.setServiceOperation("BMQueryService","queryFUPayOther")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.query.queryfupayother.io.QueryFUPayOtherRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}
def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.groupKey = src.SubGroupKey
	
	dest.groupCode = src.SubGroupCode
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar6 = srcArgs0.QueryFUPayOtherRequestMsg.QueryFUPayOtherRequest

destArgs1.freeUnitID = srcMidVar6.FreeUnitID

destArgs1.freeUnitType = srcMidVar6.FreeUnitType

destArgs1.startTime = parseDate(srcMidVar6.StartTime,Constant4Model.DATE_FORMAT)

destArgs1.endTime = parseDate(srcMidVar6.EndTime,Constant4Model.DATE_FORMAT)

destArgs1.totalNum = srcMidVar6.TotalNum

destArgs1.beginRowNum = srcMidVar6.BeginRowNum

destArgs1.fetchRowNum = srcMidVar6.FetchRowNum

listMapping2.call(srcMidVar6.QueryObj.SubGroupAccessCode,destArgs1.groupAccessCode)
	
listMapping3.call(srcMidVar6.QueryObj.CustAccessCode,destArgs1.custAccessCode)
	
listMapping4.call(srcMidVar6.QueryObj.SubAccessCode,destArgs1.subAccessCode)
	
def srcMidVar2 = srcArgs0.QueryFUPayOtherRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar2.AccessMode

def srcMidVar3 = srcMidVar2.AccessSecurity

destArgs0.loginSystem = srcMidVar3.LoginSystemCode

destArgs0.password = srcMidVar3.Password

destArgs0.remoteAddress = srcMidVar3.RemoteIP

destArgs0.businessCode = srcMidVar3.BusinessCode

destArgs0.messageSeq = srcMidVar2.MessageSeq

destArgs0.msgLanguageCode = srcMidVar2.MsgLanguageCode

def srcMidVar4 = srcMidVar2.OperatorInfo

destArgs0.channelId = srcMidVar4.ChannelID

destArgs0.operatorId = srcMidVar4.OperatorID

def srcMidVar5 = srcMidVar2.OwnershipInfo

destArgs0.beId = srcMidVar5.BEID

destArgs0.brId = srcMidVar5.BRID

def srcMidVar7 = srcMidVar2.TimeFormat

destArgs0.timeType = srcMidVar7.TimeType

destArgs0.timeZoneId = srcMidVar7.TimeZoneID

destArgs0.version = srcMidVar2.Version

mappingList(srcArgs0.QueryFUPayOtherRequestMsg.RequestHeader.AdditionalProperty,destArgs0.simpleProperty,listMapping5)
