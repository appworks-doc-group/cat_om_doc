def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.callingNumber = src.CallingNumber
	
	dest.serialNo = src.SerialNo
	
}

def destMidVar = destReturn.resultHeader

def srcMidVar = srcReturn.QueryFphCallerResultMsg.ResultHeader

destMidVar.resultCode = srcMidVar.ResultCode

destMidVar.resultDesc = srcMidVar.ResultDesc

destMidVar.version = srcMidVar.Version

def srcMidVar0 = srcReturn.QueryFphCallerResultMsg.QueryFphCallerResult

def destMidVar0 = destReturn.queryFPHCallerResponse

mappingList(srcMidVar0.FphCaller,destMidVar0.fphCallers,listMapping0)

destMidVar0.subscriberNo = srcMidVar0.SubscriberNo

destReturn._class = "com.huawei.ngcbs.cm.common.ws.client.io.om.OMQueryFPHCallerResult"
