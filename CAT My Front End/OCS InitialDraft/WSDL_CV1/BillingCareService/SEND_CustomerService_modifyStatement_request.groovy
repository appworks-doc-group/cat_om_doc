import com.huawei.ngcbs.bm.common.common.Constant4Model


def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.customer.modifystatement.io.ModifyStatementRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.BMCode
	
	dest.type = src.BMType
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	def srcMidVar7 = src.EffectiveTime
	
	dest.effMode = srcMidVar7.Mode
	
	dest.effDate=parseDate(srcMidVar7.Time, Constant4Model.DATE_FORMAT)
	
	dest.expDate=parseDate(src.ExpirationTime, Constant4Model.DATE_FORMAT)
	
//	dest.scenarioInfo = src.AccountKey + src.SubscriberKey
     if(isNull(src.SubscriberKey)) {
   
            dest.scenarioInfo.objKey = src.AccountKey
        }else {
       
            dest.scenarioInfo.objKey = src.SubscriberKey
        }
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	def srcMidVar8 = src.ExpirationTime
	
	dest.expMode = srcMidVar8.Mode
	
	dest.expDate=parseDate(srcMidVar8.Time, Constant4Model.DATE_FORMAT)
	
//	dest.scenarioInfo = src.AccountKey + src.SubscriberKey
    if(isNull(src.SubscriberKey)) {
        dest.scenarioInfo.objKey = src.AccountKey
    }else {
        dest.scenarioInfo.objKey = src.SubscriberKey
    }
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.code = src.BMCode
	
	dest.type = src.BMType
	
}

def destMidVar = destArgs1.addressInfo

def srcMidVar = srcArgs0.ModifyStatementRequestMsg.ModifyStatementRequest.AddressInfo

destMidVar.addr1 = srcMidVar.Address1

destMidVar.addr10 = srcMidVar.Address10

destMidVar.addr11 = srcMidVar.Address11

destMidVar.addr12 = srcMidVar.Address12

destMidVar.addr2 = srcMidVar.Address2

destMidVar.addr3 = srcMidVar.Address3

destMidVar.addr4 = srcMidVar.Address4

destMidVar.addr5 = srcMidVar.Address5

destMidVar.addr6 = srcMidVar.Address6

destMidVar.addr7 = srcMidVar.Address7

destMidVar.addr8 = srcMidVar.Address8

destMidVar.addr9 = srcMidVar.Address9

destMidVar.tpAddrKey = srcMidVar.AddressKey

destMidVar.postCode = srcMidVar.PostCode

def destMidVar0 = destArgs1.custAccessCode

def srcMidVar0 = srcArgs0.ModifyStatementRequestMsg.ModifyStatementRequest.RegisterCust

destMidVar0.customerCode = srcMidVar0.CustomerCode

destMidVar0.customerKey = srcMidVar0.CustomerKey

destMidVar0.primaryIdentity = srcMidVar0.PrimaryIdentity

def destMidVar1 = destArgs1.modifyStatementInfo.statementInfo

def srcMidVar1 = srcArgs0.ModifyStatementRequestMsg.ModifyStatementRequest

destMidVar1.stmtKey = srcMidVar1.SmtKey

def destMidVar2 = destArgs1.modifyStatementInfo.contactInfo

def srcMidVar2 = srcArgs0.ModifyStatementRequestMsg.ModifyStatementRequest.StatementInfo.ContactInfo

destMidVar2.addrKey = srcMidVar2.AddressKey

destMidVar2.email = srcMidVar2.Email

destMidVar2.fax = srcMidVar2.Fax

destMidVar2.firstName = srcMidVar2.FirstName

destMidVar2.homePhone = srcMidVar2.HomePhone

destMidVar2.lastName = srcMidVar2.LastName

destMidVar2.middleName = srcMidVar2.MiddleName

destMidVar2.mobilePhone = srcMidVar2.MobilePhone

destMidVar2.officePhone = srcMidVar2.OfficePhone

destMidVar2.title = srcMidVar2.Title

def srcMidVar3 = srcArgs0.ModifyStatementRequestMsg.ModifyStatementRequest.StatementInfo

destMidVar1.stmtLang = srcMidVar3.SmtLang

mappingList(srcMidVar3.SmtMedium,destArgs1.bmInfos,listMapping0)

def destMidVar3 = destArgs1.modifyStatementInfo.stmtOfferingInfo

def srcMidVar4 = srcArgs0.ModifyStatementRequestMsg.ModifyStatementRequest.StatementInfo.SmtOffering

destMidVar3.oId = srcMidVar4.OfferingID

def srcMidVar5 = srcArgs0.ModifyStatementRequestMsg.ModifyStatementRequest.StatementInfo.SmtOffering.OfferingOwner

destMidVar3.ownerKey = srcMidVar5.OwnerKey

destMidVar3.ownerType = srcMidVar5.OwnerType

destMidVar3.purchaseSeq = srcMidVar4.PurchaseSeq

def srcMidVar6 = srcArgs0.ModifyStatementRequestMsg.ModifyStatementRequest.StatementScenario

def destMidVar4 = destArgs1.modifyStatementInfo

mappingList(srcMidVar6.AddStatementScenario,destMidVar4.addScenarios,listMapping1)

mappingList(srcMidVar6.DelStatementScenario,destMidVar4.delScenarios,listMapping2)

def destMidVar5 = destArgs1.stmtOfferingInfo

destMidVar5.oId = srcMidVar4.OfferingID

destMidVar5.ownerKey = srcMidVar5.OwnerKey

destMidVar5.ownerType = srcMidVar5.OwnerType

destMidVar5.purchaseSeq = srcMidVar4.PurchaseSeq

def srcMidVar9 = srcArgs0.ModifyStatementRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar9.LoginSystemCode

destArgs0.password = srcMidVar9.Password

destArgs0.remoteAddress = srcMidVar9.RemoteIP

def srcMidVar10 = srcArgs0.ModifyStatementRequestMsg.RequestHeader

mappingList(srcMidVar10.AdditionalProperty,destArgs0.simpleProperty,listMapping3)

destArgs0.businessCode = srcMidVar10.BusinessCode

destArgs0.messageSeq = srcMidVar10.MessageSeq

destArgs0.msgLanguageCode = srcMidVar10.MsgLanguageCode

def srcMidVar11 = srcArgs0.ModifyStatementRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar11.ChannelID

destArgs0.operatorId = srcMidVar11.OperatorID

def srcMidVar12 = srcArgs0.ModifyStatementRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar12.BEID

destArgs0.brId = srcMidVar12.BRID

def srcMidVar13 = srcArgs0.ModifyStatementRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar13.TimeType

destArgs0.timeZoneId = srcMidVar13.TimeZoneID

destArgs0.version = srcMidVar10.Version

//if(srcArgs0.ModifyStatementRequestMsg.ModifyStatementRequest.StatementScenario.AddStatementScenario[0].AccountKey !=null)
//{
//	def destMidVar6 = destArgs1.modifyStatementInfo.addScenarios[0].scenarioInfo
//	
//	def srcMidVar14 = srcArgs0.ModifyStatementRequestMsg.ModifyStatementRequest.StatementScenario.AddStatementScenario[0]
//	
//	destMidVar6.objKey = srcMidVar14.AccountKey
//	
//}
//else
//{
//	def destMidVar7 = destArgs1.modifyStatementInfo.addScenarios[0].scenarioInfo
//	
//	def srcMidVar15 = srcArgs0.ModifyStatementRequestMsg.ModifyStatementRequest.StatementScenario.AddStatementScenario[0]
//	
//	destMidVar7.objKey = srcMidVar15.SubscriberKey
//	
//}
//
//if(srcArgs0.ModifyStatementRequestMsg.ModifyStatementRequest.StatementScenario.DelStatementScenario[0].AccountKey !=null)
//{
//	def destMidVar8 = destArgs1.modifyStatementInfo.delScenarios[0].scenarioInfo
//	
//	def srcMidVar16 = srcArgs0.ModifyStatementRequestMsg.ModifyStatementRequest.StatementScenario.DelStatementScenario[0]
//	
//	destMidVar8.objKey = srcMidVar16.AccountKey
//	
//}
//else
//{
//	def destMidVar9 = destArgs1.modifyStatementInfo.delScenarios[0].scenarioInfo
//	
//	def srcMidVar17 = srcArgs0.ModifyStatementRequestMsg.ModifyStatementRequest.StatementScenario.DelStatementScenario[0]
//	
//	destMidVar9.objKey = srcMidVar17.SubscriberKey
//	
//}

mappingList(srcMidVar3.SmtMedium,destMidVar4.bmInfos,listMapping4)

destArgs0.interMode = srcMidVar10.AccessMode
