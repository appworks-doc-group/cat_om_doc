def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def destMidVar = destReturn.resultHeader

def srcMidVar = srcReturn.AdjustmentResultMsg.ResultHeader

destMidVar.version = srcMidVar.Version

destMidVar.resultDesc = srcMidVar.ResultDesc

destMidVar.resultCode = srcMidVar.ResultCode

destMidVar.msgLanguageCode = srcMidVar.MsgLanguageCode

mappingList(srcMidVar.AdditionalProperty,destMidVar.simpleProperty,listMapping0)

destReturn._class = "com.huawei.ngcbs.cm.account.batch.adjustment.io.AdjustmentResult"
