def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.offering.changeofferingstatus.io.ChangeOfferingStatusRequest"

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	def srcMidVar0 = src.AccessSecurity
	
	dest.loginSystem = srcMidVar0.LoginSystemCode
	
	dest.password = srcMidVar0.Password
	
	dest.remoteAddress = srcMidVar0.RemoteIP
	
	mappingList(src.AdditionalProperty,dest.simpleProperty,listMapping1)
	
	dest.businessCode = src.BusinessCode
	
	dest.messageSeq = src.MessageSeq
	
	dest.msgLanguageCode = src.MsgLanguageCode
	
	def srcMidVar1 = src.OperatorInfo
	
	dest.channelId = srcMidVar1.ChannelID
	
	dest.operatorId = srcMidVar1.OperatorID
	
	def srcMidVar2 = src.OwnershipInfo
	
	dest.beId = srcMidVar2.BEID
	
	dest.brId = srcMidVar2.BRID
	
	def srcMidVar3 = src.TimeFormat
	
	dest.timeType = srcMidVar3.TimeType
	
	dest.timeZoneId = srcMidVar3.TimeZoneID
	
	dest.version = src.Version
	
	dest.interMode = src.AccessMode
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.accoutCode = src.AccountCode
	
	dest.accoutKey = src.AccountKey
	
	dest.payType = src.PayType
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping10

listMapping10 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	listMapping5.call(src.AcctAccessCode,dest.acctAccessCode)
	
	listMapping6.call(src.CustAccessCode,dest.custAccessCode)
	
	listMapping7.call(src.SubAccessCode,dest.subAccessCode)
	
	listMapping10.call(src.SubGroupAccessCode,dest.groupAccessCode)
	
}

def listMapping9

listMapping9 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	listMapping4.call(src.OfferingOwner,dest.anyAccessCode)
	
	def destMidVar = dest.changeOfferingStatusInfo
	
	destMidVar.newStatus = src.NewStatus
	
	destMidVar.oldStatus = src.OldStatus
	
	destMidVar.opType = src.OpType
	
	listMapping9.call(src.OfferingKey,destMidVar.offeringKey)
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	mappingList(src.OfferingInst,dest.changeOfferingStatusInfoList,listMapping3)
	
}


def srcMidVar = srcArgs0.ChangeOfferingStatusRequestMsg

listMapping0.call(srcMidVar.RequestHeader,destArgs0)

listMapping2.call(srcMidVar.ChangeOfferingStatusRequest,destArgs1)
