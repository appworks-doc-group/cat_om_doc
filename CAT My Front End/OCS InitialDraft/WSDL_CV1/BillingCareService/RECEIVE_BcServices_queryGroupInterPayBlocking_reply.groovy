import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 =
        {
            src, dest ->

                dest.Code = src.code

                dest.Value = src.value

        }

def listMapping0

listMapping0 =
        {
            src, dest ->

                mappingList(src.simpleProperty, dest.AdditionalProperty, listMapping1)

                dest.MsgLanguageCode = src.msgLanguageCode

                dest.ResultCode = src.resultCode

                dest.ResultDesc = src.resultDesc

                dest.Version = src.version

                dest.MessageSeq = src.messageSeq

        }


def listMapping6

listMapping6 =
        {
            src, dest ->

                dest.SubGroupKey = src.subGroupKey;
				
				dest.SubGorupCode = src.subGroupCode;

                dest.EffDate = formatDate(src.effDate, Constant4Model.DATE_FORMAT);

                dest.ExpDate = formatDate(src.expDate, Constant4Model.DATE_FORMAT);
        }


def listMapping7

listMapping7 =
        {
            src, dest ->

                mappingList(src.groupInterPayBlockingList, dest.GroupInterPayBlockingList, listMapping6)

        }
		
def destMidVar = destReturn.QueryGroupInterPayBlockingResultMsg

listMapping0.call(srcReturn.resultHeader, destMidVar.ResultHeader)

listMapping7.call(srcReturn.resultInfo, destMidVar.QueryGroupInterPayBlockingResult)