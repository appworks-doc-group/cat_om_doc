dest.setServiceOperation("AccountService","applyMultiPrepayment")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.account.applymultiprepayment.io.ApplyMultiPrepaymentRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	mappingList(src.AdditionalProperty,dest.simplePropertyList,listMapping6)
	
	dest.amount = src.Amount
	
	dest.contractID = src.ContractID
	
	dest.currencyID = src.CurrencyID
	
	def destMidVar2 = dest.offeringKey
	
	def srcMidVar8 = src.OfferingKey
	
	destMidVar2.oId = srcMidVar8.OfferingID
	
	destMidVar2.pSeq = srcMidVar8.PurchaseSeq
	
	dest.repayMode = src.RepayMode
	
}

def srcMidVar = srcArgs0.ApplyMultiPrepaymentRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.ApplyMultiPrepaymentRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.ApplyMultiPrepaymentRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.ApplyMultiPrepaymentRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.ApplyMultiPrepaymentRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar.Version

def srcMidVar4 = srcArgs0.ApplyMultiPrepaymentRequestMsg.ApplyMultiPrepaymentRequest.ApplyObj

def destMidVar = destArgs1.anyAccessCode

listMapping1.call(srcMidVar4.CustAccessCode,destMidVar.custAccessCode)

listMapping2.call(srcMidVar4.SubAccessCode,destMidVar.subAccessCode)

listMapping3.call(srcMidVar4.SubGroupAccessCode,destMidVar.groupAccessCode)

def destMidVar0 = destArgs1.applyPrepaymentRequest

def srcMidVar5 = srcArgs0.ApplyMultiPrepaymentRequestMsg.ApplyMultiPrepaymentRequest.ApplyPrepaymentDetail[0]

destMidVar0.amount = srcMidVar5.Amount

destMidVar0.contractID = srcMidVar5.ContractID

destMidVar0.currencyID = srcMidVar5.CurrencyID

def destMidVar1 = destArgs1.applyPrepaymentRequest.offeringKey

def srcMidVar6 = srcArgs0.ApplyMultiPrepaymentRequestMsg.ApplyMultiPrepaymentRequest.ApplyPrepaymentDetail[0].OfferingKey

destMidVar1.oId = srcMidVar6.OfferingID

destMidVar1.pSeq = srcMidVar6.PurchaseSeq

destMidVar0.repayMode = srcMidVar5.RepayMode

mappingList(srcMidVar5.AdditionalProperty,destMidVar0.simplePropertyList,listMapping4)

def srcMidVar7 = srcArgs0.ApplyMultiPrepaymentRequestMsg.ApplyMultiPrepaymentRequest

mappingList(srcMidVar7.ApplyPrepaymentDetail,destArgs1.applyPrepaymentRequestList,listMapping5)
