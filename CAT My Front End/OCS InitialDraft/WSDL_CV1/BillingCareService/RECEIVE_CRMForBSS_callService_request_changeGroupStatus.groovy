dest.setServiceOperation("CRMForBSS","changeGroupStatus")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Name = src.paraName
	
	dest.Value = src.paraValue
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Name = src.paraName
	
	dest.Value = src.paraValue
	
}

def destMidVar = destArgs0.ChangeGroupStatusRequest.RequestHeader

def srcMidVar = srcArgs0.workOrderHeader

destMidVar.AccessPwd = srcMidVar.accessPwd

destMidVar.AccessUser = srcMidVar.accessUser

destMidVar.BEId = srcMidVar.beId

destMidVar.ChannelId = srcMidVar.channelId

def destMidVar0 = destArgs0.ChangeGroupStatusRequest.RequestHeader.ExtParamList

mappingList(srcMidVar.extParamList,destMidVar0.ParameterInfo,listMapping0)

destMidVar.Language = srcMidVar.language

destMidVar.OperatorId = srcMidVar.operatorId

destMidVar.OperatorPwd = srcMidVar.operatorPwd

destMidVar.ProcessTime = srcMidVar.processTime

destMidVar.SessionId = srcMidVar.sessionId

destMidVar.TestFlag = srcMidVar.testFlag

destMidVar.TimeType = srcMidVar.timeType

destMidVar.TimeZoneID = srcMidVar.timeZoneId

destMidVar.TransactionId = srcMidVar.transactionId

destMidVar.Version = srcMidVar.version

def srcMidVar0 = srcArgs0.changeGroupStatusList[0]

def destMidVar1 = destArgs0.ChangeGroupStatusRequest.ChangeGroupStatus[0]

mappingList(srcMidVar0.extParamList,destMidVar1.ExtParamList,listMapping1)

destMidVar1.ChangeReason = srcMidVar0.changeReason

destMidVar1.ChangeType = srcMidVar0.changeType

destMidVar1.NewStatus = srcMidVar0.newStatus

destMidVar1.OldStatus = srcMidVar0.oldStatus

destMidVar1.SubscriberID = srcMidVar0.tpGroupKey
