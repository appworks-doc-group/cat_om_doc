dest.setServiceOperation("AccountService","charge2BillRollback")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.account.chargetobillrollback.io.Charge2BillRollbackRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.payType = src.PayType
	
	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.accoutCode = src.AccountCode
	
	dest.accoutKey = src.AccountKey
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.chargeSeq = src.ChargeSeq
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def srcMidVar = srcArgs0.Charge2BillRollbackRequestMsg.Charge2BillRollbackRequest

listMapping0.call(srcMidVar.AcctAccessCode,destArgs1.acctAccessCode)

destArgs1.charge2BillSerialNo = srcMidVar.Charge2BillSerialNo

listMapping1.call(srcMidVar.SubGroupAccessCode,destArgs1.subGroupAccessCode)

listMapping2.call(srcMidVar.SubAccessCode,destArgs1.subAccessCode)

listMapping5.call(srcMidVar.CustAccessCode,destArgs1.custAccessCode)

def srcMidVar0 = srcArgs0.Charge2BillRollbackRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar0.AccessMode

def srcMidVar1 = srcArgs0.Charge2BillRollbackRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar1.LoginSystemCode

destArgs0.password = srcMidVar1.Password

destArgs0.remoteAddress = srcMidVar1.RemoteIP

mappingList(srcMidVar0.AdditionalProperty,destArgs0.simpleProperty,listMapping3)

destArgs0.businessCode = srcMidVar0.BusinessCode

destArgs0.messageSeq = srcMidVar0.MessageSeq

destArgs0.msgLanguageCode = srcMidVar0.MsgLanguageCode

def srcMidVar2 = srcArgs0.Charge2BillRollbackRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar2.ChannelID

destArgs0.operatorId = srcMidVar2.OperatorID

def srcMidVar3 = srcArgs0.Charge2BillRollbackRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar3.BEID

destArgs0.brId = srcMidVar3.BRID

def srcMidVar4 = srcArgs0.Charge2BillRollbackRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar4.TimeType

destArgs0.timeZoneId = srcMidVar4.TimeZoneID

destArgs0.version = srcMidVar0.Version

mappingList(srcMidVar.RollBackFeeValues,destArgs1.rollBackFeeValues,listMapping4)
