import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->
	
	dest.OfferingID = src.id
	
	dest.OfferingCode = src.code

    dest.OfferingName = src.name
	
	dest.EffectiveDate = formatDate(src.effectiveDate, Constant4Model.DATE_FORMAT)
	
	dest.ExpireDate = formatDate(src.expireDate, Constant4Model.DATE_FORMAT)
}

def listMapping2

listMapping2 =
{
    src,dest  ->

    dest.Code = src.code

    dest.Value = src.value

}

def listMapping1

listMapping1 =
{
    src,dest  ->

    dest.MsgLanguageCode = src.msgLanguageCode

    dest.ResultCode = src.resultCode

    dest.ResultDesc = src.resultDesc

    dest.Version = src.version

    dest.MessageSeq = src.messageSeq

    mappingList(src.simpleProperty,dest.AdditionalProperty,listMapping2)

}



def destMidVar = destReturn.QueryOfferingBySubscribingResultMsg

listMapping1.call(srcReturn.resultHeader,destMidVar.ResultHeader)



def destMidVar0 = destMidVar.QueryOfferingBySubscribingResult
mappingList(srcReturn.offerInfoList,destMidVar0.OfferingInfo,listMapping0)