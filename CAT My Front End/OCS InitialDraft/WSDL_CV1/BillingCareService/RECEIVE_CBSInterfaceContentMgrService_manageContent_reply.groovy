def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.ManageContentResultMsg.ResultHeader

def srcReturnHeader = srcReturn.resultHeader

destMidVar.TransactionId = srcReturnHeader.transactionId

destMidVar.SequenceId = srcReturnHeader.sequenceId

destMidVar.ResultCode = srcReturnHeader.resultCode

destMidVar.ResultDesc = srcReturnHeader.resultDesc

destMidVar.Version = srcReturnHeader.version

destMidVar.CommandId = srcReturnHeader.commandId

def listMapping0

listMapping0 =
        {
            src, dest ->
                dest.Id = src.providerID
                dest.Name = src.providerName
                dest.Desc = src.providerDesc
        }

def listMapping1

listMapping1 =
        {
            src,dest  ->

                dest.ContentID = src.contentId

                dest.ContentName = src.contentName

                dest.ContentShortDesc = src.contentShortDesc

                dest.ContentLongDesc = src.contentLongDesc

                dest.Version = src.version

                dest.CategoryID = src.categoryId

                dest.ServiceType = src.serviceType

                dest.LastUpdOperatorID = src.lastUpdOperatorId

                dest.BillDescCode = src.billDescCode

                listMapping0.call(src.contentProvider,dest.Provider)

                dest.BillURL = src.billUrl

                dest.RequestedDuration = src.requestedDuration

        }

def listMapping2

listMapping2 =
        {
            src, dest ->

                dest.CategoryID = src.categoryId

                dest.CategoryDesc = src.categoryDesc

                dest.Minimum = src.minimum

                dest.Maximum = src.maximum

        }



def destMidVar1 = destReturn.ManageContentResultMsg.ManageContentResult

def srcMidVar2 = srcReturn.contentDetailInfo

def srcMidVar3 = srcReturn.categoryDetailInfo

mappingList(srcMidVar2.contentDetailList,destMidVar1.Content,listMapping1)

mappingList(srcMidVar3.categoryDetailList,destMidVar1.Category,listMapping2)

