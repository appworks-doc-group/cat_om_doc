import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("BMQueryService","queryExpireSubToMicro")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.query.queryexpiresubtomicro.io.QueryExpireSubToMicroRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def destMidVar = destArgs1.pagingInfo

def srcMidVar = srcArgs0.QueryExpireSubToMicroRequestMsg.QueryExpireSubToMicroRequest.PagingInfo

destMidVar.beginRowNum = srcMidVar.BeginRowNum

destMidVar.fetchRowNum = srcMidVar.FetchRowNum

destMidVar.totalRowNum = srcMidVar.TotalRowNum

def srcMidVar0 = srcArgs0.QueryExpireSubToMicroRequestMsg.QueryExpireSubToMicroRequest.TimePeriod

destArgs1.endTime = parseDate(srcMidVar0.EndTime, Constant4Model.DATE_FORMAT)

destArgs1.startTime = parseDate(srcMidVar0.StartTime, Constant4Model.DATE_FORMAT)

def srcMidVar1 = srcArgs0.QueryExpireSubToMicroRequestMsg.RequestHeader

destArgs0.version = srcMidVar1.Version

def srcMidVar2 = srcArgs0.QueryExpireSubToMicroRequestMsg.RequestHeader.TimeFormat

destArgs0.timeZoneId = srcMidVar2.TimeZoneID

destArgs0.timeType = srcMidVar2.TimeType

def srcMidVar3 = srcArgs0.QueryExpireSubToMicroRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar3.BEID

destArgs0.brId = srcMidVar3.BRID

def srcMidVar4 = srcArgs0.QueryExpireSubToMicroRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar4.ChannelID

destArgs0.operatorId = srcMidVar4.OperatorID

mappingList(srcMidVar1.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

def srcMidVar5 = srcArgs0.QueryExpireSubToMicroRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar5.LoginSystemCode

destArgs0.password = srcMidVar5.Password

destArgs0.remoteAddress = srcMidVar5.RemoteIP

destArgs0.businessCode = srcMidVar1.BusinessCode

destArgs0.messageSeq = srcMidVar1.MessageSeq

destArgs0.interMode = srcMidVar1.AccessMode

destArgs0.msgLanguageCode = srcMidVar1.MsgLanguageCode
