import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Amount = src.amount
	
	dest.CurrencyID = src.currencyID
	
	dest.CycleClass = src.cycleClass
	
	dest.CycleDueDate=formatDate(src.cycleDueDate,  Constant4Model.DATE_FORMAT)
	
	dest.CycleSequence = src.cycleSequence
	
	dest.InitialAmount = src.initialAmount
	
	dest.InstallmentInstID = src.installmentInstID
	
	dest.RealRepayDate=formatDate(src.realRepayDate,  Constant4Model.DATE_FORMAT)
	
	dest.Status = src.status
	
}

def destMidVar = destReturn.ApplyMultiInstallmentResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.Version = srcMidVar.version

destMidVar.MessageSeq = srcMidVar.messageSeq

mappingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def srcMidVar0 = srcReturn.applyInstallmentInfo

def destMidVar0 = destReturn.ApplyMultiInstallmentResultMsg.ApplyMultiInstallmentResult

mappingList(srcMidVar0.inatallmentDetailList,destMidVar0.InatallmentDetail,listMapping1)
