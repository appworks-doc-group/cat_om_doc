import com.huawei.ngcbs.bm.common.common.Constant4Model;

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.SubPropCode = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.EffectiveTime=formatDate(src.effDate, Constant4Model.DATE_FORMAT)
	
	dest.ExpirationTime=formatDate(src.expDate, Constant4Model.DATE_FORMAT)
	
	dest.CreateTime=formatDate(src.createTime, Constant4Model.DATE_FORMAT)
	
	def srcMidVar = src.property
	
	dest.PropType = srcMidVar.complexFlag
	
	dest.PropCode = srcMidVar.propCode
	
	mappingList(srcMidVar.subProps,dest.SubPropInst,listMapping2)
	
	dest.Value = srcMidVar.value
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.OfferingID = src.oId
    dest.OfferingCode = src.oCode
	dest.PurchaseSeq = src.pSeq
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.SubPropCode = src.code
	
	dest.Value = src.value
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.EffectiveTime=formatDate(src.effDate, Constant4Model.DATE_FORMAT)
	
	dest.ExpirationTime=formatDate(src.expDate, Constant4Model.DATE_FORMAT)
	
	dest.CreateTime=formatDate(src.createTime, Constant4Model.DATE_FORMAT)
	
	def srcMidVar0 = src.property
	
	dest.PropType = srcMidVar0.complexFlag
	
	dest.PropCode = srcMidVar0.propCode
	
	mappingList(srcMidVar0.subProps,dest.SubPropInst,listMapping6)
	
	dest.Value = srcMidVar0.value
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.ProductID = src.productId
	
	mappingList(src.productInstProperty,dest.ProductInstProperty,listMapping5)
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	mappingList(src.offeringInstProperty,dest.OfferingInstProperty,listMapping1)
	
    def oKeyExt = src.offeringKey
    oKeyExt._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
    
	listMapping3.call(oKeyExt,dest.OfferingKey)
	
	mappingList(src.productInst,dest.ProductInst,listMapping4)
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destReturn.QueryOfferingInstPropertyResultMsg.QueryOfferingInstPropertyResult

mappingList(srcReturn.resultBody,destMidVar.OfferingInst,listMapping0)

def destMidVar0 = destReturn.QueryOfferingInstPropertyResultMsg.ResultHeader

def srcMidVar1 = srcReturn.resultHeader

destMidVar0.MsgLanguageCode = srcMidVar1.msgLanguageCode

destMidVar0.ResultCode = srcMidVar1.resultCode

destMidVar0.ResultDesc = srcMidVar1.resultDesc

destMidVar0.Version = srcMidVar1.version

destMidVar0.MessageSeq = srcMidVar1.messageSeq

mappingList(srcMidVar1.simpleProperty,destMidVar0.AdditionalProperty,listMapping7)
