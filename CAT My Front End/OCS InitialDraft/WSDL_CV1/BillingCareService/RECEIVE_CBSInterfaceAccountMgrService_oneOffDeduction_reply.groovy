def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.AccountID = src.accountId
	
	dest.AccountType = src.accountType
	
	dest.ChgAcctBal = src.chgAcctBal
	
	dest.ChgExpTime = src.chgExpTime
	
	dest.CurrAcctBal = src.currAcctBal
	
	dest.CurrExpTime = src.currExpTime
	
	dest.MinMeasureId = src.minMeasureId
	
}

def destMidVar = destReturn.OneOffDeductionResultMsg.OneOffDeductionResult

mappingList(srcReturn.acctChgRec,destMidVar.AcctChgRec,listMapping0)

def destMidVar0 = destReturn.OneOffDeductionResultMsg.ResultHeader

def srcMidVar = srcReturn.ocs12ResultHeader

destMidVar0.CommandId = srcMidVar.commandId

destMidVar0.ResultCode = srcMidVar.resultCode

destMidVar0.ResultDesc = srcMidVar.resultDesc

destMidVar0.SequenceId = srcMidVar.sequenceId

destMidVar0.TransactionId = srcMidVar.transactionId

destMidVar0.Version = srcMidVar.version
