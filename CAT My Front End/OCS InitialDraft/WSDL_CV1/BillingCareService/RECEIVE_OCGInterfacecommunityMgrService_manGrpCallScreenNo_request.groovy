import com.huawei.ngcbs.bm.common.common.Constant4Model

def bmManGrpCallScreenNoRequest = src.payload._args[0]
def omManGrpCallScreenNoDoc = dest.payload._args[0]

def bmRequestHeader = bmManGrpCallScreenNoRequest.omRequestHeader
def omRequestHeaderDoc = omManGrpCallScreenNoDoc.ManGrpCallScreenNoRequestMsg.RequestHeader

omRequestHeaderDoc.CommandId = bmRequestHeader.commandId
omRequestHeaderDoc.Version = bmRequestHeader.version
omRequestHeaderDoc.TransactionId = bmRequestHeader.transactionId
omRequestHeaderDoc.SequenceId = bmRequestHeader.sequenceId
omRequestHeaderDoc.RequestType = bmRequestHeader.requestType

def bmSessionEntity = bmRequestHeader.sessionEntity
def omSessionEntity = omRequestHeaderDoc.SessionEntity

omSessionEntity.Name = bmSessionEntity.name
omSessionEntity.Password = bmSessionEntity.password
omSessionEntity.RemoteAddress = bmSessionEntity.remoteAddress

omRequestHeaderDoc.InterFrom = bmRequestHeader.interFrom
omRequestHeaderDoc.InterMode = bmRequestHeader.interMode
omRequestHeaderDoc.InterMedi = bmRequestHeader.interMedi
omRequestHeaderDoc.visitArea = bmRequestHeader.visitArea
omRequestHeaderDoc.currentCell = bmRequestHeader.currentCell
omRequestHeaderDoc.ThirdPartyID = bmRequestHeader.thirdPartyId
omRequestHeaderDoc.PartnerID = bmRequestHeader.partnerId
omRequestHeaderDoc.OperatorID = bmRequestHeader.operatorId
omRequestHeaderDoc.TradePartnerID = bmRequestHeader.tradePartnerId
omRequestHeaderDoc.PartnerOperID = bmRequestHeader.partnerOperId
omRequestHeaderDoc.BelToAreaID = bmRequestHeader.belToAreaId
omRequestHeaderDoc.SerialNo = bmRequestHeader.serialNo
omRequestHeaderDoc.Remark = bmRequestHeader.remark
omRequestHeaderDoc.TenantID = bmRequestHeader.tenantId

def omRequestBodyDoc = omManGrpCallScreenNoDoc.ManGrpCallScreenNoRequestMsg.ManGrpCallScreenNoRequest

omRequestBodyDoc.GroupNumber = bmManGrpCallScreenNoRequest.groupNumber
omRequestBodyDoc.CallScreenType = bmManGrpCallScreenNoRequest.callScreenType
omRequestBodyDoc.OperationType = bmManGrpCallScreenNoRequest.operationType

def listMappingCallScreenNoInfo
listMappingCallScreenNoInfo = 
{
    src,dest  ->
	dest.CallScreenNo = src.callScreenNo
	dest.ScreenNoType = src.screenNoType
	dest.EffectiveDate = formatDate(src.effectiveDate,Constant4Model.DATE_FORMAT)
	dest.ExpireDate = formatDate(src.expireDate,Constant4Model.DATE_FORMAT)
	dest.WeekStart = src.weekStart
	dest.WeekStop = src.weekStop
	dest.TimeStart = src.timeStart
	dest.TimeStop = src.timeStop
	dest.RouteNumber = src.routeNumber
	dest.RoutingMethod = src.routingMethod
	dest.ZoneID = src.zoneID
	dest.Callflow = src.callflow
	dest.ScreenClass = src.screenClass
}
mappingList(bmManGrpCallScreenNoRequest.callScreenNoInfos,omRequestBodyDoc.CallScreenNoInfo,listMappingCallScreenNoInfo)
