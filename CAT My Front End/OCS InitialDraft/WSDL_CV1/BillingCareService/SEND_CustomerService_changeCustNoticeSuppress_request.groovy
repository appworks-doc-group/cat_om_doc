def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.customer.changecustnoticesuppress.io.ChangeCustNoticeSuppressRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.channelType = src.ChannelType
	
	dest.noticeType = src.NoticeType
	
	dest.subNoticeType = src.SubNoticeType
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.channelType = src.ChannelType
	
	dest.noticeType = src.NoticeType
	
	dest.subNoticeType = src.SubNoticeType
	
}

def srcMidVar = srcArgs0.ChangeCustNoticeSuppressRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar.BEID

destArgs0.brId = srcMidVar.BRID

def srcMidVar0 = srcArgs0.ChangeCustNoticeSuppressRequestMsg.RequestHeader

destArgs0.businessCode = srcMidVar0.BusinessCode

def srcMidVar1 = srcArgs0.ChangeCustNoticeSuppressRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.messageSeq = srcMidVar0.MessageSeq

destArgs0.msgLanguageCode = srcMidVar0.MsgLanguageCode

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.ChangeCustNoticeSuppressRequestMsg.RequestHeader.AccessSecurity

destArgs0.password = srcMidVar2.Password

def srcMidVar3 = srcArgs0.ChangeCustNoticeSuppressRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar0.Version

destArgs0.loginSystem = srcMidVar2.LoginSystemCode

destArgs0.remoteAddress = srcMidVar2.RemoteIP

mappingList(srcMidVar0.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

def destMidVar = destArgs1.custAccessCode

def srcMidVar4 = srcArgs0.ChangeCustNoticeSuppressRequestMsg.ChangeCustNoticeSuppressRequest.CustAccessCode

destMidVar.customerCode = srcMidVar4.CustomerCode

destMidVar.customerKey = srcMidVar4.CustomerKey

destMidVar.primaryIdentity = srcMidVar4.PrimaryIdentity

def srcMidVar5 = srcArgs0.ChangeCustNoticeSuppressRequestMsg.ChangeCustNoticeSuppressRequest.SuppressSetting

def destMidVar0 = destArgs1.changeCustNoticeSuppressInfo

mappingList(srcMidVar5.AddSuppressSet,destMidVar0.addSuppressSet,listMapping1)

mappingList(srcMidVar5.DelSuppressSet,destMidVar0.delSuppressSet,listMapping2)

destArgs0.interMode = srcMidVar0.AccessMode
