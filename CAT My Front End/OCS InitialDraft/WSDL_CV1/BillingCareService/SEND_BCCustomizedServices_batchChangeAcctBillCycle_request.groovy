dest.setServiceOperation("AccountService","batchChangeAcctBillCycle")
def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.account.batch.changeacctbillcycle.io.BatchChangeAcctBillCycleRequest"

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.interMode = src.AccessMode
	
	def srcMidVar0 = src.AccessSecurity
	
	dest.loginSystem = srcMidVar0.LoginSystemCode
	
	dest.password = srcMidVar0.Password
	
	dest.remoteAddress = srcMidVar0.RemoteIP
	
	mappingList(src.AdditionalProperty,dest.simpleProperty,listMapping1)
	
	dest.businessCode = src.BusinessCode
	
	dest.messageSeq = src.MessageSeq
	
	dest.msgLanguageCode = src.MsgLanguageCode
	
	def srcMidVar1 = src.OperatorInfo
	
	dest.channelId = srcMidVar1.ChannelID
	
	dest.operatorId = srcMidVar1.OperatorID
	
	def srcMidVar2 = src.OwnershipInfo
	
	dest.beId = srcMidVar2.BEID
	
	dest.brId = srcMidVar2.BRID
	
	def srcMidVar3 = src.TimeFormat
	
	dest.timeType = srcMidVar3.TimeType
	
	dest.timeZoneId = srcMidVar3.TimeZoneID
	
	dest.version = src.Version
	
}

def srcMidVar = srcArgs0.BatchChangeAcctBillCycleRequestMsg

listMapping0.call(srcMidVar.RequestHeader,destArgs0)

def destMidVar = destArgs1.changeAcctBillCycle

def srcMidVar4 = srcArgs0.BatchChangeAcctBillCycleRequestMsg.BatchChangeAcctBillCycleRequest

destMidVar.newCycleType = srcMidVar4.NewBillCycleType

destMidVar.oldCycleType = srcMidVar4.OldBillCycleType

destArgs1.requestFileName = srcMidVar4.FileName

def srcMidVar5 = srcArgs0.BatchChangeAcctBillCycleRequestMsg.BatchChangeAcctBillCycleRequest.EffectiveTime

destMidVar.effMode = srcMidVar5.Mode

destMidVar.effDate = parseDate(srcMidVar5.Time, "yyyyMMddHHmmss")
