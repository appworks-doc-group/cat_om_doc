import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("BMGroupService","manageCallHuntingNo")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.group.managecallhuntingno.io.ManageCallHuntingNoRequest"

def listMapping0

listMapping0 =
		{
			src,dest  ->

				dest.code = src.Code

				dest.value = src.Value

		}

def srcMidVar = srcArgs0.ManageCallHuntingNoRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.ManageCallHuntingNoRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.ManageCallHuntingNoRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.ManageCallHuntingNoRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.ManageCallHuntingNoRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar.Version

def srcMidVar4 = srcArgs0.ManageCallHuntingNoRequestMsg.ManageCallHuntingNoRequest

def listMapping1

listMapping1 =
		{
			src,dest  ->

				dest.huntingNumber = src.HuntingNumber

				dest.priority = src.Priority

				dest.effectiveMode.time = parseDate(src.EffectiveTime.Time, Constant4Model.DATE_FORMAT)

				dest.effectiveMode.mode = src.EffectiveTime.Mode

				dest.expDate = parseDate(src.ExpDate, Constant4Model.DATE_FORMAT)

		}

def listMapping2

listMapping2 =
		{
			src,dest  ->

				dest.huntingNumber = src.HuntingNumber

				dest.expiredMode.time = parseDate(src.ExpiredTime.Time, Constant4Model.DATE_FORMAT)

				dest.expiredMode.mode = src.ExpiredTime.Mode
		}

def listMapping3

listMapping3 =
		{
			src,dest  ->

				dest.huntingMainNumber = src.HuntingMainNumber

				mappingList(src.AddHuntingNumber,dest.addHuntingNumberList,listMapping1)

				mappingList(src.DelHuntingNumber,dest.delHuntingNumberList,listMapping2)
		}

mappingList(srcMidVar4.HuntingList,destArgs1.huntingList,listMapping3)


def destMidVar = destArgs1.groupAccessCode

def srcMidVar5 = srcArgs0.ManageCallHuntingNoRequestMsg.ManageCallHuntingNoRequest.SubGroupAccessCode

destMidVar.groupCode = srcMidVar5.SubGroupCode

destMidVar.groupKey = srcMidVar5.SubGroupKey
