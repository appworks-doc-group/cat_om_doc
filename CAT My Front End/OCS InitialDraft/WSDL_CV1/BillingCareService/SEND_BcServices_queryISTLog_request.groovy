dest.setServiceOperation("QueryISTLogService","queryISTLog")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.query.queryistlog.io.QueryISTLogRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def destMidVar = destArgs1.anyAccessCode.acctAccessCode

def srcMidVar = srcArgs0.QueryISTLogRequestMsg.QueryISTLogRequest.QueryObj.AcctAccessCode

destMidVar.accoutCode = srcMidVar.AccountCode

destMidVar.accoutKey = srcMidVar.AccountKey

destMidVar.primaryIdentity = srcMidVar.PrimaryIdentity

def destMidVar0 = destArgs1.anyAccessCode.custAccessCode

def srcMidVar0 = srcArgs0.QueryISTLogRequestMsg.QueryISTLogRequest.QueryObj.CustAccessCode

destMidVar0.customerCode = srcMidVar0.CustomerCode

destMidVar0.customerKey = srcMidVar0.CustomerKey

destMidVar0.primaryIdentity = srcMidVar0.PrimaryIdentity

def destMidVar1 = destArgs1.anyAccessCode.subAccessCode

def srcMidVar1 = srcArgs0.QueryISTLogRequestMsg.QueryISTLogRequest.QueryObj.SubAccessCode

destMidVar1.primaryIdentity = srcMidVar1.PrimaryIdentity

destMidVar1.subscriberKey = srcMidVar1.SubscriberKey

def destMidVar2 = destArgs1.anyAccessCode.groupAccessCode

def srcMidVar2 = srcArgs0.QueryISTLogRequestMsg.QueryISTLogRequest.QueryObj.SubGroupAccessCode

destMidVar2.groupCode = srcMidVar2.SubGroupCode

destMidVar2.groupKey = srcMidVar2.SubGroupKey

destArgs1.messageSeq = srcArgs0.QueryISTLogRequestMsg.QueryISTLogRequest.MessageSeq

destArgs1.serviceTypeCode = srcArgs0.QueryISTLogRequestMsg.QueryISTLogRequest.ServiceTypeCode

def srcMidVar3 = srcArgs0.QueryISTLogRequestMsg.RequestHeader

destArgs0.version = srcMidVar3.Version

def srcMidVar4 = srcArgs0.QueryISTLogRequestMsg.RequestHeader.TimeFormat

destArgs0.timeZoneId = srcMidVar4.TimeZoneID

destArgs0.timeType = srcMidVar4.TimeType

def srcMidVar5 = srcArgs0.QueryISTLogRequestMsg.RequestHeader.OwnershipInfo

destArgs0.brId = srcMidVar5.BRID

destArgs0.beId = srcMidVar5.BEID

def srcMidVar6 = srcArgs0.QueryISTLogRequestMsg.RequestHeader.OperatorInfo

destArgs0.operatorId = srcMidVar6.OperatorID

destArgs0.channelId = srcMidVar6.ChannelID

destArgs0.msgLanguageCode = srcMidVar3.MsgLanguageCode

destArgs0.messageSeq = srcMidVar3.MessageSeq

destArgs0.businessCode = srcMidVar3.BusinessCode

def srcMidVar7 = srcArgs0.QueryISTLogRequestMsg.RequestHeader.AccessSecurity

destArgs0.password = srcMidVar7.Password

destArgs0.loginSystem = srcMidVar7.LoginSystemCode

destArgs0.remoteAddress = srcMidVar7.RemoteIP

destArgs0.interMode = srcMidVar3.AccessMode

mappingList(srcMidVar3.AdditionalProperty,destArgs0.simpleProperty,listMapping0)
