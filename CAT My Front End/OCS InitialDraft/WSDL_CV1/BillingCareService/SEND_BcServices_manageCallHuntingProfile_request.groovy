import com.huawei.ngcbs.bm.common.common.Constant4Model
dest.setServiceOperation("BMGroupService","manageCallHuntingProfile")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.group.managecallhuntingprofile.io.ManageCallHuntingProfileRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}


def listMapping2

listMapping2 =
		{
			src,dest  ->

				dest.weekStart = src.WeekStart

				dest.weekEnd = src.WeekEnd

				dest.timeStart = src.TimeStart

				dest.timeEnd = src.TimeEnd

		}


def listMapping1

listMapping1 =
		{
			src,dest  ->

				dest.huntingProfile = src.HuntingProfile

				dest.effMode = src.EffectiveTime.Mode

				dest.effDate = parseDate(src.EffectiveTime.Time, Constant4Model.DATE_FORMAT)

				dest.expDate = parseDate(src.ExpDate, Constant4Model.DATE_FORMAT)

				mappingList(src.WeekTimeSchema,dest.weekTimeSchemaList,listMapping2)

		}

def listMapping3

listMapping3 =
		{
			src,dest  ->

				dest.huntingProfile = src.HuntingProfile

				dest.expMode = src.ExpiredTime.Mode

				dest.expDate = parseDate(src.ExpiredTime.Time, Constant4Model.DATE_FORMAT)

		}


def srcMidVar = srcArgs0.ManageCallHuntingProfileRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.ManageCallHuntingProfileRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.ManageCallHuntingProfileRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.ManageCallHuntingProfileRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.ManageCallHuntingProfileRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar.Version

def srcMidVar4 = srcArgs0.ManageCallHuntingProfileRequestMsg.ManageCallHuntingProfileRequest.CallHuntingProfile

mappingList(srcMidVar4.AddCallHuntingProfile,destArgs1.callHuntingProfile.addCallHuntingProfileList,listMapping1)

mappingList(srcMidVar4.DelCallHuntingProfile,destArgs1.callHuntingProfile.delCallHuntingProfileList,listMapping3)

mappingList(srcMidVar4.ModCallHuntingProfile,destArgs1.callHuntingProfile.modCallHuntingProfileList,listMapping1)

def destMidVar = destArgs1.groupAccessCode

def srcMidVar5 = srcArgs0.ManageCallHuntingProfileRequestMsg.ManageCallHuntingProfileRequest.SubGroupAccessCode

destMidVar.groupCode = srcMidVar5.SubGroupCode

destMidVar.groupKey = srcMidVar5.SubGroupKey
