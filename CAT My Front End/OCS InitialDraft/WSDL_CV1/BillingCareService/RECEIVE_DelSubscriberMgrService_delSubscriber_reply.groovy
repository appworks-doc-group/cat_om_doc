import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return
                            
def destMidVar = destReturn.DelSubscriberResultMsg.ResultHeader

def srcHeader = srcReturn.resultHeader

destMidVar.CommandId = srcHeader.commandId

destMidVar.Version = srcHeader.version

destMidVar.TransactionId = srcHeader.transactionId

destMidVar.SequenceId = srcHeader.sequenceId

destMidVar.ResultCode = srcHeader.resultCode

destMidVar.ResultDesc = srcHeader.resultDesc

destMidVar.OrderId = srcHeader.orderId

destMidVar.performanceStatCmd = "DelSubscriber"

destMidVar.OperationTime = srcHeader.operationTime
