import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.ContractID = src.contractID

	dest.InstallmentInstID = src.installmentInstID
	
	dest.CycleClass = src.cycleClass
	
	dest.DelayFlag = src.delayFlag
	
	dest.Status = src.status
	
	dest.InitialAmount = src.initialAmount
	
	dest.Amount = src.amount
	
	dest.CurrencyID = src.currencyID
	
	dest.CycleDueDate=formatDate(src.cycleDueDate, Constant4Model.DATE_FORMAT)
	
	dest.CycleSequence = src.cycleSequence
	
	dest.RealRepayDate=formatDate(src.realRepayDate, Constant4Model.DATE_FORMAT)
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def srcMidVar = srcReturn

def destMidVar = destReturn.CancelInstallmentResultMsg.CancelInstallmentResult

mappingList(srcMidVar.installmentDetailList,destMidVar.InatallmentDetail,listMapping0)

def destMidVar1 = destReturn.CancelInstallmentResultMsg.ResultHeader

def srcMidVar1 = srcReturn.resultHeader

destMidVar1.MsgLanguageCode = srcMidVar1.msgLanguageCode

destMidVar1.ResultCode = srcMidVar1.resultCode

destMidVar1.ResultDesc = srcMidVar1.resultDesc

destMidVar1.Version = srcMidVar1.version

destMidVar1.MessageSeq = srcMidVar1.messageSeq

mappingList(srcMidVar1.simpleProperty,destMidVar1.AdditionalProperty,listMapping1)
