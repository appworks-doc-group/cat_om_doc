dest.setServiceOperation("OCS33BB4CCInterfaceService", "syncInstalment")

def srcArgs0 = src.payload._args[0]
def srcMidVar0 = srcArgs0.SyncInstalmentRequestMsg.RequestHeader
def srcMidVar2 = srcArgs0.SyncInstalmentRequestMsg.SyncInstalmentRequest

def destArgs0 = dest.payload._args[0]
def destArgs1 = dest.payload._args[1]

destArgs0._class = "com.huawei.ngcbs.cm.ocs33ws.charge2Billing.ws.bo.BB4CCMessageHeader"
destArgs1._class = "com.huawei.ngcbs.cm.ocs33ws.instalment.syncinstalment.io.OCS33SyncInstalmentRequest"


def listMapping0
listMapping0 = 
{
    src,dest  ->
    
	dest.name = src.name
	
	dest.value = src.value
}


def listMapping5
listMapping5 = 
{
    src,dest  ->
    
	dest.period = src.Period
	
	dest.amount = src.Amount
	
	dest.taxAmount = src.TaxAmount
	
	dest.interest = src.Interest
}



def listMapping2
listMapping2 = 
{
    src,dest  ->
    
	dest.otherId = src.OtherId
	
	dest.effDate = src.EffDate
	
	dest.itemId = src.ItemId
	
	dest.spMth = src.SpMth
	
	dest.description = src.Description
	
	dest.quantity = src.Quantity
	
	mappingList(src.PeriodSegmentList.PeriodSegment, dest.periodSegmentList, listMapping5)
	
	dest.remark = src.Remark
}



def listMapping3
listMapping3 = 
{
    src,dest  ->
    
	dest.processFlag = src.ProcessFlag
	
	dest.otherId = src.OtherId
	
	dest.adjustId = src.AdjustId
	
	dest.cancelDate = src.CancelDate
	
	dest.cancelReason = src.CancelReason
	
	dest.remark = src.Remark
}

def listMapping4
listMapping4 = 
{
    src,dest  ->
    
	dest.otherId = src.OtherId
	
	dest.adjustId = src.AdjustId
	
	dest.totalAmount = src.TotalAmount
	
	dest.finishPeriod = src.FinishPeriod
	
	dest.finishAmount = src.FinishAmount
	
	dest.openAmount = src.OpenAmount
	
	dest.newAcctId = src.NewAcctId
	
	dest.newSubId = src.NewSubId
	
	dest.newOtherId = src.NewOtherId
	
	dest.newSpMth = src.NewSpMth
	
	dest.newDescription = src.NewDescription
	
	dest.newQuantity = src.NewQuantity
	
	mappingList(src.NewPeriodSegmentList.PeriodSegment, dest.newPeriodSegmentList, listMapping5)
	

}

def listMapping1
listMapping1 = 
{
    src,dest  ->
    	
	dest.acctId = src.AcctId	
	
	dest.subId = src.SubId
	
	mappingList(src.CreateItemList.ItemValue, dest.createItemList, listMapping2)
	
	mappingList(src.CancelItemList.ItemValue, dest.cancelItemList, listMapping3)
	
	mappingList(src.ModifyItemList.ItemValue, dest.modifyItemList, listMapping4)

}

destArgs0.messageSeq = srcMidVar0.SerialNo
destArgs0.serialNo = srcMidVar0.SerialNo
destArgs0.requestFrom = srcMidVar0.RequestFrom
destArgs0.operatorId = srcMidVar0.OperatorId
destArgs0.departmentId = srcMidVar0.DepartmentId
destArgs0.version = srcMidVar0.Version
destArgs0.remark = srcMidVar0.Remark
mappingList(srcMidVar0.Param, destArgs0.param, listMapping0)
destArgs0.tenantId = srcMidVar0.TenantId
destArgs0.msgLanguageCode = srcMidVar0.Language

destArgs1.busiType = srcMidVar2.BusiType
destArgs1.bsno = srcMidVar2.Bsno
destArgs1.channelId = srcMidVar2.ChannelId

mappingList(srcMidVar2.SubFeeList.SubFeeValue, destArgs1.subFeeList, listMapping1)
