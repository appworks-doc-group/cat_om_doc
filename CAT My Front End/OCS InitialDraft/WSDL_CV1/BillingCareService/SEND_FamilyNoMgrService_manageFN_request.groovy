dest.setServiceOperation("OCS33FamilyNoMgrService","manageFN")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs33ws.core.bo.Ocs33MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs33ws.mansubfamilyno.io.OCS33ManageFNRequest"
def srcMessageHeader = srcArgs0.ManSubFamilyNoRequestMsg.RequestHeader
def srcMessageBody = srcArgs0.ManSubFamilyNoRequestMsg.ManSubFamilyNoRequest
def destArgs2 = destArgs1.familyNos
def srcFNo = srcMessageBody.FamilyNoInfo


destArgs0.commandId = srcMessageHeader.CommandId
destArgs0.version = srcMessageHeader.Version
destArgs0.transactionId = srcMessageHeader.TransactionId
destArgs0.sequenceId = srcMessageHeader.SequenceId
destArgs0.requestType = srcMessageHeader.RequestType
destArgs0.loginSystem = srcMessageHeader.SessionEntity.Name
destArgs0.password = srcMessageHeader.SessionEntity.Password
destArgs0.remoteAddress = srcMessageHeader.SessionEntity.RemoteAddress
destArgs0.interFrom = srcMessageHeader.InterFrom
destArgs0.interMedi = srcMessageHeader.InterMedi
destArgs0.interMode = srcMessageHeader.InterMode
destArgs0.visitArea = srcMessageHeader.visitArea
destArgs0.currentCell = srcMessageHeader.currentCell
destArgs0.additionInfo = srcMessageHeader.additionInfo
destArgs0.thirdPartyId = srcMessageHeader.ThirdPartyID
destArgs0.partnerId = srcMessageHeader.PartnerID
destArgs0.operatorId = srcMessageHeader.OperatorID
destArgs0.tradePartnerId = srcMessageHeader.TradePartnerID
destArgs0.partnerOperId = srcMessageHeader.PartnerOperID
destArgs0.belToAreaId = srcMessageHeader.BelToAreaID
destArgs0.reserve2 = srcMessageHeader.Reserve2
destArgs0.reserve3 = srcMessageHeader.Reserve3
destArgs0.messageSeq = srcMessageHeader.SerialNo
destArgs0.remark = srcMessageHeader.Remark
destArgs0.msgLanguageCode  = srcMessageHeader.Language
destArgs0.performanceStatCmd = "manageFN"

def listMapping0
listMapping0 = 
{
    src,dest  ->
	dest.familyNo=src.FamilyNo
	dest.phoneNoOrder=src.phoneNoOrder
	dest.subGroupType=src.subGroupType
	dest.effectiveDate=src.effectiveDate
	dest.expireDate=src.expireDate
	dest.offerId=src.OfferId
	dest.newFamilyNo=src.NewFamilyNo

}
mappingList(srcFNo,destArgs2,listMapping0)

destArgs1.subscriberNo = srcMessageBody.SubscriberNo
destArgs1.operationType = srcMessageBody.OperationType
destArgs1.handlingChargeFlag = srcMessageBody.HandleChargeFlag
destArgs1.familyNoInfo = destArgs2