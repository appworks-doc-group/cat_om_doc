def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.query.queryofferinginstproperty.io.QueryOfferingInstPropertyRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
    dest.oCode = src.OfferingCode
    dest.pSeq = src.PurchaseSeq
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.productId = src.ProductID
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

    def oKeyExt = dest.offeringKey
    oKeyExt._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
	listMapping2.call(src.OfferingKey,oKeyExt)
	
	mappingList(src.ProductInst,dest.productIdList,listMapping3)
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.accoutCode = src.AccountCode
	
	dest.accoutKey = src.AccountKey
	
	dest.payType = src.PayType
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping8

listMapping8 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	listMapping5.call(src.AcctAccessCode,dest.acctAccessCode)
	
	listMapping6.call(src.CustAccessCode,dest.custAccessCode)
	
	listMapping7.call(src.SubAccessCode,dest.subAccessCode)
	
	listMapping8.call(src.SubGroupAccessCode,dest.groupAccessCode)
	
}

def srcMidVar = srcArgs0.QueryOfferingInstPropertyRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.QueryOfferingInstPropertyRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.QueryOfferingInstPropertyRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.QueryOfferingInstPropertyRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.QueryOfferingInstPropertyRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar.Version

def srcMidVar4 = srcArgs0.QueryOfferingInstPropertyRequestMsg.QueryOfferingInstPropertyRequest

mappingList(srcMidVar4.OfferingInst,destArgs1.offeringInst,listMapping1)

listMapping4.call(srcMidVar4.OfferingOwner,destArgs1.anyAccessCode)
