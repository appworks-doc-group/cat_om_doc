dest.setServiceOperation("SubscriberService","queryFPHCaller")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.fphcaller.query.io.QueryFPHCallerRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.QueryFphCallerRequestMsg.QueryFphCallerRequest

destArgs1.listType = srcMidVar.ListType

def srcMidVar0 = srcArgs0.QueryFphCallerRequestMsg.RequestHeader

destArgs0.version = srcMidVar0.Version

def srcMidVar1 = srcArgs0.QueryFphCallerRequestMsg.RequestHeader.TimeFormat

destArgs0.timeZoneId = srcMidVar1.TimeZoneID

destArgs0.timeType = srcMidVar1.TimeType

def srcMidVar2 = srcArgs0.QueryFphCallerRequestMsg.RequestHeader.OwnershipInfo

destArgs0.brId = srcMidVar2.BRID

destArgs0.beId = srcMidVar2.BEID

def srcMidVar3 = srcArgs0.QueryFphCallerRequestMsg.RequestHeader.OperatorInfo

destArgs0.operatorId = srcMidVar3.OperatorID

destArgs0.channelId = srcMidVar3.ChannelID

destArgs0.msgLanguageCode = srcMidVar0.MsgLanguageCode

destArgs0.messageSeq = srcMidVar0.MessageSeq

mappingList(srcMidVar0.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

def srcMidVar4 = srcArgs0.QueryFphCallerRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar4.LoginSystemCode

destArgs0.password = srcMidVar4.Password

destArgs0.remoteAddress = srcMidVar4.RemoteIP

destArgs0.interMode = srcMidVar0.AccessMode

def destMidVar = destArgs1.subAccessCode

destMidVar._class = "com.huawei.ngcbs.bm.domain.entity.common.io.SubAccessCode"

destMidVar.primaryIdentity = srcMidVar.SubscriberNo
