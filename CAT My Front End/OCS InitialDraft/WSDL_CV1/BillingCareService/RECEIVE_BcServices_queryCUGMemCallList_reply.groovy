def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 =
{
	src,dest  ->

		dest.ControlNumber = src.controlNumber

		dest.FlowType = src.flowType

		dest.NumberType = src.numberType
}

def destMidVar0 = destReturn.QueryCUGMemCallListResultMsg.QueryCUGMemCallListResult

def srcMidVar = srcReturn.resultBody

mappingList(srcMidVar.callList,destMidVar0.CallList,listMapping1)

def destMidVar = destReturn.QueryCUGMemCallListResultMsg.ResultHeader

destMidVar.MsgLanguageCode = srcReturn.resultHeader.msgLanguageCode

destMidVar.ResultCode = srcReturn.resultHeader.resultCode

destMidVar.ResultDesc = srcReturn.resultHeader.resultDesc

mappingList(srcReturn.resultHeader.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

destMidVar.Version = srcReturn.resultHeader.version
