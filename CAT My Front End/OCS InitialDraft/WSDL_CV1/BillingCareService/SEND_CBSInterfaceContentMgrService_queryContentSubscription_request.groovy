dest.setServiceOperation("CBSInterfaceContentMgrService", "queryContentSubscription");

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.content.querycontentsubscription.io.QueryContentSubscriptionRequest"


def srcMidVar0 = srcArgs0.QueryContentSubscriptionRequestMsg.RequestHeader

destArgs0.beId = srcMidVar0.TenantId

destArgs0.msgLanguageCode = srcMidVar0.Language

destArgs0.operatorId = srcMidVar0.OperatorID

destArgs0.additionInfo = srcMidVar0.additionInfo

destArgs0.commandId = srcMidVar0.CommandId

destArgs0.interMedi = srcMidVar0.InterMedi

destArgs0.transactionId = srcMidVar0.TransactionId

destArgs0.reserve2 = srcMidVar0.Reserve2

destArgs0.reserve3 = srcMidVar0.Reserve3

destArgs0.interMode = srcMidVar0.InterMode

destArgs0.sequenceId = srcMidVar0.SequenceId

destArgs0.visitArea = srcMidVar0.visitArea

destArgs0.messageSeq = srcMidVar0.SerialNo

destArgs0.belToAreaId = srcMidVar0.BelToAreaID

destArgs0.partnerId = srcMidVar0.PartnerID

destArgs0.thirdPartyId = srcMidVar0.ThirdPartyID

destArgs0.remark = srcMidVar0.Remark

destArgs0.tradePartnerId = srcMidVar0.TradePartnerID

destArgs0.version = srcMidVar0.Version

destArgs0.partnerOperId = srcMidVar0.PartnerOperID

def srcMidVar1 = srcArgs0.QueryContentSubscriptionRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar1.Name

destArgs0.password = srcMidVar1.Password

destArgs0.remoteAddress = srcMidVar1.RemoteAddress

destArgs0.requestType = srcMidVar0.RequestType

destArgs0.interFrom = srcMidVar0.InterFrom

destArgs0.currentCell = srcMidVar0.currentCell


def srcMidVar2 = srcArgs0.QueryContentSubscriptionRequestMsg.QueryContentSubscriptionRequest

destArgs1.subscriberNo = srcMidVar2.SubscriberNo

destArgs1.queryMethod = srcMidVar2.QueryMethod

def listMapping0
listMapping0 = 
{
    src, dest  ->
    
	dest.serviceType = src.ServiceType
}

mappingList(srcMidVar2.Services, destArgs1.servicesList, listMapping0)



