dest.setServiceOperation("CBSInterfaceBusinessMgrService","newSubscriber");

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs12ws.subscriber.createsubscriber.io.CreateSubscriberRequestOcs12Base"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Id
	
	dest.value = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Id
	
	dest.value = src.Value
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.code = src.Id
	
	dest.value = src.Value
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	def destMidVar14 = dest.property
	
	destMidVar14.propCode = src.Id
	
	destMidVar14.value = src.Value
	
}

def listMappingProdProps

listMappingProdProps =
{
	    src,dest  ->
	
		dest.id = src.Id
	
		dest.value = src.Value
}


def listMapping4

listMapping4 = 
{
    src,dest  ->
    
    dest.id = src.Id	
    
    mappingList(src.SimpleProperty, dest.prodOrderProps, listMappingProdProps)
}

def srcMidVar = srcArgs0.NewSubscriberRequestMsg.RequestHeader

destArgs0.beId = srcMidVar.TenantId

destArgs0.additionInfo = srcMidVar.additionInfo

destArgs0.belToAreaId = srcMidVar.BelToAreaID

destArgs0.commandId = srcMidVar.CommandId

destArgs0.currentCell = srcMidVar.currentCell

destArgs0.interFrom = srcMidVar.InterFrom

destArgs0.interMedi = srcMidVar.InterMedi

destArgs0.interMode = srcMidVar.InterMode

destArgs0.operatorId = srcMidVar.OperatorID

destArgs0.partnerId = srcMidVar.PartnerID

destArgs0.partnerOperId = srcMidVar.PartnerOperID

destArgs0.remark = srcMidVar.Remark

destArgs0.requestType = srcMidVar.RequestType

destArgs0.reserve2 = srcMidVar.Reserve2

destArgs0.reserve3 = srcMidVar.Reserve3

destArgs0.sequenceId = srcMidVar.SequenceId

destArgs0.messageSeq = srcMidVar.SerialNo

def srcMidVar0 = srcArgs0.NewSubscriberRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar0.Name

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteAddress

destArgs0.thirdPartyId = srcMidVar.ThirdPartyID

destArgs0.tradePartnerId = srcMidVar.TradePartnerID

destArgs0.version = srcMidVar.Version

destArgs0.transactionId = srcMidVar.TransactionId

destArgs0.visitArea = srcMidVar.visitArea

def destMidVar = destArgs1.regCustomerInfo.individualInfo

def srcMidVar1 = srcArgs0.NewSubscriberRequestMsg.NewSubscriberRequest.Customer

destMidVar.firstName = srcMidVar1.Name

def destMidVar0 = destArgs1.regCustomerInfo.customerInfo

destMidVar0.custCode = srcMidVar1.Code

destMidVar.idType = srcMidVar1.IdType

destMidVar.idNumber = srcMidVar1.IdCode

destMidVar.gender = srcMidVar1.Gender

destArgs1.strBirthday = srcMidVar1.Birthday

def destMidVar1 = destArgs1.addresses[0]

destMidVar1.addr1 = srcMidVar1.Address

destMidVar1.postCode = srcMidVar1.ZipCode

destMidVar0.custLevel = srcMidVar1.Grade

destArgs1.custBelToAreaID = srcMidVar1.BelToAreaID

destMidVar.email = srcMidVar1.Email

destArgs1.custRegistrationTime = srcMidVar1.RegistrationTime

def destMidVar2 = destArgs1.regCustomerInfo

mappingList(srcMidVar1.SimpleProperty,destMidVar2.custProperties,listMapping0)

destMidVar0.custType = srcMidVar1.CustomerType

destMidVar.nationality = srcMidVar1.Country

destMidVar.nativePlace = srcMidVar1.NativePlace

destMidVar.race = srcMidVar1.NationType

destMidVar.occupation = srcMidVar1.JobType

destMidVar.education = srcMidVar1.Education

destArgs1.custCreditGrade = srcMidVar1.CreditGrade

destArgs1.custCreditAmount = srcMidVar1.CreditAmount

destArgs1.custSkill = srcMidVar1.Skill

destArgs1.custSocialNo = srcMidVar1.SocialNo

destArgs1.custState = srcMidVar1.CustomerState

destMidVar.marriedStatus = srcMidVar1.MaritalStatus

def srcMidVar2 = srcArgs0.NewSubscriberRequestMsg.NewSubscriberRequest.Subscriber

destArgs1.subBelToAreaID = srcMidVar2.BelToAreaID

def destMidVar3 = destArgs1.createSubscriberInfo.subBrandInfo

destMidVar3.brand = srcMidVar2.BrandId

destArgs1.subImsi = srcMidVar2.IMSI

def destMidVar4 = destArgs1.createSubscriberInfo.subscriberInfo

destMidVar4.ivrLang = srcMidVar2.Lang

destMidVar4.writtenLang = srcMidVar2.SMSLang

destArgs1.subRegistrationTime = srcMidVar2.RegistrationTime

destMidVar4.subscriberKey = srcMidVar2.Code

destArgs1.strMainProdId = srcMidVar2.MainProductID

def destMidVar6 = destArgs1.subPaymentMode

destMidVar6.paymentMode = srcMidVar2.PaidMode

destMidVar4.subPassword = srcMidVar2.Password

def destMidVar7 = destArgs1.createSubscriberInfo

mappingList(srcMidVar2.SimpleProperty,destMidVar7.subProperties,listMapping1)

def destMidVar8 = destArgs1.createSubscriberInfo.subIdentities[0]

def srcMidVar3 = srcArgs0.NewSubscriberRequestMsg.NewSubscriberRequest

destMidVar8.subIden = srcMidVar3.SubscriberNo

def destMidVar9 = destArgs1.createAccountInfos[0].accountInfo

def srcMidVar4 = srcArgs0.NewSubscriberRequestMsg.NewSubscriberRequest.Account

destMidVar9.acctName = srcMidVar4.Name

destMidVar9.acctCode = srcMidVar4.Code

destMidVar1.addr2 = srcMidVar4.Address

def destMidVar10 = destArgs1.createAccountInfos[0].contactInfo

destMidVar10.title = srcMidVar4.Title

def destMidVar11 = destArgs1.createAccountInfos[0]

destMidVar11.billCycleType = srcMidVar4.BillcycleType

mappingList(srcMidVar4.SimpleProperty,destMidVar11.properties,listMapping2)

mappingList(srcMidVar3.Product,destArgs1.appProdList,listMapping4)
