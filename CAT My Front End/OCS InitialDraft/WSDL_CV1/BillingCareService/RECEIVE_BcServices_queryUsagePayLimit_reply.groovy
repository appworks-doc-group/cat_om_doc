import com.huawei.ngcbs.bm.common.common.Constant4Model;

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping_TempLimit

listMapping_TempLimit = 
{
	src,dest  ->
	
	dest.TempLimitValue = src.tempLimitValue
	
	dest.EffDate = formatDate(src.effDate,Constant4Model.DATE_FORMAT)
	
	dest.ExpDate = formatDate(src.expDate,Constant4Model.DATE_FORMAT)
	
}

def listMapping_Limit

listMapping_Limit = 
{
    src,dest  ->

	dest.LimitValue = src.limitValue
	
	mappingList(src.tempLimitList,dest.TempLimit,listMapping_TempLimit)
	
	dest.UsedLimit = src.usedLimit
	
	dest.ReserveLimit = src.reserveLimit
	
	dest.BeginDate = formatDate(src.beginDate,Constant4Model.DATE_FORMAT)
	
	dest.EndDate = formatDate(src.endDate,Constant4Model.DATE_FORMAT)
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.UsagePayLimitKey = src.usagePayLimitKey
	
	dest.EventType = src.eventType
	
	dest.LimitRule = src.limitRule
	
	dest.LimitCycleType = src.limitCycleType
	
	dest.LimitMeasureUnit = src.limitMeasureUnit
	
	dest.EffDate = formatDate(src.effDate,Constant4Model.DATE_FORMAT)
	
	dest.ExpDate = formatDate(src.expDate,Constant4Model.DATE_FORMAT)
	
	listMapping_Limit.call(src.limit,dest.Limit)
	
}

def destMidVar = destReturn.QueryUsagePayLimitResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.Version = srcMidVar.version

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.MessageSeq = srcMidVar.messageSeq

mappingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def destMidVar1 = destReturn.QueryUsagePayLimitResultMsg.QueryUsagePayLimitResult

def srcMidVar1 = srcReturn.resultBody

mappingList(srcMidVar1.usagePayLimitList,destMidVar1.UsagePayLimitList,listMapping1)