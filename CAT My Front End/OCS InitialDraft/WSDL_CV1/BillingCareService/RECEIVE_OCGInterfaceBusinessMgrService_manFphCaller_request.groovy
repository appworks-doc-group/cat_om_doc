def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.CallingNumber = src.callingNumber
	
	dest.SerialNo = src.serialNo
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Name = src.name
	
	dest.Password = src.password
	
	dest.RemoteAddress = src.remoteAddress
	
}

def destMidVar = destArgs0.ManFphCallerRequestMsg.ManFphCallerRequest

mappingList(srcArgs0.fphCaller,destMidVar.FphCaller,listMapping0)

destMidVar.ListType = srcArgs0.listType

destMidVar.OperationType = srcArgs0.operationType

destMidVar.SubscriberNo = srcArgs0.subscriberNo

def destMidVar0 = destArgs0.ManFphCallerRequestMsg.RequestHeader

def srcMidVar = srcArgs0.omRequestHeader

destMidVar0.BelToAreaID = srcMidVar.belToAreaId

destMidVar0.CommandId = srcMidVar.commandId

destMidVar0.currentCell = srcMidVar.currentCell

destMidVar0.InterFrom = srcMidVar.interFrom

destMidVar0.InterMedi = srcMidVar.interMedi

destMidVar0.InterMode = srcMidVar.interMode

destMidVar0.OperatorID = srcMidVar.operatorId

destMidVar0.PartnerID = srcMidVar.partnerId

destMidVar0.PartnerOperID = srcMidVar.partnerOperId

destMidVar0.Remark = srcMidVar.remark

destMidVar0.RequestType = srcMidVar.requestType

destMidVar0.SequenceId = srcMidVar.sequenceId

destMidVar0.SerialNo = srcMidVar.serialNo

listMapping1.call(srcMidVar.sessionEntity,destMidVar0.SessionEntity)

destMidVar0.TenantID = srcMidVar.tenantId

destMidVar0.ThirdPartyID = srcMidVar.thirdPartyId

destMidVar0.TradePartnerID = srcMidVar.tradePartnerId

destMidVar0.TransactionId = srcMidVar.transactionId

destMidVar0.Version = srcMidVar.version

destMidVar0.visitArea = srcMidVar.visitArea
