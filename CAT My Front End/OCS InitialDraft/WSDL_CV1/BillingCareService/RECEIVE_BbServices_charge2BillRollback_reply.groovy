def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destReturn.Charge2BillRollbackResultMsg.Charge2BillRollbackResult

def srcMidVar = srcReturn.charge2BillRoolbackResultInfo

destMidVar.Charge2BillSerialNo = srcMidVar.charge2BillSerialNo

def destMidVar0 = destReturn.Charge2BillRollbackResultMsg.ResultHeader

def srcMidVar0 = srcReturn.resultHeader

destMidVar0.MsgLanguageCode = srcMidVar0.msgLanguageCode

destMidVar0.ResultCode = srcMidVar0.resultCode

destMidVar0.ResultDesc = srcMidVar0.resultDesc

destMidVar0.MessageSeq = srcMidVar0.messageSeq

mappingList(srcMidVar0.simpleProperty,destMidVar0.AdditionalProperty,listMapping0)

destMidVar0.Version = srcMidVar0.version
