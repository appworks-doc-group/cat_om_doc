dest.setServiceOperation("arUVSInterfaceExtend","queryUnbilledFUBal")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destArgs1 = dest.payload._args[1]

destArgs0._class = "com.huawei.ngcbs.cm.ocs11ws.core.bo.Ocs11MessageHeader"

destArgs1._class = "com.huawei.ngcbs.cm.query.queryUnbilledFUBal.io.QueryUnbilledFUBalRequest"

def srcMessageHeader = srcArgs0.QueryUnbilledFUBal.QueryUnbilledFUBalRequest.RequestMessage.MessageHeader

def srcMessageBody = srcArgs0.QueryUnbilledFUBal.QueryUnbilledFUBalRequest.RequestMessage.MessageBody

def srcSessionEntity = srcArgs0.QueryUnbilledFUBal.SessionEntity


destArgs0.loginSystem = srcSessionEntity.userID

destArgs0.password = srcSessionEntity.password


destArgs0.sessionEntity.userID = srcSessionEntity.userID

destArgs0.sessionEntity.password = srcSessionEntity.password

destArgs0.sessionEntity.locale = srcSessionEntity.locale

destArgs0.sessionEntity.loginVia = srcSessionEntity.loginVia

destArgs0.sessionEntity.remoteAddr = srcSessionEntity.remoteAddr

destArgs0.sessionEntity.uploadRoot = srcSessionEntity.uploadRoot

destArgs0.commandId = srcMessageHeader.CommandId

destArgs0.version = srcMessageHeader.Version

destArgs0.transactionId = srcMessageHeader.TransactionId

destArgs0.sequenceId = srcMessageHeader.SequenceId

destArgs0.requestType = srcMessageHeader.RequestType

destArgs0.tenantId = srcMessageHeader.TenantId

destArgs0.language = srcMessageHeader.Language


destArgs1.subAccessCode.primaryIdentity = srcMessageBody.MSISDN

destArgs1.subAccessCode.subscriberKey = srcMessageBody.SubId

destArgs1.acctAccessCode.accoutKey = srcMessageBody.AcctId

destArgs1.cycleId = srcMessageBody.CycleId



