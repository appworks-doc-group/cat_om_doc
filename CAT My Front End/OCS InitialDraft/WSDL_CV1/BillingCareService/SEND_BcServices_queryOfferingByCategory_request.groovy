dest.setServiceOperation("BMQueryService", "queryOfferingByCategory")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.query.queryofferingbycategory.io.QueryOfferingByCategoryRequest"

def listMapping0

listMapping0 =
        {
            src, dest ->

                dest.code = src.Code

                dest.value = src.Value

        }

def srcMidVar = srcArgs0.QueryOfferingByCategoryRequestMsg.QueryOfferingByCategoryRequest

def destMidVar = destArgs1.subAccessCode

def srcMidVar0 = srcMidVar.SubAccessCode

destMidVar.primaryIdentity = srcMidVar0.PrimaryIdentity

destMidVar.subscriberKey = srcMidVar0.SubscriberKey

def srcMidVar1 = srcMidVar.OfferingCategory

destArgs1.offeringCategory = srcMidVar.OfferingCategory

def srcMidVar2 = srcArgs0.QueryOfferingByCategoryRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar2.AccessMode

def srcMidVar3 = srcMidVar2.AccessSecurity

destArgs0.loginSystem = srcMidVar3.LoginSystemCode

destArgs0.password = srcMidVar3.Password

destArgs0.remoteAddress = srcMidVar3.RemoteIP

destArgs0.businessCode = srcMidVar3.BusinessCode

destArgs0.messageSeq = srcMidVar2.MessageSeq

destArgs0.msgLanguageCode = srcMidVar2.MsgLanguageCode

def srcMidVar4 = srcMidVar2.OperatorInfo

destArgs0.channelId = srcMidVar4.ChannelID

destArgs0.operatorId = srcMidVar4.OperatorID

def srcMidVar5 = srcMidVar2.OwnershipInfo

destArgs0.beId = srcMidVar5.BEID

destArgs0.brId = srcMidVar5.BRID

def srcMidVar6 = srcMidVar2.TimeFormat

destArgs0.timeType = srcMidVar6.TimeType

destArgs0.timeZoneId = srcMidVar6.TimeZoneID

destArgs0.version = srcMidVar2.Version

mappingList(srcMidVar1.AdditionalProperty, destArgs0.simpleProperty, listMapping0)