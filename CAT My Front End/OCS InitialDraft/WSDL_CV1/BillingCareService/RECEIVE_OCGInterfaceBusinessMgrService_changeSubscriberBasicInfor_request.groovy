def srcArgs0 = src.payload._args[0]

def srcArgs1 = src.payload._args[1]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Id = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Id = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destArgs0.ChangeSubscriberBasicInforRequestMsg.RequestHeader

destMidVar.BelToAreaID = srcArgs0.belToAreaId

destMidVar.CommandId = srcArgs0.commandId

destMidVar.currentCell = srcArgs0.currentCell

destMidVar.InterFrom = srcArgs0.interFrom

destMidVar.InterMedi = srcArgs0.interMedi

destMidVar.InterMode = srcArgs0.interMode

destMidVar.OperatorID = srcArgs0.operatorId

destMidVar.PartnerID = srcArgs0.partnerId

destMidVar.PartnerOperID = srcArgs0.partnerOperId

destMidVar.Remark = srcArgs0.remark

destMidVar.RequestType = srcArgs0.requestType

destMidVar.SequenceId = srcArgs0.sequenceId

destMidVar.SerialNo = srcArgs0.serialNo

def destMidVar0 = destArgs0.ChangeSubscriberBasicInforRequestMsg.RequestHeader.SessionEntity

def srcMidVar = srcArgs0.sessionEntity

destMidVar0.Name = srcMidVar.name

destMidVar0.Password = srcMidVar.password

destMidVar0.RemoteAddress = srcMidVar.remoteAddress

destMidVar.TenantID = srcArgs0.tenantId

destMidVar.ThirdPartyID = srcArgs0.thirdPartyId

destMidVar.TradePartnerID = srcArgs0.tradePartnerId

destMidVar.TransactionId = srcArgs0.transactionId

destMidVar.Version = srcArgs0.version

destMidVar.visitArea = srcArgs0.visitArea

def destMidVar1 = destArgs0.ChangeSubscriberBasicInforRequestMsg.ChangeSubscriberBasicInforRequest

destMidVar1.Lang = srcArgs1.lang

mappingList(srcArgs1.simplePropertys,destMidVar1.SimpleProperty,listMapping0)

destMidVar1.SMSLang = srcArgs1.smsLang

destMidVar1.SubscriberNo = srcArgs1.subscriberNo

destMidVar1.USSDLang = srcArgs1.ussdLang

destMidVar1.FirstLifeCall = srcArgs1.firstLifeCall

mappingList(srcArgs1.simpleProperties,destMidVar1.SimpleProperty,listMapping1)
