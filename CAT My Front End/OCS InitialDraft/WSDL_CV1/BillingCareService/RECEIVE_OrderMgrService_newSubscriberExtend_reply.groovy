def srcReturn = src.payload._return
def destReturn = dest.payload._return

def destMidVar0 = destReturn.NewSubscriberExtendResultMsg.ResultHeader
def srcMidVar0 = srcReturn.resultHeader

def destMidVar = destReturn.NewSubscriberExtendResultMsg.NewSubscriberResult
def srcMidVar = srcReturn.resultBody

def listMappingProdProps
listMappingProdProps =
{
	src,dest  ->
	dest.Id= src.id 
	dest.Value = src.value
}

def listMapping1
listMapping1 = 
{
    src,dest  ->
	dest.offerKey = src.OfferKey
	dest.extOfferCode = src.ExtOfferCode
	dest.extOfferOrderCode = src.ExtOfferOrderCode
}

def listMapping0
listMapping0 = 
{
    src,dest  ->
	listMapping1.call(src.offerOrderIdentify,dest.OfferOrderIdentity)
	dest.OfferCode = src.offerCode 
	dest.OfferName = src.offerName
	dest.OfferOrderKey = src.offerOrderKey		
	dest.EffectiveDate = src.effectiveDate
	dest.ExpireDate = src.expireDate
	dest.AutoType = src.autoType
	dest.Status = src.status
	mappingList(src.simplePropertys,dest.SimpleProperty, listMappingProdProps)
}

mappingList(srcMidVar.offerOrderInfos,destMidVar.OfferOrderInfo,listMapping0)

destMidVar0.CommandId = srcMidVar0.commandId
destMidVar0.OperationTime = srcMidVar0.operationTime 
destMidVar0.OrderId = srcMidVar0.orderId
destMidVar0.ResultCode = srcMidVar0.resultCode
destMidVar0.ResultDesc = srcMidVar0.resultDesc
destMidVar0.SequenceId = srcMidVar0.sequenceId
destMidVar0.Version = srcMidVar0.version
destMidVar0.TransactionId = srcMidVar0.transactionId
destMidVar0.Language = srcMidVar0.msgLanguageCode
destMidVar0.TenantId = srcMidVar0.beId