dest.setServiceOperation("CRMForBSS","changeProductStatus")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Name = src.paraName
	
	dest.Value = src.paraValue
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Name = src.paraName
	
	dest.Value = src.paraValue
	
}

def destMidVar = destArgs0.ChangeProductStatusRequest.RequestHeader

def srcMidVar = srcArgs0.workOrderHeader

destMidVar.AccessPwd = srcMidVar.accessPwd

destMidVar.AccessUser = srcMidVar.accessUser

destMidVar.BEId = srcMidVar.beId

destMidVar.ChannelId = srcMidVar.channelId

def destMidVar0 = destMidVar.ExtParamList

mappingList(srcMidVar.extParamList,destMidVar0.ParameterInfo,listMapping0)

destMidVar.Language = srcMidVar.language

destMidVar.OperatorId = srcMidVar.operatorId

destMidVar.OperatorPwd = srcMidVar.operatorPwd

destMidVar.ProcessTime = srcMidVar.processTime

destMidVar.SessionId = srcMidVar.sessionId

destMidVar.TestFlag = srcMidVar.testFlag

destMidVar.TimeType = srcMidVar.timeType

destMidVar.TimeZoneID = srcMidVar.timeZoneId

destMidVar.TransactionId = srcMidVar.transactionId

destMidVar.Version = srcMidVar.version

def destMidVar1 = destArgs0.ChangeProductStatusRequest.ChangeProductStatus

mappingList(srcArgs0.extParamList,destMidVar1.ExtParamList,listMapping1)

destMidVar1.SubscriberID = srcArgs0.subscriberID

destMidVar1.Msisdn = srcArgs0.msisdn

destMidVar1.ChangeType = srcArgs0.changeType

destMidVar1.ProductCode = srcArgs0.productCode

destMidVar1.ChangeReason = srcArgs0.changeReason
