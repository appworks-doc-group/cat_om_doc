import com.huawei.ngcbs.bm.common.common.Constant4Model;
def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Id = src.code
	
	dest.Value = src.value
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.Id = src.id
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.OfferId = src.offerId
	
	dest.OfferOrderKey = src.offerOrderKey
	
	dest.EffectiveDate = src.effectiveDate
	
	dest.ExpireDate = src.expireDate
	
	dest.AutoType = src.autoType
	
	dest.OfferCode = src.offerCode
	
	dest.Status = src.status
	
	mappingList(src.properties,dest.SimpleProperty,listMapping3)
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->
	
	dest.BalanceId = src.balanceId
	
	dest.AccountType = src.accountType
	
	dest.Amount = src.amount
	
	dest.AccountCredit = src.accountCredit
	
	dest.ApplyTime = formatDate(src.applyTime, Constant4Model.DATE_FORMAT)
	
	dest.ExpireTime = formatDate(src.expireTime, Constant4Model.DATE_FORMAT)
	
	dest.RelatedOfferID = src.relatedOfferID
	
}
                            
def destMidVar = destReturn.QueryGroupInfoResultMsg.ResultHeader

def srcHeader = srcReturn.ocs33ResultHeader

destMidVar.CommandId = srcHeader.commandId

destMidVar.Version = srcHeader.version

destMidVar.TransactionId = srcHeader.transactionId

destMidVar.SequenceId = srcHeader.sequenceId

destMidVar.ResultCode = srcHeader.resultCode

destMidVar.ResultDesc = srcHeader.resultDesc

destMidVar.OrderId = srcHeader.orderId

def destMidVar1 = destReturn.QueryGroupInfoResultMsg.QueryGroupInfoResult.CustomerInfo.Customer

def srcMidVar1 = srcReturn.customer

destMidVar1.ParentGroupNumber = srcMidVar1.pGroupNumber

destMidVar1.GroupNumber = srcMidVar1.groupNumber

destMidVar1.GroupType = srcMidVar1.groupType

destMidVar1.GroupName = srcMidVar1.groupName

destMidVar1.GroupCustomerCode = srcMidVar1.groupCustomerCode

destMidVar1.GroupCustomerID = srcMidVar1.groupCustomerID

destMidVar1.QuotaAmount = srcMidVar1.quotaAmount

mappingList(srcMidVar1.propertyList,destMidVar1.SimpleProperty,listMapping0)

def destMidVar2 = destReturn.QueryGroupInfoResultMsg.QueryGroupInfoResult.CustomerInfo.OfferOrderInfo

mappingList(srcReturn.offerOrderInfoList,destMidVar2,listMapping1)

def destMidVar3 = destReturn.QueryGroupInfoResultMsg.QueryGroupInfoResult.AccountInfo.BalanceRecord

mappingList(srcReturn.balanceRecordList,destMidVar3,listMapping2)

def destMidVar4 = destReturn.QueryGroupInfoResultMsg.QueryGroupInfoResult.AccountInfo.Account

destMidVar4.AccountID = srcReturn.accountInfo.accountID

destMidVar4.AccountCode = srcReturn.accountInfo.accountCode

destMidVar4.PaidMode = srcReturn.accountInfo.paidMode

destMidVar4.POSAcctInitBal = srcReturn.accountInfo.pOSAcctInitBal

destMidVar4.POSAcctCredit = srcReturn.accountInfo.pOSAcctCredit

destMidVar4.BillCycleType = srcReturn.accountInfo.billCycleType