def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.cancelpredeactivatesub.io.CancelPreDeactivateSubRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.CancelPreDeactivationRequestMsg.RequestHeader

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

destArgs0.version = srcMidVar.Version

def srcMidVar0 = srcArgs0.CancelPreDeactivationRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

def srcMidVar1 = srcArgs0.CancelPreDeactivationRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.CancelPreDeactivationRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.CancelPreDeactivationRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

def srcMidVar4 = srcArgs0.CancelPreDeactivationRequestMsg.CancelPreDeactivationRequest

destArgs1.opType = srcMidVar4.OpType

def destMidVar = destArgs1.subAccessCode

def srcMidVar5 = srcArgs0.CancelPreDeactivationRequestMsg.CancelPreDeactivationRequest.SubAccessCode

destMidVar.primaryIdentity = srcMidVar5.PrimaryIdentity

destMidVar.subscriberKey = srcMidVar5.SubscriberKey

def destMidVar0 = destArgs1.cancelSubPreDeactivationInfo

def srcMidVar6 = srcArgs0.CancelPreDeactivationRequestMsg.CancelPreDeactivationRequest.ResumeStatus

destMidVar0.status = srcMidVar6.Status

destMidVar0.statusDetails = srcMidVar6.StatusDetail

destArgs0.interMode = srcMidVar.AccessMode
