dest.setServiceOperation("OtherToArServices","queryPaymentLog")

def srcArgs0 = src.payload._args[0]

def srcArgs1 = src.payload._args[1]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destArgs0.QueryPaymentLogRequestMsg.QueryPaymentLogRequest.AcctAccessCode

def srcMidVar = srcArgs1.acctAccessCode

destMidVar.AccountCode = srcMidVar.accoutCode

destMidVar.AccountKey = srcMidVar.accoutKey

destMidVar.PayType = srcMidVar.payType

destMidVar.PrimaryIdentity = srcMidVar.primaryIdentity

def destMidVar0 = destArgs0.QueryPaymentLogRequestMsg.QueryPaymentLogRequest

destMidVar0.BeginRowNum = srcArgs1.beginRowNum

destMidVar0.EndTime=formatDate(srcArgs1.endTime, "yyyyMMddHHmmss")

destMidVar0.FetchRowNum = srcArgs1.fetchRowNum

destMidVar0.StartTime=formatDate(srcArgs1.startTime, "yyyyMMddHHmmss")

destMidVar0.TotalRowNum = srcArgs1.totalRowNum

def destMidVar1 = destArgs0.QueryPaymentLogRequestMsg.RequestHeader

mappingList(srcArgs0.simpleProperty,destMidVar1.AdditionalProperty,listMapping0)

destMidVar1.Version = srcArgs0.version

def destMidVar2 = destArgs0.QueryPaymentLogRequestMsg.RequestHeader.TimeFormat

destMidVar2.TimeZoneID = srcArgs0.timeZoneId

destMidVar2.TimeType = srcArgs0.timeType

def destMidVar3 = destArgs0.QueryPaymentLogRequestMsg.RequestHeader.OwnershipInfo

destMidVar3.BRID = srcArgs0.brId

destMidVar3.BEID = srcArgs0.beId

destMidVar1.BusinessCode = srcArgs0.businessCode

def destMidVar4 = destArgs0.QueryPaymentLogRequestMsg.RequestHeader.OperatorInfo

destMidVar4.ChannelID = srcArgs0.channelId

destMidVar4.OperatorID = srcArgs0.operatorId

destMidVar1.MsgLanguageCode = srcArgs0.msgLanguageCode

destMidVar1.MessageSeq = srcArgs0.messageSeq

def destMidVar5 = destArgs0.QueryPaymentLogRequestMsg.RequestHeader.AccessSecurity

destMidVar5.LoginSystemCode = srcArgs0.loginSystem

destMidVar5.Password = srcArgs0.password

destMidVar5.RemoteIP = srcArgs0.remoteAddress

destMidVar1.AccessMode = srcArgs0.interMode
