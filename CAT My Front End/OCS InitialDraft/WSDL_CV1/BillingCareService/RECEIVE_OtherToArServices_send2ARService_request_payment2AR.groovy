dest.setServiceOperation("OtherToArServices","payment2AR")

def srcArgs0 = src.payload._args[0]

def srcArgs1 = src.payload._args[1]

def destArgs0 = dest.payload._args[0]

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.AcctNo = src.acctNo
	
	dest.AcctType = src.acctType
	
	dest.Amount = src.amount
	
	dest.BankBranchCode = src.bankBranchCode
	
	dest.BankCode = src.bankCode
	
	dest.ChequeNo = src.chequeNo
	
	dest.CreditCardType = src.creditCardType
	
	dest.CurrencyID = src.currencyId
	
	dest.CVVNumber = src.cVVNumber
	
	dest.ExpDate = src.expDate
	
	dest.PaymentMethod = src.paymentMethod
	
	dest.Remark = src.remark
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.TaxAmount = src.taxAmt
	
	dest.TaxCode = src.taxCode
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.ChargeAmt = src.chargeAmt
	
	dest.ChargeCode = src.chargeCode
	
	dest.ChargeSeq = src.chargeSeq
	
	dest.ChargeType = src.chargeType
	
	dest.CurrencyID = src.currencyId
	
	dest.DepositType = src.depositType
	
	dest.DiscountAmt = src.discountAmt
	
	dest.OfferingID = src.offeringId
	
	mappingList(src.paymentDetailValueList,dest.PaymentDetail,listMapping3)
	
	dest.Remark = src.remark
	
	dest.ResModeID = src.resModeId
	
	dest.ResTypeID = src.resTypeId
	
	dest.SalesAmount = src.salesAmount
	
	mappingList(src.simplePropertyList,dest.AdditionalProperty,listMapping4)
	
	mappingList(src.taxInfoList,dest.Tax,listMapping5)
	
	dest.UnitPrice = src.unitPrice
	
	dest.WaiveAmt = src.waiveAmt
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.PrimaryIdentity = src.primaryIdentity
	
	dest.SubscriberKey = src.subscriberKey
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destArgs0.Payment2ARRequestMsg.Payment2ARRequest

destMidVar.ChannelID = srcArgs1.channelId

destMidVar.InvoiceTime = srcArgs1.invoiceTime

destMidVar.Payment2ARSerialNo = srcArgs1.payment2ARSerialNo

destMidVar.PaymentTime = srcArgs1.paymentTime

destMidVar.SalesTime = srcArgs1.salesTime

mappingList(srcArgs1.feeDetailValueList,destMidVar.PaymentInfo,listMapping0)

def destMidVar0 = destArgs0.Payment2ARRequestMsg.Payment2ARRequest.PaymentObj

listMapping1.call(srcArgs1.acctAccessCode,destMidVar0.AcctAccessCode)

def destMidVar1 = destArgs0.Payment2ARRequestMsg.Payment2ARRequest.PaymentObj.AcctAccessCode

def srcMidVar = srcArgs1.acctAccessCode

destMidVar1.AccountCode = srcMidVar.accoutCode

destMidVar1.AccountKey = srcMidVar.accoutKey

destMidVar1.PayType = srcMidVar.payType

destMidVar1.PrimaryIdentity = srcMidVar.primaryIdentity

listMapping2.call(srcArgs1.subAccessCode,destMidVar0.SubAccessCode)

def destMidVar2 = destArgs0.Payment2ARRequestMsg.RequestHeader

destMidVar2.AccessMode = srcArgs0.interMode

mappingList(srcArgs0.simpleProperty,destMidVar2.AdditionalProperty,listMapping6)

destMidVar2.BusinessCode = srcArgs0.businessCode

destMidVar2.MessageSeq = srcArgs0.messageSeq

destMidVar2.MsgLanguageCode = srcArgs0.msgLanguageCode

def destMidVar3 = destArgs0.Payment2ARRequestMsg.RequestHeader.AccessSecurity

destMidVar3.LoginSystemCode = srcArgs0.loginSystem

destMidVar3.Password = srcArgs0.password

destMidVar3.RemoteIP = srcArgs0.remoteAddress

def destMidVar4 = destArgs0.Payment2ARRequestMsg.RequestHeader.OwnershipInfo

destMidVar4.BEID = srcArgs0.beId

destMidVar4.BRID = srcArgs0.brId

def destMidVar5 = destArgs0.Payment2ARRequestMsg.RequestHeader.OperatorInfo

destMidVar5.ChannelID = srcArgs0.channelId

destMidVar5.OperatorID = srcArgs0.operatorId

def destMidVar6 = destArgs0.Payment2ARRequestMsg.RequestHeader.TimeFormat

destMidVar6.TimeType = srcArgs0.timeType

destMidVar6.TimeZoneID = srcArgs0.timeZoneId

destMidVar2.Version = srcArgs0.version
