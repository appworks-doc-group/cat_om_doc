dest.setServiceOperation("BMQueryService","queryRebate")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.query.queryrebate.io.QueryRebateRequest"

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.accoutCode = src.AccountCode
	
	dest.accoutKey = src.AccountKey
	
	dest.payType = src.PayType
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	listMapping2.call(src.AcctAccessCode,dest.acctAccessCode)
	
	listMapping3.call(src.CustAccessCode,dest.custAccessCode)
	
	listMapping4.call(src.SubGroupAccessCode,dest.groupAccessCode)
	
	listMapping5.call(src.SubAccessCode,dest.subAccessCode)
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.oCode = src.OfferingCode
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->
    
  def oKeyExt = dest.offeringKeyInfo
  
  oKeyExt._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"

	listMapping7.call(src.OfferingKey,oKeyExt)
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	listMapping1.call(src.OfferingOwner,dest.anyAccessCode)
	
	mappingList(src.OfferingInst,dest.rebateDataExList,listMapping6)
	
}

def listMapping9

listMapping9 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping8

listMapping8 = 
{
    src,dest  ->

	dest.interMode = src.AccessMode
	
	def srcMidVar0 = src.AccessSecurity
	
	dest.loginSystem = srcMidVar0.LoginSystemCode
	
	dest.password = srcMidVar0.Password
	
	dest.remoteAddress = srcMidVar0.RemoteIP
	
	mappingList(src.AdditionalProperty,dest.simpleProperty,listMapping9)
	
	dest.businessCode = src.BusinessCode
	
	dest.messageSeq = src.MessageSeq
	
	dest.msgLanguageCode = src.MsgLanguageCode
	
	def srcMidVar1 = src.OperatorInfo
	
	dest.operatorId = srcMidVar1.OperatorID
	
	dest.channelId = srcMidVar1.ChannelID
	
	def srcMidVar2 = src.OwnershipInfo
	
	dest.beId = srcMidVar2.BEID
	
	dest.brId = srcMidVar2.BRID
	
	def srcMidVar3 = src.TimeFormat
	
	dest.timeType = srcMidVar3.TimeType
	
	dest.timeZoneId = srcMidVar3.TimeZoneID
	
	dest.version = src.Version
	
}

def srcMidVar = srcArgs0.QueryRebateRequestMsg

listMapping0.call(srcMidVar.QueryRebateRequest,destArgs1)

listMapping8.call(srcMidVar.RequestHeader,destArgs0)
