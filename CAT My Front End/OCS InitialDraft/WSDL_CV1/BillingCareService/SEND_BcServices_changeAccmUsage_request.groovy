dest.setServiceOperation("SubscriberService","changeAccmUsage")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.changeaccmusage.io.ChangeAccmUsageRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.accoutCode = src.AccountCode
	
	dest.accoutKey = src.AccountKey
	
	dest.payType = src.PayType
	
	dest.primaryIdentity = src.PrimaryIdentity	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->
	
	listMapping0.call(src.CustAccessCode,dest.custAccessCode)
	
	listMapping1.call(src.SubGroupAccessCode,dest.groupAccessCode)
	
	listMapping2.call(src.SubAccessCode,dest.subAccessCode)	
	
	listMapping3.call(src.AcctAccessCode,dest.acctAccessCode)
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.businessCode = src.BusinessCode
	
	def srcMidVar0 = src.OperatorInfo
	
	dest.channelId = srcMidVar0.ChannelID
	
	def srcMidVar1 = src.AccessSecurity
	
	dest.loginSystem = srcMidVar1.LoginSystemCode
	
	dest.messageSeq = src.MessageSeq
	
	dest.msgLanguageCode = src.MsgLanguageCode
	
	dest.operatorId = srcMidVar0.OperatorID
	
	dest.password = srcMidVar1.Password
	
	def destMidVar = dest.simpleProperty[0]
	
	def srcMidVar2 = src.AdditionalProperty[0]
	
	destMidVar.value = srcMidVar2.Value
	
	def srcMidVar3 = src.TimeFormat
	
	dest.timeType = srcMidVar3.TimeType
	
	dest.timeZoneId = srcMidVar3.TimeZoneID
	
	dest.version = src.Version
	
	dest.remoteAddress = srcMidVar1.RemoteIP
	
	mappingList(src.AdditionalProperty,dest.simpleProperty,listMapping5)
	
	def srcMidVar4 = src.OwnershipInfo
	
	dest.beId = srcMidVar4.BEID
	
	dest.brId = srcMidVar4.BRID
	
	dest.interMode = src.AccessMode	
}


def listMapping7

listMapping7 = 
{
   src,dest  ->
   
   dest.operTime = parseDate(src.OperTime, "yyyyMMddHHmmss")
   
   dest.adjustType = src.AdjustType
   
   dest.cumulateValue = src.CumulateValue
   
   srcMidVar6 = src.AccmType
   
   dest.accmTypeID = srcMidVar6.AccmTypeID
   
   dest.accmTypeCode = srcMidVar6.AccmTypeCode
     
}


def srcMidVar = srcArgs0.ChangeAccmUsageRequestMsg.RequestHeader

listMapping6.call(srcMidVar,destArgs0)

def srcMidVar5 = srcArgs0.ChangeAccmUsageRequestMsg.ChangeAccmUsageRequest

listMapping4.call(srcMidVar5.ChangeObj,destArgs1.anyAccessCode);

mappingList(srcMidVar5.AccmChangeInfo,destArgs1.accmChangeInfo,listMapping7)

mappingList(srcMidVar5.AdditionalProperty,destArgs1.simpleProperty,listMapping5)



