def srcResultData = src.payload._return
def destResultData = dest.payload._return
def destResultHeader = destResultData.ManageGrpCallScreenNoResultMsg.ResultHeader

destResultHeader.Version = srcResultData.version
destResultHeader.MsgLanguageCode = srcResultData.msgLanguageCode
destResultHeader.ResultCode = srcResultData.resultCode
destResultHeader.ResultDesc = srcResultData.resultDesc
destResultHeader.MessageSeq = srcResultData.messageSeq

def listMappingAdditionalProperty
listMappingAdditionalProperty = 
{
    src,dest  ->
	dest.Code = src.code
	dest.Value = src.value
}
mappingList(srcResultData.simpleProperty,destResultHeader.AdditionalProperty,listMappingAdditionalProperty)
