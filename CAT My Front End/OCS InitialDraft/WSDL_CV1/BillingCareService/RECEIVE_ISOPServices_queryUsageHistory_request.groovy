def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destMidVar = destArgs0.queryUsageHistory.requestHeader

destMidVar.accessChannel = srcArgs0.requestHeader.accessChannel

destMidVar.beId = srcArgs0.requestHeader.beId

destMidVar.language = srcArgs0.requestHeader.language

destMidVar.operator = srcArgs0.requestHeader.operator

destMidVar.password = srcArgs0.requestHeader.password

destMidVar.transactionId = srcArgs0.requestHeader.transactionId

destArgs0.queryUsageHistory.msisdn = srcArgs0.msisdn

def listMapping0

listMapping0 =
{
    src, dest  ->

        dest.type = src.type

        dest.length = src.length

        dest.startDate = src.startDate

        dest.endDate = src.endDate
}

def destMidVar1 = destArgs0.queryUsageHistory.queryUsageList

destMidVar1.businessType = srcArgs0.queryUsageList.businessType

destMidVar1.usageType = srcArgs0.queryUsageList.usageType

destMidVar1.offeringClass = srcArgs0.queryUsageList.offeringClass

mappingList(srcArgs0.queryUsageList.periodType, destMidVar1.periodType, listMapping0)