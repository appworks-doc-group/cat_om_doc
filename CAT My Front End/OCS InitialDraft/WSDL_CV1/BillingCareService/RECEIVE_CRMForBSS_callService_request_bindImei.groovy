dest.setServiceOperation("CRMForBSS","bindImei")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.Name = src.paraName
	
	dest.Value = src.paraValue
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

        dest.Version = src.version

        dest.BEId = src.beId

        dest.TransactionId = src.transactionId

        dest.SessionId = src.sessionId

        dest.ChannelId = src.channelId

        dest.ProcessTime = src.processTime

        dest.OperatorId = src.operatorId

        dest.OperatorPwd = src.operatorPwd

        dest.Language = src.language

        dest.AccessUser = src.accessUser

        dest.AccessPwd = src.accessPwd

        dest.TestFlag = src.testFlag

        dest.TimeType = src.timeType

        dest.TimeZoneID = src.timeZoneId

        destMidVar0 = dest.ExtParamList

        mappingList(src.extParamList,destMidVar0.ParameterInfo,listMapping4)

}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Name = src.paraName
	
	dest.Value = src.paraValue
	
}

def destMidVar = destArgs0.BindImeiRequest

listMapping0.call(srcArgs0.workOrderHeader,destMidVar.RequestHeader)

def destMidVar1 = destArgs0.BindImeiRequest.BindImei

destMidVar1.SubscriberID = srcArgs0.bindImei.subscriberID

destMidVar1.Msisdn = srcArgs0.bindImei.msisdn

destMidVar1.Imei = srcArgs0.bindImei.imei

mappingList(srcArgs0.bindImei.extParamList,destMidVar1.ExtParamList,listMapping1)
