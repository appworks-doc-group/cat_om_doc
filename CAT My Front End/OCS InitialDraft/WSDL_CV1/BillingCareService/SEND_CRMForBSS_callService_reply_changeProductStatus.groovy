def srcReturn = src.payload._return

def destReturn = dest.payload._return

def srcMidVar = srcReturn.ChangeProductStatusResult.ResponseHeader

destReturn.resultCode = srcMidVar.RetCode

destReturn.resultDesc = srcMidVar.RetMsg

destReturn._class = "com.huawei.ngcbs.bm.framework.reverse.WorkOrderResult"