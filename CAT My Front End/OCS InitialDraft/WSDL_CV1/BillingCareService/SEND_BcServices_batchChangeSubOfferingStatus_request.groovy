dest.setServiceOperation("BMOfferingService","batchChangeSubOfferingStatus")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.offering.batch.changesubofferingstatus.io.BatchChangeSubOfferingStatusRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}


def srcMidVar = srcArgs0.BatchChangeSubOfferingStatusRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar.LoginSystemCode

destArgs0.password = srcMidVar.Password

destArgs0.remoteAddress = srcMidVar.RemoteIP

def srcMidVar0 = srcArgs0.BatchChangeSubOfferingStatusRequestMsg.RequestHeader

mappingList(srcMidVar0.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar0.BusinessCode

destArgs0.messageSeq = srcMidVar0.MessageSeq

destArgs0.msgLanguageCode = srcMidVar0.MsgLanguageCode

def srcMidVar1 = srcArgs0.BatchChangeSubOfferingStatusRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.BatchChangeSubOfferingStatusRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.BatchChangeSubOfferingStatusRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar0.Version

def srcMidVar4 = srcArgs0.BatchChangeSubOfferingStatusRequestMsg.BatchChangeSubOfferingStatusRequest

destArgs1.requestFileName = srcMidVar4.FileName

def destMidVar = destArgs1.changeOfferingStatusInfoEx.changeOfferingStatusInfo

destMidVar.newStatus = srcMidVar4.NewStatus

destMidVar.oldStatus = srcMidVar4.OldStatus

destMidVar.opType = srcMidVar4.OpType


def destMidVar1 = destArgs1.changeOfferingStatusInfoEx.changeOfferingStatusInfo.offeringKey

def srcMidVar5 = srcMidVar4.OfferingKey

destMidVar1.oId = srcMidVar5.OfferingID

destMidVar1.oCode = srcMidVar5.OfferingCode

destMidVar1.pSeq = srcMidVar5.PurchaseSeq

destArgs0.interMode = srcMidVar0.AccessMode
