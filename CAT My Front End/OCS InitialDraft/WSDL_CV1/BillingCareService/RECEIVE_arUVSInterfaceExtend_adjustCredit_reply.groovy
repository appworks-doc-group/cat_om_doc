def srcReturn = src.payload._return

def destReturn = dest.payload._return

def srcResultHeader = srcReturn.resultHeader

def destResultHeader = destReturn.AdjustCreditResponse.AdjustCreditResult.ResultMessage.MessageHeader

destResultHeader.CommandId = srcResultHeader.commandId

destResultHeader.Version = srcResultHeader.version

destResultHeader.TransactionId = srcResultHeader.transactionId

destResultHeader.SequenceId = srcResultHeader.sequenceId

destResultHeader.ResultCode = srcResultHeader.resultCode

destResultHeader.ResultDesc = srcResultHeader.resultDesc






