dest.setServiceOperation("OtherToArServices","queryBalance")

def srcArgs0 = src.payload._args[0]

def srcArgs1 = src.payload._args[1]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destArgs0.QueryBalanceRequestMsg.QueryBalanceRequest.QueryObj.SubAccessCode

def srcMidVar = srcArgs1.subAccessCode

destMidVar.SubscriberKey = srcMidVar.subscriberKey

destMidVar.PrimaryIdentity = srcMidVar.primaryIdentity

def destMidVar0 = destArgs0.QueryBalanceRequestMsg.QueryBalanceRequest

destMidVar0.BalanceType = srcArgs1.balanceType

def destMidVar1 = destArgs0.QueryBalanceRequestMsg.QueryBalanceRequest.QueryObj.AcctAccessCode

def srcMidVar0 = srcArgs1.acctAccessCode

destMidVar1.PrimaryIdentity = srcMidVar0.primaryIdentity

destMidVar1.PayType = srcMidVar0.payType

destMidVar1.AccountKey = srcMidVar0.accoutKey

destMidVar1.AccountCode = srcMidVar0.accoutCode

def destMidVar2 = destArgs0.QueryBalanceRequestMsg.RequestHeader

destMidVar2.Version = srcArgs0.version

def destMidVar3 = destArgs0.QueryBalanceRequestMsg.RequestHeader.TimeFormat

destMidVar3.TimeZoneID = srcArgs0.timeZoneId

destMidVar3.TimeType = srcArgs0.timeType

def destMidVar4 = destArgs0.QueryBalanceRequestMsg.RequestHeader.OwnershipInfo

destMidVar4.BRID = srcArgs0.brId

destMidVar4.BEID = srcArgs0.beId

def destMidVar5 = destArgs0.QueryBalanceRequestMsg.RequestHeader.OperatorInfo

destMidVar5.OperatorID = srcArgs0.operatorId

destMidVar5.ChannelID = srcArgs0.channelId

destMidVar2.MsgLanguageCode = srcArgs0.msgLanguageCode

destMidVar2.MessageSeq = srcArgs0.messageSeq

destMidVar2.BusinessCode = srcArgs0.businessCode

mappingList(srcArgs0.simpleProperty,destMidVar2.AdditionalProperty,listMapping0)

def destMidVar6 = destArgs0.QueryBalanceRequestMsg.RequestHeader.AccessSecurity

destMidVar6.RemoteIP = srcArgs0.remoteAddress

destMidVar6.Password = srcArgs0.password

destMidVar6.LoginSystemCode = srcArgs0.loginSystem

destMidVar2.AccessMode = srcArgs0.interMode
