def srcReturn = src.payload._return

def destReturn = dest.payload._return



def destMidVar = destReturn.QuerySubFamilyNoResultMsg.QuerySubFamilyNoResult

def srcMidVar = srcReturn.resultBody
def listMapping0

listMapping0 =
{
	src,dest  ->

	dest.effectiveDate = src.effectiveDate
	
	dest.expireDate = src.expireDate
	
	dest.FamilyNo = src.familyNo
	
	dest.GroupType = src.groupType
	
}

mappingList(srcMidVar.familyNoInfos,destMidVar.FamilyNoInfo,listMapping0)

def destMidVar0 = destReturn.QuerySubFamilyNoResultMsg.ResultHeader



srcMidVar._class = "com.huawei.ngcbs.cm.ocs12ws.subscriber.querysubfamilyno.io.QuerySubFamilyNoResultOcs12Base"



def srcMidVar0 = srcReturn.resultHeader

srcMidVar0._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12ResultHeader"


destMidVar0.CommandId = srcMidVar0.commandId

destMidVar0.ResultCode = srcMidVar0.resultCode

destMidVar0.ResultDesc = srcMidVar0.resultDesc

destMidVar0.SequenceId = srcMidVar0.sequenceId

destMidVar0.SerialNo = srcMidVar0.serialNo

destMidVar0.TransactionId = srcMidVar0.transactionId

destMidVar0.Version = srcMidVar0.version
