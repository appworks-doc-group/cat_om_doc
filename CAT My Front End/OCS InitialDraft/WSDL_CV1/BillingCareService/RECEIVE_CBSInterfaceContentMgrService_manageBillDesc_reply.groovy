def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->
	
	dest.BillDescCode = src.billDescCode
	
	dest.DescLang1 = src.descLang1
	
	dest.DescLang2 = src.descLang2
	
	dest.DescLang3 = src.descLang3
	
	dest.DescLang4 = src.descLang4
	
	dest.DescLang5 = src.descLang5
	
}

def destMidVar = destReturn.ManageBillDescResultMsg.ManageBillDescResult

mappingList(srcReturn.billDescList,destMidVar.BillDescList,listMapping0)

def srcMidVar0 = srcReturn.resultHeader

srcMidVar0._class = "com.huawei.ngcbs.bm.framework.ws.ResultHeader"

def destMidVar0 = destReturn.ManageBillDescResultMsg.ResultHeader

destMidVar0.CommandId = srcMidVar0.commandId

destMidVar0.ResultCode = srcMidVar0.resultCode

destMidVar0.ResultDesc = srcMidVar0.resultDesc

destMidVar0.SequenceId = srcMidVar0.sequenceId

destMidVar0.TransactionId = srcMidVar0.transactionId

destMidVar0.Version = srcMidVar0.version