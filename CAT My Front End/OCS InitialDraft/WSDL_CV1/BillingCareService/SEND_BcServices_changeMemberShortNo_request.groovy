dest.setServiceOperation("BMGroupService","changeMemberShortNo")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.group.changemembershortno.io.ChangeMemberShortNoRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.ChangeMemberShortNoRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar.LoginSystemCode

destArgs0.password = srcMidVar.Password

destArgs0.remoteAddress = srcMidVar.RemoteIP

def srcMidVar0 = srcArgs0.ChangeMemberShortNoRequestMsg.RequestHeader

mappingList(srcMidVar0.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar0.BusinessCode

destArgs0.messageSeq = srcMidVar0.MessageSeq

destArgs0.msgLanguageCode = srcMidVar0.MsgLanguageCode

def srcMidVar1 = srcArgs0.ChangeMemberShortNoRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.ChangeMemberShortNoRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.ChangeMemberShortNoRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar0.Version

def destMidVar = destArgs1.subAccessCode

def srcMidVar4 = srcArgs0.ChangeMemberShortNoRequestMsg.ChangeMemberShortNoRequest.GroupMember

destMidVar.primaryIdentity = srcMidVar4.PrimaryIdentity

destMidVar.subscriberKey = srcMidVar4.SubscriberKey

def destMidVar0 = destArgs1.changeSNumberInfo

def srcMidVar5 = srcArgs0.ChangeMemberShortNoRequestMsg.ChangeMemberShortNoRequest

destMidVar0.newShortNumber = srcMidVar5.NewMemberShortNo

destMidVar0.oldShortNumber = srcMidVar5.OldMemberShortNo

def destMidVar1 = destArgs1.groupAccessCode

def srcMidVar6 = srcArgs0.ChangeMemberShortNoRequestMsg.ChangeMemberShortNoRequest.SubGroupAccessCode

destMidVar1.groupCode = srcMidVar6.SubGroupCode

destMidVar1.groupKey = srcMidVar6.SubGroupKey

destArgs0.interMode = srcMidVar0.AccessMode
