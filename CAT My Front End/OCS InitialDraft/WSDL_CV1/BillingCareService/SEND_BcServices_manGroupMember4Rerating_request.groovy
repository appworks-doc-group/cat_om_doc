import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("CugRerateService","manGroupMember4Rerating")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ManGroupMember4Rerating.io.ManGroupMember4ReratingRequest"

def listMapping0

listMapping0 =
{
	src,dest  ->

	dest.code = src.Code

	dest.value = src.Value

}

def srcMidVar0 = srcArgs0.ManGroupMember4ReratingRequestMsg.RequestHeader

def srcMidVar1 = srcArgs0.ManGroupMember4ReratingRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar1.LoginSystemCode

destArgs0.password = srcMidVar1.Password

destArgs0.remoteAddress = srcMidVar1.RemoteIP

mappingList(srcMidVar0.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.version = srcMidVar0.Version

destArgs0.businessCode = srcMidVar0.BusinessCode

destArgs0.messageSeq = srcMidVar0.MessageSeq

destArgs0.msgLanguageCode = srcMidVar0.MsgLanguageCode

destArgs0.interMode = srcMidVar0.AccessMode

def srcMidVar2 = srcArgs0.ManGroupMember4ReratingRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar2.ChannelID

destArgs0.operatorId = srcMidVar2.OperatorID

def srcMidVar3 = srcArgs0.ManGroupMember4ReratingRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar3.BEID

destArgs0.brId = srcMidVar3.BRID

def srcMidVar4 = srcArgs0.ManGroupMember4ReratingRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar4.TimeType

destArgs0.timeZoneId = srcMidVar4.TimeZoneID

def srcMidVar5 = srcArgs0.ManGroupMember4ReratingRequestMsg.ManGroupMember4ReratingRequest

def destMidVar2 = destArgs1.groupAccessCode

def srcMidVar6 = srcArgs0.ManGroupMember4ReratingRequestMsg.ManGroupMember4ReratingRequest.SubGroupAccessCode

destMidVar2.groupCode = srcMidVar6.SubGroupCode

destMidVar2.groupKey = srcMidVar6.SubGroupKey

def srcMidVar7 = srcArgs0.ManGroupMember4ReratingRequestMsg.ManGroupMember4ReratingRequest.SubAccessCode

def destMidVar3 = destArgs1.subAccessCode

destMidVar3.primaryIdentity = srcMidVar7.PrimaryIdentity

destMidVar3.subscriberKey = srcMidVar7.SubscriberKey

destArgs1.operationType = srcMidVar5.OperationType

destArgs1.reratingTime = parseDate(srcMidVar5.ReratingTime,Constant4Model.DATE_FORMAT)
