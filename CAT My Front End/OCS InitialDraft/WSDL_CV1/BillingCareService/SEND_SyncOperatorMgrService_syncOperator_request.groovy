dest.setServiceOperation("OCS33SyncOperatorMgrService","syncOperator")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs33ws.syncOperator.io.OCS33SyncOperatorRequest"

def destMidVar0 = destArgs0

destMidVar0._class= "com.huawei.ngcbs.cm.ocs33ws.core.bo.Ocs33MessageHeader"

def srcMidVar0 = srcArgs0.SyncOperatorRequestMsg.RequestHeader

destMidVar0.beId  = srcMidVar0.TenantId

destMidVar0.msgLanguageCode  = srcMidVar0.Language

destMidVar0.commandId = srcMidVar0.CommandId

destMidVar0.version = srcMidVar0.Version

destMidVar0.transactionId = srcMidVar0.TransactionId

destMidVar0.sequenceId = srcMidVar0.SequenceId

destMidVar0.requestType = srcMidVar0.RequestType

destMidVar0.loginSystem = srcMidVar0.SessionEntity.Name

destMidVar0.password = srcMidVar0.SessionEntity.Password

destMidVar0.remoteAddress = srcMidVar0.SessionEntity.RemoteAddress

destMidVar0.interFrom = srcMidVar0.InterFrom

destMidVar0.interMedi = srcMidVar0.InterMedi

destMidVar0.interMode = srcMidVar0.InterMode

destMidVar0.visitArea = srcMidVar0.visitArea

destMidVar0.currentCell = srcMidVar0.currentCell

destMidVar0.additionInfo = srcMidVar0.additionInfo

destMidVar0.thirdPartyId = srcMidVar0.ThirdPartyID

destMidVar0.partnerId = srcMidVar0.PartnerID

destMidVar0.operatorId = srcMidVar0.OperatorID

destMidVar0.tradePartnerId = srcMidVar0.TradePartnerID

destMidVar0.partnerOperId = srcMidVar0.PartnerOperID

destMidVar0.belToAreaId = srcMidVar0.BelToAreaID

destMidVar0.reserve2 = srcMidVar0.Reserve2

destMidVar0.reserve3 = srcMidVar0.Reserve3

destMidVar0.messageSeq = srcMidVar0.SerialNo

destMidVar0.remark = srcMidVar0.Remark

def destMidVar1 = destArgs1.syncOperatorRequestBody

def srcMidVar1 = srcArgs0.SyncOperatorRequestMsg.SyncOperatorRequest

destMidVar1.operType = srcMidVar1.OperType

destMidVar1.operOriginCode = srcMidVar1.OperOriginCode

destMidVar1.operNewCode = srcMidVar1.OperNewCode

destMidVar1.password = srcMidVar1.Password

destMidVar1.adminOperCode = srcMidVar1.AdminOperCode

destMidVar1.effDate = srcMidVar1.EffDate

destMidVar1.expDate = srcMidVar1.ExpDate

destMidVar1.staffCode = srcMidVar1.StaffCode

destMidVar1.staffFirstName = srcMidVar1.StaffFirstName

destMidVar1.staffLastName = srcMidVar1.StaffLastName

destMidVar1.deptID = srcMidVar1.DeptID

destMidVar1.remark = srcMidVar1.Remark

destMidVar1.oldPassword = srcMidVar1.OldPassword

destMidVar1.managementCenterName = srcMidVar1.ManagementCenterName

destMidVar1.managementCenterCode = srcMidVar1.ManagementCenterCode

destMidVar1.commercialAgencyName = srcMidVar1.CommercialAgencyName

destMidVar1.commercialAgencyCode = srcMidVar1.CommercialAgencyCode

destMidVar1.accountManagerName = srcMidVar1.AccountManagerName

destMidVar1.commercialAgencyPhone = srcMidVar1.CommercialAgencyPhone

destMidVar1.cashierContactNumber = srcMidVar1.CashierContactNumber

