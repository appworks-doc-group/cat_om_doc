def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.batch.changesubbasicinfo.io.BatchChangeSubBasicInfoRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}


def srcMidVar = srcArgs0.BatchChangeSubBasicInfoRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar.LoginSystemCode

destArgs0.password = srcMidVar.Password

destArgs0.remoteAddress = srcMidVar.RemoteIP

def srcMidVar0 = srcArgs0.BatchChangeSubBasicInfoRequestMsg.RequestHeader

mappingList(srcMidVar0.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar0.BusinessCode

destArgs0.messageSeq = srcMidVar0.MessageSeq

destArgs0.msgLanguageCode = srcMidVar0.MsgLanguageCode

def srcMidVar1 = srcArgs0.BatchChangeSubBasicInfoRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.BatchChangeSubBasicInfoRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.BatchChangeSubBasicInfoRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar0.Version

def srcMidVar4 = srcArgs0.BatchChangeSubBasicInfoRequestMsg.BatchChangeSubBasicInfoRequest

destArgs1.requestFileName = srcMidVar4.FileName

def destMidVar = destArgs1.changeSubBasicInfo.subscriberInfo

def srcMidVar5 = srcArgs0.BatchChangeSubBasicInfoRequestMsg.BatchChangeSubBasicInfoRequest.SubBasicInfo

destMidVar.dunningFlag = srcMidVar5.DunningFlag

destMidVar.ivrLang = srcMidVar5.IVRLang

destMidVar.subLevel = srcMidVar5.SubLevel

def destMidVar0 = destArgs1.changeSubBasicInfo

mappingList(srcMidVar5.SubProperty,destMidVar0.subProperties,listMapping1)

destMidVar.writtenLang = srcMidVar5.WrittenLang

destArgs0.interMode = srcMidVar0.AccessMode
