import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReqDocMsg = src.payload._args[0]

def destMsgHeader = dest.payload._args[0]
destMsgHeader._class = "com.huawei.ngcbs.cm.ocs33ws.core.bo.Ocs33MessageHeader"

def destReqData = dest.payload._args[1]
destReqData._class = "com.huawei.ngcbs.cm.ocs33ws.subscriber.changeprimaryoffer.io.OCS33ChangePrimaryOfferRequest"

def srcReqHeaderDoc = srcReqDocMsg.ChangePrimaryOfferRequestMsg.RequestHeader

destMsgHeader.commandId = srcReqHeaderDoc.CommandId
destMsgHeader.version = srcReqHeaderDoc.Version
destMsgHeader.transactionId = srcReqHeaderDoc.TransactionId
destMsgHeader.sequenceId = srcReqHeaderDoc.SequenceId
destMsgHeader.requestType = srcReqHeaderDoc.RequestType
destMsgHeader.loginSystem = srcReqHeaderDoc.SessionEntity.Name
destMsgHeader.password = srcReqHeaderDoc.SessionEntity.Password
destMsgHeader.remoteAddress = srcReqHeaderDoc.SessionEntity.RemoteAddress
destMsgHeader.interFrom = srcReqHeaderDoc.InterFrom
destMsgHeader.interMedi = srcReqHeaderDoc.InterMedi
destMsgHeader.interMode = srcReqHeaderDoc.InterMode
destMsgHeader.visitArea = srcReqHeaderDoc.visitArea
destMsgHeader.currentCell = srcReqHeaderDoc.currentCell
destMsgHeader.additionInfo = srcReqHeaderDoc.additionInfo
destMsgHeader.thirdPartyId = srcReqHeaderDoc.ThirdPartyID
destMsgHeader.partnerId = srcReqHeaderDoc.PartnerID
destMsgHeader.operatorId = srcReqHeaderDoc.OperatorID
destMsgHeader.tradePartnerId = srcReqHeaderDoc.TradePartnerID
destMsgHeader.partnerOperId = srcReqHeaderDoc.PartnerOperID
destMsgHeader.belToAreaId = srcReqHeaderDoc.BelToAreaID
destMsgHeader.reserve2 = srcReqHeaderDoc.Reserve2
destMsgHeader.reserve3 = srcReqHeaderDoc.Reserve3
destMsgHeader.messageSeq = srcReqHeaderDoc.SerialNo
destMsgHeader.remark = srcReqHeaderDoc.Remark
destMsgHeader.performanceStatCmd = "ChangePrimaryOffer"

def srcReqDataDoc = srcReqDocMsg.ChangePrimaryOfferRequestMsg.ChangePrimaryOfferRequest
destReqData.subAccessCode.primaryIdentity = srcReqDataDoc.SubscriberNo
destReqData.handlingChargeFlag = srcReqDataDoc.HandleChargeFlag
def listProdPropsMapping = 
{
	src,dest  ->
	
	dest.id = src.Id
	dest.value = src.Value
}

def listNewOptionalOfferMapping = 
{
	src,dest  ->
	
	dest.offerId = src.Id
	mappingList(src.SimpleProperty, dest.prodOrderProps, listProdPropsMapping)
}

def listOldOptionalOfferMapping = 
{
	src,dest  ->
	
	dest.offerId = src.Id
	dest.offerOrderKey = src.OfferOrderKey
}

def listNewPrimaryOfferMapping = 
{
  src,dest  ->

	dest.newPrimaryOfferId = src.Id
	mappingList(src.NewOptionalOffer, dest.newOptionalOffers, listNewOptionalOfferMapping)
	

}

def listOldPrimaryOfferMapping = 
{
  src,dest  ->

	dest.oldPrimaryOfferId = src.Id
	dest.oldPrimaryOfferSeq = src.OfferOrderKey
	mappingList(src.DeletedOptionalOffer, dest.deletedOptionalOffers, listOldOptionalOfferMapping)
	
}

destReqData.validMode.mode = srcReqDataDoc.ValidMode.Mode
destReqData.validMode.effectiveDate = parseDate(srcReqDataDoc.ValidMode.EffectiveDate, Constant4Model.DATE_FORMAT)
destReqData.validMode.expireDate = parseDate(srcReqDataDoc.ValidMode.ExpireDate, Constant4Model.DATE_FORMAT)

mappingList(srcReqDataDoc.NewPrimaryOffer, destReqData.newPrimaryOffers, listNewPrimaryOfferMapping)
mappingList(srcReqDataDoc.OldPrimaryOffer, destReqData.oldPrimaryOffers, listOldPrimaryOfferMapping)
