def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.NewSubscriberResultMsg.NewSubscriberResult

def srcMidVar = srcReturn.resultBody

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.ProductID = src.productId
	dest.ProductOrderKey = src.productOrderKey		
	dest.EffectiveDate = src.effectiveDate	
	dest.ExpireDate = src.expireDate
	dest.AutoType = src.autoType
	
}

mappingList(srcMidVar.prodOrderInfoList,destMidVar.ProductOrderInfo,listMapping0)

def destMidVar0 = destReturn.NewSubscriberResultMsg.ResultHeader

def srcMidVar0 = srcReturn.resultHeader

destMidVar0.CommandId = srcMidVar0.commandId

destMidVar0.ResultCode = srcMidVar0.resultCode

destMidVar0.ResultDesc = srcMidVar0.resultDesc

destMidVar0.SequenceId = srcMidVar0.sequenceId

destMidVar0.Version = srcMidVar0.version

destMidVar0.TransactionId = srcMidVar0.transactionId


