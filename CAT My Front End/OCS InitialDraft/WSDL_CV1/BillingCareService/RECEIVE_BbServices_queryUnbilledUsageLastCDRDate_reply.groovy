def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

		dest.EndTime=formatDate(src.endTime, "yyyy-MM-dd hh:mm:ss")
	
		dest.StartTime=formatDate(src.startTime, "yyyy-MM-dd hh:mm:ss")
	
	def srcMidVar2 = src.usageCategory
	
	dest.UsageCategory = srcMidVar2.usageCategory
	
}

def destMidVar = destReturn.QueryUnbilledUsageLastCDRDateResultMsg.ResultHeader.AdditionalProperty[0]

def srcMidVar = srcReturn.resultHeader.simpleProperty[0]

destMidVar.Value = srcMidVar.value

destMidVar.Code = srcMidVar.code

def destMidVar0 = destReturn.QueryUnbilledUsageLastCDRDateResultMsg.ResultHeader

def srcMidVar0 = srcReturn.resultHeader

destMidVar0.Version = srcMidVar0.version

destMidVar0.ResultDesc = srcMidVar0.resultDesc

destMidVar0.ResultCode = srcMidVar0.resultCode

destMidVar0.MsgLanguageCode = srcMidVar0.msgLanguageCode

destMidVar0.MessageSeq = srcMidVar0.messageSeq

def srcMidVar1 = srcReturn.queryUnbilledUsageLastCDRDateResultInfo

def destMidVar1 = destReturn.QueryUnbilledUsageLastCDRDateResultMsg.QueryUnbilledUsageLastCDRDateResult

mappingList(srcMidVar1.unbilledUsageLastCDRDateInfoList,destMidVar1.UnbilledUsageLastCDRDateInfo,listMapping0)
