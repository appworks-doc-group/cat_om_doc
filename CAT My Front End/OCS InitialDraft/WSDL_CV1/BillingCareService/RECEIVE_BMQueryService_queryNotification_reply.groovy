import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Message = src.message
	
	dest.ReceiverNumber = src.receiverNumber
	
	dest.SenderAddress = src.senderAddress
    
	dest.SendingDate = formatDate(src.sendingDate,Constant4Model.DATE_FORMAT)
	
	dest.Status = src.status
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def srcMidVar = srcReturn.queryNotification

def destMidVar = destReturn.QueryNotificationResultMsg.QueryNotificationResult

mappingList(srcMidVar.notificationList,destMidVar.NotificationList,listMapping0)

def destMidVar0 = destReturn.QueryNotificationResultMsg.ResultHeader

def srcMidVar0 = srcReturn.resultHeader

destMidVar0.MsgLanguageCode = srcMidVar0.msgLanguageCode

destMidVar0.ResultCode = srcMidVar0.resultCode

destMidVar0.ResultDesc = srcMidVar0.resultDesc

destMidVar0.Version = srcMidVar0.version

mappingList(srcMidVar0.simpleProperty,destMidVar0.AdditionalProperty,listMapping1)
