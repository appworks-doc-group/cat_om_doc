def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destReturn.BatchChangeInitCreditLimitResultMsg.ResultHeader

mappingList(srcReturn.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

destMidVar.Version = srcReturn.version

destMidVar.ResultDesc = srcReturn.resultDesc

destMidVar.ResultCode = srcReturn.resultCode

destMidVar.MessageSeq = srcReturn.messageSeq

destMidVar.MsgLanguageCode = srcReturn.msgLanguageCode
