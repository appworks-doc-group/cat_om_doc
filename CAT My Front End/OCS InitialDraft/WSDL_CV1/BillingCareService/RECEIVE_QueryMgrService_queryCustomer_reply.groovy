import com.huawei.ngcbs.bm.common.common.Constant4Model;

def srcReturn = src.payload._return
def destReturn = dest.payload._return

def destReturnHeader = destReturn.QueryCustomerResultMsg.ResultHeader
def srcReturnHeader = srcReturn.resultHeader
destReturnHeader.CommandId = srcReturnHeader.commandId
destReturnHeader.SequenceId = srcReturnHeader.sequenceId
destReturnHeader.Version = srcReturnHeader.version
destReturnHeader.TransactionId = srcReturnHeader.transactionId
destReturnHeader.OperationTime = srcReturnHeader.operationTime
destReturnHeader.OrderId = srcReturnHeader.orderId
destReturnHeader.ResultCode = srcReturnHeader.resultCode
destReturnHeader.ResultDesc = srcReturnHeader.resultDesc

def srcReturnBody = srcReturn
def destReturnBody = destReturn.QueryCustomerResultMsg.QueryCustomerResult
srcReturnBody._class = "com.huawei.ngcbs.cm.ocs33ws.subscriber.querycustomer.io.OCS33QueryCustomerResult"

destReturnBody.Customer.CustomerInfo.CustomerID = srcReturnBody.customer.customerInfo.customerID
destReturnBody.Customer.CustomerInfo.CustomerCode = srcReturnBody.customer.customerInfo.customerCode
destReturnBody.Customer.CustomerInfo.FirstName = srcReturnBody.customer.customerInfo.firstName
destReturnBody.Customer.CustomerInfo.MiddleName = srcReturnBody.customer.customerInfo.middleName
destReturnBody.Customer.CustomerInfo.LastName = srcReturnBody.customer.customerInfo.lastName
destReturnBody.Customer.CustomerInfo.IdType = srcReturnBody.customer.customerInfo.idType
destReturnBody.Customer.CustomerInfo.IdCode = srcReturnBody.customer.customerInfo.idCode
destReturnBody.Customer.CustomerInfo.Gender = srcReturnBody.customer.customerInfo.gender
destReturnBody.Customer.CustomerInfo.Birthday = srcReturnBody.customer.customerInfo.birthday
destReturnBody.Customer.CustomerInfo.Title = srcReturnBody.customer.customerInfo.title
destReturnBody.Customer.CustomerInfo.HomeAddress = srcReturnBody.customer.customerInfo.homeAddress
destReturnBody.Customer.CustomerInfo.Grade = srcReturnBody.customer.customerInfo.grade
destReturnBody.Customer.CustomerInfo.Email = srcReturnBody.customer.customerInfo.email
destReturnBody.Customer.CustomerInfo.SystemNotifyNumber = srcReturnBody.customer.customerInfo.systemNotifyNumber
destReturnBody.Customer.CustomerInfo.HomePhone = srcReturnBody.customer.customerInfo.homePhone
destReturnBody.Customer.CustomerInfo.WorkPhone = srcReturnBody.customer.customerInfo.workPhone


def listSimplePropMapping = 
{
	src,dest  ->
	  dest.Id = src.id
	  dest.Value = src.value
}

mappingList(srcReturnBody.customer.customerInfo.simplePropertyList, destReturnBody.Customer.CustomerInfo.SimpleProperty, listSimplePropMapping)
def listBundleSubMapping = 
{
  src,dest  ->
	
	dest.SubscriberNo = src.subscriberNo
}

def listOfferMapping = 
{
  src,dest  ->

	dest.OfferId = src.offerId
	dest.OfferOrderKey = src.offerOrderKey
	dest.EffectiveDate = src.effectiveDate	
	dest.ExpireDate = src.expireDate
	dest.OfferCode = src.offerCode
	dest.Status = src.status
	mappingList(src.simplePropertyList, dest.SimpleProperty, listSimplePropMapping)
	dest.LevelCode = src.levelCode
	dest.OfferType = src.offerType
	mappingList(src.bundleSubscriberList, dest.BundleSubscriber, listBundleSubMapping)
	dest.PrimaryOfferOrderKey = src.primaryOfferOrderKey
	
}

mappingList(srcReturnBody.customer.offerList, destReturnBody.Customer.Offer, listOfferMapping)

def listProductMapping = 
{
  src,dest  ->

  dest.Id = src.id
	dest.ProductOrderKey = src.productOrderKey
	dest.OfferId = src.offerId
	dest.OfferOrderKey = src.offerOrderKey
	dest.EffectiveTime = formatDate(src.effectiveTime, Constant4Model.DATE_FORMAT)
	dest.ExpireTime = formatDate(src.expireTime, Constant4Model.DATE_FORMAT)
	dest.Status = src.status
	mappingList(src.simpleProperty, dest.SimpleProperty, listSimplePropMapping)
	dest.CurCycleStartTime = src.curCycleStartTime
	dest.CurCycleEndTime = src.curCycleEndTime
	dest.BillStatus = src.billStatus
	
}

def listAccmulatorMapping = 
{
	src,dest  ->
	
	  dest.CumulateID = src.cumulateID
	  dest.CumulateBeginTime = src.cumulateBeginTime
	  dest.CumulateEndTime = src.cumulateEndTime
	  dest.CumulativeAmt = src.cumulativeAmt
}
mappingList(srcReturnBody.customer.accumulatorList.accumulatorList, destReturnBody.Customer.AccumulatorList.Accumulator, listAccmulatorMapping)


def listBalanceMapping = 
{
	src,dest  ->
	  
	  dest.BalanceId = src.balanceId
	  dest.AccountType = src.accountType
	  dest.Amount = src.amount
	  dest.AccountCredit = src.accountCredit
	  dest.ApplyTime = formatDate(src.applyTime, Constant4Model.DATE_FORMAT)
	  dest.ExpireTime = formatDate(src.expireTime, Constant4Model.DATE_FORMAT)
	  dest.RelatedType = src.relatedType
	  dest.RelatedObjectID = src.relatedObjectID
	  dest.SourceObjectType = src.sourceObjectType
	  dest.SourceObjectID = src.sourceObjectID
	  dest.InitialAmount = src.initialAmount
	  dest.Consumption = src.consumption
	  
	  if(src.relatedObjectID != 0)
	  {
	  	if(isNotNull(src.offerId)) {
    		dest.OfferId = src.offerId
		}
	  	if(isNotNull(src.effectiveMode)) {
    		dest.EffectiveMode = src.effectiveMode
		}		
	  	if(isNotNull(src.expireMode)) {
    		dest.ExpireMode = src.expireMode
		}		
	  	if(isNotNull(src.effectiveDate)) {
    		dest.EffectiveDate = src.effectiveDate
		}		
	  	if(isNotNull(src.expireDate)) {
    		dest.ExpireDate = src.expireDate
		}		
	  	if(isNotNull(src.duration)) {
    		dest.Duration = src.duration
		}		
	  	if(isNotNull(src.offerDescription)) {
    		dest.OfferDescription = src.offerDescription
		}		
	  	if(isNotNull(src.offerId)) {
    		dest.OfferId = src.offerId
		}		
	  	if(isNotNull(src.bundleSubscriberNo)) {
    		dest.BundleSubscriberNo = src.bundleSubscriberNo
		}		
	  }
	  
	  dest.RelatedOfferID = src.relatedOfferID
}

def listAccountMapping = 
{
	src,dest ->
	  
	  dest.AccountKey = src.accountKey
	  dest.AccountInfo.AccountID = src.accountInfo.accountID
	  dest.AccountInfo.AccountCode = src.accountInfo.accountCode
	  dest.AccountInfo.FirstName = src.accountInfo.firstName
	  dest.AccountInfo.MiddleName = src.accountInfo.middleName
	  dest.AccountInfo.LastName = src.accountInfo.lastName
	  dest.AccountInfo.Title = src.accountInfo.title
	  dest.AccountInfo.EmailBillAddr = src.accountInfo.emailBillAddr
	  dest.AccountInfo.ContactTel = src.accountInfo.contactTel
	  dest.AccountInfo.BillLang = src.accountInfo.billLang
	  dest.AccountInfo.SMSBillLang = src.accountInfo.sMSBillLang  	  	  	  	  
	  dest.AccountInfo.PaidMode = src.accountInfo.paidMode
	  dest.AccountInfo.IsCustShareAcct = src.accountInfo.isCustShareAcct
	  dest.AccountInfo.CreditCtrlMode = src.accountInfo.creditCtrlMode
	  mappingList(src.accountInfo.simplePropertyList, dest.AccountInfo.SimpleProperty, listSimplePropMapping)
	  dest.BillingCycleDate.BillCycleOpenDate = src.billingCycleDate.billCycleOpenDate
	  dest.BillingCycleDate.BillCycleEndDate = src.billingCycleDate.billCycleEndDate
	  dest.BillingCycleDate.BillCycleType = src.billingCycleDate.billCycleType
	  dest.NewBillingCycleDate.BillCycleOpenDate = src.newBillingCycleDate.billCycleOpenDate
	  dest.NewBillingCycleDate.BillCycleEndDate = src.newBillingCycleDate.billCycleEndDate
	  dest.NewBillingCycleDate.BillCycleType = src.newBillingCycleDate.billCycleType
	  mappingList(src.balanceRecordList.balanceList, dest.BalanceRecordList.Balance, listBalanceMapping)
	  dest.BalanceRecordList.BalanceValidity = src.balanceRecordList.balanceValidity
	  dest.BalanceRecordList.AvailableCredit = src.balanceRecordList.availableCredit
	  dest.BalanceRecordList.BalanceActiveDate = src.balanceRecordList.balanceActiveDate
	  
}

mappingList(srcReturnBody.accountList, destReturnBody.Account, listAccountMapping)


def listUserGroupMapping = 
{
	src,dest  ->
	  
	  dest.GroupID = src.groupID
	  dest.GroupName = src.groupName
}

def listSubscriberMapping = 
{
	src,dest  ->
	  
	  dest.SubscriberNo = src.subscriberNo
	  dest.SubscriberInfo.PrimaryOfferId = src.subscriberInfo.primaryOfferId
	  dest.SubscriberInfo.PrimaryOfferOrderKey = src.subscriberInfo.primaryOfferOrderKey
	  dest.SubscriberInfo.MainProductId = src.subscriberInfo.mainProductId
	  dest.SubscriberInfo.NetworkType = src.subscriberInfo.networkType
	  dest.SubscriberInfo.BrandID = src.subscriberInfo.brandID
	  dest.SubscriberInfo.PaidMode = src.subscriberInfo.paidMode
	  dest.SubscriberInfo.IMSI = src.subscriberInfo.imsi
	  dest.SubscriberInfo.SubscriberCode = src.subscriberInfo.subscriberCode
	  dest.SubscriberInfo.AccountCode = src.subscriberInfo.accountCode
	  mappingList(src.subscriberInfo.simplePropertyList, dest.SubscriberInfo.SimpleProperty, listSimplePropMapping)
	  dest.SubscriberState.FirstActiveDate = src.subscriberState.firstActiveDate
	  dest.SubscriberState.LastRechargeTime = src.subscriberState.lastRechargeTime
	  dest.SubscriberState.ActiveCAC = src.subscriberState.activeCAC
	  dest.SubscriberState.ActiveStop = src.subscriberState.activeStop
	  dest.SubscriberState.SuspendStop = src.subscriberState.suspendStop
	  dest.SubscriberState.DisableStop = src.subscriberState.disableStop
	  dest.SubscriberState.LifeCycleState = src.subscriberState.lifeCycleState
	  dest.SubscriberState.DPFlag = src.subscriberState.dpFlag
	  dest.SubscriberState.FraudState = src.subscriberState.fraudState
	  dest.SubscriberState.LossFlag = src.subscriberState.lossFlag
	  dest.SubscriberState.DPEndDate = src.subscriberState.dpEndDate
	  dest.SubscriberState.DPFlag1 = src.subscriberState.dpFlag1
	  dest.SubscriberState.DPFlag2 = src.subscriberState.dpFlag2
	  dest.SubscriberState.LastActiveDate = src.subscriberState.lastActiveDate
	  dest.SubscriberState.POSUserState = src.subscriberState.posUserState
	  dest.SubscriberState.ETUFraudState = src.subscriberState.etuFraudState
	  mappingList(src.userGroupList.userGroupList, dest.UserGroupList.UserGroup, listUserGroupMapping)
	  mappingList(src.accumulatorList.accumulatorList, dest.AccumulatorList.Accumulator, listAccmulatorMapping)
	  mappingList(src.productList.productList, dest.ProductList.Product, listProductMapping)
	  dest.SubAttachedInfo.ChgMainProductTimes = src.subAttachedInfo.chgMainProductTimes
	  dest.SubAttachedInfo.ChgMainPackageTimes = src.subAttachedInfo.chgMainPackageTimes
	  dest.SubAttachedInfo.LoanAmout = src.subAttachedInfo.loanAmount
	  dest.SubAttachedInfo.LoanPoundage = src.subAttachedInfo.loanPoundage
	  dest.SubAttachedInfo.ETUReceiveAmt = src.subAttachedInfo.ETUReceiveAmt
	  dest.SubAttachedInfo.ETUGracePeriod = src.subAttachedInfo.ETUGracePeriod
	  dest.SubAttachedInfo.UnpaidFee = src.subAttachedInfo.unpaidFee
	  dest.SubAttachedInfo.ETUReceivedTime = src.subAttachedInfo.ETUReceivedTime
	  mappingList(src.consumeAccumList.accumulatorList, dest.ConsumeAccumuList.Accumulator, listAccmulatorMapping)
	  
}

mappingList(srcReturnBody.subscriberList, destReturnBody.Subscriber, listSubscriberMapping)




