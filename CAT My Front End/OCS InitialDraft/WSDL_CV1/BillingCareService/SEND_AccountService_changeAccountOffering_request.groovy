import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.account.changeaccountoffering.io.ChangeAcctOfferingRequest"

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.businessCode = src.BusinessCode
	
	dest.messageSeq = src.MessageSeq
	
	dest.msgLanguageCode = src.MsgLanguageCode
	
	dest.version = src.Version
	
	def srcMidVar0 = src.AccessSecurity
	
	dest.loginSystem = srcMidVar0.LoginSystemCode
	
	dest.password = srcMidVar0.Password
	
	dest.remoteAddress = srcMidVar0.RemoteIP
	
	mappingList(src.AdditionalProperty,dest.simpleProperty,listMapping1)
	
	def srcMidVar1 = src.OperatorInfo
	
	dest.channelId = srcMidVar1.ChannelID
	
	dest.operatorId = srcMidVar1.OperatorID
	
	def srcMidVar2 = src.OwnershipInfo
	
	dest.beId = srcMidVar2.BEID
	
	dest.brId = srcMidVar2.BRID
	
	def srcMidVar3 = src.TimeFormat
	
	dest.timeType = srcMidVar3.TimeType
	
	dest.timeZoneId = srcMidVar3.TimeZoneID
	
	dest.interMode = src.AccessMode
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.accoutCode = src.AccountCode
	
	dest.accoutKey = src.AccountKey
	
	dest.payType = src.PayType
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping11

listMapping11 = 
{
    src,dest  ->

	dest.code = src.SubPropCode
	
	dest.value = src.Value
	
}

def listMapping10

listMapping10 = 
{
    src,dest  ->

		dest.effDate=parseDate(src.EffectiveTime, "yyyyMMddHHmmss")
	
		dest.expDate=parseDate(src.ExpirationTime, "yyyyMMddHHmmss")
	
	def destMidVar1 = dest.property
	
	destMidVar1.propCode = src.PropCode
	
	destMidVar1.propType = src.PropType
	
	destMidVar1.value = src.Value
	
	mappingList(src.SubPropInst,destMidVar1.subProps,listMapping11)
	
	destMidVar1.complexFlag = src.PropType
	
}

def listMapping9

listMapping9 = 
{
    src,dest  ->

	def destMidVar0 = dest.productInst
	
	destMidVar0.packageFlag = src.PackageFlag
	
	destMidVar0.parentProdId = src.ParentProdID
	
	destMidVar0.primaryFlag = src.PrimaryFlag
	
	destMidVar0.productType = src.ProductType
	
	mappingList(src.PInstProperty,dest.properties,listMapping10)
	
	destMidVar0.prodId = src.ProductID
	
	destMidVar0.networkType = src.NetworkType
	
}

def listMapping13

listMapping13 = 
{
    src,dest  ->

	dest.code = src.SubPropCode
	
	dest.value = src.Value
	
}

def listMapping12

listMapping12 = 
{
    src,dest  ->

		dest.effDate=parseDate(src.EffectiveTime, "yyyyMMddHHmmss")
	
		dest.expDate=parseDate(src.ExpirationTime, "yyyyMMddHHmmss")
	
	def destMidVar2 = dest.property
	
	destMidVar2.propCode = src.PropCode
	
	destMidVar2.value = src.Value
	
	mappingList(src.SubPropInst,destMidVar2.subProps,listMapping13)
	
	destMidVar2.complexFlag = src.PropType
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	def srcMidVar4 = src.EffectiveTime
	
	dest.effMode = srcMidVar4.Mode
	
		dest.effDate=parseDate(srcMidVar4.Time, "yyyyMMddHHmmss")
	
		dest.expDate=parseDate(src.ExpirationTime, "yyyyMMddHHmmss")
	
	def destMidVar = dest.offeringInst
	
	destMidVar.primaryFlag = "S"
	
	destMidVar.bundleFlag = src.BundledFlag
	
	listMapping5.call(src.OfferingKey,destMidVar.offeringKey)
	
	destMidVar.status = src.Status
	
		destMidVar.trialEndTime=parseDate(src.TrialEndTime, "yyyyMMddHHmmss")
	
		destMidVar.trialStartTime=parseDate(src.TrialStartTime, "yyyyMMddHHmmss")
	
	listMapping6.call(src.ParentOfferingKey,destMidVar.parentOfferingKey)
	
	listMapping7.call(src.RelGOfferingKey,destMidVar.relGOfferingKey)
	
	mappingList(src.ProductInst,dest.productInsts,listMapping9)
	
	mappingList(src.OInstProperty,dest.properties,listMapping12)
	
		destMidVar.activeTime=parseDate(src.ActivationTime.ActiveTime, "yyyyMMddHHmmss")
	
		destMidVar.activeTimeLimit=parseDate(src.ActivationTime.ActiveTimeLimit, "yyyyMMddHHmmss")
	
	def srcMidVar5 = src.ActivationTime
	
	destMidVar.activeMode = srcMidVar5.Mode
	
	destMidVar.offeringClass = src.OfferingClass
	
}

def listMapping15

listMapping15 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping14

listMapping14 = 
{
    src,dest  ->

		dest.effDate=parseDate(src.NewEffectiveTime, "yyyyMMddHHmmss")
	
	def srcMidVar6 = src.NewExpirationTime
	
	dest.expMode = srcMidVar6.Mode
	
		dest.expDate=parseDate(srcMidVar6.Time, "yyyyMMddHHmmss")
	
	listMapping15.call(src.OfferingKey,dest.offeringKey)
	
}

def listMapping17

listMapping17 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping16

listMapping16 = 
{
    src,dest  ->

	listMapping17.call(src.OfferingKey,dest.offeringKey)
	
	dest.expMode = "I"
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	listMapping3.call(src.AcctAccessCode,dest.acctAccessCode)
	
	mappingList(src.AddOffering,dest.addOfferingInstInfo,listMapping4)
	
	mappingList(src.ModifyOffering,dest.modifyOfferingInstInfo,listMapping14)
	
	mappingList(src.DelOffering,dest.delOfferingInstInfo,listMapping16)
	
}

def srcMidVar = srcArgs0.ChangeAccountOfferingRequestMsg

listMapping0.call(srcMidVar.RequestHeader,destArgs0)

listMapping2.call(srcMidVar.ChangeAccountOfferingRequest,destArgs1)
