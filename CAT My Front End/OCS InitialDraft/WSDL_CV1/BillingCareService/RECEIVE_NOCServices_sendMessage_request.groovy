def srcArgs0 = src.payload._args[0]
def destArgs0 = dest.payload._args[0]

destArgs0.SendMessageRequest.messageName = srcArgs0.messageName
destArgs0.SendMessageRequest.msisdn = srcArgs0.msisdn
destArgs0.SendMessageRequest.email = srcArgs0.email

def destMidVar = destArgs0.SendMessageRequest.meta

destMidVar.client = srcArgs0.meta.client
destMidVar.testmode = srcArgs0.meta.testmode
destMidVar.testmsisdn = srcArgs0.meta.testmsisdn
destMidVar.testemail = srcArgs0.meta.testemail
destMidVar.deferred = srcArgs0.meta.deferred
destMidVar.priority = srcArgs0.meta.priority
destMidVar.expirationdate = srcArgs0.meta.expirationdate
destMidVar.appname = srcArgs0.meta.appname

def srcSms = srcArgs0.meta.sms
destMidVar.sms.from = srcSms.from
destMidVar.sms.validity = srcSms.validity
destMidVar.sms.'dlr-mask' = srcSms.dlrMask
destMidVar.sms.'dlr-url' = srcSms.dlrUrl

def createChannel = { localdest ->

    return { val,i  ->
        localdest.putAt(i,val)
    }

}
forEachList(srcArgs0.meta.channel, createChannel(destMidVar.channel))

destMidVar.transactionId = srcArgs0.meta.transactionId
destMidVar.language = srcArgs0.meta.language
destMidVar.curfewBehavior = srcArgs0.meta.curfewBehavior

def listMapping1

def createListValue(srcListValue , destList, listMapping1)
{
    def createStringValue = { localdest ->
        return { val,i  ->
            localdest[i] = val.stringValue
        }
    }
    forEachList(srcListValue, createStringValue(destList.string))

    def createIntValue = { localdest ->
        return { val,i  ->
            localdest[i] = val.intValue
        }
    }
    forEachList(srcListValue, createIntValue(destList.int))

    def createLongValue = { localdest ->
        return { val,i  ->
            localdest[i] = val.longValue
        }
    }
    forEachList(srcListValue, createLongValue(destList.long))

    def createBooleanValue = { localdest ->
        return { val,i  ->
            localdest[i] = val.booleanValue
        }
    }
    forEachList(srcListValue.listValue, createBooleanValue(destList.boolean))

    def createDecimalValue = { localdest ->
        return { val,i  ->
            localdest[i] = val.decimalValue
        }
    }
    forEachList(srcListValue, createDecimalValue(destList.decimal))

    def createDateValue = { localdest ->
        return { val,i  ->
            localdest[i] = val.dateValue
        }
    }
    forEachList(srcListValue, createDateValue(destList.date))

    def createDateTimeValue = { localdest ->
        return { val,i  ->
            localdest[i] = val.datetimeValue
        }
    }
    forEachList(srcListValue, createDateTimeValue(destList.datetime))

    def createListValue = { localdest ->
        return { val,i  ->
            createListValue(val.listValue, localdest[i], listMapping1)
        }
    }
    forEachList(srcListValue.listValue, createListValue(destList.list))

    def createMapValue = { localdest ->
        return { val,i  ->
            mappingList(val.mapValue.mapentry, localdest[i].mapentry,listMapping1)
        }
    }
    forEachList(srcListValue, createMapValue(destList.map))
}

listMapping1 =
{
    src,dest  ->
        dest.key = src.keyString

        dest.string = src.listType.stringValue
        dest.int = src.listType.intValue
        dest.long = src.listType.longValue
        dest.boolean = src.listType.booleanValue
        dest.decimal = src.listType.decimalValue
        dest.date = src.listType.dateValue
        dest.datetime = src.listType.datetimeValue

        createListValue(src.listType.listValue, dest.list, listMapping1)

        def srcMiddle = src.listType.mapValue
        mappingList(srcMiddle.mapentry,dest.map.mapentry,listMapping1)

}
def destMidVar1 = destArgs0.SendMessageRequest.data
mappingList(srcArgs0.mapType.mapentry,destMidVar1.mapentry,listMapping1)