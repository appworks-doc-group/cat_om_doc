import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destReturn.ChangeSubValidityResultMsg.ResultHeader

def srcMidVar0 = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar0.msgLanguageCode

destMidVar.ResultCode = srcMidVar0.resultCode

destMidVar.ResultDesc = srcMidVar0.resultDesc

destMidVar.Version = srcMidVar0.version

destMidVar.MessageSeq = srcMidVar0.messageSeq

mappingList(srcMidVar0.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def destMidVar1 = destReturn.ChangeSubValidityResultMsg.ChangeSubValidityResult.LifeCycleChgInfo

def srcMidVar1 = srcReturn.changeSubValidityResultInfo

destMidVar1.CurrentLifeCycleIndex=srcMidVar1.currentLifeCycleIndex

destMidVar1.ChgValidity=srcMidVar1.chgValidity

listMapping1 = 
{
    src,dest  ->

	dest.StatusName = src.statusName
	
	dest.StatusExpireTime = formatDate(src.statusExpireTime, Constant4Model.DATE_FORMAT)
	
	dest.StatusIndex = src.statusIndex
	
}

mappingList(srcMidVar1.newLifeCycleStatusList,destMidVar1.NewLifeCycleStatus,listMapping1)

def destMidVar2 = destReturn.ChangeSubValidityResultMsg.ChangeSubValidityResult
mappingList(srcMidVar1.simplePropertyList,destMidVar2.AdditionalProperty,listMapping0)
