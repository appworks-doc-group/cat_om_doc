dest.setServiceOperation("arUVSInterfaceService", "queryPricePlan")

def srcReqDocMsg = src.payload._args[0]
def destArgs0 = dest.payload._args[0]
def destArgs1 = dest.payload._args[1]

destArgs0._class = "com.huawei.ngcbs.cm.ocs11ws.core.bo.Ocs11MessageHeader"
destArgs1._class = "com.huawei.ngcbs.cm.ocs11ws.product.querypriceplan.io.QueryPricePlanRequest"

def srcReqHeader = srcReqDocMsg.queryPricePlan.QueryPricePlanRequest.RequestMessage.MessageHeader
def srcReqBody = srcReqDocMsg.queryPricePlan.QueryPricePlanRequest.RequestMessage.MessageBody
def srcSessionEntity = srcReqDocMsg.queryPricePlan.SessionEntity

destArgs0.commandId = srcReqHeader.CommandId
destArgs0.version = srcReqHeader.Version
destArgs0.transactionId = srcReqHeader.TransactionId
destArgs0.sequenceId = srcReqHeader.SequenceId
destArgs0.requestType = srcReqHeader.RequestType

destArgs0.loginSystem = srcSessionEntity.userID
destArgs0.password = srcSessionEntity.password
destArgs0.remoteAddress = srcSessionEntity.remoteAddr

def destSessionEntity = destArgs0.sessionEntity

destSessionEntity.userID = srcSessionEntity.userID
destSessionEntity.password = srcSessionEntity.password
destSessionEntity.remoteAddr = srcSessionEntity.remoteAddr
destSessionEntity.locale = srcSessionEntity.locale
destSessionEntity.loginVia = srcSessionEntity.loginVia
destSessionEntity.uploadRoot = srcSessionEntity.uploadRoot

destArgs1.pricePlanName = srcReqBody.PricePlanName
destArgs1.pricePlanType = srcReqBody.PricePlanType
