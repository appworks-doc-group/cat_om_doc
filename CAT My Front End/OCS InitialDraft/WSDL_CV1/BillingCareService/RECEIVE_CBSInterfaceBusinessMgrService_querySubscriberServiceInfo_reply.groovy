def srcReturn = src.payload._return

def destReturn = dest.payload._return

def srcMidVar = srcReturn.resultBody					

def destMidVar = destReturn.QuerySubscriberServiceInfoResultMsg.QuerySubscriberServiceInfoResult.Service

destMidVar.Id = srcMidVar.id

destMidVar.Status = srcMidVar.status

destMidVar.RegistrationTime = formatDate(srcMidVar.registrationTime, "yyyyMMddHHmmss")

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Id = src.code
	
	dest.Value = src.value
	
}

mappingList(srcMidVar.simplePropertyList,destMidVar.SimpleProperty,listMapping1)				 

def srcMidVar0 = srcReturn.resultHeader

def destMidVar0 = destReturn.QuerySubscriberServiceInfoResultMsg.ResultHeader

destMidVar0.CommandId = srcMidVar0.commandId

destMidVar0.ResultCode = srcMidVar0.resultCode

destMidVar0.ResultDesc = srcMidVar0.resultDesc

destMidVar0.SequenceId = srcMidVar0.sequenceId

destMidVar0.TenantId = srcMidVar0.beId

destMidVar0.Language = srcMidVar0.msgLanguageCode

destMidVar0.TransactionId = srcMidVar0.transactionId

destMidVar0.Version = srcMidVar0.version
