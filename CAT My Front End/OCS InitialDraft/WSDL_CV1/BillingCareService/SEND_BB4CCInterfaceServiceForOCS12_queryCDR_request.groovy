import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("OCS12BB4CCInterfaceService", "queryCDR")

def srcArgs0 = src.payload._args[0]
def srcMidVar0 = srcArgs0.QueryCDRRequestMsg.RequestHeader
def srcMidVar2 = srcArgs0.QueryCDRRequestMsg.QueryCDRRequest.QueryCDRValue

def destArgs0 = dest.payload._args[0]
def destArgs1 = dest.payload._args[1]

destArgs0._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.BB4CCMessageHeader"
destArgs1._class = "com.huawei.ngcbs.cm.ocs12ws.querycdr.io.OCS12QueryCDRRequest"


def listMapping0
listMapping0 = 
{
    src,dest  ->
    
	dest.name = src.name
	
	dest.value = src.value
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

 dest.serviceType = src
 
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

 dest.serviceCatagory = src
 
}

destArgs0.messageSeq = srcMidVar0.SerialNo
destArgs0.serialNo = srcMidVar0.SerialNo
destArgs0.requestFrom = srcMidVar0.RequestFrom
destArgs0.operatorId = srcMidVar0.OperatorId
destArgs0.departmentId = srcMidVar0.DepartmentId
destArgs0.version = srcMidVar0.Version
destArgs0.remark = srcMidVar0.Remark
destArgs0.beId = srcMidVar0.TenantId
destArgs0.msgLanguageCode = srcMidVar0.Language
destArgs0.password = srcMidVar0.Password
destArgs0.loginSystem = srcMidVar0.OperatorId
mappingList(srcMidVar0.Param, destArgs0.param, listMapping0)

destArgs1.subAccessCode.subscriberKey = srcMidVar2.SubId
destArgs1.subAccessCode.primaryIdentity = srcMidVar2.MDN
destArgs1.serviceType = srcMidVar2.ServiceType
destArgs1.serviceCategory = srcMidVar2.ServiceCategory
destArgs1.startTime = parseDate(srcMidVar2.StartTime, Constant4Model.DATE_FORMAT)
destArgs1.endTime = parseDate(srcMidVar2.EndTime, Constant4Model.DATE_FORMAT)
destArgs1.totalCDRNum = srcMidVar2.TotalCDRNum
destArgs1.beginRowNum = srcMidVar2.BeginRowNum
destArgs1.fetchRowNum = srcMidVar2.FetchRowNum
destArgs1.invoiceNo = srcMidVar2.InvoiceNo
destArgs1.billCycleId = srcMidVar2.BillCycleId
destArgs1.billCycleType = srcMidVar2.BillCycleType
destArgs1.cellId = srcMidVar2.CellId
destArgs1.rawInfoFlag = srcMidVar2.RawInfoFlag

mappingList(srcMidVar2.ServiceTypeList.ServiceType,destArgs1.serviceTypeList,listMapping1)
mappingList(srcMidVar2.ServiceCategoryList.ServiceCategory,destArgs1.serviceCategoryList,listMapping2)

