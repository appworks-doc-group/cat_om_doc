dest.setServiceOperation("WoServices","workOrder")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping8

listMapping8 = 
{
    src,dest  ->

	dest.Value = src.complexParamValue
	
}

def listMapping9

listMapping9 = 
{
    src,dest  ->

	dest.Value = src.paramValue
	
	dest.Code = src.paramCode
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.Value = src.complexParamValue
	
	mappingList(src.complexParamList,dest.ObjParam,listMapping8)
	
	mappingList(src.paramList,dest.Param,listMapping9)
	
}

def listMapping10

listMapping10 = 
{
    src,dest  ->

	dest.Value = src.complexParamValue
	
}

def listMapping11

listMapping11 = 
{
    src,dest  ->

	dest.Code = src.paramCode
	
	dest.Value = src.paramValue
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.Code = src.paramCode
	
	dest.Value = src.paramValue
	
	mappingList(src.complexParamList,dest.ObjParam,listMapping10)
	
	mappingList(src.paramList,dest.Param,listMapping11)
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.Value = src.complexParamValue
	
	mappingList(src.complexParamList,dest.ObjParam,listMapping6)
	
	mappingList(src.paramList,dest.Param,listMapping7)
	
}

def listMapping15

listMapping15 = 
{
    src,dest  ->

	dest.Value = src.complexParamValue
	
}

def listMapping16

listMapping16 = 
{
    src,dest  ->

	dest.Code = src.paramCode
	
	dest.Value = src.paramValue
	
}

def listMapping13

listMapping13 = 
{
    src,dest  ->

	dest.Value = src.complexParamValue
	
	mappingList(src.complexParamList,dest.ObjParam,listMapping15)
	
	mappingList(src.paramList,dest.Param,listMapping16)
	
}

def listMapping14

listMapping14 = 
{
    src,dest  ->

	
}

def listMapping12

listMapping12 = 
{
    src,dest  ->

	mappingList(src.complexParamList,dest.ObjParam,listMapping13)
	
	dest.Code = src.paramCode
	
	mappingList(src.paramList,dest.Param,listMapping14)
	
	dest.Value = src.paramValue
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.Value = src.complexParamValue
	
	mappingList(src.complexParamList,dest.ObjParam,listMapping5)
	
	mappingList(src.paramList,dest.Param,listMapping12)
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.Value = src.complexParamValue
	
	mappingList(src.complexParamList,dest.ObjParam,listMapping3)
	
	mappingList(src.paramList,dest.Param,listMapping4)
	
}

def listMapping22

listMapping22 = 
{
    src,dest  ->

	dest.Value = src.complexParamValue
	
}

def listMapping23

listMapping23 = 
{
    src,dest  ->

	dest.Code = src.paramCode
	
	dest.Value = src.paramValue
	
}

def listMapping20

listMapping20 = 
{
    src,dest  ->

	mappingList(src.complexParamList,dest.ObjParam,listMapping22)
	
	dest.Value = src.complexParamValue
	
	mappingList(src.paramList,dest.Param,listMapping23)
	
}

def listMapping24

listMapping24 = 
{
    src,dest  ->

	dest.Value = src.complexParamValue
	
}

def listMapping25

listMapping25 = 
{
    src,dest  ->

	dest.Code = src.paramCode
	
	dest.Value = src.paramValue
	
}

def listMapping21

listMapping21 = 
{
    src,dest  ->

	dest.Code = src.paramCode
	
	dest.Value = src.paramValue
	
	mappingList(src.complexParamList,dest.ObjParam,listMapping24)
	
	mappingList(src.paramList,dest.Param,listMapping25)
	
}

def listMapping18

listMapping18 = 
{
    src,dest  ->

	dest.Value = src.complexParamValue
	
	mappingList(src.complexParamList,dest.ObjParam,listMapping20)
	
	mappingList(src.paramList,dest.Param,listMapping21)
	
}

def listMapping19

listMapping19 = 
{
    src,dest  ->

	
}

def listMapping17

listMapping17 = 
{
    src,dest  ->

	dest.Code = src.paramCode
	
	dest.Value = src.paramValue
	
	mappingList(src.complexParamList,dest.ObjParam,listMapping18)
	
	mappingList(src.paramList,dest.Param,listMapping19)
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Value = src.paramValue
	
	dest.Code = src.paramCode
	
	mappingList(src.complexParamList,dest.ObjParam,listMapping2)
	
	mappingList(src.paramList,dest.Param,listMapping17)
	
}

def destMidVar = destArgs0.WorkOrderRequestMsg.RequestHeader

def srcMidVar = srcArgs0.messageHeader

destMidVar.AccessMode = srcMidVar.interMode

def destMidVar0 = destArgs0.WorkOrderRequestMsg.RequestHeader.AccessSecurity

destMidVar0.LoginSystemCode = srcMidVar.loginSystem

destMidVar0.Password = srcMidVar.password

destMidVar0.RemoteIP = srcMidVar.remoteAddress

mappingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

destMidVar.BusinessCode = srcMidVar.businessCode

destMidVar.MessageSeq = srcMidVar.messageSeq

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

def destMidVar1 = destArgs0.WorkOrderRequestMsg.RequestHeader.OperatorInfo

destMidVar1.ChannelID = srcMidVar.channelId

destMidVar1.OperatorID = srcMidVar.operatorId

def destMidVar2 = destArgs0.WorkOrderRequestMsg.RequestHeader.OwnershipInfo

destMidVar2.BEID = srcMidVar.beId

destMidVar2.BRID = srcMidVar.brId

def destMidVar3 = destArgs0.WorkOrderRequestMsg.RequestHeader.TimeFormat

destMidVar3.TimeType = srcMidVar.timeType

destMidVar3.TimeZoneID = srcMidVar.timeZoneId

destMidVar.Version = srcMidVar.version

def destMidVar4 = destArgs0.WorkOrderRequestMsg.WorkOrderRequest

def srcMidVar0 = srcArgs0.workOrderBody

destMidVar4.OrderObjCode = srcMidVar0.orderObjCode

destMidVar4.OrderObjKey = srcMidVar0.orderObjKey

destMidVar4.OrderObjType = srcMidVar0.orderObjType

destMidVar4.WorkOrderType = srcMidVar0.workOrderType

mappingList(srcMidVar0.orderParam,destMidVar4.OrderParam,listMapping1)
