dest.setServiceOperation("SubscriberService","adjustFreeUnit")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.adjustfreeunit.io.AdjustFreeUnitRequest"

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	def srcMidVar0 = src.AccessSecurity
	
	dest.loginSystem = srcMidVar0.LoginSystemCode
	
	dest.password = srcMidVar0.Password
	
	dest.remoteAddress = srcMidVar0.RemoteIP
	
	mappingList(src.AdditionalProperty,dest.simpleProperty,listMapping1)
	
	dest.businessCode = src.BusinessCode
	
	dest.messageSeq = src.MessageSeq
	
	dest.msgLanguageCode = src.MsgLanguageCode
	
	def srcMidVar1 = src.OperatorInfo
	
	dest.channelId = srcMidVar1.ChannelID
	
	dest.operatorId = srcMidVar1.OperatorID
	
	def srcMidVar2 = src.OwnershipInfo
	
	dest.beId = srcMidVar2.BEID
	
	dest.brId = srcMidVar2.BRID
	
	def srcMidVar3 = src.TimeFormat
	
	dest.timeType = srcMidVar3.TimeType
	
	dest.timeZoneId = srcMidVar3.TimeZoneID
	
	dest.version = src.Version
	
	dest.interMode = src.AccessMode
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.adjustmentAmt = src.AdjustmentAmt
	
	dest.adjustmentType = src.AdjustmentType
	
	dest.freeUnitInstanceId = src.FreeUnitInstanceID
	
	dest.freeUnitType = src.FreeUnitType
	
}


def srcMidVar = srcArgs0.AdjustFreeUnitRequestMsg

listMapping0.call(srcMidVar.RequestHeader,destArgs0)

def srcMidVar4 = srcArgs0.AdjustFreeUnitRequestMsg.AdjustFreeUnitRequest.AdjustmentObj

listMapping2.call(srcMidVar4.SubAccessCode,destArgs1.subAccessCode)

listMapping3.call(srcMidVar4.SubGroupAccessCode,destArgs1.groupAccessCode)

listMapping4.call(srcMidVar4.CustAccessCode,destArgs1.custAccessCode)

def destMidVar = destArgs1.adjustFreeUnitInfoCm

def srcMidVar5 = srcArgs0.AdjustFreeUnitRequestMsg.AdjustFreeUnitRequest

destMidVar.opType = srcMidVar5.OpType

destMidVar.adjustmentReasonCode = srcMidVar5.AdjustmentReasonCode

def destMidVar0 = destArgs1.adjustFreeUnitInfoCm.additionalProperty

mappingList(srcMidVar5.AdditionalProperty,destMidVar0.simplePropertyList,listMapping5)

mappingList(srcMidVar5.AdjustmentInfo,destMidVar.adjustmentInfoList,listMapping6)
