def srcReturn = src.payload._return

def destReturn = dest.payload._return

def srcMidVar = srcReturn.NotifyBulkLoadResult.ResponseHeader

destReturn.resultCode = srcMidVar.RetCode

destReturn.resultDesc = srcMidVar.RetMsg

destReturn._class = "com.huawei.ngcbs.bm.framework.batch.io.BulkLoadResultMessage"
