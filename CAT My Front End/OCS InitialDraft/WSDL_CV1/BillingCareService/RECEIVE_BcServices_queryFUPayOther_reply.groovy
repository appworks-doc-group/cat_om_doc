import com.huawei.ngcbs.bm.common.common.Constant4Model;

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.FreeUnitID = src.freeUnitID
	
	dest.FreeUnitType = src.freeUnitType
	
	dest.FreeUnitTypeName = src.freeUnitTypeName
	
	dest.MeasureUnit = src.measureUnit
	
	dest.MeasureUnitName = src.measureUnitName
	
	dest.PayObjType = src.payObjType
	
	dest.PayObjKey = src.payObjKey
	
	dest.PayObjCode = src.payObjCode
	
	dest.PayAmount = src.payAmount
	
	dest.CreateTime = formatDate(src.createTime,Constant4Model.DATE_FORMAT)
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.TotalNum = src.totalNum
	
	dest.BeginRowNum = src.beginRowNum
	
	dest.FetchRowNum = src.fetchRowNum
	
	mappingList(src.fuPayOtherItemList,dest.FUPayOtherItem,listMapping2)
	
}

def destMidVar = destReturn.QueryFUPayOtherResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.Version = srcMidVar.version

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.MessageSeq = srcMidVar.messageSeq

addingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def destMidVar1 = destReturn.QueryFUPayOtherResultMsg.QueryFUPayOtherResult

def srcMidVar1 = srcReturn.queryFUPayOtherResultInfo

listMapping1.call(srcMidVar1,destMidVar1)