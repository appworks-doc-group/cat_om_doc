def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs33ws.core.bo.Ocs33MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs33ws.community.addgrpmember.io.OCS33addGrpMemberRequest"

def srcMidVar = srcArgs0.AddGrpMemberRequestMsg.RequestHeader

destArgs0.commandId = srcMidVar.CommandId

destArgs0.currentCell = srcMidVar.currentCell

destArgs0.interFrom = srcMidVar.InterFrom

destArgs0.interMedi = srcMidVar.InterMedi

destArgs0.interMode = srcMidVar.InterMode

destArgs0.operatorId = srcMidVar.OperatorID

destArgs0.partnerId = srcMidVar.PartnerID

destArgs0.partnerOperId = srcMidVar.PartnerOperID

destArgs0.remark = srcMidVar.Remark

destArgs0.requestType = srcMidVar.RequestType

destArgs0.reserve2 = srcMidVar.Reserve2

destArgs0.reserve3 = srcMidVar.Reserve3

destArgs0.sequenceId = srcMidVar.SequenceId

destArgs0.messageSeq = srcMidVar.SerialNo

destArgs0.thirdPartyId = srcMidVar.ThirdPartyID

destArgs0.tradePartnerId = srcMidVar.TradePartnerID

destArgs0.version = srcMidVar.Version

destArgs0.transactionId = srcMidVar.TransactionId

destArgs0.visitArea = srcMidVar.visitArea

def srcMidVar0 = srcArgs0.AddGrpMemberRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar0.Name

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteAddress

destArgs0.performanceStatCmd = "AddGrpMember"

def srcMidVar1 = srcArgs0.AddGrpMemberRequestMsg.AddGrpMemberRequest

destArgs1.groupNumber = srcMidVar1.GroupNumber

destArgs1.grpMemberNo = srcMidVar1.GrpMemberNo

destArgs1.grpMemberShortNo = srcMidVar1.GrpMemberShortNo

destArgs1.numberType = srcMidVar1.NumberType

destArgs1.memberType = srcMidVar1.MemberType

destArgs1.quotaType = srcMidVar1.QuotaType

def listMapping10

listMapping10 = 
{
    src,dest  ->
	
	dest.id = src.Id
	
	dest.value = src.Value
	
}

def listMapping311

listMapping311 = 
{
    src,dest  ->
	
	dest.id = src.Id
	
	mappingList(src.SimpleProperty,dest.simpleProperty,listMapping10)
}

def listMapping31

listMapping31 = 
{
    src,dest  ->
	
	dest.id = src.Id
	
	mappingList(src.Service,dest.service,listMapping311)
	
	mappingList(src.SimpleProperty,dest.simpleProperty,listMapping10)
}

def listMapping3

listMapping3 = 
{
    src,dest  ->
	
	dest.id = src.Id
	
	mappingList(src.Product,dest.product,listMapping31)
	
	mappingList(src.simpleProperty,dest.product,listMapping10)
}

mappingList(srcMidVar1.GroupOffer,destArgs1.groupOffer,listMapping3)