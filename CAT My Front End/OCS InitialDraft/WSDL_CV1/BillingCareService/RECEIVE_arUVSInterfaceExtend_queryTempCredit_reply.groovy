def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar4 = destReturn.QueryTempCreditResponse.QueryTempCreditResult.ResultMessage

def destMidVar0 = destMidVar4.MessageHeader

def srcMidVar = srcReturn.ocs11ResultHeader

destMidVar0.CommandId = srcMidVar.commandId

destMidVar0.TenantId = srcMidVar.tenantId

destMidVar0.Version = srcMidVar.version

destMidVar0.TransactionId = srcMidVar.transactionId

destMidVar0.SequenceId = srcMidVar.sequenceId

destMidVar0.ResultCode = srcMidVar.resultCode

destMidVar0.ResultDesc = srcMidVar.resultDesc

destMidVar0.Language = srcMidVar.language

def destMidVar1 = destMidVar4.MessageBody

destMidVar1.Amount.Value = srcReturn.queryTempCreditResponse.amount.value

destMidVar1.Amount.MinMeasureId  = srcReturn.queryTempCreditResponse.amount.minMeasureId

def listMapping1

listMapping1 =
        {
            src,dest  ->

                dest.Amount.Value = src.amount.value

                dest.Amount.MinMeasureId = src.amount.minMeasureId

                dest.ApplyTime = src.applyTime

                dest.ExpiryTime = src.expiryTime
        }

def destMidVar2 = destMidVar4.MessageBody.TempCreditList.TempCreditRecord

def srcMidVar2 = srcReturn.queryTempCreditResponse.tempCreditList

mappingList(srcMidVar2,destMidVar2,listMapping1)


