import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.effectiveDate=formatDate(src.effectDate, Constant4Model.DATE_FORMAT)
	
	dest.expireDate=formatDate(src.expireDate, Constant4Model.DATE_FORMAT)
	
	dest.ZoneName = src.zoneName
	
	dest.HomeZoneNo = src.zoneNo
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.effectiveDate=formatDate(src.effectDate, Constant4Model.DATE_FORMAT)
	
	dest.expireDate=formatDate(src.expireDate, Constant4Model.DATE_FORMAT)
	
	dest.ZoneName = src.zoneName
	
	dest.HomeZoneNo = src.zoneNo
	
}

def srcMidVar = srcReturn.resultBody

srcMidVar._class = "com.huawei.ngcbs.cm.ocs12ws.subscriber.querysubhomezone.io.QuerySubHomeZoneResult"

def destMidVar = destReturn.QuerySubHomeZoneResultMsg.QuerySubHomeZoneResult

mappingList(srcMidVar.dataZoneInfos,destMidVar.DataZoneInfo,listMapping0)

mappingList(srcMidVar.homeZoneInfos,destMidVar.HomeZoneNoInfo,listMapping1)

def srcMidVar0 = srcReturn.resultHeader

srcMidVar0._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12ResultHeader"

def destMidVar0 = destReturn.QuerySubHomeZoneResultMsg.ResultHeader

destMidVar0.CommandId = srcMidVar0.commandId

destMidVar0.ResultCode = srcMidVar0.resultCode

destMidVar0.ResultDesc = srcMidVar0.resultDesc

destMidVar0.SequenceId = srcMidVar0.sequenceId

destMidVar0.TransactionId = srcMidVar0.transactionId

destMidVar0.Version = srcMidVar0.version
