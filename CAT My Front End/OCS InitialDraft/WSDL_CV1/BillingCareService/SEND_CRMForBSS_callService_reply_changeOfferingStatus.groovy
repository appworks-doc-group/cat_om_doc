def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.paraName = src.Name
	
	dest.paraValue = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.paraName = src.Name
	
	dest.paraValue = src.Value
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.paraName = src.Name
	
	dest.paraValue = src.Value
	
}

def destMidVar = destReturn.offeringInfo

def srcMidVar = srcReturn.ChangeOfferingStatusResult.OfferInfo

destMidVar.trialEndTime = srcMidVar.TrialEndTime

destMidVar.trialStartTime = srcMidVar.TrialStartTime

def srcMidVar0 = srcReturn.ChangeOfferingStatusResult.OfferInfo.OfferingID

destMidVar.offeringId = srcMidVar0.OfferingID

destMidVar.purchaseSeq = srcMidVar0.PurchaseSeq

destMidVar.latestActiveDate = srcMidVar.LastActiveTime

def srcMidVar1 = srcReturn.ChangeOfferingStatusResult.OfferInfo.ExtParamList

mappingList(srcMidVar1.AdditionInfo,destMidVar.extParmalist,listMapping0)

destMidVar.expireTime = srcMidVar.ExpireTime

destMidVar.effectiveTime = srcMidVar.EffectiveTime

def srcMidVar2 = srcReturn.ChangeOfferingStatusResult.ResponseHeader.ExtParamList

def destMidVar0 = destReturn.reponseHeader

mappingList(srcMidVar2.ParameterInfo,destMidVar0.paramInfos,listMapping1)

def destMidVar1 = destReturn.reponseHeader.reverseHeader

def srcMidVar3 = srcReturn.ChangeOfferingStatusResult.ResponseHeader.RequestHeader

destMidVar1.accessPwd = srcMidVar3.AccessPwd

destMidVar1.accessUser = srcMidVar3.AccessUser

destMidVar1.beId = srcMidVar3.BEId

destMidVar1.channelId = srcMidVar3.ChannelId

def srcMidVar4 = srcReturn.ChangeOfferingStatusResult.ResponseHeader.RequestHeader.ExtParamList

mappingList(srcMidVar4.ParameterInfo,destMidVar1.extParamList,listMapping2)

destMidVar1.language = srcMidVar3.Language

destMidVar1.operatorId = srcMidVar3.OperatorId

destMidVar1.operatorPwd = srcMidVar3.OperatorPwd

destMidVar1.processTime = srcMidVar3.ProcessTime

destMidVar1.sessionId = srcMidVar3.SessionId

destMidVar1.testFlag = srcMidVar3.TestFlag

destMidVar1.timeType = srcMidVar3.TimeType

destMidVar1.timeZoneId = srcMidVar3.TimeZoneID

destMidVar1.transactionId = srcMidVar3.TransactionId

destMidVar1.version = srcMidVar3.Version

def srcMidVar5 = srcReturn.ChangeOfferingStatusResult.ResponseHeader

destReturn.resultCode = srcMidVar5.RetCode

destReturn.resultDesc = srcMidVar5.RetMsg

destReturn._class = "com.huawei.ngcbs.cm.reverse.changeofferingstatus.io.ChangeOfferingStatusWSResult"
