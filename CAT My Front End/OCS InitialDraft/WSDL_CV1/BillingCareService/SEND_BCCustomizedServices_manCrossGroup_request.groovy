dest.setServiceOperation("BMGroupService","manCrossGroup")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.group.mancrossgroup.io.ManCrossGroupRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.ManCrossGroupRequestMsg.ManCrossGroupRequest

destArgs1.operType = srcMidVar.OperationType

def destMidVar = destArgs1.subGroupA

def srcMidVar0 = srcArgs0.ManCrossGroupRequestMsg.ManCrossGroupRequest.SubGroupAccessCodeA

destMidVar.groupCode = srcMidVar0.SubGroupCode

destMidVar.groupKey = srcMidVar0.SubGroupKey

def destMidVar0 = destArgs1.subGroupB

def srcMidVar1 = srcArgs0.ManCrossGroupRequestMsg.ManCrossGroupRequest.SubGroupAccessCodeB

destMidVar0.groupCode = srcMidVar1.SubGroupCode

destMidVar0.groupKey = srcMidVar1.SubGroupKey

def srcMidVar2 = srcArgs0.ManCrossGroupRequestMsg.RequestHeader

destArgs0.version = srcMidVar2.Version

def srcMidVar3 = srcArgs0.ManCrossGroupRequestMsg.RequestHeader.TimeFormat

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.timeType = srcMidVar3.TimeType

def srcMidVar4 = srcArgs0.ManCrossGroupRequestMsg.RequestHeader.OwnershipInfo

destArgs0.brId = srcMidVar4.BRID

destArgs0.beId = srcMidVar4.BEID

def srcMidVar5 = srcArgs0.ManCrossGroupRequestMsg.RequestHeader.OperatorInfo

destArgs0.operatorId = srcMidVar5.OperatorID

destArgs0.channelId = srcMidVar5.ChannelID

destArgs0.messageSeq = srcMidVar2.MessageSeq

destArgs0.businessCode = srcMidVar2.BusinessCode

mappingList(srcMidVar2.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

def srcMidVar6 = srcArgs0.ManCrossGroupRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar6.LoginSystemCode

destArgs0.password = srcMidVar6.Password

destArgs0.remoteAddress = srcMidVar6.RemoteIP

destArgs0.interMode = srcMidVar2.AccessMode

destArgs0.msgLanguageCode = srcMidVar2.MsgLanguageCode
