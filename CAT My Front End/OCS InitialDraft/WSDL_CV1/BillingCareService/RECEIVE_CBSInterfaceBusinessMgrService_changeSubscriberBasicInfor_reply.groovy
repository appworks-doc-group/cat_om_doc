def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.ChangeSubscriberBasicInforResultMsg.ResultHeader

destMidVar.CommandId = srcReturn.commandId

destMidVar.Version = srcReturn.version

destMidVar.TransactionId = srcReturn.transactionId

destMidVar.SequenceId = srcReturn.sequenceId

destMidVar.ResultCode = srcReturn.resultCode

destMidVar.ResultDesc = srcReturn.resultDesc
