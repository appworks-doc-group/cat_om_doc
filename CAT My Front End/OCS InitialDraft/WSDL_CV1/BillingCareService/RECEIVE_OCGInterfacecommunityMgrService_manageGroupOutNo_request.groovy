def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.GroupOutNumber = src.groupOutNumber
	
	dest.GroupOutShortNo = src.groupOutShortNo
	
	dest.EffectiveDate = src.effDate
	
	dest.ExpireDate = src.expDate
	
	dest.OutNumberType = src.outNumberType
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.BelToAreaID = src.belToAreaId
	
	dest.CommandId = src.commandId
	
	dest.currentCell = src.currentCell
	
	dest.InterFrom = src.interFrom
	
	dest.InterMedi = src.interMedi
	
	dest.InterMode = src.interMode
	
	dest.OperatorID = src.operatorId
	
	dest.Remark = src.remark
	
	dest.SequenceId = src.sequenceId
	
	dest.SerialNo = src.serialNo
	
	def destMidVar1 = dest.SessionEntity
	
	def srcMidVar = src.sessionEntity
	
	destMidVar1.Name = srcMidVar.name
	
	destMidVar1.Password = srcMidVar.password
	
	destMidVar1.RemoteAddress = srcMidVar.remoteAddress
	
	dest.TenantID = src.tenantId
	
	dest.TradePartnerID = src.tradePartnerId
	
	dest.TransactionId = src.transactionId
	
	dest.Version = src.version
	
	dest.visitArea = src.visitArea
	
	dest.PartnerOperID = src.partnerOperId
	
	dest.PartnerID = src.partnerId
	
	dest.RequestType = src.requestType
	
	dest.ThirdPartyID = src.thirdPartyId
	
}

def destMidVar = destArgs0.ManageGroupOutNoRequestMsg.ManageGroupOutNoRequest

destMidVar.GroupNumber = srcArgs0.groupNumber

destMidVar.OperationType = srcArgs0.operationType

mappingList(srcArgs0.groupOutNoInfoList,destMidVar.GroupOutNoInfo,listMapping0)

def destMidVar0 = destArgs0.ManageGroupOutNoRequestMsg

listMapping1.call(srcArgs0.omRequestHeader,destMidVar0.RequestHeader)
