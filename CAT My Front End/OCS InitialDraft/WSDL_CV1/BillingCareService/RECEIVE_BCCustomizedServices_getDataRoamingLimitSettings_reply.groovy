import com.huawei.ngcbs.bm.common.common.Constant4Model
def srcReturn = src.payload._return

def destReturn = dest.payload._return
destReturn._class = "com.huawei.www.bme.cbsinterface.bccustomizedservices.GetDataRoamingLimitSettingsResultMsgDocument"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destReturn.GetDataRoamingLimitSettingsResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.Version = srcMidVar.version

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.MessageSeq = srcMidVar.messageSeq

mappingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.SubscriberID = src.subscriberKey
	
	dest.ServiceNumber = src.primaryIdentity
	
	dest.RoamingLimitStatus = src.roamingLimitStatus
	
	dest.Threshold = src.threshold
	
	dest.HasReached = src.hasReached
	
	dest.CloseDate = formatDate(src.closeData, Constant4Model.DATE_FORMAT)
	
	dest.ExpiryDateOfClose = formatDate(src.expireDataOfClose, Constant4Model.DATE_FORMAT)
	
	dest.EnableSMSNotification = src.enableSMSNotification
	
}

def srcMidVar0 = srcReturn.resultInfo

def destMidVar0 = destReturn.GetDataRoamingLimitSettingsResultMsg.GetDataRoamingLimitSettingsResult
destMidVar0.ResultCount = srcMidVar0.resultCount
mappingList(srcMidVar0.roamingStatusList,destMidVar0.RoamingStatus,listMapping1)

destMidVar0.TotalRowNum = srcMidVar0.totalNumber
destMidVar0.BeginRowNum = srcMidVar0.beginRowNum
destMidVar0.FetchRowNum = srcMidVar0.fetchRowNum