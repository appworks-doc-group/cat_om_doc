def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 =
{
	src,dest  ->

	dest.TotalAmount = src.totalAmount

	dest.CurrencyID = src.currencyId

}

def destMidVar = destReturn.QuerySumInstallmentResultMsg.QuerySumInstallmentResult

def srcMidVar = srcReturn.installmentSumList

mappingList(srcMidVar,destMidVar.InstallmentSumList,listMapping0)

def destMidVar1 = destReturn.QuerySumInstallmentResultMsg.ResultHeader

def srcMidVar1 = srcReturn.resultHeader

destMidVar1.MsgLanguageCode = srcMidVar1.msgLanguageCode

destMidVar1.ResultCode = srcMidVar1.resultCode

destMidVar1.ResultDesc = srcMidVar1.resultDesc

destMidVar1.Version = srcMidVar1.version

destMidVar1.MessageSeq = srcMidVar1.messageSeq

