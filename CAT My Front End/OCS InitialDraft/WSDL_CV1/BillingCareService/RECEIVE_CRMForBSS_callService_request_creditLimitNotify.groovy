dest.setServiceOperation("CRMForBSS","creditLimitNotify")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping2

listMapping2 = 
{
    src,dest  ->

	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.AccessPwd = src.accessPwd
	
	dest.AccessUser = src.accessUser
	
	dest.BEId = src.beId
	
	dest.ChannelId = src.channelId
	
	def destMidVar0 = dest.ExtParamList.ParameterInfo[0]
	
	def srcMidVar = src.extParamList[0]
	
	destMidVar0.Name = srcMidVar.paraName
	
	destMidVar0.Value = srcMidVar.paraValue
	
	dest.Language = src.language
	
	dest.OperatorId = src.operatorId
	
	dest.OperatorPwd = src.operatorPwd
	
	dest.ProcessTime = src.processTime
	
	dest.SessionId = src.sessionId
	
	dest.TestFlag = src.testFlag
	
	dest.TimeType = src.timeType
	
	dest.TimeZoneID = src.timeZoneId
	
	dest.TransactionId = src.transactionId
	
	dest.Version = src.version
	
	def destMidVar1 = dest.ExtParamList
	
	mappingList(src.extParamList,destMidVar1.ParameterInfo,listMapping2)
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Name = src.paraName
	
	dest.Value = src.paraValue
	
}

def destMidVar = destArgs0.CreditLimitNotifyRequest

listMapping0.call(srcArgs0.workOrderHeader,destMidVar.RequestHeader)

def destMidVar2 = destArgs0.CreditLimitNotifyRequest.CreditLimitNotify

destMidVar2.AccountCode = srcArgs0.accountCode

destMidVar2.Accountkey = srcArgs0.accountKey

mappingList(srcArgs0.extParamList,destMidVar2.ExtParamList,listMapping1)

destMidVar2.OpType = srcArgs0.opType

destMidVar2.TimeStamp = srcArgs0.timestamp
