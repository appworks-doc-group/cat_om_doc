<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bcs="http://www.huawei.com/bme/cbsinterface/bcservices" xmlns:cbs="http://www.huawei.com/bme/cbsinterface/cbscommon" xmlns:bcc="http://www.huawei.com/bme/cbsinterface/bccommon">
   <soapenv:Header/>
   <soapenv:Body>
      <bcs:QueryOfferingInstPropertyRequestMsg>
         <RequestHeader>
            <cbs:Version>1</cbs:Version>
            <cbs:MessageSeq>Ricky_TEST1_${=(new java.text.SimpleDateFormat("yyyyMMddHHmmss")).format(new Date())}${=(int)(Math.random()*1000)}</cbs:MessageSeq>
            <cbs:OwnershipInfo>
               <cbs:BEID>20101</cbs:BEID>
            </cbs:OwnershipInfo>
            <cbs:AccessSecurity>
               <cbs:LoginSystemCode>ricky</cbs:LoginSystemCode>
               <cbs:Password>HdEFEEDdxWtx0uCSqBCAJF8FyDpeDkn8sWsQdd09lKg=</cbs:Password>
            </cbs:AccessSecurity>
         </RequestHeader>
         <QueryOfferingInstPropertyRequest>
            <bcs:OfferingOwner>
               <bcs:SubAccessCode>
                  <bcc:PrimaryIdentity>864201236</bcc:PrimaryIdentity>
               </bcs:SubAccessCode>
            </bcs:OfferingOwner>
            <bcs:OfferingInst>
               <bcs:OfferingKey>
                  <bcc:OfferingID>317658450</bcc:OfferingID>
               </bcs:OfferingKey>
            </bcs:OfferingInst>
         </QueryOfferingInstPropertyRequest>
      </bcs:QueryOfferingInstPropertyRequestMsg>
   </soapenv:Body>
</soapenv:Envelope>


<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <bcs:QueryOfferingInstPropertyResultMsg xmlns:bcs="http://www.huawei.com/bme/cbsinterface/bcservices" xmlns:bcc="http://www.huawei.com/bme/cbsinterface/bccommon" xmlns:cbs="http://www.huawei.com/bme/cbsinterface/cbscommon">
         <ResultHeader>
            <cbs:Version>1</cbs:Version>
            <cbs:ResultCode>0</cbs:ResultCode>
            <cbs:ResultDesc>Operation successfully.</cbs:ResultDesc>
         </ResultHeader>
         <QueryOfferingInstPropertyResult>
            <bcs:OfferingInst>
               <bcs:OfferingKey>
                  <bcc:OfferingID>317658450</bcc:OfferingID>
               </bcs:OfferingKey>
               <bcs:ProductInst>
                  <bcs:ProductID>1023</bcs:ProductID>
                  <bcs:ProductInstProperty>
                     <bcc:PropCode>C_FNINFO</bcc:PropCode>
                     <bcc:PropType>2</bcc:PropType>
                     <bcc:SubPropInst>
                        <bcc:SubPropCode>C_FN_NUMBER</bcc:SubPropCode>
                        <bcc:Value>66864201241</bcc:Value>
                     </bcc:SubPropInst>
                     <bcc:SubPropInst>
                        <bcc:SubPropCode>C_FN_TYPE</bcc:SubPropCode>
                        <bcc:Value>1</bcc:Value>
                     </bcc:SubPropInst>
                     <bcc:SubPropInst>
                        <bcc:SubPropCode>C_FN_GROUP</bcc:SubPropCode>
                        <bcc:Value>1</bcc:Value>
                     </bcc:SubPropInst>
                     <bcc:SubPropInst>
                        <bcc:SubPropCode>C_FN_SERIAL_NO</bcc:SubPropCode>
                        <bcc:Value>1</bcc:Value>
                     </bcc:SubPropInst>
                     <bcs:EffectiveTime>20190702115949</bcs:EffectiveTime>
                     <bcs:ExpirationTime>20370101000000</bcs:ExpirationTime>
                     <bcs:CreateTime>20190702115949</bcs:CreateTime>
                  </bcs:ProductInstProperty>
               </bcs:ProductInst>
            </bcs:OfferingInst>
         </QueryOfferingInstPropertyResult>
      </bcs:QueryOfferingInstPropertyResultMsg>
   </soapenv:Body>
</soapenv:Envelope>