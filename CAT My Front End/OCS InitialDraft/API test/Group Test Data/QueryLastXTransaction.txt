<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bcs="http://www.huawei.com/bme/cbsinterface/bcservices" xmlns:cbs="http://www.huawei.com/bme/cbsinterface/cbscommon" xmlns:bcc="http://www.huawei.com/bme/cbsinterface/bccommon">
   <soapenv:Header/>
   <soapenv:Body>
      <bcs:QueryLastXTransactionRequestMsg>
         <RequestHeader>
            <cbs:Version>1.0</cbs:Version>
            <cbs:BusinessCode>QueryLastXTransaction</cbs:BusinessCode>
            <cbs:MessageSeq>${=(new java.text.SimpleDateFormat("yyyyMMddHHmmss")).format(new Date())}${=(int)(Math.random()*1000)}</cbs:MessageSeq>
            <cbs:OwnershipInfo>
               <cbs:BEID>20101</cbs:BEID>
            </cbs:OwnershipInfo>
            <cbs:AccessSecurity>
               <cbs:LoginSystemCode>ricky</cbs:LoginSystemCode>
               <cbs:Password>HdEFEEDdxWtx0uCSqBCAJF8FyDpeDkn8sWsQdd09lKg=</cbs:Password>
            </cbs:AccessSecurity>
            <cbs:OperatorInfo>
               <cbs:OperatorID>101</cbs:OperatorID>
            </cbs:OperatorInfo>
            <cbs:AccessMode>3</cbs:AccessMode>
            <cbs:MsgLanguageCode>2040</cbs:MsgLanguageCode>
            <cbs:TimeFormat>
               <cbs:TimeType>1</cbs:TimeType>
            </cbs:TimeFormat>
         </RequestHeader>
         <QueryLastXTransactionRequest>
            <bcs:SubAccessCode>
               <bcc:PrimaryIdentity>864201236</bcc:PrimaryIdentity>
            </bcs:SubAccessCode>
         </QueryLastXTransactionRequest>
      </bcs:QueryLastXTransactionRequestMsg>
   </soapenv:Body>
</soapenv:Envelope>





<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <bcs:QueryLastXTransactionResultMsg xmlns:bcs="http://www.huawei.com/bme/cbsinterface/bcservices" xmlns:cbs="http://www.huawei.com/bme/cbsinterface/cbscommon">
         <ResultHeader>
            <cbs:Version>1.0</cbs:Version>
            <cbs:ResultCode>0</cbs:ResultCode>
            <cbs:MsgLanguageCode>2040</cbs:MsgLanguageCode>
            <cbs:ResultDesc>Operation successfully.</cbs:ResultDesc>
         </ResultHeader>
         <QueryLastXTransactionResult>
            <bcs:LastXRecords>
               <bcs:PrimaryIdentity>864201236</bcs:PrimaryIdentity>
               <bcs:ServiceType>12</bcs:ServiceType>
               <bcs:SubServiceType>1</bcs:SubServiceType>
               <bcs:RoamingType>0</bcs:RoamingType>
               <bcs:OppositeNumber>00166930241282</bcs:OppositeNumber>
               <bcs:StartTime>20190702115030</bcs:StartTime>
               <bcs:ChargeAmount>107</bcs:ChargeAmount>
               <bcs:CurrencyID>1141</bcs:CurrencyID>
            </bcs:LastXRecords>
            <bcs:LastXRecords>
               <bcs:PrimaryIdentity>864201236</bcs:PrimaryIdentity>
               <bcs:ServiceType>11</bcs:ServiceType>
               <bcs:SubServiceType>1</bcs:SubServiceType>
               <bcs:RoamingType>0</bcs:RoamingType>
               <bcs:OppositeNumber>00166930241282</bcs:OppositeNumber>
               <bcs:StartTime>20190702114859</bcs:StartTime>
               <bcs:ServiceUsage>60</bcs:ServiceUsage>
               <bcs:ChargeAmount>107</bcs:ChargeAmount>
               <bcs:CurrencyID>1141</bcs:CurrencyID>
            </bcs:LastXRecords>
            <bcs:LastXRecords>
               <bcs:PrimaryIdentity>864201236</bcs:PrimaryIdentity>
               <bcs:ServiceType>15</bcs:ServiceType>
               <bcs:StartTime>20190702114837</bcs:StartTime>
               <bcs:ChargeAmount>10000</bcs:ChargeAmount>
               <bcs:CurrencyID>1141</bcs:CurrencyID>
            </bcs:LastXRecords>
         </QueryLastXTransactionResult>
      </bcs:QueryLastXTransactionResultMsg>
   </soapenv:Body>
</soapenv:Envelope>