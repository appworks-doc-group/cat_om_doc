<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bcs="http://www.huawei.com/bme/cbsinterface/bcservices" xmlns:cbs="http://www.huawei.com/bme/cbsinterface/cbscommon" xmlns:bcc="http://www.huawei.com/bme/cbsinterface/bccommon">
   <soapenv:Header/>
   <soapenv:Body>
      <bcs:QueryGroupMemberListRequestMsg>
         <RequestHeader>
            *****Prepare the Request Header********
         </RequestHeader>
         <QueryGroupMemberListRequest>
            <bcs:SubGroupAccessCode>
               <bcc:SubGroupCode>333333333</bcc:SubGroupCode>
            </bcs:SubGroupAccessCode>
            <bcs:TotalNumber>3</bcs:TotalNumber>
            <bcs:BeginRowNum>0</bcs:BeginRowNum>
            <bcs:FetchRowNum>3</bcs:FetchRowNum>
         </QueryGroupMemberListRequest>
      </bcs:QueryGroupMemberListRequestMsg>
   </soapenv:Body>
</soapenv:Envelope>

Response : 

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <bcs:QueryGroupMemberListResultMsg xmlns:bcs="http://www.huawei.com/bme/cbsinterface/bcservices" xmlns:bcc="http://www.huawei.com/bme/cbsinterface/bccommon" xmlns:cbs="http://www.huawei.com/bme/cbsinterface/cbscommon">
         <ResultHeader>
            <cbs:Version>1</cbs:Version>
            <cbs:ResultCode>0</cbs:ResultCode>
            <cbs:ResultDesc>Operation successfully.</cbs:ResultDesc>
         </ResultHeader>
         <QueryGroupMemberListResult>
            <bcs:GroupMemberList>
               <bcs:PrimaryIdentity>864201253</bcs:PrimaryIdentity>
               <bcs:MemberProperty>
                  <bcc:Code>C_SERVICE_FLAG</bcc:Code>
                  <bcc:Value>000</bcc:Value>
               </bcs:MemberProperty>
            </bcs:GroupMemberList>
            <bcs:GroupMemberList>
               <bcs:PrimaryIdentity>864201254</bcs:PrimaryIdentity>
               <bcs:MemberProperty>
                  <bcc:Code>C_SERVICE_FLAG</bcc:Code>
                  <bcc:Value>000</bcc:Value>
               </bcs:MemberProperty>
            </bcs:GroupMemberList>
            <bcs:GroupMemberList>
               <bcs:PrimaryIdentity>864201255</bcs:PrimaryIdentity>
               <bcs:MemberProperty>
                 <bcc:Code>C_SERVICE_FLAG</bcc:Code>
                  <bcc:Value>000</bcc:Value>
               </bcs:MemberProperty>
            </bcs:GroupMemberList>
            <bcs:TotalNumber>3</bcs:TotalNumber>
            <bcs:BeginRowNum>0</bcs:BeginRowNum>
            <bcs:FetchRowNum>3</bcs:FetchRowNum>
         </QueryGroupMemberListResult>
      </bcs:QueryGroupMemberListResultMsg>
   </soapenv:Body>
</soapenv:Envelope>
