#!/bin/ksh
# Oracle Env.
export ORACLE_HOME=/oracle/product/10.2.0/client_1
export ORACLE_BASE=/oracle
export ORACLE_TERM=xterm
export ORACLE_SID=CRMDEVWP
export TNS_ADMIN=$ORACLE_HOME/network/admin
export NLS_LANG=AMERICAN_AMERICA.UTF8

DIR_APP="/arborbin/FX/mai/MNP_DEALER"

. $DIR_APP/parameter.ctl

echo "Enter Telno <= \\c"
read telno;

date=`date +%Y%m%d`
date_file=`date +%y%m%d%H%M`

export batch_status_tmp=10
export batch_status_complete=4.5
export batch_status_select=4

echo $CONNECTION_DEV

### Spool File ####
$ORACLE_HOME/bin/sqlplus -s $CONNECTION_DB<<EOF


update BATCH_SERVICE_TERMINATE
set BATCH_STATUS=4
 where PROPERTY_ONE='${telno}' and BATCH_STATUS in (4.5,4.25,5);
 

commit;

------------------------------------------------------------------------------------------
--------                              END SQLPLUS                               ----------
------------------------------------------------------------------------------------------
exit;
EOF



echo "###########FINISH###############"

exit 0



!