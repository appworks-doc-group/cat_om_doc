package com.cat.udb.esb.wrappers;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

//import util.Constant;
import com.cat.udb.esb.core.kenan.utils.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.BaseWrapper;

import com.cat.udb.esb.wrappers.common.ComponentLevelImpl;
import com.cat.udb.esb.wrappers.common.OrderedServiceImpl;
//import com.cat.udb.esb.wrappers.common.OrderedContractImpl;
//import com.cat.udb.esb.wrappers.common.OrderedNrcImpl;
import com.cat.udb.esb.wrappers.common.OrderedPackageImpl;

import com.cat.ibacss.esb.bo.convert.UpdateServicePackageConverter;
import com.cat.ibacss.esb.bo.convert.impl.UpdateServicePackageConverterImpl;
import com.cat.ibacss.esb.bo.convert.impl.UpdateServicePackageConverterUDBImpl;
import com.cat.ibacss.esb.bo.convert.SimpleConverter;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.bali.connection.ApiMappings;
import com.ibm.websphere.bo.BOFactory;

import commonj.sdo.DataObject;

public class UpdateServicePackageWrapper extends BaseWrapper {
	
	private static Logger logger = Logger.getLogger(UpdateServicePackageWrapper.class);

//	private Class CLASS = this.getClass();
//	
//	public UpdateServicePackageWrapper() {
//		super(Constant.LOG4j_PROPERTIES_PATH_UDB_A7);
//	}

	@Override
	protected void executeService() throws Throwable {
		//LoggerUtil.debug(PATH_LOG, "###### START UDB_A7 UpdateServicePackage ######", CLASS);
		logger.debug("###### START UDB_A7 UpdateServicePackage ######");
		
		this.request = mapToRequestHashMap(requestObj);
		response = (HashMap)request.clone();
		
		//Create Order
		//LoggerUtil.debug(PATH_LOG, "###### Call Create Order ######", CLASS);
		logger.debug("###### Call Create Order ######");
		HashMap reqOrder = MessageUtil.getOrder(request);
		HashMap resOrder = connection.call(context, ApiMappings.getCallName("OrderCreate"),reqOrder);
		//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderCreate Response ######" , CLASS, resOrder);
		if (logger.isDebugEnabled())
			logger.debug("###### OrderCreate Response ###### " + DebugUtil.getStringBuffer(resOrder).toString());
		
		request = MessageUtil.updateOrder(resOrder, request);
		HashMap order = MessageUtil.getOrder(request);
		
		String orderType = request.get("OrderType").toString();
		//LoggerUtil.debug(PATH_LOG, orderType, CLASS);
		logger.debug("orderType = " + orderType);
		
		//Create new Packages
		HashMap[] orderedPackagesNewList = (HashMap[])request.get("OrderedPackageNew");
		if (orderedPackagesNewList!=null) {
			//LoggerUtil.debug(PATH_LOG, "###### OrderedPackageNew ######", CLASS);
			logger.debug("###### OrderedPackageNew ######");
			//Object[] orderedPackages= OrderedPackageImpl.createOrderedPackage(orderedPackagesNewList, order,connection, context,PATH_LOG);
			Object[] orderedPackages= OrderedPackageImpl.createOrderedPackage(orderedPackagesNewList, order,connection, context);
			response.put("OrderedPackageNew", orderedPackages);
		}
		
					
		HashMap[] orderedServiceList = (HashMap[])request.get("Services");
		
		if (orderedServiceList != null) {
			//LoggerUtil.debug(PATH_LOG, "###### Create Service Level ######", CLASS);
			logger.debug("###### Create Service Level ######");
			ArrayList serviceResponsList = new ArrayList();
			
			for (HashMap orderedService : orderedServiceList) {
				
				HashMap serviceRequestField = (HashMap)orderedService.get("Service");
				//serviceRequestField.put("ExtendedData", MessageUtil.getExtendedData(serviceRequestField, Constant.SERVICE_EXTDATA));
				//Update Service property
				//OrderedServiceImpl.updateService(orderedService, connection, context);
				Object currencyCode = serviceRequestField.get("CurrencyCode");
				//Create new components that belong to a new package
				HashMap[] componentList = (HashMap[])serviceRequestField.get("Components");
				
				if(componentList != null){
					//LoggerUtil.debug(PATH_LOG, "###### Service-Level ComponentLevel(Components and Corridors) Create ######", CLASS);
					logger.debug("###### Service-Level ComponentLevel(Components and Corridors) Create ######");
					HashMap[] orderedPackages = (HashMap[])response.get("OrderedPackageNew");
					
//					Object[] componentLevelResponse = ComponentLevelImpl.createComponentLevel(componentList, null, order, 
//							orderedPackages, connection, context, PATH_LOG);
					Object[] componentLevelResponse = ComponentLevelImpl.createComponentLevel(componentList, null, order, orderedPackages, connection, context,currencyCode);
					serviceRequestField.put("Components",componentLevelResponse);			
				}
				
				serviceResponsList.add(orderedService);
			}
			response.put("Services", serviceResponsList.toArray());
		}
		
//		Disconnect old Packages
		HashMap[] orderedPackagesOldList = (HashMap[])request.get("OrderedPackageOld");
		
		if (orderedPackagesOldList!=null) {
			//LoggerUtil.debug(PATH_LOG, "###### OrderedPackageDisconnect ######", CLASS);
			logger.debug("###### OrderedPackageDisconnect ######");
			//Object[] orderedPackages= OrderedPackageImpl.disconnectOrderedPackage(orderedPackagesOldList, order,connection, context,PATH_LOG);
			Object[] orderedPackages= OrderedPackageImpl.disconnectOrderedPackage(orderedPackagesOldList, order,connection, context);
			response.put("OrderedPackageOld", orderedPackagesOldList);
		}
		
		//Commit Order
		//LoggerUtil.debug(PATH_LOG, "######  Commit Order ######", CLASS);
		logger.debug("######  Commit Order ######");
		reqOrder = MessageUtil.getOrder(request);
		//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderCommit ######" , OrderedPackageImpl.class, reqOrder);
		if (logger.isDebugEnabled())
			logger.debug("###### OrderCommit ###### " + DebugUtil.getStringBuffer(reqOrder).toString());
		resOrder = connection.call(context, ApiMappings.getCallName("OrderCommit"),reqOrder);
		
		response = MessageUtil.updateOrder(resOrder, response);
		return ;
	}

	@Override
	protected DataObject prepareResponseObj(DataObject reqObj, HashMap response, Throwable error) throws Exception {
		
		HashMap result = MessageUtil.getResult(error);
		String transactionLogId = MessageUtil.getTransactionLogId(reqObj); 
		DataObject responseObj = mapToResponseDataObj(reqObj, response, result, transactionLogId);
		return responseObj;
	}
	
	private HashMap mapToRequestHashMap(DataObject requestObj) throws Throwable {
		
		//Convert DataObject to hashmap
		UpdateServicePackageConverter reConverter = new UpdateServicePackageConverterUDBImpl();
		HashMap commonHashMap =  reConverter.dataObjectUpdatePackage2HashMap(requestObj);
		HashMap accountHashMap = (HashMap)commonHashMap.get("Account");
		//LoggerUtil.debugHashMap(PATH_LOG, "######  mapToRequestHashMap ######", CLASS,accountHashMap );
		if (logger.isDebugEnabled())
			logger.debug("######  mapToRequestHashMap ###### " + DebugUtil.getStringBuffer(accountHashMap).toString());
		
		return accountHashMap;
	}
	
	private DataObject mapToResponseDataObj(DataObject requestObj, HashMap response, HashMap result, String transactionLogId) throws Exception {
		
		HashMap responseHashMap = new HashMap();
		DataObject responseObj;
		
		responseHashMap.put("Account", response);
		responseHashMap.put("Result", result);
		responseHashMap.put("TransactionLogId", transactionLogId);
		
		//LoggerUtil.debugHashMap(PATH_LOG, "######  mapToResponseDataObj ######", CLASS,responseHashMap );
		if (logger.isDebugEnabled())
			logger.debug("######  mapToResponseDataObj ###### " + DebugUtil.getStringBuffer(responseHashMap).toString());
		
		UpdateServicePackageConverter serviceConvert = new UpdateServicePackageConverterImpl();
		responseObj = serviceConvert.hashMapUpdatePackage2DataObject(responseHashMap);

		//Set Order DataObject
		String orderType =  response.get("OrderType").toString();
			
		SimpleConverter simConvert = new SimpleConverter();
		BOFactory boFactory = simConvert.getBoFactory(com.cat.ibacss.esb.bo.constant.Constant.BO_FACTORY_NAME);
		DataObject orderDataObject = boFactory.create(com.cat.ibacss.esb.bo.constant.Constant.NAME_SPACE_BO, com.cat.ibacss.esb.bo.constant.Constant.BO_KENAN_ORDER);

		DataObject accountResp = responseObj.getDataObject("Account");
		accountResp.setDataObject("Order", orderDataObject);
		accountResp.setString("OrderType", orderType);

		return responseObj;
	}
}
