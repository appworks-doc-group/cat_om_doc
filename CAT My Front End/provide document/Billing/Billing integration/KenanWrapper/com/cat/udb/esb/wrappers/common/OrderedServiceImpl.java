package com.cat.udb.esb.wrappers.common;

import java.util.HashMap;
import java.util.Date;

import org.apache.log4j.Logger;

import com.cat.udb.esb.core.kenan.utils.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;
import com.cat.udb.esb.core.kenan.utils.ConverterUtil;

import com.cat.udb.esb.core.utils.DebugUtil;

//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.csgsystems.aruba.connection.Connection;
import com.csgsystems.bali.connection.ApiMappings;

public class OrderedServiceImpl {
	
	private static Logger logger = Logger.getLogger(OrderedServiceImpl.class);
	
//	public static HashMap createOrder (HashMap order, Connection connection, BSDMSessionContext context,String PATH_LOG) throws Throwable{
	public static HashMap createOrder (HashMap order, Connection connection, BSDMSessionContext context) throws Throwable{
		
		HashMap orderCreateResponse_tmp = new HashMap();
		//LoggerUtil.debug(PATH_LOG, "== Call Kenan API : OrderCreate ==", OrderedServiceImpl.class);
		logger.debug("== Call Kenan API : OrderCreate ==");
		orderCreateResponse_tmp = connection.call(context, ApiMappings.getCallName("OrderCreate"), order);
		//LoggerUtil.debugHashMap(PATH_LOG, "== orderCreateResponse_tmp ==", OrderedServiceImpl.class, orderCreateResponse_tmp);
		HashMap orderCreateResponse = new HashMap();
		orderCreateResponse.put("Order", orderCreateResponse_tmp.get("Order"));
		
		return orderCreateResponse;
	}
	
//	public static HashMap commitOrder(HashMap order, Connection connection, BSDMSessionContext context,String PATH_LOG) throws Throwable {
	public static HashMap commitOrder(HashMap order, Connection connection, BSDMSessionContext context) throws Throwable {
		HashMap orderCommitResponse_tmp = new HashMap();
		//LoggerUtil.debug(PATH_LOG, "== Call Kenan API : OrderCommit ==", OrderedServiceImpl.class);
		logger.debug("== Call Kenan API : OrderCommit ==");
		//LoggerUtil.debugHashMap(PATH_LOG, "== orderCommit request  ==", OrderedServiceImpl.class, order);
		if (logger.isDebugEnabled())
			logger.debug("== orderCommit request  == " + DebugUtil.getStringBuffer(order).toString());
		orderCommitResponse_tmp = connection.call(context, ApiMappings.getCallName("OrderCommit"), order);
		//LoggerUtil.debugHashMap(PATH_LOG, "== orderCommitResponse_tmp ==", OrderedServiceImpl.class, orderCommitResponse_tmp);
		HashMap orderCommitReponse = new HashMap();
		orderCommitReponse.put("Order", orderCommitResponse_tmp.get("Order"));
		
		return orderCommitReponse;
	}
	
//	public static HashMap serviceLevelProcess (HashMap orderService, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Throwable{
	public static HashMap serviceLevelProcess (HashMap orderService, Connection connection, BSDMSessionContext context) throws Throwable{
		// Call API: ServiceAlternateGet
		//HashMap serviceAlternateGetResponse = getServiceAlternate(orderService, connection, context, PATH_LOG);
		HashMap serviceAlternateGetResponse = getServiceAlternate(orderService, connection, context);
		
		// Set ServiceKey object
		((HashMap)orderService.get("Service")).put("Key", (HashMap)((HashMap)serviceAlternateGetResponse.get("Service")).get("Key"));
		//LoggerUtil.debugHashMap(PATH_LOG, " - - orderService - - ", OrderedServiceImpl.class, orderService);
		if (logger.isDebugEnabled())
			logger.debug(" - - orderService - - " + DebugUtil.getStringBuffer(orderService).toString());

		// Remove Source Fields that no need to update
		((HashMap)orderService.get("Service")).remove("ActiveDt");
		((HashMap)orderService.get("Service")).remove("CurrencyCode");
		((HashMap)orderService.get("Service")).remove("ServiceInactiveDt");
		
		// Updating a service
		//HashMap serviceUpdateResponse = updateService(orderService, connection, context, PATH_LOG);
		HashMap serviceUpdateResponse = updateService(orderService, connection, context);
		((HashMap)serviceUpdateResponse.get("Service")).put("SourceLineItemId", ((HashMap)orderService.get("Service")).get("SourceLineItemId").toString());
		((HashMap)serviceUpdateResponse.get("Service")).put("ServiceLineId", ((HashMap)orderService.get("Service")).get("ServiceLineId").toString());
		
		return serviceUpdateResponse;
	}
	
//	public static HashMap getServiceAlternate (HashMap serviceRequest, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Throwable{
	public static HashMap getServiceAlternate (HashMap serviceRequest, Connection connection, BSDMSessionContext context) throws Throwable{
		
		HashMap serviceAlternateResponse_tmp = new HashMap();
		HashMap serviceAlternateResponse = new HashMap();
		
		//LoggerUtil.debug(PATH_LOG, "== Call API Kenan : ServiceAlternateGet ==", OrderedServiceImpl.class);
		logger.debug("== Call API Kenan : ServiceAlternateGet ==");
		serviceAlternateResponse_tmp = connection.call(context, ApiMappings.getCallName("ServiceAlternateGet"), serviceRequest);
		
		//LoggerUtil.debugHashMap(PATH_LOG, " - - getServiceAlternate tmp - - ", OrderedServiceImpl.class, serviceAlternateResponse_tmp);
		
		serviceAlternateResponse.put("Service", serviceAlternateResponse_tmp.get("Service"));
		HashMap serviceExt = (HashMap)serviceAlternateResponse.get("Service");
		serviceExt.put("ExtendedData", MessageUtil.getExtendedData(serviceExt, Constant.SERVICE_EXTDATA));
		
		//LoggerUtil.debugHashMap(PATH_LOG, " - - getServiceAlternate - - ", OrderedServiceImpl.class, serviceAlternateResponse);
		
		return serviceAlternateResponse;
		
	}
	
//	public static HashMap updateService(HashMap serviceRequest, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Throwable{
	public static HashMap updateService(HashMap serviceRequest, Connection connection, BSDMSessionContext context) throws Throwable{
		
		HashMap serviceUpdateResponse_tmp = new HashMap();
		HashMap serviceUpdateResponse = new HashMap();
		
		//LoggerUtil.debug(PATH_LOG, "== Call API Kenan : updateService ==", OrderedServiceImpl.class);
		logger.debug("== Call API Kenan : updateService ==");
		if (logger.isDebugEnabled())
			logger.debug(DebugUtil.getStringBuffer(serviceRequest).toString());
		
		serviceUpdateResponse_tmp = connection.call(context, ApiMappings.getCallName("ServiceUpdate"), serviceRequest);
		
		//LoggerUtil.debugHashMap(PATH_LOG, "== serviceUpdateResponse_tmp ==", OrderedServiceImpl.class, serviceUpdateResponse_tmp);
		serviceUpdateResponse.put("Service", serviceUpdateResponse_tmp.get("Service"));
		HashMap serviceExt = (HashMap)serviceUpdateResponse.get("Service");
		serviceExt.put("ExtendedData", MessageUtil.getExtendedData(serviceExt, Constant.SERVICE_EXTDATA));
		
		if (serviceExt.containsKey("ExtendedData")){
			MessageUtil.setExtendedData(serviceExt);
		} 
		if (logger.isDebugEnabled())
			logger.debug("== ServiceUpdate Response == " + DebugUtil.getStringBuffer(serviceUpdateResponse).toString());
		
		return serviceUpdateResponse;
	}
	
//	public static HashMap updateOrderedService (HashMap orderedServiceRequest, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Throwable{
	public static HashMap updateOrderedService (HashMap orderedServiceRequest, Connection connection, BSDMSessionContext context) throws Throwable{
		
		HashMap orderedServiceUpdateResponse_tmp = new HashMap();
		HashMap orderedServiceUpdateResponse = new HashMap();
		
		//LoggerUtil.debug(PATH_LOG, "== Call API Kenan : updateOrderedService ==", OrderedServiceImpl.class);
		logger.debug("== Call API Kenan : updateOrderedService ==");
		orderedServiceUpdateResponse_tmp = connection.call(context, ApiMappings.getCallName("OrderedServiceUpdate"), orderedServiceRequest);
		
		//LoggerUtil.debugHashMap(PATH_LOG, "== orderedServiceUpdateResponse_tmp ==", OrderedServiceImpl.class, orderedServiceUpdateResponse_tmp);
		if (logger.isDebugEnabled())
			logger.debug("== orderedServiceUpdateResponse_tmp == " + DebugUtil.getStringBuffer(orderedServiceUpdateResponse).toString());
		orderedServiceUpdateResponse.put("Service", orderedServiceUpdateResponse_tmp.get("Service"));
		HashMap serviceExt = (HashMap)orderedServiceUpdateResponse.get("Service");
		serviceExt.put("ExtendedData", MessageUtil.getExtendedData(serviceExt, Constant.SERVICE_EXTDATA));
		
		if (serviceExt.containsKey("ExtendedData")){
			MessageUtil.setExtendedData(serviceExt);
		} 
		
		return orderedServiceUpdateResponse;
	}
	
//	public static HashMap [] componentLevelProcess (HashMap order, HashMap orderService, HashMap[] orderedComponentList, 
//			Connection connection, BSDMSessionContext context, String PATH_LOG) throws Throwable{
	public static HashMap [] componentLevelProcess (HashMap order, HashMap orderService, HashMap[] orderedComponentList, 
			Connection connection, BSDMSessionContext context) throws Throwable{
		
		//LoggerUtil.debug(PATH_LOG, "--- in componentLevelProcess ---", OrderedServiceImpl.class);
		logger.debug("--- in componentLevelProcess ---");
		
		if (orderedComponentList != null && orderedComponentList.length > 0) {
			//LoggerUtil.debugHashMap(PATH_LOG, "--- in componentLevelProcess service ---", OrderedServiceImpl.class, (HashMap)orderService.get("Service"));
			if (logger.isDebugEnabled())
				logger.debug("--- in componentLevelProcess service --- " + DebugUtil.getStringBuffer((HashMap)orderService.get("Service")).toString());
			
			HashMap service = (HashMap)orderService.get("Service");
			HashMap orderedComponent = new HashMap();
			HashMap component = new HashMap();
			HashMap orderedProductResponse = new HashMap();
			String viewId = "";
			HashMap orderProduct = new HashMap();
			
			for (int i=0; i<orderedComponentList.length; i++) {
				orderedComponent = (HashMap)orderedComponentList[i];
				
				//LoggerUtil.debugHashMap(PATH_LOG, "--- in componentLevelProcess orderedComponent ---", OrderedServiceImpl.class, orderedComponent);
				if (logger.isDebugEnabled())
					logger.debug("--- in componentLevelProcess orderedComponent --- " + DebugUtil.getStringBuffer(orderedComponent).toString());
				
				component = (HashMap)((HashMap)orderedComponent.get("OrderedComponent")).get("Component");
				
				if (viewIdValidation(component)) {
					viewId = ((HashMap)component.get("ProductRateKey")).get("ViewId").toString();
//					orderedProductResponse = orderedProductCreateProcessing(order, viewId, connection, context, PATH_LOG);
					orderedProductResponse = orderedProductCreateProcessing(order, viewId, connection, context);
				}
				else {
					viewId = ((HashMap)component.get("ProductRateOverride")).get("ViewId").toString();
					//orderedProductResponse = orderedProductCreateProcessing(order, viewId, connection, context, PATH_LOG);
					orderedProductResponse = orderedProductCreateProcessing(order, viewId, connection, context);
				}
				
				orderProduct = (HashMap)orderedProductResponse.get("Product");
				orderProduct.put("CurrencyCode", Integer.parseInt(service.get("CurrencyCode").toString()));
				orderProduct.put("ProductInactiveDt", (Date)service.get("ServiceInactiveDt"));
				orderProduct.put("ProductActiveDt", (Date)service.get("ActiveDt"));
				
				// ProductRateOverride
//				HashMap productRateOverrideResponse = ProductRateOverrideImpl.productRateOverrideProcessing(order, orderProduct, (HashMap)component.get("ProductRateOverride"), connection, context, PATH_LOG);
				HashMap productRateOverrideResponse = ProductRateOverrideImpl.productRateOverrideProcessing(order, orderProduct, (HashMap)component.get("ProductRateOverride"), connection, context);
				if(productRateOverrideResponse != null){
					component.put("ProductRateOverride", productRateOverrideResponse);
					productRateOverrideResponse = null;
				}
				
				// ProductRateKey
//				HashMap productRateKeyResponse = ProductRateKeyImpl.productRateKeyProcessing(order, orderProduct, (HashMap)component.get("ProductRateKey"), connection, context, PATH_LOG);
				HashMap productRateKeyResponse = ProductRateKeyImpl.productRateKeyProcessing(order, orderProduct, (HashMap)component.get("ProductRateKey"), connection, context);
				if(productRateKeyResponse != null){
					component.put("ProductRateKey", productRateKeyResponse);
					productRateKeyResponse = null;
				}
				
			}
		}
		return orderedComponentList;
	}
	
	public static boolean viewIdValidation (HashMap orderedComponent) throws Throwable{
		
		//LoggerUtil.debugHashMap(PATH_LOG, "--- in viewIdValidation orderedComponent ---", OrderedServiceImpl.class, orderedComponent);
		
		if (orderedComponent.containsKey("ProductRateKey")) {
			if (orderedComponent.containsKey("ProductRateOverride")) {
				if(!((HashMap)orderedComponent.get("ProductRateKey")).get("ViewId").toString().equals(((HashMap)orderedComponent.get("ProductRateOverride")).get("ViewId").toString()) ) {
					throw new Throwable("ProductRateKey.ViewId does not match with ProductRateOverride.ViewId");
				} else return false;
			} else return true;
		} else if (orderedComponent.containsKey("ProductRateOverride")) {
			return false;
		} else throw new Throwable("Component.ProductRateKey or Component.ProductRateOverride not found");
	}
	
//	public static HashMap orderedProductCreateProcessing(HashMap order, String viewId, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Throwable{
	public static HashMap orderedProductCreateProcessing(HashMap order, String viewId, Connection connection, BSDMSessionContext context) throws Throwable{
		HashMap orderedProductRequest_tmp = null;
		HashMap orderedProductRequest = null;

		// Find current product data
		//HashMap productResponse = productFind(viewId, connection, context, PATH_LOG);
		HashMap productResponse = productFind(viewId, connection, context);
		
		//LoggerUtil.debugHashMap(PATH_LOG, " *** productResponse *** ", OrderedServiceImpl.class, productResponse);
		if (logger.isDebugEnabled())
			logger.debug(" *** productResponse *** " + DebugUtil.getStringBuffer(productResponse).toString());
		// Create new product
		((HashMap)productResponse.get("Product")).put("PrevViewId", ((HashMap)((HashMap)productResponse.get("Product")).get("Key")).get("ViewId").toString() ); //.setPrevViewId(productResponse.getProductKey().getViewId());
		((HashMap)productResponse.get("Product")).remove("Key");
		((HashMap)productResponse.get("Product")).remove("ViewStatus");
		
		orderedProductRequest_tmp = new HashMap();
		orderedProductRequest_tmp.put("Product", productResponse);
		orderedProductRequest_tmp.put("Order", (HashMap)order.get("Order"));
		orderedProductRequest_tmp.put("FindExistingSO", true);
		orderedProductRequest = new HashMap();
		orderedProductRequest.put("OrderedProduct", orderedProductRequest_tmp);
		
		// Call API: OrderedProductCreate
		HashMap orderedProductResponse = connection.call(context, ApiMappings.getCallName("OrderedProductCreate"), orderedProductRequest);
		
		return orderedProductResponse;
	}
	
//	public static HashMap productFind(String viewId, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Throwable{
	public static HashMap productFind(String viewId, Connection connection, BSDMSessionContext context) throws Throwable{
		HashMap request = new HashMap();
		HashMap field = new HashMap();
		HashMap condition = new HashMap();
		HashMap productRequest = null;

		// Preparing Data Structure for ProductFind
		condition.clear();
		condition.put("Equal", ConverterUtil.toInteger("ViewId[0]", viewId.split("\\|")[0]));
		field.put("TrackingId", (HashMap)condition.clone());
		condition.clear();
		condition.put("Equal", ConverterUtil.toInteger("ViewId[1]", viewId.split("\\|")[1]));
		field.put("TrackingIdServ", (HashMap)condition.clone());
		condition.clear();
		condition.put("Equal", 2);
		field.put("ViewStatus", (HashMap)condition.clone());
		field.put("Fetch", true);
		request.put("Product", (HashMap)field.clone());
		condition.clear();
		field.clear();
		
		productRequest = new HashMap();
		productRequest.put("Product", request.get("Product"));
		
		HashMap productResponseList[] = ConverterUtil.getHashMapArray(connection.call(context, ApiMappings.getCallName("ProductFind"), productRequest), "ProductList","Product");

		// Result Condition: expect query return 1 record.
		if (productResponseList != null){
			if (productResponseList.length == 1){
				return productResponseList[0];
			}
			else if (productResponseList.length > 1) {
				throw new Throwable("Product.TrackingId and Product.TrackingIdServ has more one Product.ViewId that active currently, Check your condition");
			}
			else
				throw new Throwable("Product not found, Check Product.TrackingId and Product.TrackingIdServ to be valid");
		}
		else
			throw new Throwable("Product not found, Check Product.TrackingId and Product.TrackingIdServ to be valid");
	}
}
