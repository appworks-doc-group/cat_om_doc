package com.cat.udb.esb.wrappers.common;

import java.util.HashMap;

import org.apache.log4j.Logger;

//import util.HashMapHelper;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;
import com.cat.udb.esb.core.utils.DebugUtil;

//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.csgsystems.aruba.connection.Connection;
import com.csgsystems.bali.connection.ApiMappings;

public class OrderedComponentImpl {
	
	private static Logger logger = Logger.getLogger(OrderedComponentImpl.class);

	//public static HashMap createOrderedComponent(HashMap component, HashMap service, HashMap order, 
	//		HashMap[] orderedPackages,Connection connection, BSDMSessionContext context,String PATH_LOG) throws Throwable{
	public static HashMap createOrderedComponent(HashMap component, HashMap service, HashMap order, 
		HashMap[] orderedPackages,Connection connection, BSDMSessionContext context, Object currencyCode) throws Throwable {
		
		//Init request
		HashMap orderedComponentCreateResponse = null;
		Object orderDetail = order.get("Order");
		component.put("Order", orderDetail);
		component.put("FindExistingSO", true);
		component.put("VerboseResponse", true);
		
		HashMap componentField = (HashMap)component.get("Component");
		//componentField.put("CurrencyCode", currencyCode);
		
		if(service != null){
			HashMap serviceField = (HashMap)service.get("Service");
			componentField.put("ParentServiceInternalId", serviceField.get("ServiceInternalId"));
			componentField.put("ParentServiceInternalIdResets", serviceField.get("ServiceInternalIdResets"));
		}
		
		//Find ProductPackageKey
		HashMap packageKey = new HashMap();
		
		if(orderedPackages != null){
			packageKey = MessageUtil.getProductPackageKeyByComponent(orderedPackages, component);
			if(packageKey == null){
				throw new Exception("Uable to find a parent package for a component");
			}
			componentField.put("PackageInstId", packageKey.get("PackageInstId"));
			componentField.put("PackageInstIdServ", packageKey.get("PackageInstIdServ"));
		}
		
		//Call API
		//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedComponentCreate Request ######" , OrderedComponentImpl.class, component);
		if (logger.isDebugEnabled())
			logger.debug("###### OrderedComponentCreate Request ###### " + DebugUtil.getStringBuffer(component));
		orderedComponentCreateResponse = connection.call(context, ApiMappings.getCallName("OrderedComponentCreate"), component);
		//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedComponentCreate Response ######" , OrderedComponentImpl.class, orderedComponentCreateResponse);
		if (logger.isDebugEnabled())
			logger.debug("###### OrderedComponentCreate Response ###### " + DebugUtil.getStringBuffer(orderedComponentCreateResponse).toString());
		
		//Create ProductRateKey && ProductRateOverride
		Object[] productList = (Object[])orderedComponentCreateResponse.get("ProductList");
		if(productList != null && productList.length != 0){
			HashMap product = (HashMap)productList[0];
			if(product != null){
				
				if(componentField.get("ExtendedData")!=null){
					logger.debug("####### ExtenedData #######");
					ProductNameOverrideImpl.updateProductExtendedData(product,(Object[])componentField.get("ExtendedData"),connection, context);
				}
				
				//Create ProductRateKey
				if(componentField.get("ProductRateKey")!=null){
						HashMap productRateKey = ProductRateKeyImpl.createProductRateKey(product,(HashMap)componentField.get("ProductRateKey"),connection, context);
						((HashMap)orderedComponentCreateResponse.get("Component")).putAll(productRateKey);
				}
				
				//Create ProductRateOverride
				if(componentField.get("ProductRateOverride")!=null){
//					Object currencyCode = componentField.get("CurrencyCode");
//					if (currencyCode == null) {
//						currencyCode = component.get("CurrencyCode");
//					}
					HashMap productRateOverride = ProductRateOverrideImpl.createProductRateOverride(product,(HashMap)componentField.get("ProductRateOverride"),currencyCode,connection, context);
					((HashMap)orderedComponentCreateResponse.get("Component")).putAll(productRateOverride);
				}
			}
		}
	
		((HashMap)orderedComponentCreateResponse.get("Component")).put("ServiceLineId", componentField.get("ServiceLineId"));
		((HashMap)orderedComponentCreateResponse.get("Component")).put("SourceLineItemId", componentField.get("SourceLineItemId"));
	
		//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedComponentCreate Response ######" , OrderedComponentImpl.class, orderedComponentCreateResponse);
		if (logger.isDebugEnabled())
			logger.debug("###### OrderedComponentCreate Response ###### " + DebugUtil.getStringBuffer(orderedComponentCreateResponse).toString());
		return orderedComponentCreateResponse;
	}
	
	//public static HashMap disconnectOrderedComponent(HashMap orderComponent, HashMap service, HashMap order, 
	//		Connection connection, BSDMSessionContext context,String PATH_LOG) throws Throwable{
	public static HashMap disconnectOrderedComponent(HashMap orderComponent, HashMap service, HashMap order, 
		Connection connection, BSDMSessionContext context) throws Throwable{

		//Init request
		HashMap orderedComponentUpdateResponse = null;
		Object orderDetail = order.get("Order");
		HashMap component = (HashMap)orderComponent.get("OrderedComponent");
		component.put("Order", orderDetail);
		component.put("FindExistingSO", true);
		
		HashMap componentField = (HashMap)component.get("Component");
		if (service != null){
			HashMap serviceField = (HashMap)service.get("Service");
			componentField.put("CurrencyCode", serviceField.get("CurrencyCode"));
		}			
		
		// Calling API: OrderedComponentDisconnect
		//LoggerUtil.debug(PATH_LOG,"###### Call API Kenan : OrderedComponentDisconnect ######" , OrderedComponentImpl.class);
		logger.debug("###### Call API Kenan : OrderedComponentDisconnect ######");
		//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedComponentDisconnect Request ######" , OrderedComponentImpl.class, component);
		if (logger.isDebugEnabled())
			logger.debug("###### OrderedComponentDisconnect Request ###### " + DebugUtil.getStringBuffer(component).toString());
		
		HashMap orderedComponentUpdateResponse_tmp = connection.call(context, ApiMappings.getCallName("OrderedComponentDisconnect"), component);
		
		//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedComponentDisconnect Response ######" , OrderedComponentImpl.class, orderedComponentUpdateResponse_tmp);
		
		if (orderedComponentUpdateResponse_tmp.containsKey("Component")) {
			((HashMap)orderedComponentUpdateResponse_tmp.get("Component")).put("ServiceLineId", componentField.get("ServiceLineId"));
			((HashMap)orderedComponentUpdateResponse_tmp.get("Component")).put("SourceLineItemId", componentField.get("SourceLineItemId"));
		} 
		
		orderedComponentUpdateResponse = new HashMap();
		orderedComponentUpdateResponse.put("OrderedComponent", orderedComponentUpdateResponse_tmp);
		
		if (logger.isDebugEnabled())
			logger.debug("###### OrderedComponentDisconnect Response ###### " + DebugUtil.getStringBuffer(orderedComponentUpdateResponse).toString());
		
		return orderedComponentUpdateResponse;
	}
	
}
