package com.cat.udb.esb.wrappers;

import java.util.HashMap;

import org.apache.log4j.Logger;

//import util.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.udb.esb.core.utils.DebugUtil;

//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.bali.connection.ApiMappings;
import com.cat.udb.esb.core.kenan.wrappers.OrderedServiceBaseWrapper;

public class ReconnectServiceWrapper extends OrderedServiceBaseWrapper {
	
	private static Logger logger = Logger.getLogger(ReconnectServiceWrapper.class);

//	private Class CLASS = this.getClass();
//	
//	public ReconnectServiceWrapper() {
//		super(Constant.LOG4j_PROPERTIES_PATH_UDB_A4);
//	}

	@Override
	protected void executeOrderedService() throws Exception {
		try {
			HashMap orderedService = MessageUtil.getOrderedService(request);
			//LoggerUtil.debugHashMap(PATH_LOG,"###### Call OrderedServiceResume ######" , CLASS, orderedService);
			if (logger.isDebugEnabled())
				logger.debug("###### Call OrderedServiceResume ###### " + DebugUtil.getStringBuffer(orderedService).toString());
			
			HashMap APIresponse = connection.call(context, ApiMappings.getCallName("OrderedServiceResume"), orderedService);
			
			//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedServiceResume Response ######" , CLASS, APIresponse);
			if (logger.isDebugEnabled())
				logger.debug("###### OrderedServiceResume Response ###### " + DebugUtil.getStringBuffer(APIresponse).toString());
			
			response = MessageUtil.setOrderedService(APIresponse, request);
		}
		catch (Exception e) {
			logger.error(DebugUtil.getStackTrace(e));
			throw e;
		}
		//return;
	}
}
