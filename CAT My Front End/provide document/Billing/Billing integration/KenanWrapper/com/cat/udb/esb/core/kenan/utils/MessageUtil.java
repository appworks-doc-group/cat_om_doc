package com.cat.udb.esb.core.kenan.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.cat.ibacss.esb.bo.convert.*;
import com.cat.ibacss.esb.bo.convert.impl.*;
import commonj.sdo.DataObject;

public class MessageUtil {
	
	public static String getTransactionLogId(DataObject reqObj) {
		
		String transactionLogId = "";
		//get transaction ID
		if(reqObj != null && reqObj.getDataObject("TransactionLog") != null){
			DataObject tranLogDO = reqObj.getDataObject("TransactionLog");
			transactionLogId = tranLogDO.getString("TranID");
		}
		return transactionLogId; 
	}

	public static HashMap getResult(Throwable t ) {
		HashMap result = new HashMap();
		if(t==null){
			result.put("Code", "0");
			result.put("Text", "Success");
		}else{
			result.put("Code", "1");
			result.put("Text", t.getLocalizedMessage());
			result.put("Detail", t.getStackTrace());
		}
		return result;
	}
	
	public static HashMap updateOrder(HashMap order, HashMap request) {
		
		request.put("Order", order);
		return request;
	}

	public static HashMap getOrder(HashMap request) {
		
		HashMap order = (HashMap)request.get("Order");
		return order;
	}

	public static HashMap setOrderedService(HashMap OrderedService, HashMap request){
		
		HashMap[] map = new HashMap[1];
		map[0] = OrderedService;
		request.put("OrderedService", map);
		return request;
	}
	
	public static HashMap getOrderedService(HashMap request) {

		HashMap[] OrderedService = (HashMap[])request.get("OrderedService");
		HashMap service = OrderedService[0]; 	
		
		Object order = ((HashMap)request.get("Order")).get("Order");
		
		service.put("Order",order);
		
		return service;
	}
	
	public static HashMap getProductPackageKeyByComponent(HashMap[] orderedPackages, HashMap component){
		
		HashMap componentField = (HashMap)component.get("Component");
		
		String componentPackageId = componentField.get("PackageId").toString().trim();
		String componentServiceLineId = componentField.get("ServiceLineId").toString().trim();
		
		for (HashMap productPackage : orderedPackages) {
			HashMapHelper.printHashMap(productPackage);	
			HashMap productPackageField = (HashMap)productPackage.get("ProductPackage");
			
			String packagePackageId = productPackageField.get("PackageId").toString().trim();
			String packageServiceLineId = productPackageField.get("ServiceLineId").toString().trim();
				
			if ((packagePackageId.equals(componentPackageId))&&
					(packageServiceLineId.equals(componentServiceLineId))){
				
				return (HashMap)productPackageField.get("Key");
			}
		}
		return null;
	}
	
	public static Object getProductRateViewId(HashMap key){
		
		Object trackingId = key.get("TrackingId");
		Object trackingIdServ = key.get("TrackingIdServ");
		Object activeDt = key.get("ActiveDt");
		
		StringBuilder viewID = new StringBuilder();
		viewID.append(trackingId);
		viewID.append('|');
		viewID.append(trackingIdServ);
		viewID.append('|');
		viewID.append(ConverterUtil.toDateString((Date)activeDt));

		return viewID;
	}
	
	public static Object[] getExtendedData(HashMap request, String extDataType) throws Exception {

		String extData[] = ExtendedDataLoader.load().getProperty(extDataType).split("\\,");
		List extendedData = new ArrayList();
		for (String extendedKey : extData) {
			if(request.containsKey(extendedKey)){
				extendedData.add(request.get(extendedKey));
			}
		}
		return extendedData.toArray();
	}
	
	public static void setExtendedData(HashMap request){
		Object[] extendedDataList = (Object[])(request.get("ExtendedData"));
		for (Object extendedData : extendedDataList) {
			HashMap Data = (HashMap)extendedData;
			request.put(Data.get("ParamName").toString(), Data);
		}

	}
}
