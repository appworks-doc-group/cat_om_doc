package com.cat.udb.esb.wrappers;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cat.udb.esb.core.kenan.utils.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.BaseWrapper;

import com.cat.ibacss.esb.bo.convert.UpdatePaymentMethodConverter;
import com.cat.ibacss.esb.bo.convert.impl.UpdatePaymentMethodConverterImpl;
import com.csgsystems.bali.connection.ApiMappings;
import commonj.sdo.DataObject;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;

public class UpdatePaymentMethodWrapper extends BaseWrapper {
	
	private static Logger logger = Logger.getLogger(UpdatePaymentMethodWrapper.class);
	
	//private static final String PATH_LOG = Constant.LOG4j_PROPERITES_PATH_UDB_A15;
//	private Class CLASS = this.getClass();
//	
//	public UpdatePaymentMethodWrapper() {
//		super(Constant.LOG4j_PROPERITES_PATH_UDB_A15);
//	}
	
	@Override
	protected void executeService() throws Throwable {
		
		//LoggerUtil.debug(PATH_LOG, "###### UpdatePaymentMethodWrapper - executeService START ######", CLASS);
		logger.debug("###### UpdatePaymentMethodWrapper - executeService START ######");
		
		//Convert request object
		//LoggerUtil.debug(PATH_LOG, "###### mapToRequestUpdateHashMap ######", CLASS);
		logger.debug("###### mapToRequestUpdateHashMap ######");
		HashMap updatePaymentMethod = null;
		updatePaymentMethod = mapToRequestServiceHashMap(requestObj);
		//LoggerUtil.debugHashMap(PATH_LOG, "== mapToRequestServiceHashMap ==", CLASS, updatePaymentMethod);
		if (logger.isDebugEnabled())
			logger.debug("== mapToRequestServiceHashMap == " + DebugUtil.getStringBuffer(updatePaymentMethod).toString());
		
		//Init request message
		request = new HashMap();
		request.put("Account", (HashMap)updatePaymentMethod.get("Account"));
		HashMap requestExt = (HashMap)request.get("Account");
		requestExt.put("ExtendedData", MessageUtil.getExtendedData(requestExt, Constant.ACCOUNT_EXTDATA));
		//LoggerUtil.debugHashMap(PATH_LOG, "== account ==", CLASS, request);
		if (logger.isDebugEnabled())
			logger.debug("== account == " + DebugUtil.getStringBuffer(request).toString());
		
		//Init paymentProfile request message
		HashMap paymentProfileObject = new HashMap();
		paymentProfileObject.put("PaymentProfile", (HashMap)updatePaymentMethod.get("PaymentProfile"));
		HashMap paymentProfileRequest = (HashMap)paymentProfileObject.get("PaymentProfile");
		
		//Call UpdatePaymentMethod
		HashMap accountUpdateResponse = new HashMap();
		accountUpdateResponse = updatePaymentMethod (paymentProfileRequest);
		
		//LoggerUtil.debug(PATH_LOG, "###### UpdatePaymentMethodWrapper - executeService END ######", CLASS);
		logger.debug("###### UpdatePaymentMethodWrapper - executeService END ######");
		
		if (((HashMap)accountUpdateResponse.get("Account")).containsKey("ExtendedData")){
			MessageUtil.setExtendedData(((HashMap)accountUpdateResponse.get("Account")));
			((HashMap)accountUpdateResponse.get("Account")).remove("ExtendedData");
		}
		
		response = accountUpdateResponse;
		//LoggerUtil.debugHashMap(PATH_LOG, "accountUpdateResponse LAST", CLASS, accountUpdateResponse);
		if (logger.isDebugEnabled())
			logger.debug("accountUpdateResponse LAST " + DebugUtil.getStringBuffer(accountUpdateResponse).toString());
		
		return;
	}
	
	private HashMap updatePaymentMethod (HashMap paymentProfileRequest) throws Exception {
		
		HashMap [] paymentProfileList = new HashMap [1];
		HashMap accountUpdateResponse = new HashMap();
		if (Integer.parseInt(paymentProfileRequest.get("PayMethod").toString()) == 1){ // Check payment method = 1, no need create new one, use existing payment profile

			//LoggerUtil.debug(PATH_LOG, "#-#-#-#-#-#-#- Pay Method = 1 -#-#-#-#-#-#-#", CLASS);
			logger.debug("#-#-#-#-#-#-#- Pay Method = 1 -#-#-#-#-#-#-#");
			HashMap paymentProfileFind = findPaymentProfileObject(Integer.parseInt(paymentProfileRequest.get("AccountInternalId").toString()),Integer.parseInt(paymentProfileRequest.get("PayMethod").toString()));
			
			// Call Kenan API "PaymentProfileFind"
			//LoggerUtil.debug(PATH_LOG, "== Call Kenan API PaymentProfileFind ==", CLASS );
			logger.debug("== Call Kenan API PaymentProfileFind ==");
			HashMap PaymentProfileRes = connection.call(context, ApiMappings.getCallName("PaymentProfileFind"), paymentProfileFind);
			//LoggerUtil.debugHashMap(PATH_LOG, "== PaymentProfileRes 1==", CLASS, PaymentProfileRes);
			if (logger.isDebugEnabled())
				logger.debug("== PaymentProfileRes 1== " + DebugUtil.getStringBuffer(PaymentProfileRes).toString());
			
			HashMap [] paymentProfileFindResponse = null;
			paymentProfileFindResponse = getObjectList(PaymentProfileRes, "PaymentProfileList", "PaymentProfile");
			
			if (paymentProfileFindResponse.length == 1) {
				HashMap p = new HashMap();
				p = (HashMap)((HashMap)paymentProfileFindResponse[0].get("PaymentProfile")).clone();
				((HashMap)request.get("Account")).put("PaymentProfileId", ((HashMap)p.get("Key")).get("ProfileId"));
				
				// Call Kenan API "AccountUpdate"
				HashMap tempRes = connection.call(context, ApiMappings.getCallName("AccountUpdate"), request);
				accountUpdateResponse.put("Account", tempRes.get("Account"));	
				paymentProfileList[0] = new HashMap();
				paymentProfileList[0].put("PaymentProfile", p);
				
			}
		}
		else {	
			//LoggerUtil.debug(PATH_LOG, "#-#-#-#-#-#-#- Pay Method <> 1 -#-#-#-#-#-#-#", CLASS);
			logger.debug("#-#-#-#-#-#-#- Pay Method <> 1 -#-#-#-#-#-#-#");
			String args[] = new String[4];
			args[0] = paymentProfileRequest.get("AccountInternalId").toString();
			args[1] = paymentProfileRequest.get("CustBankSortCode").toString();
			args[2] = paymentProfileRequest.get("CustBankAccNum").toString();
			args[3] = paymentProfileRequest.get("OwnrLname").toString();
			
			// Call Kenan API "PaymentProfileFind"
			//LoggerUtil.debug(PATH_LOG, "== Call Kenan API PaymentProfileFind ==", CLASS );
			logger.debug("== Call Kenan API PaymentProfileFind ==");
			HashMap PaymentProfileRes = connection.call(context, ApiMappings.getCallName("PaymentProfileFind"), findExistingPaymentProfile(args));
			//LoggerUtil.debugHashMap(PATH_LOG, "== PaymentProfileRes 2==", CLASS, PaymentProfileRes);
			if (logger.isDebugEnabled())
				logger.debug("== PaymentProfileRes 2== " + DebugUtil.getStringBuffer(PaymentProfileRes).toString());
			
			HashMap [] paymentProfileFindResponse = null;
			paymentProfileFindResponse = getObjectList(PaymentProfileRes, "PaymentProfileList", "PaymentProfile");
			
			if (paymentProfileFindResponse.length > 0) {
				//LoggerUtil.debug(PATH_LOG, "== paymentProfileFindResponse.length > 0 ==", CLASS );
				logger.debug("== paymentProfileFindResponse.length > 0 ==");
				
				HashMap tempRes = null;
				HashMap pp = null;
				HashMap paymentProfileUpdateResponse = null;
				HashMap paymentProfileRequestObject = null;
				for (int i=0; i<paymentProfileFindResponse.length; i++) {					
					pp = new HashMap();
					pp = (HashMap)paymentProfileFindResponse[i].get("PaymentProfile");
					paymentProfileRequestObject = new HashMap();
					paymentProfileRequest.put("Key",pp.get("Key"));
					paymentProfileRequestObject.put("PaymentProfile", paymentProfileRequest);
					
					// Call Kenan API "PaymentProfileUpdate"
					tempRes = new HashMap();
					tempRes = connection.call(context, ApiMappings.getCallName("PaymentProfileUpdate"), paymentProfileRequestObject);
					paymentProfileUpdateResponse = new HashMap();
					paymentProfileUpdateResponse.put("PaymentProfile", tempRes.get("PaymentProfile"));
					
					if(paymentProfileUpdateResponse != null){ // Update latest payment profile id to account no		
						//LoggerUtil.debug(PATH_LOG, "== Update latest payment profile id to account no ==", CLASS );
						logger.debug("== Update latest payment profile id to account no ==");
						((HashMap)request.get("Account")).put("PaymentProfileId", ((HashMap)((HashMap)((HashMap)paymentProfileUpdateResponse).get("PaymentProfile")).get("Key")).get("ProfileId"));
						
						// Call Kenan API "AccountUpdate"
						tempRes = new HashMap();
						tempRes = connection.call(context, ApiMappings.getCallName("AccountUpdate"), request);
						accountUpdateResponse.put("Account", tempRes.get("Account"));	
						paymentProfileList[0] = new HashMap();
						paymentProfileList[0].put("PaymentProfile", paymentProfileUpdateResponse);
					}
				}
			}
			else {
				//LoggerUtil.debug(PATH_LOG, "== paymentProfileFindResponse.length <= 0 ==", CLASS );
				logger.debug("== paymentProfileFindResponse.length <= 0 ==");
				HashMap tempRes = new HashMap();
				HashMap paymentProfileRequestObject = null;
				
				// AccountInternalId
				paymentProfileRequestObject = new HashMap();
				paymentProfileRequest.put("AccountInternalId", (((HashMap)((HashMap)request.get("Account")).get("Key"))).get("AccountInternalId"));
				paymentProfileRequestObject.put("PaymentProfile", paymentProfileRequest);

				// Call Kenan API "PaymentProfileCreate"
				tempRes = connection.call(context, ApiMappings.getCallName("PaymentProfileCreate"), paymentProfileRequestObject);
				HashMap paymentProfileUpdateResponse = new HashMap();
				paymentProfileUpdateResponse.put("PaymentProfile", tempRes.get("PaymentProfile"));
				
				if(paymentProfileUpdateResponse != null){ // Update latest payment profile id to account no						
					((HashMap)request.get("Account")).put("PaymentProfileId", ((HashMap)((HashMap)((HashMap)paymentProfileUpdateResponse).get("PaymentProfile")).get("Key")).get("ProfileId"));
					
					// Call Kenan API "AccountUpdate"
					tempRes = new HashMap();
					tempRes = connection.call(context, ApiMappings.getCallName("AccountUpdate"), request);
					accountUpdateResponse.put("Account", tempRes.get("Account"));	 
					paymentProfileList[0] = new HashMap();
					paymentProfileList[0].put("PaymentProfile", paymentProfileUpdateResponse);						
				}
			}
		}

		accountUpdateResponse.put("PaymentProfiles", paymentProfileList);
		
		return accountUpdateResponse;
	}

	@Override
	protected DataObject prepareResponseObj(DataObject reqObj, HashMap response, Throwable error) throws Exception {
		MessageUtil msgHelper = new MessageUtil();
		HashMap result = msgHelper.getResult(error);
		String transactionLogId = msgHelper.getTransactionLogId(reqObj);
		DataObject responseObj = mapToResponseServiceDataObj(response, result, transactionLogId);
		return responseObj;
	}
	
	public HashMap mapToRequestServiceHashMap(DataObject requestObj) throws Exception {
		HashMap requestHashMap = null;
		try {
			UpdatePaymentMethodConverter upConverter = new UpdatePaymentMethodConverterImpl();
			requestHashMap = upConverter.dataObject2HashMap(requestObj);
		}
		catch (Exception e) {
			//LoggerUtil.error(PATH_LOG, "Exception : UpdatePaymentMethodWrapperImpl = mapUdatePaymentMethodObject ---> "+e.getMessage(), CLASS);
			logger.error(DebugUtil.getStackTrace(e));
			throw e;
		}
		return requestHashMap;
	}
	
	private DataObject mapToResponseServiceDataObj(HashMap response, 
			HashMap result, String transactionLogId) throws Exception {
		
		HashMap responseHashMap = new HashMap();
		DataObject responseObj;
		
		MessageUtil msgHelper = new MessageUtil();
		
		if (response != null){
			responseHashMap.put("Account", (HashMap)response.get("Account"));
		}else{
			responseHashMap.put("Account", request);
		}
		
		responseHashMap.put("Result", result);
		responseHashMap.put("TransactionLogId", transactionLogId);
		
		//LoggerUtil.debug(PATH_LOG, "#### mapToResponseServiceDataObj : ####", CLASS);
		logger.debug("#### mapToResponseServiceDataObj : ####");
		//LoggerUtil.debugHashMap(PATH_LOG, "#### mapToResponseServiceDataObj : ####", CLASS,responseHashMap);
		if (logger.isDebugEnabled())
			logger.debug("#### mapToResponseServiceDataObj : #### " + DebugUtil.getStringBuffer(responseHashMap).toString());
		
		UpdatePaymentMethodConverter UpdatePaymentMethodConverter = new UpdatePaymentMethodConverterImpl();
		DataObject requestObj = UpdatePaymentMethodConverter.hashMapUpdate2DataObject(responseHashMap);
		
		return requestObj;
	}
	
	private HashMap findPaymentProfileObject(Integer accountInternal, Integer paymethod)  throws Exception {
		HashMap request = new HashMap();
		HashMap field = new HashMap();
		HashMap condition = new HashMap();
		request.clear();
		field.clear();
		condition.clear();
		condition.put("Equal",paymethod);
		field.put("PayMethod", (HashMap)condition.clone());
		condition.clear();
		condition.put("Equal",accountInternal);
		field.put("AccountInternalId", (HashMap)condition.clone());
		field.put("Fetch", true);
		request.put("PaymentProfile", (HashMap)field.clone());
		return request;
	}
	
	private HashMap findExistingPaymentProfile(String args[])  throws Exception {
		HashMap condition = new HashMap();
		HashMap field = new HashMap();
		HashMap request = new HashMap();
		/*
		 * Condition for check existing payment profile
		 * 
		 * 1. Account Internal Id
		 * 2. Cust Bank Sort Code
		 * 3. Cust Bank Acct Name
		 * 4. Ownr Lname
		 */
		condition.clear();
		condition.put("Equal", Integer.valueOf(args[0]));
		field.put("AccountInternalId", (HashMap)condition.clone());
		condition.clear();
		condition.put("Equal", args[1]);
		field.put("CustBankSortCode", (HashMap)condition.clone());
		condition.clear();
		condition.put("Equal", args[2]);
		field.put("CustBankAccNum", (HashMap)condition.clone());
		condition.clear();
		condition.put("Equal", args[3]);
		field.put("OwnrLname", (HashMap)condition.clone());
		field.put("Fetch", true);
		request.put("PaymentProfile", (HashMap)field.clone());
		return request;
	}
	
	public static HashMap[] getObjectList(HashMap rspObjList,String objListName,String ObjName)  throws Exception {
		Object[] obj = null;
		HashMap[] ret = null;
		HashMap responseList[] = null;
		HashMap objOut = null;
		
		try{
			obj = (Object[]) rspObjList.get(objListName);
			ret = new HashMap[obj.length];
			responseList = new HashMap[obj.length];
			objOut = new HashMap();
			for(int i=0;i<obj.length;i++){
				ret[i] = (HashMap) obj[i];
				objOut.put(ObjName, (HashMap)ret[i].clone());
				responseList[i] = (HashMap)objOut.clone();
			}
		}
		catch (Exception e) {
			//e.printStackTrace();
			logger.error(DebugUtil.getStackTrace(e));
			throw e;
		}
		
		return responseList;
	}
}
