package com.cat.udb.esb.wrappers;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cat.udb.esb.core.kenan.utils.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.BaseWrapper;

import com.cat.ibacss.esb.bo.convert.UpdateAccountConverter;
import com.cat.ibacss.esb.bo.convert.impl.UpdateAccountConverterUDBImpl;
import com.csgsystems.bali.connection.ApiMappings;
import commonj.sdo.DataObject;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;

public class UpdateAccountWrapper extends BaseWrapper {
	
	private static Logger logger = Logger.getLogger(UpdateAccountWrapper.class);
	
	//private static final String PATH_LOG = Constant.LOG4j_PROPERITES_PATH_UDB_A14;
//	private Class CLASS = this.getClass();
//	
//	public UpdateAccountWrapper() {
//		super(Constant.LOG4j_PROPERITES_PATH_UDB_A14);
//	}
	
	@Override
	protected void executeService() throws Throwable {
		//LoggerUtil.debug(PATH_LOG, "###### UpdateAccountWrapper - executeService START ######", CLASS);
		logger.debug("###### UpdateAccountWrapper - executeService START ######");
		
		//Init request message
		//LoggerUtil.debug(PATH_LOG, "###### mapToRequestUpdateHashMap ######", CLASS);
		logger.debug("###### mapToRequestUpdateHashMap ######");
		HashMap mapRetrieveAccount = null;
		request = new HashMap();
		mapRetrieveAccount = mapToRequestServiceHashMap(requestObj);
		request.put("Account", (HashMap)mapRetrieveAccount.get("Account"));

		HashMap accountHM = (HashMap)mapRetrieveAccount.get("Account");
		logger.debug("##### Nothic: Request Account Hashmap = " + accountHM.toString());
		
		
		HashMap requestExt = (HashMap)request.get("Account");
		requestExt.put("ExtendedData", MessageUtil.getExtendedData(requestExt, Constant.ACCOUNT_EXTDATA));
		
		//((HashMap)request.get("Account")).put("ExtendedData", MessageUtil.getExtendedData((HashMap)request.get("Account"), Constant.ACCOUNT_EXTDATA));
		//LoggerUtil.debugHashMap(PATH_LOG, "request", CLASS, request);
		if (logger.isDebugEnabled())
			logger.debug("request " + DebugUtil.getStringBuffer(request).toString());
		
		

		logger.debug("##### Nothic: Account Hashmap in executeService = " + ((HashMap)mapRetrieveAccount.get("Account")));
		//Call Kenan API "AccountUpdate"
		//LoggerUtil.debug(PATH_LOG, "###### Call Kenan API AccountUpdate ######", CLASS);
		logger.debug("###### Call Kenan API AccountUpdate ######");
		HashMap tempRes = connection.call(context, ApiMappings.getCallName("AccountUpdate"), request);
		HashMap APIresponse = new HashMap();
		APIresponse.put("Account", tempRes.get("Account"));
		//LoggerUtil.debugHashMap(PATH_LOG, "APIresponse", CLASS, APIresponse);
		if (logger.isDebugEnabled())
			logger.debug("APIresponse " + DebugUtil.getStringBuffer(APIresponse).toString());
		
		//Call method update customer service center
		//LoggerUtil.debug(PATH_LOG, "###### Call updateAccountDetail ######", CLASS);
		logger.debug("###### Call updateAccountDetail ######");
		HashMap [] customerServiceCenterUpdateResponseList = updateAccountDetail((HashMap)request.get("Account"));
		((HashMap)APIresponse.get("Account")).put("CustomerServiceCenters", customerServiceCenterUpdateResponseList);
		
		//LoggerUtil.debug(PATH_LOG, "###### UpdateAccountWrapper - executeService END ######", CLASS);
		logger.debug("###### UpdateAccountWrapper - executeService END ######");
		
		if (((HashMap)APIresponse.get("Account")).containsKey("ExtendedData")){
			MessageUtil.setExtendedData(((HashMap)APIresponse.get("Account")));
			((HashMap)APIresponse.get("Account")).remove("ExtendedData");
		}
		
		response = APIresponse;
		//LoggerUtil.debugHashMap(PATH_LOG, "APIresponse LAST", CLASS, APIresponse);
		if (logger.isDebugEnabled())
			logger.debug("APIresponse LAST " + DebugUtil.getStringBuffer(APIresponse).toString());
		
		return;
	}

	@Override
	protected DataObject prepareResponseObj(DataObject reqObj, HashMap response, Throwable error) throws Exception {
		MessageUtil msgHelper = new MessageUtil();
		HashMap result = msgHelper.getResult(error);
		String transactionLogId = msgHelper.getTransactionLogId(reqObj);
		DataObject responseObj = mapToResponseServiceDataObj(response, result, transactionLogId);
		
		return responseObj;
	}
	
	protected HashMap [] updateAccountDetail (HashMap reqAccount) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "----------- UpdateAccountWrapper - updateAccountDetail ----------", CLASS);
		logger.debug("----------- UpdateAccountWrapper - updateAccountDetail ----------");
		//LoggerUtil.debugHashMap(PATH_LOG, "reqAccount", CLASS, reqAccount);
		if (logger.isDebugEnabled())
			logger.debug("reqAccount " + DebugUtil.getStringBuffer(reqAccount).toString());
		
		HashMap [] customerServiceCenterUpdateListResponse = null;
		try {
			int csvSize = ((HashMap[])reqAccount.get("CustomerServiceCenters")).length;
			if (csvSize > 0){
				
	/*			edit by pae 2013/12/2 p'ta p'tong  crm send ISP not to check account category 6 p'nu comment edit this part 
				if ( Integer.parseInt(reqAccount.get("AccountCategory").toString()) == 2) {
					LoggerUtil.debug(PATH_LOG, "Case AccountCategory = 2", CLASS);
					if (reqAccount.get("GovernmentType") != null && reqAccount.get("GovernmentId") != null ) {
						LoggerUtil.debug(PATH_LOG, "Call updateCustomerService", CLASS);
						customerServiceCenterUpdateListResponse = new HashMap [csvSize+1];
						customerServiceCenterUpdateListResponse = updateCustomerService(reqAccount, customerServiceCenterUpdateListResponse, csvSize);
						
						HashMap cscKey = new HashMap();
						cscKey.put("AccountInternalId", ((HashMap)reqAccount.get("Key")).get("AccountInternalId"));
						cscKey.put("ServiceCenterType", reqAccount.get("GovernmentType"));
						
						HashMap csc = new HashMap();
						csc.put("Key", cscKey);
						csc.put("ServiceCenterId", Integer.parseInt(reqAccount.get("GovernmentId").toString()));
						
						LoggerUtil.debug(PATH_LOG, "Call the last API : CustomerServiceCenterUpdate", CLASS);
						LoggerUtil.debugHashMap(PATH_LOG, "csc", CLASS, csc);
						HashMap customerServiceCenterUpdateResponse = connection.call(context, ApiMappings.getCallName("CustomerServiceCenterUpdate"), csc);
						customerServiceCenterUpdateListResponse[csvSize].put("CustomerServiceCenter", customerServiceCenterUpdateResponse.get("CustomerServiceCenter"));
						LoggerUtil.debugHashMap(PATH_LOG, "customerServiceCenterUpdateListResponse["+csvSize+"]", CLASS, customerServiceCenterUpdateListResponse[csvSize]);
					
					}else{
						LoggerUtil.error(PATH_LOG, "Exception UpdateAccountWrapper: AccountCategoryType 2 is require Account.GovernmentId and Account.GovernmentType ", CLASS);
						throw new Exception("AccountCategoryType 2 is require Account.GovernmentId and Account.GovernmentType");
					}
					
				}else{
					LoggerUtil.debug(PATH_LOG, "Case AccountCategory <> 2", CLASS);
					LoggerUtil.debug(PATH_LOG, "Call updateCustomerService", CLASS);
					customerServiceCenterUpdateListResponse = new HashMap [csvSize];
					customerServiceCenterUpdateListResponse = updateCustomerService(reqAccount, customerServiceCenterUpdateListResponse, csvSize);

				}
				*/
				
				customerServiceCenterUpdateListResponse = new HashMap [csvSize];
				customerServiceCenterUpdateListResponse = updateCustomerService(reqAccount, customerServiceCenterUpdateListResponse, csvSize);
				
				//LoggerUtil.debug(PATH_LOG, "----------- UpdateAccountWrapper - customerServiceCenterUpdateListRequest Completed----------", CLASS);
				logger.debug("----------- UpdateAccountWrapper - customerServiceCenterUpdateListRequest Completed----------");
				
				return customerServiceCenterUpdateListResponse;
			}
			else {
				return null;
			}			
		}
		catch (Exception e) {
			//LoggerUtil.error(PATH_LOG, "Exception UpdateAccountWrapper updateAccountDetail : "+e.getMessage(), CLASS);
			logger.error(DebugUtil.getStackTrace(e));
			throw e;
		}		
	}
	
	private HashMap [] updateCustomerService (HashMap reqAccount, HashMap [] customerServiceCenterUpdateListResponse, int csvSize) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "###### UpdateAccountWrapper - updateCustomerService ######", CLASS);
		logger.debug("###### UpdateAccountWrapper - updateCustomerService ######");
		
		try {
			HashMap [] customerServiceCenterUpdate = (HashMap[])reqAccount.get("CustomerServiceCenters");
			HashMap customerServiceCenterUpdateResponse = new HashMap();
			//LoggerUtil.debug(PATH_LOG, "----- Loop call API : CustomerServiceCenterUpdate, loop count : "+csvSize+" ------ ", CLASS);
			logger.debug("----- Loop call API : CustomerServiceCenterUpdate, loop count : "+csvSize+" ------ ");
			
			for (int i=0; i<csvSize; i++){
				//LoggerUtil.debug(PATH_LOG, "Loop : "+i, CLASS);
				logger.debug("Loop : "+i);
				//LoggerUtil.debug(PATH_LOG, "((HashMap)reqAccount.get(Key)).get(AccountInternalId)"+((HashMap)reqAccount.get("Key")).get("AccountInternalId"), CLASS);
				logger.debug("((HashMap)reqAccount.get(Key)).get(AccountInternalId)"+((HashMap)reqAccount.get("Key")).get("AccountInternalId"));
				//LoggerUtil.debugHashMap(PATH_LOG, "((HashMap)customerServiceCenterUpdate[i].get(Key))", CLASS, ((HashMap) ((HashMap)customerServiceCenterUpdate[i].get("CustomerServiceCenter")).get("Key")));
				if (logger.isDebugEnabled())
					logger.debug("((HashMap)customerServiceCenterUpdate[i].get(Key)) " + DebugUtil.getStringBuffer(((HashMap) ((HashMap)customerServiceCenterUpdate[i].get("CustomerServiceCenter")).get("Key"))).toString());
				
				((HashMap) ((HashMap)customerServiceCenterUpdate[i].get("CustomerServiceCenter")).get("Key")).put("AccountInternalId", ((HashMap)reqAccount.get("Key")).get("AccountInternalId"));
				
				//LoggerUtil.debugHashMap(PATH_LOG, "customerServiceCenterUpdate[i]", CLASS, customerServiceCenterUpdate[i]);
				if (logger.isDebugEnabled())
					logger.debug("customerServiceCenterUpdate[i] " + DebugUtil.getStringBuffer(customerServiceCenterUpdate[i]).toString());
				
				customerServiceCenterUpdateResponse = connection.call(context, ApiMappings.getCallName("CustomerServiceCenterUpdate"), customerServiceCenterUpdate[i]);
				
				//LoggerUtil.debugHashMap(PATH_LOG, " ### customerServiceCenterUpdateResponse ###", CLASS, customerServiceCenterUpdateResponse);
				if (logger.isDebugEnabled())
					logger.debug(" ### customerServiceCenterUpdateResponse ### " + DebugUtil.getStringBuffer(customerServiceCenterUpdateResponse).toString());
				
				customerServiceCenterUpdateListResponse[i] = new HashMap();
				customerServiceCenterUpdateListResponse[i].put("CustomerServiceCenter", customerServiceCenterUpdateResponse.get("CustomerServiceCenter"));
				//LoggerUtil.debugHashMap(PATH_LOG, "customerServiceCenterUpdateListResponse["+i+"]", CLASS, customerServiceCenterUpdateListResponse[i]);
				if (logger.isDebugEnabled())
					logger.debug("customerServiceCenterUpdateListResponse["+i+"] " + DebugUtil.getStringBuffer(customerServiceCenterUpdateListResponse[i]).toString());
				customerServiceCenterUpdateResponse = null;
			}
			return customerServiceCenterUpdateListResponse;
		}
		catch (Exception e) {
			//LoggerUtil.error(PATH_LOG, "Exception UpdateAccountWrapper updateCustomerService : "+e.getMessage(), CLASS);
			logger.error(DebugUtil.getStackTrace(e));
			throw e;
		}	
	}
	
	public HashMap mapToRequestServiceHashMap(DataObject requestObj) throws Exception {
		HashMap requestHashMap = null;
		try {
			UpdateAccountConverter upConverter = new UpdateAccountConverterUDBImpl();
			requestHashMap = upConverter.dataObject2HashMap(requestObj);
		}
		catch (Exception e) {
			//LoggerUtil.error(PATH_LOG, "Exception : UpdateAccountWrapperImpl = mapUpdateAccountObject ---> "+e.getMessage(), CLASS);
			logger.error(DebugUtil.getStackTrace(e));
			throw e;
		}
		return requestHashMap;
	}
	
	private DataObject mapToResponseServiceDataObj(HashMap response, HashMap result, String transactionLogId) throws Exception {
		
		HashMap responseHashMap = new HashMap();
		DataObject responseObj;
		
		MessageUtil msgHelper = new MessageUtil();
		
		if (response != null){
			responseHashMap.put("Account", (HashMap)response.get("Account"));
		}
		else{
			responseHashMap.put("Account", request);
		}
		
		responseHashMap.put("Result", result);
		responseHashMap.put("TransactionLogId", transactionLogId);
		
		//LoggerUtil.debug(PATH_LOG, "#### mapToResponseServiceDataObj : ####", CLASS);
		logger.debug("#### mapToResponseServiceDataObj : ####");
		//LoggerUtil.debugHashMap(PATH_LOG, "#### mapToResponseServiceDataObj : ####", CLASS,responseHashMap);
		if (logger.isDebugEnabled())
			logger.debug("#### mapToResponseServiceDataObj : #### " + DebugUtil.getStringBuffer(responseHashMap).toString());
		
		UpdateAccountConverter UpdateAccounConvert = new UpdateAccountConverterUDBImpl();
		DataObject requestObj = UpdateAccounConvert.hashMapUpdate2DataObject(responseHashMap);
		
		return requestObj;
	}
}
