package com.cat.udb.esb.core.kenan.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import org.apache.log4j.Logger;

import terrapin.tuxedo.TuxError;

import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.csgsystems.aruba.connection.BSDMSettings;
import com.csgsystems.aruba.connection.Connection;
import com.csgsystems.aruba.connection.ConnectionFactory;
import com.csgsystems.aruba.connection.ServiceException;
import com.csgsystems.bali.connection.ApiMappings;
import com.csgsystems.fx.security.SecurityManager;
import com.csgsystems.fx.security.SecurityManagerFactory;
import com.csgsystems.fx.security.util.AuthenticationException;

import com.cat.udb.esb.core.kenan.config.Authentication;
import com.cat.udb.esb.core.kenan.config.Context;
import com.cat.udb.esb.core.kenan.config.FileProperty;

import com.cat.udb.esb.core.utils.DebugUtil;

public class ConnectionManager {
	
	private static Logger logger = Logger.getLogger(ConnectionManager.class);
	private static SecurityManager sm;
	private static Properties prop = null;
	private static HashMap trigConnection = new HashMap();
	private static HashMap trigConnectionField = new HashMap();
	private static HashMap trigConnectionKey = new HashMap();
	
	private static synchronized void loadConfiguration() throws IOException{
		if (prop == null){
			prop = new Properties();
			prop.load(ClassLoader.getSystemClassLoader().getResourceAsStream(FileProperty.WRAPPER_API));
		}
	}
	
	private static synchronized void loadConfiguration(String fileName) throws IOException{
		if(prop == null){
			prop = new Properties();
			prop.load(ClassLoader.getSystemClassLoader().getResourceAsStream(fileName));
		}
	}
	
	public static BSDMSettings getBSDMSetting(){
		return BSDMSettings.getDefault();
	}
	
	public static synchronized SecurityManager getAuthentication() throws Throwable {
		if(sm != null){
			logger.info("Already login " + sm.hashCode());
			return sm;
		}else{
			logger.info("Waiting for login...");
			logger.debug("Waiting for Create SM ... ");
			sm = SecurityManagerFactory.createSecurityManager(prop.getProperty(Authentication.USER_REALM), prop.getProperty(Authentication.USER_NAME), null, prop.getProperty(Authentication.USER_PSWD));
			logger.debug("Create SM Completed ... ");
			try{
				sm.authenticate();
			}catch (AuthenticationException e) {
				e.printStackTrace();
			}
			logger.info("Login Completed " + sm.hashCode());
		}
		return sm;
	}
	
	public static BSDMSessionContext getSession() throws Throwable {
		return getSession(2);
	}
	
	public static BSDMSessionContext getSession(int serverId) throws Throwable{
			BSDMSessionContext context = new BSDMSessionContext();
			context = BSDMSessionContext.getDefaultContext();
			context.setSecurityContext(getAuthentication());
			context.setOperatorName(prop.getProperty(Context.CONTEXT_OPERNAME_DEFAULT));
			context.setBlockSize(Integer.valueOf(prop.getProperty(Context.CONTEXT_BLOCKSIZE)));
			context.setExecutionTraceFlag(Boolean.valueOf(prop.getProperty(Context.CONTEXT_BLOCKSIZE)));
			context.setServerId(serverId);
			return context;
	}
	
	public static BSDMSessionContext getSession(int serverId, String operName) throws Throwable{
		BSDMSessionContext context = new BSDMSessionContext();
		context = BSDMSessionContext.getDefaultContext();
		context.setSecurityContext(getAuthentication());
		context.setBlockSize(Integer.valueOf(prop.getProperty(Context.CONTEXT_BLOCKSIZE)));
		context.setExecutionTraceFlag(Boolean.valueOf(prop.getProperty(Context.CONTEXT_BLOCKSIZE)));
		context.setExecutionTraceFlag(false);
		context.setServerId(serverId);
		return context;
}

	public static Connection getConnection() throws Throwable {
		Connection connection = null;
		try {
			// 1.	Loading Configuration from wrapper_api.properties
			loadConfiguration();
			
			// 2.	Authentication with Security Manager Server
			getAuthentication();
			
			// 3.	Get Connection with default CONTEXT
			connection = ConnectionFactory.instance().createConnection(BSDMSettings.getDefault());
			logger.info("Waiting for check connection [ "+ connection.hashCode() +" ] ..... ");
			logger.debug("Command Name : " + ApiMappings.getCallName("ServerGet"));
			logger.debug("Request : " + getServerId());
			connection.call(ApiMappings.getCallName("ServerGet"), getServerId());
			logger.info("Connection available [ "+ connection.hashCode() +" ]");
			return connection;
		}
		catch (TuxError e) {
			boolean isAvailable = false;
			int i = 3;
			String localizedMessage = null;
			while (i < Integer.valueOf(prop.getProperty(com.cat.udb.esb.core.kenan.config.Connection.CONNECTION_RETRY))) {
				try{
					Thread.sleep(1000);
					connection = ConnectionFactory.instance().createConnection(BSDMSettings.getDefault());
					connection.call(ApiMappings.getCallName("ServerGet"), getServerId());
					logger.info("Connection available [ "+ connection.hashCode() +" ]");
					isAvailable = true;
					break;
				}
				catch(TuxError e1){
					localizedMessage = "TuxError: " + e1.getLocalizedMessage();
					logger.info("No Connection available [ "+ connection.hashCode() +" ]: " + localizedMessage);
					isAvailable = false;
					i = i+1;
				}
				catch (InterruptedException e1) {
					localizedMessage = "InterruptedException: " + e1.getLocalizedMessage();
					logger.info("No Connection available [ "+ connection.hashCode() +" ]: " + localizedMessage);
					isAvailable = false;
					i = i+1;
				}
				catch(ServiceException e1){
					logger.info("Connection available [ "+ connection.hashCode() +" ]");
					isAvailable = true;
					break;
				}
			}
			
			/*
			 * After trying to connect
			 */
			if (isAvailable) {
				return connection;
			}
			else {
				logger.info("No Connection available [ "+ connection.hashCode() +" ]: " + isAvailable);
				throw new Throwable(localizedMessage);
			}
		}
		catch (ServiceException e){
			logger.warn(DebugUtil.getStackTrace(e));
			logger.info("Connection available [ "+ connection.hashCode() +" ]");
			return connection;
		}
		catch (Exception e) {
			logger.error(DebugUtil.getStackTrace(e));
			throw e;
		}
	}
	
	public static Connection getConnection(String fileName) throws Throwable {
		Connection connection = null;
		try{
			// 1.	Loading Configuration from wrapper_api.properties
			loadConfiguration(fileName);
			
			// 2.	Authentication with Security Manager Server
			getAuthentication();
			
			// 3.	Get Connection with default CONTEXT
			connection = ConnectionFactory.instance().createConnection(BSDMSettings.getDefault());
			logger.info("Waiting for check connection [ "+ connection.hashCode() +" ] ..... ");
			logger.debug("Command Name : " + ApiMappings.getCallName("ServerGet"));
			logger.debug("Request : " + getServerId());
			connection.call(ApiMappings.getCallName("ServerGet"), getServerId());
			logger.info("Connection available [ "+ connection.hashCode() +" ]");
			return connection;
		}catch(TuxError e){
			boolean isAvailable = false;
			int i = 3;
			String localizedMessage = null;
			while(i < Integer.valueOf(prop.getProperty(com.cat.udb.esb.core.kenan.config.Connection.CONNECTION_RETRY))){
				try{
					Thread.sleep(1000);
					connection = ConnectionFactory.instance().createConnection(BSDMSettings.getDefault());
					connection.call(ApiMappings.getCallName("ServerGet"), getServerId());
					logger.info("Connection available [ "+ connection.hashCode() +" ]");
					isAvailable = true;
					break;
				}catch(TuxError e1){
					localizedMessage = "TuxError: " + e1.getLocalizedMessage();
					logger.info("No Connection available [ "+ connection.hashCode() +" ]: " + localizedMessage);
					isAvailable = false;
					i = i+1;
				} catch (InterruptedException e1) {
					localizedMessage = "InterruptedException: " + e1.getLocalizedMessage();
					logger.info("No Connection available [ "+ connection.hashCode() +" ]: " + localizedMessage);
					isAvailable = false;
					i = i+1;
				}catch(ServiceException e1){
					logger.info("Connection available [ "+ connection.hashCode() +" ]");
					isAvailable = true;
					break;
				}
			}
			
			/*
			 * After trying to connect
			 */
			if(isAvailable){
				return connection;
			}else{
				logger.info("No Connection available [ "+ connection.hashCode() +" ]: " + isAvailable);
				throw new Throwable(localizedMessage);
			}
		}catch(ServiceException e){
			logger.info("Connection available [ "+ connection.hashCode() +" ]");
			return connection;
		}
	}
	
	private static HashMap getServerId(){
		trigConnectionKey.put("ServerId", 3);
		trigConnectionField.put("Key", trigConnectionKey);
		trigConnection.put("Server", trigConnectionField);
		return trigConnection;
	}

}
