package com.cat.udb.esb.wrappers;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cat.ibacss.esb.bo.convert.PaymentConverter;
import com.cat.ibacss.esb.bo.convert.impl.PaymentConverterImpl;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.bali.connection.ApiMappings;
import commonj.sdo.DataObject;

//import com.cat.udb.esb.core.kenan.utils.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.BaseWrapper;

public class DeletePaymentWrapper extends BaseWrapper {
	
	private static Logger logger = Logger.getLogger(DeletePaymentWrapper.class);
	
//	private Class CLASS = this.getClass();
//	
//	public DeletePaymentWrapper() {
//		super(Constant.LOG4j_PROPERTIES_PATH_UDB_F12);
//	}
	
	@Override
	protected void executeService() throws Throwable {
		//Init request message
		request = mapToRequestHashMap(requestObj);
		//LoggerUtil.debugHashMap(PATH_LOG,"###### DeletePayment Request ######" , CLASS, request);
		if (logger.isDebugEnabled())
			logger.debug("###### DeletePayment Request ###### " + DebugUtil.getStringBuffer(request).toString());
		
		//Call Kenan API
		HashMap APIresponse = connection.call(context, ApiMappings.getCallName("PaymentDelete"), request);
		//LoggerUtil.debugHashMap(PATH_LOG,"###### DeletePayment Response ######" , CLASS, APIresponse);
		if (logger.isDebugEnabled())
			logger.debug("###### DeletePayment Response ###### " + DebugUtil.getStringBuffer(request).toString());
		
		response = APIresponse;

		return;
	}
	
	private HashMap mapToRequestHashMap(DataObject requestObj) throws Exception{
		//Return Order  
		PaymentConverter reConverter = new PaymentConverterImpl();
		HashMap requestHashMap = reConverter.dataObjectReverse2HashMap(requestObj);
		
		//return requestHashMap;
		return requestHashMap;
	}
	
	public DataObject prepareResponseObj(DataObject reqObj, HashMap response, Throwable error) throws Exception{
		HashMap result = MessageUtil.getResult(error);
		String transactionLogId = MessageUtil.getTransactionLogId(reqObj);
		DataObject responseObj = mapToDataObj(response, result, transactionLogId);    //obtaining the response object
		
		return responseObj;
		
	}
	private DataObject mapToDataObj(HashMap response, HashMap result, String transactionLogId) throws Exception {
		
		HashMap responseHashMap = new HashMap();
		HashMap paymentHashMap = new HashMap();
		DataObject responseObj;
		
		
		responseHashMap.put("Result", result);
		responseHashMap.put("Payment", paymentHashMap);		//inserting an empty payment hashmap to struture the hashmap into the correct format
		
		if (response != null){
			responseHashMap.putAll(response);
		}else{
			responseHashMap.put("Account", request);
		}
		
		responseHashMap.put("TransactionLogId", transactionLogId);
		
		//LoggerUtil.debugHashMap(PATH_LOG,"###### mapToResponseReverseDataObj ######" , CLASS, responseHashMap);
		if (logger.isDebugEnabled())
			logger.debug("###### mapToResponseReverseDataObj ###### " + DebugUtil.getStringBuffer(responseHashMap).toString());	
		
		PaymentConverter reConverter = new PaymentConverterImpl();
		DataObject requestObj = reConverter.hashMapCreate2DataObject(responseHashMap);
		
		return requestObj;
	}
}
