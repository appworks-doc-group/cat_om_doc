package com.cat.udb.esb.wrappers.common;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.cat.udb.esb.core.kenan.utils.ConverterUtil;
//import util.HashMapHelper;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;
import com.cat.udb.esb.core.utils.DebugUtil;

//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.csgsystems.aruba.connection.Connection;
import com.csgsystems.bali.connection.ApiMappings;

public class ProductRateKeyImpl {
	
	private static Logger logger = Logger.getLogger(ProductRateKeyImpl.class);

	//public static HashMap createProductRateKey(HashMap product, HashMap productRateKeyField, 
	//	Connection connection, BSDMSessionContext context, String PATH_LOG) throws Throwable{
	public static HashMap createProductRateKey(HashMap product, HashMap productRateKeyField, 
		Connection connection, BSDMSessionContext context) throws Throwable{
		
		//Init request
		HashMap productRateKeyKey = new HashMap();
		if (product != null) {
			productRateKeyKey.put("ActiveDt", product.get("ProductActiveDt"));
			productRateKeyKey.put("TrackingId", product.get("TrackingId"));
			productRateKeyKey.put("TrackingIdServ", product.get("TrackingIdServ"));
			productRateKeyField.put("Key", productRateKeyKey);
		}
		
		HashMap productRateKey = new HashMap();
		productRateKey.put("ProductRateKey", productRateKeyField);
		
		//Call API
		//LoggerUtil.debugHashMap(PATH_LOG,"###### ProductRateKeyCreate Request ######" , ProductRateKeyImpl.class, productRateKey);
		if (logger.isDebugEnabled())
			logger.debug("###### ProductRateKeyCreate Request ###### " + DebugUtil.getStringBuffer(productRateKey).toString());
		HashMap productRateKeyCreateResponse = connection.call(context, ApiMappings.getCallName("ProductRateKeyCreate"), productRateKey);
		
		//Set ViewId
		HashMap productRateKeyCreateResponseField =  (HashMap)productRateKeyCreateResponse.get("ProductRateKey");	
		if (productRateKeyKey == null)
			productRateKeyKey = (HashMap)productRateKeyCreateResponseField.get("Key");
		Object viewId = MessageUtil.getProductRateViewId(productRateKeyKey);		
		productRateKeyCreateResponseField.put("ViewId", viewId);

		//LoggerUtil.debugHashMap(PATH_LOG,"###### ProductRateKeyCreate Response ######" , ProductRateKeyImpl.class, productRateKeyCreateResponse);
		if (logger.isDebugEnabled())
			logger.debug("###### ProductRateKeyCreate Response ###### " + DebugUtil.getStringBuffer(productRateKeyCreateResponse).toString());
		
		return productRateKeyCreateResponse;
	}
	
	//public static HashMap deleteProductRateKey(HashMap productRateKey, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Throwable {
	public static HashMap deleteProductRateKey(HashMap productRateKey, Connection connection, BSDMSessionContext context) throws Throwable {		
		if (productRateKey != null) {
			HashMap productRateKeyResponse = connection.call(context, ApiMappings.getCallName("ProductRateKeyDelete"), productRateKey);
			return productRateKeyResponse;
		}
		else{
			return null;
		}
	}
	
//	public static HashMap modifyProductRateKey(HashMap productRateKey, Connection connection, 
//			BSDMSessionContext context, String PATH_LOG) throws Throwable{
	public static HashMap modifyProductRateKey(HashMap productRateKey, Connection connection, BSDMSessionContext context) throws Throwable{
		
		if(productRateKey != null){
			String actionType = productRateKey.get("ActionType").toString();
			
			int trackingId = Integer.parseInt(((HashMap)productRateKey.get("Key")).get("TrackingId").toString());
			int trackingIdServ = Integer.parseInt(((HashMap)productRateKey.get("Key")).get("TrackingIdServ").toString());
			HashMap productRateKeyFindData = productRateKeyFindData(trackingId, trackingIdServ);
			
			List productRateKeyFindResponse = ConverterUtil.getObjectList(connection.call(context, ApiMappings.getCallName("ProductRateKeyFind"), productRateKey), "ProductRateKeyList", "ProductRateKey" );
			
			HashMap productRateKeyResponse = null;
			if(actionType.equals("CREATE")){
				if(productRateKeyFindResponse.isEmpty()){
					productRateKey.remove("InactiveDate");
					//productRateKeyResponse = createProductRateKey(null, productRateKey, connection, context, PATH_LOG);
					productRateKeyResponse = createProductRateKey(null, productRateKey, connection, context);
				}
				else if(productRateKeyFindResponse.size() > 1){
					throw new Throwable("ProductRateKey of Product.TrackingId and Product.TrackingIdServ is existing in system");
				}
			}
			else if(actionType.equals("UPDATE")){
				if(productRateKeyFindResponse.isEmpty()){
					throw new Throwable("Not foud ProductRateKey of Product.TrackingId and Product.TrackingIdServ");
				}
				else if(productRateKeyFindResponse.size() > 1){
					throw new Throwable("ProductRateKey of Product.TrackingId and Product.TrackingIdServ cannot active more than 1 record");
				}
				else{
					productRateKeyResponse = (HashMap)productRateKeyFindResponse.get(0);
					
					// Delete
					productRateKeyResponse.put("InactiveDate", (Date)productRateKey.get("InactiveDate"));
					//deleteProductRateKey(productRateKeyResponse, connection, context, PATH_LOG);
					deleteProductRateKey(productRateKeyResponse, connection, context);
					productRateKeyResponse = null;
					
					// Create
					productRateKey.remove("InactiveDate");
					//productRateKeyResponse = createProductRateKey(null, productRateKey, connection, context, PATH_LOG);
					productRateKeyResponse = createProductRateKey(null, productRateKey, connection, context);
				}
			}
			else if(actionType.equals("DELETE")){
				if(productRateKeyFindResponse.isEmpty()){
					throw new Throwable("Not foud Product.TrackingId and Product.TrackingIdServ");
				}
				else if(productRateKeyFindResponse.size() > 1){
					throw new Throwable("Product.TrackingId and Product.TrackingIdServ cannot active more than 1 record");
				}
				else{
					productRateKeyResponse = (HashMap)productRateKeyFindResponse.get(0);
					
					// Delete
					productRateKeyResponse.put("InactiveDate", (Date)productRateKey.get("InactiveDate"));
					//productRateKeyResponse = deleteProductRateKey(productRateKeyResponse, connection, context, PATH_LOG);
					productRateKeyResponse = deleteProductRateKey(productRateKeyResponse, connection, context);
				}
			}
			else{
				throw new Throwable("ProductRateKey.ActionType doesn't match, ActionType[CREATE|UPDATE|DELETE]");
			}
			return productRateKeyResponse;
		}
		else{
			return null;
		}	
	}
	
//	public static HashMap productRateKeyProcessing(HashMap order, HashMap product , HashMap productRateKeyRequest, 
//			Connection connection, BSDMSessionContext context, String PATH_LOG)  throws Throwable{
	public static HashMap productRateKeyProcessing(HashMap order, HashMap product , HashMap productRateKeyRequest, Connection connection, BSDMSessionContext context)  throws Throwable{
		
		if(productRateKeyRequest != null){

			if(product != null){
				HashMap productRateKeyKey = new HashMap();
				productRateKeyKey.put("ActiveDt", (Date)product.get("ProductActiveDt"));
				productRateKeyKey.put("TrackingId", Integer.parseInt(product.get("TrackingId").toString()));
				productRateKeyKey.put("TrackingIdServ", Integer.parseInt(product.get("TrackingIdServ").toString()));
				
				productRateKeyRequest.put("Key", productRateKeyKey);
				productRateKeyRequest.put("InactiveDate", (Date)product.get("ProductInactiveDt"));
				productRateKeyKey = null;
			}
			
			//HashMap productRateKeyResponse = modifyProductRateKey(productRateKeyRequest, connection, context, PATH_LOG);
			HashMap productRateKeyResponse = modifyProductRateKey(productRateKeyRequest, connection, context);
			
			return productRateKeyResponse;
		}
		else{
			return null;
		}
	}
	
	public static HashMap productRateKeyFindData(Integer trackingId, Integer trackingIdServ) throws Throwable{
		HashMap condition = new HashMap();
		HashMap key = new HashMap();
		HashMap field = new HashMap();
		HashMap request = new HashMap();

		condition.put("Equal", trackingId);
		key.put("TrackingId", (HashMap)condition.clone());
		
		condition.clear();
		condition.put("Equal", trackingIdServ);
		key.put("TrackingIdServ", (HashMap)condition.clone());
		key.put("Fetch", true);
		field.put("Key", (HashMap)key.clone());
		
		condition.clear();
		condition.put("IsNull", true);
		field.put("InactiveDt", (HashMap)condition.clone());
		field.put("Fetch", true);
		request.put("ProductRateKey", (HashMap)field.clone());
		
		return request;

	}
	
}
