package com.cat.udb.esb.wrappers;

import java.util.HashMap;

import org.apache.log4j.Logger;

//import util.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.OrderedServiceBaseWrapper;

//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.bali.connection.ApiMappings;

public class SuspendServiceWrapper extends OrderedServiceBaseWrapper {
	
	private static Logger logger = Logger.getLogger(SuspendServiceWrapper.class);
//	private Class CLASS = this.getClass();
//	
//	public SuspendServiceWrapper() {
//		super(Constant.LOG4j_PROPERTIES_PATH_UDB_A3);
//	}

	@Override
	protected void executeOrderedService() throws Exception {
		HashMap orderedService = MessageUtil.getOrderedService(request);
		//LoggerUtil.debugHashMap(PATH_LOG,"###### Call OrderedServiceSuspend ######" , CLASS, orderedService);
		if (logger.isDebugEnabled())
			logger.debug("###### Call OrderedServiceSuspend ###### " + DebugUtil.getStringBuffer(orderedService).toString());
		HashMap APIresponse = connection.call(context, ApiMappings.getCallName("OrderedServiceSuspend"), orderedService);
		//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedServiceSuspend Response ######" , CLASS, APIresponse);
		if (logger.isDebugEnabled())
			logger.debug("###### OrderedServiceSuspend Response ###### " + DebugUtil.getStringBuffer(APIresponse).toString());
		
		response = MessageUtil.setOrderedService(APIresponse, request);
		
		return;
	}
}
