package com.cat.udb.esb.wrappers;

import java.util.HashMap;

import org.apache.log4j.Logger;

//import util.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.BaseWrapper;

import com.cat.ibacss.esb.bo.convert.InvoiceConverter;
import com.cat.ibacss.esb.bo.convert.impl.InvoiceConverterImpl;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.bali.connection.ApiMappings;
import commonj.sdo.DataObject;

public class RetrieveInvoiceWrapper extends BaseWrapper {
	
	private static Logger logger = Logger.getLogger(RetrieveInvoiceWrapper.class);
//	private Class CLASS = this.getClass();
//	
//	public RetrieveInvoiceWrapper(){
//		super(Constant.LOG4j_PROPERTIES_PATH_UDB_F10);
//	}

	@SuppressWarnings("unchecked")
	@Override
	protected void executeService() throws Throwable {
		//LoggerUtil.debug(PATH_LOG, "#### start executeService() ####", CLASS);
		System.out.println("#### start executeService() ####");

		HashMap requestHashMap = new HashMap();
		HashMap responseHashMap = new HashMap();
		HashMap account = new HashMap();
		HashMap invoice = new HashMap();
		HashMap returnKeyInvoice = new HashMap();
		HashMap keyInvoice = new HashMap();
		MessageUtil msgUtil = new MessageUtil();
		
		requestHashMap = mapToRequestServiceHashMap(requestObj);
		
		returnKeyInvoice = (HashMap)requestHashMap.get("Invoice");
		keyInvoice.put("Key", returnKeyInvoice.get("Key"));
		keyInvoice.put("Fetch", true);
		invoice.put("Invoice", (HashMap)keyInvoice.clone());
		//LoggerUtil.debugHashMap(PATH_LOG, "InvoiceRequest", CLASS, invoice);
		if (logger.isDebugEnabled())
			System.out.println("InvoiceRequest " + DebugUtil.getStringBuffer(invoice).toString());

		responseHashMap = connection.call(context, ApiMappings.getCallName("InvoiceGet"), (HashMap)invoice.clone());
		
		response = responseHashMap;
		
		//LoggerUtil.debug(PATH_LOG, "#### end executeService() ####", CLASS);
		System.out.println("#### end executeService() ####");
	}

	@SuppressWarnings("unchecked")
	@Override
	protected DataObject prepareResponseObj(DataObject reqObj, HashMap response, Throwable error) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "#### start prepareResposneObj() ####", CLASS);
		System.out.println("#### start prepareResposneObj() ####");
		DataObject responseObj;
		if(response == null){
			response = new HashMap();
		}
		HashMap result = MessageUtil.getResult(error);
		response.put("Result", result);
		response.put("TransactionLogId", MessageUtil.getTransactionLogId(reqObj));
		responseObj = mapToResponseServiceDataObj(response);
		
		//LoggerUtil.debug(PATH_LOG, "#### end prepareResponseObj() ####", CLASS);
		System.out.println("#### end prepareResponseObj() ####");
		
		return responseObj;
	}
	
	private HashMap mapToRequestServiceHashMap(DataObject requestObj) throws Exception{
		//LoggerUtil.debug(PATH_LOG, "#### start maptToRequestServiceHashMap() ####", CLASS);
		System.out.println("#### start maptToRequestServiceHashMap() ####");
		
		HashMap response = new HashMap();
		InvoiceConverter converter = new InvoiceConverterImpl();
		response = converter.dataObject2HashMapInvoice(requestObj);
		//LoggerUtil.debugHashMap(PATH_LOG, "RetrieveInvoiceRequest", CLASS, response);
		if (logger.isDebugEnabled())
			System.out.println("RetrieveInvoiceRequest " + DebugUtil.getStringBuffer(response).toString());
		
		//LoggerUtil.debug(PATH_LOG, "#### end maptToRequestServiceHashMap() ####", CLASS);
		System.out.println("#### end maptToRequestServiceHashMap() ####");
		
		return response;
	}
	
	private DataObject mapToResponseServiceDataObj(HashMap responseHashMap) throws Exception{
		//LoggerUtil.debug(PATH_LOG, "#### start mapToResponseServiceDataObj() ####", CLASS);
		System.out.println("#### start mapToResponseServiceDataObj() ####");
		DataObject response;
		
		//LoggerUtil.debugHashMap(PATH_LOG, "RetrieveInvoiceResponse", CLASS, responseHashMap);
		if (logger.isDebugEnabled())
			System.out.println("RetrieveInvoiceResponse " + DebugUtil.getStringBuffer(responseHashMap).toString());
		InvoiceConverter converter = new InvoiceConverterImpl();
		response = converter.hashMap2DataObjectInvoice(responseHashMap);
		
		//LoggerUtil.debug(PATH_LOG, "#### end mapToResponseServiceDataObj() ####", CLASS);
		System.out.println("#### end mapToResponseServiceDataObj() ####");
		return response;
	}
}
