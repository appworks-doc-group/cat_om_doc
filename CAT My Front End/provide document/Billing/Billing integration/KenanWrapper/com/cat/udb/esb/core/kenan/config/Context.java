package com.cat.udb.esb.core.kenan.config;

public interface Context {

	public static final String CONTEXT_OPERNAME_DEFAULT = "CONTEXT_OPERNAME_DEFAULT";
	public static final String CONTEXT_BLOCKSIZE = "CONTEXT_BLOCKSIZE";
	public static final String CONTEXT_EXECUTION_TRACE_FLAG = "CONTEXT_EXECUTION_TRACE_FLAG";
	
}
