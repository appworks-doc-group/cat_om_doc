package com.cat.udb.esb.wrappers;

import java.util.HashMap;

import org.apache.log4j.Logger;

//import util.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.BaseWrapper;

import com.cat.udb.esb.wrappers.common.UpdateServiceComponentImpl;
import com.cat.ibacss.esb.bo.convert.UpdateServiceComponentConverter;
import com.cat.ibacss.esb.bo.convert.impl.UpdateServiceComponentConverterImpl;
import com.cat.ibacss.esb.bo.convert.impl.UpdateServiceComponentConverterUDBImpl;
import com.cat.ibacss.esb.bo.convert.SimpleConverter;
import com.ibm.websphere.bo.BOFactory;

import commonj.sdo.DataObject;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;

public class UpdateServiceComponentWrapper extends BaseWrapper {
	
	private static Logger logger = Logger.getLogger(UpdateServiceComponentWrapper.class);
	
	//private static final String PATH_LOG = Constant.LOG4j_PROPERTIES_PATH_UDB_A38;
	//private Class CLASS = this.getClass();
	private String gOrderType = "";
	private String gOrderNumber = "";
	
//	public UpdateServiceComponentWrapper() {
//		super(Constant.LOG4j_PROPERTIES_PATH_UDB_A38);
//	}
	
	@Override
	protected void executeService() throws Throwable {
		//LoggerUtil.debug(PATH_LOG, "###### UpdateServiceComponentWrapper - executeService START ######", CLASS);
		logger.debug("###### UpdateServiceComponentWrapper - executeService START ######");

		HashMap updateService = null;
		HashMap updateObject_rsp = null;		
		int option = checkChangeComponent(requestObj);
		String operationString = getOperationName(option);
		//LoggerUtil.debug(PATH_LOG, "=====>> operationString : "+operationString, CLASS);
		logger.debug("=====>> operationString : "+operationString);

		//Convert request object
		//LoggerUtil.debug(PATH_LOG, "###### mapToRequestUpdateHashMap ######", CLASS);
		logger.debug("###### mapToRequestUpdateHashMap ######");
		updateService = mapToRequestServiceHashMap(requestObj);
		response = (HashMap)updateService.clone();
		//LoggerUtil.debugHashMap(PATH_LOG, "== mapToRequestServiceHashMap ==", CLASS, updateService);
		if (logger.isDebugEnabled())
			logger.debug("== mapToRequestServiceHashMap ==");
		
		if (option == 1) {
			//LoggerUtil.debug(PATH_LOG, "***** OPTION 1 BEGIN*****", CLASS);
			logger.debug("***** OPTION 1 BEGIN*****");
			//updateObject_rsp = UpdateServiceComponentImpl.OverrideService((HashMap)updateService.clone(), connection, context, PATH_LOG);
			updateObject_rsp = UpdateServiceComponentImpl.OverrideService((HashMap)updateService.clone(), connection, context);
			//LoggerUtil.debug(PATH_LOG, "***** OPTION 1 END*****", CLASS);
			logger.debug("***** OPTION 1 END*****");
		}
		else if (option == 2) {
			//LoggerUtil.debug(PATH_LOG, "***** OPTION 2 BEGIN*****", CLASS);
			logger.debug("***** OPTION 2 BEGIN*****");
			//updateObject_rsp = UpdateServiceComponentImpl.changeComponent((HashMap)updateService.clone(), connection, context, PATH_LOG);
			updateObject_rsp = UpdateServiceComponentImpl.changeComponent((HashMap)updateService.clone(), connection, context);
			//LoggerUtil.debug(PATH_LOG, "***** OPTION 2 END*****", CLASS);
			logger.debug("***** OPTION 2 END*****");
		}
		else if (option == 3) {
			//LoggerUtil.debug(PATH_LOG, "***** OPTION 3 BEGIN*****", CLASS);
			logger.debug("***** OPTION 3 BEGIN*****");
			updateObject_rsp = new HashMap();
			//updateObject_rsp = UpdateServiceComponentImpl.updateService((HashMap)updateService.clone(), connection, context, PATH_LOG);
			updateObject_rsp = UpdateServiceComponentImpl.updateService((HashMap)updateService.clone(), connection, context);
			//LoggerUtil.debug(PATH_LOG, "***** OPTION 3 END*****", CLASS);
			logger.debug("***** OPTION 3 END*****");
		}
		
		//LoggerUtil.debugHashMap(PATH_LOG, "updateObject_rsp LAST", CLASS, updateObject_rsp);
		if (logger.isDebugEnabled())
			logger.debug("updateObject_rsp LAST " + DebugUtil.getStringBuffer(updateObject_rsp).toString());
		//LoggerUtil.debug(PATH_LOG, "###### UpdateServiceComponentWrapper - executeService END ######", CLASS);
		logger.debug("###### UpdateServiceComponentWrapper - executeService END ######");
		
		response = updateObject_rsp;
		
		return;
	}
	
	@Override
	protected DataObject prepareResponseObj(DataObject reqObj, HashMap response, Throwable error) throws Exception {
		
		MessageUtil msgHelper = new MessageUtil();
		HashMap result = msgHelper.getResult(error);
		String transactionLogId = msgHelper.getTransactionLogId(reqObj);
		DataObject responseObj = mapToResponseServiceDataObj(response, result, transactionLogId);
		
		if (responseObj != null) {
			SimpleConverter simConvert = new SimpleConverter();
			BOFactory boFactory = simConvert.getBoFactory(com.cat.ibacss.esb.bo.constant.Constant.BO_FACTORY_NAME);
			DataObject orderDataObject = boFactory.create(com.cat.ibacss.esb.bo.constant.Constant.NAME_SPACE_BO, com.cat.ibacss.esb.bo.constant.Constant.BO_KENAN_ORDER);
			orderDataObject.setString("OrderNumber", gOrderNumber);

			DataObject accountResp = responseObj.getDataObject("Account");
			accountResp.setDataObject("Order", orderDataObject);
			accountResp.setString("OrderType", gOrderType);
			
			//accountResp.setString("Remark", reqViewId);
			//LoggerUtil.debug(PATH_LOG, "###### Update Service Component Method Done ######", CLASS);
			logger.debug("###### Update Service Component Method Done ######");
		}
		else {
			//LoggerUtil.debug(PATH_LOG, "###### Update Service Component Method Not Done ######", CLASS);
			logger.debug("###### Update Service Component Method Not Done ######");
		}	
		
		return responseObj;
	}
	
	public HashMap mapToRequestServiceHashMap(DataObject requestObj) throws Exception {
		HashMap requestHashMap = null;
		try {
			UpdateServiceComponentConverter upConverter = new UpdateServiceComponentConverterUDBImpl();
			requestHashMap = upConverter.dataObjectUpdateComponentHashMap(requestObj);
		}
		catch (Exception e) {
			//LoggerUtil.error(PATH_LOG, "Exception : UpdateServiceComponentWrapperImpl = mapUpdateServiceComponentObject ---> "+e.getMessage(), CLASS);
			logger.error(DebugUtil.getStackTrace(e));
			throw e;
		}
		return requestHashMap;
	}
	
	private DataObject mapToResponseServiceDataObj(HashMap response, HashMap result, String transactionLogId) throws Exception {
		
		HashMap responseHashMap = new HashMap();
		DataObject responseObj;
		
		MessageUtil msgHelper = new MessageUtil();
		
		if (response != null){
			responseHashMap.put("Account", (HashMap)response.get("Account"));
			gOrderNumber = ((HashMap)((HashMap)((HashMap)response.get("Account")).get("Order")).get("Order")).get("OrderNumber").toString();
		}
		else{
			responseHashMap.put("Account", request);
		}
		
		responseHashMap.put("Result", result);
		responseHashMap.put("TransactionLogId", transactionLogId);
		
		//LoggerUtil.debug(PATH_LOG, "#### mapToResponseServiceDataObj : ####", CLASS);
		logger.debug("#### mapToResponseServiceDataObj : ####");
		//LoggerUtil.debugHashMap(PATH_LOG, "#### mapToResponseServiceDataObj : ####", CLASS,responseHashMap);
		if (logger.isDebugEnabled())
			logger.debug("#### mapToResponseServiceDataObj : #### " + DebugUtil.getStringBuffer(responseHashMap).toString());
		
		UpdateServiceComponentConverter UpdatePaymentMethodConverter = new UpdateServiceComponentConverterImpl();
		DataObject requestObj = UpdatePaymentMethodConverter.hashMapUpdateComponent2DataObject(responseHashMap);
		
		return requestObj;
	}
	

	/**
	 * Check operation name that is ChangeComponent or ChangeOverride.
	 * 1 = "ChangeOverride"
	 * 2 = "ChangeComponent"
	 * @param argDO
	 * @return int
	 */
	public int checkChangeComponent(DataObject argDO) {
		if (argDO != null && argDO.getDataObject("Account") != null) {
			DataObject accountReq = argDO.getDataObject("Account");
			String orderType = accountReq.getString("OrderType");
			gOrderType = orderType;
			if (orderType == null)
				return -1;
			else if ("COR".equalsIgnoreCase(orderType))
				return 1;
			else if ("CSA".equalsIgnoreCase(orderType))
				return 3;
			else
				return 2;
		}
		return -1;
	}

	/**
	 * Get String Operation name.
	 * @param argDO
	 * @return String
	 */
	public String getOperationName(int option) {
		if (option == 1)
			return "Override";
		else if (option == 2)
			return "Service Component";
		else if (option == 3)
			return "Services";
		else
			return "";
	}	
	
}
