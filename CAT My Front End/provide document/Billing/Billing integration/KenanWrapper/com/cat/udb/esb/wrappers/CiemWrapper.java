package com.cat.udb.esb.wrappers;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.cat.udb.esb.core.kenan.utils.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;
import com.cat.udb.esb.wrappers.common.OrderedCiemImpl;

import com.cat.ibacss.esb.bo.convert.SimpleConverter;
import com.cat.ibacss.esb.bo.convert.UpdateCIEMConverter;
import com.cat.ibacss.esb.bo.convert.impl.UpdateCIEMConverterImpl;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.bali.connection.ApiMappings;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.BaseWrapper;

import com.ibm.websphere.bo.BOFactory;
import commonj.sdo.DataObject;

public class CiemWrapper extends BaseWrapper {
	
	private static Logger logger = Logger.getLogger(CiemWrapper.class);
	
	//private Class CLASS = this.getClass();
	
	private String optionSystem = "";
	
//	public CiemWrapper(){
//		super(Constant.LOG4j_PROPERTIES_PATH_UDB_A8);
//	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void executeService() throws Throwable {
		//LoggerUtil.debug(PATH_LOG, "#### Start executeService() ####", CLASS);
		logger.debug("#### Start executeService() ####");
		
		optionSystem = checkOption(requestObj);
		request = mapToRequestServiceHashMap(requestObj, optionSystem);
		HashMap acctTmp = (HashMap)request.get("Account");
		acctTmp.put("ExtendedData", MessageUtil.getExtendedData(acctTmp, Constant.ACCOUNT_EXTDATA));
		
		if (optionSystem.equalsIgnoreCase("CIEM")) {
			response = changeCiem(acctTmp);
		}
		else {
			response = changeInventory(acctTmp);
		}

		//LoggerUtil.debug(PATH_LOG, "#### Start executeService() ####", CLASS);
		logger.debug("#### Start executeService() ####");
	}

	@SuppressWarnings("unchecked")
	@Override
	protected DataObject prepareResponseObj(DataObject reqObj, HashMap response, Throwable error) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "#### Start prepareResponseObj ####", CLASS);
		logger.debug("#### Start prepareResponseObj ####");
		
		HashMap result = MessageUtil.getResult(error);
		String transactionId = MessageUtil.getTransactionLogId(reqObj);
		String sourceOrderId = "";
		sourceOrderId = reqObj.getDataObject("Account").getDataObject("Order").getString("OrderNumber");
		HashMap responseHashMap = new HashMap();
		responseHashMap.put("Account", response);
		responseHashMap.put("Result", result);
		responseHashMap.put("TransactionLogId", transactionId);
		DataObject responseObj = mapToResponseServiceDataObj(responseHashMap, optionSystem);
		
		if (responseObj != null) {
			SimpleConverter simConvert = new SimpleConverter();
			BOFactory boFactory = simConvert.getBoFactory(com.cat.ibacss.esb.bo.constant.Constant.BO_FACTORY_NAME);
			DataObject orderDataObject = boFactory.create(com.cat.ibacss.esb.bo.constant.Constant.NAME_SPACE_BO, com.cat.ibacss.esb.bo.constant.Constant.BO_KENAN_ORDER);
			orderDataObject.setString("OrderNumber", sourceOrderId);
			if(responseObj.getDataObject("Account") != null){
				responseObj.getDataObject("Account").setDataObject("Order", orderDataObject);
			}
		}
		else {
			//LoggerUtil.info(PATH_LOG, "#### mapping return null. ####", CLASS);
			logger.info("#### mapping return null. ####");
		}
		
		//LoggerUtil.debug(PATH_LOG, "#### End prepareResponseObj ####", CLASS);
		logger.debug("#### End prepareResponseObj ####");
		return responseObj;
	}
	
	private HashMap mapToRequestServiceHashMap(DataObject requestObj, String optionSystem) throws Exception{
		//LoggerUtil.debug(PATH_LOG, "#### Start mapToRequestServiceHashMap() ####", CLASS);
		logger.debug("#### Start mapToRequestServiceHashMap() ####");
		
		HashMap response = null;
		UpdateCIEMConverter converter = new UpdateCIEMConverterImpl();
		if (optionSystem.equalsIgnoreCase("CIEM")) {
			response = converter.dataObjectCIEM2HashMap(requestObj);
		}
		else {
			response = converter.dataObjectInventoriesHashMap(requestObj);
		}
		//LoggerUtil.debugHashMap(PATH_LOG, "UpdateCiemRequest", CLASS, response);
		if (logger.isDebugEnabled())
			logger.debug("UpdateCiemRequest " + DebugUtil.getStringBuffer(response).toString());
		
		//LoggerUtil.debug(PATH_LOG, "#### End mapToRequestServiceHashMap() ####", CLASS);
		logger.debug("#### End mapToRequestServiceHashMap() ####");
		return response;
	}
	
	private DataObject mapToResponseServiceDataObj(HashMap responseHashMap, String optionSystem) throws Exception{
		//LoggerUtil.debug(PATH_LOG, "#### Start mapToResponseServiceDataObj() ####", CLASS);
		logger.debug("#### Start mapToResponseServiceDataObj() ####");
		
		//LoggerUtil.debugHashMap(PATH_LOG, "UpdateCiemResponse", CLASS, responseHashMap);
		if (logger.isDebugEnabled())
			logger.debug("UpdateCiemResponse " + DebugUtil.getStringBuffer(responseHashMap).toString());
		DataObject responseObj = null;
		UpdateCIEMConverter converter = new UpdateCIEMConverterImpl();
		if (optionSystem.equalsIgnoreCase("CIEM")) {
			responseObj = converter.hashMapCIEM2DataObject(responseHashMap);
		}
		else {
			responseObj = converter.hashMapInventories2DataObject(responseHashMap);
		}
		
		//LoggerUtil.debug(PATH_LOG, "#### End mapToResponseServiceDataObj() ####", CLASS);
		logger.debug("#### End mapToResponseServiceDataObj() ####");
		return responseObj;
	}
	
	private String checkOption(DataObject dataObject) throws Exception {
		List orderedServiceList = null;
		List invList = null;
		List ciemList1 = null;
		List ciemList2 = null;
		DataObject orderedObject = null;
		DataObject objectDataCiem = null;
		
		try {
			if (dataObject != null ) {
				DataObject accountDataObject = dataObject.getDataObject("Account");
				if (accountDataObject != null ) {
					orderedServiceList = accountDataObject.getList("OrderedService");
					
					if (orderedServiceList != null) {
						orderedObject = (DataObject)orderedServiceList.get(0);
						objectDataCiem = orderedObject.getDataObject("OrderedCustomerIdEquipMap");
						if (objectDataCiem != null ) {
							ciemList1 = objectDataCiem.getList("CustomerIdEquipMapOld");
							ciemList2 = objectDataCiem.getList("CustomerIdEquipMapNew");
						}
						invList = orderedObject.getList("Inventories");
						if (ciemList1 != null && ciemList2 != null)
							if (ciemList1.size() > 0 && ciemList2.size() > 0)
								return "CIEM";
						else if (invList != null && invList.size() > 0)
							return "Inventories";
						else
							return "Inventories";
					} 
				}
			}
		}
		catch (Exception e) {
			//e.printStackTrace();
			logger.error(DebugUtil.getStackTrace(e));
			return "Inventories";
		}
		return "Inventories";
	}
	
	@SuppressWarnings("unchecked")
	private HashMap changeCiem(HashMap requestHashMap) throws Exception{
		//LoggerUtil.debug(PATH_LOG, "#### Start changeCiem() ####", CLASS);
		logger.debug("#### Start changeCiem() ####");
		
		HashMap orderHashMapResponse = null;
		HashMap orderHashMapRequest = (HashMap)requestHashMap.get("Order");
		
		//LoggerUtil.debugHashMap(PATH_LOG, "OrderCreateRequest", CLASS, orderHashMapRequest);
		if (logger.isDebugEnabled())
			logger.debug("OrderCreateRequest " + DebugUtil.getStringBuffer(orderHashMapRequest).toString());
		orderHashMapResponse = connection.call(context, ApiMappings.getCallName("OrderCreate"), orderHashMapRequest);
		//LoggerUtil.debugHashMap(PATH_LOG, "OrderCreateResponse", CLASS, orderHashMapResponse);
		if (logger.isDebugEnabled())
			logger.debug("OrderCreateResponse " + DebugUtil.getStringBuffer(orderHashMapResponse));
		HashMap openedOrder = new HashMap();
		openedOrder = (HashMap)orderHashMapResponse.get("Order");
		
		HashMap[] services = (HashMap[])(requestHashMap.get("Services") != null ? requestHashMap.get("Services") : null);
		
		for (int i = 0 ; i < services.length ; i++){
			HashMap mainService = null;
			mainService = services[i];
			//		if(services != null && services.length > 0){
			//			mainService = services[0];
			//		}
			//		if(mainService != null){
			
			HashMap service = (HashMap)mainService.get("Service");
			service.put("ExtendedData", MessageUtil.getExtendedData(service, Constant.SERVICE_EXTDATA));
			//LoggerUtil.debugHashMap(PATH_LOG, "ServiceObj", CLASS, service);
			logger.debug("ServiceObj " + DebugUtil.getStringBuffer(service));
			
			HashMap[] orderedCiemOld = (HashMap[])service.get("OrderedCiemOld");
			HashMap[] orderedCiemNew = (HashMap[])service.get("OrderedCiemNew");
			
			HashMap[] orderedCiemResponseHashMapArray = null;
			//orderedCiemResponseHashMapArray = OrderedCiemImpl.orderedCiemDisconnectList(openedOrder, orderedCiemOld, connection, context, PATH_LOG);
			orderedCiemResponseHashMapArray = OrderedCiemImpl.orderedCiemDisconnectList(openedOrder, orderedCiemOld, connection, context);
			if(orderedCiemResponseHashMapArray != null){
				service.put("OrderedCiemOld", (HashMap[])orderedCiemResponseHashMapArray.clone());
			}
			orderedCiemResponseHashMapArray = null;
			
			//orderedCiemResponseHashMapArray =  OrderedCiemImpl.orderedCiemCreateList(null, openedOrder, orderedCiemNew, connection, context, PATH_LOG);
			orderedCiemResponseHashMapArray =  OrderedCiemImpl.orderedCiemCreateList(null, openedOrder, orderedCiemNew, connection, context);
			if(orderedCiemResponseHashMapArray != null){
				service.put("OrderedCiemNew", (HashMap[])orderedCiemResponseHashMapArray.clone());
			}
			orderedCiemResponseHashMapArray = null;
			
			mainService.put("service", (HashMap)service);
			
			services[i] = mainService;
		}
		requestHashMap.put("Services", services);
		services = null;
		
		//LoggerUtil.debugHashMap(PATH_LOG, "OrderCommitRequest", CLASS, orderHashMapResponse);
		if (logger.isDebugEnabled())
			logger.debug("OrderCommitRequest " + DebugUtil.getStringBuffer(orderHashMapResponse));
		HashMap orderCommitResponse = connection.call(context, ApiMappings.getCallName("OrderCommit"), orderHashMapResponse);
		//LoggerUtil.debugHashMap(PATH_LOG, "OrderCommitResponse", CLASS, orderCommitResponse);
		
		requestHashMap.put("Order", orderCommitResponse);
		
		//LoggerUtil.debug(PATH_LOG, "#### End changeCiem() ####", CLASS);
		logger.debug("#### End changeCiem() ####");
		return requestHashMap;
	}
	
	@SuppressWarnings("unchecked")
	private HashMap changeInventory(HashMap accountRequest) throws Exception{
		//LoggerUtil.debug(PATH_LOG, "#### Start changeInventory ####", CLASS);
		logger.debug("#### Start changeInventory ####");
		
		HashMap orderHashMap = (HashMap)accountRequest.get("Order");
		HashMap orderHashMapResponse = connection.call(context, ApiMappings.getCallName("OrderCreate"), orderHashMap);
		//LoggerUtil.debugHashMap(PATH_LOG, "OrderCreateResponse", CLASS, orderHashMapResponse);
		if (logger.isDebugEnabled())
			logger.debug("OrderCreateResponse " + DebugUtil.getStringBuffer(orderHashMapResponse).toString());
		
		if(orderHashMapResponse != null){
			accountRequest.put("Order", (HashMap)orderHashMapResponse.clone());
		}
		orderHashMapResponse = (HashMap)accountRequest.get("Order");
		
		HashMap[] services = (HashMap[])accountRequest.get("Services");
		
		for( int i = 0 ; i < services.length ; i++ ){
			HashMap service = (HashMap)services[i];
			service.put("ExtendedData", MessageUtil.getExtendedData(service, Constant.SERVICE_EXTDATA));
			//HashMap[] orderedInventoriesSwapResponse = OrderedCiemImpl.orderedInventorySwapList(orderHashMapResponse, (HashMap[])service.get("Inventories"), connection, context, PATH_LOG);
			HashMap[] orderedInventoriesSwapResponse = OrderedCiemImpl.orderedInventorySwapList(orderHashMapResponse, (HashMap[])service.get("Inventories"), connection, context);
			if(orderedInventoriesSwapResponse!=null){
				service.put("OrderedInventories", (HashMap[])orderedInventoriesSwapResponse.clone());
			}
			services[i] = (HashMap)service.clone();
			
			service = null;
			orderedInventoriesSwapResponse = null;
		}
		
		accountRequest.put("Services", (HashMap[])services.clone());
		
		HashMap orderCommitResponseHashMap = connection.call(context, ApiMappings.getCallName("OrderCommit"), orderHashMapResponse);
		//LoggerUtil.debugHashMap(PATH_LOG, "OrderCommitResponse", CLASS, orderCommitResponseHashMap);
		if (logger.isDebugEnabled())
			logger.debug("OrderCommitResponse " + DebugUtil.getStringBuffer(orderCommitResponseHashMap));
		accountRequest.put("Order", (HashMap)orderCommitResponseHashMap.clone());
		
		//LoggerUtil.debug(PATH_LOG, "#### End changeInventory ####", CLASS);
		logger.debug("#### End changeInventory ####");
		return accountRequest;
	}
}