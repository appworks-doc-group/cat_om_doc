package com.cat.udb.esb.wrappers;

import java.io.IOException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import terrapin.tuxedo.FmlError;
import terrapin.tuxedo.TuxApplicationError;
import terrapin.tuxedo.TuxError;

//import util.Constant;
//import util.HashMapHelper;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.BaseWrapper;

import com.cat.ibacss.esb.bo.convert.CancelOrderConverter;
import com.cat.ibacss.esb.bo.convert.ServiceChangeConverter;
import com.cat.ibacss.esb.bo.convert.impl.CancelOrderConverterImpl;
import com.cat.ibacss.esb.bo.convert.impl.ServiceChangeConverterImpl;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.csgsystems.aruba.connection.Connection;
import com.csgsystems.aruba.connection.ServiceException;
import com.csgsystems.bali.connection.ApiMappings;
import com.csgsystems.fx.security.util.FxException;

import commonj.sdo.DataObject;

public class OrderCancelWrapper extends BaseWrapper {
	
	private static Logger logger = Logger.getLogger(OrderCancelWrapper.class);
	
//	private Class CLASS = this.getClass();
//	
//	public OrderCancelWrapper() {
//		super(Constant.LOG4j_PROPERTIES_PATH_UDB_A42);
//	}

	@Override
	protected void executeService() throws Throwable {
		//Init request message
		request = mapToRequestHashMap(requestObj);
		//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedServiceSuspend Request ######" , CLASS, request);
		HashMap orderField = (HashMap)request.get("Order");
		HashMap orderKey = (HashMap)orderField.get("Key");
		//Find Order Id
		HashMap key = findOrderId((String)orderKey.get("OrderId"),connection,context);
		
		orderField.put("Key", key);
	
		if (logger.isDebugEnabled())
			logger.debug("###### OrdereCancel Request ###### " + DebugUtil.getStringBuffer(request).toString());
	
		//Call Kenan API
		HashMap APIresponse = connection.call(context, ApiMappings.getCallName("OrderCancel"), request);
		//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedServiceSuspend Response ######" , CLASS, APIresponse);
		if (logger.isDebugEnabled())
			logger.debug("###### OrderedCancel Response ###### " + DebugUtil.getStringBuffer(APIresponse).toString());
		
		response = MessageUtil.updateOrder(APIresponse, request);
		return;
	}

	@Override
	protected DataObject prepareResponseObj(DataObject reqObj, HashMap response, Throwable error) throws Exception {
		
		HashMap result = MessageUtil.getResult(error);
		String transactionLogId = MessageUtil.getTransactionLogId(reqObj);
		
		DataObject responseObj = mapToDataObj(response, result, transactionLogId);
		
		return responseObj;
	}
	
	private HashMap mapToRequestHashMap(DataObject requestObj) throws Exception{
		
		//Return Order  
		CancelOrderConverter cancelOrderConvert = new CancelOrderConverterImpl();
		HashMap requestHashMap = cancelOrderConvert.dataObject2HashMap(requestObj);
		
		//LoggerUtil.debugHashMap(PATH_LOG,"###### mapToRequestCancelHashMap ######" , CLASS, requestHashMap);
		if (logger.isDebugEnabled())
			logger.debug("###### mapToRequestCancelHashMap ###### " + DebugUtil.getStringBuffer(requestHashMap).toString());
			
		return requestHashMap;
	}
	
	private DataObject mapToDataObj(HashMap response, 
			HashMap result, String transactionLogId) throws Exception {
		
		HashMap responseHashMap = new HashMap();
		DataObject responseObj;
		
		if (response != null){
			responseHashMap.put("Account", response);
		}else{
			responseHashMap.put("Account", request);
		}
		
		responseHashMap.put("Result", result);
		responseHashMap.put("TransactionLogId", transactionLogId);
		
		//LoggerUtil.debugHashMap(PATH_LOG,"###### mapToResponseCancelDataObj ######" , CLASS, responseHashMap);
		if (logger.isDebugEnabled())
			logger.debug("###### mapToResponseCancelDataObj ###### " + DebugUtil.getStringBuffer(responseHashMap).toString());
		
		CancelOrderConverter cancelOrderConvert = new CancelOrderConverterImpl();
		DataObject requestObj = cancelOrderConvert.hashMap2DataObject(responseHashMap);
		
		return requestObj;
	}
	
	private HashMap findOrderId(String orderNumber,Connection connection, BSDMSessionContext context) throws Throwable{
		
		HashMap orderRequest = new HashMap();
		HashMap findOrder = new HashMap();
		HashMap condition = new HashMap();
		HashMap equalOrderNumber = new HashMap();
		HashMap equalOrderStatus = new HashMap();
		
		equalOrderStatus.put("Equal", 20);
		equalOrderNumber.put("Equal", orderNumber);
		condition.put("OrderNumber", equalOrderNumber);
		condition.put("OrderStatusId", equalOrderStatus);
		findOrder.putAll(condition);
		findOrder.put("Fetch", true);
		orderRequest.put("Order", findOrder);
		
		logger.debug("###### Find Order Request ###### " + DebugUtil.getStringBuffer(orderRequest).toString());
		HashMap APIresponse = connection.call(context, ApiMappings.getCallName("OrderFind"), orderRequest);
		logger.debug("###### Find Order Response ###### " + DebugUtil.getStringBuffer(APIresponse).toString());
		
		Object[] orderlist = (Object[])APIresponse.get("OrderList");
		
		if(orderlist.length == 0)
			throw(new Throwable("No Order Found"));
		
		HashMap orderKey = (HashMap)orderlist[0];
		
		return (HashMap)orderKey.get("Key");
	}
}