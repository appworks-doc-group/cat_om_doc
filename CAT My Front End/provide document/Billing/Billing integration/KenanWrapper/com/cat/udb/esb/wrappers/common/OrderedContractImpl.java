package com.cat.udb.esb.wrappers.common;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cat.udb.esb.core.kenan.utils.ConverterUtil;
import com.cat.udb.esb.core.utils.DebugUtil;
//import util.HashMapHelper;

//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.csgsystems.aruba.connection.Connection;
import com.csgsystems.bali.connection.ApiMappings;

public class OrderedContractImpl {
	
	private static Logger logger = Logger.getLogger(OrderedContractImpl.class);
	
	//public static HashMap[] createOrderedContract(HashMap[] orderedContractList, 
	//		HashMap order, Connection connection, BSDMSessionContext context,String PATH_LOG) throws Throwable{
	public static HashMap[] createOrderedContract(HashMap[] orderedContractList, HashMap order, Connection connection, BSDMSessionContext context) throws Throwable{
		ArrayList orderedContractResponseList = new ArrayList();
		Object orderField = order.get("Order");
		HashMap serviceField;
		
		for (HashMap orderedContract : orderedContractList) {
			//Init request
			HashMap orderedContractField = (HashMap)orderedContract.get("OrderedContract");
			orderedContractField.put("Order", orderField);
			orderedContractField.put("FindExistingSO", true);
				
			//API Call
			//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedContractCreate Request ######" , OrderedContractImpl.class, orderedContractField);
			if (logger.isDebugEnabled())
				logger.debug("###### OrderedContractCreate Request ###### " + DebugUtil.getStringBuffer(orderedContractField).toString());
			HashMap orderedContractCreateResponse = connection.call(context, ApiMappings.getCallName("OrderedContractCreate"), orderedContractField);
				
			//Set ServiceLineID & SourceLineItemId
			orderedContractCreateResponse.put("ServiceLineId", orderedContractField.get("ServiceLineId"));
			orderedContractCreateResponse.put("SourceLineItemId", orderedContractField.get("SourceLineItemId"));
			
			//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedContractCreate Response ######" , OrderedContractImpl.class, orderedContractCreateResponse);
			if (logger.isDebugEnabled())
				logger.debug("###### OrderedContractCreate Response ###### " + DebugUtil.getStringBuffer(orderedContractCreateResponse).toString());
			HashMap orderedContractCreate = new HashMap();
			orderedContractCreate.put("OrderedContract", orderedContractCreateResponse);
			orderedContractResponseList.add(orderedContractCreate);	
		}
		return ConverterUtil.toHashMapArray(orderedContractResponseList);
	}
	
//	public static HashMap[] modifyOrderedContract(HashMap[] orderedContractList, 
//		HashMap order, Connection connection, BSDMSessionContext context,String PATH_LOG) throws Throwable{
	public static HashMap[] modifyOrderedContract(HashMap[] orderedContractList, 
			HashMap order, Connection connection, BSDMSessionContext context) throws Throwable{
		
		//LoggerUtil.debug(PATH_LOG," --- Call modifyOrderedContract --- " , OrderedContractImpl.class );
		logger.debug(" --- Call modifyOrderedContract --- ");
		
		ArrayList orderedContractResponseList = new ArrayList();
		HashMap orderField = (HashMap)order.get("Order");
		HashMap serviceField;
		int i = 0;
		
		for (HashMap orderedContract : orderedContractList) {
			//LoggerUtil.debugHashMap(PATH_LOG," --- orderedContract  --- "+i++ + " " , OrderedContractImpl.class, orderedContract);
			if (logger.isDebugEnabled())
				logger.debug(" --- orderedContract  --- "+i++ + " " + DebugUtil.getStringBuffer(orderedContract));
			
			//Init request
			HashMap orderedContractField = (HashMap)orderedContract.get("OrderedContract");
			orderedContractField.put("Order", orderField);
			orderedContractField.put("FindExistingSO", true);
				
			//API Call
			HashMap orderedContractModifyResponse = null;
			if(orderedContractField.get("ActionType").equals("CREATE")){
				//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedContractCreate Request ######" , OrderedContractImpl.class, orderedContractField);
				if (logger.isDebugEnabled())
					logger.debug("###### OrderedContractCreate Request ###### " + DebugUtil.getStringBuffer(orderedContractField).toString());
				orderedContractModifyResponse = connection.call(context, ApiMappings.getCallName("OrderedContractCreate"), orderedContractField);
			}
			else if(orderedContractField.get("ActionType").equals("DISCONNECT")){
				//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedContractDisconnect Request ######" , OrderedContractImpl.class, orderedContractField);
				if (logger.isDebugEnabled())
					logger.debug("###### OrderedContractDisconnect Request ###### " + DebugUtil.getStringBuffer(orderedContractField).toString());
				orderedContractModifyResponse = connection.call(context, ApiMappings.getCallName("OrderedContractDisconnect"), orderedContractField);
			}
			//Set ServiceLineID & SourceLineItemId
			orderedContractModifyResponse.put("ServiceLineId", orderedContractField.get("ServiceLineId"));
			orderedContractModifyResponse.put("SourceLineItemId", orderedContractField.get("SourceLineItemId"));
			
			HashMap orderedContractOut = new HashMap();
			orderedContractOut.put("OrderedContract", orderedContractModifyResponse);
			//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedContractDisconnect Response ######" , OrderedContractImpl.class, orderedContractOut);
			if (logger.isDebugEnabled())
				logger.debug("###### OrderedContractDisconnect Response ###### " + DebugUtil.getStringBuffer(orderedContractOut));
			orderedContractResponseList.add(orderedContractOut);	
		}
		return ConverterUtil.toHashMapArray(orderedContractResponseList);
	}
}
