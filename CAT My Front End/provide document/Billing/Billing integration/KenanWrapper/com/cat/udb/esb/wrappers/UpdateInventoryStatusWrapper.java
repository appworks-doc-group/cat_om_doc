package com.cat.udb.esb.wrappers;

import java.util.HashMap;

import org.apache.log4j.Logger;

//import util.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.BaseWrapper;

import com.cat.ibacss.esb.bo.convert.UpdateServiceInventoryConverter;
import com.cat.ibacss.esb.bo.convert.impl.UpdateServiceInventoryConverterImpl;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.bali.connection.ApiMappings;
import commonj.sdo.DataObject;

public class UpdateInventoryStatusWrapper extends BaseWrapper {
	
	private static Logger logger = Logger.getLogger(UpdateInventoryStatusWrapper.class);
//	private Class CLASS = this.getClass();
//	
//	public UpdateInventoryStatusWrapper(){
//		super(Constant.LOG4j_PROPERTIES_PATH_UDB_A37);
//	}

	@Override
	protected void executeService() throws Throwable {
		//LoggerUtil.debug(PATH_LOG, "#### start executeService() ####", CLASS);
		logger.debug("#### start executeService() ####");
		HashMap requestMap = new HashMap();
		//MessageUtil msgHelper = new MessageUtil();
		requestMap = mapToRequestServiceHashMap(requestObj);
		request = requestMap;
		
		UpdateServiceInventoryConverter converter = new UpdateServiceInventoryConverterImpl();
		short invStatus = converter.getInvStatus(requestObj);
		logger.debug("invStatus = " + invStatus);
		
		if (invStatus == 1 || invStatus == 2) {
			response = updateInvStatus(request, invStatus);
		}
		else {
			throw new Exception("Unsupported invStatus value (" + invStatus + "). Only 1 (reserve) or 2 (unreserve) is expected.");
		}
		
		//LoggerUtil.debug(PATH_LOG, "#### end executeService() ####", CLASS);
		logger.debug("#### end executeService() ####");
	}

	@Override
	protected DataObject prepareResponseObj(DataObject reqObj, HashMap response, Throwable error) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "#### start prepareResponseObj() ####", CLASS);
		logger.debug("#### start prepareResponseObj() ####");
		
		MessageUtil msgHelper = new MessageUtil();
		HashMap result = msgHelper.getResult(error);
		String transactionLogId = msgHelper.getTransactionLogId(reqObj);
		DataObject responseObj = null;
		
		if(response != null){
			responseObj = mapToResponseServiceDataObj(reqObj, response, result, transactionLogId);
		}
		
		//LoggerUtil.debug(PATH_LOG, "#### end prepareResponseObj() ####", CLASS);
		logger.debug("#### end prepareResponseObj() ####");
		
		return responseObj;
	}
	
	private HashMap mapToRequestServiceHashMap(DataObject reqObj) throws Exception{
		//LoggerUtil.debug(PATH_LOG, "#### start mapToRequestServiceHashMap() ####", CLASS);
		logger.debug("#### start mapToRequestServiceHashMap() ####");
		
		UpdateServiceInventoryConverter converter = new UpdateServiceInventoryConverterImpl();
		HashMap requestHashMap = converter.dataObject2HashMap(reqObj);
		//LoggerUtil.debugHashMap(PATH_LOG, "UpdateInventoryRequest", CLASS, requestHashMap);
		if (logger.isDebugEnabled())
			logger.debug("UpdateInventoryRequest " + DebugUtil.getStringBuffer(requestHashMap).toString());
		
		//LoggerUtil.debug(PATH_LOG, "#### end mapToRequestServiceHashMap() ####", CLASS);
		logger.debug("#### end mapToRequestServiceHashMap() ####");
		
		return requestHashMap;
	}
	
	private DataObject mapToResponseServiceDataObj(DataObject reqObj,HashMap responseHashMap, HashMap resultHashMap, String transactionLogId) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "#### start mapToResponseServiceDataObj() ####", CLASS);
		logger.debug("#### start mapToResponseServiceDataObj() ####");
		HashMap response = new HashMap();
		DataObject responseObject;
		
		response.put("Account", responseHashMap);
		response.put("Result", resultHashMap);
		response.put("TransactionLogId", transactionLogId);
		//LoggerUtil.debugHashMap(PATH_LOG, "UpdateInventoryResponse", CLASS, response);
		if (logger.isDebugEnabled())
			logger.debug("UpdateInventoryResponse " + DebugUtil.getStringBuffer(response).toString());
		
		UpdateServiceInventoryConverter converter = new UpdateServiceInventoryConverterImpl();
		
		//don't use in method.
		short invStatus = 0;
		responseObject = converter.hashMap2DataObject(response, invStatus);
		
		//LoggerUtil.debug(PATH_LOG, "#### end mapToResponseServiceDataObj() ####", CLASS);
		logger.debug("#### end mapToResponseServiceDataObj() ####");
		
		return responseObject;
	}
	
	private HashMap updateInvStatus(HashMap request, short status) throws Exception{
		//LoggerUtil.debug(PATH_LOG, "#### start updateInvStatus() ####", CLASS);
		logger.debug("#### start updateInvStatus() ####");
		HashMap response = new HashMap();
		if(status == 1){
			response = reserve(request);
		} else if(status == 2){
			response = unreserve(request);
		}
		
		//LoggerUtil.debug(PATH_LOG, "#### end updateInvStatus() ####", CLASS);
		logger.debug("#### end updateInvStatus() ####");
		
		return response;
	}
	
	private HashMap reserve(HashMap requestHM) throws Exception{
		
		//LoggerUtil.debug(PATH_LOG, "#### start reserve() ####", CLASS);
		logger.debug("#### start reserve() ####");
		
		HashMap response = requestHM;
		HashMap services[] = (HashMap[])requestHM.get("Services");
		for(HashMap hm : services){
			HashMap service = (HashMap)hm.get("Service");
			HashMap inventories[] = (HashMap[])service.get("Inventories");
			for(HashMap elm : inventories){
				response = connection.call(context, ApiMappings.getCallName("InvElementReserve"),elm);
			}
		}
		
		//LoggerUtil.debug(PATH_LOG, "#### end reserve() ####", CLASS);
		logger.debug("#### end reserve() ####");
		
		return requestHM;
	}
	
	private HashMap unreserve(HashMap requestHM) throws Exception{
		//LoggerUtil.debug(PATH_LOG, "#### start unreserve() ####", CLASS);
		logger.debug("#### start unreserve() ####");
		HashMap response = requestHM;
		HashMap services[] = (HashMap[])requestHM.get("Services");
		for(HashMap hm : services){
			HashMap service = (HashMap)hm.get("Service");
			HashMap inventories[] = (HashMap[])service.get("Inventories");
			for(HashMap elm : inventories){
				response = connection.call(context, ApiMappings.getCallName("InvElementUnReserve"),elm);
			}
		}
		
		//LoggerUtil.debug(PATH_LOG, "#### end unreserve() ####", CLASS);
		logger.debug("#### end unreserve() ####");
		
		return requestHM;
	}
}
