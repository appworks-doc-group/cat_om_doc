package com.cat.udb.esb.wrappers;

import java.util.HashMap;

import org.apache.log4j.Logger;

//import util.Constant;
import com.cat.udb.esb.core.kenan.utils.ConverterUtil;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.BaseWrapper;

import com.cat.ibacss.esb.bo.convert.PaymentConverter;
import com.cat.ibacss.esb.bo.convert.impl.PaymentConverterImpl;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.bali.connection.ApiMappings;
import commonj.sdo.DataObject;

public class RetrievePaymentListWrapper extends BaseWrapper {
	
	private static Logger logger = Logger.getLogger(RetrievePaymentListWrapper.class);
//	private Class CLASS = this.getClass();
//	
//	public RetrievePaymentListWrapper(){
//		super(Constant.LOG4j_PROPERTIES_PATH_UDB_F14);
//	}

	@SuppressWarnings("unchecked")
	@Override
	protected void executeService() throws Throwable {
		//LoggerUtil.debug(PATH_LOG, "#### Start executeService ####", CLASS);
		logger.debug("#### Start executeService ####");
		
		request = mapToRequestServiceHashMap(requestObj);
		
		HashMap responseAPI = new HashMap();
		HashMap[] paymentList = null;
		HashMap paymentRequest = new HashMap();
		HashMap returnPayment = (HashMap)request.get("Payment");
		
		paymentRequest.put("Payment", (HashMap)returnPayment.clone());
		
		responseAPI = connection.call(context, ApiMappings.getCallName("PaymentFind"), paymentRequest);
		if(responseAPI.containsKey("TotalCount")){
			Integer totalCount = ConverterUtil.toInteger("TotalCount", responseAPI.get("TotalCount").toString());
			if(totalCount > 0){
				paymentList = getObjectList(responseAPI, "PaymentList", "Payment");
			}else{
				throw new Throwable("Payment not found");
			}
		} else {
			throw new Throwable("Invalid Payment Structure which returun from backend");
		}
		
		paymentRequest.put("Payments", paymentList);
		
		response = paymentRequest;
		
		//LoggerUtil.debug(PATH_LOG, "#### End executeService ####", CLASS);
		logger.debug("#### End executeService ####");
	}

	@SuppressWarnings("unchecked")
	@Override
	protected DataObject prepareResponseObj(DataObject reqObj, HashMap response, Throwable error) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "#### Start prepareResponseObj ####", CLASS);
		logger.debug("#### Start prepareResponseObj ####");
		
		HashMap result = MessageUtil.getResult(error);
		String transactionLogId = MessageUtil.getTransactionLogId(reqObj);
		response.put("Result", result);
		response.put("TransactionLogId", transactionLogId);
		DataObject responseObj = mapToResponseServiceObj(response);
		
		//LoggerUtil.debug(PATH_LOG, "#### End prepareResponseObj ####", CLASS);
		logger.debug("#### End prepareResponseObj ####");
		
		return responseObj;
	}
	
	private HashMap mapToRequestServiceHashMap(DataObject requestObj) throws Exception{
		//LoggerUtil.debug(PATH_LOG, "#### Start mapToRequestServiceHashMap ####", CLASS);
		logger.debug("#### Start mapToRequestServiceHashMap ####");
		
		HashMap response;
		PaymentConverter converter = new PaymentConverterImpl();
		response = converter.dataObjectRetrieveList2HashMap(requestObj);
		//LoggerUtil.debugHashMap(PATH_LOG, "RetrievePaymentListRequest", CLASS, response);
		if (logger.isDebugEnabled())
			logger.debug("RetrievePaymentListRequest " + DebugUtil.getStringBuffer(response).toString());
		
		//LoggerUtil.debug(PATH_LOG, "#### End mapToRequestServiceHashMap ####", CLASS);
		logger.debug("#### End mapToRequestServiceHashMap ####");
		
		return response;
	}
	
	private DataObject mapToResponseServiceObj(HashMap responseHashMap) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "#### Start mapToResponseServiceObj ####", CLASS);
		logger.debug("#### Start mapToResponseServiceObj ####");
		
		//LoggerUtil.debugHashMap(PATH_LOG, "RetrievePaymentListResponse", CLASS, responseHashMap);
		if (logger.isDebugEnabled())
			logger.debug("RetrievePaymentListResponse " + DebugUtil.getStringBuffer(responseHashMap).toString());
		
		DataObject responseObj;
		PaymentConverter converter = new PaymentConverterImpl();
		responseObj = converter.hashMapRetrieveList2DataObject(responseHashMap);
		
		//LoggerUtil.debug(PATH_LOG, "#### End mapToResponseServiceObj ####", CLASS);
		logger.debug("#### End mapToResponseServiceObj ####");
		
		return responseObj;
	}
	
	public static HashMap[] getObjectList(HashMap rspObjList,String objListName,String ObjName)  throws Exception {
		Object[] obj = null;
		HashMap[] ret = null;
		HashMap responseList[] = null;
		HashMap objOut = null;
		try{
			obj = (Object[]) rspObjList.get(objListName);
			ret = new HashMap[obj.length];
			responseList = new HashMap[obj.length];
			objOut = new HashMap();
			for(int i=0;i<obj.length;i++){
				ret[i] = (HashMap) obj[i];
				objOut.put(ObjName, (HashMap)ret[i].clone());
				responseList[i] = (HashMap)objOut.clone();
			}
		}
		catch (Exception e) {
			//e.printStackTrace();
			logger.error(DebugUtil.getStackTrace(e));
			throw e;
		}
		
		return responseList;
	}
}
