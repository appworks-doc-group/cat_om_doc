package com.cat.udb.esb.wrappers;

import org.apache.log4j.Logger;

//import com.cat.udb.esb.core.kenan.utils.Constant;
import com.cat.udb.esb.core.kenan.wrappers.InterimBaseWrapper;

import com.cat.udb.esb.core.utils.ConfigReader;
import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.utils.StringUtil;

import cat.ibacss.bean.InterimBillBean;
import cat.ibacss.irbl.invoke.InterimBillInvocation;
import cat.ibacss.irbl.invoke.impl.InterimBillInvocationImpl;

import com.cat.ibacss.esb.bo.convert.InterimConverter;
import com.cat.ibacss.esb.bo.convert.impl.InterimConverterImpl;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import commonj.sdo.DataObject;

public class CreateInterimBillWrapper extends InterimBaseWrapper {
	
	private static Logger logger = Logger.getLogger(CreateInterimBillWrapper.class);

	private static final String CONFIG_KEY_INTERIM_BILL_URL_PROVIDER = "interim_bill_url_provider";
//	private Class CLASS = this.getClass();
//
//	public CreateInterimBillWrapper(){
//		super(Constant.Log4j_PROPERTIES_PATH_UDB_A45);
//	}
	
	@Override
	protected void executeService() throws Throwable {
		//LoggerUtil.debug(PATH_LOG, "#### start executeService() ####", CLASS);
		logger.debug("#### start executeService() ####");
		
		InterimBillBean requestBean = mapToRequestServiceBean(requestObj);
		InterimBillBean responseBean = null;
			
		responseBean = callInterimBillClient(requestBean);
		
		response = responseBean;
		//LoggerUtil.debug(PATH_LOG, "#### end executeService() ####", CLASS);
		logger.debug("#### end executeService() ####");
	}

	@Override
	protected DataObject prepareResponseObj(DataObject reqObj, InterimBillBean response, Throwable error) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "#### start prepareResponseObj() ####", CLASS);
		logger.debug("#### start prepareResponseObj() ####");
		
		DataObject responseObj = null;
		
		if(response != null){
			responseObj = mapToResponseServiceObject(response);
		}
		//LoggerUtil.debug(PATH_LOG, "#### end prepareResponseObj() ####", CLASS);
		logger.debug("#### end prepareResponseObj() ####");
		return responseObj;
	}
	
	private InterimBillBean mapToRequestServiceBean(DataObject reqObj) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "#### start mapToRequestServiceBean() ####", CLASS);
		logger.debug("#### start mapToRequestServiceBean() ####");
		
		InterimBillBean mapData_Rsp = null;
		InterimConverter reConverter = new InterimConverterImpl();
		
		mapData_Rsp = reConverter.dataObjectCreate2Bean(reqObj);
		//LoggerUtil.debug(PATH_LOG, "#### requestBean : " + mapData_Rsp + "####", CLASS);
		logger.debug("#### requestBean : " + mapData_Rsp + "####");
		
		//LoggerUtil.debug(PATH_LOG, "#### end mapToRequestServiceBean() ####", CLASS);
		logger.debug("#### end mapToRequestServiceBean() ####");
		
		return mapData_Rsp;
	}
	
	private DataObject mapToResponseServiceObject(InterimBillBean responseBean) throws Exception{
		//LoggerUtil.debug(PATH_LOG, "#### start mapToResponseServiceObject() ####", CLASS);
		logger.debug("#### start mapToResponseServiceObject() ####");
		
		DataObject responseObj = null;
		InterimConverter reConvert = new InterimConverterImpl();
		//LoggerUtil.debug(PATH_LOG, "#### responseBean : " + responseBean + "####", CLASS);
		logger.debug("#### responseBean : " + responseBean + "####");
		responseObj = reConvert.beanCreate2DataObject(responseBean);
		
		//LoggerUtil.debug(PATH_LOG, "#### end mapToResponseServiceObject() ####", CLASS);
		logger.debug("#### end mapToResponseServiceObject() ####");
		
		return responseObj;
	}
	
	private InterimBillBean callInterimBillClient(InterimBillBean requestBean){
		//LoggerUtil.debug(PATH_LOG, "#### start callInterimBillClient() ####", CLASS);
		logger.debug("#### start callInterimBillClient() ####");
		
		InterimBillBean responseBean = null;
		try {
			String interimBillURLProvider = ConfigReader.getConfig(CONFIG_KEY_INTERIM_BILL_URL_PROVIDER);
			if (StringUtil.equalsNullBlank(interimBillURLProvider))
				throw new NullPointerException("No configuration value for key " + CONFIG_KEY_INTERIM_BILL_URL_PROVIDER + ". Please check main.xml");
			
			if (logger.isDebugEnabled())
				logger.debug("Value of config key '" + CONFIG_KEY_INTERIM_BILL_URL_PROVIDER + "' = " + interimBillURLProvider);
			//InterimBillInvocation invoker = new InterimBillInvocationImpl(Constant.INTERIM_BILL_URL_PROVIDER);
			InterimBillInvocation invoker = new InterimBillInvocationImpl(interimBillURLProvider);
			responseBean = invoker.create(requestBean);
		}
		catch (Exception ex){
			logger.error(DebugUtil.getStackTrace(ex));
			responseBean = requestBean;
			responseBean.setReturnCode("1");
			responseBean.setReturnDescription(ex.getLocalizedMessage());
		}
		//LoggerUtil.debug(PATH_LOG, "#### end callInterimBillClient() ####", CLASS);
		logger.debug("#### end callInterimBillClient() ####");
		
		return responseBean;
	}
	
}
