package com.cat.udb.esb.wrappers.common;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cat.udb.esb.core.kenan.utils.ConverterUtil;
import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.wrappers.CiemWrapper;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.csgsystems.aruba.connection.Connection;
import com.csgsystems.bali.connection.ApiMappings;

public class OrderedCiemImpl {
	
	private static Logger logger = Logger.getLogger(OrderedCiemImpl.class);

//	public static HashMap[] createOrderedCiem(HashMap[] orderedCiemList,HashMap service, HashMap order, 
//			Connection connection, BSDMSessionContext context,String PATH_LOG) throws Throwable{
	public static HashMap[] createOrderedCiem(HashMap[] orderedCiemList,HashMap service, HashMap order, 
			Connection connection, BSDMSessionContext context) throws Throwable{
		
		ArrayList orderedCiemResponseList = new ArrayList();
		HashMap serviceField = (HashMap)service.get("Service");
		HashMap orderField = (HashMap)order.get("Order");
		for (HashMap orderedCiem : orderedCiemList) {
			
			//Init request
			orderedCiem.put("Order", orderField);
			orderedCiem.put("FindExistingSO", true);

			((HashMap)orderedCiem.get("CustomerIdEquipMap")).put("ServiceInternalId", serviceField.get("ServiceInternalId"));
			((HashMap)orderedCiem.get("CustomerIdEquipMap")).put("ServiceInternalIdResets", serviceField.get("ServiceInternalIdResets"));
			
			//Call OrderedCiemCreate
			//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedCiemCreate Request ######" , OrderedCiemImpl.class, orderedCiem);
			if (logger.isDebugEnabled())
				logger.debug("###### OrderedCiemCreate Request ###### " + DebugUtil.getStringBuffer(orderedCiem).toString());
			HashMap orderedCiemCreateResponse = connection.call(context, ApiMappings.getCallName("OrderedCiemCreate"), orderedCiem);
			
			//Set ServiceLineID & SourceLineItemId 
			HashMap orderedCiemField = (HashMap)orderedCiem.get("CustomerIdEquipMap");
			orderedCiemCreateResponse.put("ServiceLineId", orderedCiemField.get("ServiceLineId"));
			orderedCiemCreateResponse.put("SourceLineItemId", orderedCiemField.get("SourceLineItemId"));
			
			//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedCiemCreate Response ######" , OrderedCiemImpl.class, orderedCiemCreateResponse);
			if (logger.isDebugEnabled())
				logger.debug("###### OrderedCiemCreate Response ###### " + DebugUtil.getStringBuffer(orderedCiemCreateResponse).toString());
			orderedCiemResponseList.add(orderedCiemCreateResponse);
		}
		return ConverterUtil.toHashMapArray(orderedCiemResponseList);
	}
	
	@SuppressWarnings("unchecked")
	//public static HashMap[] orderedInventorySwapList(HashMap order, HashMap[] orderedInventories, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Exception{
	public static HashMap[] orderedInventorySwapList(HashMap order, HashMap[] orderedInventories, Connection connection, BSDMSessionContext context) throws Exception{
		//LoggerUtil.debug(PATH_LOG, "#### Start orderedInventorySwapList ####", OrderedCiemImpl.class);
		logger.debug("#### Start orderedInventorySwapList ####");
		
		boolean checkInventoriesArrayLength = (null != orderedInventories && orderedInventories.length > 0);
		HashMap[] inventoriesResponseArrayHashMap = null;
		if(checkInventoriesArrayLength){
			inventoriesResponseArrayHashMap = new HashMap[orderedInventories.length];
			for(int i = 0 ; i < orderedInventories.length ; i++){
				HashMap orderedInventorySwap = new HashMap();
				orderedInventorySwap = (HashMap)orderedInventories[i].get("OrderedInventorySwap");
				orderedInventorySwap.put("Order", order);
				orderedInventorySwap.put("FindExistingSO", true);
				
				HashMap orderedInventorySwapReq = new HashMap();
				orderedInventorySwapReq.put("OrderedInventorySwap", orderedInventorySwap);
				
				HashMap orderedInventorySwapResponse = connection.call(context, ApiMappings.getCallName("OrderedInventorySwap"), orderedInventorySwapReq);
				
				inventoriesResponseArrayHashMap[i] = (HashMap)orderedInventorySwapResponse.clone();
				orderedInventorySwap = null;
				orderedInventorySwapResponse = null;
			}
		}
		
		//LoggerUtil.debug(PATH_LOG, "#### End orderedInventorySwapList ####", OrderedCiemImpl.class);
		logger.debug("#### End orderedInventorySwapList ####");
		
		return inventoriesResponseArrayHashMap;
	}

	@SuppressWarnings("unchecked")
	//public static HashMap[] orderedCiemDisconnectList(HashMap order, HashMap[] OrderedCiemOld, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Exception{
	public static HashMap[] orderedCiemDisconnectList(HashMap order, HashMap[] OrderedCiemOld, Connection connection, BSDMSessionContext context) throws Exception{
		//LoggerUtil.debug(PATH_LOG, "#### Start orderedCiemDisconnectList ####", OrderedCiemImpl.class);
		logger.debug("#### Start orderedCiemDisconnectList ####");
		
		boolean checkOrderService = (OrderedCiemOld != null && OrderedCiemOld.length > 0);
		HashMap[] orderedCiemResponseHashMappArray = null;
		if(checkOrderService) {
			orderedCiemResponseHashMappArray = new HashMap[OrderedCiemOld.length];
			for(int i = 0 ; i < OrderedCiemOld.length ; i++){
				HashMap orderedCiemRequestHashMap = OrderedCiemOld[i];
				HashMap CIEM = (HashMap)orderedCiemRequestHashMap.get("CustomerIdEquipMap");
				//CIEM.put("VerboseResponse", false);
				//CIEM.put("FindExistingSO", true);
				//CIEM.put("InactiveDt", orderedCiemRequestHashMap.get("InactiveDt"));
				HashMap CIEMReq = new HashMap();
				CIEMReq.put("CustomerIdEquipMap", CIEM);
				CIEMReq.put("Order",order);
				CIEMReq.put("VerboseResponse", false);
				CIEMReq.put("FindExistingSO", true);
				CIEMReq.put("InactiveDt", orderedCiemRequestHashMap.get("InactiveDt"));
				
				if (logger.isDebugEnabled())
					logger.debug("OrderedCiemDisconnectRequest " + DebugUtil.getStringBuffer(CIEMReq).toString());
				
				HashMap orderedCiemResponse = connection.call(context, ApiMappings.getCallName("OrderedCiemDisconnect"), CIEMReq);
				
				((HashMap)orderedCiemResponse.get("CustomerIdEquipMap")).put("SourceLineItemId", ((HashMap)orderedCiemRequestHashMap.get("CustomerIdEquipMap")).get("SourceLineItemId"));
				((HashMap)orderedCiemResponse.get("CustomerIdEquipMap")).put("ServiceLineId", ((HashMap)orderedCiemRequestHashMap.get("CustomerIdEquipMap")).get("ServiceLineId"));
			
				if (logger.isDebugEnabled())
					logger.debug("OrderedCiemDisconnectResponse " + DebugUtil.getStringBuffer(orderedCiemResponse).toString());
			
				orderedCiemResponseHashMappArray[i] = (HashMap)orderedCiemResponse.clone();
			}
		}
		
		//LoggerUtil.debug(PATH_LOG, "#### End orderedCiemDisconnectList ####", OrderedCiemImpl.class);
		logger.debug("#### End orderedCiemDisconnectList ####");
		return orderedCiemResponseHashMappArray;
	}
	
	@SuppressWarnings("unchecked")
	//public static HashMap[] orderedCiemCreateList(HashMap service, HashMap order, HashMap[] orderedCiemNew, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Exception {
	public static HashMap[] orderedCiemCreateList(HashMap service, HashMap order, HashMap[] orderedCiemNew, Connection connection, BSDMSessionContext context) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "#### Start orderedCiemCreateList ####", OrderedCiemImpl.class);
		logger.debug("#### Start orderedCiemCreateList ####");
		
		boolean checkArrayExist = (orderedCiemNew != null && orderedCiemNew.length >0);
		ArrayList<HashMap<String, Object>> responseList = new ArrayList<HashMap<String,Object>>();
		
		if (checkArrayExist){
			for (HashMap orderHm : orderedCiemNew){
				if(service != null){
					((HashMap)orderHm.get("CustomerIdEquipMap")).put("ServiceInternalId", service.get("ServiceInternalId"));
					((HashMap)orderHm.get("CustomerIdEquipMap")).put("ServiceInternalIdResets", service.get("ServiceInternalIdResets"));
				}
				
				HashMap CIEM = (HashMap)orderHm.get("CustomerIdEquipMap");
				//CIEM.put("FindExistingSO", true);
				//CIEM.put("VerboseResponse", false);
				
				HashMap CIEMReq = new HashMap();
				CIEMReq.put("CustomerIdEquipMap", CIEM);
				CIEMReq.put("Order", (HashMap)order.clone());
				CIEMReq.put("FindExistingSO", true);
				CIEMReq.put("VerboseResponse", false);
				
				//LoggerUtil.debugHashMap(PATH_LOG, "OrderedCiemNewRequest", OrderedCiemImpl.class, CIEMReq);
				if (logger.isDebugEnabled())
					logger.debug("OrderedCiemNewRequest " + DebugUtil.getStringBuffer(CIEMReq).toString());
				orderHm = connection.call(context, ApiMappings.getCallName("OrderedCiemCreate"), CIEMReq);
				//LoggerUtil.debugHashMap(PATH_LOG, "OrderedCiemNewResponse", OrderedCiemImpl.class, orderHm);
				if (logger.isDebugEnabled())
					logger.debug("OrderedCiemNewResponse " + DebugUtil.getStringBuffer(orderHm).toString());
				
				HashMap orderedCiemField = (HashMap)orderHm.get("CustomerIdEquipMap");
				orderHm.put("ServiceLineId", orderedCiemField.get("ServiceLineId"));
				orderHm.put("SourceLineItemId", orderedCiemField.get("SourceLineItemId"));
				
				responseList.add(orderHm);
			}
		}
		//LoggerUtil.debug(PATH_LOG, "#### End orderedCiemCreateList ####", OrderedCiemImpl.class);
		logger.debug("#### End orderedCiemCreateList ####");
		return ConverterUtil.toHashMapArray(responseList);
	}
}
