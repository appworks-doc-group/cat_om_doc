package com.cat.udb.esb.wrappers;

//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import org.apache.log4j.Logger;

//import com.csgsystems.aruba.connection.Connection;
import com.csgsystems.bali.connection.ApiMappings;
//import util.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.OrderedServiceBaseWrapper;

public class UpdateServiceOwnerWrapper extends OrderedServiceBaseWrapper {
	
	private static Logger logger = Logger.getLogger(UpdateServiceOwnerWrapper.class);

//    public UpdateServiceOwnerWrapper()
//    {
//        super(Constant.LOG4j_PROPERTIES_PATH_UDB_A6);
//        CLASS = getClass();
//    }

    protected void executeOrderedService() throws Exception {
        java.util.HashMap orderedService = MessageUtil.getOrderedService(request);
        //LoggerUtil.debugHashMap(PATH_LOG, "###### Call OrderedServiceTransfer ######", CLASS, orderedService);
        if (logger.isDebugEnabled())
        	logger.debug("###### Call OrderedServiceTransfer ###### " + DebugUtil.getStringBuffer(orderedService).toString());
        java.util.HashMap APIresponse = connection.call(context, ApiMappings.getCallName("OrderedServiceTransfer"), orderedService);
        //LoggerUtil.debugHashMap(PATH_LOG, "###### OrderedServiceTransfer Response ######", CLASS, APIresponse);
        if (logger.isDebugEnabled())
        	logger.debug("###### OrderedServiceTransfer Response ###### " + DebugUtil.getStringBuffer(APIresponse).toString());
        response = MessageUtil.setOrderedService(APIresponse, request);
    }

    //private Class CLASS;
}