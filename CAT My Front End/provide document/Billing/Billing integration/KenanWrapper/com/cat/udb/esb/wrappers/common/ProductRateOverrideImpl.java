package com.cat.udb.esb.wrappers.common;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.cat.udb.esb.core.kenan.utils.ConverterUtil;
//import util.HashMapHelper;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;
import com.cat.udb.esb.core.utils.DebugUtil;

//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.csgsystems.aruba.connection.Connection;
import com.csgsystems.bali.connection.ApiMappings;

public class ProductRateOverrideImpl {
	
	private static Logger logger = Logger.getLogger(ProductRateOverrideImpl.class);

//	public static HashMap createProductRateOverride(HashMap product, HashMap productRateOverrideField, 
//			Object currencyCode , Connection connection, BSDMSessionContext context, String PATH_LOG) throws Throwable{
	public static HashMap createProductRateOverride(HashMap product, HashMap productRateOverrideField, Object currencyCode , Connection connection, BSDMSessionContext context) throws Throwable{
		
		HashMap productRateOverrideCreateResponse = null;
	
		//Init request
		HashMap productRateOverrideKey = new HashMap();
		if (product != null) {
			productRateOverrideKey.put("ActiveDt", product.get("ProductActiveDt"));
			productRateOverrideKey.put("TrackingId", product.get("TrackingId"));
			productRateOverrideKey.put("TrackingIdServ", product.get("TrackingIdServ"));
			
			productRateOverrideField.put("Key", productRateOverrideKey);
		}
		if (currencyCode != null)
			productRateOverrideField.put("CurrencyCode",currencyCode);
		
		HashMap productRateOverride = new HashMap();
		productRateOverride.put("ProductRateOverride", productRateOverrideField);
		
		//Call API
		//LoggerUtil.debugHashMap(PATH_LOG,"###### ProductRateOverrideCreate Request ######" , ProductRateOverrideImpl.class, productRateOverride);
		if (logger.isDebugEnabled())
			logger.debug("###### ProductRateOverrideCreate Request ###### " + DebugUtil.getStringBuffer(productRateOverride).toString());
		productRateOverrideCreateResponse = connection.call(context, ApiMappings.getCallName("ProductRateOverrideCreate"), productRateOverride);
		
		//Set ViewId
		HashMap productRateOverrideCreateResponseField =  (HashMap)productRateOverrideCreateResponse.get("ProductRateOverride");
		if (productRateOverrideKey == null) 
			productRateOverrideKey = (HashMap)productRateOverrideCreateResponseField.get("Key");
		
		Object viewId = MessageUtil.getProductRateViewId(productRateOverrideKey);		
		productRateOverrideCreateResponseField.put("ViewId", viewId);
		
		//LoggerUtil.debugHashMap(PATH_LOG,"###### ProductRateOverrideCreate Response ######" , ProductRateOverrideImpl.class, productRateOverrideCreateResponse);
		if (logger.isDebugEnabled())
			logger.debug("###### ProductRateOverrideCreate Response ###### " + DebugUtil.getStringBuffer(productRateOverrideCreateResponse).toString());
		return productRateOverrideCreateResponse;
	}
	
	//public static HashMap deleteProductRateOverride(HashMap productRateOverride, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Throwable {
	public static HashMap deleteProductRateOverride(HashMap productRateOverride, Connection connection, BSDMSessionContext context) throws Throwable {
		if(productRateOverride != null){
			HashMap productRateOverrideResponse = connection.call(context, ApiMappings.getCallName("ProductRateOverrideDelete"), productRateOverride);
			return productRateOverrideResponse;
		}else{
			return null;
		}
	}
	
//	public static HashMap modifyProductRateOverride(HashMap productRateOverride, Connection connection, 
//			BSDMSessionContext context, String PATH_LOG) throws Throwable {
	public static HashMap modifyProductRateOverride(HashMap productRateOverride, Connection connection, BSDMSessionContext context) throws Throwable {
		
		if(productRateOverride != null){
			
			String actionType = productRateOverride.get("ActionType").toString();
			
			int trackingId = Integer.parseInt(((HashMap)productRateOverride.get("Key")).get("TrackingId").toString());
			int trackingIdServ = Integer.parseInt(((HashMap)productRateOverride.get("Key")).get("TrackingIdServ").toString());
			HashMap productRateOverrideFindData = productRateOverrideFindData(trackingId, trackingIdServ);
			List productRateOverrideFindResponse = ConverterUtil.getObjectList(connection.call(context, ApiMappings.getCallName("ProductRateOverrideFind"), productRateOverride), "ProductRateOverrideList", "ProductRateOverride" );
			
			
			HashMap productRateOverrideResponse = null;
			if(actionType.equals("CREATE")){
				if(productRateOverrideFindResponse.isEmpty()){
					((HashMap)productRateOverride).remove("InactiveDate");
//					productRateOverrideResponse = createProductRateOverride(null, productRateOverride, null, connection, context, PATH_LOG);
					productRateOverrideResponse = createProductRateOverride(null, productRateOverride, null, connection, context);
				}
				else if(productRateOverrideFindResponse.size() > 1){
					throw new Throwable("ProductRateOverride of Product.TrackingId and Product.TrackingIdServ is existing in system");
				}
			}
			else if(actionType.equals("UPDATE")){
				if(productRateOverrideFindResponse.isEmpty()){
					throw new Throwable("Not foud ProductRateOverride of Product.TrackingId and Product.TrackingIdServ");
				}
				else if(productRateOverrideFindResponse.size() > 1){
					throw new Throwable("ProductRateOverride of Product.TrackingId and Product.TrackingIdServ cannot active more than 1 record");
				}
				else{
					productRateOverrideResponse = (HashMap)productRateOverrideFindResponse.get(0);
					// Delete
					productRateOverrideResponse.put("InactiveDate", (Date)productRateOverride.get("InactiveDate"));
					//deleteProductRateOverride(productRateOverrideResponse, connection, context, PATH_LOG);
					deleteProductRateOverride(productRateOverrideResponse, connection, context);
					productRateOverrideResponse = null;
					
					// Create
					((HashMap)productRateOverride).remove("InactiveDate");
//					productRateOverrideResponse = createProductRateOverride(null, productRateOverride, null, connection, context, PATH_LOG);
					productRateOverrideResponse = createProductRateOverride(null, productRateOverride, null, connection, context);
				}
			}
			else if(actionType.equals("DELETE")){
				if(productRateOverrideFindResponse.isEmpty()){
					throw new Throwable("Not foud Product.TrackingId and Product.TrackingIdServ");
				}
				else if(productRateOverrideFindResponse.size() > 1){
					throw new Throwable("Product.TrackingId and Product.TrackingIdServ cannot active more than 1 record");
				}
				else{
					productRateOverrideResponse = (HashMap)productRateOverrideFindResponse.get(0);
					
					// Delete
					productRateOverrideResponse.put("InactiveDate", (Date)productRateOverride.get("InactiveDate"));
					//productRateOverrideResponse = deleteProductRateOverride(productRateOverrideResponse, connection, context, PATH_LOG);
					productRateOverrideResponse = deleteProductRateOverride(productRateOverrideResponse, connection, context);
				}
			}
			else{
				throw new Throwable("ProductRateOverride.ActionType doesn't match, ActionType[CREATE|UPDATE|DELETE]");
			}
			return productRateOverrideResponse;
			
		}else{
			return null;
		}
	}
	
//	public static HashMap  productRateOverrideProcessing(HashMap order, HashMap product , HashMap productRateOverrideRequest, 
//			Connection connection, BSDMSessionContext context, String PATH_LOG)  throws Throwable{
	public static HashMap  productRateOverrideProcessing(HashMap order, HashMap product , HashMap productRateOverrideRequest, 
			Connection connection, BSDMSessionContext context)  throws Throwable{
		
		if (productRateOverrideRequest != null){
			if(product != null){
				HashMap productRateOverrideKey = new HashMap();
				productRateOverrideKey.put("ActiveDt", (Date)product.get("ProductActiveDt"));
				productRateOverrideKey.put("TrackingId", Integer.parseInt(product.get("TrackingId").toString()));
				productRateOverrideKey.put("TrackingIdServ", Integer.parseInt(product.get("TrackingIdServ").toString()));
				
				productRateOverrideRequest.put("Key", productRateOverrideKey);
				productRateOverrideRequest.put("CurrencyCode", Integer.parseInt(product.get("CurrencyCode").toString()));
				productRateOverrideRequest.put("InactiveDate", (Date)product.get("ProductInactiveDt"));
				productRateOverrideKey = null;
			}

//			HashMap productRateOverrideResponse = modifyProductRateOverride(productRateOverrideRequest, connection, context, PATH_LOG);
			HashMap productRateOverrideResponse = modifyProductRateOverride(productRateOverrideRequest, connection, context);
			
			return productRateOverrideResponse;
		}
		else{
			return null;
		}
	}
	
	public static HashMap productRateOverrideFindData(Integer trackingId, Integer trackingIdServ) throws Throwable{
		HashMap condition = new HashMap();
		HashMap key = new HashMap();
		HashMap field = new HashMap();
		HashMap request = new HashMap();

		condition.put("Equal", trackingId);
		key.put("TrackingId", (HashMap)condition.clone());
		
		condition.clear();
		condition.put("Equal", trackingIdServ);
		key.put("TrackingIdServ", (HashMap)condition.clone());
		key.put("Fetch", true);
		field.put("Key", (HashMap)key.clone());
		
		condition.clear();
		condition.put("IsNull", true);
		field.put("InactiveDt", (HashMap)condition.clone());
		field.put("Fetch", true);
		
		request.put("ProductRateOverride", (HashMap)field.clone());
		
		return request;
	}
	
}
