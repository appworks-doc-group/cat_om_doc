package com.cat.udb.esb.core.kenan.wrappers;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cat.udb.esb.core.utils.DebugUtil;

//import util.Constant;
//import com.cat.udb.esb.core.kenan.utils.HashMapHelper;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.ibacss.esb.bo.convert.ServiceChangeConverter;
import com.cat.ibacss.esb.bo.convert.impl.ServiceChangeConverterImpl;

//import com.cat.udb.esb.core.utils.LoggerUtil;
import com.csgsystems.bali.connection.ApiMappings;
import commonj.sdo.DataObject;

public abstract class OrderedServiceBaseWrapper extends BaseWrapper {
	
	private static Logger logger = Logger.getLogger(OrderedServiceBaseWrapper.class);

	//private Class CLASS = this.getClass();
	
//	public OrderedServiceChangeBaseWrapper(String path_log) {
//		super(path_log);
//	}

	@Override
	protected void executeService() throws Throwable {
		logger.debug("executeService()...");
		try {
			this.request = mapToRequestHashMap(requestObj);
			
			//Create Order
			//LoggerUtil.debug(PATH_LOG, "###### Call Create Order ######", CLASS);
			logger.debug("###### Call Create Order ######");
			HashMap reqOrder = MessageUtil.getOrder(request);
			HashMap resOrder = connection.call(context, ApiMappings.getCallName("OrderCreate"),reqOrder);
			
			request = MessageUtil.updateOrder(resOrder, request);
			
			//Implement OrderService
			executeOrderedService();
			
			//Commit Order
			//LoggerUtil.debug(PATH_LOG, "###### Call Commit Order ######", CLASS);
			logger.debug("###### Call Commit Order ######");
			reqOrder = MessageUtil.getOrder(request);
			resOrder = connection.call(context, ApiMappings.getCallName("OrderCommit"),reqOrder);
			
			response = MessageUtil.updateOrder(request,resOrder);
		}
		catch (Exception e) {
			logger.error(DebugUtil.getStackTrace(e));
			throw e;
		}
		//return ;
	}
	
	
	@Override
	protected DataObject prepareResponseObj(DataObject reqObj, HashMap response, Throwable error) throws Exception {
			
			HashMap result = MessageUtil.getResult(error);
			String transactionLogId = MessageUtil.getTransactionLogId(reqObj); 
			DataObject responseObj = mapToDataObj(reqObj, request, result, transactionLogId);
			
			return responseObj;
	}
	
	private HashMap mapToRequestHashMap(DataObject requestObj) throws Throwable {
		HashMap commonHashMap = null;
		
		try {
//			Convert DataObject to hashmap
			ServiceChangeConverter reConverter = new ServiceChangeConverterImpl();
			commonHashMap =  reConverter.dataObject2HashMap(requestObj);
			//LoggerUtil.debugHashMap(PATH_LOG,"###### mapToRequestServiceHashMap ######" , CLASS, commonHashMap);
			logger.debug("###### mapToRequestServiceHashMap ######" + DebugUtil.getStringBuffer(commonHashMap).toString());
		}
		catch (Exception e) {
			logger.error(DebugUtil.getStackTrace(e));
			throw e;
		}
		
		return commonHashMap;
	}
	
	private DataObject mapToDataObj(DataObject requestObj, HashMap response, HashMap result, String transactionLogId) throws Exception {
		
		HashMap responseHashMap = new HashMap();
		DataObject responseObj;
		
		responseHashMap.put("Account", response);
		responseHashMap.put("Result", result);
		responseHashMap.put("TransactionLogId", transactionLogId);
		
		//LoggerUtil.debugHashMap(PATH_LOG, "###### mapToResponseServiceDataObj ######" , CLASS, responseHashMap);
		logger.debug("###### mapToResponseServiceDataObj ######" + DebugUtil.getStringBuffer(responseHashMap).toString());
		ServiceChangeConverter serviceChangeConvert = new ServiceChangeConverterImpl();
		
		short serviceType = serviceChangeConvert.getOrderedServiceType(requestObj);
		String concatId = serviceChangeConvert.getBillingServiceConcatId(requestObj);
		
		responseObj = serviceChangeConvert.hashMap2DataObject(responseHashMap, serviceType, concatId);

		return responseObj;
	}
	
	protected abstract void executeOrderedService() throws Exception;
}
