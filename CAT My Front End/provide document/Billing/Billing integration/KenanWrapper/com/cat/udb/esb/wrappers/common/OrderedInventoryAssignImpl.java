package com.cat.udb.esb.wrappers.common;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cat.udb.esb.core.kenan.utils.ConverterUtil;
//import util.HashMapHelper;
import com.cat.udb.esb.core.utils.DebugUtil;

//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.csgsystems.aruba.connection.Connection;
import com.csgsystems.bali.connection.ApiMappings;
//import com.ibm.ws.install.configmanager.actionengine.ant.utils.ForEachAntTask;

public class OrderedInventoryAssignImpl {
	
	private static Logger logger = Logger.getLogger(OrderedInventoryAssignImpl.class);

//	public static HashMap[] createOrderedInventoryAssign(HashMap[] orderedInventoryAssignList,HashMap service, HashMap order, 
//			Connection connection, BSDMSessionContext context, String PATH_LOG) throws Throwable{
	public static HashMap[] createOrderedInventoryAssign(HashMap[] orderedInventoryAssignList,HashMap service, HashMap order, 
			Connection connection, BSDMSessionContext context) throws Throwable{
		
		ArrayList orderedInventoryAssignResponseList  = new ArrayList();
		HashMap serviceField = (HashMap)service.get("Service");
		HashMap serviceKey = new HashMap();
		serviceKey.put("Key", (HashMap)serviceField.get("Key"));

		for (HashMap orderedInventoryAssign : orderedInventoryAssignList) {
			
			HashMap orderedInventoryAssignField = (HashMap)orderedInventoryAssign.get("OrderedInventoryAssign");
			//Init request
			orderedInventoryAssignField.putAll(order);
			orderedInventoryAssignField.put("FindExistingSO", true);
			orderedInventoryAssignField.put("Service",serviceKey);
			
			//Call API
			//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedInventoryAssign Request ######" , OrderedInventoryAssignImpl.class, orderedInventoryAssignField);
			if (logger.isDebugEnabled())
				logger.debug("###### OrderedInventoryAssign Request ###### " + DebugUtil.getStringBuffer(orderedInventoryAssignField).toString());
			HashMap orderedInventoryAssignResponse = connection.call(context, ApiMappings.getCallName("OrderedInventoryAssign"), orderedInventoryAssignField);
			
			//Set ServiceLineID & SourceLineItemId
			Object[] invElementList = (Object[])orderedInventoryAssignResponse.get("InvElementList");
			HashMap invElement = (HashMap)invElementList[0];
			invElement.put("ServiceLineId", serviceField.get("ServiceLineId"));
			invElement.put("SourceLineItemId", serviceField.get("SourceLineItemId"));
			orderedInventoryAssignField.put("InvElement", invElement);
			
			//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedInventoryAssign Response ######" , OrderedInventoryAssignImpl.class, orderedInventoryAssign);
			if (logger.isDebugEnabled())
				logger.debug("###### OrderedInventoryAssign Response ###### " + DebugUtil.getStringBuffer(orderedInventoryAssign).toString());
			orderedInventoryAssignResponseList.add(orderedInventoryAssign);
		}
		return ConverterUtil.toHashMapArray(orderedInventoryAssignResponseList);
	}
}
