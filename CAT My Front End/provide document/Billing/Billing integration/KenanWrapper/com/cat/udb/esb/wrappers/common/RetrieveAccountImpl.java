package com.cat.udb.esb.wrappers.common;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cat.udb.esb.core.kenan.utils.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;
import com.cat.udb.esb.core.kenan.utils.ConverterUtil;

import com.cat.udb.esb.core.utils.DebugUtil;

//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.csgsystems.aruba.connection.Connection;
import com.csgsystems.bali.connection.ApiMappings;

public class RetrieveAccountImpl {
	
	private static Logger logger = Logger.getLogger(RetrieveAccountImpl.class);

//	public static HashMap retrieveAccountByMobileNumber (HashMap APIRequest, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Throwable  {
	public static HashMap retrieveAccountByMobileNumber (HashMap APIRequest, Connection connection, BSDMSessionContext context) throws Throwable  {
		
		//LoggerUtil.debug(PATH_LOG, "#*-*#*-*#*-*# BEGIN RetrieveAccountImpl : retrieveAccountByMobileNumber #*-*#*-*#*-*#", RetrieveAccountImpl.class);
		logger.debug("#*-*#*-*#*-*# BEGIN RetrieveAccountImpl : retrieveAccountByMobileNumber #*-*#*-*#*-*#");
		
		// Initail variable
		HashMap customerIdEquipMapResponse = new HashMap();
		HashMap serviceFind = new HashMap();
		HashMap accountGet = new HashMap();
		HashMap accountGetResponse = new HashMap();
		
		// Call API "CustomerIdEquipMapFind"
		//LoggerUtil.debug(PATH_LOG, "###### Call Kenan API : CustomerIdEquipMapFind ######", RetrieveAccountImpl.class);
		logger.debug("###### Call Kenan API : CustomerIdEquipMapFind ######");
		HashMap map1 = connection.call(context, ApiMappings.getCallName("CustomerIdEquipMapFind"), APIRequest);
		HashMap [] customerIdEquipMapFindListResponse = ConverterUtil.getHashMapArray(map1, "CustomerIdEquipMapList", "CustomerIdEquipMap");
		if (customerIdEquipMapFindListResponse.length == 1){
			
			customerIdEquipMapResponse = (HashMap)((HashMap)customerIdEquipMapFindListResponse[0]).get("CustomerIdEquipMap");
			//LoggerUtil.debugHashMap(PATH_LOG, "###### customerIdEquipMapResponse ######", RetrieveAccountImpl.class, customerIdEquipMapResponse);
			if (logger.isDebugEnabled())
				logger.debug("###### customerIdEquipMapResponse ###### " + DebugUtil.getStringBuffer(customerIdEquipMapResponse).toString());
			serviceFind = serviceFindObject (Integer.parseInt(customerIdEquipMapResponse.get("ServiceInternalId").toString()), Integer.parseInt(customerIdEquipMapResponse.get("ServiceInternalIdResets").toString()));
			
			// Call API "ServiceFind"
			//LoggerUtil.debug(PATH_LOG, "###### Call Kenan API : ServiceFind ######", RetrieveAccountImpl.class);
			logger.debug("###### Call Kenan API : ServiceFind ######");
			HashMap map2 = connection.call(context, ApiMappings.getCallName("ServiceFind"), serviceFind);
			HashMap [] serviceFindResponseList = ConverterUtil.getHashMapArray(map2, "ServiceList", "Service");
			
			if (serviceFindResponseList.length == 1){
				serviceFind = new HashMap();
				serviceFind = (HashMap)((HashMap)serviceFindResponseList[0]).get("Service");
				//LoggerUtil.debugHashMap(PATH_LOG, "###### serviceFind ######", RetrieveAccountImpl.class, serviceFind);
				if (logger.isDebugEnabled())
					logger.debug("###### serviceFind ###### " + DebugUtil.getStringBuffer(serviceFind).toString());
				accountGet = accountGetObjectData( Integer.parseInt(serviceFind.get("ParentAccountInternalId").toString()));
//				accountGetResponse = accountGetDetail(accountGet, connection, context, PATH_LOG);
				accountGetResponse = accountGetDetail(accountGet, connection, context);
				accountGetResponse.put("ServerId", 3);
				
			}
			else if (serviceFindResponseList.length > 1){
				throw new Throwable("Found Service more than 1 record with condition " + serviceFind);
			}
			else
				throw new Throwable("Service not found with condition " + serviceFind);
			
			
		}
		else if (customerIdEquipMapFindListResponse.length > 1){
			throw new Throwable("Found Service External Id more than 1 record with condition " + (HashMap)APIRequest.get("CustomerIdEquipMap"));
		}
		else
			throw new Throwable("Service External Id not found with condition " + (HashMap)APIRequest.get("CustomerIdEquipMap"));
		
		//LoggerUtil.debug(PATH_LOG, "#*-*#*-*#*-*# END RetrieveAccountImpl : retrieveAccountByMobileNumber #*-*#*-*#*-*#", RetrieveAccountImpl.class);
		logger.debug("#*-*#*-*#*-*# END RetrieveAccountImpl : retrieveAccountByMobileNumber #*-*#*-*#*-*#");
		
		return accountGetResponse;
	}
	
//	public static HashMap retrieveAccountByInvoiceNumber (HashMap APIRequest, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Throwable  {
	public static HashMap retrieveAccountByInvoiceNumber (HashMap APIRequest, Connection connection, BSDMSessionContext context) throws Throwable {
		//LoggerUtil.debug(PATH_LOG, "#*-*#*-*#*-*# BEGIN RetrieveAccountImpl : retrieveAccountByInvoiceNumber #*-*#*-*#*-*#", RetrieveAccountImpl.class);
		logger.debug("#*-*#*-*#*-*# BEGIN RetrieveAccountImpl : retrieveAccountByInvoiceNumber #*-*#*-*#*-*#");
		
		// Initail variable
		HashMap invoiceData = new HashMap();
		HashMap accountGet = new HashMap();
		HashMap accountGetResponse = new HashMap();
		
		// Call API "InvoiceFind"
		//LoggerUtil.debug(PATH_LOG, "###### Call Kenan API : InvoiceFind ######", RetrieveAccountImpl.class);
		logger.debug("###### Call Kenan API : InvoiceFind ######");
		HashMap map1 = connection.call(context, ApiMappings.getCallName("InvoiceFind"), APIRequest);
		HashMap [] invoiceFindResponseList = ConverterUtil.getHashMapArray(map1, "InvoiceList", "Invoice");
		
		if (invoiceFindResponseList.length == 1){
			invoiceData = (HashMap)((HashMap)invoiceFindResponseList[0]).get("Invoice");
			//LoggerUtil.debugHashMap(PATH_LOG, "###### invoiceData ######", RetrieveAccountImpl.class, invoiceData);
			if (logger.isDebugEnabled())
				logger.debug("###### invoiceData ###### " + DebugUtil.getStringBuffer(invoiceData).toString());
			accountGet = accountGetObjectData( Integer.parseInt(invoiceData.get("AccountInternalId").toString()));
			//accountGetResponse = accountGetDetail(accountGet, connection, context, PATH_LOG);
			accountGetResponse = accountGetDetail(accountGet, connection, context);
			accountGetResponse.put("ServerId", 3);
			
		}
		else if (invoiceFindResponseList.length > 1){
			throw new Throwable("Found Invoice more than 1 record with condition " + (HashMap)APIRequest.get("Invoice"));
		}
		else
			throw new Throwable("Invoice not found with condition " + (HashMap)APIRequest.get("Invoice"));
		
		//LoggerUtil.debug(PATH_LOG, "#*-*#*-*#*-*# END RetrieveAccountImpl : retrieveAccountByInvoiceNumber #*-*#*-*#*-*#", RetrieveAccountImpl.class);
		logger.debug("#*-*#*-*#*-*# END RetrieveAccountImpl : retrieveAccountByInvoiceNumber #*-*#*-*#*-*#");
		
		return accountGetResponse;
	}
	
//	public static HashMap retrieveAccountList (HashMap APIRequest, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Throwable  {
	public static HashMap retrieveAccountList (HashMap APIRequest, Connection connection, BSDMSessionContext context) throws Throwable  {
		
		//LoggerUtil.debug(PATH_LOG, "#*-*#*-*#*-*# BEGIN RetrieveAccountImpl : retrieveAccountList #*-*#*-*#*-*#", RetrieveAccountImpl.class);
		logger.debug("#*-*#*-*#*-*# BEGIN RetrieveAccountImpl : retrieveAccountList #*-*#*-*#*-*#");
		
		// Initail variable
		HashMap accountLocateFindResponse = new HashMap();
		
		//Call API "AccountLocateFind"
		//LoggerUtil.debug(PATH_LOG, "###### Call Kenan API : AccountLocateFind ######", RetrieveAccountImpl.class);
		logger.debug("###### Call Kenan API : AccountLocateFind ######");
		//LoggerUtil.debugHashMap(PATH_LOG, "###### (HashMap)APIRequest.get(AccountLocateList) ######", RetrieveAccountImpl.class, (HashMap)APIRequest.get("AccountLocate"));
		if (logger.isDebugEnabled())
			logger.debug("###### (HashMap)APIRequest.get(AccountLocateList) ###### " + DebugUtil.getStringBuffer((HashMap)APIRequest.get("AccountLocate")).toString());
			
		accountLocateFindResponse = connection.call(context, ApiMappings.getCallName("AccountLocateFind"), APIRequest);
		
		if (Integer.parseInt(accountLocateFindResponse.get("TotalCount").toString()) <= 0){
			throw new Throwable("Account not found with condition " + (HashMap)APIRequest.get("AccountLocate"));
		}
		
		//LoggerUtil.debug(PATH_LOG, "#*-*#*-*#*-*# END RetrieveAccountImpl : retrieveAccountList #*-*#*-*#*-*#", RetrieveAccountImpl.class);
		logger.debug("#*-*#*-*#*-*# END RetrieveAccountImpl : retrieveAccountList #*-*#*-*#*-*#");
		
		return accountLocateFindResponse;
	}
	
	private static HashMap serviceFindObject(Integer serviceInternalId, Integer serviceInternalIdResets) throws Throwable{

		HashMap condition = new HashMap();
		HashMap field = new HashMap();
		HashMap serviceFind = new HashMap();
		
		condition.put("Equal",serviceInternalId);
		field.put("ServiceInternalId", (HashMap)condition.clone());
		
		condition.clear();
		condition.put("Equal",serviceInternalIdResets);
		field.put("ServiceInternalIdResets", (HashMap)condition.clone());
		
		condition.clear();
		condition.put("IsNull",true);
		field.put("ServiceInactiveDt",(HashMap)condition.clone());
		
		condition.clear();
		condition.put("Fetch",true);
		field.put("ParentAccountInternalId",(HashMap)condition.clone());
		
		serviceFind.put("Service",(HashMap)field.clone());
		HashMap serviceExt = new HashMap();
		serviceExt = (HashMap)serviceFind.get("Service");
		serviceExt.put("ExtendedData", MessageUtil.getExtendedData(serviceExt, Constant.SERVICE_EXTDATA));
		
		return serviceFind;
	}
	
	private static HashMap accountGetObjectData(Integer accountInternalId) throws Throwable{
		HashMap accountId = new HashMap();
		HashMap accountKey = new HashMap();
		HashMap accountGet = new HashMap();
		
		accountId.put("AccountInternalId", accountInternalId);
		accountKey.put("Key", accountId);
		accountGet.put("Account", accountKey);

		HashMap accountExt = new HashMap();
		accountExt = (HashMap)accountGet.get("Account");
		accountExt.put("ExtendedData", MessageUtil.getExtendedData(accountExt, Constant.ACCOUNT_EXTDATA));
		
		return accountGet;
	}
	
//	public static HashMap accountGetDetail(HashMap accountReq, Connection connection, BSDMSessionContext context, String PATH_LOG) throws Throwable{
	public static HashMap accountGetDetail(HashMap accountReq, Connection connection, BSDMSessionContext context) throws Throwable{

		HashMap request = new HashMap();
		request.put("Account", (HashMap)accountReq.get("Account"));

		//LoggerUtil.debugHashMap(PATH_LOG, "== accountReq ==", RetrieveAccountImpl.class, accountReq);
		if (logger.isDebugEnabled())
			logger.debug("== accountReq == " + DebugUtil.getStringBuffer(accountReq).toString());
		
		//Call Kenan API : "AccountGet"
		//LoggerUtil.debug(PATH_LOG, "###### Call Kenan API : AccountGet ######", RetrieveAccountImpl.class);
		logger.debug("###### Call Kenan API : AccountGet ######");
		HashMap tempAPIresponse = connection.call(context, ApiMappings.getCallName("AccountGet"), request);
		HashMap APIresponse = new HashMap();
		APIresponse.put("Account", (HashMap) tempAPIresponse.get("Account"));
		//LoggerUtil.debugHashMap(PATH_LOG, "APIresponse", RetrieveAccountImpl.class, APIresponse);
		if (logger.isDebugEnabled())
			logger.debug("APIresponse " + DebugUtil.getStringBuffer(APIresponse).toString());
		
		//Call Kenan Api : "AccountBalanceSummary"
		//LoggerUtil.debug(PATH_LOG, "###### Call Kenan API : AccountBalanceSummary ######", RetrieveAccountImpl.class);
		logger.debug("###### Call Kenan API : AccountBalanceSummary ######");
		HashMap APIresponseBalanceSum = connection.call(context, ApiMappings.getCallName("AccountBalanceSummary"), APIresponse);
		((HashMap)APIresponse.get("Account")).put("AccountBalanceSummary", APIresponseBalanceSum);
		//LoggerUtil.debugHashMap(PATH_LOG, "APIresponseBalanceSum", RetrieveAccountImpl.class, APIresponseBalanceSum);
		if (logger.isDebugEnabled())
			logger.debug("APIresponseBalanceSum " + DebugUtil.getStringBuffer(APIresponseBalanceSum).toString());
		
		// Retrieve current payment profile
		//LoggerUtil.debug(PATH_LOG, "###### Retrieve current payment profile ######", RetrieveAccountImpl.class);
		logger.debug("###### Retrieve current payment profile ######");
		HashMap paymentProfileKey = new HashMap();
		paymentProfileKey.put("ProfileId", ((HashMap)(APIresponse.get("Account"))).get("PaymentProfileId"));
		HashMap paymentProfileGetTemp = new HashMap();
		paymentProfileGetTemp.put("Key", paymentProfileKey);
		HashMap paymentProfileGet = new HashMap();
		paymentProfileGet.put("PaymentProfile", paymentProfileGetTemp);
		//LoggerUtil.debugHashMap(PATH_LOG, "paymentProfileGet", RetrieveAccountImpl.class, paymentProfileGet);
		if (logger.isDebugEnabled())
			logger.debug("paymentProfileGet " + DebugUtil.getStringBuffer(paymentProfileGet).toString());
				
		//Call Kenan Api : "PaymentProfileGet"
		//LoggerUtil.debug(PATH_LOG, "###### Call Kenan API : PaymentProfileGet ######", RetrieveAccountImpl.class);
		logger.debug("###### Call Kenan API : PaymentProfileGet ######");
		HashMap APIresponsePaymentProfile = connection.call(context, ApiMappings.getCallName("PaymentProfileGet"), paymentProfileGet);
		//LoggerUtil.debugHashMap(PATH_LOG, "APIresponsePaymentProfile", RetrieveAccountImpl.class, APIresponsePaymentProfile);
		if (logger.isDebugEnabled())
			logger.debug("APIresponsePaymentProfile " + DebugUtil.getStringBuffer(APIresponsePaymentProfile).toString());
		HashMap[] paymentProfiles = new HashMap[1];
		paymentProfiles[0] = APIresponsePaymentProfile;
		
		//Put Payment Profile
		((HashMap)APIresponse.get("Account")).put("PaymentProfiles", paymentProfiles);
		
		// Retrieve customer service center
		//LoggerUtil.debug(PATH_LOG, "######  Retrieve customer service center ######", RetrieveAccountImpl.class);
		logger.debug("######  Retrieve customer service center ######");
		HashMap obj = new HashMap();
		obj = customerServiceObjectData(APIresponse);
		//LoggerUtil.debugHashMap(PATH_LOG, "obj", RetrieveAccountImpl.class, obj);
		if (logger.isDebugEnabled())
			logger.debug("obj " + DebugUtil.getStringBuffer(obj).toString());
		
		//Call Kenan Api : "CustomerServiceCenterFind"
		//LoggerUtil.debug(PATH_LOG, "###### Call Kenan API : CustomerServiceCenterFind ######", RetrieveAccountImpl.class);
		logger.debug("###### Call Kenan API : CustomerServiceCenterFind ######");
		HashMap customerServiceCenterFindResponse = connection.call(context, ApiMappings.getCallName("CustomerServiceCenterFind"), obj);
		//LoggerUtil.debugHashMap(PATH_LOG, "customerServiceCenterFindResponse", RetrieveAccountImpl.class, customerServiceCenterFindResponse);
		if (logger.isDebugEnabled())
			logger.debug("customerServiceCenterFindResponse " + DebugUtil.getStringBuffer(customerServiceCenterFindResponse).toString());
		
		//LoggerUtil.debug(PATH_LOG, "###### call generateCustomerServiceCenterResponseList ######", RetrieveAccountImpl.class);
		logger.debug("###### call generateCustomerServiceCenterResponseList ######");
//		HashMap[] customerServiceCenterFindResponseList = generateCustomerServiceCenterResponseList(customerServiceCenterFindResponse, connection, context, PATH_LOG);
		HashMap[] customerServiceCenterFindResponseList = generateCustomerServiceCenterResponseList(customerServiceCenterFindResponse, connection, context);
		((HashMap)APIresponse.get("Account")).put("CustomerServiceCenters", customerServiceCenterFindResponseList);
		//LoggerUtil.debugHashMap(PATH_LOG, "APIresponse", RetrieveAccountImpl.class, APIresponse);
		if (logger.isDebugEnabled())
			logger.debug("APIresponse " + DebugUtil.getStringBuffer(APIresponse).toString());
		
		//LoggerUtil.debug(PATH_LOG, "###### RetrieveAccountImpl - accountGetDetail END ######", RetrieveAccountImpl.class);
		logger.debug("###### RetrieveAccountImpl - accountGetDetail END ######");
		
		return APIresponse;
	}
	
	private static HashMap customerServiceObjectData(HashMap APIresponse) throws Throwable{
		
		HashMap condition = new HashMap();
		HashMap obj = new HashMap();
		HashMap field = new HashMap();
		HashMap key = new HashMap();
		condition.put("Equal", ((HashMap)((HashMap)APIresponse.get("Account")).get("Key")).get("AccountInternalId") );
		key.put("AccountInternalId", (HashMap)condition.clone());
		key.put("Fetch", true);
		field.put("Key", (HashMap)key.clone());
		field.put("Fetch", true);
		obj.put("CustomerServiceCenter",(HashMap)field.clone());
		return obj;
	}
	
//	private static HashMap[] generateCustomerServiceCenterResponseList(HashMap customerServiceCenterFindResponse, Connection connection, BSDMSessionContext context, String PATH_LOG)throws Exception {
	private static HashMap[] generateCustomerServiceCenterResponseList(HashMap customerServiceCenterFindResponse, Connection connection, BSDMSessionContext context)throws Exception {
		
		//LoggerUtil.debug(PATH_LOG, "###### generateCustomerServiceCenterResponseList START ######", RetrieveAccountImpl.class);
		logger.debug("###### generateCustomerServiceCenterResponseList START ######");
		
		HashMap maps[] = null;
		maps = ConverterUtil.getHashMapArray(customerServiceCenterFindResponse, "CustomerServiceCenterList", "CustomerServiceCenter");
		int cscSize = maps.length;
		HashMap [] customerServiceCenterResponseList = new HashMap[cscSize];
		HashMap condition = new HashMap();
		HashMap obj = new HashMap();
		HashMap field = new HashMap();
		HashMap key = new HashMap();
		HashMap serviceCenterFindResponsemaps[] = null;
		HashMap customerServiceCenterResponse = new HashMap();
		HashMap serviceCenterFindResponse = new HashMap();
		HashMap serviceCenterGetResponse = new HashMap();
		HashMap customerService = new HashMap();
		
		try{
			for(int i=0;i<cscSize;i++){
				
				customerServiceCenterResponse = (HashMap)maps[i];
				customerService = (HashMap)customerServiceCenterResponse.get("CustomerServiceCenter");
				if(customerService.get("ServiceCenterId") != null){
					condition.put("Equal", customerService.get("ServiceCenterId"));
					key.put("ServiceCenterId", (HashMap)condition.clone());
					field.put("Key", (HashMap)key.clone());
					field.put("Fetch", true);
					obj.put("ServiceCenter", (HashMap)field.clone());
					
					//Call Kenan Api : "ServiceCenterFind" //ServiceCenter
					serviceCenterFindResponse = connection.call(context, ApiMappings.getCallName("ServiceCenterFind"), obj);
					serviceCenterFindResponsemaps = ConverterUtil.getHashMapArray(serviceCenterFindResponse, "ServiceCenterList", "ServiceCenter");
					
					int scSize = serviceCenterFindResponsemaps.length;
					if(scSize >= 1){
						//Call Kenan Api : "ServiceCenterGet"
						serviceCenterGetResponse = connection.call(context, ApiMappings.getCallName("ServiceCenterGet"), serviceCenterFindResponsemaps[0]);
						customerService.put("ScName", ((HashMap)serviceCenterGetResponse.get("ServiceCenter")).get("ScName")); //setScName(serviceCenterGetResponse.getScName());
						customerService.put("ScAddress1", ((HashMap)serviceCenterGetResponse.get("ServiceCenter")).get("ScAddress1")); // setScAddress1(serviceCenterGetResponse.getScAddress1());
						customerService.put("ScAddress2", ((HashMap)serviceCenterGetResponse.get("ServiceCenter")).get("ScAddress2")); //  setScAddress2(serviceCenterGetResponse.getScAddress2());
						customerService.put("ScAddress3", ((HashMap)serviceCenterGetResponse.get("ServiceCenter")).get("ScAddress3")); //  setScAddress3(serviceCenterGetResponse.getScAddress3());
						customerServiceCenterResponseList[i] = new HashMap();
						customerServiceCenterResponseList[i].put("CustomerServiceCenter", customerService); //= customerServiceCenterResponse;
					} else {
						throw new Exception("ServiceCenterId not found, make sure ServiceCenterId " + customerServiceCenterResponse.get("ServiceCenterId") + " is existing in the system.");
					}
				}
				else{
					throw new Exception("ServiceCenterId is null.");
				}
				
				condition.clear();
				obj.clear();
				field.clear();
				key.clear();
				serviceCenterFindResponsemaps = null;
				customerServiceCenterResponse = null;
				serviceCenterFindResponse = null;
				serviceCenterGetResponse = null;
				customerService = null;
			}
			//LoggerUtil.debug(PATH_LOG, "###### generateCustomerServiceCenterResponseList END in try ######", RetrieveAccountImpl.class);
			logger.debug("###### generateCustomerServiceCenterResponseList END in try ######");
		}
		catch(Exception e){
			//LoggerUtil.error(PATH_LOG, "###### generateCustomerServiceCenterResponseList END in catch ######", RetrieveAccountImpl.class);
			logger.error(DebugUtil.getStackTrace(e));
			throw e;
		}
		finally{
			maps = null;
			condition.clear();
			obj.clear();
			field.clear();
			key.clear();
			serviceCenterFindResponsemaps = null;
			customerServiceCenterResponse = null;
			serviceCenterFindResponse = null;
			serviceCenterGetResponse = null;
			customerService = null;
		}
		
		return customerServiceCenterResponseList;
	}
}
