package com.cat.udb.esb.wrappers;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

//import util.*;
import com.cat.udb.esb.core.kenan.utils.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.ibacss.esb.bo.convert.ServiceConverter;
import com.cat.ibacss.esb.bo.convert.impl.ServiceConverterUDBImpl;
import com.csgsystems.bali.connection.ApiMappings;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.BaseWrapper;

import com.cat.udb.esb.wrappers.common.OrderedCiemImpl;
import com.cat.udb.esb.wrappers.common.OrderedNrcImpl;
import com.cat.udb.esb.wrappers.common.OrderedContractImpl;
import com.cat.udb.esb.wrappers.common.OrderedPackageImpl;
import com.cat.udb.esb.wrappers.common.ComponentLevelImpl;
import com.cat.udb.esb.wrappers.common.OrderedInventoryAssignImpl;

//import wrapper.common.*;
import commonj.sdo.DataObject;

//import com.cat.ibacss.esb.bo.utils.LoggerUtil;

public class CreateServiceWrapper extends BaseWrapper {
	
	private static Logger logger = Logger.getLogger(CreateServiceWrapper.class);

//	private Class CLASS = this.getClass();
//	
//	public CreateServiceWrapper() {
//		super(Constant.LOG4j_PROPERITES_PATH_UDB_A2);
//	}

	@Override
	protected void executeService() throws Throwable {
		//LoggerUtil.debug(PATH_LOG, "###### START UDB_A2 CreateService ######", CLASS);
		logger.debug("###### START UDB_A2 CreateService ######");
		
		this.request = mapToRequestHashMap(requestObj);
		response = (HashMap)request.clone();
		
		//Create Order
		//LoggerUtil.debug(PATH_LOG, "###### Call Create Order ######", CLASS);
		logger.debug("###### Call Create Order ######");
		HashMap reqOrder = MessageUtil.getOrder(request);
		HashMap resOrder = connection.call(context, ApiMappings.getCallName("OrderCreate"),reqOrder);
		//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderCreate Response ######" , CLASS, resOrder);
		if (logger.isDebugEnabled())
			logger.debug("###### OrderCreate Response ######");
		
		request = MessageUtil.updateOrder(resOrder, request);
		
		HashMap order = MessageUtil.getOrder(request);
		
		//OrderedNrcCreate
		HashMap[] orderNrcsList = (HashMap[])request.get("OrderedNrcs");
		
		if(orderNrcsList != null){
			//LoggerUtil.debug(PATH_LOG, "###### OrderedNrcCreate ######", CLASS);
			logger.debug("###### OrderedNrcCreate ######");
			//Object[] orderedNrcs = OrderedNrcImpl.createOrderedNrc(orderNrcsList,null,order,connection, context ,PATH_LOG);
			Object[] orderedNrcs = OrderedNrcImpl.createOrderedNrc(orderNrcsList,null,order,connection, context);
			response.put("OrderedNrcs", orderedNrcs);
		}
		
		//OrderedContractCreate
		HashMap[] orderedContractList = (HashMap[])request.get("OrderedContracts");
		if(orderedContractList != null){
			//LoggerUtil.debug(PATH_LOG, "###### OrderedContractCreate ######", CLASS);
			logger.debug("###### OrderedContractCreate ######");
			//Object[] orderedContracts= OrderedContractImpl.createOrderedContract(orderedContractList, order,connection, context,PATH_LOG);
			Object[] orderedContracts= OrderedContractImpl.createOrderedContract(orderedContractList, order,connection, context);
			response.put("OrderedContracts", orderedContracts);
		}
		
		//OrderedPackageCreate
		HashMap[] orderedPackagesList = (HashMap[])request.get("ProductPackages");
		if(orderedPackagesList!=null){
			//LoggerUtil.debug(PATH_LOG, "###### ProductPackages ######", CLASS);
			logger.debug("###### ProductPackages ######");
			//Object[] orderedPackages= OrderedPackageImpl.createOrderedPackage(orderedPackagesList, order,connection, context,PATH_LOG);
			Object[] orderedPackages= OrderedPackageImpl.createOrderedPackage(orderedPackagesList, order,connection, context);
			response.put("ProductPackages", orderedPackages);
		}
		
		//Create Service Level
		HashMap[] orderedServiceList = (HashMap[])request.get("Services");
		if(orderedServiceList != null){
			//LoggerUtil.debug(PATH_LOG, "###### Create Service Level ######", CLASS);
			logger.debug("###### Create Service Level ######");
			ArrayList serviceResponsList = new ArrayList();
			
			for (HashMap orderedService : orderedServiceList) {
				HashMap serviceRequestField = (HashMap)orderedService.get("Service");
				Object currencyCode = serviceRequestField.get("CurrencyCode");
				//Create Service
				Object orderDetail = order.get("Order");
				orderedService.put("Order", orderDetail);
				orderedService.put("FindExistingSO", true);
				serviceRequestField.put("ExtendedData", MessageUtil.getExtendedData(serviceRequestField, Constant.SERVICE_EXTDATA));
				
				//LoggerUtil.debug(PATH_LOG, "###### Call OrderedServiceCreate ######", CLASS);
				logger.debug("###### Call OrderedServiceCreate ######");
				//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedServiceCreate Request ######" , CLASS, orderedService);
				if (logger.isDebugEnabled())
					logger.debug("###### OrderedServiceCreate Request ###### " + DebugUtil.getStringBuffer(orderedService).toString());
				
				HashMap orderedServiceCreateResponse = connection.call(context, ApiMappings.getCallName("OrderedServiceCreate"), orderedService);
				
				HashMap orderedServiceCreateResponseField = (HashMap)orderedServiceCreateResponse.get("Service");
				orderedServiceCreateResponseField.put("ServiceLineId", ((HashMap)orderedService.get("Service")).get("ServiceLineId"));
				orderedServiceCreateResponseField.put("SourceLineItemId", ((HashMap)orderedService.get("Service")).get("SourceLineItemId"));
				
				//LoggerUtil.debugHashMap(PATH_LOG,"###### OrderedServiceCreate Response ######" , CLASS, orderedServiceCreateResponse);
				if (logger.isDebugEnabled())
					logger.debug("###### OrderedServiceCreate Response ###### " + DebugUtil.getStringBuffer(orderedServiceCreateResponse).toString());
				
				if (orderedServiceCreateResponseField.containsKey("ExtendedData")){
					MessageUtil.setExtendedData(orderedServiceCreateResponseField);
				}
				
				//OrderedComponentCreate & CorridorCreate
				HashMap[] componentList = (HashMap[])serviceRequestField.get("Components");
				if(componentList != null){
					//LoggerUtil.debug(PATH_LOG, "###### Service-Level ComponentLevel(Components and Corridors) Create ######", CLASS);
					logger.debug("###### Service-Level ComponentLevel(Components and Corridors) Create ######");
					HashMap[] orderedPackages = (HashMap[])response.get("ProductPackages");
//					Object[] componentLevelResponse = ComponentLevelImpl.createComponentLevel(componentList, orderedServiceCreateResponse, order, 
//							orderedPackages, connection, context, PATH_LOG);
					Object[] componentLevelResponse = ComponentLevelImpl.createComponentLevel(componentList, orderedServiceCreateResponse, order, 
							orderedPackages, connection, context,currencyCode);
					((HashMap)orderedServiceCreateResponse.get("Service")).put("Components",componentLevelResponse);			
				}
				
				//OrderedCiemCreate
				HashMap[] orderedCiemList = (HashMap[])serviceRequestField.get("CustomerIdEquipMaps");
				if(orderedCiemList!=null){
					//LoggerUtil.debug(PATH_LOG, "###### Service-Level OrderedCiemCreate ######", CLASS);
					logger.debug("###### Service-Level OrderedCiemCreate ######");
					//Object[] orderedCiems = OrderedCiemImpl.createOrderedCiem(orderedCiemList,orderedServiceCreateResponse, order, connection, context,PATH_LOG);
					Object[] orderedCiems = OrderedCiemImpl.createOrderedCiem(orderedCiemList,orderedServiceCreateResponse, order, connection, context);
					((HashMap)orderedServiceCreateResponse.get("Service")).put("CustomerIdEquipMaps",orderedCiems);
				}
				
				//OrderedInventoryAssign
				HashMap[] orderedInventoryAssignList = (HashMap[])serviceRequestField.get("Inventories");
				if(orderedInventoryAssignList!=null){
					//LoggerUtil.debug(PATH_LOG, "######  Service-Level OrderedInventoryAssign ######", CLASS);
					logger.debug("######  Service-Level OrderedInventoryAssign ######");
					//Object[] orderedInventoryAssign = OrderedInventoryAssignImpl.createOrderedInventoryAssign(orderedInventoryAssignList,orderedServiceCreateResponse, order, connection, context,PATH_LOG);
					Object[] orderedInventoryAssign = OrderedInventoryAssignImpl.createOrderedInventoryAssign(orderedInventoryAssignList,orderedServiceCreateResponse, order, connection, context);
					((HashMap)orderedServiceCreateResponse.get("Service")).put("Inventories",orderedInventoryAssign);
				}
				
				//OrderedNrcCreate
				orderNrcsList = (HashMap[])serviceRequestField.get("OrderedNrcs");
				if(orderNrcsList != null){
					//LoggerUtil.debug(PATH_LOG, "######  Service-Level OrderedNrcCreate ######", CLASS);
					logger.debug("######  Service-Level OrderedNrcCreate ######");
					//Object[] orderedNrcs = OrderedNrcImpl.createOrderedNrc(orderNrcsList,orderedServiceCreateResponse,order, connection, context,PATH_LOG);
					Object[] orderedNrcs = OrderedNrcImpl.createOrderedNrc(orderNrcsList,orderedServiceCreateResponse,order, connection, context);
					((HashMap)orderedServiceCreateResponse.get("Service")).put("OrderedNrcs",orderedNrcs);
				}
							
				serviceResponsList.add(orderedServiceCreateResponse);
			}
			response.put("Services", serviceResponsList.toArray());
		}
		
		//Commit Order
		//LoggerUtil.debug(PATH_LOG, "######  Commit Order ######", CLASS);
		logger.debug("######  Commit Order ######");
		reqOrder = MessageUtil.getOrder(request);
		resOrder = connection.call(context, ApiMappings.getCallName("OrderCommit"),reqOrder);
		
		response = MessageUtil.updateOrder(resOrder, response);
		
		return ;
	}

	@Override
	protected DataObject prepareResponseObj(DataObject reqObj, HashMap response, Throwable error) throws Exception {
			
			HashMap result = MessageUtil.getResult(error);
			String transactionLogId = MessageUtil.getTransactionLogId(reqObj); 
			DataObject responseObj = mapToResponseDataObj(reqObj, response, result, transactionLogId);
			
			return responseObj;
	}
	
	private HashMap mapToRequestHashMap(DataObject requestObj) throws Throwable {
		
		//Convert DataObject to hashmap
		ServiceConverter reConverter = new ServiceConverterUDBImpl();
		HashMap commonHashMap =  reConverter.dataObject2HashMap(requestObj);
		//LoggerUtil.debugHashMap(PATH_LOG, "######  mapToRequestServiceHashMap ######", CLASS,commonHashMap );
		if (logger.isDebugEnabled())
			logger.debug("######  mapToRequestServiceHashMap ###### " + DebugUtil.getStringBuffer(commonHashMap).toString());
		return commonHashMap;
	}
	
	private DataObject mapToResponseDataObj(DataObject requestObj, HashMap response, HashMap result, String transactionLogId) throws Exception {
		
		HashMap responseHashMap = new HashMap();
		DataObject responseObj;
		
		responseHashMap.put("Account", response);
		responseHashMap.put("Result", result);
		responseHashMap.put("TransactionLogId", transactionLogId);
		
		//LoggerUtil.debugHashMap(PATH_LOG, "######  mapToResponseServiceDataObj ######", CLASS,responseHashMap );
		if (logger.isDebugEnabled())
			logger.debug("######  mapToResponseServiceDataObj ###### " + DebugUtil.getStringBuffer(responseHashMap).toString());
		
		ServiceConverter serviceConvert = new ServiceConverterUDBImpl();
		responseObj = serviceConvert.hashMap2DataObject(responseHashMap);

		return responseObj;
	}
}
