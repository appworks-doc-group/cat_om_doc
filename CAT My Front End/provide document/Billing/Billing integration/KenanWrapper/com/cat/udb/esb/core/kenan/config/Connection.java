package com.cat.udb.esb.core.kenan.config;

public interface Connection {

	public static final String CONNECTION_RETRY = "CONNECTION_RETRY";
	public static final String TRANSACTION_TIMEOUT = "TRANSACTION_TIMEOUT";
	
}
