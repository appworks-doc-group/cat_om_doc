package com.cat.udb.esb.wrappers.common;

import java.util.HashMap;
//import java.util.Iterator;
//import java.util.List;
import java.util.Date;

import org.apache.log4j.Logger;

//import com.cat.udb.esb.core.kenan.utils.ConverterUtil;
//import util.HashMapHelper;
import com.cat.udb.esb.core.utils.DebugUtil;

//import com.cat.ibacss.esb.bo.utils.LoggerUtil;
import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.csgsystems.aruba.connection.Connection;
import com.csgsystems.bali.connection.ApiMappings;

public class CorridorImpl {
	
	private static Logger logger = Logger.getLogger(CorridorImpl.class);
		
//	public static HashMap createCorridor(HashMap corridor, HashMap service, Connection connection, 
//			BSDMSessionContext context,String PATH_LOG) throws Throwable{
	public static HashMap createCorridor(HashMap corridor, HashMap service, Connection connection, 
			BSDMSessionContext context) throws Throwable{
		
		//Validate CorridorPlanGroup
		HashMap corridorField = (HashMap)corridor.get("Corridor");
		HashMap serviceField = null;
		if (service != null)
			serviceField = (HashMap)service.get("Service");

		HashMap corridorPlanGroupTarget = new HashMap();
		HashMap corridorPlanGroupKey = new HashMap();
		HashMap corridorPlanGroupRequest = new HashMap();
		
		//Set Corridor Plan Group search condition
		HashMap condition = new HashMap();
		condition.put("Equal", corridorField.get("CorridorPlanId"));
		corridorPlanGroupTarget.put("CorridorPlanId", condition);
		corridorPlanGroupTarget.put("Fetch", true);
		corridorPlanGroupKey.put("Key",corridorPlanGroupTarget);
		corridorPlanGroupRequest.put("CorridorPlanGroup", corridorPlanGroupKey);
		corridorPlanGroupRequest.put("Fetch", true);
		
		//LoggerUtil.debugHashMap(PATH_LOG,"###### CorridorPlanGroupFind Request ######" , CorridorImpl.class , corridorPlanGroupRequest);
		if (logger.isDebugEnabled())
			logger.debug("###### CorridorPlanGroupFind Request ###### " + DebugUtil.getStringBuffer(corridorPlanGroupRequest).toString());
		
		//Call CorridorPlanGroupFind
		HashMap crdPlanGroupFindResponse = connection.call(context, ApiMappings.getCallName("CorridorPlanGroupFind"), corridorPlanGroupRequest);
		//LoggerUtil.debugHashMap(PATH_LOG,"###### CorridorPlanGroupFind Response ######" , CorridorImpl.class , crdPlanGroupFindResponse);
		if (logger.isDebugEnabled())
			logger.debug("###### CorridorPlanGroupFind Response ###### " + DebugUtil.getStringBuffer(crdPlanGroupFindResponse).toString());
		
		Object[] crdPlanGroupFind = (Object[])crdPlanGroupFindResponse.get("CorridorPlanGroupList");
		
		if(crdPlanGroupFind == null){
			throw new Throwable("Corridor Plan not found.");
		}

		HashMap corridorCreateResponse = null;
		//Set CorridorCreate request from CorridorPlanGroup
		for (Object object : crdPlanGroupFind) {
			HashMap crdPlanGroup = (HashMap)object;
			if (serviceField != null){
				corridorField.put("ServiceInternalId", serviceField.get("ServiceInternalId"));
				corridorField.put("ServiceInternalIdResets", serviceField.get("ServiceInternalIdResets"));				
			}
			corridorPlanGroupKey = (HashMap)crdPlanGroup.get("Key");
			int targetIsXact = (Integer)(corridorPlanGroupKey.get("TargetIsXact"));
			corridorField.put("OriginIsXact", targetIsXact);
			corridorField.put("PointCategory", corridorPlanGroupKey.get("PointCategory"));
			corridorField.put("PointTarget", corridorPlanGroupKey.get("PointTarget"));
			
			if(targetIsXact == 1){
				corridorField.put("TargetIsXact", true);
			}
			else{
				corridorField.put("TargetIsXact", false);
			}
			
			//LoggerUtil.debugHashMap(PATH_LOG,"###### CorridorCreate Request ######" , CorridorImpl.class , corridor);
			if (logger.isDebugEnabled())
				logger.debug("###### CorridorCreate Request ###### " + DebugUtil.getStringBuffer(corridor).toString());
			
			//Call API
			corridorCreateResponse = connection.call(context, ApiMappings.getCallName("CorridorCreate"), corridor);
		}
		//LoggerUtil.debugHashMap(PATH_LOG,"###### CorridorCreate Response ######" , CorridorImpl.class , corridorCreateResponse);
		if (logger.isDebugEnabled())
			logger.debug("###### CorridorCreate Response ###### " + DebugUtil.getStringBuffer(corridorCreateResponse).toString());
		return corridorCreateResponse;
	}
	
//	public static void updateCorridor(HashMap corridor, HashMap service, Connection connection, 
//			BSDMSessionContext context,String PATH_LOG) throws Throwable{
	public static void updateCorridor(HashMap corridor, HashMap service, Connection connection, BSDMSessionContext context) throws Throwable{
		
		HashMap corridorField = (HashMap)corridor.get("Corridor");
		
		HashMap condition = new HashMap();
		HashMap field = new HashMap();
		HashMap request = new HashMap();
		
		condition.clear();
		condition.put("Equal", Integer.parseInt(corridorField.get("AccountInternalId").toString()));
		field.put("AccountInternalId", (HashMap)condition.clone());
		condition.clear();
		condition.put("Equal", Integer.parseInt(corridorField.get("CorridorPlanId").toString()));
		field.put("CorridorPlanId", (HashMap)condition.clone());
		condition.clear();
		condition.put("Equal", Integer.parseInt(corridorField.get("ServiceInternalId").toString()));
		field.put("ServiceInternalId", (HashMap)condition.clone());
		condition.clear();
		condition.put("Equal", Integer.parseInt(corridorField.get("ServiceInternalIdResets").toString()));
		field.put("ServiceInternalIdResets", (HashMap)condition.clone());
		condition.clear();
		field.put("Fetch", true);
		request.put("Corridor", (HashMap)field.clone());
		field = null;
		HashMap response = connection.call(context, ApiMappings.getCallName("CorridorFind"), request);
		Object[] corridorFindLst = (Object[])response.get("CorridorList");
		
		if(corridorFindLst == null){
			throw new Throwable("Corridor Plan not found.");
		}
		//Set CorridorCreate request from CorridorPlanGroup
		for (Object object : corridorFindLst) {
			HashMap crdPlan = (HashMap)object;
			HashMap corridorPlanGroupKey = (HashMap)crdPlan.get("Key");
			HashMap corridorRequest_tmp = new HashMap();
			corridorRequest_tmp.put("Key", corridorPlanGroupKey);
			corridorRequest_tmp.put("InactiveDt", (Date)corridorField.get("InactiveDt"));
			HashMap corridorRequest = new HashMap();
			corridorRequest.put("Corridor", corridorRequest_tmp);

			//Call API
			connection.call(context, ApiMappings.getCallName("CorridorUpdate"), corridorRequest);
		}	
	}
}
