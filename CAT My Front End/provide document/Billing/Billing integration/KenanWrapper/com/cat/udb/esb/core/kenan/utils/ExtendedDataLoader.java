package com.cat.udb.esb.core.kenan.utils;

import java.io.IOException;
import java.util.Properties;

public class ExtendedDataLoader {

	public static Properties prop = null;
	
	public static synchronized Properties load() throws Exception {
		if(prop == null) {
			prop = new Properties();
			try {
				prop.load(ClassLoader.getSystemResourceAsStream(Constant.API_EXTDATA));
			} catch (IOException e) {
				System.err.println("Not found Configuration File " + Constant.API_EXTDATA + " in the classpath");
				throw e;
			} catch(NullPointerException e){
				System.err.println("Not found Configuration File " + Constant.API_EXTDATA + " in the classpath");
				throw e;
			}
			return prop;
		}
		else{
			return prop;
		}
	}
	
}
