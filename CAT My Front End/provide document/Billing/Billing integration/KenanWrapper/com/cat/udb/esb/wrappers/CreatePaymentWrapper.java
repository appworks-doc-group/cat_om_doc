package com.cat.udb.esb.wrappers;

import java.util.HashMap;

import org.apache.log4j.Logger;

//import com.cat.udb.esb.core.kenan.utils.Constant;
import com.cat.udb.esb.core.kenan.utils.MessageUtil;

import com.cat.ibacss.esb.bo.convert.PaymentConverter;
import com.cat.ibacss.esb.bo.convert.impl.PaymentConverterImpl;
//import com.cat.ibacss.esb.bo.utils.LoggerUtil;

import com.cat.udb.esb.core.utils.DebugUtil;
import com.cat.udb.esb.core.kenan.wrappers.BaseWrapper;

import com.csgsystems.bali.connection.ApiMappings;
import commonj.sdo.DataObject;

public class CreatePaymentWrapper extends BaseWrapper {
	
	private static Logger logger = Logger.getLogger(CreatePaymentWrapper.class);
//	private Class CLASS = this.getClass();
//
//	public CreatePaymentWrapper() {
//		super(Constant.LOG4j_PROPERTIES_PATH_UDB_F4);
//	}

	@SuppressWarnings("unchecked")
	@Override
	protected void executeService() throws Throwable {
		//LoggerUtil.debug(PATH_LOG, "#### Start executeService ####", CLASS);
		logger.debug("#### Start executeService ####");
		
		request = mapToRequestServiceHashMap(requestObj);
		HashMap payment = (HashMap)request.get("Payment");
		HashMap paymentCreate = new HashMap();
		payment.remove("Key");
		paymentCreate.put("Payment", (HashMap)payment.clone());
		
		HashMap paymentReq = new HashMap();
		paymentReq.put("Payment", paymentCreate);
		
		//LoggerUtil.debugHashMap(PATH_LOG, "PaymetCreateRequest", CLASS, paymentCreate);
		if (logger.isDebugEnabled())
			logger.debug("PaymetCreateRequest " + DebugUtil.getStringBuffer(paymentCreate).toString());
		HashMap paymentCreateResponse = connection.call(context, ApiMappings.getCallName("PaymentCreate"), paymentCreate);
		//LoggerUtil.debugHashMap(PATH_LOG, "PaymentCreateResponse", CLASS, paymentCreateResponse);
		if (logger.isDebugEnabled())
			logger.debug("PaymentCreateResponse " + DebugUtil.getStringBuffer(paymentCreateResponse));
		
		response = paymentCreateResponse;
		
		//LoggerUtil.debug(PATH_LOG, "#### End executeService ####", CLASS);
		logger.debug("#### End executeService ####");
	}

	@SuppressWarnings("unchecked")
	@Override
	protected DataObject prepareResponseObj(DataObject reqObj, HashMap response, Throwable error) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "#### Start prepareResponseObj ####", CLASS);
		logger.debug("#### Start prepareResponseObj ####");
		
		if(response == null){
			throw new Exception("response from Kenan API is null.");
		}
		HashMap result = MessageUtil.getResult(error);
		String transactionLogId = MessageUtil.getTransactionLogId(reqObj);
		response.put("Result", result);
		response.put("TransactionLogId", transactionLogId);
		DataObject responseObj = mapToResponseServiceObj(response);
		
		//LoggerUtil.debug(PATH_LOG, "#### Start prepareResponseObj ####", CLASS);
		logger.debug("#### Start prepareResponseObj ####");
		
		return responseObj;
	}
	
	private HashMap mapToRequestServiceHashMap(DataObject reqObj) throws Exception{
		//LoggerUtil.debug(PATH_LOG, "#### Start mapToRequestServiceHashMap ####", CLASS);
		logger.debug("#### Start mapToRequestServiceHashMap ####");
		HashMap responseHashMap = null;
		PaymentConverter converter = new PaymentConverterImpl();
		responseHashMap = converter.dataObject2HashMap(reqObj);
		//LoggerUtil.debugHashMap(PATH_LOG, "CreatePaymentRequest", CLASS, responseHashMap);
		if (logger.isDebugEnabled())
			logger.debug("CreatePaymentRequest " + DebugUtil.getStringBuffer(responseHashMap).toString());
		//LoggerUtil.debug(PATH_LOG, "#### End mapToRequestServiceHashMap ####", CLASS);
		logger.debug("#### End mapToRequestServiceHashMap ####");
		
		return responseHashMap;
	}
	
	private DataObject mapToResponseServiceObj(HashMap respHM) throws Exception {
		//LoggerUtil.debug(PATH_LOG, "#### Start mapToResponseServiceObj ####", CLASS);
		logger.debug("#### Start mapToResponseServiceObj ####");
		//LoggerUtil.debugHashMap(PATH_LOG, "CreatePaymentResponse", CLASS, respHM);
		if (logger.isDebugEnabled())
			logger.debug("CreatePaymentResponse " + DebugUtil.getStringBuffer(respHM).toString());
		DataObject respObj = null;
		PaymentConverter converter = new PaymentConverterImpl();
		respObj = converter.hashMapCreate2DataObject(respHM);
		//LoggerUtil.debug(PATH_LOG, "#### End mapToResponseServiceObj ####", CLASS);
		logger.debug("#### End mapToResponseServiceObj ####");
		
		return respObj;
	}

}
