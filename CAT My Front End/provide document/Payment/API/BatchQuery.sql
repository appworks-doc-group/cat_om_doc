select
  DISTINCT trunc(sysdate) PERIOD,
  'RECON_MY',
  sv.SUBSCR_NO,
  pay.ACCOUNT_NO,
  acct.EXTERNAL_ID ACCOUNT_EXT_ID,
  equip.EXTERNAL_ID SERVICE_EXT_ID,
  sysdate,
  'BATCH_' || to_char(sysdate, 'YYYYMMDD'),
  sv.VIEW_ID,
  sv.VIEW_ID,
  'MY' BILL_GROUP
from
  bmf pay
  join customer_id_acct_map acct 
    on pay.ACCOUNT_NO = acct.ACCOUNT_NO
    and acct.IS_CURRENT = 1
    and acct.EXTERNAL_ID_TYPE = 1
  join service sv 
    on pay.ACCOUNT_NO = sv.PARENT_ACCOUNT_NO
  join SERVICE_STATUS sv_st 
    on sv.subscr_no = sv_st.SUBSCR_NO
    and sv_st.INACTIVE_DT is null
  join customer_id_equip_map equip 
    on sv.SUBSCR_NO = equip.SUBSCR_NO
    and equip.IS_CURRENT = 1
    and equip.EXTERNAL_ID_TYPE in (17)
  left join cmf c 
    on sv.PARENT_ACCOUNT_NO = c.ACCOUNT_NO
  left join MKT_CODE_VALUES m 
    on c.MKT_CODE = m.MKT_CODE
where
  to_number(to_char(pay.POST_DATE, 'YYYYMMDD')) <= to_number(to_char(sysdate, 'YYYYMMDD'))
  and not exists(
    select
      1
    from
      cmf_balance bal
    where
      bal.BALANCE_DUE > 0
      and length(bal.BILL_REF_NO) > 6
      and bal.PPDD_DATE < sysdate
      and bal.ACCOUNT_NO = pay.ACCOUNT_NO
  )
  and sv_st.status_reason_id in (2) -- 2 Operator Initiate , 5 M Debt
  and sv_st.STATUS_ID = 3 -- 3 Suspended , -- 1 Active
  and not exists (
    select
      1
    from
      ord_service_order sord
    where
      sord.SUBSCR_NO = sv.SUBSCR_NO
      and sord.ORDER_STATUS_ID < 80
      and sord.SERVICE_ORDER_TYPE_ID = 70
  )
  and not exists (
    select
      1
    from
      CCUSER.AUTO_RECON_MY_CUT cut
    where
      CUT.CONTRACT = acct.EXTERNAL_ID
  ) -- add by Atapon 2016/01/07
  and not exists (
    select
      1
    from
      CAT_WRT_DEBT_DTL w
    where
      w.ACCOUNT_NO = pay.ACCOUNT_NO
      and w.WRT_STATUS = 4
  ) -- Exclude write off
;