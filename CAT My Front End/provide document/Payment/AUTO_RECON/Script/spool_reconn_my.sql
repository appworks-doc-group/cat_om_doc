drop table ccuser.auto_recon_my_sub_tmp purge
/

create table ccuser.auto_recon_my_sub_tmp as
select 
   CONTRNO, 
   ACCOUNT_NO, 
   SUBSCR_NO, 
   EXTERNAL_ID, 
   STATUS_ID, 
   DISPLAY_VALUE, 
   STATUS_REASON_ID, 
   ACTIVE_DT, 
   SERVICE_INACTIVE_DT, 
   to_char(sysdate,'YYYY-MM-DD HH24:MI:SS') as log_date,
   '&file_recon' as file_name 
from ccuser.auto_recon_sub_all a   /*=========================*/
where status_id = 3
and status_reason_id = 2
and bill_group in ('45','66','67')    /*========== Bill group ===============*/
and contrno
not in (
select contract
from ccuser.auto_recon_my_log
where substr(log_date,1,10) = to_char(sysdate,'YYYY-MM-DD')
)
/

set heading off
set newpage 0
set space 0
set linesize 2000
set pagesize 0
set echo off
set feedback off
set verify off
set heading off
set markup html off spool off
set termout off
set colsep <|
set trimspool on

spool &file_name

select 
	rpad(contrno,10,' ') || 
	rpad(account_no,10,' ') || 
	rpad(trim(subscr_no),16,' ') ||
	rpad(trim(external_id),16,' ')
from  ccuser.auto_recon_my_sub_tmp
/

spool off;

insert into ccuser.auto_recon_my_log
select *
from ccuser.auto_recon_my_sub_tmp
/


commit;


