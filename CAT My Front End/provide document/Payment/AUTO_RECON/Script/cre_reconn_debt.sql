set timing on

drop table ccuser.auto_recon_contract_all purge;

-- Create receipt&invoice all
--
create table ccuser.auto_recon_contract_all as
select  b.rowid as receipt_id, b.pos_no , b.receipt_type, b.receipt_no , b.receipt_date , a.contrno as contrno , a.bill_group , c.invoice_no ,  d.cheque_flag
from contract@webbill a
--
join receipt@webbill b
on
    a.contrno = b.contrno
and a.bill_group in ('45','66','67','H2','NH','HO','40')  -- Bill Group
and b.receipt_status = 'N'
--and substr(b.create_datetime,1,10) = to_char(sysdate,'YYYY-MM-DD')
and b.receipt_date = to_char(sysdate,'YYYY-MM-DD') || ' 00:00:00'
--
join invoice@webbill c
on
    b.pos_no = c.pos_no
and b.receipt_type = c.receipt_type
and b.receipt_no = c.receipt_no
and b.receipt_date = c.receipt_date
--
join catpos.account@webbill d
on
   c.invoice_no = d.ar_ref
/

-- delete writeoff
--
delete ccuser.auto_recon_contract_all
where receipt_id in (
select receipt_id
from  ccuser.auto_recon_contract_all
where cheque_flag > 0
)
/

commit;

-- H2 hinet      308   94   3082 
-- HN Cable      339   94   3088
-- HO C Internet 340   64   3089    * CAT Onnet
-- 40 hotnet     341   94   3411

-- 45 my           110    17     1101
-- 65 my IP VPN    110    17     1102
-- 66 my CAT Staff 110    17     1103
-- 69 my USO       110    17     1106

--#============================================================================

drop table ccuser.auto_recon_accno_tmp_all purge;

create table ccuser.auto_recon_accno_tmp_all as
select b.contrno , contr.account_no, bill_group, max(b.ar_duedate) as max_due
from (select distinct contrno , bill_group from  ccuser.auto_recon_contract_all) a
join account@webbill b
on 
    a.contrno = b.contrno
and substr(b.ar_duedate,1,10) <= to_char(sysdate,'YYYY-MM-DD')
--
join arbor.customer_id_acct_map contr
on 
      b.contrno = contr.external_id
and  contr.EXTERNAL_ID_TYPE = 1
--
group by b.contrno ,contr.account_no , bill_group
having sum(b.ar_am_debt) = 0
/

--============================================================================


drop table ccuser.auto_recon_sub_all purge;

-- Create sub status
--
create table ccuser.auto_recon_sub_all 
as select a1.contrno  
       ,a1.account_no  
      ,a1.bill_group
      ,a2.subscr_no 
      ,a6.EXTERNAL_ID 
      ,a4.STATUS_ID
      ,a4.DISPLAY_VALUE 
      ,a5.STATUS_REASON_ID
      ,a3.active_dt
      ,a2.service_inactive_dt 
      ,rank() over (partition by a2.parent_account_no,a6.external_id
       order by trunc(a3.active_dt) desc ,a2.subscr_no desc) service_rank
from  (select distinct account_no,contrno,bill_group  from  ccuser.auto_recon_accno_tmp_all) a1
join   arbor.service a2
on     a2.PARENT_ACCOUNT_NO = a1.ACCOUNT_NO
join   arbor.SERVICE_STATUS a3
on     a2.SUBSCR_NO = a3.SUBSCR_NO
and    a3.ACTIVE_DT <= sysdate
and   (a3.INACTIVE_DT > sysdate or a3.INACTIVE_DT is null)
--and   a3.status_id = 3
--and   a3.STATUS_REASON_ID = 2
join   arbor.status_values a4
on     a3.STATUS_ID = a4.STATUS_ID
join   arbor.status_reason_values a5
on     a3.STATUS_REASON_ID = a5.STATUS_REASON_ID
join   arbor.customer_id_equip_map a6
on     a2.SUBSCR_NO = a6.SUBSCR_NO
and    a6.EXTERNAL_ID_TYPE in (17,64,94)
and    a6.is_current=1
--join arbor.cat_map_ext_id cei 
--on a2.emf_config_id = cei.emf_config_id 
--and a6.external_id_type = cei.external_id_type
--
order by a1.contrno, a3.active_dt
/

insert into ccuser.auto_recon_sub_all_log
select a.* , sysdate
from ccuser.auto_recon_sub_all a
/

commit;


-- H2 hinet      308   94   3082 
-- HN Cable      339   94   3088
-- HO C Internet 340   64   3089    * CAT Onnet
-- 40 hotnet     341   94   3411

-- 45 my           110    17     1101
-- 65 my IP VPN    110    17     1102
-- 66 my CAT Staff 110    17     1103
-- 69 my USO       110    17     1106


--status_id = 3
--and status_reason_id = 2
