def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar4 = destReturn.QueryCreditResponse.QueryCreditResult.ResultMessage

def destMidVar0 = destMidVar4.MessageHeader

def srcMidVar = srcReturn.ocs11ResultHeader

destMidVar0.CommandId = srcMidVar.commandId

destMidVar0.TenantId = srcMidVar.tenantId

destMidVar0.Version = srcMidVar.version

destMidVar0.TransactionId = srcMidVar.transactionId

destMidVar0.SequenceId = srcMidVar.sequenceId

destMidVar0.ResultCode = srcMidVar.resultCode

destMidVar0.ResultDesc = srcMidVar.resultDesc

destMidVar0.Language = srcMidVar.language

def listMapping1

listMapping1 =
		{
			src,dest  ->

				dest.acctId = src.acctId

				dest.acctCode = src.acctCode

				dest.balanceType = src.balanceType

				dest.Amount = src.amount

		}

def listMapping2

listMapping2 =
		{
			src,dest  ->

				mappingList(src.queryCreditResultValueTypeList,dest.CreditResultValue,listMapping1)
		}

def destMidVar1 = destReturn.QueryCreditResponse.QueryCreditResult.ResultMessage
mappingList(srcReturn.queryCreditResult,destMidVar1.CreditResultList,listMapping2)
