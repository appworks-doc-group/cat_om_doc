dest.setServiceOperation("GroupCugService","changeCUGMemCallList")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.groupcug.changecugmemcalllist.io.ChangeCUGMemCallListRequest"

def listMapping0

listMapping0 =
{
	src,dest  ->

	dest.code = src.Code

	dest.value = src.Value

}

def srcMidVar0 = srcArgs0.ChangeCUGMemCallListRequestMsg.RequestHeader

def srcMidVar1 = srcArgs0.ChangeCUGMemCallListRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar1.LoginSystemCode

destArgs0.password = srcMidVar1.Password

destArgs0.remoteAddress = srcMidVar1.RemoteIP

mappingList(srcMidVar0.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.version = srcMidVar0.Version

destArgs0.businessCode = srcMidVar0.BusinessCode

destArgs0.messageSeq = srcMidVar0.MessageSeq

destArgs0.msgLanguageCode = srcMidVar0.MsgLanguageCode

destArgs0.interMode = srcMidVar0.AccessMode

def srcMidVar2 = srcArgs0.ChangeCUGMemCallListRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar2.ChannelID

destArgs0.operatorId = srcMidVar2.OperatorID

def srcMidVar3 = srcArgs0.ChangeCUGMemCallListRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar3.BEID

destArgs0.brId = srcMidVar3.BRID

def srcMidVar4 = srcArgs0.ChangeCUGMemCallListRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar4.TimeType

destArgs0.timeZoneId = srcMidVar4.TimeZoneID

def srcMidVar5 = srcArgs0.ChangeCUGMemCallListRequestMsg.ChangeCUGMemCallListRequest

def destMidVar2 = destArgs1.groupAccessCode

def srcMidVar6 = srcArgs0.ChangeCUGMemCallListRequestMsg.ChangeCUGMemCallListRequest.SubGroupAccessCode

destMidVar2.groupCode = srcMidVar6.SubGroupCode

destMidVar2.groupKey = srcMidVar6.SubGroupKey

def srcMidVar7 = srcArgs0.ChangeCUGMemCallListRequestMsg.ChangeCUGMemCallListRequest.SubAccessCode

def destMidVar3 = destArgs1.subAccessCode

destMidVar3.primaryIdentity = srcMidVar7.PrimaryIdentity

destMidVar3.subscriberKey = srcMidVar7.SubscriberKey

def listMapping2
listMapping2 =
{
    src,dest  ->

	dest.flowType = src.FlowType

    dest.controlNumber = src.ControlNumber
	
	dest.numberType = src.NumberType

}

def listMapping3
listMapping3 =
{
    src,dest  ->

	dest.flowType = src.FlowType

    dest.controlNumber = src.ControlNumber	

}

def listMapping1
listMapping1 =
{
    src,dest  ->

	mappingList(src.AddCallList,dest.addCallList,listMapping2)
	mappingList(src.DelCallList,dest.delCallList,listMapping3)

	
}


mappingList(srcMidVar5.CallList,destArgs1.callLists,listMapping1)


