dest.setServiceOperation("BMGroupService","queryGrpCallScreenNo")

def srcReqDocMsg = src.payload._args[0]
def srcReqHeaderDoc = srcReqDocMsg.QueryGrpCallScreenNoRequestMsg.RequestHeader

def destMsgHeader = dest.payload._args[0]
destMsgHeader._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

destMsgHeader.version = srcReqHeaderDoc.Version
destMsgHeader.businessCode = srcReqHeaderDoc.BusinessCode
destMsgHeader.messageSeq = srcReqHeaderDoc.MessageSeq
destMsgHeader.beId = srcReqHeaderDoc.OwnershipInfo.BEID
destMsgHeader.brId = srcReqHeaderDoc.OwnershipInfo.BRID
destMsgHeader.loginSystem = srcReqHeaderDoc.AccessSecurity.LoginSystemCode
destMsgHeader.password = srcReqHeaderDoc.AccessSecurity.Password
destMsgHeader.remoteAddress = srcReqHeaderDoc.AccessSecurity.RemoteIP
destMsgHeader.operatorId = srcReqHeaderDoc.OperatorInfo.OperatorID
destMsgHeader.channelId = srcReqHeaderDoc.OperatorInfo.ChannelID
destMsgHeader.interMode = srcReqHeaderDoc.AccessMode
destMsgHeader.msgLanguageCode = srcReqHeaderDoc.MsgLanguageCode
destMsgHeader.timeType = srcReqHeaderDoc.TimeFormat.TimeType
destMsgHeader.timeZoneId = srcReqHeaderDoc.TimeFormat.TimeZoneID

def listMappingAdditionalProperty
listMappingAdditionalProperty = 
{
    src,dest  ->
	dest.code = src.Code
	dest.value = src.Value
}
mappingList(srcReqHeaderDoc.AdditionalProperty,destMsgHeader.simpleProperty,listMappingAdditionalProperty)

def srcReqDataDoc = srcReqDocMsg.QueryGrpCallScreenNoRequestMsg.QueryGrpCallScreenNoRequest
def destReqData = dest.payload._args[1]
destReqData._class = "com.huawei.ngcbs.cm.group.callscreen.io.QueryGrpCallScreenNoRequest"

def destGroupAccessCode = destReqData.groupAccessCode
destGroupAccessCode.groupKey = srcReqDataDoc.SubGroupAccessCode.SubGroupKey
destGroupAccessCode.groupCode = srcReqDataDoc.SubGroupAccessCode.SubGroupCode

destReqData.callScreenType = srcReqDataDoc.CallScreenType
