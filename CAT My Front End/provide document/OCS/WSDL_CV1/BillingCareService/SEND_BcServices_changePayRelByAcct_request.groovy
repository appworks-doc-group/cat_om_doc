import com.huawei.ngcbs.bm.common.common.Constant4Model
dest.setServiceOperation("AccountService","changePayRelByAcct")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.account.changepayrelbyacct.ChangePayRelByAcctRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}
def listMapping24

listMapping24 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping25

listMapping25 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping26

listMapping26 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
	
}

def listMapping27

listMapping27 = 
{
    src,dest  ->

	dest.accoutCode = src.AccountCode
	
	dest.accoutKey = src.AccountKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->
	
	dest.subIdentity = src.SubIdentity
    
	dest.paymentRelation.paymentRelationKey = src.PayRelationKey
	
	def destMidVar = dest.paymentRelation
	
	destMidVar.priority = src.Priority
	
	destMidVar.finalFlag = src.OnlyPayRelFlag
	
	if(!src.BalCLType.isNull()){
	
	destMidVar._class = "com.huawei.ngcbs.cm.domain.entity.common.io.PaymentRelationInfoExtInfo"
	destMidVar.balanceType = src.BalCLType.BalanceType
	destMidVar.creditLimitType = src.BalCLType.CreditLimitType
	
	}
	
	def destMidVar1 = dest.paymentRelation.offeringKey
	
	destMidVar1._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
	
	def srcMidVar9 = src.PayRelExtRule
	
	destMidVar.chargeCode = srcMidVar9.ChargeCode
	
	destMidVar.extRuleCode = srcMidVar9.ControlRule
	
	def srcMidVar10 = src.PayRelExtRule.OfferingKey
	
	destMidVar1.oId = srcMidVar10.OfferingID
	
	destMidVar1.oCode = srcMidVar10.OfferingCode
	
	destMidVar1.pSeq = srcMidVar10.PurchaseSeq
	
	dest.paymentLimitInfo.currencyId = src.PayLimit.CurrencyID
	
	dest.paymentLimitInfo.limitRule = src.PayLimit.LimitRule
	
	dest.paymentLimitInfo.cBonusFlag = src.PayLimit.CBonusFlag
	
	dest.paymentLimitInfo.limitCycleType = src.PayLimit.LimitCycleType
	
	dest.paymentLimitInfo.limitMeasureUnit = src.PayLimit.Limit.LimitMeasureUnit
	
	dest.paymentLimitInfo.limitType = src.PayLimit.Limit.LimitType
	
	dest.paymentLimitInfo.limitValue = src.PayLimit.Limit.LimitValue
	
	dest.paymentLimitInfo.limitValueType = src.PayLimit.Limit.LimitValueType
	
	def srcMidVar8 = src.EffectiveTime
	
	dest.effMode = srcMidVar8.Mode
	
	dest.effDate = parseDate(srcMidVar8.Time,Constant4Model.DATE_FORMAT)
	
	dest.expDate = parseDate(src.ExpirationTime,Constant4Model.DATE_FORMAT)
	
}

def listMapping41

listMapping41 =
{
	src,dest  ->
	
	dest._class = "com.huawei.ngcbs.cm.account.changepayrelbyacct.ModPaymentRelationByAcct"
	
	dest.subIdentity = src.SubIdentity
	
	dest.modPaymentRelation.paymentRelationKey = src.PayRelationKey
	
	def destMidVar2 = dest.modPaymentRelation
	
	destMidVar2.finalFlag = src.OnlyPayRelFlag
	
	dest.limitValue = src.LimitValue
	
	if(!src.BalCLType.isNull()){
	
	destMidVar2._class = "com.huawei.ngcbs.cm.domain.entity.common.io.PaymentRelationInfoExtInfo"
	
	destMidVar2.balanceType = src.BalCLType.BalanceType
	destMidVar2.creditLimitType = src.BalCLType.CreditLimitType
	
	}
	
	destMidVar2.priority = src.Priority
	
	def destMidVar21 = dest.modPaymentRelation.offeringKey
	
	destMidVar21._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
	
	def srcMidVar20 = src.PayRelExtRule
	
	destMidVar2.chargeCode = srcMidVar20.ChargeCode
	
	destMidVar2.extRuleCode = srcMidVar20.ControlRule
	
	def srcMidVar21 = src.PayRelExtRule.OfferingKey
	
	destMidVar21.oId = srcMidVar21.OfferingID
	
	destMidVar21.oCode = srcMidVar21.OfferingCode
	
	destMidVar21.pSeq = srcMidVar21.PurchaseSeq
}

def listMapping29

listMapping29 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}
def listMapping131

listMapping131 = 
{
    src,dest  ->
	
	dest._class = "com.huawei.ngcbs.cm.account.changepayrelbyacct.DelPaymentRelationInfoByAcct"
	
	dest.subIdentity = src.SubIdentity
	
	def srcMidVar12 = src.ExpirationTime
	
	dest.expMode = srcMidVar12.Mode
	
	dest.expDate=parseDate(srcMidVar12.Time,Constant4Model.DATE_FORMAT)
	
	dest.paymentRelationKey = src.PayRelationKey
	
	dest.filterEffDate = parseDate(src.EffectiveTime,Constant4Model.DATE_FORMAT)
}
def srcMidVar23 = srcArgs0.ChangePayRelByAcctRequestMsg.ChangePayRelByAcctRequest.PaymentObj

listMapping24.call(srcMidVar23.CustAccessCode,destArgs1.custAccessCode)

listMapping25.call(srcMidVar23.SubAccessCode,destArgs1.subAccessCode)

listMapping26.call(srcMidVar23.SubGroupAccessCode,destArgs1.groupAccessCode)

listMapping27.call(srcMidVar23.AcctAccessCode,destArgs1.acctAccessCode)

def srcModRel = srcArgs0.ChangePayRelByAcctRequestMsg.ChangePayRelByAcctRequest.PayRelation.ModPayRelation

def destModRel = destArgs1.changePaymentRelationByAcct.modPaymentRelations.modPayRelation

mappingList(srcModRel,destModRel,listMapping41)

def srcMidVar = srcArgs0.ChangePayRelByAcctRequestMsg.ChangePayRelByAcctRequest

def destMidVar = destArgs1.changePaymentRelationByAcct.addPaymentRelationInfoByAcct

def srcMidVar1 = srcArgs0.ChangePayRelByAcctRequestMsg.ChangePayRelByAcctRequest.PayRelation.AddPayRelation

mappingList(srcMidVar1,destMidVar,listMapping1)

def srcMidVar28 = srcArgs0.ChangePayRelByAcctRequestMsg.ChangePayRelByAcctRequest.AdditionalProperty

mappingList(srcMidVar28,destArgs1.simplePropertyList,listMapping29)

def srcMidVar30 = srcArgs0.ChangePayRelByAcctRequestMsg.ChangePayRelByAcctRequest.PayRelation.DelPayRelation
def destMidVar30 = destArgs1.changePaymentRelationByAcct.delPaymentRelations
mappingList(srcMidVar30,destMidVar30,listMapping131)

def srcMidVar3 = srcArgs0.ChangePayRelByAcctRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar3.AccessMode

def srcMidVar4 = srcArgs0.ChangePayRelByAcctRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar4.LoginSystemCode

destArgs0.password = srcMidVar4.Password

destArgs0.remoteAddress = srcMidVar4.RemoteIP

mappingList(srcMidVar3.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar3.BusinessCode

destArgs0.messageSeq = srcMidVar3.MessageSeq

destArgs0.msgLanguageCode = srcMidVar3.MsgLanguageCode

def srcMidVar5 = srcArgs0.ChangePayRelByAcctRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar5.ChannelID

destArgs0.operatorId = srcMidVar5.OperatorID

def srcMidVar6 = srcArgs0.ChangePayRelByAcctRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar6.BEID

destArgs0.brId = srcMidVar6.BRID

def srcMidVar7 = srcArgs0.ChangePayRelByAcctRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar7.TimeType

destArgs0.timeZoneId = srcMidVar7.TimeZoneID

destArgs0.version = srcMidVar3.Version
