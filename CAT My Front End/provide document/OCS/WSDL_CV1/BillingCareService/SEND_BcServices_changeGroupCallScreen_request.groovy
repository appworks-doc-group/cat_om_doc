import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("BMGroupService", "changeGroupCallScreen")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.group.changegroupcallscreen.io.ChangeGroupCallScreenRequest"

def listMapping0

listMapping0 =
        {
            src, dest ->

                dest.code = src.Code

                dest.value = src.Value

        }


def listMapping1

listMapping1 =
        {
            src, dest ->

                dest.payScreenProfile = src.PayScreenProfile

                dest.effDateMode = src.EffectiveTime.Mode

                dest.effDate = parseDate(src.EffectiveTime.Time, Constant4Model.DATE_FORMAT)

                dest.expDate = parseDate(src.ExpDate, Constant4Model.DATE_FORMAT)

                dest.screenNumber = src.ScreenNumber

        }

def listMapping2

listMapping2 =
        {
            src, dest ->

                dest.payScreenProfile = src.PayScreenProfile

                dest.screenNumber = src.ScreenNumber

        }

def listMapping3

listMapping3 =
        {
            src, dest ->

                dest.groupCode = src.SubGroupCode

                dest.groupKey = src.SubGroupKey

        }



def srcMidVar4 = srcArgs0.ChangeGroupCallScreenRequestMsg.ChangeGroupCallScreenRequest.CallScreenList

mappingList(srcMidVar4.AddCallScreenNumber, destArgs1.addCallScreenNumberList, listMapping1)

mappingList(srcMidVar4.DelCallScreenNumber, destArgs1.delCallScreenNumberList, listMapping2)


def srcMidVar5 = srcArgs0.ChangeGroupCallScreenRequestMsg.ChangeGroupCallScreenRequest

listMapping3.call(srcMidVar5.SubGroupAccessCode, destArgs1.groupAccessCode)



def srcMidVar = srcArgs0.ChangeGroupCallScreenRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar.LoginSystemCode

destArgs0.password = srcMidVar.Password

destArgs0.remoteAddress = srcMidVar.RemoteIP

def srcMidVar0 = srcArgs0.ChangeGroupCallScreenRequestMsg.RequestHeader

mappingList(srcMidVar0.AdditionalProperty, destArgs0.simpleProperty, listMapping0)

destArgs0.businessCode = srcMidVar0.BusinessCode

destArgs0.messageSeq = srcMidVar0.MessageSeq

destArgs0.msgLanguageCode = srcMidVar0.MsgLanguageCode

def srcMidVar1 = srcArgs0.ChangeGroupCallScreenRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.ChangeGroupCallScreenRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.ChangeGroupCallScreenRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar0.Version

destArgs0.interMode = srcMidVar0.AccessMode

