import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.amount = src.Amount
	
	dest.creditInstId = src.CreditInstID
	
	dest.effectiveTime=parseDate(src.EffectiveTime,Constant4Model.DATE_FORMAT)
	
	dest.expireTime=parseDate(src.ExpireTime,Constant4Model.DATE_FORMAT)
	
	dest.limitClass = src.LimitClass
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	mappingList(src.CreditAmountInfo,dest.creditAmountInfoList,listMapping2)
	
	dest.creditLimitType = src.CreditLimitType
	
	dest.creditLimitTypeName = src.CreditLimitTypeName
	
	dest.currencyId = src.CurrencyID
	
	dest.totalCreditAmount = src.TotalCreditAmount
	
	dest.totalRemainAmount = src.TotalRemainAmount
	
	dest.totalUsageAmount = src.TotalUsageAmount
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.amount = src.Amount
	
	dest.balanceInstanceId = src.BalanceInstanceID
	
	dest.effectiveTime=parseDate(src.EffectiveTime,Constant4Model.DATE_FORMAT)
	
	dest.expireTime=parseDate(src.ExpireTime,Constant4Model.DATE_FORMAT)
	
	dest.initialAmount = src.InitialAmount
	
	def destMidVar1 = dest.acctBalOriginal
	 
	def srcMidVar1 = src.AcctBalOriginal
	
	destMidVar1.originalType = srcMidVar1.OriginalType
	
	destMidVar1.originalID = srcMidVar1.OriginalID
	
	dest.lastUpdateTime = src.LastUpdateTime
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	mappingList(src.BalanceDetail,dest.balanceDetailList,listMapping4)
	
	dest.balanceType = src.BalanceType
	
	dest.balanceTypeName = src.BalanceTypeName
	
	dest.currencyId = src.CurrencyID
	
	dest.depositFlag = src.DepositFlag
	
	dest.refundFlag = src.RefundFlag
	
	dest.totalAmount = src.TotalAmount
	
	dest.reservedAmount = src.ReservedAmount
	
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.currencyId = src.CurrencyID
	
	dest.outStandingAmount = src.OutStandingAmount
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.billCycleBeginTime = parseDate(src.BillCycleBeginTime,Constant4Model.DATE_FORMAT)
	
	dest.billCycleEndTime = parseDate(src.BillCycleEndTime,Constant4Model.DATE_FORMAT)
	
	dest.billCycleId = src.BillCycleID
	
	dest.dueDate = parseDate(src.DueDate,Constant4Model.DATE_FORMAT)
	
	mappingList(src.OutStandingDetail,dest.outStandingDetailList,listMapping6)
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	mappingList(src.AccountCredit,dest.accountCreditList,listMapping1)
	
	mappingList(src.BalanceResult,dest.acctBalanceList,listMapping3)
	
	dest.acctKey = src.AcctKey
	
	mappingList(src.OutStandingList,dest.outStandingList,listMapping5)
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcReturn.QueryBalanceResultMsg.QueryBalanceResult

mappingList(srcMidVar.AcctList,destReturn.balanceResultInfo.acctList,listMapping0)

def srcMidVar0 = srcReturn.QueryBalanceResultMsg.ResultHeader

def destMidVar = destReturn.resultHeader

mappingList(srcMidVar0.AdditionalProperty,destMidVar.simpleProperty,listMapping7)

destMidVar.msgLanguageCode = srcMidVar0.MsgLanguageCode

destMidVar.resultCode = srcMidVar0.ResultCode

destMidVar.resultDesc = srcMidVar0.ResultDesc

destMidVar.version = srcMidVar0.Version

destReturn._class = "com.huawei.ngcbs.cm.common.ws.client.io.ar.queryBalance.io.QueryBalanceResult"
