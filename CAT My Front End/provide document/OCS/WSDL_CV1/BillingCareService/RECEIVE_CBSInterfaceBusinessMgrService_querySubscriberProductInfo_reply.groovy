def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.BillStatus = src.billStatus
	
	dest.CurCycleEndTime = src.curCycleEndTime
	
	dest.CurCycleStartTime = src.curCycleStartTime
	
	dest.EffectiveDate = src.effectiveDate
	
	dest.ExpiredDate = src.expiredDate
	
	dest.Id = src.id
	
	dest.ProductOrderKey = src.productOrderKey
	
	dest.status = src.status
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.Id = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Id = src.id
	
	dest.RegistrationTime=formatDate(src.registrationTime, "yyyyMMddHHmmss")
	
	mappingList(src.simplePropertyList,dest.SimpleProperty,listMapping2)
	
	dest.Status = src.status
	
}

def srcMidVar = srcReturn.resultBody

srcMidVar._class = "com.huawei.ngcbs.cm.ocs12ws.subscriber.querysubscriberproductinfo.io.QuerySubscriberProductInfoResultOcs12Base"

def destMidVar = destReturn.QuerySubscriberProductInfoResultMsg.QuerySubscriberProductInfoResult

mappingList(srcMidVar.product,destMidVar.Product,listMapping0)

mappingList(srcMidVar.serviceList,destMidVar.Service,listMapping1)

def srcMidVar0 = srcReturn.resultHeader

srcMidVar0._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12ResultHeader"

def destMidVar0 = destReturn.QuerySubscriberProductInfoResultMsg.ResultHeader

destMidVar0.CommandId = srcMidVar0.commandId

destMidVar0.ResultCode = srcMidVar0.resultCode

destMidVar0.ResultDesc = srcMidVar0.resultDesc

destMidVar0.SequenceId = srcMidVar0.sequenceId

destMidVar0.SerialNo = srcMidVar0.serialNo

destMidVar0.TransactionId = srcMidVar0.transactionId

destMidVar0.Version = srcMidVar0.version
