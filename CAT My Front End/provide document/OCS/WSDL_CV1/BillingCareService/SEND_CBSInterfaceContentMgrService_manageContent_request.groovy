dest.setServiceOperation("CBSInterfaceContentMgrService", "manageContent");

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.content.managecontent.io.ManageContentRequest"


def srcMidVar0 = srcArgs0.ManageContentRequestMsg.RequestHeader

destArgs0.beId = srcMidVar0.TenantId

destArgs0.msgLanguageCode = srcMidVar0.Language

destArgs0.operatorId = srcMidVar0.OperatorID

destArgs0.additionInfo = srcMidVar0.additionInfo

destArgs0.commandId = srcMidVar0.CommandId

destArgs0.interMedi = srcMidVar0.InterMedi

destArgs0.transactionId = srcMidVar0.TransactionId

destArgs0.reserve2 = srcMidVar0.Reserve2

destArgs0.reserve3 = srcMidVar0.Reserve3

destArgs0.interMode = srcMidVar0.InterMode

destArgs0.sequenceId = srcMidVar0.SequenceId

destArgs0.visitArea = srcMidVar0.visitArea

destArgs0.messageSeq = srcMidVar0.SerialNo

destArgs0.belToAreaId = srcMidVar0.BelToAreaID

destArgs0.partnerId = srcMidVar0.PartnerID

destArgs0.thirdPartyId = srcMidVar0.ThirdPartyID

destArgs0.remark = srcMidVar0.Remark

destArgs0.tradePartnerId = srcMidVar0.TradePartnerID

destArgs0.version = srcMidVar0.Version

destArgs0.partnerOperId = srcMidVar0.PartnerOperID

def srcMidVar1 = srcArgs0.ManageContentRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar1.Name

destArgs0.password = srcMidVar1.Password

destArgs0.remoteAddress = srcMidVar1.RemoteAddress

destArgs0.requestType = srcMidVar0.RequestType

destArgs0.interFrom = srcMidVar0.InterFrom

destArgs0.currentCell = srcMidVar0.currentCell


def srcMidVar2 = srcArgs0.ManageContentRequestMsg.ManageContentRequest

destArgs1.operationType = srcMidVar2.OperationType

destArgs1.queryMethod = srcMidVar2.QueryMethod

destArgs1.contentId = srcMidVar2.ContentID

destArgs1.contentName = srcMidVar2.ContentName

destArgs1.contentShortDesc = srcMidVar2.ContentShortDesc

destArgs1.contentLongDesc = srcMidVar2.ContentLongDesc

destArgs1.verion = srcMidVar2.Version

destArgs1.providerId = srcMidVar2.ProviderID

destArgs1.recordNumber = srcMidVar2.RecordNumber

destArgs1.startRecordNum = srcMidVar2.StartRecordNum

destArgs1.categoryID = srcMidVar2.CategoryID

destArgs1.serviceType = srcMidVar2.ServiceType

destArgs1.billDescCode = srcMidVar2.BillDescCode

destArgs1.billUrl = srcMidVar2.BillURL

destArgs1.requestedDuration = srcMidVar2.RequestedDuration



