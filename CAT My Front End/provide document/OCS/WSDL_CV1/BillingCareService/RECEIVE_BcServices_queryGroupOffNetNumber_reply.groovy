import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.MsgLanguageCode = src.msgLanguageCode
	
	dest.ResultCode = src.resultCode
	
	dest.ResultDesc = src.resultDesc
	
	dest.Version = src.version

	dest.MessageSeq = src.messageSeq
	
	mappingList(src.simpleProperty,dest.AdditionalProperty,listMapping1)
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.EffectiveTime=formatDate(src.effDate, Constant4Model.DATE_FORMAT)
	
	dest.ExpirationTime=formatDate(src.expDate, Constant4Model.DATE_FORMAT)
	
	dest.NumberType = src.numberType
	
	dest.OffNetNumber = src.offNetNumber
	
	dest.OffNetNumberGroupID = src.offNetNumberGroupId
	
	dest.OffNetShortNumber = src.offNetShortNumber
	
}

def destMidVar = destReturn.QueryGroupOffNetNumberResultMsg

listMapping0.call(srcReturn.resultHeader,destMidVar.ResultHeader)

def destMidVar0 = destReturn.QueryGroupOffNetNumberResultMsg.QueryGroupOffNetNumberResult

mappingList(srcReturn.offNetNumbers,destMidVar0.GroupOffNetNumber,listMapping2)
