dest.setServiceOperation("CMOtherService","synSpecialNumberTariff")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.other.common.io.Request"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.other.io.synspecialnumber.SynSpecialNumberTariffRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	def srcMidVar4 = src.NUMBER_RANGE.BEGIN_RANGE
	
	dest.beginRange = srcMidVar4."#text"
	
	def srcMidVar5 = src.NUMBER_RANGE.END_RANGE
	
	dest.endRange = srcMidVar5."#text"
	
	def destMidVar0 = dest.tariff
	
	def srcMidVar6 = src.TARIFF
	
	destMidVar0.currency = srcMidVar6.CURRENCY
	
	destMidVar0.offPeak = srcMidVar6.OFF_PEAK
	
	destMidVar0.peak = srcMidVar6.PEAK
	
	destMidVar0.type = srcMidVar6.TYPE
	
	destMidVar0.vat = srcMidVar6.VAT
	
	def destMidVar1 = dest.newTariff
	
	def srcMidVar7 = src.TARIFF_NEW
	
	destMidVar1.currency = srcMidVar7.CURRENCY
	
	destMidVar1.offPeak = srcMidVar7.OFF_PEAK
	
	destMidVar1.peak = srcMidVar7.PEAK
	
	destMidVar1.type = srcMidVar7.TYPE
	
	destMidVar1.vat = srcMidVar7.VAT
	
	def destMidVar2 = dest.oldTariff
	
	def srcMidVar8 = src.TARIFF_OLD
	
	destMidVar2.currency = srcMidVar8.CURRENCY
	
	destMidVar2.offPeak = srcMidVar8.OFF_PEAK
	
	destMidVar2.peak = srcMidVar8.PEAK
	
	destMidVar2.type = srcMidVar8.TYPE
	
	destMidVar2.vat = srcMidVar8.VAT
	
}

def srcMidVar = srcArgs0.HandlePortingMessageRequest.Header.MessageHeader.From

destArgs0.remoteAddress = srcMidVar.Location

def destMidVar = destArgs1.tariffInfo

def srcMidVar0 = srcArgs0.HandlePortingMessageRequest.Body.Request.PORTING_MESSAGE

destMidVar.name = srcMidVar0.name

destMidVar.type = srcMidVar0.type

def srcMidVar1 = srcArgs0.HandlePortingMessageRequest.Body.Request.PORTING_MESSAGE.PORTING_ORDER

destMidVar.plannedTime = srcMidVar1.PLANNED_DATETIME

def srcMidVar2 = srcArgs0.HandlePortingMessageRequest.Header.MessageHeader.From.PartyId

destArgs0.loginSystem = srcMidVar.Location

def srcMidVar3 = srcArgs0.HandlePortingMessageRequest.Body.Request.PORTING_MESSAGE.PORTING_ORDER_INFO_LIST

mappingList(srcMidVar3.PORTING_ORDER_INFO,destMidVar.spNumberTariffInfos,listMapping0)

destArgs0.fromLocation = srcMidVar.Location

destArgs0.fromType = srcMidVar2.type

def srcMidVar9 = srcArgs0.HandlePortingMessageRequest.Header.MessageHeader.To

destArgs0.toLocation = srcMidVar9.Location

def srcMidVar10 = srcArgs0.HandlePortingMessageRequest.Header.MessageHeader.To.PartyId

destArgs0.toType = srcMidVar10.type

def srcMidVar11 = srcArgs0.HandlePortingMessageRequest.Header.MessageHeader.ConnectionInfo

destArgs0.capId = srcMidVar11.CPAId

def srcMidVar12 = srcArgs0.HandlePortingMessageRequest.Header.MessageHeader.HeaderFields

destArgs0.messageId = srcMidVar12.MessageId

destArgs0.time = srcMidVar12.Timestamp

destArgs0.extendMessageId = srcMidVar11.ExternalRefToMessageId

def srcMidVar13 = srcArgs0.HandlePortingMessageRequest.Header.MessageHeader.Service

destArgs0.paraDigm = srcMidVar13.Paradigm

destArgs0.name = srcMidVar13.Name

destArgs0.version = srcMidVar13.Version

destArgs0.fromPartyId= srcMidVar2."#text"

destArgs0.toPartyId= srcMidVar10."#text"
