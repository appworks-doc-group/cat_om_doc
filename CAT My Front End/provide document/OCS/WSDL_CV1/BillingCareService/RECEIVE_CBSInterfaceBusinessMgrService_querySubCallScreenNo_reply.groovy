import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.callScreenNo = src.callScreenNo
	
	dest.routeNumber = src.routeNumber
	
	dest.weekStart = src.weekStart
	
	dest.weekStop = src.weekStop
	
	dest.effectiveDate=formatDate(src.effectiveDate, Constant4Model.DATE_FORMAT)
	
	dest.expireDate=formatDate(src.expireDate, Constant4Model.DATE_FORMAT)
	
	dest.IRRouteFlag = src.irRouteFlag
	
	dest.RoutingMethod = src.routingMethod
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.routeNumber = src.routeNumber
	
	dest.weekStart = src.weekStart
	
	dest.weekStop = src.weekStop
	
	dest.expireDate=formatDate(src.expireDate, Constant4Model.DATE_FORMAT)
	
	dest.effectiveDate=formatDate(src.effectiveDate, Constant4Model.DATE_FORMAT)
	
	dest.IRRouteFlag = src.irRouteFlag
	
	dest.RoutingMethod = src.routingMethod
	
}

def srcMidVar = srcReturn.resultHeader

srcMidVar._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12ResultHeader"

def destMidVar = destReturn.QuerySubCallScreenNoResultMsg.ResultHeader

destMidVar.CommandId = srcMidVar.commandId

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.SequenceId = srcMidVar.sequenceId

destMidVar.TransactionId = srcMidVar.transactionId

destMidVar.Version = srcMidVar.version

def srcMidVar0 = srcReturn.resultBody

srcMidVar0._class = "com.huawei.ngcbs.cm.ocs12ws.subscriber.querysubcallscreenno.io.QuerySubCallScreenNoResultOcs12Base"

def destMidVar0 = destReturn.QuerySubCallScreenNoResultMsg.QuerySubCallScreenNoResult

mappingList(srcMidVar0.callScreenNoInfos,destMidVar0.CallScreenNoInfo,listMapping0)

destMidVar0.CallScreenType = srcMidVar0.callScreenType

mappingList(srcMidVar0.nonCallScreenNoInfos,destMidVar0.NonCallScreenNoInfo,listMapping1)
