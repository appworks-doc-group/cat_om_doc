def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destArgs1 = dest.payload._args[1]

def srcMidVar = srcArgs0.ChangeSubscriberBasicInforRequestMsg.RequestHeader

destArgs0.beId = srcMidVar.TenantId

destArgs0.commandId = srcMidVar.CommandId

destArgs0.interMedi = srcMidVar.InterMedi

destArgs0.additionInfo = srcMidVar.additionInfo

destArgs0.operatorId = srcMidVar.OperatorID

destArgs0.transactionId = srcMidVar.TransactionId

destArgs0.reserve2 = srcMidVar.Reserve2

destArgs0.thirdPartyId = srcMidVar.ThirdPartyID

destArgs0.reserve3 = srcMidVar.Reserve3

destArgs0.interMode = srcMidVar.InterMode

destArgs0.sequenceId = srcMidVar.SequenceId

destArgs0.visitArea = srcMidVar.visitArea

destArgs0.messageSeq = srcMidVar.SerialNo

destArgs0.belToAreaId = srcMidVar.BelToAreaID

destArgs0.currentCell = srcMidVar.currentCell

destArgs0.partnerId = srcMidVar.PartnerID

destArgs0.remark = srcMidVar.Remark

destArgs0.tradePartnerId = srcMidVar.TradePartnerID

destArgs0.version = srcMidVar.Version

destArgs0.partnerOperId = srcMidVar.PartnerOperID

destArgs0.requestType = srcMidVar.RequestType

def srcMidVar0 = srcArgs0.ChangeSubscriberBasicInforRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar0.Name

destArgs0.password = srcMidVar0.Password

destArgs0.interFrom = srcMidVar.InterFrom

def srcMidVar1 = srcArgs0.ChangeSubscriberBasicInforRequestMsg.ChangeSubscriberBasicInforRequest

destArgs1.lang = srcMidVar1.Lang

destArgs1.ussdLang = srcMidVar1.USSDLang

destArgs1.smsLang = srcMidVar1.SMSLang

destArgs1.belToAreaId = srcMidVar1.BelToAreaID

destArgs1.initialCredit = srcMidVar1.InitialCredit

destArgs1.code = srcMidVar1.Code

def destMidVar = destArgs1.subAccessCode

destMidVar.primaryIdentity = srcMidVar1.SubscriberNo

def listMapping0

listMapping0 = 
{
    src0,dest0  ->

	dest0.value = src0.Value
	
	dest0.code = src0.Id
	
}

addingList(srcMidVar1.SimpleProperty,destArgs1.subProperties,listMapping0)

destArgs0.remoteAddress = srcMidVar0.RemoteAddress
