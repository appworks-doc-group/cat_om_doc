def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	
}

def destMidVar = destReturn.BatchChangeAcctBillCycleResultMsg.ResultHeader

destMidVar.MsgLanguageCode = srcReturn.msgLanguageCode

destMidVar.ResultCode = srcReturn.resultCode

destMidVar.ResultDesc = srcReturn.resultDesc

destMidVar.Version = srcReturn.version

destMidVar.MessageSeq = srcReturn.messageSeq

def destMidVar0 = destReturn.BatchChangeAcctBillCycleResultMsg.ResultHeader.AdditionalProperty[0]

def srcMidVar = srcReturn.simpleProperty[0]

destMidVar0.Code = srcMidVar.code

destMidVar0.Value = srcMidVar.value

mappingList(srcReturn.simpleProperty,destMidVar.AdditionalProperty,listMapping0)
