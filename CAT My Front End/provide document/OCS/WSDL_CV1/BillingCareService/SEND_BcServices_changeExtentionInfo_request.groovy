import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("CCChangeExtentionInfoService","updateMNPInfo")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destArgs1 = dest.payload._args[1]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

destArgs1._class = "com.huawei.ngcbs.bc.netconf.npdata.io.ChangeExtentionInfoRequest"



def srcMidVar10 = srcArgs0.ChangeExtentionInfoRequestMsg.ChangeExtentionInfoRequest.AddExtentionInfo

def destMidVar10 = destArgs1.addExtentionInfoList

def listMapping6

listMapping6 = 
{
	src,dest ->
	
	dest.extentionInfo = src.ExtentionInfo
	 
	def srcMidVar11 = src.EffectiveTime
	
	def destMidVar11 = dest.effectiveTime
	
	destMidVar11.time = parseDate(srcMidVar11.Time,Constant4Model.DATE_FORMAT)
	
	destMidVar11.mode = srcMidVar11.Mode
	
	dest.expirationTime = parseDate(src.ExpirationTime,Constant4Model.DATE_FORMAT)
}

mappingList(srcMidVar10,destMidVar10,listMapping6)


def srcMidVar30 = srcArgs0.ChangeExtentionInfoRequestMsg.ChangeExtentionInfoRequest.DelExtentionInfo

def destMidVar3 = destArgs1.delExtentionInfoList

destMidVar3.extentionInfo = srcMidVar30.ExtentionInfo


def listMapping22

listMapping22 = 
{
	src,dest ->
	
	dest.extentionInfo = src.ExtentionInfo
}
mappingList(srcMidVar30,destMidVar3,listMapping22)


def srcMidVar33 = srcArgs0.ChangeExtentionInfoRequestMsg.ChangeExtentionInfoRequest.ModifyExtentionInfo

def destMidVar7 = destArgs1.modifyExtentionInfoList

def listMapping11

listMapping11 =  
{
	src,dest ->
	
	dest.extentionInfo = src.ExtentionInfo
	
	dest.newEffectiveTime = parseDate(src.NewEffectiveTime,Constant4Model.DATE_FORMAT)
	
	def srcMidVar16 = src.NewExpirationTime
	
	def destMidVar16 = dest.newExpirationTime
	
	destMidVar16.time = parseDate(srcMidVar16.Time,Constant4Model.DATE_FORMAT)
	
	destMidVar16.mode = srcMidVar16.Mode
	
}
mappingList(srcMidVar33 , destMidVar7 ,listMapping11)



def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.ChangeExtentionInfoRequestMsg.RequestHeader

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

def srcMidVar0 = srcArgs0.ChangeExtentionInfoRequestMsg.ChangeExtentionInfoRequest

destArgs1.busitype = srcMidVar0.BusiType

destArgs1.isdn = srcMidVar0.ISDN

destArgs11 =  destArgs1.subAccessCode

destArgs11.primaryIdentity = srcMidVar0.ISDN

def srcMidVar1 = srcArgs0.ChangeExtentionInfoRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar1.LoginSystemCode

destArgs0.password = srcMidVar1.Password

destArgs0.remoteAddress = srcMidVar1.RemoteIP

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar2 = srcArgs0.ChangeExtentionInfoRequestMsg.RequestHeader.OperatorInfo

destArgs0.operatorId = srcMidVar2.OperatorID

destArgs0.channelId = srcMidVar2.ChannelID

def srcMidVar3 = srcArgs0.ChangeExtentionInfoRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar3.BEID

destArgs0.brId = srcMidVar3.BRID

def srcMidVar4 = srcArgs0.ChangeExtentionInfoRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar4.TimeType

destArgs0.timeZoneId = srcMidVar4.TimeZoneID

destArgs0.version = srcMidVar.Version






