dest.setServiceOperation("CRMForBSS","activeSubscriber")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.Name = src.paraName
	
	dest.Value = src.paraValue
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.AccessPwd = src.accessPwd
	
	dest.AccessUser = src.accessUser
	
	dest.BEId = src.beId
	
	dest.ChannelId = src.channelId
	
	def destMidVar0 = dest.ExtParamList
	
	mappingList(src.extParamList,destMidVar0.ParameterInfo,listMapping4)
	
	dest.Language = src.language
	
	dest.OperatorId = src.operatorId
	
	dest.ProcessTime = src.processTime
	
	dest.OperatorPwd = src.operatorPwd
	
	dest.SessionId = src.sessionId
	
	dest.TestFlag = src.testFlag
	
	dest.TimeType = src.timeType
	
	dest.TimeZoneID = src.timeZoneId
	
	dest.TransactionId = src.transactionId
	
	dest.Version = src.version
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Name = src.paraName
	
	dest.Value = src.paraValue
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.Name = src.paraName
	
	dest.Value = src.paraValue
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.ActiveTime = src.activeTime
	
	dest.EffectiveTime = src.effectiveTime
	
	dest.ExpireTime = src.expireTime
	
	def destMidVar2 = dest.ExtParamList
	
	mappingList(src.extParmalist,destMidVar2.AdditionInfo,listMapping3)
	
	dest.LastActiveTime = src.lastActiveTime
	
	def destMidVar3 = dest.OfferingID
	
	destMidVar3.OfferingID = src.offeringId
	
	destMidVar3.PurchaseSeq = src.purchaseSeq
	
	dest.TrialEndTime = src.trialEndTime
	
	dest.TrialStartTime = src.trialStartTime
	
}

def destMidVar = destArgs0.ActiveSubscriberRequest

listMapping0.call(srcArgs0.workOrderHeader,destMidVar.RequestHeader)

def destMidVar1 = destArgs0.ActiveSubscriberRequest.ActiveSubscriber

destMidVar1.ActiveTime = srcArgs0.activeTime

destMidVar1.ExprieTime = srcArgs0.expireTime

destMidVar1.SubscriberID = srcArgs0.subscriberKey

destMidVar1.Msisdn = srcArgs0.subscriberNo

destMidVar1.TrialEndTime = srcArgs0.trialEndTime

destMidVar1.TrialStartTime = srcArgs0.trialStartTime

mappingList(srcArgs0.extParamList,destMidVar1.ExtParamList,listMapping1)

mappingList(srcArgs0.offeringInfoList,destMidVar1.ActOfferInfo,listMapping2)
