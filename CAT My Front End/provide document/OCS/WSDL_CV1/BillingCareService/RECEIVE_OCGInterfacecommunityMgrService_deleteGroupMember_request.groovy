def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.BelToAreaID = src.belToAreaId
	
	dest.CommandId = src.commandId
	
	dest.currentCell = src.currentCell
	
	dest.InterFrom = src.interFrom
	
	dest.InterMedi = src.interMedi
	
	dest.InterMode = src.interMode
	
	dest.OperatorID = src.operatorId
	
	dest.PartnerID = src.partnerId
	
	dest.Remark = src.remark
	
	dest.SequenceId = src.sequenceId
	
	dest.SerialNo = src.serialNo
	
	def destMidVar0 = dest.SessionEntity
	
	def srcMidVar = src.sessionEntity
	
	destMidVar0.Name = srcMidVar.name
	
	destMidVar0.Password = srcMidVar.password
	
	destMidVar0.RemoteAddress = srcMidVar.remoteAddress
	
	dest.TenantID = src.tenantId
	
	dest.TransactionId = src.transactionId
	
	dest.Version = src.version
	
	dest.visitArea = src.visitArea
	
	dest.PartnerOperID = src.partnerOperId
	
	dest.RequestType = src.requestType
	
	dest.ThirdPartyID = src.thirdPartyId
	
	dest.TradePartnerID = src.tradePartnerId
	
}

def destMidVar = destArgs0.DeleteGroupMemberRequestMsg

listMapping0.call(srcArgs0.omRequestHeader,destMidVar.RequestHeader)

def destMidVar1 = destArgs0.DeleteGroupMemberRequestMsg.DeleteGroupMemberRequest

destMidVar1.GroupNumber = srcArgs0.groupNumber

destMidVar1.GrpMemberNo = srcArgs0.grpMemberNo

destMidVar1.GrpMemberShortNo = srcArgs0.grpMemberShortNo
