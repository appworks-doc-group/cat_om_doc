def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.ManAppendSubIdentityResultMsg.ResultHeader

destMidVar.MsgLanguageCode = srcReturn.msgLanguageCode

destMidVar.Version = srcReturn.version

destMidVar.ResultCode = srcReturn.resultCode

destMidVar.ResultDesc = srcReturn.resultDesc

def listMapping0

listMapping0 = 
{
    src0,dest0  ->

	dest0.Code = src0.code
	
	dest0.Value = src0.value
	
}

addingList(srcReturn.simpleProperty,destMidVar.AdditionalProperty,listMapping0)
