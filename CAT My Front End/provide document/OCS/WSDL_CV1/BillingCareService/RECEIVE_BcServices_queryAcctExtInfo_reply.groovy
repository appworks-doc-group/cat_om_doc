def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def destMidVar1 = destReturn.QueryAcctExtInfoResultMsg.QueryAcctExtInfoResult

def srcMidVar1 = srcReturn.queryAcctExtInInfo

mappingList(srcMidVar1.additionalProperty,destMidVar1.AdditionalProperty,listMapping0)

def destMidVar0 = destReturn.QueryAcctExtInfoResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar0.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar0.ResultCode = srcMidVar.resultCode

destMidVar0.ResultDesc = srcMidVar.resultDesc

destMidVar0.Version = srcMidVar.version

destMidVar0.MessageSeq = srcMidVar.messageSeq

mappingList(srcMidVar.simpleProperty,destMidVar0.AdditionalProperty,listMapping0)