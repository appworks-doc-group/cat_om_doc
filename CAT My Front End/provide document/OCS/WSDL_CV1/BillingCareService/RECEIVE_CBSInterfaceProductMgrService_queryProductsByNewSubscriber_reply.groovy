def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.EffectiveDate = src.effectiveDate
	
	dest.ExpireDate = src.expireDate
	
	dest.Id = src.id
	
	dest.Name = src.name
	
}

def destMidVar = destReturn.QueryProductsByNewSubscriberResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.CommandId = srcMidVar.commandId

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.SequenceId = srcMidVar.sequenceId

destMidVar.TransactionId = srcMidVar.transactionId

destMidVar.Version = srcMidVar.version

def srcMidVar0 = srcReturn.resultBody

def destMidVar0 = destReturn.QueryProductsByNewSubscriberResultMsg.QueryProductsByNewSubscriberResult

mappingList(srcMidVar0.productInfoList,destMidVar0.ProductInfo,listMapping0)
