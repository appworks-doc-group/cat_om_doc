def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.QueryMemberCountOfGroupResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.CommandId = srcMidVar.commandId

destMidVar.Version = srcMidVar.version

destMidVar.TransactionId = srcMidVar.transactionId

destMidVar.SequenceId = srcMidVar.sequenceId

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.OperationTime = srcMidVar.operationTime

destMidVar.OrderId = srcMidVar.orderId

def destMidVar0 = destReturn.QueryMemberCountOfGroupResultMsg.QueryMemberCountOfGroupResult

destMidVar0.MemberCount = srcReturn.memberCount