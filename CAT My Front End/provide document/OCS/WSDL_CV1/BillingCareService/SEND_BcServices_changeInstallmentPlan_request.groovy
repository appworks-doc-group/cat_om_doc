dest.setServiceOperation("AccountService","changeInstallmentPlan")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.account.changeinstallmentplan.io.ChangeInstallmentPlanRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.value = src.Value
	
	dest.code = src.Code
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
	
}



def listMapping9

listMapping9 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping10

listMapping10 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.cycleSequence = src.CycleSequence
	
	dest.cycleAmount = src.CycleAmount
	
	dest.initialAmount = src.InitialAmount
	
	dest.repayDueDate= parseDate(src.RepayDueDate, "yyyyMMddHHmmss")
		
	dest.cycleStatus = src.CycleStatus
	
	mappingList(src.AdditionalProperty,dest.simplePropertyList,listMapping9)
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.cycleSequence = src.CycleSequence
	
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.cycleSequence = src.CycleSequence
	
	dest.currAmount = src.CurrAmount
	
	dest.checkCurrAmount = src.CheckCurrAmount
	
	dest.initialAmount = src.InitialAmount
	
	dest.checkInitialAmount = src.CheckInitialAmount
	
	dest.repayDueDate= parseDate(src.RepayDueDate, "yyyyMMddHHmmss")
		
	dest.cycleStatus = src.CycleStatus
	
	mappingList(src.AdditionalProperty,dest.simplePropertyList,listMapping10)

}

def listMapping8

listMapping8 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.ChangeInstallmentPlanRequestMsg.RequestHeader

destArgs0.version = srcMidVar.Version

def srcMidVar0 = srcArgs0.ChangeInstallmentPlanRequestMsg.RequestHeader.TimeFormat

destArgs0.timeZoneId = srcMidVar0.TimeZoneID

destArgs0.timeType = srcMidVar0.TimeType

def srcMidVar1 = srcArgs0.ChangeInstallmentPlanRequestMsg.RequestHeader.OwnershipInfo

destArgs0.brId = srcMidVar1.BRID

destArgs0.beId = srcMidVar1.BEID

def srcMidVar2 = srcArgs0.ChangeInstallmentPlanRequestMsg.RequestHeader.OperatorInfo

destArgs0.operatorId = srcMidVar2.OperatorID

destArgs0.channelId = srcMidVar2.ChannelID

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.businessCode = srcMidVar.BusinessCode

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

def srcMidVar3 = srcArgs0.ChangeInstallmentPlanRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar3.LoginSystemCode

destArgs0.password = srcMidVar3.Password

destArgs0.remoteAddress = srcMidVar3.RemoteIP

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar4 = srcArgs0.ChangeInstallmentPlanRequestMsg.ChangeInstallmentPlanRequest

def srcMidVar5 = srcArgs0.ChangeInstallmentPlanRequestMsg.ChangeInstallmentPlanRequest.ChangeObj

listMapping2.call(srcMidVar5.CustAccessCode,destArgs1.custAccessCode)

listMapping3.call(srcMidVar5.SubAccessCode,destArgs1.subAccessCode)

listMapping4.call(srcMidVar5.SubGroupAccessCode,destArgs1.groupAccessCode)

destArgs1.contractID = srcMidVar4.ContractID

destArgs1.totalAmount = srcMidVar4.TotalAmount

destArgs1.totalCycle = srcMidVar4.TotalCycle

destArgs1.currencyId = srcMidVar4.CurrencyID

destArgs1.chargeCode = srcMidVar4.ChargeCode

destArgs1.opType = srcMidVar4.OpType

mappingList(srcMidVar4.AdditionalProperty,destArgs1.simplePropertyList,listMapping8)

def srcMidVar6 = srcArgs0.ChangeInstallmentPlanRequestMsg.ChangeInstallmentPlanRequest.InstallmentPlan

def destArgs2 = destArgs1.installmentPlan

mappingList(srcMidVar6.AddPlan,destArgs2.addPlan,listMapping5)

mappingList(srcMidVar6.DelPlan,destArgs2.delPlan,listMapping6)

mappingList(srcMidVar6.ModPlan,destArgs2.modPlan,listMapping7)


