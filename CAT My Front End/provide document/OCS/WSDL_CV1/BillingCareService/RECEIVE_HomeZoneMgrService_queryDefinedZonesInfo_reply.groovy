def srcReturn = src.payload._return
def destReturn = dest.payload._return

def destMidVar = destReturn.QueryDefinedZonesInfoResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.CommandId = srcMidVar.commandId
destMidVar.Version = srcMidVar.version
destMidVar.TransactionId = srcMidVar.transactionId
destMidVar.SequenceId = srcMidVar.sequenceId
destMidVar.ResultCode = srcMidVar.resultCode
destMidVar.ResultDesc = srcMidVar.resultDesc
destMidVar.OperationTime = srcMidVar.operationTime
destMidVar.OrderId = srcMidVar.orderId

def srcBusinessData = srcReturn.resultBody
def destReturnData = destReturn.QueryDefinedZonesInfoResultMsg.QueryDefinedZonesInfoResult

def listMapping0
listMapping0 =
{
    src,dest  ->
        dest.ZoneID = src.id
        dest.ZoneName = src.name
        dest.ZoneType = src.type
}
mappingList(srcBusinessData.zones,destReturnData.DefinedZonesInfo,listMapping0)