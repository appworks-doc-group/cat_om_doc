import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("BMQueryService","querySubscriberStatement")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.query.querysubscriberstgatement.io.QuerySubscriberStatementRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def destMidVar = destArgs1.subAccessCode

def srcMidVar = srcArgs0.QuerySubscriberStatementRequestMsg.QuerySubscriberStatementRequest.SubAccessCode

destMidVar.primaryIdentity = srcMidVar.PrimaryIdentity

destMidVar.subscriberKey = srcMidVar.SubscriberKey

def srcMidVar0 = srcArgs0.QuerySubscriberStatementRequestMsg.QuerySubscriberStatementRequest

destArgs1.eventType = srcMidVar0.EventType

destArgs1.totalRowNum = srcMidVar0.TotalRowNum

destArgs1.beginRowNum = srcMidVar0.BeginRowNum

destArgs1.fetchRowNum = srcMidVar0.FetchRowNum

destArgs1.startTime = parseDate(srcMidVar0.StartTime,Constant4Model.DATE_FORMAT)

destArgs1.endTime = parseDate(srcMidVar0.EndTime,Constant4Model.DATE_FORMAT)

def srcMidVar3 = srcArgs0.QuerySubscriberStatementRequestMsg.RequestHeader

destArgs0.version = srcMidVar3.Version

def srcMidVar4 = srcArgs0.QuerySubscriberStatementRequestMsg.RequestHeader.TimeFormat

destArgs0.timeZoneId = srcMidVar4.TimeZoneID

destArgs0.timeType = srcMidVar4.TimeType

def srcMidVar5 = srcArgs0.QuerySubscriberStatementRequestMsg.RequestHeader.OwnershipInfo

destArgs0.brId = srcMidVar5.BRID

destArgs0.beId = srcMidVar5.BEID

def srcMidVar6 = srcArgs0.QuerySubscriberStatementRequestMsg.RequestHeader.OperatorInfo

destArgs0.operatorId = srcMidVar6.OperatorID

destArgs0.channelId = srcMidVar6.ChannelID

destArgs0.msgLanguageCode = srcMidVar3.MsgLanguageCode

destArgs0.messageSeq = srcMidVar3.MessageSeq

destArgs0.businessCode = srcMidVar3.BusinessCode

def srcMidVar7 = srcArgs0.QuerySubscriberStatementRequestMsg.RequestHeader.AccessSecurity

destArgs0.password = srcMidVar7.Password

destArgs0.loginSystem = srcMidVar7.LoginSystemCode

destArgs0.remoteAddress = srcMidVar7.RemoteIP

destArgs0.interMode = srcMidVar3.AccessMode

mappingList(srcMidVar3.AdditionalProperty,destArgs0.simpleProperty,listMapping0)
