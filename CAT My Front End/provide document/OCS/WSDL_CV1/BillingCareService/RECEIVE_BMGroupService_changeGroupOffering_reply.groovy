import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.MsgLanguageCode = src.msgLanguageCode
	
	dest.ResultCode = src.resultCode
	
	dest.ResultDesc = src.resultDesc
	
	dest.Version = src.version
	
	mappingList(src.simpleProperty,dest.AdditionalProperty,listMapping1)
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.OfferingID = src.oId
	
	dest.PurchaseSeq = src.pSeq
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	listMapping4.call(src.offeringKey,dest.OfferingKey)
	
	dest.NewEffectiveTime=formatDate(src.effDate, Constant4Model.DATE_FORMAT)
	
	dest.NewExpirationTime=formatDate(src.expDate, Constant4Model.DATE_FORMAT)
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	mappingList(src.modifyOfferingInfos,dest.ModifyOffering,listMapping3)
	
}

def destMidVar = destReturn.ChangeGroupOfferingResultMsg

listMapping0.call(srcReturn.resultHeader,destMidVar.ResultHeader)

listMapping2.call(srcReturn.resultBody,destMidVar.ChangeGroupOfferingResult)
