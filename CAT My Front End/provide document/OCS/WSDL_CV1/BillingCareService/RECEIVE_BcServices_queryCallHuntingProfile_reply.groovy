def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 =
		{
			src,dest  ->

				dest.Code = src.code

				dest.Value = src.value

		}

def listMapping2

listMapping2 =
		{
			src, dest ->

				dest.WeekStart = src.weekStart

				dest.WeekEnd = src.weekEnd

				dest.TimeStart = src.timeStart

				dest.TimeEnd = src.timeEnd
		}


def listMapping4

listMapping4 =
		{
			src, dest ->
				dest.HuntingNumber = src.huntingNumber
		}

def destMidVar = destReturn.QueryCallHuntingProfileResponseMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.Version = srcMidVar.version

destMidVar.MessageSeq = srcMidVar.messageSeq

mappingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def destMidVar1 = destReturn.QueryCallHuntingProfileResponseMsg.QueryCallHuntingProfileResponse.SubGroupAccessCode

def srcMidVar0 = srcReturn.groupAccessCode

destMidVar1.SubGroupCode = srcMidVar0.groupCode

destMidVar1.SubGroupKey = srcMidVar0.groupKey

def listMapping3

listMapping3 =
		{
			src, dest ->
				dest.HuntingMainNumber = src.huntingMainNumber
				dest.HuntingProfile.ProfileCode = src.huntingProfileInfo.huntingProfile
				mappingList(src.huntingProfileInfo.weekTimeSchemas,dest.HuntingProfile.WeekTimeSchema,listMapping2)
				mappingList(src.huntingList,dest.HuntingList,listMapping4)
		}
def destMidVar0 = destReturn.QueryCallHuntingProfileResponseMsg.QueryCallHuntingProfileResponse

mappingList(srcReturn.huntingMainInfoList,destMidVar0.HuntingMainList,listMapping3)

