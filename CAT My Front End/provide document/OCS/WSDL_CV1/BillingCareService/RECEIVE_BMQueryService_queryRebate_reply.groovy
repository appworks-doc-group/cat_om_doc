import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.OfferingID = src.oId
	
	dest.PurchaseSeq = src.pSeq
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.LastRebateDate=formatDate(src.lastRebateDate, Constant4Model.DATE_FORMAT)
	
	listMapping2.call(src.offeringKeyInfo,dest.OfferingKey)
	
	dest.ProcessedCycleNum = src.processedCycleNum
	
	dest.SuccessCycleNum = src.successCycleNum
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	mappingList(src.queryRebateDataList,dest.Rebate,listMapping1)
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	mappingList(src.simpleProperty,dest.AdditionalProperty,listMapping4)
	
	dest.Version = src.version
	
	dest.MsgLanguageCode = src.msgLanguageCode
	
	dest.ResultCode = src.resultCode
	
	dest.ResultDesc = src.resultDesc
	
}

def destMidVar = destReturn.QueryRebateResultMsg

listMapping0.call(srcReturn.resultInfo,destMidVar.QueryRebateResult)

listMapping3.call(srcReturn.resultHeader,destMidVar.ResultHeader)
