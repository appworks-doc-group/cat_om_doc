import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturnData = src.payload._return
def destReturnDoc = dest.payload._return

def destHeader = destReturnDoc.ChangeGroupOfferResultMsg.ResultHeader
def srcHeader = srcReturnData.resultHeader

destHeader.CommandId = srcHeader.commandId
destHeader.ResultCode = srcHeader.resultCode
destHeader.ResultDesc = srcHeader.resultDesc
destHeader.SequenceId = srcHeader.sequenceId
destHeader.Version = srcHeader.version
destHeader.TransactionId = srcHeader.transactionId
destHeader.OperationTime = srcHeader.operationTime
destHeader.OrderId = srcHeader.orderId

def srcBusinessData = srcReturnData.resultBody
srcBusinessData._class = "com.huawei.ngcbs.cm.ocs33ws.group.changegroupoffering.io.OCS33ChangeGroupOfferingResult"
def destReturnData = destReturnDoc.ChangeGroupOfferResultMsg.ChangeGroupOfferResult

def listSimplePropertyMapping = 
{
	src,dest  ->
	
	dest.Id = src.id
	dest.Value = src.value
}

def listProdOrderMapping = 
{
  src,dest  ->

	dest.OfferId = src.offerId
	dest.OfferOrderKey = src.offerOrderKey		
	dest.EffectiveDate = src.effectiveDate
	dest.ExpireDate = src.expireDate
	dest.AutoType = src.autoType
	dest.OfferCode = src.offerCode
	dest.Status = src.status
	mappingList(src.properties, dest.SimpleProperty, listSimplePropertyMapping)
}

mappingList(srcBusinessData.offerOrderInfoList, destReturnData.OfferOrderInfo, listProdOrderMapping)