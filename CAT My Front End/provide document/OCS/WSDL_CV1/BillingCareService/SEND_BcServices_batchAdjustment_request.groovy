dest.setServiceOperation("AccountService","batchAdjustment")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.account.batch.adjustment.io.BatchAdjustmentRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.adjustAmt = src.AdjustmentAmt
	
	dest.adjustmentType = src.AdjustmentType
	
	dest.balanceId = src.BalanceID
	
	dest.balanceType = src.BalanceType
	
	dest.currencyId = src.CurrencyID
	
	dest.adjustmentMode = src.AdjustmentMode
	
	dest.effectiveTime = src.EffectiveTime
	
	dest.expireTime = src.ExpireTime
	
	dest.offsetUnit = src.OffsetUnit

	dest.offsetValue = src.OffsetValue
	
	dest.selectInstanceMode = src.SelectInstanceMode
	
	dest.validityExtMethod = src.ValidityExtMethod
	
	dest.extendDays = src.ExtendDays
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.adjustmentAmt = src.AdjustmentAmt
	
	dest.adjustmentType = src.AdjustmentType
	
	dest.adjustmentMode = src.AdjustmentMode
	
	dest.effectiveTime = src.EffectiveTime
	
	dest.expireTime = src.ExpireTime
	
	dest.offsetUnit = src.OffsetUnit

	dest.offsetValue = src.OffsetValue
	
	dest.selectInstanceMode = src.SelectInstanceMode
	
	dest.validityExtMethod = src.ValidityExtMethod
	
	dest.extendDays = src.ExtendDays
	
	dest.freeUnitInstanceID = src.FreeUnitInstanceID
	
	dest.freeUnitType = src.FreeUnitType
	
}

def srcMidVar = srcArgs0.BatchAdjustmentRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.BatchAdjustmentRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.BatchAdjustmentRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.BatchAdjustmentRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.BatchAdjustmentRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar.Version

def srcMidVar4 = srcArgs0.BatchAdjustmentRequestMsg.BatchAdjustmentRequest

mappingList(srcMidVar4.AdjustmentInfo,destArgs1.balanceAdjustments,listMapping1)

destArgs1.adjustmentReasonCode = srcMidVar4.AdjustmentReasonCode

destArgs1.requestFileName = srcMidVar4.FileName

destArgs1.opType = srcMidVar4.OpType

mappingList(srcMidVar4.FreeUnitAdjustmentInfo,destArgs1.freeUnitAdjustments,listMapping2)

mappingList(srcMidVar4.AdditionalProperty,destArgs1.additionalProperty,listMapping0)
