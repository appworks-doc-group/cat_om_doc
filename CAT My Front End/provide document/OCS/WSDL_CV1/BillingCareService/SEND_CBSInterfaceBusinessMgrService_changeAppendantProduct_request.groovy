dest.setServiceOperation("CBSInterfaceBusinessMgrService","changeAppendantProduct")

def srcReqDocMsg = src.payload._args[0]

def destMsgHeader = dest.payload._args[0]
destMsgHeader._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12MessageHeader"

def destReqData = dest.payload._args[1]
destReqData._class = "com.huawei.ngcbs.cm.ocs12ws.subscriber.changeappprods.io.ChangeAppProdsRequestOcs12Base"


def srcReqHeaderDoc = srcReqDocMsg.ChangeAppendantProductRequestMsg.RequestHeader

destMsgHeader.beId = srcReqHeaderDoc.TenantId
destMsgHeader.commandId = srcReqHeaderDoc.CommandId
destMsgHeader.version = srcReqHeaderDoc.Version
destMsgHeader.transactionId = srcReqHeaderDoc.TransactionId
destMsgHeader.sequenceId = srcReqHeaderDoc.SequenceId
destMsgHeader.requestType = srcReqHeaderDoc.RequestType
destMsgHeader.loginSystem = srcReqHeaderDoc.SessionEntity.Name
destMsgHeader.password = srcReqHeaderDoc.SessionEntity.Password
destMsgHeader.remoteAddress = srcReqHeaderDoc.SessionEntity.RemoteAddress
destMsgHeader.interFrom = srcReqHeaderDoc.InterFrom
destMsgHeader.interMedi = srcReqHeaderDoc.InterMedi
destMsgHeader.interMode = srcReqHeaderDoc.InterMode
destMsgHeader.visitArea = srcReqHeaderDoc.visitArea
destMsgHeader.currentCell = srcReqHeaderDoc.currentCell
destMsgHeader.additionInfo = srcReqHeaderDoc.additionInfo
destMsgHeader.thirdPartyId = srcReqHeaderDoc.ThirdPartyID
destMsgHeader.partnerId = srcReqHeaderDoc.PartnerID
destMsgHeader.operatorId = srcReqHeaderDoc.OperatorID
destMsgHeader.tradePartnerId = srcReqHeaderDoc.TradePartnerID
destMsgHeader.partnerOperId = srcReqHeaderDoc.PartnerOperID
destMsgHeader.belToAreaId = srcReqHeaderDoc.BelToAreaID
destMsgHeader.reserve2 = srcReqHeaderDoc.Reserve2
destMsgHeader.reserve3 = srcReqHeaderDoc.Reserve3
destMsgHeader.messageSeq = srcReqHeaderDoc.SerialNo
destMsgHeader.remark = srcReqHeaderDoc.Remark

def srcReqDataDoc = srcReqDocMsg.ChangeAppendantProductRequestMsg.ChangeAppendantProductRequest
destReqData.subscriberNo = srcReqDataDoc.SubscriberNo
destReqData.handlingChargeFlag = srcReqDataDoc.HandlingChargeFlag


def listServicePropsMapping = 
{
	src,dest  ->
	
	dest.id = src.Id
	dest.value = src.Value	
}

def listProdServicesMapping = 
{
	src,dest  ->
	
	dest.id = src.Id
	
	mappingList(src.SimpleProperty, dest.serviceProps, listServicePropsMapping)	
}

def listProdPropsMapping = 
{
	src,dest  ->
	
	dest.id = src.Id
	dest.value = src.Value
}


def listProdMapping = 
{
  src,dest  ->

	dest.operationType = src.OperationType	
	dest.id = src.Id
	dest.prodOrderKey = src.ProductOrderKey
	dest.validMode = src.ValidMode
	dest.effDate = src.EffectiveDate
	dest.expDate = src.ExpireDate
	dest.status = src.Status
	
	mappingList(src.SimpleProperty, dest.prodOrderProps, listProdPropsMapping)
	
	mappingList(src.Service, dest.prodServices, listProdServicesMapping)	
}

mappingList(srcReqDataDoc.Product, destReqData.changedProdOrders, listProdMapping)