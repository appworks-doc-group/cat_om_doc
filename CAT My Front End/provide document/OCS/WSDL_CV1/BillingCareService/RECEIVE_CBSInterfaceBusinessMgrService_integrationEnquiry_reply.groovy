def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Id = src.code
	
	dest.Value = src.value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.BillStatus = src.billStatus
	
	dest.CurCycleEndTime=formatDate(src.curCycleEndTime, "yyyyMMddHHmmss")
	
	dest.CurCycleStartTime=formatDate(src.curCycleStartTime, "yyyyMMddHHmmss")
	
	dest.EffectiveDate=formatDate(src.effectiveDate, "yyyyMMddHHmmss")
	
	dest.ExpiredDate=formatDate(src.expireDate, "yyyyMMddHHmmss")
	
	dest.Id = src.id
	
	dest.ProductOrderKey = src.productOrderKey
	
	mappingList(src.simplePropertyList,dest.SimpleProperty,listMapping1)
	
	dest.Status = src.status
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.Id = src.code
	
	dest.Value = src.value
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.Id = src.id
	
	dest.RegistrationTime=formatDate(src.registrationTime, "yyyyMMddHHmmss")
	
	mappingList(src.simplePropertyList,dest.SimpleProperty,listMapping3)
	
	dest.Status = src.status
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.Id = src.code
	
	dest.Value = src.value
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.GroupID = src.groupId
	
	dest.GroupName = src.groupName
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.AccountCredit = src.accountCredit
	
	dest.AccountKey = src.accountKey
	
	dest.AccountType = src.accountType
	
	dest.Balance = src.balance
	
	dest.BalanceDesc = src.balanceDesc
	
	dest.ExpireTime=formatDate(src.expireTime, "yyyyMMddHHmmss")
	
	dest.MinMeasureId = src.minMeasureId
	
	dest.ProductID = src.productId
	
	dest.ProductOrderKey = src.productOrderKey
	
	dest.UnitType = src.unitType
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.CumulateBeginTime=formatDate(src.cumulateBeginTime, "yyyyMMddHHmmss")
	
	dest.CumulateEndTime=formatDate(src.cumulateEndTime, "yyyyMMddHHmmss")
	
	dest.CumulateID = src.cumulateId
	
	dest.CumulativeAmt = src.cumulativeAmt
	
}

destReturn.IntegrationEnquiryResultMsg.IntegrationEnquiryResult.BillingCycleDate.BillCycleEndDate=formatDate(srcReturn.integrationEnquiryResultInfo.billCycleDate.billCycleEndDate, "yyyyMMdd")

destReturn.IntegrationEnquiryResultMsg.IntegrationEnquiryResult.BillingCycleDate.BillCycleOpenDate=formatDate(srcReturn.integrationEnquiryResultInfo.billCycleDate.billCycleOpenDate, "yyyyMMdd")

def destMidVar = destReturn.IntegrationEnquiryResultMsg.IntegrationEnquiryResult.BillingCycleDate

def srcMidVar = srcReturn.integrationEnquiryResultInfo.billCycleDate

destMidVar.BillCycleType = srcMidVar.billCycleType

def srcMidVar0 = srcReturn.integrationEnquiryResultInfo.subscirberInfo

def destMidVar0 = destReturn.IntegrationEnquiryResultMsg.IntegrationEnquiryResult.SubscriberInfo

mappingList(srcMidVar0.productList,destMidVar0.Product,listMapping0)

mappingList(srcMidVar0.serviceList,destMidVar0.Service,listMapping2)

def destMidVar1 = destReturn.IntegrationEnquiryResultMsg.IntegrationEnquiryResult.SubscriberInfo.Subscriber

def srcMidVar1 = srcReturn.integrationEnquiryResultInfo.subscirberInfo.subscriberBasic

destMidVar1.BelToAreaID = srcMidVar1.belToAreaId

destMidVar1.BrandId = srcMidVar1.brandId

destMidVar1.Code = srcMidVar1.code

destMidVar1.IMSI = srcMidVar1.imsi

destMidVar1.InitialCredit = srcMidVar1.initialCredit

destMidVar1.Lang = srcMidVar1.lang

destMidVar1.MainProductID = srcMidVar1.mainProductId

destMidVar1.PaidMode = srcMidVar1.paidMode

destMidVar0.Subscriber.RegistrationTime=formatDate(srcMidVar0.subscriberBasic.registrationTime, "yyyyMMddHHmmss")

mappingList(srcMidVar1.simplePropertyList,destMidVar1.SimpleProperty,listMapping4)

destMidVar1.SMSLang = srcMidVar1.smsLang

destMidVar1.USSDLang = srcMidVar1.ussdLang

def destMidVar2 = destReturn.IntegrationEnquiryResultMsg.IntegrationEnquiryResult.SubscriberState

def srcMidVar2 = srcReturn.integrationEnquiryResultInfo.subscirberState

destMidVar2.ActiveCAC = srcMidVar2.activeCAC

destMidVar2.ActiveStop=formatDate(srcMidVar2.activeStop, "yyyyMMdd")

destMidVar2.DisableStop=formatDate(srcMidVar2.disableStop, "yyyyMMdd")

destMidVar2.DPEndDate=formatDate(srcMidVar2.dpEndDate, "yyyyMMdd")

destMidVar2.DPFlag = srcMidVar2.dpFlag

destMidVar2.FirstActiveDate=formatDate(srcMidVar2.firstActiveDate, "yyyyMMddHHmmss")

destMidVar2.FraudState = srcMidVar2.fraudState

destMidVar2.LifeCycleState = srcMidVar2.lifeCycleState

destMidVar2.LockedFlag = srcMidVar2.lockedFlag

destMidVar2.LossFlag = srcMidVar2.lossFlag

destMidVar2.POSUserState = srcMidVar2.posUserState

destMidVar2.SuspendStop=formatDate(srcMidVar2.suspendStop, "yyyyMMdd")

def destMidVar3 = destReturn.IntegrationEnquiryResultMsg.ResultHeader

def srcMidVar3 = srcReturn.resultHeader

destMidVar3.CommandId = srcMidVar3.commandId

destMidVar3.ResultCode = srcMidVar3.resultCode

destMidVar3.ResultDesc = srcMidVar3.resultDesc

destMidVar3.SequenceId = srcMidVar3.sequenceId

destMidVar3.SerialNo = srcMidVar3.serialNo

destMidVar3.TransactionId = srcMidVar3.transactionId

destMidVar3.Version = srcMidVar3.version

def srcMidVar4 = srcReturn.integrationEnquiryResultInfo

def destMidVar4 = destReturn.IntegrationEnquiryResultMsg.IntegrationEnquiryResult.UserGroupList

mappingList(srcMidVar4.groupInfoList,destMidVar4.UserGroup,listMapping5)

def destMidVar5 = destReturn.IntegrationEnquiryResultMsg.IntegrationEnquiryResult.BalanceRecordList

mappingList(srcMidVar4.balanceRecordList,destMidVar5.BalanceRecord,listMapping6)

def destMidVar6 = destReturn.IntegrationEnquiryResultMsg.IntegrationEnquiryResult.CumulativeItemList

mappingList(srcMidVar4.cumulativeItemList,destMidVar6.CumulativeItem,listMapping7)
