import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}



def destMidVar = destReturn.QueryExtentionInfoResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.Version = srcMidVar.version

destMidVar.MessageSeq = srcMidVar.messageSeq

mappingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def destMidVar3 = destReturn.QueryExtentionInfoResultMsg.QueryExtentionInfoResult


if(!isNull(srcReturn.queryNpDataInfo))
{
	destMidVar3.ExtentionInfo[0] = srcReturn.queryNpDataInfo[0].extentionInfo

	destMidVar3.EffectiveTime[0] =formatDate(srcReturn.queryNpDataInfo[0].effectiveTime,Constant4Model.DATE_FORMAT)

	destMidVar3.ExpirationTime[0] =formatDate(srcReturn.queryNpDataInfo[0].expirationTime,Constant4Model.DATE_FORMAT)
}
else
{
	destMidVar3.value = ""
}
	



