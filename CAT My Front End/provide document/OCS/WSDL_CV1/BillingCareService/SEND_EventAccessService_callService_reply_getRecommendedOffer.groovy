def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.attrMapKey = src.key
	
	dest.attrMapValue = src.value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.clickUrl = src.clickUrl
	
	dest.desc = src.desc
	
	dest.displayMode = src.displayMode
	
	dest.offerId = src.offerId
	
	dest.offerName = src.offerName
	
	dest.offerType = src.offerType
	
	dest.priority = src.priority
	
	dest.url = src.url
	
	mappingList(src.offerAttrMap,dest.offerAttrMap,listMapping1)
	
}

def srcMidVar = srcReturn.getRecommendedOfferResponse.return

destReturn.resultCode = srcMidVar.reresultCode

destReturn.resultMessage = srcMidVar.resultMessage

mappingList(srcMidVar.offerList,destReturn.offerList,listMapping0)

destReturn._class = "com.huawei.ngcbs.cm.common.ws.client.io.isop.RecommendedOfferResult"
