def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destArgs1 = dest.payload._args[1]

def srcMidVar = srcArgs0.ChangeCustBasicInforRequestMsg.RequestHeader

destArgs0.beId = srcMidVar.TenantId

destArgs0.operatorId = srcMidVar.OperatorID

destArgs0.additionInfo = srcMidVar.additionInfo

destArgs0.commandId = srcMidVar.CommandId

destArgs0.interMedi = srcMidVar.InterMedi

destArgs0.transactionId = srcMidVar.TransactionId

destArgs0.reserve2 = srcMidVar.Reserve2

destArgs0.thirdPartyId = srcMidVar.ThirdPartyID

destArgs0.reserve3 = srcMidVar.Reserve3

destArgs0.interMode = srcMidVar.InterMode

destArgs0.sequenceId = srcMidVar.SequenceId

destArgs0.visitArea = srcMidVar.visitArea

destArgs0.messageSeq = srcMidVar.SerialNo

destArgs0.belToAreaId = srcMidVar.BelToAreaID

destArgs0.currentCell = srcMidVar.currentCell

destArgs0.partnerId = srcMidVar.PartnerID

destArgs0.remark = srcMidVar.Remark

destArgs0.version = srcMidVar.Version

destArgs0.tradePartnerId = srcMidVar.TradePartnerID

def srcMidVar0 = srcArgs0.ChangeCustBasicInforRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar0.Name

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteAddress

destArgs0.requestType = srcMidVar.RequestType

destArgs0.interFrom = srcMidVar.InterFrom

def srcMidVar1 = srcArgs0.ChangeCustBasicInforRequestMsg.ChangeCustBasicInforRequest

destArgs1.custSkill = srcMidVar1.Skill

destArgs1.custCreditGrade = srcMidVar1.CreditGrade

def destMidVar = destArgs1.changeCustBasicInfo.individualInfo

destMidVar.idNumber = srcMidVar1.IdCode

def destMidVar0 = destArgs1.changeCustBasicInfo.customerInfo

destMidVar0.custCode = srcMidVar1.Code

destMidVar.race = srcMidVar1.NationType

destArgs1.strBirthday = srcMidVar1.Birthday

destMidVar0.custLevel = srcMidVar1.Grade

destMidVar.marriedStatus = srcMidVar1.MaritalStatus

def listMapping0

listMapping0 = 
{
    src0,dest0  ->

	dest0.code = src0.Id
	
	dest0.value = src0.Value
	
}

def destMidVar1 = destArgs1.changeCustBasicInfo

addingList(srcMidVar1.SimpleProperty,destMidVar1.custProperties,listMapping0)

def destMidVar2 = destArgs1.custAccessCode

destMidVar2.primaryIdentity = srcMidVar1.SubscriberNo

destMidVar0.custType = srcMidVar1.CustomerType

destArgs1.custState = srcMidVar1.CustomerState

destMidVar.nationality = srcMidVar1.Country

def destMidVar3 = destArgs1.addressInfo

destMidVar3.postCode = srcMidVar1.ZipCode

destArgs1.custCreditAmount = srcMidVar1.CreditAmount

destMidVar.firstName = srcMidVar1.Name

destMidVar.occupation = srcMidVar1.JobType

destMidVar.education = srcMidVar1.Education

destMidVar.email = srcMidVar1.Email

destMidVar.nativePlace = srcMidVar1.NativePlace

destArgs1.custSocialNo = srcMidVar1.SocialNo

destMidVar3.addr1 = srcMidVar1.Address

destMidVar.gender = srcMidVar1.Gender

destArgs1.custBelToAreaID = srcMidVar1.BelToAreaID

destMidVar.idType = srcMidVar1.IdType
