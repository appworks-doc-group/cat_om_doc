def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.zoneName = src.zoneName
	
	dest.zoneType = src.zoneType
	
	dest.zonetID = src.zoneId
	
}

def destMidVar = destReturn.QueryZoneMappingResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

mappingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

destMidVar.Version = srcMidVar.version

destMidVar.MessageSeq = srcMidVar.messageSeq

def destMidVar0 = destReturn.QueryZoneMappingResultMsg.QueryZoneMappingResult

mappingList(srcReturn.zoneMappingInfos,destMidVar0.zoneDetail,listMapping1)
