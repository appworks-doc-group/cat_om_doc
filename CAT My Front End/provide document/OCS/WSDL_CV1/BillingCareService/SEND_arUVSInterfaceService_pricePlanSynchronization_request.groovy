dest.setServiceOperation("arUVSInterfaceService", "pricePlanSynchronization")


def srcReqDocMsg = src.payload._args[0]
def destArgs0 = dest.payload._args[0]
def destArgs1 = dest.payload._args[1]

destArgs0._class = "com.huawei.ngcbs.cm.ocs11ws.core.bo.Ocs11MessageHeader"
destArgs1._class = "com.huawei.ngcbs.cm.ocs11ws.io.priceplansynchronization.OCS11PricePlanSynchronizationRequest"

def srcReqHeaderDoc = srcReqDocMsg.pricePlanSynchronization.PricePlanSynchronizationRequest.RequestMessage.MessageHeader
def srcReqBodyDoc = srcReqDocMsg.pricePlanSynchronization.PricePlanSynchronizationRequest.RequestMessage.MessageBody

def srcSessionEntity = srcReqDocMsg.pricePlanSynchronization.SessionEntity
destArgs0.commandId = srcReqHeaderDoc.CommandId
destArgs0.version = srcReqHeaderDoc.Version
destArgs0.transactionId = srcReqHeaderDoc.TransactionId
destArgs0.sequenceId = srcReqHeaderDoc.SequenceId
destArgs0.requestType = srcReqHeaderDoc.RequestType
destArgs0.loginSystem = srcSessionEntity.userID
destArgs0.password = srcSessionEntity.password
destArgs0.remoteAddress = srcSessionEntity.remoteAddr

def destSessionEntity = destArgs0.sessionEntity
destSessionEntity.userID = srcSessionEntity.userID
destSessionEntity.password = srcSessionEntity.password
destSessionEntity.locale = srcSessionEntity.locale
destSessionEntity.loginVia = srcSessionEntity.loginVia
destSessionEntity.remoteAddr = srcSessionEntity.remoteAddr
destSessionEntity.uploadRoot = srcSessionEntity.uploadRoot
 
destArgs1.productID = srcReqBodyDoc.ProductID
destArgs1.productType = srcReqBodyDoc.ProductType
destArgs1.priceID = srcReqBodyDoc.PriceID
destArgs1.subBrandID = srcReqBodyDoc.SubBrandID

