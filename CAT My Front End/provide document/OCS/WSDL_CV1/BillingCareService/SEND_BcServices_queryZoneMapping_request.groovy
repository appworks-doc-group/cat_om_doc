dest.setServiceOperation("BMQueryService","queryZoneMapping")
def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.query.queryzonemapping.io.QueryZoneMappingRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.QueryZoneMappingRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.QueryZoneMappingRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.QueryZoneMappingRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.QueryZoneMappingRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.QueryZoneMappingRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar.Version

nameValueListToObject(srcArgs0.QueryZoneMappingRequestMsg.QueryZoneMappingRequest.AdditionalProperty,destArgs1.additionalProperty,"Value","Code")

def destMidVar = destArgs1.additionalProperty

def srcMidVar4 = srcArgs0.QueryZoneMappingRequestMsg.QueryZoneMappingRequest.AdditionalProperty[0]

destMidVar.code = srcMidVar4.Code

destMidVar.value = srcMidVar4.Value

def srcMidVar5 = srcArgs0.QueryZoneMappingRequestMsg.QueryZoneMappingRequest

destArgs1.locationID = srcMidVar5.locationID

destArgs1.locationType = srcMidVar5.locationType
