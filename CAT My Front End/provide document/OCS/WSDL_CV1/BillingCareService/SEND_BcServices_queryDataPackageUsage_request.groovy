dest.setServiceOperation("BMQueryService","queryDataPackageUsage")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destArgs1 = dest.payload._args[1]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.value = src.Value
	
	dest.code = src.Code
	
}

def destMidVar = destArgs1.subAccessCode

def srcMidVar = srcArgs0.QueryDataPackageUsageRequestMsg.QueryDataPackageUsageRequest.SubAccessCode

destMidVar.primaryIdentity = srcMidVar.PrimaryIdentity

destMidVar.subscriberKey = srcMidVar.SubscriberKey

def srcMidVar0 = srcArgs0.QueryDataPackageUsageRequestMsg.RequestHeader

destArgs0.innerInterMode = srcMidVar0.AccessMode

destArgs0.messageSeq = srcMidVar0.MessageSeq

destArgs0.businessCode = srcMidVar0.BusinessCode

def srcMidVar1 = srcArgs0.QueryDataPackageUsageRequestMsg.RequestHeader.OperatorInfo

destArgs0.operatorId = srcMidVar1.OperatorID

destArgs0.channelId = srcMidVar1.ChannelID

addingList(srcMidVar0.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.msgLanguageCode = srcMidVar0.MsgLanguageCode

def srcMidVar2 = srcArgs0.QueryDataPackageUsageRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar2.LoginSystemCode

destArgs0.password = srcMidVar2.Password

destArgs0.remoteAddress = srcMidVar2.RemoteIP

destArgs0.version = srcMidVar0.Version

def srcMidVar3 = srcArgs0.QueryDataPackageUsageRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar3.BEID

destArgs0.brId = srcMidVar3.BRID

def srcMidVar4 = srcArgs0.QueryDataPackageUsageRequestMsg.RequestHeader.TimeFormat

destArgs0.timeZoneId = srcMidVar4.TimeZoneID

destArgs0.timeType = srcMidVar4.TimeType
