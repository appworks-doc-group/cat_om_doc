dest.setServiceOperation("arUVSInterfaceExtend","adjustCredit")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destArgs1 = dest.payload._args[1]

destArgs0._class = "com.huawei.ngcbs.cm.ocs11ws.core.bo.Ocs11MessageHeader"

destArgs1._class = "com.huawei.ngcbs.cm.ocs11ws.io.adjustcredit.AdjustCreditRequest"

def srcMessageHeader = srcArgs0.AdjustCredit.AdjustCreditRequest.RequestMessage.MessageHeader

def srcMessageBody = srcArgs0.AdjustCredit.AdjustCreditRequest.RequestMessage.MessageBody

def srcSessionEntity = srcArgs0.AdjustCredit.SessionEntity

def destSessionEntity = destArgs0.sessionEntity

destArgs0.loginSystem = srcSessionEntity.userID
destArgs0.password = srcSessionEntity.password

destSessionEntity.userID = srcSessionEntity.userID
destSessionEntity.password = srcSessionEntity.password
destSessionEntity.remoteAddr = srcSessionEntity.remoteAddr
destSessionEntity.locale = srcSessionEntity.locale
destSessionEntity.loginVia = srcSessionEntity.loginVia
destSessionEntity.uploadRoot = srcSessionEntity.uploadRoot

destArgs0.commandId = srcMessageHeader.CommandId
destArgs0.version = srcMessageHeader.Version
destArgs0.transactionId = srcMessageHeader.TransactionId
destArgs0.sequenceId = srcMessageHeader.SequenceId
destArgs0.requestType = srcMessageHeader.RequestType
destArgs0.tenantId = srcMessageHeader.TenantId
destArgs0.language = srcMessageHeader.Language

destArgs1.acctAccessCode.accoutKey = srcMessageBody.AcctId

destArgs1.acctAccessCode.accoutCode = srcMessageBody.AcctCode

destArgs1.subAccessCode.subscriberKey = srcMessageBody.subId

destArgs1.subAccessCode.primaryIdentity = srcMessageBody.msisdn

destArgs1.adjustType = srcMessageBody.AdjustType

destArgs1.adjustFlag = srcMessageBody.AdjustFlag

destArgs1.amount = srcMessageBody.Amount

destArgs1.adjustReson = srcMessageBody.AdjustReson
