import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("SubscriberService","penaltyCalculation")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.penaltycalculation.io.PenaltyCalculationRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.oCode = src.OfferingCode
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.oCode = src.OfferingCode
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.oCode = src.OfferingCode
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.oCode = src.OfferingCode
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.cancelOfferingInst.beginTime=parseDate(src.CancelOfferingInst.BeginTime,Constant4Model.DATE_FORMAT)
	
	dest.cancelOfferingInst.endTime=parseDate(src.CancelOfferingInst.EndTime,Constant4Model.DATE_FORMAT)
	
	def destMidVar = dest.cancelOfferingInst
	
	def srcMidVar5 = src.CancelOfferingInst
	
	destMidVar.offeringInstId = srcMidVar5.OfferingInstID
	
	listMapping2.call(srcMidVar5.OfferingKey,destMidVar.offeringKey)
	
	listMapping2.call(srcMidVar5.DowngradeOfferingKey,destMidVar.downgradeOfferingKey)
	
	dest.downgradeOfferingInst.beginTime=parseDate(src.DowngradeOfferingInst.BeginTime,Constant4Model.DATE_FORMAT)
	
	dest.downgradeOfferingInst.endTime=parseDate(src.DowngradeOfferingInst.EndTime,Constant4Model.DATE_FORMAT)
	
	def destMidVar0 = dest.downgradeOfferingInst
	
	def srcMidVar6 = src.DowngradeOfferingInst
	
	destMidVar0.offeringInstId = srcMidVar6.OfferingInstID
	
	listMapping3.call(srcMidVar6.OfferingKey,destMidVar0.offeringKey)
	
	dest.promotionOfferingInst.beginTime=parseDate(src.PromotionOfferingInst.BeginTime,Constant4Model.DATE_FORMAT)
	
	dest.promotionOfferingInst.endTime=parseDate(src.PromotionOfferingInst.EndTime,Constant4Model.DATE_FORMAT)
	
	def destMidVar1 = dest.promotionOfferingInst
	
	def srcMidVar7 = src.PromotionOfferingInst
	
	destMidVar1.offeringInstId = srcMidVar7.OfferingInstID
	
	listMapping4.call(srcMidVar7.OfferingKey,destMidVar1.offeringKey)

	dest.downgradePromotionOfferingInst.beginTime=parseDate(src.DowngradePromotionOfferingInst.BeginTime,Constant4Model.DATE_FORMAT)

	dest.downgradePromotionOfferingInst.endTime=parseDate(src.DowngradePromotionOfferingInst.EndTime,Constant4Model.DATE_FORMAT)

	def destMidVar2 = dest.downgradePromotionOfferingInst

	def srcMidVar8 = src.DowngradePromotionOfferingInst

	destMidVar2.offeringInstId = srcMidVar8.OfferingInstID
	
	listMapping5.call(srcMidVar8.OfferingKey,destMidVar2.offeringKey)
}

def srcMidVar = srcArgs0.PenaltyCalculationRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar.LoginSystemCode

destArgs0.password = srcMidVar.Password

destArgs0.remoteAddress = srcMidVar.RemoteIP

def srcMidVar0 = srcArgs0.PenaltyCalculationRequestMsg.RequestHeader

mappingList(srcMidVar0.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar0.BusinessCode

destArgs0.messageSeq = srcMidVar0.MessageSeq

destArgs0.msgLanguageCode = srcMidVar0.MsgLanguageCode

def srcMidVar1 = srcArgs0.PenaltyCalculationRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.PenaltyCalculationRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.PenaltyCalculationRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar0.Version

def srcMidVar4 = srcArgs0.PenaltyCalculationRequestMsg.PenaltyCalculationRequest

destArgs1.opType = srcMidVar4.OpType

mappingList(srcMidVar4.PenaltyOfferingInstInfoList,destArgs1.penaltyOfferingInstInfoList,listMapping1)

def destMidVar2 = destArgs1.subAccessCode

def srcMidVar8 = srcArgs0.PenaltyCalculationRequestMsg.PenaltyCalculationRequest.SubAccessCode

destMidVar2.primaryIdentity = srcMidVar8.PrimaryIdentity

destMidVar2.subscriberKey = srcMidVar8.SubscriberKey

def destSubGroupAccessCode = destArgs1.groupAccessCode

def srcSubGroupAccessCode = srcArgs0.PenaltyCalculationRequestMsg.PenaltyCalculationRequest.SubGroupAccessCode

destSubGroupAccessCode.groupKey = srcSubGroupAccessCode.SubGroupKey

destSubGroupAccessCode.groupCode = srcSubGroupAccessCode.SubGroupCode
