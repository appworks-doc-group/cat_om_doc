import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("CustomerService","changeTaxExemption")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.customer.changetaxexempt.io.ChangeTaxExemptionRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.exptPlanCode = src.ExptPlanCODE
	
	def destMidVar0 = dest.taxExemptionInfo
	
	destMidVar0.objKey = src.ObjKey
	
	destMidVar0.objType = src.ObjType
	
	destMidVar0.taxScope = src.TaxScope
	
	def srcMidVar6 = src.EffectiveTime
	
	dest.effMode = srcMidVar6.Mode
	
	destMidVar0.effDate = parseDate(srcMidVar6.Time, Constant4Model.DATE_FORMAT)
    
  destMidVar0.expDate = parseDate(src.ExpirationTime, Constant4Model.DATE_FORMAT)
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.exptPlanCode = src.ExptPlanCODE
	
	def destMidVar1 = dest.taxExemptionInfo
	
	destMidVar1.objType = src.ObjType
	
	destMidVar1.objKey = src.ObjKey
	
	def srcMidVar7 = src.ExpirationTime
	
	dest.expMode = srcMidVar7.Mode
	
	destMidVar1.expDate = parseDate(srcMidVar7.Time, Constant4Model.DATE_FORMAT)
	
}

def srcMidVar = srcArgs0.ChangeTaxExemptionRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.ChangeTaxExemptionRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.ChangeTaxExemptionRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.ChangeTaxExemptionRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.ChangeTaxExemptionRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar.Version

destArgs0.businessCode = srcMidVar.BusinessCode

def srcMidVar4 = srcArgs0.ChangeTaxExemptionRequestMsg.ChangeTaxExemptionRequest

def destMidVar = destArgs1.changeTaxExemptionInfo4CM

mappingList(srcMidVar4.AddExemption,destMidVar.addTaxExemptions,listMapping1)

mappingList(srcMidVar4.DelExemption,destMidVar.delTaxExemptions,listMapping2)

def destMidVar2 = destArgs1.custAccessCode

def srcMidVar5 = srcArgs0.ChangeTaxExemptionRequestMsg.ChangeTaxExemptionRequest.RegisterCust

destMidVar2.customerCode = srcMidVar5.CustomerCode

destMidVar2.customerKey = srcMidVar5.CustomerKey

destMidVar2.primaryIdentity = srcMidVar5.PrimaryIdentity
