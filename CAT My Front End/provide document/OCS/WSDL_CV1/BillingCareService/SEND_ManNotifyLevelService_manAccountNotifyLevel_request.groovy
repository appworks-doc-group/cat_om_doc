def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs33ws.core.bo.Ocs33MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs33ws.account.manaccountnotifylevel.io.OCS33ManAccountNotifyLevelRequest"

def srcReqHeaderDoc = srcArgs0.ManAccountNotifyLevelRequestMsg.RequestHeader

def srcReqDataDoc = srcArgs0.ManAccountNotifyLevelRequestMsg.ManAccountNotifyLevelRequest

destArgs0.operatorId = srcReqHeaderDoc.OperatorID

destArgs0.additionInfo = srcReqHeaderDoc.additionInfo

destArgs0.commandId = srcReqHeaderDoc.CommandId

destArgs0.transactionId = srcReqHeaderDoc.TransactionId

destArgs0.interMedi = srcReqHeaderDoc.InterMedi

destArgs0.reserve2 = srcReqHeaderDoc.Reserve2

destArgs0.thirdPartyId = srcReqHeaderDoc.ThirdPartyID

destArgs0.reserve3 = srcReqHeaderDoc.Reserve3

destArgs0.sequenceId = srcReqHeaderDoc.SequenceId

destArgs0.visitArea = srcReqHeaderDoc.visitArea

destArgs0.belToAreaId = srcReqHeaderDoc.BelToAreaID

destArgs0.currentCell = srcReqHeaderDoc.currentCell

destArgs0.partnerId = srcReqHeaderDoc.PartnerID

destArgs0.tradePartnerId = srcReqHeaderDoc.TradePartnerID

destArgs0.partnerOperId = srcReqHeaderDoc.PartnerOperID

destArgs0.version = srcReqHeaderDoc.Version

destArgs0.remark = srcReqHeaderDoc.Remark

destArgs0.loginSystem = srcReqHeaderDoc.SessionEntity.Name

destArgs0.password = srcReqHeaderDoc.SessionEntity.Password

destArgs0.remoteAddress = srcReqHeaderDoc.SessionEntity.RemoteAddress

destArgs0.performanceStatCmd = "ManAccountNotifyLevel"

destArgs0.interFrom = srcReqHeaderDoc.InterFrom

destArgs0.requestType = srcReqHeaderDoc.RequestType

destArgs0.messageSeq = srcReqHeaderDoc.SerialNo

destArgs1.subscriberNo = srcReqDataDoc.SubscriberNo

def destArgs2 = destArgs1.ocs33AccountLevelInfo

destArgs2.objectType = srcReqDataDoc.AccountLevelInfo.ObjectType

destArgs2.accountObject = srcReqDataDoc.AccountLevelInfo.AccountObject

def destArgs3 = destArgs2.levelList

def srcArgs3 = srcReqDataDoc.AccountLevelInfo.LevelList

listMapping0 = 
{
    src,dest  ->

	dest.level = src.Level
	
	dest.sequenceNo = src.SequenceNo
	
	
}

mappingList(srcArgs3,destArgs3,listMapping0)