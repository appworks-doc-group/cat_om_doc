def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.BelToAreaID = src.belToAreaId
	
	dest.CommandId = src.commandId
	
	dest.currentCell = src.currentCell
	
	dest.InterFrom = src.interFrom
	
	dest.OperatorID = src.operatorId
	
	dest.PartnerID = src.partnerId
	
	dest.PartnerOperID = src.partnerOperId
	
	dest.Remark = src.remark
	
	dest.SequenceId = src.sequenceId
	
	dest.SerialNo = src.serialNo
	
	def destMidVar1 = dest.SessionEntity
	
	def srcMidVar = src.sessionEntity
	
	destMidVar1.Name = srcMidVar.name
	
	destMidVar1.Password = srcMidVar.password
	
	destMidVar1.RemoteAddress = srcMidVar.remoteAddress
	
	dest.TenantID = src.tenantId
	
	dest.TransactionId = src.transactionId
	
	dest.Version = src.version
	
	dest.visitArea = src.visitArea
	
	dest.InterMedi = src.interMedi
	
	dest.InterMode = src.interMode
	
	dest.RequestType = src.requestType
	
	dest.ThirdPartyID = src.thirdPartyId
	
	dest.TradePartnerID = src.tradePartnerId
	
}

def destMidVar = destArgs0.QuerySpecnumberTypeRequestMsg.QuerySpecnumberTypeRequest

destMidVar.MatchType = srcArgs0.matchType

destMidVar.SubscriberNo = srcArgs0.subscriberNo

def destMidVar0 = destArgs0.QuerySpecnumberTypeRequestMsg

listMapping0.call(srcArgs0.omRequestHeader,destMidVar0.RequestHeader)
