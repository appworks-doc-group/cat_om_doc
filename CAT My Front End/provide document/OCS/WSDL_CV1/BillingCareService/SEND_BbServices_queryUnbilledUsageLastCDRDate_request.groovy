dest.setServiceOperation("BMQueryService","queryUnbilledUsageLastCDRDate")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.query.queryunbilledusagelastcdrdate.io.QueryUnbilledUsageLastCDRDateRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.usageCategory = src.UsageCategory
	
}

def srcMidVar = srcArgs0.QueryUnbilledUsageLastCDRDateRequestMsg.QueryUnbilledUsageLastCDRDateRequest

mappingList(srcMidVar.UsageCategoryList,destArgs1.usageCategoryList,listMapping0)

def destMidVar = destArgs1.subAccessCode

def srcMidVar0 = srcArgs0.QueryUnbilledUsageLastCDRDateRequestMsg.QueryUnbilledUsageLastCDRDateRequest.SubAccessCode

destMidVar.primaryIdentity = srcMidVar0.PrimaryIdentity

destMidVar.subscriberKey = srcMidVar0.SubscriberKey

def srcMidVar1 = srcArgs0.QueryUnbilledUsageLastCDRDateRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar1.LoginSystemCode

destArgs0.remoteAddress = srcMidVar1.RemoteIP

def destMidVar0 = destArgs0.simpleProperty[0]

def srcMidVar2 = srcArgs0.QueryUnbilledUsageLastCDRDateRequestMsg.RequestHeader.AdditionalProperty[0]

destMidVar0.code = srcMidVar2.Code

destMidVar0.value = srcMidVar2.Value

def srcMidVar3 = srcArgs0.QueryUnbilledUsageLastCDRDateRequestMsg.RequestHeader

destArgs0.businessCode = srcMidVar3.BusinessCode

destArgs0.messageSeq = srcMidVar3.MessageSeq

destArgs0.msgLanguageCode = srcMidVar3.MsgLanguageCode

def srcMidVar4 = srcArgs0.QueryUnbilledUsageLastCDRDateRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar4.ChannelID

destArgs0.operatorId = srcMidVar4.OperatorID

def srcMidVar5 = srcArgs0.QueryUnbilledUsageLastCDRDateRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar5.BEID

destArgs0.brId = srcMidVar5.BRID

def srcMidVar6 = srcArgs0.QueryUnbilledUsageLastCDRDateRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar6.TimeType

destArgs0.timeZoneId = srcMidVar6.TimeZoneID

destArgs0.version = srcMidVar3.Version

destArgs0.interMode = srcMidVar3.AccessMode
