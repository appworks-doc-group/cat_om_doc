import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcResultData = src.payload._return
def destResultDoc = dest.payload._return

def destResultHeader = destResultDoc.QueryGrpCallScreenNoResultMsg.ResultHeader
def srcResultHeader = srcResultData.resultHeader

destResultHeader.Version = srcResultHeader.version
destResultHeader.MsgLanguageCode = srcResultHeader.msgLanguageCode
destResultHeader.ResultCode = srcResultHeader.resultCode
destResultHeader.ResultDesc = srcResultHeader.resultDesc
destResultHeader.MessageSeq = srcResultHeader.messageSeq

def listMappingAdditionalProperty
listMappingAdditionalProperty = 
{
    src,dest  ->
	dest.Code = src.code
	dest.Value = src.value
}
mappingList(srcResultHeader.simpleProperty,destResultHeader.AdditionalProperty,listMappingAdditionalProperty)


def destResultData = destResultDoc.QueryGrpCallScreenNoResultMsg.QueryGrpCallScreenNoResult
def srcResultBody = srcResultData.resultBody

def listMappingCallScreenNoInfo
listMappingCallScreenNoInfo = 
{
    src,dest  ->
	dest.CallScreenNo = src.callScreenNo
	dest.ScreenNoType = src.screenNoType
	dest.EffectiveDate = formatDate(src.effectiveDate, Constant4Model.DATE_FORMAT)
	dest.ExpireDate = formatDate(src.expireDate, Constant4Model.DATE_FORMAT)
	dest.WeekStart = src.weekStart
	dest.WeekStop = src.weekStop
	dest.TimeStart = src.timeStart
	dest.TimeStop = src.timeStop
	dest.RouteNumber = src.routeNumber
	dest.RoutingMethod = src.routingMethod
}
addingList(srcResultBody.callScreenNoInfos,destResultData.CallScreenNoInfo,listMappingCallScreenNoInfo)
