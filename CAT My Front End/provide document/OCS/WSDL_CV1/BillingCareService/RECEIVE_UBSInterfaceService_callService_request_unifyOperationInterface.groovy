dest.setServiceOperation("UBSInterfaceService","unifyOperationInterface")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping11

listMapping11 = 
{
    src,dest  ->

	dest.key = src.keyOfEntity
	
	dest.value = src.valueOfEntity
	
}

def listMapping12

listMapping12 = 
{
    src,dest  ->

	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	mappingList(src.entityListOfEntity,dest.entity,listMapping11)
	
	mappingList(src.itemListOfEntity,dest.item,listMapping12)
	
	dest.key = src.keyOfEntity
	
	dest.value = src.valueOfEntity
	
}

def listMapping13

listMapping13 = 
{
    src,dest  ->

	
}

def listMapping14

listMapping14 = 
{
    src,dest  ->

	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	mappingList(src.entityListOfItem,dest.entity,listMapping11)
	
	mappingList(src.itemListOfItem,dest.item,listMapping14)
	
	dest.value = src.valueOfItem
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	mappingList(src.entityListOfEntity,dest.entity,listMapping3)
	
	mappingList(src.itemListOfEntity,dest.item,listMapping4)
	
	dest.key = src.keyOfEntity
	
	dest.value = src.valueOfEntity
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	
}

def listMapping8

listMapping8 = 
{
    src,dest  ->

	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	mappingList(src.entityListOfEntity,dest.entity,listMapping7)
	
	mappingList(src.itemListOfEntity,dest.item,listMapping8)
	
	dest.key = src.keyOfEntity
	
	dest.value = src.valueOfEntity
	
}

def listMapping9

listMapping9 = 
{
    src,dest  ->

	
}

def listMapping10

listMapping10 = 
{
    src,dest  ->

	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	mappingList(src.entityListOfItem,dest.entity,listMapping9)
	
	mappingList(src.itemListOfItem,dest.item,listMapping10)
	
	dest.value = src.valueOfItem
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.value = src.valueOfItem
	
	mappingList(src.entityListOfItem,dest.entity,listMapping5)
	
	mappingList(src.itemListOfItem,dest.item,listMapping6)
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	mappingList(src.entityListOfEntity,dest.entity,listMapping1)
	
	mappingList(src.itemListOfEntity,dest.item,listMapping2)
	
	dest.key = src.keyOfEntity
	
	dest.value = src.valueOfEntity
	
}

def destMidVar = destArgs0.UnifyOperationRequest.requestBody

def srcMidVar = srcArgs0.requestBody

destMidVar.operation = srcMidVar.operation

mappingList(srcMidVar.param,destMidVar.param,listMapping0)

def destMidVar0 = destArgs0.UnifyOperationRequest.requestHeader

def srcMidVar0 = srcArgs0.requestHeader

destMidVar0.beId = srcMidVar0.beId

destMidVar0.busiCode = srcMidVar0.busiCode

destMidVar0.channel = srcMidVar0.channel

destMidVar0.language = srcMidVar0.language

destMidVar0.password = srcMidVar0.password

destMidVar0.transactionId = srcMidVar0.transactionId

destMidVar0.username = srcMidVar0.username

destMidVar0.version = srcMidVar0.version
