def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.BalanceId = src.balanceId
	
	dest.AccountType = src.accountType
	
	dest.ChgAcctBal = src.chgAcctBal
	
	dest.ChgExpTime = src.chgExpTime
	
	dest.CurrAcctBal = src.currAcctBal
	
	dest.CurrExpTime = src.currExpTime
	
	dest.MinMeasureId = src.minMeasureId
	
}

def destMidVar = destReturn.OneOffDeductionResultMsg.OneOffDeductionResult

def srcBusinessData = srcReturn.resultBody

mappingList(srcBusinessData.acctChgRec,destMidVar.AcctChgRec,listMapping0)

def destHeader = destReturn.OneOffDeductionResultMsg.ResultHeader

def srcHeader = srcReturn.resultHeader

destHeader.CommandId = srcHeader.commandId
destHeader.ResultCode = srcHeader.resultCode
destHeader.ResultDesc = srcHeader.resultDesc
destHeader.SequenceId = srcHeader.sequenceId
destHeader.Version = srcHeader.version
destHeader.TransactionId = srcHeader.transactionId
destHeader.OperationTime = srcHeader.operationTime
destHeader.OrderId = srcHeader.orderId
