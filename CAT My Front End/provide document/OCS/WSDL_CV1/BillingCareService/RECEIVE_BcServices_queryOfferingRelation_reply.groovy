import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 =
        {
            src, dest ->

                dest.Code = src.code

                dest.Value = src.value

        }

def listMapping0

listMapping0 =
        {
            src, dest ->

                mappingList(src.simpleProperty, dest.AdditionalProperty, listMapping1)

                dest.MsgLanguageCode = src.msgLanguageCode

                dest.ResultCode = src.resultCode

                dest.ResultDesc = src.resultDesc

                dest.Version = src.version

                dest.MessageSeq = src.messageSeq

        }

def listMapping2

listMapping2 =
        {
            src, dest ->

                dest.PrimaryIdentity = src.primaryIdentity

                dest.SubscriberKey = src.subscriberKey

        }

def listMapping3

listMapping3 =
        {
            src, dest ->

                dest.OfferingID = src.oId

                dest.OfferingCode = src.oCode

                dest.PurchaseSeq = src.pSeq
        }

def listMapping4

listMapping4 =
        {
            src, dest ->

                dest.LimitCycleType = src.limitCycleType

                dest.LimitValue = src.limitValue

                dest.LimitValueExpr = src.limitValueExpr

                dest.MeasureUnit = src.measureUnit
        }

def listMapping5

listMapping5 =
        {
            src, dest ->

                dest.Mode = src.mode

                dest.Time = formatDate(src.expTime, Constant4Model.DATE_FORMAT)

        }


def listMapping6
listMapping6 =
        {
            src, dest ->

                dest.RelationKey = src.relationKey;

                listMapping2.call(src.destSubAccessCode, dest.DestSubIdentify);

                listMapping3.call(src.offeringKey, dest.OfferingKey);

                listMapping4.call(src.freeUnitShareLimit, dest.FreeUnitShareLimit);

                dest.EffectiveTime = formatDate(src.effectiveTime, Constant4Model.DATE_FORMAT);

                listMapping5.call(src.expirationTime, dest.ExpirationTime);
        }


def listMapping7

listMapping7 =
        {
            src, dest ->

                mappingList(src.offeringRelationList, dest.OfferingRelation, listMapping6)

        }
def destMidVar = destReturn.QueryOfferingRelationResultMsg

listMapping0.call(srcReturn.resultHeader, destMidVar.ResultHeader)

listMapping7.call(srcReturn.resultInfo, destMidVar.QueryOfferingRelationResult)
