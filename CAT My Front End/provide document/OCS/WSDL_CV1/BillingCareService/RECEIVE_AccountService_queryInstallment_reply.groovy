import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.CycleClass = src.cycleClass
	
	dest.DelayFlag = src.delayFlag
	
	dest.Status = src.status
	
	dest.InitialAmount = src.initialAmount
	
	dest.Amount = src.amount
	
	dest.CurrencyID = src.currencyID
	
	dest.CycleDueDate=formatDate(src.cycleDueDate, Constant4Model.DATE_FORMAT)
	
	dest.CycleSequence = src.cycleSequence
	
	dest.RealRepayDate=formatDate(src.realRepayDate, Constant4Model.DATE_FORMAT)
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def srcMidVar = srcReturn.queryInstallmentInfo

def destMidVar = destReturn.QueryInstallmentResultMsg.QueryInstallmentResult

mappingList(srcMidVar.applyInstallmentList,destMidVar.InatallmentDetail,listMapping0)

destMidVar.ContractID = srcMidVar.contractID

destMidVar.CurrencyID = srcMidVar.currencyID

destMidVar.InstallmentInstID = srcMidVar.installmentInstID

def destMidVar0 = destReturn.QueryInstallmentResultMsg.QueryInstallmentResult.OfferingKey

def srcMidVar0 = srcReturn.queryInstallmentInfo.offeringKey

destMidVar0.OfferingID = srcMidVar0.oId

destMidVar0.PurchaseSeq = srcMidVar0.pSeq

destMidVar.TotalAmount = srcMidVar.totalAmount

destMidVar.TotalCycle = srcMidVar.totalCycle

def destMidVar1 = destReturn.QueryInstallmentResultMsg.ResultHeader

def srcMidVar1 = srcReturn.resultHeader

destMidVar1.MsgLanguageCode = srcMidVar1.msgLanguageCode

destMidVar1.ResultCode = srcMidVar1.resultCode

destMidVar1.ResultDesc = srcMidVar1.resultDesc

destMidVar1.Version = srcMidVar1.version

mappingList(srcMidVar1.simpleProperty,destMidVar1.AdditionalProperty,listMapping1)
