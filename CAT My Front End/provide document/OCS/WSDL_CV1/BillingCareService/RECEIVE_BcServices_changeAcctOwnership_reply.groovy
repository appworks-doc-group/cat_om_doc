def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destReturn.ChangeAcctOwnershipResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

mappingList(srcReturn.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def destMidVar0 = destReturn.ChangeAcctOwnershipResultMsg.ChangeAcctOwnershipResult.SiteInfo

def srcMidVar0 = srcReturn.changeAcctOwnershipResultInfo.siteInfo

destMidVar0.PrimarySite = srcMidVar0.primarySite

destMidVar0.SecondarySite = srcMidVar0.secondarySite

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.Version = srcMidVar.version

destMidVar.MessageSeq = srcMidVar.messageSeq
