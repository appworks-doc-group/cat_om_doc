dest.setServiceOperation("arUVSInterfaceService", "setMainCommodity")


def srcReqDocMsg = src.payload._args[0]
def destArgs0 = dest.payload._args[0]
def destArgs1 = dest.payload._args[1]

destArgs0._class = "com.huawei.ngcbs.cm.ocs11ws.core.bo.Ocs11MessageHeader"
destArgs1._class = "com.huawei.ngcbs.cm.ocs11ws.setmaincommodity.io.Ocs11SetMainCommodityRequest"

def srcReqHeaderDoc = srcReqDocMsg.setMainCommodity.SetMainCommodityRequest.RequestMessage.MessageHeader
def srcReqBodyDoc = srcReqDocMsg.setMainCommodity.SetMainCommodityRequest.RequestMessage.MessageBody

def srcSessionEntity = srcReqDocMsg.setMainCommodity.SessionEntity
destArgs0.commandId = srcReqHeaderDoc.CommandId
destArgs0.version = srcReqHeaderDoc.Version
destArgs0.transactionId = srcReqHeaderDoc.TransactionId
destArgs0.sequenceId = srcReqHeaderDoc.SequenceId
destArgs0.requestType = srcReqHeaderDoc.RequestType
destArgs0.loginSystem = srcSessionEntity.userID
destArgs0.password = srcSessionEntity.password
destArgs0.remoteAddress = srcSessionEntity.remoteAddr

def destSessionEntity = destArgs0.sessionEntity
destSessionEntity.userID = srcSessionEntity.userID
destSessionEntity.password = srcSessionEntity.password
destSessionEntity.locale = srcSessionEntity.locale
destSessionEntity.loginVia = srcSessionEntity.loginVia
destSessionEntity.remoteAddr = srcSessionEntity.remoteAddr
destSessionEntity.uploadRoot = srcSessionEntity.uploadRoot
 
destArgs1.subCosId = srcReqBodyDoc.SubCosId
destArgs1.subCosName = srcReqBodyDoc.SubCosName
destArgs1.brandId = srcReqBodyDoc.BrandId
destArgs1.brandName = srcReqBodyDoc.BrandName

