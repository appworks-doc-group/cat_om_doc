def srcReturn = src.payload._return

def destReturn = dest.payload._return

srcReturn._class= "com.huawei.ngcbs.cm.ocs33ws.core.bo.Ocs33ResultHeader"

def destMidVar = destReturn.SyncOperatorResultMsg.ResultHeader

destMidVar.CommandId = srcReturn.commandId

destMidVar.TransactionId = srcReturn.transactionId

destMidVar.SequenceId = srcReturn.sequenceId

destMidVar.ResultCode = srcReturn.resultCode

destMidVar.ResultDesc = srcReturn.resultDesc

destMidVar.OrderId = srcReturn.orderId

destMidVar.OperationTime = srcReturn.operationTime

destMidVar.Language = srcReturn.msgLanguageCode
