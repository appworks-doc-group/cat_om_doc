def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.QueryContentSubscriptionResultMsg.ResultHeader

def srcReturnHeader = srcReturn.resultHeader

destMidVar.TransactionId = srcReturnHeader.transactionId

destMidVar.SequenceId = srcReturnHeader.sequenceId

destMidVar.ResultCode = srcReturnHeader.resultCode

destMidVar.ResultDesc = srcReturnHeader.resultDesc

destMidVar.Version = srcReturnHeader.version

destMidVar.CommandId = srcReturnHeader.commandId

def listMapping0

listMapping0 =
        {
            src, dest ->
			
                dest.AccountType = src.accountType

                dest.InitAmount = src.initAmount

                dest.Balance = src.balance

                dest.MinMeasureId = src.minMeasureId
				
				dest.BalanceDesc = src.balanceDesc

                dest.ContentID = src.contentID

                dest.CategoryID = src.categoryID
        }



def listMapping1

listMapping1 =
        {
            src,dest  ->

                dest.Id = src.code

                dest.Value = src.value
        }

def listMapping2

listMapping2 =
        {
            src,dest  ->
			
			    dest.Product.Id = src.product.id
				
				dest.Product.Name = src.product.name
				
				dest.Product.Desc = src.product.desc

                dest.ProductOrderKey = src.productOrderKey

                dest.ProductType = src.productType

                dest.EffectiveDate = src.effectiveDate

                dest.ExpireDate = src.expireDate

                dest.PurchasePrice = src.purchasePrice
				
				dest.UsagePrice = src.usagePrice
				
				dest.RenewalPrice = src.renewalPrice
				
                mappingList(src.balanceRecordList,dest.BalanceRecord,listMapping0)
				
				dest.NextBillDate = src.nextBillDate
				
                mappingList(src.simplePropertyList,dest.SimpleProperty,listMapping1)
				
				dest.Status = src.status
        }

def destMidVar1 = destReturn.QueryContentSubscriptionResultMsg.QueryContentSubscriptionResult

def srcMidVar2 = srcReturn.productsList

mappingList(srcMidVar2,destMidVar1.Products,listMapping2)

