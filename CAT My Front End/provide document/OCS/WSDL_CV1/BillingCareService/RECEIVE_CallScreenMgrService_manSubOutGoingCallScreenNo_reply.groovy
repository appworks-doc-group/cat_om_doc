def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.ManSubOutGoingCallScreenNoResultMsg.ResultHeader

destMidVar.CommandId = srcReturn.commandId

destMidVar.ResultCode = srcReturn.resultCode

destMidVar.ResultDesc = srcReturn.resultDesc

destMidVar.SequenceId = srcReturn.sequenceId

destMidVar.Version = srcReturn.version

destMidVar.TransactionId = srcReturn.transactionId
