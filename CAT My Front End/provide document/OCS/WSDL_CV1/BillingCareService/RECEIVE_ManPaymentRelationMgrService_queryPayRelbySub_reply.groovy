import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return
                            
def destMidVar = destReturn.QueryPayRelbySubResultMsg.ResultHeader

def srcHeader = srcReturn.resultHeader

destMidVar.CommandId = srcHeader.commandId

destMidVar.Version = srcHeader.version

destMidVar.TransactionId = srcHeader.transactionId

destMidVar.SequenceId = srcHeader.sequenceId

destMidVar.ResultCode = srcHeader.resultCode

destMidVar.ResultDesc = srcHeader.resultDesc

destMidVar.OrderId = srcHeader.orderId

destMidVar.OperationTime = srcHeader.operationTime

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.RecID = src.recId
	
	dest.TimeSchemaID = src.timeSchemaId
	
	dest.ServiceScene = src.serviceScene
	
	dest.Priority = src.priority
	
	dest.ConditionKey = src.conditionKey
	
	dest.ApplyTime = formatDate(src.applyTime,Constant4Model.DATE_FORMAT)
	
	dest.ExpireTime = formatDate(src.expireTime,Constant4Model.DATE_FORMAT)	
}


def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Quota = src.quota
	
	dest.NotifyType = src.notifyType
	
	dest.SendWorkOrder = src.sendWorkOrder
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.Accountcode = src.accountcode
	
	dest.PaymentRelKey = src.paymentRelKey
	
	dest.ShareSourceType = src.shareSourceType
	
	dest.AccountType = src.accountType
	
	dest.RuleType = src.ruleType
	
	dest.Quota = src.quota
	
	dest.UsedQuota = src.usedQuota
	
	dest.MeasureID = src.measureId
	
	dest.Permillage = src.permillage
	
	dest.ApplyTime = formatDate(src.applyTime,Constant4Model.DATE_FORMAT)
	
	dest.ExpireTime = formatDate(src.expireTime,Constant4Model.DATE_FORMAT)

	mappingList(src.paymentRelationSubs,dest.PaymentSubs,listMapping0)	
	
	mappingList(src.paymentRelationNotifyRule,dest.NotifyRule,listMapping1)
}

def srcBody = srcReturn.resultBody

def destBody = destReturn.QueryPayRelbySubResultMsg.QueryPayRelbySubResult

destBody.SubscriberNo = srcBody.subscriberNo

mappingList(srcBody.paymentRelationDetail,destBody.PaymentRelation,listMapping2)