import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("BMGroupService","changeGroupOffNetNumber")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.group.changegroupoffnetnumber.io.ChangeGroupOffNetNumberRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	def srcMidVar1 = src.EffectiveTime
	
	dest.effMode = srcMidVar1.Mode
	
	dest.effDate=parseDate(srcMidVar1.Time,Constant4Model.DATE_FORMAT)
	
	dest.expDate=parseDate(src.ExpirationTime,Constant4Model.DATE_FORMAT)
	
	dest.numberType = src.NumberType
	
	dest.offNetNumber = src.OffNetNumber
	
	dest.offNetNumberGroupId = src.OffNetNumberGroupID
	
	dest.offNetShortNumber = src.OffNetShortNumber
	
	dest.opType = src.OpType
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def destMidVar = destArgs1.groupAccessCode

def srcMidVar = srcArgs0.ChangeGroupOffNetNumberRequestMsg.ChangeGroupOffNetNumberRequest.SubGroupAccessCode

destMidVar.groupCode = srcMidVar.SubGroupCode

destMidVar.groupKey = srcMidVar.SubGroupKey

def srcMidVar0 = srcArgs0.ChangeGroupOffNetNumberRequestMsg.ChangeGroupOffNetNumberRequest

mappingList(srcMidVar0.GroupOffNetNumber,destArgs1.groupOffNetNumbers,listMapping0)

def srcMidVar2 = srcArgs0.ChangeGroupOffNetNumberRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar2.AccessMode

def srcMidVar3 = srcArgs0.ChangeGroupOffNetNumberRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar3.LoginSystemCode

destArgs0.password = srcMidVar3.Password

destArgs0.remoteAddress = srcMidVar3.RemoteIP

mappingList(srcMidVar2.AdditionalProperty,destArgs0.simpleProperty,listMapping1)

destArgs0.businessCode = srcMidVar2.BusinessCode

destArgs0.messageSeq = srcMidVar2.MessageSeq

destArgs0.msgLanguageCode = srcMidVar2.MsgLanguageCode

def srcMidVar4 = srcArgs0.ChangeGroupOffNetNumberRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar4.ChannelID

destArgs0.operatorId = srcMidVar4.OperatorID

def srcMidVar5 = srcArgs0.ChangeGroupOffNetNumberRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar5.BEID

destArgs0.brId = srcMidVar5.BRID

def srcMidVar6 = srcArgs0.ChangeGroupOffNetNumberRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar6.TimeType

destArgs0.timeZoneId = srcMidVar6.TimeZoneID

destArgs0.version = srcMidVar2.Version
