def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.query.queryofferingrentcycle.io.QueryOfferingRentCycleRequest"

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.accoutCode = src.AccountCode
	
	dest.accoutKey = src.AccountKey
	
	dest.payType = src.PayType
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	listMapping3.call(src.AcctAccessCode,dest.acctAccessCode)
	
	listMapping4.call(src.CustAccessCode,dest.custAccessCode)
	
	listMapping5.call(src.SubAccessCode,dest.subAccessCode)
	
	listMapping6.call(src.SubGroupAccessCode,dest.groupAccessCode)
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	listMapping1.call(src.OfferingKey,dest.offeringKeyInfo)
	
	listMapping2.call(src.OfferingOwner,dest.anyAccessCode)
	
}

def listMapping8

listMapping8 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.interMode = src.AccessMode
	
	def srcMidVar1 = src.AccessSecurity
	
	dest.loginSystem = srcMidVar1.LoginSystemCode
	
	dest.password = srcMidVar1.Password
	
	dest.remoteAddress = srcMidVar1.RemoteIP
	
	mappingList(src.AdditionalProperty,dest.simpleProperty,listMapping8)
	
	dest.businessCode = src.BusinessCode
	
	dest.messageSeq = src.MessageSeq
	
	dest.msgLanguageCode = src.MsgLanguageCode
	
	def srcMidVar2 = src.OperatorInfo
	
	dest.channelId = srcMidVar2.ChannelID
	
	dest.operatorId = srcMidVar2.OperatorID
	
	def srcMidVar3 = src.OwnershipInfo
	
	dest.beId = srcMidVar3.BEID
	
	dest.brId = srcMidVar3.BRID
	
	def srcMidVar4 = src.TimeFormat
	
	dest.timeType = srcMidVar4.TimeType
	
	dest.timeZoneId = srcMidVar4.TimeZoneID
	
	dest.version = src.Version
	
}

def listMapping9

listMapping9 = 
{
    src,dest  ->

	
}

def srcMidVar = srcArgs0.QueryOfferingRentCycleRequestMsg.QueryOfferingRentCycleRequest

mappingList(srcMidVar.OfferingInst,destArgs1.offeringRecentCycleDataExList,listMapping0)

def srcMidVar0 = srcArgs0.QueryOfferingRentCycleRequestMsg

listMapping7.call(srcMidVar0.RequestHeader,destArgs0)

listMapping9.call(srcMidVar0.QueryOfferingRentCycleRequest,destArgs1)
