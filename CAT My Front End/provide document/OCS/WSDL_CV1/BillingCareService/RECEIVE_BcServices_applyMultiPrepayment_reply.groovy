import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Amount = src.amount
	
	dest.CurrencyID = src.currencyID
	
	dest.CycleClass = src.cycleClass
	
	dest.CycleDueDate=formatDate(src.cycleDueDate, Constant4Model.DATE_FORMAT)
	
	dest.CycleSequence = src.cycleSequence
	
	dest.DelayFlag = src.delayFlag
	
	dest.InitialAmount = src.initialAmount
	
	dest.RealRepayDate=formatDate(src.realRepayDate, Constant4Model.DATE_FORMAT)
	
	dest.Status = src.status
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.Amount = src.amount
	
	dest.CurrencyID = src.currencyID
	
	dest.CycleClass = src.cycleClass
	
	dest.CycleDueDate=formatDate(src.cycleDueDate, Constant4Model.DATE_FORMAT)
	
	dest.CycleSequence = src.cycleSequence
	
	dest.DelayFlag = src.delayFlag
	
	dest.InitialAmount = src.initialAmount
	
	dest.Status = src.status
	
	dest.RealRepayDate=formatDate(src.realRepayDate, Constant4Model.DATE_FORMAT)
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.ContractID = src.contractID
	
	dest.CurrencyID = src.currencyID
	
	mappingList(src.inatallmentDetailList,dest.InatallmentDetail,listMapping3)
	
	dest.InstallmentInstID = src.installmentInstID
	
	def destMidVar2 = dest.OfferingKey
	
	def srcMidVar2 = src.offeringKey
	
	destMidVar2.OfferingID = srcMidVar2.oId
	
	destMidVar2.PurchaseSeq = srcMidVar2.pSeq
	
	dest.TotalAmount = src.totalAmount
	
	dest.TotalCycle = src.totalCycle
	
}

def destMidVar = destReturn.ApplyMultiPrepaymentResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.Version = srcMidVar.version

destMidVar.MessageSeq = srcMidVar.messageSeq

mappingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def destMidVar0 = destReturn.ApplyMultiPrepaymentResultMsg.ApplyMultiPrepaymentResult

def srcMidVar0 = srcReturn.applyPrepaymentInfo

destMidVar0.ContractID = srcMidVar0.contractID

destMidVar0.CurrencyID = srcMidVar0.currencyID

destMidVar0.InstallmentInstID = srcMidVar0.installmentInstID

destMidVar0.TotalAmount = srcMidVar0.totalAmount

destMidVar0.TotalCycle = srcMidVar0.totalCycle

mappingList(srcMidVar0.inatallmentDetailList,destMidVar0.InatallmentDetail,listMapping1)

def destMidVar1 = destReturn.ApplyMultiPrepaymentResultMsg.ApplyMultiPrepaymentResult.OfferingKey

def srcMidVar1 = srcReturn.applyPrepaymentInfo.offeringKey

destMidVar1.OfferingID = srcMidVar1.oId

destMidVar1.PurchaseSeq = srcMidVar1.pSeq

mappingList(srcReturn.applyPrepaymentInfoList,destMidVar0.ApplyPrepaymentDetail,listMapping2)
