def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.QueryShareRelationResultMsg.ResultHeader

def srcReturnHeader = srcReturn.resultHeader

destMidVar.CommandId = srcReturnHeader.commandId

destMidVar.ResultCode = srcReturnHeader.resultCode

destMidVar.ResultDesc = srcReturnHeader.resultDesc

destMidVar.SequenceId = srcReturnHeader.sequenceId

destMidVar.TransactionId = srcReturnHeader.transactionId

destMidVar.Version = srcReturnHeader.version

def srcReturnBody = srcReturn
def destReturnBody = destReturn.QueryShareRelationResultMsg.QueryShareRelationResult
srcReturnBody._class = "com.huawei.ngcbs.cm.ocs12ws.community.querysharerelation.io.QueryShareRelationResult"

def listOfferMapping = 
{
  src,dest  ->

	dest.shareRuleId = src.shareRuleId
	dest.ShareType = src.shareType
	dest.ServiceType = src.serviceType	
	dest.RuleType = src.ruleType
	dest.ShareValue = src.shareValue
	dest.UsedQuota = src.usedQuota
	dest.TimeSchemaID = src.timeSchemaID
	dest.Priority = src.priority
	dest.Remark = src.remark
	
}
mappingList(srcReturnBody.shareInfoList.shareInfoList, destReturnBody.ShareInfo, listOfferMapping)