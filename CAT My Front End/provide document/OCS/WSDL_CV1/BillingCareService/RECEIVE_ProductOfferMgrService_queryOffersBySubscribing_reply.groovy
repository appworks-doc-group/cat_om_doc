import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->
	
	dest.Id = src.id
	
	dest.Name = src.name
	
	dest.ReleasedBeginTime = formatDate(src.releasedBeginTime, Constant4Model.DATE_FORMAT)
	
	dest.ReleasedEndTime = formatDate(src.releasedEndTime, Constant4Model.DATE_FORMAT)
	
	dest.EffectiveDate = formatDate(src.effectiveDate, Constant4Model.DATE_FORMAT)
	
	dest.ExpireDate = formatDate(src.expireDate, Constant4Model.DATE_FORMAT)

	dest.Remark = src.remark

	dest.Version = src.version

	dest.OfferType = src.offerType

	dest.OfferLevel = src.offerLevel
	
}

def srcMidVar = srcReturn.resultHeader

def destMidVar = destReturn.QueryOffersBySubscribingResultMsg.ResultHeader

destMidVar.CommandId = srcMidVar.commandId

destMidVar.Version = srcMidVar.version

destMidVar.TransactionId = srcMidVar.transactionId

destMidVar.SequenceId = srcMidVar.sequenceId

destMidVar.TenantId = srcMidVar.beId

destMidVar.Language = srcMidVar.msgLanguageCode

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.OrderId = srcMidVar.orderId

destMidVar.OperationTime = srcMidVar.operationTime

def destMidVar0 = destReturn.QueryOffersBySubscribingResultMsg.QueryOffersBySubscribingResult

def srcMidVar0 = srcReturn.resultBody

mappingList(srcMidVar0.offerInfoList,destMidVar0.OfferInfo,listMapping0)