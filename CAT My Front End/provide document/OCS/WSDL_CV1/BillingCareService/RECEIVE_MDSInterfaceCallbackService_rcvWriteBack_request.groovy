dest.setServiceOperation("MDSInterfaceCallbackService","rcvWriteBack")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.common.provision.ws.io.ProvisionWriteBackRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	
}

def destMidVar = destArgs0.resultHeader

def srcMidVar = srcArgs0.RcvWriteBack.WriteBackMsg.RequestMessage.MessageHeader

destMidVar.sysPassword = srcMidVar.SysPassword

destMidVar.sysUser = srcMidVar.SysUser

def srcMidVar0 = srcArgs0.RcvWriteBack.WriteBackMsg.RequestMessage.MessageBody

destArgs0.time = srcMidVar0.Time

destArgs0.serialId = srcMidVar0.Serial

def destMidVar0 = destArgs0.retParaList.retParamList[0]

def srcMidVar1 = srcArgs0.RcvWriteBack.WriteBackMsg.RequestMessage.MessageBody.RetParaList.Para[0]

destMidVar0.paraName = srcMidVar1.Name

destMidVar0.paraValue = srcMidVar1.Value

def srcMidVar2 = srcArgs0.RcvWriteBack.WriteBackMsg.RequestMessage.MessageBody.RetParaList

def destMidVar1 = destArgs0.retParaList

mappingList(srcMidVar2.Para,destMidVar1.retParamList,listMapping0)

destArgs0.retDesc = srcMidVar0.RetDesc

destArgs0.retCode = srcMidVar0.RetCode

destArgs0.id = srcMidVar0.ID
