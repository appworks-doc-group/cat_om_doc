def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.resultHeader

def srcMidVar = srcReturn.BlacklistResetResultMsg.ResultHeader

destMidVar.resultCode = srcMidVar.ResultCode

destMidVar.resultDesc = srcMidVar.ResultDesc

destMidVar.version = srcMidVar.Version

destReturn._class = "com.huawei.ngcbs.cm.common.ws.client.io.uvc.BlacklistResetResult"

def srcMidResult = srcReturn.BlacklistResetResultMsg.BlacklistQueryResult

def destMidVar1 = destReturn

destMidVar1.resultCode = srcMidResult.ResultCode
