import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.QuerySubCallScreenNoResultMsg.ResultHeader
def srcHeader = srcReturn.resultHeader

destMidVar.CommandId = srcHeader.commandId
destMidVar.ResultCode = srcHeader.resultCode
destMidVar.ResultDesc = srcHeader.resultDesc
destMidVar.SequenceId = srcHeader.sequenceId
destMidVar.Version = srcHeader.version
destMidVar.TransactionId = srcHeader.transactionId
destMidVar.OperationTime = srcHeader.operationTime
destMidVar.OrderId = srcHeader.orderId

def srcBusinessData = srcReturn.resultBody
srcBusinessData._class = "com.huawei.ngcbs.cm.ocs33ws.subcallscreenno.querysubcallscreenno.io.OCS33QuerySubCallScreenNoResult"
def destMidVar0 = destReturn.QuerySubCallScreenNoResultMsg.QuerySubCallScreenNoResult

destMidVar0.CallScreenType = srcBusinessData.callScreenType

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.callScreenNo = src.callScreenNo
	
	dest.routeNumber = src.routeNumber
	
	dest.weekStart = src.weekStart
	
	dest.weekStop = src.weekStop
	
	dest.effectiveDate=formatDate(src.effectiveDate, Constant4Model.DATE_FORMAT)
	
	dest.expireDate=formatDate(src.expireDate, Constant4Model.DATE_FORMAT)
	
	dest.IRRouteFlag = src.irRouteFlag
	
	dest.RoutingMethod = src.routingMethod
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.routeNumber = src.routeNumber
	
	dest.weekStart = src.weekStart
	
	dest.weekStop = src.weekStop
	
	dest.expireDate=formatDate(src.expireDate, Constant4Model.DATE_FORMAT)
	
	dest.effectiveDate=formatDate(src.effectiveDate, Constant4Model.DATE_FORMAT)
	
	dest.IRRouteFlag = src.irRouteFlag
	
	dest.RoutingMethod = src.routingMethod
	
}

mappingList(srcBusinessData.callScreenNoInfos,destMidVar0.CallScreenNoInfo,listMapping0)

mappingList(srcBusinessData.nonCallScreenNoInfos,destMidVar0.NonCallScreenNoInfo,listMapping1)
