dest.setServiceOperation("BMGroupService","deleteSuperGroupMember")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.group.deletesupergroupmember.io.DeleteSuperGroupMemberRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	def destMidVar0 = dest.subGroupAccessCode
	
	def srcMidVar6 = src.SubGroupAccessCode
	
	destMidVar0.groupCode = srcMidVar6.SubGroupCode
	
	destMidVar0.groupKey = srcMidVar6.SubGroupKey
	
}

def srcMidVar = srcArgs0.DeleteSuperGroupMemberRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.DeleteSuperGroupMemberRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.DeleteSuperGroupMemberRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.DeleteSuperGroupMemberRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.DeleteSuperGroupMemberRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar.Version

def destMidVar = destArgs1.superGroup

def srcMidVar4 = srcArgs0.DeleteSuperGroupMemberRequestMsg.DeleteSuperGroupMemberRequest.SuperGroupAccess

destMidVar.superGrpCode = srcMidVar4.SuperGroupCode

destMidVar.superGrpKey = srcMidVar4.SuperGroupKey

def srcMidVar5 = srcArgs0.DeleteSuperGroupMemberRequestMsg.DeleteSuperGroupMemberRequest

mappingList(srcMidVar5.members,destArgs1.members,listMapping1)
