import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destReturn.ChangeAcctBillFrequencyResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.Version = srcMidVar.version

destMidVar.MessageSeq = srcMidVar.messageSeq

mappingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def destMidVar0 = destReturn.ChangeAcctBillFrequencyResultMsg.ChangeAcctBillFrequencyResult

def srcMidVar0 = srcReturn.resultBody

destMidVar0.AcctCycleBeginDate = formatDate(srcMidVar0.acctCycleBeginDate, Constant4Model.DATE_FORMAT)

destMidVar0.BillFrequency = srcMidVar0.billFrequency




