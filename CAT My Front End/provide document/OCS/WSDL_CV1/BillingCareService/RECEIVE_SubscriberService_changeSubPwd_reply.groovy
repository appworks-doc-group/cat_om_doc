import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.AcctKey = src.acctKey
	
	def destMidVar1 = dest.BalanceChgInfo[0]
	
	def srcMidVar1 = src.balanceChgInfoList[0]
	
	destMidVar1.BalanceID = srcMidVar1.balanceId
	
	destMidVar1.BalanceType = srcMidVar1.balanceType
	
	destMidVar1.BalanceTypeName = srcMidVar1.balanceTypeName
	
	destMidVar1.CurrencyID = srcMidVar1.currencyId
	
	destMidVar1.NewBalanceAmt = srcMidVar1.newBalanceAmt
	
	destMidVar1.OldBalanceAmt = srcMidVar1.oldBalanceAmt
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.AccmBeginDate=formatDate(src.accmBeginDate, Constant4Model.DATE_FORMAT)
	
	dest.AccmEndDate=formatDate(src.accmEndDate, Constant4Model.DATE_FORMAT)
	
	dest.AcctKey = src.acctKey
	
	dest.CreditInstID = src.creditInstId
	
	dest.CreditLimitType = src.creditLimitType
	
	dest.CurrencyID = src.currencyId
	
	dest.CurrentAmt = src.currentAmt
	
	dest.OriginLimitAmt = src.originLimitAmt
	
	dest.PaidAmt = src.paidAmt
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.FreeUnitInstanceID = src.freeUnitInstanceId
	
	dest.FreeUnitType = src.freeUnitType
	
	dest.FreeUnitTypeName = src.freeUnitTypeName
	
	dest.MeasureUnit = src.measureUnit
	
	dest.MeasureUnitName = src.measureUnitName
	
	dest.NewAmt = src.newAmt
	
	dest.OldAmt = src.oldAmt
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	mappingList(src.freeUnitChgInfoList,dest.FreeUnitChgInfo,listMapping4)
	
	dest.OwnerKey = src.ownerKey
	
	dest.OwnerType = src.ownerType
	
}

def destMidVar = destReturn.ChangeSubPwdResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.Version = srcMidVar.version

mappingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def srcMidVar0 = srcReturn.feeDeductionResultInfo

def destMidVar0 = destReturn.ChangeSubPwdResultMsg.ChangeSubPwdResult

mappingList(srcMidVar0.acctBalanceChangeInfoList,destMidVar0.AcctBalanceChangeList,listMapping1)

mappingList(srcMidVar0.creditLmtChangeList,destMidVar0.CreditLimitChangeList,listMapping2)

mappingList(srcMidVar0.freeUnitChangeList,destMidVar0.FreeUnitChangeList,listMapping3)
