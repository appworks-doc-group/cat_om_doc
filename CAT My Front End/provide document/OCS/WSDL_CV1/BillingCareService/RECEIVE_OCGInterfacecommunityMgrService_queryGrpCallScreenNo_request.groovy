def bmQueryGrpCallScreenNoRequest = src.payload._args[0]
def omQueryGrpCallScreenNoDoc = dest.payload._args[0]

def bmRequestHeader = bmQueryGrpCallScreenNoRequest.omRequestHeader
def omRequestHeaderDoc = omQueryGrpCallScreenNoDoc.QueryGrpCallScreenNoRequestMsg.RequestHeader

omRequestHeaderDoc.CommandId = bmRequestHeader.commandId
omRequestHeaderDoc.Version = bmRequestHeader.version
omRequestHeaderDoc.TransactionId = bmRequestHeader.transactionId
omRequestHeaderDoc.SequenceId = bmRequestHeader.sequenceId
omRequestHeaderDoc.RequestType = bmRequestHeader.requestType

def bmSessionEntity = bmRequestHeader.sessionEntity
def omSessionEntity = omRequestHeaderDoc.SessionEntity

omSessionEntity.Name = bmSessionEntity.name
omSessionEntity.Password = bmSessionEntity.password
omSessionEntity.RemoteAddress = bmSessionEntity.remoteAddress

omRequestHeaderDoc.InterFrom = bmRequestHeader.interFrom
omRequestHeaderDoc.InterMode = bmRequestHeader.interMode
omRequestHeaderDoc.InterMedi = bmRequestHeader.interMedi
omRequestHeaderDoc.visitArea = bmRequestHeader.visitArea
omRequestHeaderDoc.currentCell = bmRequestHeader.currentCell
omRequestHeaderDoc.ThirdPartyID = bmRequestHeader.thirdPartyId
omRequestHeaderDoc.PartnerID = bmRequestHeader.partnerId
omRequestHeaderDoc.OperatorID = bmRequestHeader.operatorId
omRequestHeaderDoc.TradePartnerID = bmRequestHeader.tradePartnerId
omRequestHeaderDoc.PartnerOperID = bmRequestHeader.partnerOperId
omRequestHeaderDoc.BelToAreaID = bmRequestHeader.belToAreaId
omRequestHeaderDoc.SerialNo = bmRequestHeader.serialNo
omRequestHeaderDoc.Remark = bmRequestHeader.remark
omRequestHeaderDoc.TenantID = bmRequestHeader.tenantId

def omRequestBodyDoc = omQueryGrpCallScreenNoDoc.QueryGrpCallScreenNoRequestMsg.QueryGrpCallScreenNoRequest

omRequestBodyDoc.GroupNumber = bmQueryGrpCallScreenNoRequest.groupNumber
omRequestBodyDoc.CallScreenType = bmQueryGrpCallScreenNoRequest.callScreenType
