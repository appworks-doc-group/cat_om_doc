def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.ManageProviderResultMsg.ResultHeader

def srcReturnHeader = srcReturn.resultHeader

destMidVar.TransactionId = srcReturnHeader.transactionId

destMidVar.SequenceId = srcReturnHeader.sequenceId

destMidVar.ResultCode = srcReturnHeader.resultCode

destMidVar.ResultDesc = srcReturnHeader.resultDesc

destMidVar.Version = srcReturnHeader.version

destMidVar.CommandId = srcReturnHeader.commandId

def listMapping0

listMapping0 =
        {
            src, dest ->
                dest.Id = src.serviceType
                dest.Name = src.serviceName
        }



def listMapping1

listMapping1 =
        {
            src,dest  ->

                dest.ProviderID = src.providerID

                dest.ProviderName = src.providerName

                dest.ProviderDesc = src.providerDesc

                dest.Version = src.version

                dest.LastUpdOperatorID = src.lastUpdOperId

                dest.ProviderCompany = src.providerCompany

        }

def listMapping2

listMapping2 =
        {
            src,dest  ->

                listMapping1.call(src.contentProviderVO,dest)

                mappingList(src.contentService,dest.Service,listMapping0)

        }

def destMidVar1 = destReturn.ManageProviderResultMsg.ManageProviderResult

def srcMidVar2 = srcReturn.contentProviderDetailInfo

mappingList(srcMidVar2.contentProviderDetailList,destMidVar1.Provider,listMapping2)

