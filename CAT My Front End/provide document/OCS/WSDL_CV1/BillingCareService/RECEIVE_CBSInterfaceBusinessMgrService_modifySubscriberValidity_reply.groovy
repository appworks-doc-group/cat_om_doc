def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.ModifySubscriberValidityResultMsg.ModifySubscriberValidityResult

destMidVar.ActiveStop = srcReturn.activeStop

destMidVar.DisableStop = srcReturn.disableStop

destMidVar.LifeCycleState = srcReturn.lifeCycleState

destMidVar.SuspendStop = srcReturn.suspendStop

def destMidVar0 = destReturn.ModifySubscriberValidityResultMsg.ResultHeader

destMidVar0.CommandId = srcReturn.commandId

destMidVar0.TransactionId = srcReturn.transactionId

destMidVar0.SequenceId = srcReturn.sequenceId

destMidVar0.ResultCode = srcReturn.resultCode

destMidVar0.ResultDesc = srcReturn.resultDesc

def srcMidVar = srcReturn.resultHeader

destMidVar0.CommandId = srcMidVar.commandId

destMidVar0.TransactionId = srcMidVar.transactionId

destMidVar0.SequenceId = srcMidVar.sequenceId

destMidVar0.ResultCode = srcMidVar.resultCode

destMidVar0.ResultDesc = srcMidVar.resultDesc

destMidVar0.Version = srcMidVar.version

def srcMidVar0 = srcReturn.resultBody

destMidVar.ActiveStop = srcMidVar0.activeStop

destMidVar.DisableStop = srcMidVar0.disableStop

destMidVar.LifeCycleState = srcMidVar0.lifeCycleState

destMidVar.SuspendStop = srcMidVar0.suspendStop
