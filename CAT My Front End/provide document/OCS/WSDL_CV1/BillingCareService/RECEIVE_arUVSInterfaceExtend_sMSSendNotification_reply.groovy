def srcReturn = src.payload._return   

def destReturn = dest.payload._return    

def srcResultHeader = srcReturn.ocs11ResultHeader

def srcResultBody = srcReturn.resultBody

def destMidVar = destReturn.SMSSendNotificationResponse.SMSSendNotificationResult.ResultMessage.MessageHeader

def destMessageBody = destReturn.SMSSendNotificationResponse.SMSSendNotificationResult.ResultMessage.MessageBody

destMidVar.CommandId = srcResultHeader.commandId
destMidVar.Version = srcResultHeader.version
destMidVar.TransactionId = srcResultHeader.transactionId
destMidVar.SequenceId = srcResultHeader.sequenceId
destMidVar.ResultCode = srcResultHeader.resultCode
destMidVar.ResultDesc = srcResultHeader.resultDesc
destMidVar.TenantId = srcResultHeader.tenantId
destMidVar.Language = srcResultHeader.language