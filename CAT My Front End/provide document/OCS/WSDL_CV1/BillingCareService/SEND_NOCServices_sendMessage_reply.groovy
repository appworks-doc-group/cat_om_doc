def srcReturn = src.payload._return
def destReturn = dest.payload._return

destReturn.status = srcReturn.SendMessageResponse.status
destReturn.transactionId = srcReturn.SendMessageResponse.transactionId
destReturn.overallErrorCode = srcReturn.SendMessageResponse.overallErrorCode
destReturn.overallErrorDescription = srcReturn.SendMessageResponse.overallErrorDescription

def listMapping0
listMapping0 =
{
    src, dest ->
        dest.msisdn = src.msisdn
        dest.email = src.email
        dest.channel = src.channel
        dest.text = src.text
        dest.emailSubject = src.emailSubject
        dest.title = src.title
        dest.errorCode = src.errorCode
        dest.errorDescription = src.errorDescription

        dest.curfewDetail.curfewBehavior = src.curfewDetail.curfewBehavior
        dest.curfewDetail.curfewAction = src.curfewDetail.curfewAction
        dest.curfewDetail.deferredTill = src.curfewDetail.deferredTill
}
mappingList(srcReturn.SendMessageResponse.details,destReturn.details,listMapping0)

destReturn._class = "com.huawei.ngcbs.cm.common.ws.client.io.noc.SendMessageResponse"
