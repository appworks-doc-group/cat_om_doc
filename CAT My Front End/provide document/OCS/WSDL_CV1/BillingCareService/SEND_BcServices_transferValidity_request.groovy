dest.setServiceOperation("SubscriberService","transferValidity")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.transfervalidity.io.TransferValidityRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.TransferValidityRequestMsg.TransferValidityRequest

def srcMidVar_transferor = srcMidVar.TransferorObj

def srcMidVar_transferee = srcMidVar.TransfereeObj

listMapping0.call(srcMidVar_transferor.SubAccessCode,destArgs1.transferorObj)

listMapping2.call(srcMidVar_transferee.SubAccessCode,destArgs1.transfereeObj)

destArgs1.transferDays = srcMidVar.TransferDays

mappingList(srcMidVar.AdditionalProperty,destArgs1.simplePropertyList,listMapping3)

def srcMidVar0 = srcArgs0.TransferValidityRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

def srcMidVar1 = srcArgs0.TransferValidityRequestMsg.RequestHeader

mappingList(srcMidVar1.AdditionalProperty,destArgs0.simpleProperty,listMapping1)

destArgs0.businessCode = srcMidVar1.BusinessCode

destArgs0.messageSeq = srcMidVar1.MessageSeq

destArgs0.msgLanguageCode = srcMidVar1.MsgLanguageCode

def srcMidVar2 = srcArgs0.TransferValidityRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar2.ChannelID

destArgs0.operatorId = srcMidVar2.OperatorID

def srcMidVar3 = srcArgs0.TransferValidityRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar3.BEID

destArgs0.brId = srcMidVar3.BRID

def srcMidVar4 = srcArgs0.TransferValidityRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar4.TimeType

destArgs0.timeZoneId = srcMidVar4.TimeZoneID

destArgs0.version = srcMidVar1.Version

destArgs1.handlingChargeFlag = srcMidVar.HandlingChargeFlag

def destMidVar = destArgs1.changeSubValidityInfo

destMidVar.opType = srcMidVar.OpType

destMidVar.validityIncrememt = srcMidVar.ValidityIncrement

destArgs0.interMode = srcMidVar1.AccessMode
