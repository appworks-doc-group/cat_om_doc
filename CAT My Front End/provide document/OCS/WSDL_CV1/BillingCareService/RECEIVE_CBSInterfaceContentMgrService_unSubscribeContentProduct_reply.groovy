def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.UnSubscribeContentProductResultMsg.ResultHeader

def srcReturnHeader = srcReturn.resultHeader

destMidVar.TransactionId = srcReturnHeader.transactionId

destMidVar.SequenceId = srcReturnHeader.sequenceId

destMidVar.ResultCode = srcReturnHeader.resultCode

destMidVar.ResultDesc = srcReturnHeader.resultDesc

destMidVar.Version = srcReturnHeader.version

destMidVar.CommandId = srcReturnHeader.commandId

def destMidVar1 = destReturn.UnSubscribeContentProductResultMsg.UnSubscribeContentProductResult

destMidVar1.SubscriberNo = srcReturn.subId
