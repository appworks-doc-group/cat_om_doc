dest.setServiceOperation("CBSInterfaceBusinessMgrService","modifySubscriberState");

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def srcMidVar = srcArgs0.ModifySubscriberStateRequestMsg.RequestHeader

destArgs0.beId = srcMidVar.TenantId

destArgs0.commandId = srcMidVar.CommandId

destArgs0.interMedi = srcMidVar.InterMedi

destArgs0.additionInfo = srcMidVar.additionInfo

destArgs0.operatorId = srcMidVar.OperatorID

destArgs0.transactionId = srcMidVar.TransactionId

destArgs0.reserve2 = srcMidVar.Reserve2

destArgs0.thirdPartyId = srcMidVar.ThirdPartyID

destArgs0.reserve3 = srcMidVar.Reserve3

destArgs0.interMode = srcMidVar.InterMode

destArgs0.sequenceId = srcMidVar.SequenceId

destArgs0.beId = srcMidVar.TenantId

destArgs0.msgLanguageCode = srcMidVar.Language

destArgs0.visitArea = srcMidVar.visitArea

destArgs0.messageSeq = srcMidVar.SerialNo

destArgs0.belToAreaId = srcMidVar.BelToAreaID

destArgs0.currentCell = srcMidVar.currentCell

destArgs0.partnerId = srcMidVar.PartnerID

destArgs0.remark = srcMidVar.Remark

destArgs0.tradePartnerId = srcMidVar.TradePartnerID

destArgs0.version = srcMidVar.Version

destArgs0.partnerOperId = srcMidVar.PartnerOperID

destArgs0.requestType = srcMidVar.RequestType

def srcMidVar0 = srcArgs0.ModifySubscriberStateRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar0.Name

destArgs0.password = srcMidVar0.Password

destArgs0.interFrom = srcMidVar.InterFrom

def destArgs1 = dest.payload._args[1]

def srcMidVar1 = srcArgs0.ModifySubscriberStateRequestMsg.ModifySubscriberStateRequest

def destMidVar = destArgs1.subAccessCode

destMidVar.primaryIdentity = srcMidVar1.SubscriberNo

def destMidVar1 = destArgs1.acctAccessCode

destMidVar1.primaryIdentity = srcMidVar1.SubscriberNo

destArgs1.operateType = srcMidVar1.OperateType

destArgs1.suspendDays = srcMidVar1.SuspendDays

destArgs1.handlingChargeFlag = srcMidVar1.HandlingChargeFlag






















