import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs33ws.core.bo.Ocs33MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs33ws.subscriber.newsubscriber.io.OCS33NewSubscriberRequest"

def srcMidVar = srcArgs0.NewSubscriberRequestMsg.RequestHeader

destArgs0.performanceStatCmd = "NewSubscriber"

destArgs0.commandId = srcMidVar.CommandId

destArgs0.currentCell = srcMidVar.currentCell

destArgs0.interFrom = srcMidVar.InterFrom

destArgs0.interMedi = srcMidVar.InterMedi

destArgs0.interMode = srcMidVar.InterMode

destArgs0.operatorId = srcMidVar.OperatorID

destArgs0.partnerId = srcMidVar.PartnerID

destArgs0.partnerOperId = srcMidVar.PartnerOperID

destArgs0.remark = srcMidVar.Remark

destArgs0.requestType = srcMidVar.RequestType

destArgs0.reserve2 = srcMidVar.Reserve2

destArgs0.reserve3 = srcMidVar.Reserve3

destArgs0.sequenceId = srcMidVar.SequenceId

destArgs0.messageSeq = srcMidVar.SerialNo

def srcMidVar0 = srcArgs0.NewSubscriberRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar0.Name

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteAddress

destArgs0.thirdPartyId = srcMidVar.ThirdPartyID

destArgs0.tradePartnerId = srcMidVar.TradePartnerID

destArgs0.version = srcMidVar.Version

destArgs0.transactionId = srcMidVar.TransactionId

destArgs0.visitArea = srcMidVar.visitArea

def listMapping0
listMapping0 = 
{
    src,dest  ->

	def destMidVar = dest.accountInfo
	
	dest.billCycleType = src.BillCycleType
	
	destMidVar.acctKey = src.AccountCode
	
	destMidVar.acctCode = src.AccountID
	
	destMidVar.paymentType = src.PaidMode
	
	dest.isCustShareAcct = src.IsCustShareAcct
	
	dest.operType = src.OperType
	
	dest.ppsAcctInitBal = src.PPSAcctInitBal
	
	dest.ppsAcctCredit = src.PPSAcctCredit
	
	dest.posAcctInitBal = src.POSAcctInitBal
	
	dest.posAcctCredit = src.POSAcctCredit
}

def listMappingProdProps
listMappingProdProps =
{
	src,dest  ->
	dest.id = src.Id
	dest.value = src.Value
}

def srcMidVar1 = srcArgs0.NewSubscriberRequestMsg.NewSubscriberRequest.Customer
def destMidVar1 = destArgs1.regCustomerInfo.customerInfo
destMidVar1.tpCustKey = srcMidVar1.CustomerCode
destMidVar1.custCode=srcMidVar1.CustomerID
destArgs1.type = srcMidVar1.OperType

def srcMidVar2 = srcArgs0.NewSubscriberRequestMsg.NewSubscriberRequest
mappingList(srcMidVar2.Account,destArgs1.createAccountInfos,listMapping0)

def srcMidVar3 = srcArgs0.NewSubscriberRequestMsg.NewSubscriberRequest.PrimaryOffer
destArgs1.primaryOfferId = srcMidVar3.Id

def listMapping2
listMapping2 = 
{
    src,dest  ->
	dest.id = src.Id
	dest.value = src.Value
}

def listMapping1
listMapping1 = 
{
    src,dest  ->
    
	dest.serviceNumber = src.SubscriberNo
	dest.subscriberCode = src.SubscriberCode
	dest.accountCode = src.AccountCode
	dest.mainProdId = src.MainProductId
	dest.imsi = src.IMSI
	mappingList(src.SimpleProperty, dest.subPros, listMapping2)
}
mappingList(srcMidVar3.Subscriber,destArgs1.subscribers,listMapping1)

def listMapping3
listMapping3 = 
{
	src,dest  ->
	dest.offerId = src.Id
	mappingList(src.SimpleProperty, dest.prodOrderProps, listMappingProdProps)
}

mappingList(srcMidVar2.OptionalOffer,destArgs1.optionalOffers,listMapping3)

def srcMidVar4 = srcArgs0.NewSubscriberRequestMsg.NewSubscriberRequest
destArgs1.oldSubscriberNo = srcMidVar4.SubscriberNo

