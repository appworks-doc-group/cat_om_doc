def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}


def listMapping2
listMapping2 = 
{
    src,dest  ->

	dest.LevelLeft = src.levelLeft
	
	dest.LevelRight = src.levelRight
	
	dest.SequenceNo = src.sequenceNo
	
	dest.ActionRuleID = src.actionRuleId
}

def listMapping3
listMapping3 = 
{
    src,dest  ->

	dest.ObjectType = src.objectType
	
	dest.AccountObject = src.accountObject
	
	dest.NotifyMode = src.notifyMode
	
	dest.NotifyGateProcessMode = src.notifyGateProcessMode
	
	mappingList(src.levelList,dest.LevelList,listMapping2)
	
}

def srcMidVar0 = srcReturn.resultHeader

def destMidVar0 = destReturn.QueryAccountNotificationLevelResultMsg.ResultHeader

mappingList(srcMidVar0.simpleProperty,destMidVar0.AdditionalProperty,listMapping0)

destMidVar0.ResultDesc = srcMidVar0.resultDesc

destMidVar0.ResultCode = srcMidVar0.resultCode

destMidVar0.MsgLanguageCode = srcMidVar0.msgLanguageCode

destMidVar0.Version = srcMidVar0.version

destMidVar0.MessageSeq = srcMidVar0.messageSeq

def destMidVar1 = destReturn.QueryAccountNotificationLevelResultMsg.QueryAccountNotificationLevelResult

def srcMidVar1 = srcReturn.queryAccountNotifyLevelResultInfo

destMidVar1.SubAccessCode.PrimaryIdentity = srcMidVar1.subAccessCode.primaryIdentity

destMidVar1.SubAccessCode.SubscriberKey = srcMidVar1.subAccessCode.subscriberKey

destMidVar1.SubGroupAccessCode.SubGroupKey = srcMidVar1.groupAccessCode.groupKey

destMidVar1.SubGroupAccessCode.SubGroupCode = srcMidVar1.groupAccessCode.groupCode

mappingList(srcMidVar1.accountLevelInfos,destMidVar1.AccountLevelInfo,listMapping3)








