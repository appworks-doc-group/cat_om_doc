import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

destReturn.QuerySubInforToMicroResultMsg.QuerySubInforToMicroResult.Subscriber.EffectiveDate=formatDate(srcReturn.subInforToMicroResult.firstActivationTime, Constant4Model.DATE_FORMAT)

def destMidVar = destReturn.QuerySubInforToMicroResultMsg.QuerySubInforToMicroResult.Subscriber

def srcMidVar = srcReturn.subInforToMicroResult

destMidVar.Language = srcMidVar.language

destMidVar.SubType = srcMidVar.paymentMode

destMidVar.SubStatus = srcMidVar.subStatus

destMidVar.ManagementStatus = srcMidVar.managementStatus

destMidVar.BlacklistStatus = srcMidVar.blacklistStatus

def destMidVar0 = destReturn.QuerySubInforToMicroResultMsg.ResultHeader

def srcMidVar0 = srcReturn.resultHeader

destMidVar0.MsgLanguageCode = srcMidVar0.msgLanguageCode

destMidVar0.ResultCode = srcMidVar0.resultCode

destMidVar0.ResultDesc = srcMidVar0.resultDesc

mappingList(srcMidVar0.simpleProperty,destMidVar0.AdditionalProperty,listMapping0)

destMidVar0.Version = srcMidVar0.version

destMidVar0.MessageSeq = srcMidVar0.messageSeq
