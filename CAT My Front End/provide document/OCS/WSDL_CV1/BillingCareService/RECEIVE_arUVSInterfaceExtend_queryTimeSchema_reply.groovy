def srcReturn = src.payload._return   

def destReturn = dest.payload._return    

def srcResultHeader = srcReturn.ocs11ResultHeader

def destMidVar = destReturn.QueryTimeSchemaResponse.QueryTimeSchemaResult.ResultMessage.MessageHeader

def destMessageBody = destReturn.QueryTimeSchemaResponse.QueryTimeSchemaResult.ResultMessage.MessageBody

destMidVar.CommandId = srcResultHeader.commandId
destMidVar.Version = srcResultHeader.version
destMidVar.TransactionId = srcResultHeader.transactionId
destMidVar.SequenceId = srcResultHeader.sequenceId
destMidVar.ResultCode = srcResultHeader.resultCode
destMidVar.ResultDesc = srcResultHeader.resultDesc
destMidVar.TenantId = srcResultHeader.tenantId
destMidVar.Language = srcResultHeader.language


def listMapping0

listMapping0 =
{
    src,dest  ->

    dest.TimeSchemaKey = src.timeSchemaKey
    
    dest.TimeSchemaName = src.timeSchemaName
}

mappingList(srcReturn.queryTimeSchemaResultInfo.timeSchemaRecords,destMessageBody.TimeSchemaList.TimeSchemaRecord,listMapping0)

