def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping2

listMapping2 =
        {
            src, dest ->

                dest.Code = src.code

                dest.Value = src.value

        }


def listMapping3

listMapping3 =
        {
            src, dest ->


                def srcMidVar0 = src.offeringKey

                srcMidVar0._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"

                def destMidVar1 = dest.OfferingKey

                destMidVar1.OfferingCode = srcMidVar0.oCode

                destMidVar1.OfferingID = srcMidVar0.oId

                destMidVar1.PurchaseSeq = srcMidVar0.pSeq

                dest.RentDeductionStatus=src.rentDeductionStatus


        }



def destMidVar = destReturn.EarlyRenewResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.Version = srcMidVar.version

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.MessageSeq = srcMidVar.messageSeq

//convert simpleProperty
mappingList(srcMidVar.simpleProperty, destMidVar.AdditionalProperty, listMapping2)


def destMidVar0 = destReturn.EarlyRenewResultMsg.EarlyRenewResult

mappingList(srcReturn.earlyRenewResultInfo.earlyRenewOffering, destMidVar0.EarlyRenewOffering, listMapping3)

