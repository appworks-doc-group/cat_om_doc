import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.offering.batch.changeofferingproperty.io.BatchChangeOfferingPropertyRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.code = src.SubPropCode
	
	dest.value = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	def srcMidVar6 = src.EffectiveTime
	
	dest.effDate =  parseDate(srcMidVar6.Time,Constant4Model.DATE_FORMAT)
	
	dest.effMode = srcMidVar6.Mode 
	
	dest.expDate =  parseDate(src.ExpirationTime,Constant4Model.DATE_FORMAT)
	
	def destMidVar2 = dest.property
	
	destMidVar2.propCode = src.PropCode
	
	destMidVar2.complexFlag = src.PropType
	
	mappingList(src.SubPropInst,destMidVar2.subProps,listMapping2)
	
	destMidVar2.value = src.Value
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.code = src.SubPropCode
	
	dest.value = src.Value
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	def srcMidVar7 = src.ExpirationTime
	
	dest.expDate = parseDate(srcMidVar7.Time,Constant4Model.DATE_FORMAT)
	
	dest.expMode = srcMidVar7.Mode
	
	def destMidVar3 = dest.instProperty
	
	destMidVar3.propCode = src.PropCode
	
	destMidVar3.complexFlag = src.PropType
	
	mappingList(src.SubPropInst,destMidVar3.subProps,listMapping4)
	
	destMidVar3.value = src.Value
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.code = src.SubPropCode
	
	dest.value = src.Value
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	def srcMidVar9 = src.EffectiveTime
	
	dest.effDate = parseDate(srcMidVar9.Time,Constant4Model.DATE_FORMAT)
	
	dest.effMode = srcMidVar9.Mode
	
	dest.expDate = parseDate(src.ExpirationTime,Constant4Model.DATE_FORMAT)
	
	def destMidVar5 = dest.property
	
	destMidVar5.propCode = src.PropCode
	
	destMidVar5.complexFlag = src.PropType
	
	mappingList(src.SubPropInst,destMidVar5.subProps,listMapping6)
	
	destMidVar5.value = src.Value
	
}

def listMapping8

listMapping8 = 
{
    src,dest  ->

	dest.code = src.SubPropCode
	
	dest.value = src.Value
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	def srcMidVar10 = src.ExpirationTime
	
	dest.expDate = parseDate(srcMidVar10.Time,Constant4Model.DATE_FORMAT)
	
	dest.expMode = srcMidVar10.Mode
	
	def destMidVar6 = dest.instProperty
	
	destMidVar6.propCode = src.PropCode
	
	destMidVar6.complexFlag = src.PropType
	
	mappingList(src.SubPropInst,destMidVar6.subProps,listMapping8)
	
	destMidVar6.value = src.Value
	
}

def srcMidVar = srcArgs0.BatchChangeOfferingPropertyRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.BatchChangeOfferingPropertyRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.BatchChangeOfferingPropertyRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.BatchChangeOfferingPropertyRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.BatchChangeOfferingPropertyRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar.Version

destArgs1.requestFileName=srcArgs0.BatchChangeOfferingPropertyRequestMsg.BatchChangeOfferingPropertyRequest.FileName

def destMidVar = destArgs1.changeOfferingPropertyInfo.offeringKey

def srcMidVar4 = srcArgs0.BatchChangeOfferingPropertyRequestMsg.BatchChangeOfferingPropertyRequest.OfferingKey

destMidVar.oId = srcMidVar4.OfferingID

destMidVar.pSeq = srcMidVar4.PurchaseSeq

def destMidVar0 = destArgs1.changeProductPropertyInfo.offeringKey

destMidVar0.oId = srcMidVar4.OfferingID

destMidVar0.pSeq = srcMidVar4.PurchaseSeq

def srcMidVar5 = srcArgs0.BatchChangeOfferingPropertyRequestMsg.BatchChangeOfferingPropertyRequest.OfferingInstProperty

def destMidVar1 = destArgs1.changeOfferingPropertyInfo

mappingList(srcMidVar5.AddProperty,destMidVar1.addOfferingInstProperties,listMapping1)

mappingList(srcMidVar5.DelProperty,destMidVar1.delOfferingInstProperties,listMapping3)

def srcMidVar8 = srcArgs0.BatchChangeOfferingPropertyRequestMsg.BatchChangeOfferingPropertyRequest.ProductInst

def destMidVar4 = destArgs1.changeProductPropertyInfo

mappingList(srcMidVar8.AddProperty,destMidVar4.addProperties,listMapping5)

mappingList(srcMidVar8.DelProperty,destMidVar4.delProperties,listMapping7)

destMidVar4.productId = srcMidVar8.ProductID
