dest.setServiceOperation("OCS33ChangeOwnerMgrService","changeOwner")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destArgs1 = dest.payload._args[1]

destArgs0._class = "com.huawei.ngcbs.cm.ocs33ws.core.bo.Ocs33MessageHeader"

destArgs1._class = "com.huawei.ngcbs.cm.ocs33ws.changeowner.io.OCS33ChangeOwnerRequest"

def srcMessageHeader = srcArgs0.ChangeOwnerRequestMsg.RequestHeader

def srcMessageBody = srcArgs0.ChangeOwnerRequestMsg.ChangeOwnerRequest
destArgs0.beId  = srcMessageHeader.TenantId
destArgs0.msgLanguageCode  = srcMessageHeader.Language
destArgs0.commandId = srcMessageHeader.CommandId
destArgs0.version = srcMessageHeader.Version
destArgs0.transactionId = srcMessageHeader.TransactionId
destArgs0.sequenceId = srcMessageHeader.SequenceId
destArgs0.requestType = srcMessageHeader.RequestType
destArgs0.loginSystem = srcMessageHeader.SessionEntity.Name
destArgs0.password = srcMessageHeader.SessionEntity.Password
destArgs0.remoteAddress = srcMessageHeader.SessionEntity.RemoteAddress
destArgs0.interFrom = srcMessageHeader.InterFrom
destArgs0.interMedi = srcMessageHeader.InterMedi
destArgs0.interMode = srcMessageHeader.InterMode
destArgs0.visitArea = srcMessageHeader.visitArea
destArgs0.currentCell = srcMessageHeader.currentCell
destArgs0.additionInfo = srcMessageHeader.additionInfo
destArgs0.thirdPartyId = srcMessageHeader.ThirdPartyID
destArgs0.partnerId = srcMessageHeader.PartnerID
destArgs0.operatorId = srcMessageHeader.OperatorID
destArgs0.tradePartnerId = srcMessageHeader.TradePartnerID
destArgs0.partnerOperId = srcMessageHeader.PartnerOperID
destArgs0.belToAreaId = srcMessageHeader.BelToAreaID
destArgs0.reserve2 = srcMessageHeader.Reserve2
destArgs0.reserve3 = srcMessageHeader.Reserve3
destArgs0.messageSeq = srcMessageHeader.SerialNo
destArgs0.remark = srcMessageHeader.Remark
destArgs0.performanceStatCmd = "ChangeOwner"

def srcMidVar0 = srcMessageBody.Customer
def destMidVar0 = destArgs1.customer

destMidVar0.firstName = srcMidVar0.FirstName
destMidVar0.middleName = srcMidVar0.MiddleName
destMidVar0.lastName = srcMidVar0.LastName
destMidVar0.customerID = srcMidVar0.CustomerID
destMidVar0.customerCode = srcMidVar0.CustomerCode
destMidVar0.idType = srcMidVar0.IdType
destMidVar0.idCode = srcMidVar0.IdCode
destMidVar0.gender = srcMidVar0.Gender
destMidVar0.birthday = srcMidVar0.Birthday
destMidVar0.title = srcMidVar0.Title
destMidVar0.salary = srcMidVar0.Salary
destMidVar0.homeAddress = srcMidVar0.HomeAddress
destMidVar0.grade = srcMidVar0.Grade
destMidVar0.email = srcMidVar0.Email
destMidVar0.zipCode = srcMidVar0.ZipCode
destMidVar0.country = srcMidVar0.Country
destMidVar0.nativePlace = srcMidVar0.NativePlace
destMidVar0.nationType = srcMidVar0.NationType
destMidVar0.jobType = srcMidVar0.JobType
destMidVar0.education = srcMidVar0.Education
destMidVar0.maritalStatus = srcMidVar0.MaritalStatus
destMidVar0.skill = srcMidVar0.Skill
destMidVar0.socialNo = srcMidVar0.SocialNo
destMidVar0.creditGrade = srcMidVar0.CreditGrade
destMidVar0.creditAmount = srcMidVar0.CreditAmount
destMidVar0.customerState = srcMidVar0.CustomerState
destMidVar0.contactNumber1 = srcMidVar0.ContactNumber1
destMidVar0.contactNumber2 = srcMidVar0.ContactNumber2
destMidVar0.contactNumber3 = srcMidVar0.ContactNumber3
destMidVar0.contactNumber4 = srcMidVar0.ContactNumber4  
destMidVar0.contactNumber5 = srcMidVar0.ContactNumber5
destMidVar0.systemNofityNumber = srcMidVar0.SystemNofityNumber
destMidVar0.homePhone = srcMidVar0.HomePhone
destMidVar0.workPhone = srcMidVar0.WorkPhone
destMidVar0.belToAreaID = srcMidVar0.BelToAreaID
destMidVar0.postAddress1 = srcMidVar0.PostAddress1
destMidVar0.postAddress2 = srcMidVar0.PostAddress2
destMidVar0.postAddress3 = srcMidVar0.PostAddress3
destMidVar0.postAddress4 = srcMidVar0.PostAddress4
destMidVar0.postAddress5 = srcMidVar0.PostAddress5
destMidVar0.remark = srcMidVar0.Remark
destMidVar0.langType = srcMidVar0.LangType
destMidVar0.timeZone = srcMidVar0.TimeZone
def listMapping0
listMapping0 = 
{
    src,dest  ->
	  dest.id = src.Id
	  dest.value = src.Value	
}
mappingList(srcMidVar0.SimpleProperty,destMidVar0.simplePropertys,listMapping0)
destMidVar0.operationType = srcMidVar0.OperType

def srcMidVar1 = srcMessageBody.Account
def destMidVar1 = destArgs1.account

destMidVar1.firstName = srcMidVar1.FirstName
destMidVar1.middleName = srcMidVar1.MiddleName
destMidVar1.lastName = srcMidVar1.LastName
destMidVar1.accountID = srcMidVar1.AccountID
destMidVar1.accountCode = srcMidVar1.AccountCode
destMidVar1.paidMode = srcMidVar1.PaidMode
destMidVar1.paymentMethod = srcMidVar1.PaymentMethod
destMidVar1.billFlag = srcMidVar1.BillFlag
destMidVar1.title = srcMidVar1.Title
destMidVar1.billAddress1 = srcMidVar1.BillAddress1
destMidVar1.billAddress2 = srcMidVar1.BillAddress2
destMidVar1.billAddress3 = srcMidVar1.BillAddress3
destMidVar1.billAddress4 = srcMidVar1.BillAddress4
destMidVar1.billAddress5 = srcMidVar1.BillAddress5
destMidVar1.zipCode = srcMidVar1.ZipCode
destMidVar1.billLang = srcMidVar1.BillLang
destMidVar1.emailBillAddr = srcMidVar1.EmailBillAddr
destMidVar1.sMSBillLang = srcMidVar1.SMSBillLang
destMidVar1.sMSBillAddr = srcMidVar1.SMSBillAddr
destMidVar1.bankAcctNo = srcMidVar1.BankAcctNo
destMidVar1.bankID = srcMidVar1.BankID
destMidVar1.bankName = srcMidVar1.BankName
destMidVar1.bankAcctName = srcMidVar1.BankAcctName
destMidVar1.bankAccType = srcMidVar1.BankAccType
destMidVar1.bankAcctActiveDate = srcMidVar1.BankAcctActiveDate
destMidVar1.cardExpiryDate = srcMidVar1.CardExpiryDate
destMidVar1.SFID = srcMidVar1.SFID
destMidVar1.SPID = srcMidVar1.SPID
destMidVar1.cCGroup = srcMidVar1.CCGroup
destMidVar1.cCSubGroup = srcMidVar1.CCSubGroup
destMidVar1.vATNumber = srcMidVar1.VATNumber
destMidVar1.printVATNo = srcMidVar1.PrintVATNo
destMidVar1.dueDate = srcMidVar1.DueDate
destMidVar1.pPSAcctInitBal = srcMidVar1.PPSAcctInitBal
destMidVar1.pPSAcctCredit = srcMidVar1.PPSAcctCredit
destMidVar1.pOSAcctInitBal = srcMidVar1.POSAcctInitBal
destMidVar1.pOSAcctCredit = srcMidVar1.POSAcctCredit
destMidVar1.contactTel = srcMidVar1.ContactTel
destMidVar1.dCCallForward = srcMidVar1.DCCallForward
destMidVar1.billCycleCredit = srcMidVar1.BillCycleCredit
destMidVar1.creditCtrlMode = srcMidVar1.CreditCtrlMode
destMidVar1.staffID = srcMidVar1.StaffID
mappingList(srcMidVar1.SimpleProperty,destMidVar1.simplePropertys,listMapping0)
destMidVar1.billCycleType = srcMidVar1.BillCycleType
destMidVar1.operType = srcMidVar1.OperType

def srcMidVar2 = srcMessageBody.Subscriber
def destMidVar2 = destArgs1.subscriber

destMidVar2.subscriberNo = srcMidVar2.SubscriberNo
destMidVar2.customerCode = srcMidVar2.CustomerCode
destMidVar2.subscriberCode = srcMidVar2.SubscriberCode
destMidVar2.accountCode = srcMidVar2.AccountCode
destMidVar2.password = srcMidVar2.Password

destArgs1.serviceCtrlFlag = srcMessageBody.ServiceCtrlFlag