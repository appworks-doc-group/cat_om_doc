dest.setServiceOperation("AccountService", "batchFeeDeduction")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.account.batch.feededuction.io.BatchFeeDeductionRequest"

def listMapping0

listMapping0 =
        {
            src, dest ->

                dest.code = src.Code

                dest.value = src.Value
        }

def srcMidVar = srcArgs0.BatchFeeDeductionRequestMsg.RequestHeader

destArgs0.version = srcMidVar.Version

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

def srcMidVar0 = srcMidVar.OwnershipInfo

destArgs0.beId = srcMidVar0.BEID

destArgs0.brId = srcMidVar0.BRID

def srcMidVar1 = srcMidVar.AccessSecurity

destArgs0.loginSystem = srcMidVar1.LoginSystemCode

destArgs0.password = srcMidVar1.Password

destArgs0.remoteAddress = srcMidVar1.RemoteIP

def srcMidVar2 = srcMidVar.OperatorInfo

destArgs0.operatorId = srcMidVar2.OperatorID

destArgs0.channelId = srcMidVar2.ChannelID

destArgs0.interMode = srcMidVar.AccessMode

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar3 = srcMidVar.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

mappingList(srcMidVar.AdditionalProperty, destArgs0.simpleProperty, listMapping0)

def srcMidVar4 = srcArgs0.BatchFeeDeductionRequestMsg.BatchFeeDeductionRequest

destArgs1.deductBatchNo = srcMidVar4.DeductBatchNo

destArgs1.opType = srcMidVar4.OpType

def srcMidVar5 = srcMidVar4.OperatorInfo

destArgs1.operationInfo.operationCode = srcMidVar5.OperationCode

mappingList(srcMidVar5.OperationProperty, destArgs1.operationInfo.operationProperty, listMapping0)

destArgs1.channelId = srcMidVar4.ChannelID

destArgs1.remark = srcMidVar4.Remark

mappingList(srcMidVar4.AdditionalProperty, destArgs1.additionalProperty, listMapping0)

destArgs1.requestFileName = srcMidVar4.FileName