def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.QueryAppendSubIdentityResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

def listMapping0

listMapping0 = 
{
    src0,dest0  ->

	dest0.Code = src0.code
	
	dest0.Value = src0.value
	
}

addingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.Version = srcMidVar.version

def listMapping1

listMapping1 = 
{
    src1,dest1  ->

	dest1.SubIdentity = src1.subIdentity
	
	dest1.IMSI = src1.imsi
	
	dest1.SubIdentityType = src1.subIdentityType
	
}

def destMidVar0 = destReturn.QueryAppendSubIdentityResultMsg.QueryAppendSubIdentityResult

addingList(srcReturn.subIdentityList,destMidVar0.SubIdentityList,listMapping1)
