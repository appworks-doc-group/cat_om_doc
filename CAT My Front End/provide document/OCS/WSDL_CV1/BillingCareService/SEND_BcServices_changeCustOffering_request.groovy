import com.huawei.ngcbs.bm.common.common.Constant4Model


dest.setServiceOperation("CustomerService","changeCustOffering")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.customer.changecustoffering.io.ChangeCustOfferingRequest"

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	def srcMidVar0 = src.AccessSecurity
	
	dest.loginSystem = srcMidVar0.LoginSystemCode
	
	dest.password = srcMidVar0.Password
	
	dest.remoteAddress = srcMidVar0.RemoteIP
	
	mappingList(src.AdditionalProperty,dest.simpleProperty,listMapping1)
	
	dest.businessCode = src.BusinessCode
	
	dest.messageSeq = src.MessageSeq
	
	dest.msgLanguageCode = src.MsgLanguageCode
	
	def srcMidVar1 = src.OperatorInfo
	
	dest.channelId = srcMidVar1.ChannelID
	
	dest.operatorId = srcMidVar1.OperatorID
	
	dest.version = src.Version
	
	def srcMidVar2 = src.TimeFormat
	
	dest.timeType = srcMidVar2.TimeType
	
	dest.timeZoneId = srcMidVar2.TimeZoneID
	
	def srcMidVar3 = src.OwnershipInfo
	
	dest.beId = srcMidVar3.BEID
	
	dest.brId = srcMidVar3.BRID
	
	dest.interMode = src.AccessMode
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping9

listMapping9 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.oCode = src.OfferingCode
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping12

listMapping12 = 
{
    src,dest  ->

	dest.code = src.SubPropCode
	
	dest.value = src.Value
	
}

def listMapping11

listMapping11 = 
{
    src,dest  ->

	def destMidVar1 = dest.property
	
	destMidVar1.propCode = src.PropCode
	
	mappingList(src.SubPropInst,destMidVar1.subProps,listMapping12)
	
	destMidVar1.value = src.Value
	
	destMidVar1.complexFlag = src.PropType
	
		dest.effDate=parseDate(src.EffectiveTime, Constant4Model.DATE_FORMAT)
	
		dest.expDate=parseDate(src.ExpirationTime, Constant4Model.DATE_FORMAT)
	
}

def listMapping10

listMapping10 = 
{
    src,dest  ->

	def destMidVar0 = dest.productInst
	
	destMidVar0.networkType = src.NetworkType
	
	destMidVar0.packageFlag = src.PackageFlag
	
	destMidVar0.parentProdId = src.ParentProdID
	
	destMidVar0.primaryFlag = src.PrimaryFlag
	
	destMidVar0.prodId = src.ProductID
	
	destMidVar0.productType = src.ProductType
	
	mappingList(src.PInstProperty,dest.properties,listMapping11)
	
}

def listMapping13

listMapping13 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.oCode = src.OfferingCode
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping14

listMapping14 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.oCode = src.OfferingCode
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping16

listMapping16 = 
{
    src,dest  ->

	dest.code = src.SubPropCode
	
	dest.value = src.Value
	
}

def listMapping15

listMapping15 = 
{
    src,dest  ->

	def destMidVar2 = dest.property
	
	mappingList(src.SubPropInst,destMidVar2.subProps,listMapping16)
	
	destMidVar2.value = src.Value
	
	destMidVar2.complexFlag = src.PropType
	
	destMidVar2.propCode = src.PropCode
	
		dest.effDate=parseDate(src.EffectiveTime, Constant4Model.DATE_FORMAT)
	
		dest.expDate=parseDate(src.ExpirationTime, Constant4Model.DATE_FORMAT)
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	def srcMidVar4 = src.ActivationTime
	
	dest.activeMode = srcMidVar4.Mode
	
	def destMidVar = dest.offeringInst
	
	destMidVar.status = src.Status
	
		destMidVar.trialEndTime=parseDate(src.TrialEndTime, Constant4Model.DATE_FORMAT)
	
		destMidVar.trialStartTime=parseDate(src.TrialStartTime, Constant4Model.DATE_FORMAT)
		
	def oKeyExt9 = destMidVar.relGOfferingKey
	
  oKeyExt9._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
	
	listMapping9.call(src.RelGOfferingKey,oKeyExt9)
	
		destMidVar.activeTime=parseDate(srcMidVar4.ActiveTime, Constant4Model.DATE_FORMAT)
	
		destMidVar.activeTimeLimit=parseDate(srcMidVar4.ActiveTimeLimit, Constant4Model.DATE_FORMAT)
	
	mappingList(src.ProductInst,dest.productInsts,listMapping10)
	
	def oKeyExt13 = destMidVar.parentOfferingKey
	
  oKeyExt13._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
	
	listMapping13.call(src.ParentOfferingKey,oKeyExt13)
	
	def oKeyExt14 = destMidVar.offeringKey
	
  oKeyExt14._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
	
	listMapping14.call(src.OfferingKey,oKeyExt14)
	
	destMidVar.offeringClass = src.OfferingClass
	
	def srcMidVar5 = src.EffectiveTime
	
	dest.effMode = srcMidVar5.Mode
	
		dest.effDate=parseDate(srcMidVar5.Time, Constant4Model.DATE_FORMAT)
	
		dest.expDate=parseDate(src.ExpirationTime, Constant4Model.DATE_FORMAT)
	
	destMidVar.activeMode = srcMidVar4.Mode
	
	mappingList(src.OInstProperty,dest.properties,listMapping15)
	
	destMidVar.bundleFlag = src.BundledFlag
	
}

def listMapping8

listMapping8 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.oCode = src.OfferingCode
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->
    
  def oKeyExt8 = dest.offeringKey
  
  oKeyExt8._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"

	listMapping8.call(src.OfferingKey,oKeyExt8)
	
	dest.expMode = "I"
	
	dest.isDelOfferFlag = "Y"

  dest.expDate = parseDate(src.ExpirationTime, Constant4Model.DATE_FORMAT)
	
}

def listMapping18

listMapping18 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.oCode = src.OfferingCode
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping17

listMapping17 = 
{
    src,dest  ->

		dest.effDate=parseDate(src.NewEffectiveTime, Constant4Model.DATE_FORMAT)
	
	def srcMidVar6 = src.NewExpirationTime
	
	dest.expMode = srcMidVar6.Mode
	
		dest.expDate=parseDate(srcMidVar6.Time, Constant4Model.DATE_FORMAT)
		
	def oKeyExt18 = dest.offeringKey
	
  oKeyExt18._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
	
	listMapping18.call(src.OfferingKey,oKeyExt18)
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	listMapping5.call(src.CustAccessCode,dest.custAccessCode)
	
	mappingList(src.AddOffering,dest.addOfferingInsts,listMapping6)
	
	addingList(src.DelOffering,dest.modifyOfferingInsts,listMapping7)
	
	addingList(src.ModifyOffering,dest.modifyOfferingInsts,listMapping17)
	
}

def srcMidVar = srcArgs0.ChangeCustOfferingRequestMsg

listMapping0.call(srcMidVar.RequestHeader,destArgs0)

listMapping2.call(srcMidVar.ChangeCustOfferingRequest,destArgs1)
