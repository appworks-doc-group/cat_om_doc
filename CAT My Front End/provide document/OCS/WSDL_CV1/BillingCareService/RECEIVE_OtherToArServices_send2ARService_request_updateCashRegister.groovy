dest.setServiceOperation("OtherToArServices","updateCashRegister")

def srcArgs0 = src.payload._args[0]

def srcArgs1 = src.payload._args[1]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	listMapping2.call(src.additionalProperty,dest.AdditionalProperty)
	
	dest.PaymentMethodID = src.paymentMethodId
	
	dest.TransactionAmount = src.transactionAmount
	
}

def destMidVar = destArgs0.UpdateCashRegisterRequestMsg.RequestHeader

destMidVar.AccessMode = srcArgs0.interMode

def destMidVar0 = destArgs0.UpdateCashRegisterRequestMsg.RequestHeader.AccessSecurity

destMidVar0.LoginSystemCode = srcArgs0.loginSystem

destMidVar.MessageSeq = srcArgs0.messageSeq

destMidVar.MsgLanguageCode = srcArgs0.msgLanguageCode

destMidVar.BusinessCode = srcArgs0.businessCode

mappingList(srcArgs0.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

destMidVar0.Password = srcArgs0.password

destMidVar0.RemoteIP = srcArgs0.remoteAddress

def destMidVar1 = destArgs0.UpdateCashRegisterRequestMsg.RequestHeader.OperatorInfo

destMidVar1.ChannelID = srcArgs0.channelId

destMidVar1.OperatorID = srcArgs0.operatorId

def destMidVar2 = destArgs0.UpdateCashRegisterRequestMsg.RequestHeader.OwnershipInfo

destMidVar2.BEID = srcArgs0.beId

destMidVar2.BRID = srcArgs0.brId

def destMidVar3 = destArgs0.UpdateCashRegisterRequestMsg.RequestHeader.TimeFormat

destMidVar3.TimeType = srcArgs0.timeType

destMidVar3.TimeZoneID = srcArgs0.timeZoneId

destMidVar.Version = srcArgs0.version

def destMidVar4 = destArgs0.UpdateCashRegisterRequestMsg.UpdateCashRegisterRequest

destMidVar4.BatchNo = srcArgs1.batchNo

destMidVar4.CurrencyID = srcArgs1.currencyId

destMidVar4.OperID = srcArgs1.operId

destMidVar4.TransactionID = srcArgs1.transactionId

mappingList(srcArgs1.transactionList,destMidVar4.TransactionList,listMapping1)

destMidVar4.TransactionTime = srcArgs1.transactionTime
