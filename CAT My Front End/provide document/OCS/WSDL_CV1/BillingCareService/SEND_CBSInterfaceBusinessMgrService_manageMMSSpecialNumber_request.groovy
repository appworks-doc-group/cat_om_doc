dest.setServiceOperation("CBSInterfaceBusinessMgrService", "manageMMSSpecialNumber");

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs12ws.subscriber.managemmsspecialnumber.io.ManageMMSSpecialNumberRequest"

def srcMidVar = srcArgs0.ManageMMSSpecialNumberRequestMsg.ManageMMSSpecialNumberRequest

destArgs1.operationType = srcMidVar.OperationType

destArgs1.specialNumber = srcMidVar.SpecialNumber

def listMapping0

listMapping0 =
        {
            src, dest ->
                dest.specialNumberType = src.SpecialType
                dest.numberPart = src.NumberPart
                dest.interfaceType = src.InterfaceType

        }

mappingList(srcMidVar.SpecialInfo,destArgs1.specialInfoList,listMapping0)

def srcMidVar0 = srcArgs0.ManageMMSSpecialNumberRequestMsg.RequestHeader

destArgs0.beId = srcMidVar0.TenantId

destArgs0.operatorId = srcMidVar0.OperatorID

destArgs0.additionInfo = srcMidVar0.additionInfo

destArgs0.commandId = srcMidVar0.CommandId

destArgs0.interMedi = srcMidVar0.InterMedi

destArgs0.transactionId = srcMidVar0.TransactionId

destArgs0.reserve2 = srcMidVar0.Reserve2

destArgs0.reserve3 = srcMidVar0.Reserve3

destArgs0.interMode = srcMidVar0.InterMode

destArgs0.sequenceId = srcMidVar0.SequenceId

destArgs0.visitArea = srcMidVar0.visitArea

destArgs0.messageSeq = srcMidVar0.SerialNo

destArgs0.belToAreaId = srcMidVar0.BelToAreaID

destArgs0.partnerId = srcMidVar0.PartnerID

destArgs0.thirdPartyId = srcMidVar0.ThirdPartyID

destArgs0.remark = srcMidVar0.Remark

destArgs0.tradePartnerId = srcMidVar0.TradePartnerID

destArgs0.version = srcMidVar0.Version

destArgs0.partnerOperId = srcMidVar0.PartnerOperID

def srcMidVar1 = srcArgs0.ManageMMSSpecialNumberRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar1.Name

destArgs0.password = srcMidVar1.Password

destArgs0.remoteAddress = srcMidVar1.RemoteAddress

destArgs0.requestType = srcMidVar0.RequestType

destArgs0.interFrom = srcMidVar0.InterFrom

destArgs0.currentCell = srcMidVar0.currentCell