def srcReturn = src.payload._return
def destReturn = dest.payload._return

def srcResultHeader = srcReturn.resultHeader
def srcResultBody = srcReturn.resultBody

def destMidVar = destReturn.setMainCommodityResponse.SetMainCommodityResult.ResultMessage.MessageHeader

destMidVar.CommandId = srcResultHeader.commandId
destMidVar.Version = srcResultHeader.version
destMidVar.TransactionId = srcResultHeader.transactionId
destMidVar.SequenceId = srcResultHeader.sequenceId
destMidVar.ResultCode = srcResultHeader.resultCode
destMidVar.ResultDesc = srcResultHeader.resultDesc

