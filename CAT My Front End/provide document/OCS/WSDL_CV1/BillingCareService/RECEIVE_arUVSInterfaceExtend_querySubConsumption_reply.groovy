def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar4 = destReturn.QuerySubConsumptionResponse.QuerySubConsumptionResult.ResultMessage

def destMidVar0 = destMidVar4.MessageHeader

def srcMidVar = srcReturn.ocs11ResultHeader

destMidVar0.CommandId = srcMidVar.commandId

destMidVar0.TenantId = srcMidVar.tenantId

destMidVar0.Version = srcMidVar.version

destMidVar0.TransactionId = srcMidVar.transactionId

destMidVar0.SequenceId = srcMidVar.sequenceId

destMidVar0.ResultCode = srcMidVar.resultCode

destMidVar0.ResultDesc = srcMidVar.resultDesc

destMidVar0.Language = srcMidVar.language

def destMidVar1 = destMidVar4.MessageBody

destMidVar1.SubscriberID = srcReturn.querySubConsumptionResponse.subscriberID

destMidVar1.PreAmount = srcReturn.querySubConsumptionResponse.preAmount

destMidVar1.CurAmount = srcReturn.querySubConsumptionResponse.curAmount

destMidVar1.Paidmode = srcReturn.querySubConsumptionResponse.paidmode



