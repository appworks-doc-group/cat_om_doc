def srcReturnData = src.payload._return
def destReturnDoc = dest.payload._return

def destHeader = destReturnDoc.DelSubscriberExtendResultMsg.ResultHeader
def srcHeader = srcReturnData.resultHeader

destHeader.CommandId = srcHeader.commandId
destHeader.ResultCode = srcHeader.resultCode
destHeader.ResultDesc = srcHeader.resultDesc
destHeader.SequenceId = srcHeader.sequenceId
destHeader.Version = srcHeader.version
destHeader.TransactionId = srcHeader.transactionId
destHeader.OperationTime = srcHeader.operationTime
destHeader.OrderId = srcHeader.orderId

def srcBusinessData = srcReturnData.resultBody
srcBusinessData._class = "com.huawei.ngcbs.cm.ocs33ws.subscriber.delsubscriberextend.io.OCS33DelSubscriberExtendResult"
def destReturnData = destReturnDoc.DelSubscriberExtendResultMsg.DelSubscriberExtendResult

def listMapping0 = 
{
  src,dest  ->
	dest.SubscriberNo = src.subscriberNo
	dest.Balance = src.balance
	dest.PrimaryOfferOrderIdentity.OfferKey = src.primaryOfferOrderIdentity.offerKey
	dest.PrimaryOfferOrderIdentity.ExtOfferCode = src.primaryOfferOrderIdentity.extOfferCode
	dest.PrimaryOfferOrderIdentity.ExtOfferOrderCode = src.primaryOfferOrderIdentity.extOfferOrderCode
}

mappingList(srcBusinessData.delSubscriberInfList, destReturnData.DelSubscriberInfo, listMapping0)
