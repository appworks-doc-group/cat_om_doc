def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Name = src.name
	
	dest.Password = src.password
	
	dest.RemoteAddress = src.remoteAddress
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.BelToAreaID = src.belToAreaId
	
	dest.CommandId = src.commandId
	
	dest.currentCell = src.currentCell
	
	dest.InterFrom = src.interFrom
	
	dest.InterMedi = src.interMedi
	
	dest.InterMode = src.interMode
	
	dest.OperatorID = src.operatorId
	
	dest.PartnerID = src.partnerId
	
	dest.PartnerOperID = src.partnerOperId
	
	dest.Remark = src.remark
	
	dest.RequestType = src.requestType
	
	dest.SequenceId = src.sequenceId
	
	dest.SerialNo = src.serialNo
	
	dest.TenantID = src.tenantId
	
	dest.ThirdPartyID = src.thirdPartyId
	
	dest.TradePartnerID = src.tradePartnerId
	
	dest.TransactionId = src.transactionId
	
	dest.Version = src.version
	
	dest.visitArea = src.visitArea
	
	listMapping1.call(src.sessionEntity,dest.SessionEntity)
	
}

def destMidVar = destArgs0.ChangeMSISDNRequestMsg.ChangeMSISDNRequest

destMidVar.CBPID = srcArgs0.cbpId

destMidVar.NewSubscriberNo = srcArgs0.newSubscriberNo

destMidVar.SubscriberNo = srcArgs0.subscriberNo

def destMidVar0 = destArgs0.ChangeMSISDNRequestMsg

listMapping0.call(srcArgs0.omRequestHeader,destMidVar0.RequestHeader)
