def srcReturn = src.payload._return

def destReturn = dest.payload._return

def srcMidVar = srcReturn.QuerySpecnumberTypeResultMsg.QuerySpecnumberTypeResult

destReturn.numberType = srcMidVar.NumberType

destReturn.subscriberNo = srcMidVar.SpecialNumber

def destMidVar = destReturn.resultHeader

def srcMidVar0 = srcReturn.QuerySpecnumberTypeResultMsg.ResultHeader

destMidVar.resultCode = srcMidVar0.ResultCode

destMidVar.resultDesc = srcMidVar0.ResultDesc

destMidVar.version = srcMidVar0.Version

destReturn._class = "com.huawei.ngcbs.cm.common.ws.client.io.om.OMQuerySpecialNumberTypeResult"
