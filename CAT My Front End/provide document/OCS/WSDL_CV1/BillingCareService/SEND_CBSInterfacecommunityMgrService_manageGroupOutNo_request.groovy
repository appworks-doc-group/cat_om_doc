dest.setServiceOperation("CBSInterfacecommunityMgrService","manageGroupOutNo")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs12ws.group.ManageGroupOutNo.io.ManageGroupOutNoRequest"

def srcMidVar = srcArgs0.ManageGroupOutNoRequestMsg.RequestHeader

destArgs0.beId = srcMidVar.TenantId

destArgs0.additionInfo = srcMidVar.additionInfo

destArgs0.belToAreaId = srcMidVar.BelToAreaID

destArgs0.commandId = srcMidVar.CommandId

destArgs0.currentCell = srcMidVar.currentCell

destArgs0.interFrom = srcMidVar.InterFrom

destArgs0.interMedi = srcMidVar.InterMedi

destArgs0.interMode = srcMidVar.InterMode

destArgs0.operatorId = srcMidVar.OperatorID

destArgs0.partnerId = srcMidVar.PartnerID

destArgs0.partnerOperId = srcMidVar.PartnerOperID

destArgs0.remark = srcMidVar.Remark

destArgs0.requestType = srcMidVar.RequestType

destArgs0.reserve2 = srcMidVar.Reserve2

destArgs0.reserve3 = srcMidVar.Reserve3

destArgs0.sequenceId = srcMidVar.SequenceId

destArgs0.messageSeq = srcMidVar.SerialNo

destArgs0.thirdPartyId = srcMidVar.ThirdPartyID

destArgs0.tradePartnerId = srcMidVar.TradePartnerID

destArgs0.transactionId = srcMidVar.TransactionId

destArgs0.version = srcMidVar.Version

destArgs0.visitArea = srcMidVar.visitArea

def srcMidVar0 = srcArgs0.ManageGroupOutNoRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar0.Name

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteAddress

def srcArgs1 = srcArgs0.ManageGroupOutNoRequestMsg.ManageGroupOutNoRequest

destArgs1.groupNumber = srcArgs1.GroupNumber

destArgs1.operationType = srcArgs1.OperationType

def mapping0 =
{
src,dest  ->

dest.groupOutNumber = src.GroupOutNumber

dest.outNumberType  = src.OutNumberType

dest.groupOutShortNo = src.GroupOutShortNo

dest.outTeamKey = src.OutTeamKey

dest.effectiveDate = src.EffectiveDate

dest.expireDate = src.ExpireDate

dest.operationType = src.OperationType

}

mappingList(srcArgs1.GroupOutNoInfo , destArgs1.groupOutNoInfoList , mapping0)



