def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destArgs0.BalanceShiftRequestMsg.BalanceShiftRequest.TransfereeAcct

destMidVar.AccountCode = srcArgs0.newAcctCode

def destMidVar0 = destArgs0.BalanceShiftRequestMsg.BalanceShiftRequest.TransferorAcct

destMidVar0.AccountCode = srcArgs0.oldAcctCode

def destMidVar1 = destArgs0.BalanceShiftRequestMsg.RequestHeader

def srcMidVar = srcArgs0.messageHeader

destMidVar1.AccessMode = srcMidVar.interMode

def destMidVar2 = destArgs0.BalanceShiftRequestMsg.RequestHeader.AccessSecurity

destMidVar2.LoginSystemCode = srcMidVar.loginSystem

destMidVar2.Password = srcMidVar.password

destMidVar2.RemoteIP = srcMidVar.remoteAddress

mappingList(srcMidVar.simpleProperty,destMidVar1.AdditionalProperty,listMapping0)

destMidVar1.BusinessCode = srcMidVar.businessCode

destMidVar1.MessageSeq = srcMidVar.messageSeq

destMidVar1.MsgLanguageCode = srcMidVar.msgLanguageCode

def destMidVar3 = destArgs0.BalanceShiftRequestMsg.RequestHeader.OperatorInfo

destMidVar3.ChannelID = srcMidVar.channelId

destMidVar3.OperatorID = srcMidVar.operatorId

def destMidVar4 = destArgs0.BalanceShiftRequestMsg.RequestHeader.OwnershipInfo

destMidVar4.BEID = srcMidVar.beId

destMidVar4.BRID = srcMidVar.brId

def destMidVar5 = destArgs0.BalanceShiftRequestMsg.RequestHeader.TimeFormat

destMidVar5.TimeType = srcMidVar.timeType

destMidVar5.TimeZoneID = srcMidVar.timeZoneId

destMidVar1.Version = srcMidVar.version
