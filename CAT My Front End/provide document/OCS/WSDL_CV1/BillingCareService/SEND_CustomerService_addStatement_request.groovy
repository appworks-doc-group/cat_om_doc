import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.customer.addstatement.io.AddStatementRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.addr1 = src.Address1
	
	dest.addr10 = src.Address10
	
	dest.addr11 = src.Address11
	
	dest.addr12 = src.Address12
	
	dest.addr2 = src.Address2
	
	dest.addr3 = src.Address3
	
	dest.addr4 = src.Address4
	
	dest.addr5 = src.Address5
	
	dest.addr6 = src.Address6
	
	dest.addr7 = src.Address7
	
	dest.addr8 = src.Address8
	
	dest.addr9 = src.Address9
	
	dest.tpAddrKey = src.AddressKey
	
	dest.postCode = src.PostCode
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.BMCode
	
	dest.type = src.BMType
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.objKey = src
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.objKey = src
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.AddStatementRequestMsg.AddStatementRequest

listMapping0.call(srcMidVar.AddressInfo,destArgs1.addressInfo)

def destMidVar = destArgs1.addStatementInfo

def srcMidVar0 = srcArgs0.AddStatementRequestMsg.AddStatementRequest.EffectiveTime

destMidVar.effMode = srcMidVar0.Mode

destMidVar.effDate=parseDate(srcMidVar0.Time, Constant4Model.DATE_FORMAT)

def destMidVar0 = destArgs1.custAccessCode

def srcMidVar1 = srcArgs0.AddStatementRequestMsg.AddStatementRequest.RegisterCust

destMidVar0.customerCode = srcMidVar1.CustomerCode

destMidVar0.primaryIdentity = srcMidVar1.PrimaryIdentity

destMidVar0.customerKey = srcMidVar1.CustomerKey

def destMidVar1 = destArgs1.addStatementInfo.statementInfo

def srcMidVar2 = srcArgs0.AddStatementRequestMsg.AddStatementRequest.Statement

destMidVar1.invoiceType = srcMidVar2.InvoiceType

destMidVar1.stmtClass = srcMidVar2.SmtClass

destMidVar1.stmtKey = srcMidVar2.SmtKey

def destMidVar2 = destArgs1.addStatementInfo.contactInfo

def srcMidVar3 = srcArgs0.AddStatementRequestMsg.AddStatementRequest.Statement.SmtInfo.ContactInfo

destMidVar2.addrKey = srcMidVar3.AddressKey

destMidVar2.email = srcMidVar3.Email

destMidVar2.fax = srcMidVar3.Fax

destMidVar2.firstName = srcMidVar3.FirstName

destMidVar2.homePhone = srcMidVar3.HomePhone

destMidVar2.lastName = srcMidVar3.LastName

destMidVar2.middleName = srcMidVar3.MiddleName

destMidVar2.mobilePhone = srcMidVar3.MobilePhone

destMidVar2.officePhone = srcMidVar3.OfficePhone

destMidVar2.title = srcMidVar3.Title

def srcMidVar4 = srcArgs0.AddStatementRequestMsg.AddStatementRequest.Statement.SmtInfo

destMidVar1.stmtLang = srcMidVar4.SmtLang

mappingList(srcMidVar4.SmtMedium,destMidVar.bmInfos,listMapping1)

def destMidVar3 = destArgs1.addStatementInfo.stmtOfferingInfo

def srcMidVar5 = srcArgs0.AddStatementRequestMsg.AddStatementRequest.Statement.SmtInfo.SmtOffering

destMidVar3.oId = srcMidVar5.OfferingID

def srcMidVar6 = srcArgs0.AddStatementRequestMsg.AddStatementRequest.Statement.SmtInfo.SmtOffering.OfferingOwner

destMidVar3.ownerKey = srcMidVar6.OwnerKey

destMidVar3.ownerType = srcMidVar6.OwnerType

destMidVar3.purchaseSeq = srcMidVar5.PurchaseSeq

def srcMidVar7 = srcArgs0.AddStatementRequestMsg.AddStatementRequest.StatementScenario

mappingList(srcMidVar7.AccountKey,destMidVar.scenarios,listMapping2)

mappingList(srcMidVar7.SubscriberKey,destMidVar.scenarios,listMapping3)

def srcMidVar8 = srcArgs0.AddStatementRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar8.LoginSystemCode

destArgs0.password = srcMidVar8.Password

destArgs0.remoteAddress = srcMidVar8.RemoteIP

def srcMidVar9 = srcArgs0.AddStatementRequestMsg.RequestHeader

mappingList(srcMidVar9.AdditionalProperty,destArgs0.simpleProperty,listMapping4)

destArgs0.businessCode = srcMidVar9.BusinessCode

destArgs0.messageSeq = srcMidVar9.MessageSeq

destArgs0.msgLanguageCode = srcMidVar9.MsgLanguageCode

def srcMidVar10 = srcArgs0.AddStatementRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar10.ChannelID

destArgs0.operatorId = srcMidVar10.OperatorID

def srcMidVar11 = srcArgs0.AddStatementRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar11.BEID

destArgs0.brId = srcMidVar11.BRID

def srcMidVar12 = srcArgs0.AddStatementRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar12.TimeType

destArgs0.timeZoneId = srcMidVar12.TimeZoneID

destArgs0.version = srcMidVar9.Version

destArgs0.interMode = srcMidVar9.AccessMode
