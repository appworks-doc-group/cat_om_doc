import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.SubaccountType = src.subAccountType
	
	dest.SubaccountChargedAmount = src.subAccountChargedAmount
	
	dest.PromotionMoneySourceOriginName = src.promotionMoneySourceOriginName
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.FreeunitType = src.freeUnitType
	
	dest.FreeUnitChargeAmount = src.freeUnitChargeAmount
	
	dest.FreeUnitSourceOriginName = src.freeUnitSourceOriginName
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.EventTypeDescription = src.eventTypeDescription
	
	dest.ServiceFlow = src.serviceFlow
	
	dest.CallType = src.callType
	
	dest.OriginNumber = src.originNumber
	
	dest.DestinationNumber = src.destinationNumber
	
	dest.RatingOffering = src.ratingOffering
	
	dest.StartTime = formatDate(src.startTime, Constant4Model.DATE_FORMAT)
	
	dest.EndTime = formatDate(src.endTime, Constant4Model.DATE_FORMAT)
	
	dest.DurationORVolume = src.durationORVolume
	
	dest.ChargedAmount = src.chargedAmount
	
	dest.MainBalance = src.mainBalance
	
	dest.CCBalance = src.ccBalance
	
	dest.Description = src.description
	
	mappingList(src.chargedPromotionMoney,dest.ChargedPromotionMoney,listMapping0)
	
	mappingList(src.chargedFreeUnitDetail,dest.ChargedFreeUnitDetail,listMapping3)
}



def destMidVar = destReturn.QuerySubscriberStatementResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.Version = srcMidVar.version

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.MessageSeq = srcMidVar.messageSeq

mappingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def destMidVar0 = destReturn.QuerySubscriberStatementResultMsg.QuerySubscriberStatementResult

mappingList(srcReturn.statementDetail,destMidVar0.StatementDetail,listMapping1)

destMidVar0.TotalCDRNum = srcReturn.totalCDRNum 

destMidVar0.BeginRowNum = srcReturn.beginRowNum

destMidVar0.FetchRowNum = srcReturn.fetchRowNum
