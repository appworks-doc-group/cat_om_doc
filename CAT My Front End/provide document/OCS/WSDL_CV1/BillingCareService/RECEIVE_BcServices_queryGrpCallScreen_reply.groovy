import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 =
        {
            src,dest  ->

                dest.Code = src.code

                dest.Value = src.value

        }

def listMapping1

listMapping1 =
        {
            src, dest ->

                dest.WeekStart = src.weekStart

                dest.WeekStop = src.weekEnd

                dest.TimeStart = src.timeStart

                dest.TimeStop = src.timeEnd

        }

def listMapping2

listMapping2 =
        {
            src, dest ->

                dest.ScreenNumber = src.screenNumber
        }

def listMapping3

listMapping3 =
        {
            src, dest ->

                dest.CallScreenProfile = src.callScreenProfile

                dest.ProfileName = src.profileName

                dest.ScreenType = src.screenType

                dest.ListType = src.listType

                dest.ProfileRule = src.profileRule

                dest.CSPPrefix = src.cspPrefix

                dest.EffectDate = formatDate(src.effectDate, Constant4Model.DATE_FORMAT)

                dest.ExpireDate = formatDate(src.expireDate, Constant4Model.DATE_FORMAT)

                mappingList(src.weekSchema,dest.WeekTimeSchema,listMapping1)

                mappingList(src.screenNumberList,dest.ScreenNumberList,listMapping2)
        }

def destMidVar1 = destReturn.QueryGrpCallScreenResultMsg.QueryGrpCallScreenResult

mappingList(srcReturn.callScreenProfileList,destMidVar1.CallScreenProfile,listMapping3)



def destMidVar = destReturn.QueryGrpCallScreenResultMsg.ResultHeader

def srcResultHeader = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcResultHeader.msgLanguageCode

destMidVar.ResultCode = srcResultHeader.resultCode

destMidVar.ResultDesc = srcResultHeader.resultDesc

destMidVar.Version = srcResultHeader.version

destMidVar.MessageSeq = srcResultHeader.messageSeq

mappingList(srcResultHeader.simpleProperty,destMidVar.AdditionalProperty,listMapping0)
