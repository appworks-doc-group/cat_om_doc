import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("BMGroupService", "changeGroupCallScreenProfile")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.group.changegroupcallscreenprofile.io.ChangeGroupCallScreenProfileRequest"

def listMapping0

listMapping0 =
        {
            src, dest ->

                dest.code = src.Code

                dest.value = src.Value

        }

def listMapping1

listMapping1 =
        {
            src, dest ->
                dest.weekStart = src.WeekStart
                dest.weekEnd = src.WeekEnd
                dest.timeStart = src.TimeStart
                dest.timeEnd = src.TimeEnd

        }

def listMapping2

listMapping2 =
        {
            src, dest ->

                dest.callScreenProfile = src.CallScreenProfile

                dest.profileName = src.ProfileName

                dest.screenType = src.ScreenType

                dest.listType = src.ListType

                dest.forbiddenType = src.ForbiddenType

                dest.effDateMode = src.EffectiveTime.Mode

                dest.effDate = parseDate(src.EffectiveTime.Time, Constant4Model.DATE_FORMAT)

                dest.expDate = parseDate(src.ExpDate, Constant4Model.DATE_FORMAT)

                mappingList(src.WeekTimeSchema, dest.weekSchema, listMapping1)

                dest.profileRule = src.ProfileRule

                dest.cspPrefix = src.CSPPrefix

        }

def listMapping3

listMapping3 =
        {
            src, dest ->
                dest.callScreenProfile = src.CallScreenProfile

        }

def listMapping4

listMapping4 =
        {
            src, dest ->
                
                dest.callScreenProfile = src.CallScreenProfile
                
                mappingList(src.WeekTimeSchema, dest.weekSchema, listMapping1)
        }

def listMapping5

listMapping5 =
        {
            src, dest ->

                dest.groupCode = src.SubGroupCode

                dest.groupKey = src.SubGroupKey

        }



def srcMidVar4 = srcArgs0.ChangeGroupCallScreenProfileRequestMsg.ChangeGroupCallScreenProfileRequest.CallScreenProfile

mappingList(srcMidVar4.AddCallScreenProfile, destArgs1.addCallScreenProfileList, listMapping2)

mappingList(srcMidVar4.DelCallScreenProfile, destArgs1.delCallScreenProfileList, listMapping3)

mappingList(srcMidVar4.ModCallScreenProfile, destArgs1.modCallScreenProfileList, listMapping4)

def srcMidVar5 = srcArgs0.ChangeGroupCallScreenProfileRequestMsg.ChangeGroupCallScreenProfileRequest

listMapping5.call(srcMidVar5.SubGroupAccessCode, destArgs1.groupAccessCode)



def srcMidVar = srcArgs0.ChangeGroupCallScreenProfileRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar.LoginSystemCode

destArgs0.password = srcMidVar.Password

destArgs0.remoteAddress = srcMidVar.RemoteIP

def srcMidVar0 = srcArgs0.ChangeGroupCallScreenProfileRequestMsg.RequestHeader

mappingList(srcMidVar0.AdditionalProperty, destArgs0.simpleProperty, listMapping0)

destArgs0.businessCode = srcMidVar0.BusinessCode

destArgs0.messageSeq = srcMidVar0.MessageSeq

destArgs0.msgLanguageCode = srcMidVar0.MsgLanguageCode

def srcMidVar1 = srcArgs0.ChangeGroupCallScreenProfileRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.ChangeGroupCallScreenProfileRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.ChangeGroupCallScreenProfileRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar0.Version

destArgs0.interMode = srcMidVar0.AccessMode

