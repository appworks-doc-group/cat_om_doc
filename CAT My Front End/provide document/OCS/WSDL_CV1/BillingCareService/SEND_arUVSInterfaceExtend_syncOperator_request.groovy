dest.setServiceOperation("arUVSInterfaceExtend","syncOperator")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destArgs1 = dest.payload._args[1]

destArgs0._class = "com.huawei.ngcbs.cm.ocs11ws.core.bo.Ocs11MessageHeader"

destArgs1._class = "com.huawei.ngcbs.cm.operator.io.SyncOperatorRequest"

def srcMessageHeader = srcArgs0.SyncOperator.SyncOperatorRequest.RequestMessage.MessageHeader

def srcMessageBody = srcArgs0.SyncOperator.SyncOperatorRequest.RequestMessage.MessageBody

def srcSessionEntity = srcArgs0.SyncOperator.SessionEntity


destArgs0.loginSystem = srcSessionEntity.userID

destArgs0.password = srcSessionEntity.password


destArgs0.sessionEntity.userID = srcSessionEntity.userID

destArgs0.sessionEntity.password = srcSessionEntity.password

destArgs0.sessionEntity.locale = srcSessionEntity.locale

destArgs0.sessionEntity.loginVia = srcSessionEntity.loginVia

destArgs0.sessionEntity.remoteAddr = srcSessionEntity.remoteAddr

destArgs0.sessionEntity.uploadRoot = srcSessionEntity.uploadRoot

destArgs0.commandId = srcMessageHeader.CommandId

destArgs0.version = srcMessageHeader.Version

destArgs0.transactionId = srcMessageHeader.TransactionId

destArgs0.sequenceId = srcMessageHeader.SequenceId

destArgs0.requestType = srcMessageHeader.RequestType

destArgs0.tenantId = srcMessageHeader.TenantId

destArgs0.language = srcMessageHeader.Language

destArgs0.messageSeq = srcMessageBody.LogID

destArgs0.interMode = srcMessageBody.AccessMethod

destArgs1.operType = srcMessageBody.OperType
destArgs1.operOriginCode = srcMessageBody.OperOriginCode
destArgs1.operNewCode = srcMessageBody.OperNewCode
destArgs1.password = srcMessageBody.Password
destArgs1.adminOperCode = srcMessageBody.AdminOperCode
destArgs1.remark = srcMessageBody.Remark
destArgs1.effDate = srcMessageBody.EffDate
destArgs1.expDate = srcMessageBody.ExpDate
destArgs1.staffCode = srcMessageBody.StaffCode
destArgs1.staffFirstName = srcMessageBody.StaffFirstName
destArgs1.staffLastName = srcMessageBody.StaffLastName
destArgs1.deptID = srcMessageBody.DeptID
destArgs1.oldPassword = srcMessageBody.OldPassword
