def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping2

def listMapping1

listMapping1 = 
{
    src,dest  ->

		dest.FreeUnitType = src.freeUnitType

		dest.FreeUnitTypeName = src.freeUnitTypeName

		dest.MeasureUnit = src.measureUnit

		dest.MeasureUnitName = src.measureUnitName

		dest.TotalAvailableAmount = src.totalAvailableAmount

		dest.TotalInitialAmount = src.totalInitialAmount

}

def listMapping3

listMapping3 = 
{
    src,dest  ->

		dest.BalanceType = src.balanceType

		dest.BalanceTypeName = src.balanceTypeName

		dest.CurrencyID = src.currencyID

		dest.TotalAvailableAmount = src.totalAvailableAmount
	
}

def listMapping7

listMapping7 =
{
    src,dest  ->

	dest.Code = src.code

	dest.Value = src.value

}

def destMidVar = destReturn.QueryTransferLimitResultMsg.QueryTransferLimitResult

destMidVar.TransferLimit.DailyTimesLimit = srcReturn.queryTransferLimitResultInfo.transferLimit.dailyTimesLimit

destMidVar.TransferLimit.UsedDailyTimesLimit = srcReturn.queryTransferLimitResultInfo.transferLimit.usedDailyTimesLimit

destMidVar.TransferLimit.DailyAmountLimit = srcReturn.queryTransferLimitResultInfo.transferLimit.dailyAmountLimit

destMidVar.TransferLimit.UsedDailyAmountLimit = srcReturn.queryTransferLimitResultInfo.transferLimit.usedDailyAmountLimit

destMidVar.TransferLimit.MonthlyTimesLimit = srcReturn.queryTransferLimitResultInfo.transferLimit.monthlyTimesLimit

destMidVar.TransferLimit.UsedMonthlyTimesLimit = srcReturn.queryTransferLimitResultInfo.transferLimit.usedMonthlyTimesLimit

destMidVar.TransferLimit.MonthlyAmountLimit = srcReturn.queryTransferLimitResultInfo.transferLimit.monthlyAmountLimit

destMidVar.TransferLimit.UsedMonthlyAmountLimit = srcReturn.queryTransferLimitResultInfo.transferLimit.usedMonthlyAmountLimit

mappingList(srcReturn.queryTransferLimitResultInfo.freeUnitItemList,destMidVar.FreeUnitItem,listMapping1)

mappingList(srcReturn.queryTransferLimitResultInfo.balanceItemList,destMidVar.BalanceItem,listMapping3)


def destMidVar0 = destReturn.QueryTransferLimitResultMsg.ResultHeader

def srcMidVar1 = srcReturn.resultHeader

destMidVar0.MsgLanguageCode = srcMidVar1.msgLanguageCode

destMidVar0.ResultCode = srcMidVar1.resultCode

destMidVar0.ResultDesc = srcMidVar1.resultDesc

destMidVar0.Version = srcMidVar1.version

destMidVar0.MessageSeq = srcMidVar1.messageSeq

mappingList(srcMidVar1.simpleProperty,destMidVar0.AdditionalProperty,listMapping7)
