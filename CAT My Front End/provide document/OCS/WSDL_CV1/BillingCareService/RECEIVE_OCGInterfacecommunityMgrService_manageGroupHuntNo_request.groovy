def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destMidVar = destArgs0.ManageGroupHuntNoRequestMsg.RequestHeader

def srcMidVar = srcArgs0.omRequestHeader

destMidVar.additionInfo = srcMidVar.belToAreaId

destMidVar.CommandId = srcMidVar.commandId

destMidVar.currentCell = srcMidVar.currentCell

destMidVar.InterFrom = srcMidVar.interFrom

destMidVar.InterMedi = srcMidVar.interMedi

destMidVar.InterMode = srcMidVar.interMode

destMidVar.OperatorID = srcMidVar.operatorId

destMidVar.PartnerID = srcMidVar.partnerId

destMidVar.PartnerOperID = srcMidVar.partnerOperId

destMidVar.Remark = srcMidVar.remark

destMidVar.RequestType = srcMidVar.requestType

destMidVar.SequenceId = srcMidVar.sequenceId

destMidVar.SerialNo = srcMidVar.serialNo

def destMidVar0 = destArgs0.ManageGroupHuntNoRequestMsg.RequestHeader.SessionEntity

def srcMidVar0 = srcArgs0.omRequestHeader.sessionEntity

destMidVar0.Name = srcMidVar0.name

destMidVar0.Password = srcMidVar0.password

destMidVar0.RemoteAddress = srcMidVar0.remoteAddress

destMidVar.TenantID = srcMidVar.tenantId

destMidVar.ThirdPartyID = srcMidVar.thirdPartyId

destMidVar.TradePartnerID = srcMidVar.tradePartnerId

destMidVar.Version = srcMidVar.version

destMidVar.visitArea = srcMidVar.visitArea

destMidVar.TransactionId = srcMidVar.transactionId

def destMidVar1 = destArgs0.ManageGroupHuntNoRequestMsg.ManageGroupHuntNoRequest

destMidVar1.EffectiveTime = srcArgs0.effectiveTime

destMidVar1.ExpireTime = srcArgs0.expireTime

destMidVar1.GroupNumber = srcArgs0.groupNumber

destMidVar1.HuntingMainNumber = srcArgs0.huntingMainNumber

destMidVar1.HuntingNumber = srcArgs0.huntingNumber

destMidVar1.OperationType = srcArgs0.operationType

destMidVar1.Priority = srcArgs0.priority

destMidVar1.UserState = srcArgs0.userState
