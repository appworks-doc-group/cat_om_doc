import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturnData = src.payload._return
def destReturnDoc = dest.payload._return

def destHeader = destReturnDoc.ChangePrimaryOfferResultMsg.ResultHeader
def srcHeader = srcReturnData.resultHeader

destHeader.CommandId = srcHeader.commandId
destHeader.ResultCode = srcHeader.resultCode
destHeader.ResultDesc = srcHeader.resultDesc
destHeader.SequenceId = srcHeader.sequenceId
destHeader.Version = srcHeader.version
destHeader.TransactionId = srcHeader.transactionId
destHeader.OperationTime = srcHeader.operationTime
destHeader.OrderId = srcHeader.orderId

def srcBusinessData = srcReturnData.resultBody
srcBusinessData._class = "com.huawei.ngcbs.cm.ocs33ws.subscriber.changeprimaryoffer.io.OCS33ChangePrimaryOfferResult"
def destReturnData = destReturnDoc.ChangePrimaryOfferResultMsg.ChangePrimaryOfferResult

def listProdPropsMapping = 
{
	src,dest  ->
	
	dest.Id = src.id
	dest.Value = src.value
}

def listProdOrderMapping = 
{
  src,dest  ->

	dest.OfferId = src.offerId
	dest.OfferOrderKey = src.offerOrderKey		
	dest.EffectiveDate = src.effectiveDate
	dest.ExpireDate = src.expireDate
	dest.AutoType = src.autoType
	dest.OfferCode = src.offerCode
	mappingList(src.properties, dest.SimpleProperty, listProdPropsMapping)
}

mappingList(srcBusinessData.prodOrderInfoList, destReturnData.OfferOrderInfo, listProdOrderMapping)
