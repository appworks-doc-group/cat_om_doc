dest.setServiceOperation("BMQueryService","queryOfferingInfo")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destArgs1 = dest.payload._args[1]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.value = src.Value
	
	dest.code = src.Code
	
}

def srcMidVar = srcArgs0.QueryOfferingInfoRequestMsg.RequestHeader

addingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.innerInterMode = srcMidVar.AccessMode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.businessCode = srcMidVar.BusinessCode

def srcMidVar0 = srcArgs0.QueryOfferingInfoRequestMsg.RequestHeader.OperatorInfo

destArgs0.operatorId = srcMidVar0.OperatorID

destArgs0.channelId = srcMidVar0.ChannelID

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.QueryOfferingInfoRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar1.LoginSystemCode

destArgs0.password = srcMidVar1.Password

destArgs0.remoteAddress = srcMidVar1.RemoteIP

destArgs0.version = srcMidVar.Version

def srcMidVar2 = srcArgs0.QueryOfferingInfoRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.QueryOfferingInfoRequestMsg.RequestHeader.TimeFormat

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.timeType = srcMidVar3.TimeType

def destMidVar = destArgs1.offeringAccessCode

def srcMidVar4 = srcArgs0.QueryOfferingInfoRequestMsg.QueryOfferingInfoRequest.OfferingAccessCode

destMidVar.offeringId = srcMidVar4.OfferingID

destMidVar.offeringCode = srcMidVar4.OfferingCode
