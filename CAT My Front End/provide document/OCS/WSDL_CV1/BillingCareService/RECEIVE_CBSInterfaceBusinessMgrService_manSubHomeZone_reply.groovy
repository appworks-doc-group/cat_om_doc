def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.ManSubHomeZoneResultMsg.ResultHeader

destMidVar.CommandId = srcReturn.commandId

destMidVar.ResultCode = srcReturn.resultCode

destMidVar.ResultDesc = srcReturn.resultDesc

destMidVar.SequenceId = srcReturn.sequenceId

destMidVar.SerialNo = srcReturn.serialNo

destMidVar.TransactionId = srcReturn.transactionId

destMidVar.Version = srcReturn.version
