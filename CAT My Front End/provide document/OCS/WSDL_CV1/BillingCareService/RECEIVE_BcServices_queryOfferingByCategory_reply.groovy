import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 =
        {
            src, dest ->

                dest.Code = src.code

                dest.Value = src.value
        }

def listMapping1

listMapping1 =
        {
            src, dest ->

                dest.OfferingID = src.offeringID

                dest.OfferingName = src.offeringName

                dest.OfferingCode = src.offeringCode

                dest.Description = src.description

                dest.PrimaryFlag = src.primaryFlag

                dest.BundleFlag = src.bundleFlag

                dest.PaymentMode = src.paymentMode

                dest.EffDate = formatDate(src.effDate, Constant4Model.DATE_FORMAT)

                dest.ExpDate = formatDate(src.expDate, Constant4Model.DATE_FORMAT)

                dest.SubscriberEffDate = formatDate(src.subscriberEffDate, Constant4Model.DATE_FORMAT)

                dest.SubscriberExpDate = formatDate(src.subscriberExpDate, Constant4Model.DATE_FORMAT)
        }

def destMidVar = destReturn.QueryOfferingByCategoryResultMsg.ResultHeader

def srcMidVar0 = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar0.msgLanguageCode

destMidVar.ResultCode = srcMidVar0.resultCode

destMidVar.ResultDesc = srcMidVar0.resultDesc

destMidVar.Version = srcMidVar0.version

destMidVar.MessageSeq = srcMidVar0.messageSeq

mappingList(srcMidVar0.simpleProperty, destMidVar.AdditionalProperty, listMapping0)

def destMidVar1 = destReturn.QueryOfferingByCategoryResultMsg.QueryOfferingByCategoryResult

def srcMidVar1 = srcReturn.offeringBasicInfos

mappingList(srcMidVar1.offeringBasicInfos, destMidVar1.OfferingBasicInfos, listMapping1)