import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("BMGroupService","manageGrpCallScreenNo")

def srcReqDocMsg = src.payload._args[0]
def srcReqHeaderDoc = srcReqDocMsg.ManageGrpCallScreenNoRequestMsg.RequestHeader

def destMsgHeader = dest.payload._args[0]
destMsgHeader._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

destMsgHeader.version = srcReqHeaderDoc.Version
destMsgHeader.businessCode = srcReqHeaderDoc.BusinessCode
destMsgHeader.messageSeq = srcReqHeaderDoc.MessageSeq
destMsgHeader.beId = srcReqHeaderDoc.OwnershipInfo.BEID
destMsgHeader.brId = srcReqHeaderDoc.OwnershipInfo.BRID
destMsgHeader.loginSystem = srcReqHeaderDoc.AccessSecurity.LoginSystemCode
destMsgHeader.password = srcReqHeaderDoc.AccessSecurity.Password
destMsgHeader.remoteAddress = srcReqHeaderDoc.AccessSecurity.RemoteIP
destMsgHeader.operatorId = srcReqHeaderDoc.OperatorInfo.OperatorID
destMsgHeader.channelId = srcReqHeaderDoc.OperatorInfo.ChannelID
destMsgHeader.interMode = srcReqHeaderDoc.AccessMode
destMsgHeader.msgLanguageCode = srcReqHeaderDoc.MsgLanguageCode
destMsgHeader.timeType = srcReqHeaderDoc.TimeFormat.TimeType
destMsgHeader.timeZoneId = srcReqHeaderDoc.TimeFormat.TimeZoneID

def listMappingAdditionalProperty
listMappingAdditionalProperty = 
{
    src,dest  ->
	dest.code = src.Code
	dest.value = src.Value
}
mappingList(srcReqHeaderDoc.AdditionalProperty,destMsgHeader.simpleProperty,listMappingAdditionalProperty)

def srcReqDataDoc = srcReqDocMsg.ManageGrpCallScreenNoRequestMsg.ManageGrpCallScreenNoRequest
def destReqData = dest.payload._args[1]
destReqData._class = "com.huawei.ngcbs.cm.group.callscreen.io.ManageGrpCallScreenNoRequest"

def destGroupAccessCode = destReqData.groupAccessCode
destGroupAccessCode.groupKey = srcReqDataDoc.SubGroupAccessCode.SubGroupKey
destGroupAccessCode.groupCode = srcReqDataDoc.SubGroupAccessCode.SubGroupCode

destReqData.callScreenType = srcReqDataDoc.CallScreenType
destReqData.operationType = srcReqDataDoc.OperationType

def listMappingCallScreenNoInfo
listMappingCallScreenNoInfo = 
{
    src,dest  ->
	dest.callScreenNo = src.CallScreenNo
	dest.screenNoType = src.ScreenNoType
	dest.effectiveDate = parseDate(src.EffectiveDate,Constant4Model.DATE_FORMAT)
	dest.expireDate = parseDate(src.ExpireDate,Constant4Model.DATE_FORMAT)
	dest.weekStart = src.WeekStart
	dest.weekStop = src.WeekStop
	dest.timeStart = src.TimeStart
	dest.timeStop = src.TimeStop
	dest.routeNumber = src.RouteNumber
	dest.routingMethod = src.RoutingMethod
	dest.screenClass = src.ScreenClass
	dest.flowType = src.FlowType
	dest.zoneID = src.ZoneID
}
mappingList(srcReqDataDoc.CallScreenNoInfo,destReqData.callScreenNoInfos,listMappingCallScreenNoInfo)
