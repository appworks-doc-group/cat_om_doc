dest.setServiceOperation("CBSInterfaceBusinessMgrService","manSubFamilyNo")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs12ws.subscriber.mansubfamilyno.io.ManSubFamilyNoRequestOcs12Base"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.effectiveDate = src.effectiveDate
	
	dest.expireDate = src.expireDate
	
	dest.familyNo = src.FamilyNo
	
	dest.newFamilyNo = src.NewFamilyNo
	
	dest.subGroupType = src.subGroupType
	
	dest.phoneNoOrder = src.phoneNoOrder
	
}

def srcMidVar = srcArgs0.ManSubFamilyNoRequestMsg.RequestHeader

destArgs0.beId = srcMidVar.TenantId

destArgs0.additionInfo = srcMidVar.additionInfo

destArgs0.belToAreaId = srcMidVar.BelToAreaID

destArgs0.commandId = srcMidVar.CommandId

destArgs0.currentCell = srcMidVar.currentCell

destArgs0.interFrom = srcMidVar.InterFrom

destArgs0.operatorId = srcMidVar.OperatorID

destArgs0.partnerId = srcMidVar.PartnerID

destArgs0.partnerOperId = srcMidVar.PartnerOperID

destArgs0.remark = srcMidVar.Remark

destArgs0.requestType = srcMidVar.RequestType

destArgs0.reserve2 = srcMidVar.Reserve2

destArgs0.reserve3 = srcMidVar.Reserve3

destArgs0.sequenceId = srcMidVar.SequenceId

destArgs0.messageSeq = srcMidVar.SerialNo

destArgs0.businessCode = srcMidVar.CommandId

def srcMidVar0 = srcArgs0.ManSubFamilyNoRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar0.Name

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteAddress

destArgs0.thirdPartyId = srcMidVar.ThirdPartyID

destArgs0.tradePartnerId = srcMidVar.TradePartnerID

destArgs0.transactionId = srcMidVar.TransactionId

destArgs0.version = srcMidVar.Version

destArgs0.visitArea = srcMidVar.visitArea

def srcMidVar1 = srcArgs0.ManSubFamilyNoRequestMsg.ManSubFamilyNoRequest

destArgs1.handlingChargeFlag = srcMidVar1.HandlingChargeFlag

destArgs1.operationType = srcMidVar1.OperationType

destArgs1.subscriberNo = srcMidVar1.SubscriberNo

mappingList(srcMidVar1.FamilyNoInfo,destArgs1.ocs12FamilyNoInfoList,listMapping0)

destArgs0.interMedi = srcMidVar.InterMedi

destArgs0.interMode = srcMidVar.InterMode
