import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.OfferingID = src.oId
	
	dest.PurchaseSeq = src.pSeq
	
}

def destMidVar = destReturn.ChangeSubOfferingResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

mappingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

destMidVar.Version = srcMidVar.version

destReturn.ChangeSubOfferingResultMsg.ChangeSubOfferingResult.ModifyOffering[0].NewEffectiveTime=formatDate(srcReturn.modifyOfferings[0].effDate, Constant4Model.DATE_FORMAT)

destReturn.ChangeSubOfferingResultMsg.ChangeSubOfferingResult.ModifyOffering[0].NewExpirationTime=formatDate(srcReturn.modifyOfferings[0].expDate, Constant4Model.DATE_FORMAT)

def srcMidVar0 = srcReturn.modifyOfferings[0]

def destMidVar0 = destReturn.ChangeSubOfferingResultMsg.ChangeSubOfferingResult.ModifyOffering[0]

listMapping1.call(srcMidVar0.offeringKey,destMidVar0.OfferingKey)
