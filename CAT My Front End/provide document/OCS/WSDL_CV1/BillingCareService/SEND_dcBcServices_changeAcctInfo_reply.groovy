def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	mappingList(src.AdditionalProperty,dest.additionalPropertyList,listMapping1)
	
	dest.msgLanguageCode = src.MsgLanguageCode
	
	dest.resultCode = src.ResultCode
	
	dest.resultDesc = src.ResultDesc
	
	dest.version = src.Version
	
}

def srcMidVar = srcReturn.ChangeAcctInfoResultMsg

listMapping0.call(srcMidVar.ResultHeader,destReturn.resultHeader)

destReturn._class = "com.huawei.ngcbs.arm.dc.ootb.outer.domain.ChangeAcctInfoResult"
