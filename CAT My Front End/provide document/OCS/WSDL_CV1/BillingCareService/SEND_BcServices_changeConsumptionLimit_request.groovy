import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("CustomerService","changeConsumptionLimitWithResult")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.customer.changespdlmt.io.ChangeConsumptionLimitRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.code = src.ParamCode
	
	dest.value = src.ParamValue
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.effDate = parseDate(src.EffectiveTime,Constant4Model.DATE_FORMAT)
	
	dest.expDate = parseDate(src.ExpirationTime,Constant4Model.DATE_FORMAT)
	
	mappingList(src.LimitParam,dest.spdlmtParamInfos,listMapping2)
	
	def destMidVar = dest.spendingLimitInfo
	
	destMidVar.limitType = src.LimitType
	
	def destMidVar0 = dest.spendingLimitInfo.currencyLimit
	
	destMidVar0.limitValue = src.LimitValue
	
	def destMidVar1 = dest.spendingLimitInfo.usageLimit
	
	destMidVar1.limitValue = src.LimitValue
	
	destMidVar1.measureId = src.MesureID
	
	destMidVar1.measureType = src.MesureType
	
	destMidVar.unitType = src.UnitType
	
	destMidVar0.currencyId = src.CurrencyID

	destMidVar.limitControlFlag = src.LimitCtrlAction
	
	destMidVar.spendingLimitCatelog = src.OpType

}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.limitType = src.LimitType

	dest.effectiveTime=parseDate(src.EffectiveTime,Constant4Model.DATE_FORMAT)
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.code = src.ParamCode
	
	dest.value = src.ParamValue
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.code = src.ParamCode
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.newValue = src.NewParamValue
	
	dest.oldValue = src.OldParamValue
	
	dest.code = src.ParamCode
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	def srcMidVar7 = src.LimitParam
	
	mappingList(srcMidVar7.AddLimitParam,dest.addSpdlmtParamInfos,listMapping5)
	
	mappingList(srcMidVar7.DelLimitParam,dest.delSpdlmtParamInfos,listMapping6)
	
	mappingList(srcMidVar7.ModifyLimitParam,dest.setSpdlmtParamInfos,listMapping7)
	
	def destMidVar4 = dest.setSpendingLimitInfo
	
	destMidVar4.limitType = src.LimitType
	
	destMidVar4.newAmount = src.NewAmount
	
	destMidVar4.oldAmount = src.OldAmount

	destMidVar4.limitControlFlag = src.LimitCtrlAction

	dest.effectiveTime= parseDate(src.EffectiveTime,Constant4Model.DATE_FORMAT)
	
}

def srcMidVar = srcArgs0.ChangeConsumptionLimitRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.ChangeConsumptionLimitRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.ChangeConsumptionLimitRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.ChangeConsumptionLimitRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.ChangeConsumptionLimitRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar.Version

def srcMidVar4 = srcArgs0.ChangeConsumptionLimitRequestMsg.ChangeConsumptionLimitRequest

mappingList(srcMidVar4.AddLimit,destArgs1.createSpendingLimitInfos,listMapping1)

def destMidVar2 = destArgs1.custAccessCode

def srcMidVar5 = srcArgs0.ChangeConsumptionLimitRequestMsg.ChangeConsumptionLimitRequest.LimitObj.CustAccessCode

destMidVar2.customerCode = srcMidVar5.CustomerCode

destMidVar2.customerKey = srcMidVar5.CustomerKey

destMidVar2.primaryIdentity = srcMidVar5.PrimaryIdentity

def destMidVar3 = destArgs1.subAccessCode

def srcMidVar6 = srcArgs0.ChangeConsumptionLimitRequestMsg.ChangeConsumptionLimitRequest.LimitObj.SubAccessCode

destMidVar3.primaryIdentity = srcMidVar6.PrimaryIdentity

destMidVar3.subscriberKey = srcMidVar6.SubscriberKey

mappingList(srcMidVar4.DelLimit,destArgs1.deleteSpendingLimitInfos,listMapping3)

mappingList(srcMidVar4.ModifyLimit,destArgs1.modifySpendingLimitInfos,listMapping4)
