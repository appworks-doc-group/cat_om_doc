dest.setServiceOperation("ManPaymentRelationMgrService","queryPayRelbySub");

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs33ws.core.bo.Ocs33MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs33ws.subscriber.querypayrelsub.io.OCS33QueryPayRelBySubRequest"

def srcMidVar = srcArgs0.QueryPayRelbySubRequestMsg.QueryPayRelbySubRequest

destArgs1.subscriberNO = srcMidVar.SubscriberNO

destArgs1.paymentAcctCode = srcMidVar.PaymentAcctCode

def srcMidVar0 = srcArgs0.QueryPayRelbySubRequestMsg.RequestHeader

destArgs0.additionInfo = srcMidVar0.AdditionInfo

destArgs0.belToAreaId = srcMidVar0.BelToAreaID

destArgs0.commandId = srcMidVar0.CommandId

destArgs0.currentCell = srcMidVar0.CurrentCell

destArgs0.interFrom = srcMidVar0.InterFrom

destArgs0.interMedi = srcMidVar0.InterMedi

destArgs0.interMode = srcMidVar0.InterMode

destArgs0.operatorId = srcMidVar0.OperatorID

destArgs0.partnerId = srcMidVar0.PartnerID

destArgs0.partnerOperId = srcMidVar0.PartnerOperID

destArgs0.remark = srcMidVar0.Remark

destArgs0.requestType = srcMidVar0.RequestType

destArgs0.reserve2 = srcMidVar0.Reserve2

destArgs0.reserve3 = srcMidVar0.Reserve3

destArgs0.sequenceId = srcMidVar0.SequenceId

destArgs0.beId = srcMidVar0.TenantId

destArgs0.msgLanguageCode = srcMidVar0.Language

destArgs0.messageSeq = srcMidVar0.SerialNo

destArgs0.thirdPartyId = srcMidVar0.ThirdPartyID

destArgs0.tradePartnerId = srcMidVar0.TradePartnerID

destArgs0.transactionId = srcMidVar0.TransactionId

destArgs0.version = srcMidVar0.Version

destArgs0.visitArea = srcMidVar0.VisitArea

def srcMidVar1 = srcArgs0.QueryPayRelbySubRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar1.Name

destArgs0.password = srcMidVar1.Password

destArgs0.remoteAddress = srcMidVar1.RemoteAddress