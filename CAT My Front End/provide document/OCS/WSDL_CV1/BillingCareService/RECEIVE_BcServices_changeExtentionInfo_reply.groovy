def srcReturn = src.payload._return

def destReturn = dest.payload._return


def listMapping0

listMapping0 = 
{
    src,dest  ->

	def destMidVar = dest.ResultHeader
	
	destMidVar.MsgLanguageCode = src.msgLanguageCode
	
	destMidVar.ResultCode = src.resultCode
	
	destMidVar.ResultDesc = src.resultDesc
	
	destMidVar.Version = src.version

	destMidVar.MessageSeq = src.messageSeq
	
}

listMapping0.call(srcReturn,destReturn.ChangeExtentionInfoResultMsg)