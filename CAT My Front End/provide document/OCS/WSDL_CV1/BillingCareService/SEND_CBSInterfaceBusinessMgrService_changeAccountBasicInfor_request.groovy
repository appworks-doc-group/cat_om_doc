dest.setServiceOperation("CBSInterfaceBusinessMgrService","changeAccountBasicInfor")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs12ws.account.io.ChangeAccountBasicInforRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Id
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.ChangeAccountBasicInforRequestMsg.RequestHeader

destArgs0.beId = srcMidVar.TenantId

destArgs0.additionInfo = srcMidVar.additionInfo

destArgs0.belToAreaId = srcMidVar.BelToAreaID

destArgs0.commandId = srcMidVar.CommandId

destArgs0.currentCell = srcMidVar.currentCell

destArgs0.interFrom = srcMidVar.InterFrom

destArgs0.interMedi = srcMidVar.InterMedi

destArgs0.interMode = srcMidVar.InterMode

destArgs0.operatorId = srcMidVar.OperatorID

destArgs0.partnerId = srcMidVar.PartnerID

destArgs0.partnerOperId = srcMidVar.PartnerOperID

destArgs0.remark = srcMidVar.Remark

destArgs0.requestType = srcMidVar.RequestType

destArgs0.reserve2 = srcMidVar.Reserve2

destArgs0.reserve3 = srcMidVar.Reserve3

destArgs0.sequenceId = srcMidVar.SequenceId

destArgs0.messageSeq = srcMidVar.SerialNo

def srcMidVar0 = srcArgs0.ChangeAccountBasicInforRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar0.Name

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteAddress

destArgs0.thirdPartyId = srcMidVar.ThirdPartyID

destArgs0.tradePartnerId = srcMidVar.TradePartnerID

destArgs0.transactionId = srcMidVar.TransactionId

destArgs0.version = srcMidVar.Version

destArgs0.visitArea = srcMidVar.visitArea

def srcMidVar1 = srcArgs0.ChangeAccountBasicInforRequestMsg.ChangeAccountBasicInforRequest

destArgs1.address = srcMidVar1.Address

destArgs1.custID = srcMidVar1.CustID

destArgs1.name = srcMidVar1.Name

mappingList(srcMidVar1.SimpleProperty,destArgs1.simpleProperty,listMapping0)

destArgs1.title = srcMidVar1.Title

destArgs1.tpKey = srcMidVar1.Code

destArgs1.subscriberNo = srcMidVar1.SubscriberNo
