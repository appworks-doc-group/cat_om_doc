import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("OtherToArServices","queryInvoice")

def srcArgs0 = src.payload._args[0]

def srcArgs1 = src.payload._args[1]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destArgs0.QueryInvoiceRequestMsg.QueryInvoiceRequest.AcctAccessCode

def srcMidVar = srcArgs1.acctAccessCode

destMidVar.AccountCode = srcMidVar.accoutCode

destMidVar.AccountKey = srcMidVar.accoutKey

destMidVar.PayType = srcMidVar.payType

destMidVar.PrimaryIdentity = srcMidVar.primaryIdentity

def destMidVar0 = destArgs0.QueryInvoiceRequestMsg.QueryInvoiceRequest

destMidVar0.BillCycleID = srcArgs1.billCycleId

destMidVar0.NumberOfBillCycle = srcArgs1.numberOfBillCycle

destMidVar0.TimePeriod.EndTime=formatDate(srcArgs1.timePeriod.endTime, "yyyy-MM-dd hh:mm:ss")

destMidVar0.TimePeriod.StartTime=formatDate(srcArgs1.timePeriod.startTime, "yyyy-MM-dd hh:mm:ss")

def destMidVar1 = destArgs0.QueryInvoiceRequestMsg.RequestHeader

destMidVar1.Version = srcArgs0.version

def destMidVar2 = destArgs0.QueryInvoiceRequestMsg.RequestHeader.TimeFormat

destMidVar2.TimeZoneID = srcArgs0.timeZoneId

destMidVar2.TimeType = srcArgs0.timeType

def destMidVar3 = destArgs0.QueryInvoiceRequestMsg.RequestHeader.OwnershipInfo

destMidVar3.BRID = srcArgs0.brId

destMidVar3.BEID = srcArgs0.beId

def destMidVar4 = destArgs0.QueryInvoiceRequestMsg.RequestHeader.OperatorInfo

destMidVar4.OperatorID = srcArgs0.operatorId

destMidVar4.ChannelID = srcArgs0.channelId

destMidVar1.MsgLanguageCode = srcArgs0.msgLanguageCode

destMidVar1.MessageSeq = srcArgs0.messageSeq

destMidVar1.BusinessCode = srcArgs0.businessCode

mappingList(srcArgs0.simpleProperty,destMidVar1.AdditionalProperty,listMapping0)

def destMidVar5 = destArgs0.QueryInvoiceRequestMsg.RequestHeader.AccessSecurity

destMidVar5.LoginSystemCode = srcArgs0.loginSystem

destMidVar5.Password = srcArgs0.password

destMidVar5.RemoteIP = srcArgs0.remoteAddress

destMidVar1.AccessMode = srcArgs0.interMode
