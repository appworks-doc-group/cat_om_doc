dest.setServiceOperation("CommunityMgrService","modifyGroupBasicInfo")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destArgs1 = dest.payload._args[1]

destArgs0._class = "com.huawei.ngcbs.cm.ocs33ws.core.bo.Ocs33MessageHeader"

destArgs1._class = "com.huawei.ngcbs.cm.ocs33ws.community.groupbasicinfo.io.OCS33ModifyGroupBasicInfoRequest"

def destArgs2 = destArgs1.groupAccount


def srcMessageHeader = srcArgs0.ModifyGroupBasicInfoRequestMsg.RequestHeader

def srcMessageBody = srcArgs0.ModifyGroupBasicInfoRequestMsg.ModifyGroupBasicInfoRequest

def srcAcct = srcMessageBody.GroupAccount

destArgs0.commandId = srcMessageHeader.CommandId
destArgs0.version = srcMessageHeader.Version
destArgs0.transactionId = srcMessageHeader.TransactionId
destArgs0.sequenceId = srcMessageHeader.SequenceId
destArgs0.requestType = srcMessageHeader.RequestType
destArgs0.loginSystem = srcMessageHeader.SessionEntity.Name
destArgs0.password = srcMessageHeader.SessionEntity.Password
destArgs0.remoteAddress = srcMessageHeader.SessionEntity.RemoteAddress
destArgs0.interFrom = srcMessageHeader.InterFrom
destArgs0.interMedi = srcMessageHeader.InterMedi
destArgs0.interMode = srcMessageHeader.InterMode
destArgs0.visitArea = srcMessageHeader.visitArea
destArgs0.currentCell = srcMessageHeader.currentCell
destArgs0.additionInfo = srcMessageHeader.additionInfo
destArgs0.thirdPartyId = srcMessageHeader.ThirdPartyID
destArgs0.partnerId = srcMessageHeader.PartnerID
destArgs0.operatorId = srcMessageHeader.OperatorID
destArgs0.tradePartnerId = srcMessageHeader.TradePartnerID
destArgs0.partnerOperId = srcMessageHeader.PartnerOperID
destArgs0.belToAreaId = srcMessageHeader.BelToAreaID
destArgs0.reserve2 = srcMessageHeader.Reserve2
destArgs0.reserve3 = srcMessageHeader.Reserve3
destArgs0.messageSeq = srcMessageHeader.SerialNo
destArgs0.remark = srcMessageHeader.Remark
destArgs0.msgLanguageCode  = srcMessageHeader.Language
destArgs0.performanceStatCmd = "ChangeGroupBasicInfo"

destArgs2.firstName = srcAcct.FirstName
destArgs2.middleName = srcAcct.MiddleName
destArgs2.lastName = srcAcct.LastName
destArgs2.accountID = srcAcct.AccountID
destArgs2.accountCode = srcAcct.AccountCode
destArgs2.paidMode = srcAcct.PaidMode
destArgs2.paymentMethod = srcAcct.PaymentMethod
destArgs2.billFlag = srcAcct.BillFlag
destArgs2.title = srcAcct.Title
destArgs2.billAddress1 = srcAcct.BillAddress1
destArgs2.billAddress2 = srcAcct.BillAddress2
destArgs2.billAddress3 = srcAcct.BillAddress3
destArgs2.billAddress4 = srcAcct.BillAddress4
destArgs2.billAddress5 = srcAcct.BillAddress5
destArgs2.zipCode = srcAcct.ZipCode
destArgs2.billLang = srcAcct.BillLang
destArgs2.emailBillAddr = srcAcct.EmailBillAddr
destArgs2.sMSBillLang = srcAcct.SMSBillLang
destArgs2.sMSBillAddr = srcAcct.SMSBillAddr
destArgs2.bankAcctNo = srcAcct.BankAcctNo
destArgs2.bankID = srcAcct.BankID
destArgs2.bankName = srcAcct.BankName
destArgs2.bankAcctName = srcAcct.BankAcctName
destArgs2.bankAccType = srcAcct.BankAccType
destArgs2.bankAcctActiveDate = srcAcct.BankAcctActiveDate
destArgs2.cardExpiryDate = srcAcct.CardExpiryDate
destArgs2.SFID = srcAcct.SFID
destArgs2.SPID = srcAcct.SPID
destArgs2.cCGroup = srcAcct.CCGroup
destArgs2.cCSubGroup = srcAcct.CCSubGroup
destArgs2.vATNumber = srcAcct.VATNumber
destArgs2.printVATNo = srcAcct.PrintVATNo
destArgs2.dueDate = srcAcct.DueDate
destArgs2.pPSAcctInitBal = srcAcct.PPSAcctInitBal
destArgs2.pPSAcctCredit = srcAcct.PPSAcctCredit
destArgs2.pOSAcctInitBal = srcAcct.POSAcctInitBal
destArgs2.pOSAcctCredit = srcAcct.POSAcctCredit
destArgs2.contactTel = srcAcct.ContactTel
destArgs2.dCCallForward = srcAcct.DCCallForward
destArgs2.billCycleCredit = srcAcct.BillCycleCredit
destArgs2.creditCtrlMode = srcAcct.CreditCtrlMode
destArgs2.staffID = srcAcct.StaffID

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.id = src.Id
	
	dest.value = src.Value
	
}
mappingList(srcAcct.SimpleProperty,destArgs2.simplePropertys,listMapping0)

destArgs1.groupNumber = srcMessageBody.GroupNumber

def  destArgs3 = destArgs1.groupInfo
def  srcgroup = srcMessageBody.GroupBasicInfo

destArgs3.groupType=srcgroup.GroupType
destArgs3.registerNo=srcgroup.RegisterNo
destArgs3.maxCountOfCug=srcgroup.MaxCountOfCug
destArgs3.maxHuntingList=srcgroup.MaxHuntingList
destArgs3.belToAreaID=srcgroup.BelToAreaID
destArgs3.maxMemNumber=srcgroup.MaxMemNumber
destArgs3.groupName=srcgroup.GroupName
destArgs3.custLevel=srcgroup.CustLevel

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.id = src.Id
	
	dest.value = src.Value
	
}
mappingList(srcgroup.SimpleProperty,destArgs3.simplePropertys,listMapping1)

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.ratingGroup = src.RatingGroup
	
}
mappingList(srcgroup.RatingGroupList,destArgs3.ratingGroupList,listMapping2)

def listMapping3

listMapping2 = 
{
    src,dest  ->
	
	dest.callingCellId = src.CallingCellId
	
}
mappingList(srcgroup.OfficeZoneInfo,destArgs3.officeZoneInfo,listMapping3)





