import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMappingNum

listMappingNum = 
{
    src,dest  ->

	dest.ServiceNum = src.serviceNum
	
	dest.ExpireTime = formatDate(src.expireTime, Constant4Model.DATE_FORMAT)
	
}

def destMidVar = destReturn.QueryExpireSubToMicroResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

mappingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

destMidVar.Version = srcMidVar.version

destMidVar.MessageSeq = srcMidVar.messageSeq


def destMidResultVar = destReturn.QueryExpireSubToMicroResultMsg.QueryExpireSubToMicroResult

def srcMidResultVar = srcReturn.queryExpireSubToMicroResultInfo


def destMidPagingVar = destMidResultVar.PagingInfo

def srcMidPagingVar = srcMidResultVar.pagingInfo

destMidPagingVar.TotalRowNum = srcMidPagingVar.totalRowNum

destMidPagingVar.BeginRowNum = srcMidPagingVar.beginRowNum

destMidPagingVar.FetchRowNum = srcMidPagingVar.fetchRowNum


mappingList(srcMidResultVar.expireSubscriberList,destMidResultVar.ExpireSubscriber,listMappingNum)

