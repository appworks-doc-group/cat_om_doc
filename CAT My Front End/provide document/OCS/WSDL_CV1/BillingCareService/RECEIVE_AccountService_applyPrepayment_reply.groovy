import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.CycleClass = src.cycleClass
	
	dest.InitialAmount = src.initialAmount
	
	dest.Amount = src.amount
	
	dest.CurrencyID = src.currencyID
	
	dest.CycleDueDate=formatDate(src.cycleDueDate, Constant4Model.DATE_FORMAT)
	
	dest.CycleSequence = src.cycleSequence
	
	dest.DelayFlag = src.delayFlag
	
	dest.RealRepayDate=formatDate(src.realRepayDate, Constant4Model.DATE_FORMAT)
	
	dest.Status = src.status
	
}

def destMidVar = destReturn.ApplyPrepaymentResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.Version = srcMidVar.version

mappingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

def destMidVar0 = destReturn.ApplyPrepaymentResultMsg.ApplyPrepaymentResult

def srcMidVar0 = srcReturn.applyPrepaymentInfo

destMidVar0.ContractID = srcMidVar0.contractID

destMidVar0.CurrencyID = srcMidVar0.currencyID

mappingList(srcMidVar0.inatallmentDetailList,destMidVar0.InatallmentDetail,listMapping1)

destMidVar0.InstallmentInstID = srcMidVar0.installmentInstID

def destMidVar1 = destReturn.ApplyPrepaymentResultMsg.ApplyPrepaymentResult.OfferingKey

def srcMidVar1 = srcReturn.applyPrepaymentInfo.offeringKey

destMidVar1.OfferingID = srcMidVar1.oId

destMidVar1.PurchaseSeq = srcMidVar1.pSeq

destMidVar0.TotalAmount = srcMidVar0.totalAmount

destMidVar0.TotalCycle = srcMidVar0.totalCycle
