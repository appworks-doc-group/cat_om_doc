def srcArgs0 = src.payload._args[0]

def srcArgs1 = src.payload._args[1]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Name = src.paraName
	
	dest.Value = src.paraValue
	
}

def destMidVar = destArgs0.NotifyDelCUGMemberRequest.RequestHeader.ExtParamList

mappingList(srcArgs0.extParamList,destMidVar.ParameterInfo,listMapping0)

def destMidVar0 = destArgs0.NotifyDelCUGMemberRequest.RequestHeader

destMidVar0.ChannelId = srcArgs0.channelId

destMidVar0.BEId = srcArgs0.beId

destMidVar0.AccessUser = srcArgs0.accessUser

destMidVar0.AccessPwd = srcArgs0.accessPwd

destMidVar0.Language = srcArgs0.language

destMidVar0.OperatorId = srcArgs0.operatorId

destMidVar0.OperatorPwd = srcArgs0.operatorPwd

destMidVar0.ProcessTime = srcArgs0.processTime

destMidVar0.SessionId = srcArgs0.sessionId

destMidVar0.TestFlag = srcArgs0.testFlag

destMidVar0.TimeType = srcArgs0.timeType

destMidVar0.TimeZoneID = srcArgs0.timeZoneId

destMidVar0.Version = srcArgs0.version

destMidVar0.TransactionId = srcArgs0.transactionId

def destMidVar1 = destArgs0.NotifyDelCUGMemberRequest

destMidVar1.BusiSeq = srcArgs1.busiSeq

destMidVar1.ResultCode = srcArgs1.resultCode

destMidVar1.ResultDesc = srcArgs1.resultDesc
