def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.MsgLanguageCode = src.msgLanguageCode
	
	dest.ResultCode = src.resultCode
	
	dest.ResultDesc = src.resultDesc
	
	dest.Version = src.version
	
	mappingList(src.simpleProperty,dest.AdditionalProperty,listMapping1)
	
}

def destMidVar = destReturn.ChangeRechageBlacklistResultMsg

listMapping0.call(srcReturn,destMidVar.ResultHeader)
