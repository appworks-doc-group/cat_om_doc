import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 =
        {
            src, dest ->

                dest.CycleSequence = src.cycleSequence

                dest.InitialAmount = src.initialAmount

                dest.Amount = src.amount

                dest.CurrencyID = src.currencyID

                dest.CycleClass = src.cycleClass

                dest.Status = src.status

                dest.CycleDueDate = formatDate(src.cycleDueDate, Constant4Model.DATE_FORMAT)

                dest.RealRepayDate = formatDate(src.realRepayDate, Constant4Model.DATE_FORMAT)

                dest.DelayFlag = src.delayFlag

        }

def listMapping1

listMapping1 =
        {
            src, dest ->

                dest.Code = src.code

                dest.Value = src.value

        }

def listMapping2

listMapping2 =
        {
            src, dest ->

                dest.ContractID = src.contractID

                dest.InstallmentInstID = src.installmentInstID

                mappingList(src.installmentDetailList, dest.InatallmentDetail, listMapping0)

        }

def destMidVar = destReturn.CancelMultiInstallmentResultMsg.CancelMultiInstallmentResult

mappingList(srcReturn.cancelInstallmentList, destMidVar.Installment, listMapping2)

def destMidVar1 = destReturn.CancelMultiInstallmentResultMsg.ResultHeader

def srcMidVar1 = srcReturn.resultHeader

destMidVar1.MsgLanguageCode = srcMidVar1.msgLanguageCode

destMidVar1.ResultCode = srcMidVar1.resultCode

destMidVar1.ResultDesc = srcMidVar1.resultDesc

destMidVar1.Version = srcMidVar1.version

destMidVar1.MessageSeq = srcMidVar1.messageSeq

mappingList(srcMidVar1.simpleProperty, destMidVar1.AdditionalProperty, listMapping1)
