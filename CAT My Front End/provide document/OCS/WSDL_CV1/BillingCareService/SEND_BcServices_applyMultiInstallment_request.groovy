dest.setServiceOperation("AccountService","applyMultiInstallment")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.account.applymultiinstallment.io.ApplyMultiInstallmentRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	mappingList(src.AdditionalProperty,dest.simplePropertyList,listMapping5)
	
	dest.chargeCode = src.ChargeCode
	
	dest.contractID = src.ContractID
	
	dest.currencyID = src.CurrencyID
	
	dest.cycleLength = src.CycleLength
	
	dest.cycleRefDate = src.CycleRefDate
	
	dest.cycleType = src.CycleType
	
	def srcMidVar6 = src.InatallmentPlan
	
	dest.finalCycleAmount = srcMidVar6.FinalCycleAmount
	
	dest.firstCycleAmount = srcMidVar6.FirstCycleAmount
	
	dest.planType = srcMidVar6.PlanType
	
	dest.totalAmount = src.TotalAmount
	
	dest.totalCycle = src.TotalCycle

	dest.delayCycle = src.DelayCycle
}

def srcMidVar = srcArgs0.ApplyMultiInstallmentRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.ApplyMultiInstallmentRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.ApplyMultiInstallmentRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.ApplyMultiInstallmentRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.ApplyMultiInstallmentRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar.Version

def srcMidVar4 = srcArgs0.ApplyMultiInstallmentRequestMsg.ApplyMultiInstallmentRequest.ApplyObj

def destMidVar = destArgs1.anyAccessCode

listMapping1.call(srcMidVar4.CustAccessCode,destMidVar.custAccessCode)

listMapping2.call(srcMidVar4.SubAccessCode,destMidVar.subAccessCode)

listMapping3.call(srcMidVar4.SubGroupAccessCode,destMidVar.groupAccessCode)

def srcMidVar5 = srcArgs0.ApplyMultiInstallmentRequestMsg.ApplyMultiInstallmentRequest

mappingList(srcMidVar5.ApplyInatallmentDetail,destArgs1.applyInstallmentRequestList,listMapping4)

destArgs0.messageSeq = srcMidVar.MessageSeq
