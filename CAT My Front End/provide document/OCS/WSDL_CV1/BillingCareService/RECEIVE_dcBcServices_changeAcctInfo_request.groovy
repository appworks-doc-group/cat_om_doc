def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.LoginSystemCode = src.loginSystemCode
	
	dest.Password = src.password
	
	dest.RemoteIP = src.remoteIP
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.ChannelID = src.channelId
	
	dest.OperatorID = src.operatorId
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.BEID = src.beId
	
	dest.BRID = src.brId
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.TimeType = src.timeType
	
	dest.TimeZoneID = src.timeZoneID
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	listMapping1.call(src.accessSecurity,dest.AccessSecurity)
	
	mappingList(src.additionalPropertyList,dest.AdditionalProperty,listMapping2)
	
	dest.BusinessCode = src.businessCode
	
	dest.MessageSeq = src.messageSeq
	
	dest.MsgLanguageCode = src.msgLanguageCode
	
	listMapping3.call(src.operatorInfo,dest.OperatorInfo)
	
	listMapping4.call(src.ownershipInfo,dest.OwnershipInfo)
	
	listMapping5.call(src.timeFormat,dest.TimeFormat)
	
	dest.Version = src.version
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.AccountCode = src.accountCode
	
	dest.AccountKey = src.accountKey
	
	dest.PrimaryIdentity = src.primaryIdentity
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destArgs0.ChangeAcctInfoRequestMsg

listMapping0.call(srcArgs0.requestHeader,destMidVar.RequestHeader)

def destMidVar0 = destArgs0.ChangeAcctInfoRequestMsg.ChangeAcctInfoRequest

listMapping6.call(srcArgs0.acctAccessCode,destMidVar0.AcctAccessCode)

def destMidVar1 = destArgs0.ChangeAcctInfoRequestMsg.ChangeAcctInfoRequest.AcctBasicInfo

addingList(srcArgs0.acctProperty,destMidVar1.AcctProperty,listMapping7)