def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 =
{
    src,dest  ->

        dest.paraName = src.Name

		dest.paraValue = src.Value

}

def listMapping0

listMapping0 =
{
    src,dest  ->

	dest.accessPwd = src.AccessPwd

	dest.accessUser = src.AccessUser

	def destMidVar = dest.extParamList[0]

	def srcMidVar0 = src.ExtParamList.ParameterInfo[0]

	destMidVar.paraName = srcMidVar0.Name

	destMidVar.paraValue = srcMidVar0.Value

	dest.language = src.Language

	dest.operatorId = src.OperatorId

	dest.processTime = src.ProcessTime

	dest.sessionId = src.SessionId

	dest.testFlag = src.TestFlag

	dest.timeType = src.TimeType

	dest.timeZoneId = src.TimeZoneID

	dest.transactionId = src.TransactionId

	dest.version = src.Version

	dest.beId = src.BEId

	dest.channelId = src.ChannelId

	def srcMidVar1 = src.ExtParamList

	mappingList(srcMidVar1.ParameterInfo,dest.extParamList,listMapping1)

	dest.operatorPwd = src.OperatorPwd

}

def listMapping2

listMapping2 =
{
    src,dest  ->

	dest.paraName = src.Name

	dest.paraValue = src.Value

}

def srcMidVar = srcReturn.BindImeiResult.ResponseHeader

destReturn.resultCode = srcMidVar.RetCode

destReturn.resultDesc = srcMidVar.RetMsg

listMapping0.call(srcMidVar.RequestHeader,destReturn.reverseHeader)

def srcMidVar2 = srcReturn.BindImeiResult.ResponseHeader.ExtParamList

mappingList(srcMidVar2.ParameterInfo,destReturn.paramInfos,listMapping2)

destReturn._class = "com.huawei.ngcbs.cm.reverse.ws.ReverseResponseHeader"
