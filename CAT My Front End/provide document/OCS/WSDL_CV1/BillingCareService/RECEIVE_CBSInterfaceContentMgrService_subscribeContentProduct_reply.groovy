import com.huawei.ngcbs.bm.common.common.Constant4Model
def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.SubscribeContentProductResultMsg.ResultHeader

def srcReturnHeader = srcReturn.resultHeader

destMidVar.TransactionId = srcReturnHeader.transactionId

destMidVar.SequenceId = srcReturnHeader.sequenceId

destMidVar.ResultCode = srcReturnHeader.resultCode

destMidVar.ResultDesc = srcReturnHeader.resultDesc

destMidVar.Version = srcReturnHeader.version

destMidVar.CommandId = srcReturnHeader.commandId

def listMapping0

listMapping0 =
        {
            src, dest ->
                dest.ProductID = src.productID
                dest.ProductOrderKey = src.productOrderKey
                dest.EffectiveDate = formatDate(src.effectiveDate, Constant4Model.DATE_FORMAT)
                dest.ExpireDate = formatDate(src.expireDate, Constant4Model.DATE_FORMAT)
                dest.AutoType = src.autoType
        }



def destMidVar1 = destReturn.SubscribeContentProductResultMsg.SubscribeContentProductResult

destMidVar1.SubscriberNo = srcReturn.subscriberNo

mappingList(srcReturn.productOrderInfoList, destMidVar1.ProductOrderInfo, listMapping0)


