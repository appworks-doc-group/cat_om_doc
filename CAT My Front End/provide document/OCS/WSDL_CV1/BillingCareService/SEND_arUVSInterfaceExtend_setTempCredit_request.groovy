import com.huawei.ngcbs.bm.common.common.Constant4Model;

dest.setServiceOperation("arUVSInterfaceExtend","setTempCredit");

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs11ws.core.bo.Ocs11MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs11ws.io.settempcredit.SetTempCreditRequestOcs12Base"

def srcMidVar = srcArgs0.SetTempCredit.SetTempCreditRequest.RequestMessage.MessageHeader

def srcMidVar1 = srcArgs0.SetTempCredit.SetTempCreditRequest.RequestMessage.MessageBody

destArgs0.requestType = srcMidVar.RequestType

destArgs0.beId = srcMidVar.TenantId

destArgs0.sequenceId = srcMidVar.SequenceId

destArgs0.transactionId = srcMidVar.TransactionId

destArgs0.version = srcMidVar.Version

destArgs0.commandId = srcMidVar.CommandId

destArgs0.msgLanguageCode = srcMidVar.Language

destArgs1.acctId = srcMidVar1.AcctId

destArgs1.acctCode = srcMidVar1.AcctCode

destArgs1.subId = srcMidVar1.SubId

destArgs1.msisn = srcMidVar1.Msisdn

destArgs1.operType = srcMidVar1.OperType

destArgs1.applyTime = srcMidVar1.ApplyTime

destArgs1.expiryTime = srcMidVar1.ExpiryTime

destArgs1.amount.value = srcMidVar1.Amount.Value

destArgs1.amount.minMeasureId = srcMidVar1.Amount.MinMeasureId

def srcSessionEntity = srcArgs0.SetTempCredit.SessionEntity

destArgs0.sessionEntity.userID = srcSessionEntity.userID

destArgs0.sessionEntity.password = srcSessionEntity.password

destArgs0.sessionEntity.locale = srcSessionEntity.locale

destArgs0.sessionEntity.loginVia = srcSessionEntity.loginVia

destArgs0.sessionEntity.remoteAddr = srcSessionEntity.remoteAddr

destArgs0.sessionEntity.uploadRoot = srcSessionEntity.uploadRoot

destArgs0.loginSystem = srcSessionEntity.userID

destArgs0.password = srcSessionEntity.password

