dest.setServiceOperation("BMGroupService","batchCreateGroup")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.group.batch.creategroup.io.BatchCreateGroupRequest"

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar0 = srcArgs0.BatchCreateGroupRequestMsg.BatchCreateGroupRequest

destArgs1.requestFileName = srcMidVar0.FileName

def srcMidVar1 = srcArgs0.BatchCreateGroupRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar1.LoginSystemCode

destArgs0.password = srcMidVar1.Password

destArgs0.remoteAddress = srcMidVar1.RemoteIP

def srcMidVar2 = srcArgs0.BatchCreateGroupRequestMsg.RequestHeader

mappingList(srcMidVar2.AdditionalProperty,destArgs0.simpleProperty,listMapping1)

destArgs0.messageSeq = srcMidVar2.MessageSeq

destArgs0.businessCode = srcMidVar2.BusinessCode

def srcMidVar3 = srcArgs0.BatchCreateGroupRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar3.ChannelID

destArgs0.operatorId = srcMidVar3.OperatorID

def srcMidVar4 = srcArgs0.BatchCreateGroupRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar4.BEID

destArgs0.brId = srcMidVar4.BRID

def srcMidVar5 = srcArgs0.BatchCreateGroupRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar5.TimeType

destArgs0.timeZoneId = srcMidVar5.TimeZoneID

destArgs0.version = srcMidVar2.Version

destArgs0.interMode = srcMidVar2.AccessMode

destArgs0.msgLanguageCode = srcMidVar2.MsgLanguageCode
