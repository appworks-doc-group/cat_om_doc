def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.HandlePortingMessageResponse.Body.Result

destMidVar.Status = srcReturn.status

def destMidVar0 = destReturn.HandlePortingMessageResponse.Body.Result.Fault

def srcMidVar = srcReturn.resultHeader

destMidVar0.Code = srcMidVar.resultCode

destMidVar0.Reason = srcMidVar.resultDesc

def destMidVar1 = destReturn.HandlePortingMessageResponse.Header.MessageHeader.From

destMidVar1.Location = srcReturn.fromLocation

def destMidVar2 = destReturn.HandlePortingMessageResponse.Header.MessageHeader.From.PartyId

destMidVar2.type = srcReturn.fromType

destMidVar2."#text" = srcReturn.fromPartyId

def destMidVar3 = destReturn.HandlePortingMessageResponse.Header.MessageHeader.ConnectionInfo

destMidVar3.CPAId = srcReturn.capId

destMidVar3.ExternalRefToMessageId = srcReturn.extendMessageId

def destMidVar4 = destReturn.HandlePortingMessageResponse.Header.MessageHeader.To

destMidVar4.Location = srcReturn.toLocation

def destMidVar5 = destReturn.HandlePortingMessageResponse.Header.MessageHeader.To.PartyId

destMidVar5.type = srcReturn.toType

destMidVar5."#text" = srcReturn.toPartyId

def destMidVar6 = destReturn.HandlePortingMessageResponse.Header.MessageHeader.Service

destMidVar6.Version = srcMidVar.version

destMidVar6.Name = srcReturn.name

destMidVar6.Paradigm = srcReturn.paraDigm

def destMidVar7 = destReturn.HandlePortingMessageResponse.Header.MessageHeader.HeaderFields

destMidVar7.MessageId = srcReturn.messageId

destMidVar7.Timestamp = srcReturn.time
