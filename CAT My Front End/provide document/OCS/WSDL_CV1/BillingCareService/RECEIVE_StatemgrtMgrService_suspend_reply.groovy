def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.SuspendResultMsg.ResultHeader

destMidVar.CommandId = srcReturn.commandId

destMidVar.TransactionId = srcReturn.transactionId

destMidVar.ResultCode = srcReturn.resultCode

destMidVar.Version = srcReturn.version

destMidVar.ResultDesc = srcReturn.resultDesc

destMidVar.SequenceId = srcReturn.sequenceId

destMidVar.OperationTime = srcReturn.operationTime

destMidVar.OrderId = srcReturn.orderId