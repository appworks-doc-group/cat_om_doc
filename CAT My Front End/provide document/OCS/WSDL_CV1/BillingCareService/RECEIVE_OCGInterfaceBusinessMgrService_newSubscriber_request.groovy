def srcArgs0 = src.payload._args[0]

def srcArgs1 = src.payload._args[1]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Value = src.value
	
	dest.Id = src.code
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Name = src.name
	
	dest.Password = src.password
	
	dest.RemoteAddress = src.remoteAddress
	
}

def destMidVar = destArgs0.NewSubscriberRequestMsg.NewSubscriberRequest.Subscriber

destMidVar.Brandid = srcArgs1.brandId


destMidVar.IMSI = srcArgs1.imsi

destMidVar.Lang = srcArgs1.lang

destMidVar.MainProductID = srcArgs1.mainProductId

destMidVar.PaidMode = srcArgs1.paidMode

def destMidVar1 = destArgs0.NewSubscriberRequestMsg.NewSubscriberRequest

destMidVar1.SDUID = srcArgs1.sduId

mappingList(srcArgs1.simplePropertyList,destMidVar.SimpleProperty,listMapping0)

destMidVar.SMSLang = srcArgs1.smsLang

destMidVar1.SubscriberNo = srcArgs1.subscriberNo

destMidVar.USSDLang = srcArgs1.ussdLang

destMidVar1.CBPID = srcArgs1.cbpId

def destMidVar2 = destArgs0.NewSubscriberRequestMsg.RequestHeader

destMidVar2.BelToAreaID = srcArgs0.belToAreaId

destMidVar2.CommandId = srcArgs0.commandId

destMidVar2.currentCell = srcArgs0.currentCell

destMidVar2.InterFrom = srcArgs0.interFrom

destMidVar2.InterMedi = srcArgs0.interMedi

destMidVar2.InterMode = srcArgs0.interMode

destMidVar2.OperatorID = srcArgs0.operatorId

destMidVar2.PartnerID = srcArgs0.partnerId

destMidVar2.PartnerOperID = srcArgs0.partnerOperId

destMidVar2.Remark = srcArgs0.remark

destMidVar2.RequestType = srcArgs0.requestType

destMidVar2.SequenceId = srcArgs0.sequenceId

destMidVar2.SerialNo = srcArgs0.serialNo

listMapping1.call(srcArgs0.sessionEntity,destMidVar2.SessionEntity)

destMidVar2.TenantID = srcArgs0.tenantId

destMidVar2.ThirdPartyID = srcArgs0.thirdPartyId

destMidVar2.TradePartnerID = srcArgs0.tradePartnerId

destMidVar2.TransactionId = srcArgs0.transactionId

destMidVar2.Version = srcArgs0.version

destMidVar2.visitArea = srcArgs0.visitArea
