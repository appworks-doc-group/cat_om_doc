import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturnData = src.payload._return
def destReturnDoc = dest.payload._return

def destHeader = destReturnDoc.ChangeOptionalOfferExtendResultMsg.ResultHeader
def srcHeader = srcReturnData.resultHeader

destHeader.CommandId = srcHeader.commandId
destHeader.ResultCode = srcHeader.resultCode
destHeader.ResultDesc = srcHeader.resultDesc
destHeader.SequenceId = srcHeader.sequenceId
destHeader.Version = srcHeader.version
destHeader.TransactionId = srcHeader.transactionId
destHeader.OperationTime = srcHeader.operationTime
destHeader.OrderId = srcHeader.orderId

def srcBusinessData = srcReturnData.resultBody
srcBusinessData._class = "com.huawei.ngcbs.cm.ocs33ws.offer.chgopofr.io.OCS33ChangeOptionalOfferExtendResult"
def destReturnData = destReturnDoc.ChangeOptionalOfferExtendResultMsg.ChangeOptionalOfferExtendResult

def listProdPropsMapping = 
{
	src,dest  ->
	
	dest.Id = src.id
	dest.Value = src.value
}

def listMapping0 = 
{
	src,dest  ->
	
	dest.OfferKey = src.offerKey
	dest.ExtOfferCode = src.extOfferCode
	dest.ExtOfferOrderCode = src.extOfferOrderCode
}

def listProdOrderMapping = 
{
  src,dest  ->

	listMapping0.call(src.offerOrderIdentify,dest.OfferOrderIdentity)
	dest.OfferCode = src.offerCode
	dest.OfferName = src.offerName
	dest.OfferOrderKey = src.offerOrderKey	
	dest.EffectiveDate = src.effectiveDate
	dest.ExpireDate = src.expireDate
	dest.AutoType = src.autoType
	dest.Status = src.status
	mappingList(src.simplePropertys, dest.SimpleProperty, listProdPropsMapping)
}

mappingList(srcBusinessData.offerOrderInfos, destReturnData.OfferOrderInfo, listProdOrderMapping)
