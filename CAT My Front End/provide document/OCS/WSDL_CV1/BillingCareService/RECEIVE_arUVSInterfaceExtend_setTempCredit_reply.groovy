def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar4 = destReturn.SetTempCreditResponse.SetTempCreditResult.ResultMessage

def destMidVar0 = destMidVar4.MessageHeader

def srcMidVar =  srcReturn.ocs11ResultHeader

destMidVar0.CommandId = srcMidVar.commandId

destMidVar0.Version = srcMidVar.version

destMidVar0.TransactionId = srcMidVar.transactionId

destMidVar0.SequenceId = srcMidVar.sequenceId

destMidVar0.ResultCode = srcMidVar.resultCode

destMidVar0.ResultDesc = srcMidVar.resultDesc

destMidVar0.Language = srcMidVar.language

destMidVar0.TenantId = srcMidVar.tenantId




