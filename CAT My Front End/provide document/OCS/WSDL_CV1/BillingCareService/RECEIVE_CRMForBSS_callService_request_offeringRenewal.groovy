dest.setServiceOperation("CRMForBSS","offeringRenewal")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Name = src.paraName
	
	dest.Value = src.paraValue
	
}

def destMidVar = destArgs0.OfferingRenewalRequest.OfferingRenewal

destMidVar.Msisdn = srcArgs0.msisdn

destMidVar.OfferingID = srcArgs0.offeringID

destMidVar.OldProductOrderKey = srcArgs0.oldProductOrderKey

destMidVar.ProductOrderKey = srcArgs0.productOrderKey

destMidVar.SubscriberKey = srcArgs0.subscriberKey

def destMidVar0 = destArgs0.OfferingRenewalRequest.RequestHeader

def srcMidVar = srcArgs0.workOrderHeader

destMidVar0.AccessPwd = srcMidVar.accessPwd

destMidVar0.AccessUser = srcMidVar.accessUser

destMidVar0.BEId = srcMidVar.beId

destMidVar0.ChannelId = srcMidVar.channelId

def destMidVar1 = destArgs0.OfferingRenewalRequest.RequestHeader.ExtParamList

mappingList(srcMidVar.extParamList,destMidVar1.ParameterInfo,listMapping0)

destMidVar0.Language = srcMidVar.language

destMidVar0.OperatorId = srcMidVar.operatorId

destMidVar0.OperatorPwd = srcMidVar.operatorPwd

destMidVar0.ProcessTime = srcMidVar.processTime

destMidVar0.SessionId = srcMidVar.sessionId

destMidVar0.TestFlag = srcMidVar.testFlag

destMidVar0.TimeType = srcMidVar.timeType

destMidVar0.TimeZoneID = srcMidVar.timeZoneId

destMidVar0.Version = srcMidVar.version

destMidVar0.TransactionId = srcMidVar.transactionId
