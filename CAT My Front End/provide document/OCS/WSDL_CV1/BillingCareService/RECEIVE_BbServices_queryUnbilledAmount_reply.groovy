import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.MsgLanguageCode = src.msgLanguageCode
	
	dest.ResultCode = src.resultCode
	
	dest.ResultDesc = src.resultDesc
	
	dest.Version = src.version

	dest.MessageSeq = src.messageSeq
	
	mappingList(src.simpleProperty,dest.AdditionalProperty,listMapping1)
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.SubCategory = src.subCategory
	
	mappingList(src.additionalProperty,dest.AdditionalProperty,listMapping5)
	
	dest.CurrencyID = src.currencyId
	
	dest.ChargeAmount = src.chargeAmount
	
	dest.OfferingID = src.offeringId
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	mappingList(src.propList,dest.PropList,listMapping7)
	
}

def listMapping9

listMapping9 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping8

listMapping8 = 
{
    src,dest  ->

	mappingList(src.additionalProperty,dest.AdditionalProperty,listMapping9)
	
	dest.ChargeAmount = src.chargeAmount
	
	dest.CurrencyID = src.currencyId
	
	dest.OfferingID = src.offeringId
	
}

def listMapping11

listMapping11 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping10

listMapping10 = 
{
    src,dest  ->

	mappingList(src.additionalProperty,dest.AdditionalProperty,listMapping11)
	
	dest.ChargeAmount = src.chargeAmount
	
	dest.CurrencyID = src.currencyId
	
	dest.OfferingID = src.offeringId
	
}

def listMapping13

listMapping13 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping12

listMapping12 = 
{
    src,dest  ->

	mappingList(src.additionalProperty,dest.AdditionalProperty,listMapping13)
	
	dest.ChargeAmount = src.chargeAmount
	
	dest.ChargeCode = src.chargeCode
	
	dest.CurrencyID = src.currencyId
	
	dest.SubCategory = src.subCategory
	
	dest.ChargeTime=formatDate(src.chargeTime, Constant4Model.DATE_FORMAT)
	
}

def listMapping15

listMapping15 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping14

listMapping14 = 
{
    src,dest  ->

	mappingList(src.additionalProperty,dest.AdditionalProperty,listMapping15)
	
	dest.ChargeAmount = src.chargeAmount
	
	dest.CurrencyID = src.currencyId
	
	dest.OfferingID = src.offeringId
	
}

def listMapping17

listMapping17 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping16

listMapping16 = 
{
    src,dest  ->

	mappingList(src.additionalProperty,dest.AdditionalProperty,listMapping17)
	
	dest.CurrencyID = src.currencyId
	
	dest.TaxAmount = src.taxAmount
	
	dest.TaxCode = src.taxCode
	
}

def listMapping19

listMapping19 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping18

listMapping18 = 
{
    src,dest  ->

	dest.ActualVolume = src.actualVolume
	
	mappingList(src.additionalProperty,dest.AdditionalProperty,listMapping19)
	
	dest.ChargeAmount = src.chargeAmount
	
	dest.ChargeCode = src.chargeCode
	
	dest.CurrencyID = src.currencyId
	
	dest.FreeVolume = src.freeVolume
	
	dest.MeasureUnit = src.measureUnit
	
	dest.RatingDiscVolume = src.ratingDiscVolume
	
	dest.RatingVolume = src.ratingVolume
	
	dest.SubCategory = src.subCategory
	
}

def listMapping21

listMapping21 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping20

listMapping20 = 
{
    src,dest  ->

	mappingList(src.additionalProperty,dest.AdditionalProperty,listMapping21)
	
	dest.AdjustAmount = src.adjustAmount
	
	dest.CurrencyID = src.currencyId
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	listMapping4.call(src.billMediumCharge,dest.BillMediumCharge)
	
	listMapping6.call(src.customizedCharge,dest.CustomizedCharge)
	
	listMapping8.call(src.discount,dest.Discount)
	
	listMapping10.call(src.minCommitmentCharge,dest.MinCommitmentCharge)
	
	listMapping12.call(src.nRecurringCharge,dest.NRecurringCharge)
	
	listMapping14.call(src.recurringCharge,dest.RecurringCharge)
	
	listMapping16.call(src.tax,dest.Tax)
	
	listMapping18.call(src.usageCharge,dest.UsageCharge)
	
	listMapping20.call(src.adjustmentInfo,dest.Adjustment)
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.AcctKey = src.acctKey
	
	dest.BillChargeID = src.billChargeId
	
	dest.BillCycleID = src.billCycleId
	
	dest.Category = src.category
	
	dest.CustKey = src.custKey
	
	dest.ObjKey = src.objKey
	
	dest.ObjType = src.objType
	
	dest.PrimaryIdentity = src.primaryIdentity
	
	listMapping3.call(src.unbilledInfo,dest.UnbilledInfo)
	
}

def destMidVar = destReturn.QueryUnbilledAmountResultMsg

listMapping0.call(srcReturn.resultHeader,destMidVar.ResultHeader)

def srcMidVar = srcReturn.resultBody

def destMidVar0 = destReturn.QueryUnbilledAmountResultMsg.QueryUnbilledAmountResult

mappingList(srcMidVar.unbilledChargeList,destMidVar0.UnbilledAmountList,listMapping2)
