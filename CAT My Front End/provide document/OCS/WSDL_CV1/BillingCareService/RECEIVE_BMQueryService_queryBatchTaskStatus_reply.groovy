def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.FailedNumber = src.failedNumber
	
	dest.ProcessedNumber = src.processedNumber
	
	dest.ResultCode = src.resultCode
	
	dest.ResultDesc = src.resultDesc
	
	dest.Status = src.status
	
	dest.SuccessNumber = src.successNumber
	
	dest.TotalNumber = src.totalNumber
	
	dest.ValidNumber = src.validNumber
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destReturn.QueryBatchTaskStatusResultMsg

listMapping0.call(srcReturn.resultBody,destMidVar.QueryBatchTaskStatusResult)

def destMidVar0 = destReturn.QueryBatchTaskStatusResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar0.MsgLanguageCode = srcMidVar.msgLanguageCode

mappingList(srcMidVar.simpleProperty,destMidVar0.AdditionalProperty,listMapping1)

destMidVar0.ResultDesc = srcMidVar.resultDesc

destMidVar0.ResultCode = srcMidVar.resultCode

destMidVar0.Version = srcMidVar.version
