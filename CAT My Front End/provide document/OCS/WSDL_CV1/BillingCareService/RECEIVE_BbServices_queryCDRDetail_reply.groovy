import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.Amount = src.amount
	
	dest.CurAmount = src.curAmount
	
	dest.CurExpireTime = formatDate(src.curExpireTime,Constant4Model.DATE_FORMAT)
	
	dest.FreeUnitId = src.freeUnitId
	
	dest.FreeUnitType = src.freeUnitType
	
	dest.MeasureUnit = src.measureUnit
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.TaxAmt = src.taxAmt
	
	dest.TaxCode = src.taxCode
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.ChargeAmount = src.chargeAmount
	
	dest.ChargeCode = src.chargeCodeId
	
	dest.ChargeCodeName = src.chargeCodeName
	
	dest.CurExpireTime = formatDate(src.curExpireTime,Constant4Model.DATE_FORMAT)
	
	dest.CurrencyID = src.currencyId
	
	dest.CurrentAmount = src.currentAmount
	
	dest.OfferingID = src.offeringId
	
	dest.PayAcctKey = src.payAcctKey
	
	dest.PayObjClass = src.payObjClass
	
	dest.PayObjID = src.payObjId
	
	dest.PayObjType = src.payObjType
	
	dest.PlanID = src.planId
	
	mappingList(src.tax,dest.Tax,listMapping6)
	
}

def srcMidVar = srcReturn.queryCDRDetailResultInfo.additionalProperty

def destMidVar = destReturn.QueryCDRDetailResultMsg.QueryCDRDetailResult.CDRInfo

mappingList(srcMidVar.simplePropertyList,destMidVar.AdditionalProperty,listMapping0)

def srcMidVar0 = srcReturn.queryCDRDetailResultInfo

destMidVar.CdrSeq = srcMidVar0.cdrSeq

def srcMidVar1 = srcReturn.queryCDRDetailResultInfo.volumeInfo

def destMidVar0 = destReturn.QueryCDRDetailResultMsg.QueryCDRDetailResult.CDRInfo.VolumeInfo

mappingList(srcMidVar1.freeUnitList,destMidVar0.FreeUnitList,listMapping3)

def destMidVar1 = destReturn.QueryCDRDetailResultMsg.ResultHeader

def srcMidVar2 = srcReturn.resultHeader

destMidVar1.MsgLanguageCode = srcMidVar2.msgLanguageCode

destMidVar1.ResultCode = srcMidVar2.resultCode

destMidVar1.ResultDesc = srcMidVar2.resultDesc

destMidVar1.MessageSeq = srcMidVar2.messageSeq

mappingList(srcMidVar2.simpleProperty,destMidVar1.AdditionalProperty,listMapping4)

destMidVar1.Version = srcMidVar2.version

mappingList(srcMidVar0.chargeDetail,destMidVar.ChargeDetail,listMapping5)
