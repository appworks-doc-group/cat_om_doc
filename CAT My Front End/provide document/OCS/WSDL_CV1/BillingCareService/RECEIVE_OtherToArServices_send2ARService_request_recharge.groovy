import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("OtherToArServices","recharge")

def srcArgs0 = src.payload._args[0]

def srcArgs1 = src.payload._args[1]

def destArgs0 = dest.payload._args[0]

def destMidVar = destArgs0.RechargeRequestMsg.RequestHeader

destMidVar.Version = srcArgs0.version

destMidVar.MessageSeq = srcArgs0.messageSeq

destMidVar.BusinessCode = srcArgs0.businessCode

destMidVar.AccessMode = srcArgs0.interMode

def destMidVar0 = destArgs0.RechargeRequestMsg.RequestHeader.OwnershipInfo

destMidVar0.BEID = srcArgs0.beId

destMidVar0.BRID = srcArgs0.brId

def listMapping0

listMapping0 = 
{
    src0,dest0  ->

	dest0.Code = src0.code
	
	dest0.Value = src0.value
	
}

addingList(srcArgs0.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

def destMidVar1 = destArgs0.RechargeRequestMsg.RequestHeader.AccessSecurity

destMidVar1.LoginSystemCode = srcArgs0.loginSystem

destMidVar1.Password = srcArgs0.password

destMidVar1.RemoteIP = srcArgs0.remoteAddress

def destMidVar2 = destArgs0.RechargeRequestMsg.RequestHeader.OperatorInfo

destMidVar2.OperatorID = srcArgs0.operatorId

destMidVar2.ChannelID = srcArgs0.channelId

destMidVar.MsgLanguageCode = srcArgs0.msgLanguageCode

def destMidVar3 = destArgs0.RechargeRequestMsg.RequestHeader.TimeFormat

destMidVar3.TimeZoneID = srcArgs0.timeZoneId

destMidVar3.TimeType = srcArgs0.timeType

def destMidVar4 = destArgs0.RechargeRequestMsg.RechargeRequest.RechargeObj.SubAccessCode

def srcMidVar = srcArgs1.subAccessCode

destMidVar4.PrimaryIdentity = srcMidVar.primaryIdentity

destMidVar4.SubscriberKey = srcMidVar.subscriberKey

def destMidVar5 = destArgs0.RechargeRequestMsg.RechargeRequest.RechargeObj.AcctAccessCode

def srcMidVar0 = srcArgs1.acctAccessCode

destMidVar5.PrimaryIdentity = srcMidVar0.primaryIdentity

destMidVar5.PayType = srcMidVar0.payType

destMidVar5.AccountKey = srcMidVar0.accoutKey

destMidVar5.AccountCode = srcMidVar0.accoutCode

def destMidVar6 = destArgs0.RechargeRequestMsg.RechargeRequest

destMidVar6.RechargeSerialNo = srcArgs1.rechargeSerialNo

destMidVar6.RechargeType = srcArgs1.rechargeType

def listMapping1

listMapping1 = 
{
    src1,dest1  ->

	dest1.PaymentMethod = src1.paymentMethod
	
	dest1.Amount = src1.amount
	
	dest1.BankInfo = src1.bankInfo
	
	def destMidVar8 = dest1.BankInfo
	
	def srcMidVar1 = src1.bankInfo
	
	destMidVar8.BankBranchCode = srcMidVar1.bankBranchCode
	
	destMidVar8.AcctType = srcMidVar1.acctType
	
	destMidVar8.AcctName = srcMidVar1.acctName
	
	destMidVar8.CreditCardType = srcMidVar1.creditCardType
	
	destMidVar8.ExpDate = formatDate(srcMidVar1.expDate, "yyyy-MM-dd hh:mm:ss")
	
	destMidVar8.CVVNumber = srcMidVar1.cvvNumber
	
	destMidVar8.BankCode = srcMidVar1.bankCode
	
	destMidVar8.AcctNo = srcMidVar1.acctno
	
	destMidVar8.CheckNo = srcMidVar1.checkNo
	
}

def destMidVar7 = destArgs0.RechargeRequestMsg.RechargeRequest.RechargeInfo

addingList(srcArgs1.cashPaymentInfos,destMidVar7.CashPayment,listMapping1)

destMidVar7.CardPayment = srcArgs1.cardPayment

def destMidVar9 = destArgs0.RechargeRequestMsg.RechargeRequest.RechargeInfo.CardPayment

def srcMidVar2 = srcArgs1.cardPayment

destMidVar9.CardPinNumber = srcMidVar2.cardPinNumber

destMidVar9.CardSequence = srcMidVar2.cardSequence

destMidVar6.CurrencyID = srcArgs1.currencyId

destMidVar6.RechargeChannelID = srcArgs1.rechargeChannelId

def listMapping2

listMapping2 = 
{
    src2,dest2  ->

	dest2.Value = src2.value
	
	dest2.Code = src2.code
	
}

addingList(srcArgs1.additionalProperty,destMidVar6.AdditionalProperty,listMapping2)
