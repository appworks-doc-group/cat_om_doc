import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.NewEffectiveTime=formatDate(src.effDate, Constant4Model.DATE_FORMAT)
	
	dest.NewExpirationTime=formatDate(src.expDate, Constant4Model.DATE_FORMAT)
	
	def destMidVar0 = dest.OfferingKey
	
	def srcMidVar = src.offeringKey
	
	destMidVar0.OfferingID = srcMidVar.oId
	
	destMidVar0.PurchaseSeq = srcMidVar.pSeq
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	mappingList(src.modifyOfferingInfos,dest.ModifyOffering,listMapping3)
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.MsgLanguageCode = src.msgLanguageCode
	
	dest.ResultCode = src.resultCode
	
	dest.ResultDesc = src.resultDesc
	
	dest.Version = src.version
	
	mappingList(src.simpleProperty,dest.AdditionalProperty,listMapping2)
	
}

def destMidVar = destReturn.ChangeAccountOfferingResultMsg

listMapping0.call(srcReturn.resultBody,destMidVar.ChangeAccountOfferingResult)

listMapping1.call(srcReturn.resultHeader,destMidVar.ResultHeader)
