dest.setServiceOperation("AccountService","cancelMultiInstallment")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.account.cancelmultiinstallment.io.CancelMultiInstallmentRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar0 = srcArgs0.CancelMultiInstallmentRequestMsg.CancelMultiInstallmentRequest.CancelObj

listMapping0.call(srcMidVar0.CustAccessCode,destArgs1.custAccessCode)

listMapping1.call(srcMidVar0.SubAccessCode,destArgs1.subAccessCode)

listMapping2.call(srcMidVar0.SubGroupAccessCode,destArgs1.groupAccessCode)

def srcMidVar1 = srcArgs0.CancelMultiInstallmentRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar1.AccessMode

def srcMidVar2 = srcArgs0.CancelMultiInstallmentRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar2.LoginSystemCode

destArgs0.password = srcMidVar2.Password

destArgs0.remoteAddress = srcMidVar2.RemoteIP

mappingList(srcMidVar1.AdditionalProperty,destArgs0.simpleProperty,listMapping3)

destArgs0.businessCode = srcMidVar1.BusinessCode

destArgs0.messageSeq = srcMidVar1.MessageSeq

destArgs0.msgLanguageCode = srcMidVar1.MsgLanguageCode

def srcMidVar3 = srcArgs0.CancelMultiInstallmentRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar3.ChannelID

destArgs0.operatorId = srcMidVar3.OperatorID

def srcMidVar4 = srcArgs0.CancelMultiInstallmentRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar4.BEID

destArgs0.brId = srcMidVar4.BRID

def srcMidVar5 = srcArgs0.CancelMultiInstallmentRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar5.TimeType

destArgs0.timeZoneId = srcMidVar5.TimeZoneID

destArgs0.version = srcMidVar1.Version


def listMapping4

listMapping4 =
		{
			src,dest  ->

				dest.contractID = src.ContractID

		}

def srcMidVar6 = srcArgs0.CancelMultiInstallmentRequestMsg.CancelMultiInstallmentRequest

mappingList(srcMidVar6.CancelInstallment,destArgs1.cancelInstallmentList,listMapping4)