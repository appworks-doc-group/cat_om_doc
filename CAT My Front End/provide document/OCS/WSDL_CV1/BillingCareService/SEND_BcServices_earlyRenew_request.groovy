dest.setServiceOperation("SubscriberService","earlyRenew")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.earlyrenew.EarlyRenewRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping1

listMapping1 =
{
	src,dest  ->

		dest.oCode = src.OfferingCode

		dest.oId = src.OfferingID

		dest.pSeq = src.PurchaseSeq

}

def listMapping2

listMapping2 =
{
	src,dest  ->

		dest.primaryIdentity = src.PrimaryIdentity

		dest.subscriberKey = src.SubscriberKey

}

def listMapping3

listMapping3 =
{
    src,dest  ->
	
	def destMidVar1 = dest.offeringKey
	
	destMidVar1._class = "com.huawei.ngcbs.bm.domain.entity.offering.io.OfferingKeyInfo"
	
	listMapping1.call(src.OfferingKey,destMidVar1)

}

def listMapping4

listMapping4 =
{
	src,dest  ->

		dest.code = src.Code

		dest.value = src.Value

}

def srcMidVar = srcArgs0.EarlyRenewRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.EarlyRenewRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.EarlyRenewRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.EarlyRenewRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.ChangeSubOfferingRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar.Version

def srcMidVar4 = srcArgs0.EarlyRenewRequestMsg.EarlyRenewRequest

listMapping2.call(srcMidVar4.SubAccessCode,destArgs1.subAccessCode)


mappingList(srcMidVar4.EarlyRenewOffering,destArgs1.earlyRenewOffering,listMapping3)

def srcMidVar5 = srcArgs0.EarlyRenewRequestMsg.EarlyRenewRequest.AdditionalProperty

mappingList(srcMidVar5,destArgs1.simplePropertyList,listMapping4)
