def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.batch.changesubvalidity.io.BatchChangeSubValidityRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.value = src.Value
	
	dest.code = src.Code
	
}


def srcMidVar = srcArgs0.BatchChangeSubValidityRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar.LoginSystemCode

destArgs0.password = srcMidVar.Password

destArgs0.remoteAddress = srcMidVar.RemoteIP

def srcMidVar0 = srcArgs0.BatchChangeSubValidityRequestMsg.RequestHeader

mappingList(srcMidVar0.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar0.BusinessCode

destArgs0.messageSeq = srcMidVar0.MessageSeq

destArgs0.msgLanguageCode = srcMidVar0.MsgLanguageCode

def srcMidVar1 = srcArgs0.BatchChangeSubValidityRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.BatchChangeSubValidityRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.BatchChangeSubValidityRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar0.Version

def destMidVar = destArgs1.batchOperationInfo

def srcMidVar4 = srcArgs0.BatchChangeSubValidityRequestMsg.BatchChangeSubValidityRequest

destMidVar.requestFileName = srcMidVar4.FileName

def destMidVar0 = destArgs1.changeSubValidityInfo.changeSubValidityInfo

destMidVar0.opType = srcMidVar4.OpType

destMidVar0.validityIncrememt = srcMidVar4.ValidityIncrement

destArgs1.requestFileName = srcMidVar4.FileName

destArgs0.interMode = srcMidVar0.AccessMode
