import com.huawei.ngcbs.bm.common.common.Constant4Model
dest.setServiceOperation("CustomerService","custDeactivation")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.customer.deactivecustomer.io.CustDeactivationRequest"

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	mappingList(src.AdditionalProperty,dest.simpleProperty,listMapping1)
	
	dest.version = src.Version
	
	def srcMidVar0 = src.TimeFormat
	
	dest.timeType = srcMidVar0.TimeType
	
	dest.timeZoneId = srcMidVar0.TimeZoneID
	
	def srcMidVar1 = src.OwnershipInfo
	
	dest.beId = srcMidVar1.BEID
	
	dest.brId = srcMidVar1.BRID
	
	def srcMidVar2 = src.OperatorInfo
	
	dest.channelId = srcMidVar2.ChannelID
	
	dest.operatorId = srcMidVar2.OperatorID
	
	dest.messageSeq = src.MessageSeq
	
	dest.msgLanguageCode = src.MsgLanguageCode
	
	dest.businessCode = src.BusinessCode
	
	def srcMidVar3 = src.AccessSecurity
	
	dest.remoteAddress = srcMidVar3.RemoteIP
	
	dest.password = srcMidVar3.Password
	
	dest.loginSystem = srcMidVar3.LoginSystemCode
	
	dest.interMode = src.AccessMode
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	mappingList(src.AdditionalProperty,dest.additionalProperty,listMapping3)
	
	listMapping4.call(src.CustAccessCode,dest.custAccessCode)
	
	dest.opType = src.OpType
		dest.effectiveTime = parseDate(src.EffectiveTime,Constant4Model.DATE_FORMAT)
	
}

def srcMidVar = srcArgs0.CustDeactivationRequestMsg

listMapping0.call(srcMidVar.RequestHeader,destArgs0)

listMapping2.call(srcMidVar.CustDeactivationRequest,destArgs1)
