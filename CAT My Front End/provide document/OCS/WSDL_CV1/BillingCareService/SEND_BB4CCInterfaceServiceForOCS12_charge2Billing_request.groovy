dest.setServiceOperation("OCS12BB4CCInterfaceService", "charge2Billing")

def srcArgs0 = src.payload._args[0]
def srcMidVar0 = srcArgs0.Charge2BillingRequestMsg.RequestHeader
def srcMidVar2 = srcArgs0.Charge2BillingRequestMsg.Charge2BillingRequest

def destArgs0 = dest.payload._args[0]
def destArgs1 = dest.payload._args[1]

destArgs0._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.BB4CCMessageHeader"
destArgs1._class = "com.huawei.ngcbs.cm.ocs12ws.charge2billing.io.OCS12Charge2BillingRequest"


def listMapping0
listMapping0 = 
{
    src,dest  ->
    
	dest.name = src.name
	
	dest.value = src.value
}

def listMapping2
listMapping2 = 
{
    src,dest  ->

	dest.itemId = src.ItemId

	dest.amount = src.Amount
	

}

def listMapping1
listMapping1 = 
{
    src,dest  ->
    	
	dest.acctId = src.AcctId	
	
	dest.subId = src.SubId
	
	dest.msisdn = src.Msisdn
	
	mappingList(src.ItemList.ItemValue, dest.itemList, listMapping2)

}

destArgs0.messageSeq = srcMidVar0.SerialNo
destArgs0.serialNo = srcMidVar0.SerialNo
destArgs0.requestFrom = srcMidVar0.RequestFrom
destArgs0.operatorId = srcMidVar0.OperatorId
destArgs0.departmentId = srcMidVar0.DepartmentId
destArgs0.version = srcMidVar0.Version
destArgs0.remark = srcMidVar0.Remark
mappingList(srcMidVar0.Param, destArgs0.param, listMapping0)
destArgs0.beId = srcMidVar0.TenantId
destArgs0.msgLanguageCode = srcMidVar0.Language
destArgs0.password = srcMidVar0.Password
destArgs0.loginSystem = srcMidVar0.OperatorId

destArgs1.busiType = srcMidVar2.BusiType
destArgs1.bsno = srcMidVar2.Bsno
destArgs1.channelId = srcMidVar2.ChannelId
destArgs1.remark = srcMidVar2.Remark


mappingList(srcMidVar2.SubFeeList.SubFeeValue, destArgs1.subFeeList, listMapping1)
