import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("BMGroupService","changeGroupInterPayBlocking")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.group.changegrouppayinterblocking.io.ChangeGroupInterPayBlockingRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	listMapping1.call(src.SubGroupAccessCode,dest.groupAccessCode)
	
	def srcMidVar7 = src.EffectiveTime
	
	dest.effMode = srcMidVar7.Mode
	
	dest.effDate = parseDate(srcMidVar7.Time, Constant4Model.DATE_FORMAT)
	
	dest.expDate = parseDate(src.ExpDate, Constant4Model.DATE_FORMAT)
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	listMapping1.call(src.SubGroupAccessCode,dest.groupAccessCode)

}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	mappingList(src.AddPayBlockingGroup,dest.addPayBlockingGroups,listMapping3)
	
    mappingList(src.DelPayBlockingGroup,dest.delPayBlockingGroups,listMapping4)
}

def srcMidVar = srcArgs0.ChangeGroupInterPayBlockingRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.ChangeGroupInterPayBlockingRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.ChangeGroupInterPayBlockingRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.ChangeGroupInterPayBlockingRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.ChangeGroupInterPayBlockingRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar.Version

def srcMidVar4 = srcArgs0.ChangeGroupInterPayBlockingRequestMsg.ChangeGroupInterPayBlockingRequest

listMapping1.call(srcMidVar4.SubGroupAccessCode,destArgs1.groupAccessCode)

listMapping2.call(srcMidVar4.PayBlockingGroup,destArgs1.payBlockingGroup)