def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.paraName = src.Name
	
	dest.paraValue = src.Value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.accessPwd = src.AccessPwd
	
	dest.accessUser = src.AccessUser
	
	dest.beId = src.BEId
	
	dest.channelId = src.ChannelId
	
	dest.language = src.Language
	
	dest.operatorId = src.OperatorId
	
	dest.operatorPwd = src.OperatorPwd
	
	dest.processTime = src.ProcessTime
	
	dest.sessionId = src.SessionId
	
	dest.testFlag = src.TestFlag
	
	dest.timeType = src.TimeType
	
	dest.timeZoneId = src.TimeZoneID
	
	dest.transactionId = src.TransactionId
	
	dest.version = src.Version
	
	def srcMidVar0 = src.ExtParamList
	
	mappingList(srcMidVar0.ParameterInfo,dest.extParamList,listMapping1)
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.paraName = src.Name
	
	dest.paraValue = src.Value
	
}

def srcMidVar = srcReturn.CreditLimitNotifyResult.ResponseHeader

listMapping0.call(srcMidVar.RequestHeader,destReturn.reverseHeader)

destReturn.resultCode = srcMidVar.RetCode

destReturn.resultDesc = srcMidVar.RetMsg

def srcMidVar1 = srcReturn.CreditLimitNotifyResult.ResponseHeader.ExtParamList

mappingList(srcMidVar1.ParameterInfo,destReturn.paramInfos,listMapping2)

destReturn._class = "com.huawei.ngcbs.cm.reverse.vpncreditnotify.io.CreditLimitNotifyResult"
