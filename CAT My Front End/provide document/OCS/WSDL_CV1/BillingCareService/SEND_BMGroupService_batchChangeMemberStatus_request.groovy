def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.group.batch.changememberstatus.io.BatchChangeMemberStatusRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.BatchChangeMemberStatusRequestMsg.BatchChangeMemberStatusRequest

destArgs1.requestFileName = srcMidVar.FileName

def destMidVar = destArgs1.changeGroupMemberStatusRequest.changeMemberStatusInfo

destMidVar.newStatus = srcMidVar.NewStatus

destMidVar.oldStatus = srcMidVar.OldStatus

destMidVar.opType = srcMidVar.OpType

def destMidVar0 = destArgs1.changeGroupMemberStatusRequest

listMapping0.call(srcMidVar.SubGroupAccessCode,destMidVar0.groupAccessCode)

def srcMidVar0 = srcArgs0.BatchChangeMemberStatusRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

def srcMidVar1 = srcArgs0.BatchChangeMemberStatusRequestMsg.RequestHeader

mappingList(srcMidVar1.AdditionalProperty,destArgs0.simpleProperty,listMapping1)

destArgs0.businessCode = srcMidVar1.BusinessCode

destArgs0.messageSeq = srcMidVar1.MessageSeq

destArgs0.msgLanguageCode = srcMidVar1.MsgLanguageCode

def srcMidVar2 = srcArgs0.BatchChangeMemberStatusRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar2.ChannelID

destArgs0.operatorId = srcMidVar2.OperatorID

def srcMidVar3 = srcArgs0.BatchChangeMemberStatusRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar3.BEID

destArgs0.brId = srcMidVar3.BRID

def srcMidVar4 = srcArgs0.BatchChangeMemberStatusRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar4.TimeType

destArgs0.timeZoneId = srcMidVar4.TimeZoneID

destArgs0.version = srcMidVar1.Version

destArgs0.interMode = srcMidVar1.AccessMode
