import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping4

listMapping4 =
{
	src,dest  ->

	dest.balanceId = src.BalanceID

	dest.balanceType = src.BalanceType

	dest.balanceTypeName = src.BalanceTypeName

	dest.currencyId = src.CurrencyID

	dest.newBalanceAmt = src.NewBalanceAmt

	dest.oldBalanceAmt = src.OldBalanceAmt

}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.taxAmount = src.TaxAmount
	
	dest.taxCode = src.TaxCode
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.applyAmount = src.ApplyAmount
	
	dest.applyType = src.ApplyType
	
	dest.chargeAmount = src.ChargeAmount
	
	dest.chargeCode = src.ChargeCode
	
	dest.discountAmount = src.DiscountAmount
	
	dest.invoiceDetailId = src.InvoiceDetailID
	
	dest.invoiceId = src.InvoiceID
	
	dest.invoiceNo = src.InvoiceNo
	
	mappingList(src.TaxList,dest.taxList,listMapping3)
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	def destMidVar1 = dest.cardInfo
	
	def srcMidVar1 = src.CardInfo
	
	destMidVar1.cardPinNumber = srcMidVar1.CardPinNumber
	
	destMidVar1.cardSequence = srcMidVar1.CardSequence
	
	dest.accessMode = src.AccessMode
	
	dest.acctKey = src.AcctKey
	
	dest.amount = src.Amount
	
	def destMidVar2 = dest.bankInfo
	
	def srcMidVar2 = src.BankInfo
	
	destMidVar2.acctName = srcMidVar2.AcctName
	
	destMidVar2.acctno = srcMidVar2.AcctNo
	
	destMidVar2.acctType = srcMidVar2.AcctType
	
	destMidVar2.bankBranchCode = srcMidVar2.BankBranchCode
	
	destMidVar2.bankCode = srcMidVar2.BankCode
	
	destMidVar2.checkNo = srcMidVar2.CheckNo
	
	destMidVar2.creditCardType = srcMidVar2.CreditCardType
	
	destMidVar2.cvvNumber = srcMidVar2.CVVNumber
	
	dest.currencyId = src.CurrencyID
	
	dest.currencyRate = src.CurrencyRate
	
	dest.custKey = src.CustKey
	
	dest.deptId = src.DeptID
	
	dest.extPayType = src.ExtPayType
	
	dest.extTransId = src.ExtTransID
	
	dest.operId = src.OperID
	
	dest.oriAmount = src.OriAmount

	dest.curAmount = src.CurAmount
	
	dest.oriCurrencyId = src.OriCurrencyID
	
	dest.payChannelId = src.PayChannelID
	
	mappingList(src.PaymentDetail,dest.paymentDetail,listMapping2)

	mappingList(src.AdditionalProperty,dest.additionalProperty,listMapping0)

	mappingList(src.BalanceChgInfo,dest.balanceChgInfo,listMapping4)
	
	dest.paymentMethod = src.PaymentMethod
	
	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.status = src.Status
	
	dest.subKey = src.SubKey
	
	dest.transId = src.TransID
	
	dest.transType = src.TransType
	
	destMidVar2.expDate = parseDate(srcMidVar2.ExpDate,Constant4Model.DATE_FORMAT)
	
	dest.paymentTime = parseDate(src.PaymentTime,Constant4Model.DATE_FORMAT)
	
}

def srcMidVar = srcReturn.QueryPaymentLogResultMsg.ResultHeader

def destMidVar = destReturn.resultHeader

mappingList(srcMidVar.AdditionalProperty,destMidVar.simpleProperty,listMapping0)

destMidVar.version = srcMidVar.Version

destMidVar.resultCode = srcMidVar.ResultCode

destMidVar.resultDesc = srcMidVar.ResultDesc

destMidVar.msgLanguageCode = srcMidVar.MsgLanguageCode

def destMidVar0 = destReturn.paymentLogResultInfo

def srcMidVar0 = srcReturn.QueryPaymentLogResultMsg.QueryPaymentLogResult

destMidVar0.beginRowNum = srcMidVar0.BeginRowNum

destMidVar0.fetchRowNum = srcMidVar0.FetchRowNum

destMidVar0.totalRowNum = srcMidVar0.TotalRowNum

mappingList(srcMidVar0.PaymentInfo,destMidVar0.paymentInfoList,listMapping1)

destReturn._class = "com.huawei.ngcbs.cm.common.ws.client.io.ar.queryPaymentLog.io.QueryPaymentLogResult"
