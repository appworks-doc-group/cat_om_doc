def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destReturn.SupplementProfileResultMsg.ResultHeader

def destMidVar1 = destReturn.SupplementProfileResultMsg.SupplementProfileResult.SiteInfo

def srcMidVar1 = srcReturn.supplementProfileResultInfo.siteInfo

destMidVar1.PrimarySite = srcMidVar1.primarySite

destMidVar1.SecondarySite = srcMidVar1.secondarySite

srcMidVar = srcReturn.resultHeader

destMidVar.Version = srcMidVar.version

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.MessageSeq = srcMidVar.messageSeq

mappingList(srcReturn.simpleProperty,destMidVar.AdditionalProperty,listMapping0)
