dest.setServiceOperation("CustomerService","cleanUpCustData")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "java.lang.Object"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

destArgs1._class = "com.huawei.ngcbs.cm.customer.cleanupdata.io.CleanUpCustDataRequest"

def srcMidVar = srcArgs0.CleanUpCustDataRequestMsg.CleanUpCustDataRequest

destArgs1.registerCustKey = srcMidVar.RegisterCustKey

def srcMidVar0 = srcArgs0.CleanUpCustDataRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

def srcMidVar1 = srcArgs0.CleanUpCustDataRequestMsg.RequestHeader

mappingList(srcMidVar1.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar1.BusinessCode

destArgs0.messageSeq = srcMidVar1.MessageSeq

destArgs0.msgLanguageCode = srcMidVar1.MsgLanguageCode

def srcMidVar2 = srcArgs0.CleanUpCustDataRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar2.ChannelID

destArgs0.operatorId = srcMidVar2.OperatorID

def srcMidVar3 = srcArgs0.CleanUpCustDataRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar3.BEID

destArgs0.brId = srcMidVar3.BRID

def srcMidVar4 = srcArgs0.CleanUpCustDataRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar4.TimeType

destArgs0.timeZoneId = srcMidVar4.TimeZoneID

destArgs0.interMode = srcMidVar1.AccessMode

destArgs0.version = srcMidVar1.Version
