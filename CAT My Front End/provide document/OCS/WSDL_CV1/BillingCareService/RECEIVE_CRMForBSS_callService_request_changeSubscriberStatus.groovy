dest.setServiceOperation("CRMForBSS","changeSubscriberStatus")
def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Name = src.paraName
	
	dest.Value = src.paraValue
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Name = src.paraName
	
	dest.Value = src.paraValue
	
}

def destMidVar = destArgs0.ChangeSubscriberStatusRequest.ChangeSubscriberStatus[0]

def srcMidVar = srcArgs0.changeSubscriberStatusList[0]

destMidVar.ChangeReason = srcMidVar.changeReason

destMidVar.ChangeType = srcMidVar.changeType

mappingList(srcMidVar.extParamList,destMidVar.ExtParamList,listMapping0)

destMidVar.Msisdn = srcMidVar.msisdn

destMidVar.NewStatus = srcMidVar.newStatus

destMidVar.OldStatus = srcMidVar.oldStatus

destMidVar.SubscriberID = srcMidVar.subscriberId

def destMidVar0 = destArgs0.ChangeSubscriberStatusRequest.RequestHeader

def srcMidVar0 = srcArgs0.workOrderHeader

destMidVar0.AccessPwd = srcMidVar0.accessPwd

destMidVar0.AccessUser = srcMidVar0.accessUser

destMidVar0.BEId = srcMidVar0.beId

destMidVar0.ChannelId = srcMidVar0.channelId

destMidVar0.Language = srcMidVar0.language

destMidVar0.OperatorId = srcMidVar0.operatorId

destMidVar0.OperatorPwd = srcMidVar0.operatorPwd

destMidVar0.ProcessTime = srcMidVar0.processTime

destMidVar0.SessionId = srcMidVar0.sessionId

destMidVar0.TestFlag = srcMidVar0.testFlag

destMidVar0.TimeType = srcMidVar0.timeType

destMidVar0.TimeZoneID = srcMidVar0.timeZoneId

destMidVar0.TransactionId = srcMidVar0.transactionId

destMidVar0.Version = srcMidVar0.version

def destMidVar1 = destArgs0.ChangeSubscriberStatusRequest.RequestHeader.ExtParamList

mappingList(srcMidVar0.extParamList,destMidVar1.ParameterInfo,listMapping1)
