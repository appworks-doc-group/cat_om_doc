def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 = 
{
    src,dest  ->

	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.accessPwd = src.AccessPwd
	
	dest.accessUser = src.AccessUser
	
	def destMidVar = dest.extParamList[0]
	
	def srcMidVar0 = src.ExtParamList.ParameterInfo[0]
	
	destMidVar.paraName = srcMidVar0.Name
	
	destMidVar.paraValue = srcMidVar0.Value
	
	dest.language = src.Language
	
	dest.operatorId = src.OperatorId
	
	dest.processTime = src.ProcessTime
	
	dest.sessionId = src.SessionId
	
	dest.testFlag = src.TestFlag
	
	dest.timeType = src.TimeType
	
	dest.timeZoneId = src.TimeZoneID
	
	dest.transactionId = src.TransactionId
	
	dest.version = src.Version
	
	dest.beId = src.BEId
	
	dest.channelId = src.ChannelId
	
	def srcMidVar1 = src.ExtParamList
	
	mappingList(srcMidVar1.ParameterInfo,dest.extParamList,listMapping1)
	
	dest.operatorPwd = src.OperatorPwd
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.paraName = src.Name
	
	dest.paraValue = src.Value
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.paraName = src.Name
	
	dest.paraValue = src.Value
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.paraName = src.Name
	
	dest.paraValue = src.Value
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.activeTime = src.ActiveTime
	
	dest.effectiveTime = src.EffectiveTime
	
	dest.expireTime = src.ExpireTime
	
	dest.lastActiveTime = src.LastActiveTime
	
	def srcMidVar4 = src.ExtParamList
	
	mappingList(srcMidVar4.AdditionInfo,dest.extParmalist,listMapping5)
	
	def srcMidVar5 = src.OfferingID
	
	dest.offeringId = srcMidVar5.OfferingID
	
	dest.purchaseSeq = srcMidVar5.PurchaseSeq
	
	dest.trialEndTime = src.TrialEndTime
	
	dest.trialStartTime = src.TrialStartTime
	
}

def srcMidVar = srcReturn.ActiveSubscriberResult.ResponseHeader

destReturn.resultCode = srcMidVar.RetCode

destReturn.resultDesc = srcMidVar.RetMsg

listMapping0.call(srcMidVar.RequestHeader,destReturn.reverseHeader)

def srcMidVar2 = srcReturn.ActiveSubscriberResult.ResponseHeader.ExtParamList

mappingList(srcMidVar2.ParameterInfo,destReturn.paramInfos,listMapping2)

def srcMidVar3 = srcReturn.ActiveSubscriberResult.ActiveInfo

destReturn.activeTime = srcMidVar3.ActiveTime

destReturn.expireTime = srcMidVar3.ExprieTime

destReturn.msisdn = srcMidVar3.Msisdn

destReturn.subscriberId = srcMidVar3.SubscriberID

destReturn.trialEndTime=srcMidVar3.TrialEndTime

destReturn.trialStartTime=srcMidVar3.TrialStartTime

mappingList(srcMidVar3.ExtParamList,destReturn.extParmalist,listMapping3)

mappingList(srcMidVar3.ActOfferInfo,destReturn.actOfferInfos,listMapping4)

destReturn._class = "com.huawei.ngcbs.cm.reverse.subactivation.io.ActiveSubscriberResult"
