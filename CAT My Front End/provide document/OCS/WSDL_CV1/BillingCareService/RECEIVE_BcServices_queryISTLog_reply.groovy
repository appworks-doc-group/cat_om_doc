import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	mappingList(src.simpleProperty,dest.AdditionalProperty,listMapping1)
	
	dest.MsgLanguageCode = src.msgLanguageCode
	
	dest.ResultCode = src.resultCode
	
	dest.ResultDesc = src.resultDesc
	
	dest.Version = src.version

    dest.MessageSeq = src.messageSeq
	
}

def destMidVar = destReturn.QueryISTLogResultMsg

listMapping0.call(srcReturn.resultHeader,destMidVar.ResultHeader)

def destMidVar0 = destReturn.QueryISTLogResultMsg.QueryISTLogResult.ISTLogDetail

def srcMidVar = srcReturn.queryISTLogResultInfo

   destMidVar0.RecordID = srcMidVar.recordID

   destMidVar0.MessageSeq = srcMidVar.messageSeq

   destMidVar0.RequestSysCode = srcMidVar.requestSysCode 

   destMidVar0.InteractMode = srcMidVar.interactMode

   destMidVar0.CustID = srcMidVar.custID

   destMidVar0.ServiceTypeCode = srcMidVar.serviceTypeCode

   destMidVar0.OperationObjectType = srcMidVar.operationObjectType

   destMidVar0.OperationObjectCode = srcMidVar.operationObjectCode

   destMidVar0.BeginTime=formatDate(srcMidVar.beginTime, Constant4Model.DATE_FORMAT)

   destMidVar0.EndTime=formatDate(srcMidVar.endTime, Constant4Model.DATE_FORMAT)

   destMidVar0.Status = srcMidVar.status

   destMidVar0.ResultCode = srcMidVar.resultCode

   destMidVar0.OperatorID = srcMidVar.operatorID


