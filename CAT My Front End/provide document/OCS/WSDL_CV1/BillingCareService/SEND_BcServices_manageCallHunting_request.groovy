import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("BMGroupService","manageCallHunting")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.group.managecallhunting.io.ManageCallHuntingRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.ManageCallHuntingRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar.AccessMode

def srcMidVar0 = srcArgs0.ManageCallHuntingRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

mappingList(srcMidVar.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar.BusinessCode

destArgs0.messageSeq = srcMidVar.MessageSeq

destArgs0.msgLanguageCode = srcMidVar.MsgLanguageCode

def srcMidVar1 = srcArgs0.ManageCallHuntingRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.operatorId = srcMidVar1.OperatorID

def srcMidVar2 = srcArgs0.ManageCallHuntingRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar2.BEID

destArgs0.brId = srcMidVar2.BRID

def srcMidVar3 = srcArgs0.ManageCallHuntingRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar.Version

def srcMidVar4 = srcArgs0.ManageCallHuntingRequestMsg.ManageCallHuntingRequest

destArgs1.effectiveTime = parseDate(srcMidVar4.EffectiveTime,Constant4Model.DATE_FORMAT)

destArgs1.expireTime = parseDate(srcMidVar4.ExpireTime,Constant4Model.DATE_FORMAT)

destArgs1.huntingMainNumber = srcMidVar4.HuntingMainNumber

destArgs1.huntingNumber = srcMidVar4.HuntingNumber

destArgs1.operationType = srcMidVar4.OperationType

destArgs1.priority = srcMidVar4.Priority

destArgs1.userState = srcMidVar4.UserState

def destMidVar = destArgs1.groupAccessCode

def srcMidVar5 = srcArgs0.ManageCallHuntingRequestMsg.ManageCallHuntingRequest.SubGroupAccessCode

destMidVar.groupCode = srcMidVar5.SubGroupCode

destMidVar.groupKey = srcMidVar5.SubGroupKey
