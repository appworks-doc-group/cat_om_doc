def srcReturn = src.payload._return   

def destReturn = dest.payload._return    

def srcResultHeader = srcReturn.resultHeader

def srcResultBody = srcReturn.resultBody

def destMidVar = destReturn.QueryUnbilledFUBalResponse.QueryUnbilledFUBalResult.ResultMessage.MessageHeader

def srcMidVar = destReturn.QueryUnbilledFUBalResponse.QueryUnbilledFUBalResult.ResultMessage.UnbilledFUBalList

destMidVar.CommandId = srcResultHeader.commandId
destMidVar.Version = srcResultHeader.version
destMidVar.TransactionId = srcResultHeader.transactionId
destMidVar.SequenceId = srcResultHeader.sequenceId
destMidVar.ResultCode = srcResultHeader.resultCode
destMidVar.ResultDesc = srcResultHeader.resultDesc
destMidVar.TenantId = srcResultHeader.tenantId
destMidVar.Language = srcResultHeader.language

def listMapping1

listMapping1 =
        {
            src,dest  ->

                dest.SubId = src.subKey

                dest.AcctId = src.acctKey

                dest.MDN = src.subscriberNo

                dest.CycleId = src.billCycleId

                dest.AcctType = src.accountType

                dest.ProductId = src.productId

                dest.LastRollover = src.lastRollover

                dest.Added = src.added

                dest.Used = src.used

                dest.CurrentBal = src.currentBal

                dest.UnitType = src.unitType

                dest.ApplyTime = formatDate(src.applyTime, "yyyyMMddHHmmss")

                dest.ExpireTime = formatDate(src.expireTime, "yyyyMMddHHmmss")

                dest.Rolloverstatus = src.rollOverstatus

                dest.Lastdatetoavailrollover = src.lastdatetoavailrollover

                dest.Expirytimeofrollover = src.expirytimeofrollover

                dest.RolloverAmtLastCycle = src.rolloverAmtLastCycle

        }

mappingList(srcResultBody.unbilledFUBalList,srcMidVar.UnbilledFUBalRecord,listMapping1)




