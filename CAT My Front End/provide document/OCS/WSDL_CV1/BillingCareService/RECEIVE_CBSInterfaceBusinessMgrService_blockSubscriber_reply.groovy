def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.BlockSubscriberResultMsg.ResultHeader

destMidVar.CommandId = srcReturn.commandId

destMidVar.Version = srcReturn.version

destMidVar.TransactionId = srcReturn.transactionId

destMidVar.SequenceId = srcReturn.sequenceId

destMidVar.TenantId = srcReturn.beId

destMidVar.Language = srcReturn.msgLanguageCode

destMidVar.ResultCode = srcReturn.resultCode

destMidVar.ResultDesc = srcReturn.resultDesc

