
def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.customer.delstatement.io.DeleteStatementRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def destMidVar = destArgs1.custAccessCode

def srcMidVar = srcArgs0.DelStatementRequestMsg.DelStatementRequest.RegisterCust

destMidVar.customerCode = srcMidVar.CustomerCode

destMidVar.customerKey = srcMidVar.CustomerKey

def destMidVar0 = destArgs1.deleteStatementInfo

def srcMidVar0 = srcArgs0.DelStatementRequestMsg.DelStatementRequest

destMidVar0.stmtKey = srcMidVar0.SmtKey

def srcMidVar1 = srcArgs0.DelStatementRequestMsg.RequestHeader

destArgs0.msgLanguageCode = srcMidVar1.MsgLanguageCode

def srcMidVar2 = srcArgs0.DelStatementRequestMsg.RequestHeader.AccessSecurity

destArgs0.password = srcMidVar2.Password

destArgs0.remoteAddress = srcMidVar2.RemoteIP

mappingList(srcMidVar1.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar1.BusinessCode

destArgs0.messageSeq = srcMidVar1.MessageSeq

def srcMidVar3 = srcArgs0.DelStatementRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar3.ChannelID

destArgs0.operatorId = srcMidVar3.OperatorID

def srcMidVar4 = srcArgs0.DelStatementRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar4.BEID

destArgs0.brId = srcMidVar4.BRID

def srcMidVar5 = srcArgs0.DelStatementRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar5.TimeType

destArgs0.timeZoneId = srcMidVar5.TimeZoneID

destArgs0.version = srcMidVar1.Version

destMidVar.primaryIdentity = srcMidVar.PrimaryIdentity

destArgs0.loginSystem = srcMidVar2.LoginSystemCode
