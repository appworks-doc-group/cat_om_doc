import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.cm.ocs12ws.core.bo.Ocs12MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.ocs12ws.subscriber.batch.io.BatchOperationInfoOcs12Base"

def listMapping6

listMapping6 = 
{
    src,dest  ->

	def destMidVar22 = dest.property
	
	destMidVar22.propCode = src.Id
	
	destMidVar22.value = src.Value
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	def destMidVar21 = dest.offeringInst.offeringKey
	
	destMidVar21.oId = src.Id
	
	mappingList(src.SimpleProperty,dest.properties,listMapping6)
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.oId = src.Id
	
	dest.pSeq = src.ProductOrderKey
	
}

def listMapping8

listMapping8 = 
{
    src,dest  ->

	dest.code = src.Id
    
    dest.value = src.Value
	
}

def listMapping9

listMapping9 = 
{
    src,dest  ->

	dest.code = src.Id
	
	dest.value = src.Value
	
}

def listMapping10

listMapping10 = 
{
    src,dest  ->

	dest.code = src.Id
	
	dest.value = src.Value
	
}

def listMapping11

listMapping11 = 
{
    src,dest  ->

	dest.id = src.Id
	
	dest.value = src.Value
	
}

def listMapping12

listMapping12 = 
{
    src,dest  ->

	dest.id = src.Id
	
	mappingList(src.SimpleProperty,dest.prodOrderProps,listMapping11)
	
}


def srcMidVar = srcArgs0.BatchOperationRequestMsg.RequestHeader

destArgs0.beId = srcMidVar.TenantId

destArgs0.additionInfo = srcMidVar.additionInfo

destArgs0.belToAreaId = srcMidVar.BelToAreaID

destArgs0.commandId = srcMidVar.CommandId

destArgs0.currentCell = srcMidVar.currentCell

destArgs0.interFrom = srcMidVar.InterFrom

destArgs0.interMedi = srcMidVar.InterMedi

destArgs0.interMode = srcMidVar.InterMode

destArgs0.operatorId = srcMidVar.OperatorID

destArgs0.partnerId = srcMidVar.PartnerID

destArgs0.partnerOperId = srcMidVar.PartnerOperID

destArgs0.remark = srcMidVar.Remark

destArgs0.requestType = srcMidVar.RequestType

destArgs0.reserve2 = srcMidVar.Reserve2

destArgs0.reserve3 = srcMidVar.Reserve3

destArgs0.sequenceId = srcMidVar.SequenceId

destArgs0.messageSeq = srcMidVar.SerialNo

def srcMidVar0 = srcArgs0.BatchOperationRequestMsg.RequestHeader.SessionEntity

destArgs0.loginSystem = srcMidVar0.Name

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteAddress

destArgs0.thirdPartyId = srcMidVar.ThirdPartyID

destArgs0.tradePartnerId = srcMidVar.TradePartnerID

destArgs0.transactionId = srcMidVar.TransactionId

destArgs0.version = srcMidVar.Version

destArgs0.visitArea = srcMidVar.visitArea

def destMidVar = destArgs1.batchActiveSubRequest

def srcMidVar1 = srcArgs0.BatchOperationRequestMsg.BatchOperationRequest.BatchActiveSubscriber

destMidVar.requestFileName = srcMidVar1.FileName

def destMidVar0 = destArgs1.batchDeleteSubRequest

def srcMidVar2 = srcArgs0.BatchOperationRequestMsg.BatchOperationRequest.BatchDeleteSubscriber

destMidVar0.requestFileName = srcMidVar2.FileName

def destMidVar1 = destArgs1.batchNewSubscriberRequest.batchNewSubscriberRequest

def srcMidVar3 = srcArgs0.BatchOperationRequestMsg.BatchOperationRequest.BatchNewSubscriber.Customer

destMidVar1.custBelToAreaID = srcMidVar3.BelToAreaID

destMidVar1.custCreditAmount = srcMidVar3.CreditAmount

destMidVar1.custCreditGrade = srcMidVar3.CreditGrade

destMidVar1.custRegistrationTime = srcMidVar3.RegistrationTime

destMidVar1.custSkill = srcMidVar3.Skill

destMidVar1.custSocialNo = srcMidVar3.SocialNo

destMidVar1.custState = srcMidVar3.CustomerState

def destMidVar2 = destArgs1.batchNewSubscriberRequest.batchNewSubscriberRequest.regCustomerInfo.individualInfo

destMidVar2.education = srcMidVar3.Education

destMidVar2.email = srcMidVar3.Email

destMidVar2.gender = srcMidVar3.Gender

destMidVar2.nativePlace = srcMidVar3.NativePlace

destMidVar2.idType = srcMidVar3.IdType

destMidVar2.idNumber = srcMidVar3.IdCode

destMidVar2.occupation = srcMidVar3.JobType

destMidVar2.marriedStatus = srcMidVar3.MaritalStatus

def srcMidVar4 = srcArgs0.BatchOperationRequestMsg.BatchOperationRequest.BatchNewSubscriber.Subscriber

destMidVar1.subBelToAreaID = srcMidVar4.BelToAreaID

destMidVar1.subRegistrationTime = srcMidVar4.RegistrationTime

def destMidVar3 = destArgs1.batchNewSubscriberRequest.batchNewSubscriberRequest.createSubscriberInfo.subscriberInfo

destMidVar3.subPassword = srcMidVar4.Password

def destMidVar4 = destArgs1.batchNewSubscriberRequest.batchNewSubscriberRequest.createSubscriberInfo.subBrandInfo

destMidVar4.brand = srcMidVar4.BrandId

def destMidVar5 = destArgs1.batchNewSubscriberRequest.batchNewSubscriberRequest.createAccountInfos[0]

def srcMidVar5 = srcArgs0.BatchOperationRequestMsg.BatchOperationRequest.BatchNewSubscriber.Account

destMidVar5.billCycleType = srcMidVar5.BillcycleType

def destMidVar6 = destArgs1.batchNewSubscriberRequest.batchNewSubscriberRequest.createAccountInfos[0].accountInfo

destMidVar6.acctName = srcMidVar5.Name

def destMidVar7 = destArgs1.batchNewSubscriberRequest.batchNewSubscriberRequest.createAccountInfos[0].contactInfo

destMidVar7.title = srcMidVar5.Title

destMidVar2.nationality = srcMidVar3.Country

def destMidVar8 = destArgs1.batchNewSubscriberRequest.batchNewSubscriberRequest.addresses[0]

destMidVar8.addr2 = srcMidVar5.Address

destMidVar6.acctCode = srcMidVar5.Code

destMidVar8.addr1 = srcMidVar3.Address

def destMidVar9 = destArgs1.batchNewSubscriberRequest.batchNewSubscriberRequest.regCustomerInfo.customerInfo

destMidVar9.custCode = srcMidVar3.Code

destMidVar9.custType = srcMidVar3.CustomerType

destMidVar9.custLevel = srcMidVar3.Grade

destMidVar2.firstName = srcMidVar3.Name

destMidVar2.race = srcMidVar3.NationType

destMidVar8.postCode = srcMidVar3.ZipCode

destMidVar3.subscriberKey = srcMidVar4.Code

destMidVar3.ivrLang = srcMidVar4.Lang

def destMidVar13 = destArgs1.batchNewSubscriberRequest.batchNewSubscriberRequest.subPaymentMode

destMidVar13.paymentMode = srcMidVar4.PaidMode

destMidVar3.writtenLang = srcMidVar4.SMSLang

def destMidVar14 = destArgs1.batchNewSubscriberRequest

def srcMidVar8 = srcArgs0.BatchOperationRequestMsg.BatchOperationRequest.BatchNewSubscriber

destMidVar14.requestFileName = srcMidVar8.FileName

def destMidVar15 = destArgs1.batchChangeMainProdRequest

def srcMidVar9 = srcArgs0.BatchOperationRequestMsg.BatchOperationRequest.BatchChangeMainProduct

destMidVar15.requestFileName = srcMidVar9.FileName

def destMidVar16 = destArgs1.batchChangeMainProdRequest.changeMainProdRequestOcs12Base

destMidVar16.handlingChargeFlag = srcMidVar9.HandlingChargeFlag

def destMidVar17 = destArgs1.batchChangeMainProdRequest.changeMainProdRequestOcs12Base.changeSubOfferingInfo.subBrand

destMidVar17.brand = srcMidVar9.NewBrandId

destMidVar16.posAcctCredit = srcMidVar9.posAcctCredit

destMidVar16.posAcctInitBal = srcMidVar9.PosAcctInitBal

destMidVar16.ppsAcctCredit = srcMidVar9.ppsAcctCredit

destMidVar16.ppsAcctInitBal = srcMidVar9.ppsAcctInitBal

destMidVar15.changeMainProdRequestOcs12Base.changeSubOfferingInfo.newPrimaryOffering.effDate=parseDate(srcMidVar9.EffectiveDate,Constant4Model.DAY_FORMAT)

def destMidVar18 = destArgs1.batchChangeMainProdRequest.changeMainProdRequestOcs12Base.changeSubOfferingInfo.newPrimaryOffering.offeringInst.offeringKey

destMidVar18.oId = srcMidVar9.NewMainProductId

def destMidVar19 = destArgs1.batchChangeMainProdRequest.changeMainProdRequestOcs12Base.changeSubOfferingInfo.newPrimaryOffering

destMidVar19.effMode = srcMidVar9.ValidMode

def destMidVar20 = destArgs1.batchChangeMainProdRequest.changeMainProdRequestOcs12Base.changeSubOfferingInfo

mappingList(srcMidVar9.Product,destMidVar20.addSuppOfferings,listMapping5)

mappingList(srcMidVar9.RemovedProduct,destMidVar20.delSuppOfferings,listMapping7)

mappingList(srcMidVar8.Product,destMidVar1.appProdList,listMapping12)

destMidVar1.strMainProdId = srcMidVar4.MainProductID

destMidVar1.strBirthday = srcMidVar3.Birthday

def destMidVar23 = destArgs1.batchNewSubscriberRequest.batchNewSubscriberRequest.regCustomerInfo

mappingList(srcMidVar3.SimpleProperty,destMidVar23.custProperties,listMapping8)

def destMidVar24 = destArgs1.batchNewSubscriberRequest.batchNewSubscriberRequest.createSubscriberInfo

mappingList(srcMidVar4.SimpleProperty,destMidVar24.subProperties,listMapping9)

mappingList(srcMidVar5.SimpleProperty,destMidVar5.properties,listMapping10)


def listMapping1010

listMapping1010 = 
{
    src,dest  ->

	dest.id = src.Id
	
	dest.type = src.Type
	
	dest.value = src.Value
	
}

def listMapping109

listMapping109 = 
{
    src,dest  ->

	dest.id = src.Id
	
	dest.registrationTime=src.RegistrationTime
	
	mappingList(src.SimpleProperty,dest.serviceProps,listMapping1010)
	
}

def listMapping1011

listMapping1011 = 
{
    src,dest  ->

	dest.id = src.Id
	
	dest.type = src.Type
	
	dest.value = src.Value
	
}

def listMapping108

listMapping108 = 
{
    src,dest  ->

	dest.id = src.Id
	
	dest.name = src.Name
	
	mappingList(src.Service,dest.prodServices,listMapping109)
	
	def srcMidVar1014 = src.Service[0]
	
	dest.status = srcMidVar1014.Status
	
	mappingList(src.SimpleProperty,dest.prodOrderProps,listMapping1011)
	
}

def listMapping1014

listMapping1014 = 
{
    src,dest  ->

	dest.id = src.Id
	
	dest.type = src.Type
	
	dest.value = src.Value
	
}

def listMapping1013

listMapping1013 = 
{
    src,dest  ->

	dest.id = src.Id
	
	mappingList(src.SimpleProperty,dest.serviceProps,listMapping1014)
	
}

def listMapping1012

listMapping1012 = 
{
    src,dest  ->

	dest.id = src.ProductID
	
	dest.prodOrderKey = src.ProductOrderKey
	
	mappingList(src.Service,dest.prodServices,listMapping1013)
	
}

def srcMidVar1013 = srcArgs0.BatchOperationRequestMsg.BatchOperationRequest.BatchSubscribe

def destMidVar1029 = destArgs1.batchSubscribeOcs12Request.changeAppProdsRequestOcs12Base

mappingList(srcMidVar1013.Product,destMidVar1029.changedProdOrders,listMapping108)

def destMidVar1030 = destArgs1.batchSubscribeOcs12Request.changeAppProdsRequestOcs12Base.changedProdOrders[0]

destMidVar1030.validMode = srcMidVar1013.ValidMode

destMidVar1029.changedProdOrders[0].effDate=srcMidVar1013.EffectiveDate

destMidVar1029.changedProdOrders[0].expDate=srcMidVar1013.ExpireDate

def destMidVar1031 = destArgs1.batchSubscribeOcs12Request

destMidVar1031.requestFileName = srcMidVar1013.FileName

destMidVar1029.handlingChargeFlag = srcMidVar1013.HandlingChargeFlag

def srcMidVar1015 = srcArgs0.BatchOperationRequestMsg.BatchOperationRequest.BatchUnSubscribe

def destMidVar1032 = destArgs1.batchUnSubscribeOcs12Request.changeAppProdsRequestOcs12Base

mappingList(srcMidVar1015.Product,destMidVar1032.changedProdOrders,listMapping1012)

def destMidVar1033 = destArgs1.batchUnSubscribeOcs12Request.changeAppProdsRequestOcs12Base.changedProdOrders[0]

destMidVar1033.validMode = srcMidVar1015.ValidMode

destMidVar1033.expDate=srcMidVar1015.ExpireDate

def destMidVar1034 = destArgs1.batchUnSubscribeOcs12Request

destMidVar1034.requestFileName = srcMidVar1015.FileName

def destBatchCashRechargeRequest = destArgs1.batchCashRechargeRequest
def srcBatchCashRechargeRequest = srcArgs0.BatchOperationRequestMsg.BatchOperationRequest.BatchCashRecharge
destBatchCashRechargeRequest.requestFileName = srcBatchCashRechargeRequest.FileName

def destBatchAjustAccountRequest = destArgs1.batchAdjustAccountRequest
def srcBatchAjustAccountRequest = srcArgs0.BatchOperationRequestMsg.BatchOperationRequest.BatchAjustAccount

destBatchAjustAccountRequest.requestFileName = srcBatchAjustAccountRequest.FileName
destBatchAjustAccountRequest.operateType = srcBatchAjustAccountRequest.OperateType
destBatchAjustAccountRequest.rtner = srcBatchAjustAccountRequest.Rtner
destBatchAjustAccountRequest.additionalInfo = srcBatchAjustAccountRequest.AdditionalInfo
destBatchAjustAccountRequest.sPCode = srcBatchAjustAccountRequest.SPCode

def listMapping1025

listMapping1025 = 
{
    src,dest  ->

	dest.accountType = src.AccountType
	
	dest.currAcctChgAmt = src.CurrAcctChgAmt
	
	dest.glId= src.GLID
	
	dest.minMeasureId = src.MinMeasureId
	
	dest.productId = src.ProductID
	
	dest.expireTime = src.ExpireTime
}

def srcMidVar11 = srcArgs0.BatchOperationRequestMsg.BatchOperationRequest.BatchAjustAccount.ModifyAcctFeeList
mappingList(srcMidVar11.ModifyAcctFee,destBatchAjustAccountRequest.modifyAcctFeeList,listMapping1025)

destBatchAjustAccountRequest.validityIncrement = srcBatchAjustAccountRequest.ValidityIncrement
destBatchAjustAccountRequest.merchant = srcBatchAjustAccountRequest.Merchant
destBatchAjustAccountRequest.service = srcBatchAjustAccountRequest.Service

def destbatchQuerySubInfoRequest = destArgs1.batchQuerySubInfoRequest
def srcbatchQuerySubInfoRequest = srcArgs0.BatchOperationRequestMsg.BatchOperationRequest.BatchQuerySubInfo
destbatchQuerySubInfoRequest.requestFileName = srcbatchQuerySubInfoRequest.FileName