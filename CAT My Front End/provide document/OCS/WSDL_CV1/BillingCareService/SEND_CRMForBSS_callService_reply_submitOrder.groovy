def srcReturn = src.payload._return

def destReturn = dest.payload._return

def srcMidVar = srcReturn.SubmitOrderResponse.ResponseHeader

destReturn.resultCode = srcMidVar.RetCode

destReturn.resultDesc = srcMidVar.RetMsg
