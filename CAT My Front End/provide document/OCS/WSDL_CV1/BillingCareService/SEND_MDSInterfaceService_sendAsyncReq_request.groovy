dest.setServiceOperation("MDSInterfaceService","sendAsyncReq")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Name = src.paraName
	
	dest.Value = src.paraValue
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.BEID = src.beId
	
	dest.BizCode = src.bizCode
	
	def srcMidVar = src.paramList
	
	def destMidVar0 = dest.ParaList
	
	mappingList(srcMidVar.retParamList,destMidVar0.Para,listMapping1)
	
	dest.Pri = src.priority
	
	dest.ReservedExeTime = src.reservedExeTime
	
	dest.Serial = src.serialId
	
	dest.Time = src.time
	
	dest.TimeOut = src.timeOut
	
	dest.TranID = src.tranId
	
}

def destMidVar = destArgs0.SendAsyncReq.AsyncRequestMsg.RequestMessage

mappingList(srcArgs0.requestBody,destMidVar.MessageBody,listMapping0)

def destMidVar1 = destArgs0.SendAsyncReq.AsyncRequestMsg.RequestMessage.MessageHeader

def srcMidVar0 = srcArgs0.requestHeader

destMidVar1.DebugFlag = srcMidVar0.debugFlag

destMidVar1.SessionID = srcMidVar0.sessId

destMidVar1.SysPassword = srcMidVar0.sysPassword

destMidVar1.SysUser = srcMidVar0.sysUser

def destMidVar2 = destArgs0.SendAsyncReq.AsyncRequestMsg.RequestMessage.MessageHeader.TraceIDs

def srcMidVar1 = srcArgs0.requestHeader.traceIds

destMidVar2.TraceID = srcMidVar1.traceIds
