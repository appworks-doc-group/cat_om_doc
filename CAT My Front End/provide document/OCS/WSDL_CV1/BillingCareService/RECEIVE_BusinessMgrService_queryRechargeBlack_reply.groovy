import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturnData = src.payload._return
def destReturnDoc = dest.payload._return    

def destHeader = destReturnDoc.QueryRechargeBlackResultMsg.ResultHeader

def srcHeader = srcReturnData.resultHeader

destHeader.CommandId = srcHeader.commandId
destHeader.ResultCode = srcHeader.resultCode
destHeader.ResultDesc = srcHeader.resultDesc
destHeader.TransactionId = srcHeader.transactionId
destHeader.SequenceId = srcHeader.sequenceId
destHeader.Version = srcHeader.version
destHeader.OperationTime = srcHeader.operationTime
destHeader.OrderId = srcHeader.orderId

def srcBusinessData = srcReturnData.resultBody
srcBusinessData._class = "com.huawei.ngcbs.cm.ocs33ws.cbsinterfacebusiness.io.OCS33QueryRechargeBlackResult"
def destMessageBody = destReturnDoc.QueryRechargeBlackResultMsg.QueryRechargeBlackResult
destMessageBody.RechargeBlackFlag = srcBusinessData.rechargeBlackFlag
