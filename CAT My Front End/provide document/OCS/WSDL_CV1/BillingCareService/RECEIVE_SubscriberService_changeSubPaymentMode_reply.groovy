import com.huawei.ngcbs.bm.common.common.Constant4Model


def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 = 
{
    src,dest  ->

	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	def destMidVar0 = dest.AdditionalProperty[0]
	
	def srcMidVar = src.simpleProperty[0]
	
	destMidVar0.Code = srcMidVar.code
	
	destMidVar0.Value = srcMidVar.value
	
	dest.MsgLanguageCode = src.msgLanguageCode
	
	dest.ResultCode = src.resultCode
	
	dest.ResultDesc = src.resultDesc
	
	dest.Version = src.version
	
	mappingList(src.simpleProperty,dest.AdditionalProperty,listMapping1)
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	def destMidVar2 = dest.OfferingOwner
	
	def srcMidVar0 = src.owner
	
	destMidVar2.OwnerKey = srcMidVar0.ownerKey
	
	destMidVar2.OwnerType = srcMidVar0.ownerType
	
			dest.NewEffectiveTime=formatDate(src.modifyOfferingInstInfo.effDate,Constant4Model.DATE_FORMAT)
	
			dest.NewExpirationTime=formatDate(src.modifyOfferingInstInfo.expDate,Constant4Model.DATE_FORMAT)
	
	def destMidVar3 = dest.OfferingKey
	
	def srcMidVar1 = src.modifyOfferingInstInfo.offeringKey
	
	destMidVar3.OfferingID = srcMidVar1.oId
	
	destMidVar3.PurchaseSeq = srcMidVar1.pSeq
	
}

def destMidVar = destReturn.ChangeSubPaymentModeResultMsg

listMapping0.call(srcReturn.resultHeader,destMidVar.ResultHeader)

def destMidVar1 = destReturn.ChangeSubPaymentModeResultMsg.ChangeSubPaymentModeResult

mappingList(srcReturn.modifyOfferings,destMidVar1.ModifyOffering,listMapping2)
