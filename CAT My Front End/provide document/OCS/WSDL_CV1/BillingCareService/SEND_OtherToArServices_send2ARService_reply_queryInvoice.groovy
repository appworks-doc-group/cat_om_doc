import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.chargeAmount = src.ChargeAmount
	
	dest.chargeCode = src.ChargeCode
	
	dest.currencyId = src.CurrencyID
	
	dest.invoiceId = src.InvoiceID
	
	dest.openAmount = src.OpenAmount
	
	dest.serviceCategory = src.ServiceCategory
	
	dest.status = src.Status
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.acctKey = src.AcctKey
	
	dest.billCycleBeginTime = parseDate(src.BillCycleBeginTime,Constant4Model.DATE_FORMAT)
	
	dest.billCycleEndTime = parseDate(src.BillCycleEndTime,Constant4Model.DATE_FORMAT)
	
	dest.billCycleId = src.BillCycleID
	
	dest.currencyId = src.CurrencyID
	
	dest.custKey = src.CustKey
	
	dest.dueDate = parseDate(src.DueDate,Constant4Model.DATE_FORMAT)
	
	dest.invoiceAmount = src.InvoiceAmount
	
	dest.invoiceDate = parseDate(src.InvoiceDate,Constant4Model.DATE_FORMAT)
	
	mappingList(src.InvoiceDetail,dest.invoiceDetailList,listMapping1)
	
	dest.invoiceId = src.InvoiceID
	
	dest.invoiceNo = src.InvoiceNo
	
	dest.openAmount = src.OpenAmount
	
	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.settleDate = parseDate(src.SettleDate,Constant4Model.DATE_FORMAT)
	
	dest.status = src.Status
	
	dest.subKey = src.SubKey
	
	dest.transType = src.TransType
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.value = src.Value
	
	dest.code = src.Code
	
}

def srcMidVar = srcReturn.QueryInvoiceResultMsg.QueryInvoiceResult

mappingList(srcMidVar.InvoiceInfo,destReturn.invoiceResultInfo.invoiceInfoList,listMapping0)

def srcMidVar0 = srcReturn.QueryInvoiceResultMsg.ResultHeader

def destMidVar = destReturn.resultHeader

mappingList(srcMidVar0.AdditionalProperty,destMidVar.simpleProperty,listMapping2)

destMidVar.msgLanguageCode = srcMidVar0.MsgLanguageCode

destMidVar.resultCode = srcMidVar0.ResultCode

destMidVar.resultDesc = srcMidVar0.ResultDesc

destMidVar.version = srcMidVar0.Version

destReturn._class = "com.huawei.ngcbs.cm.common.ws.client.io.ar.queryInvoice.io.QueryInvoiceResult"
