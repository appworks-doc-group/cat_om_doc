import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("BMQueryService","queryAggregationAmount")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.query.queryaggregationamount.io.QueryAggregationAmountRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.accoutCode = src.AccountCode
	
	dest.accoutKey = src.AccountKey
	
	dest.payType = src.PayType
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def srcMidVar = srcArgs0.QueryAggregationAmountRequestMsg.QueryAggregationAmountRequest

destArgs1.billCycleId = srcMidVar.BillCycleID

listMapping0.call(srcMidVar.SubAccessCode,destArgs1.subAccessCode)

destArgs1.endTime=parseDate(srcMidVar.TimePeriod.EndTime, Constant4Model.DATE_FORMAT)

destArgs1.startTime=parseDate(srcMidVar.TimePeriod.StartTime, Constant4Model.DATE_FORMAT)

def srcMidVar0 = srcArgs0.QueryAggregationAmountRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar0.LoginSystemCode

destArgs0.password = srcMidVar0.Password

destArgs0.remoteAddress = srcMidVar0.RemoteIP

def srcMidVar1 = srcArgs0.QueryAggregationAmountRequestMsg.RequestHeader

mappingList(srcMidVar1.AdditionalProperty,destArgs0.simpleProperty,listMapping1)

destArgs0.businessCode = srcMidVar1.BusinessCode

destArgs0.messageSeq = srcMidVar1.MessageSeq

destArgs0.msgLanguageCode = srcMidVar1.MsgLanguageCode

def srcMidVar2 = srcArgs0.QueryAggregationAmountRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar2.ChannelID

destArgs0.operatorId = srcMidVar2.OperatorID

def srcMidVar3 = srcArgs0.QueryAggregationAmountRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar3.BEID

destArgs0.brId = srcMidVar3.BRID

def srcMidVar4 = srcArgs0.QueryAggregationAmountRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar4.TimeType

destArgs0.timeZoneId = srcMidVar4.TimeZoneID

destArgs0.version = srcMidVar1.Version

destArgs0.interMode = srcMidVar1.AccessMode

listMapping2.call(srcMidVar.AcctAccessCode,destArgs1.acctAccessCode)
