def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Value = src.value
	
	dest.Id = src.code
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.BelToAreaID = src.belToAreaId
	
	dest.CommandId = src.commandId
	
	dest.currentCell = src.currentCell
	
	dest.InterFrom = src.interFrom
	
	dest.InterMedi = src.interMedi
	
	dest.InterMode = src.interMode
	
	dest.OperatorID = src.operatorId
	
	dest.Remark = src.remark
	
	dest.SequenceId = src.sequenceId
	
	dest.SerialNo = src.serialNo
	
	def destMidVar2 = dest.SessionEntity
	
	def srcMidVar = src.sessionEntity
	
	destMidVar2.Name = srcMidVar.name
	
	destMidVar2.Password = srcMidVar.password
	
	destMidVar2.RemoteAddress = srcMidVar.remoteAddress
	
	dest.TenantID = src.tenantId
	
	dest.TradePartnerID = src.tradePartnerId
	
	dest.TransactionId = src.transactionId
	
	dest.Version = src.version
	
	dest.visitArea = src.visitArea
	
	dest.PartnerID = src.partnerId
	
	dest.PartnerOperID = src.partnerOperId
	
	dest.RequestType = src.requestType
	
	dest.ThirdPartyID = src.thirdPartyId
	
}

def destMidVar = destArgs0.ModifyGroupMemberBasicInfoRequestMsg.ModifyGroupMemberBasicInfoRequest.GroupMemberInfo

destMidVar.PBXDisplayNumber = srcArgs0.displayNumber

destMidVar.ScreenClass = srcArgs0.screenClass

def destMidVar0 = destArgs0.ModifyGroupMemberBasicInfoRequestMsg.ModifyGroupMemberBasicInfoRequest

destMidVar0.GroupNumber = srcArgs0.groupNumber

destMidVar0.GrpMemberNo = srcArgs0.grpMemberNo

destMidVar0.GrpMemberShortNo = srcArgs0.grpMemberShortNo

destMidVar.GrpMemberType = srcArgs0.memberType

mappingList(srcArgs0.simplePropertyList,destMidVar.SimpleProperty,listMapping0)

def destMidVar1 = destArgs0.ModifyGroupMemberBasicInfoRequestMsg

listMapping1.call(srcArgs0.omRequestHeader,destMidVar1.RequestHeader)
