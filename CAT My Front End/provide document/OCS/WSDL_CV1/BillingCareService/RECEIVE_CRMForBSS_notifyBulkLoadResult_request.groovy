dest.setServiceOperation("CRMForBSS","notifyBulkLoadResult")

def srcArgs0 = src.payload._args[0]

def srcArgs1 = src.payload._args[1]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Name = src.paraName
	
	dest.Value = src.paraValue
	
}

def destMidVar = destArgs0.NotifyBulkLoadRequest.RequestHeader

destMidVar.AccessPwd = srcArgs0.accessPwd

destMidVar.AccessUser = srcArgs0.accessUser

destMidVar.BEId = srcArgs0.beId

destMidVar.ChannelId = srcArgs0.channelId

def destMidVar0 = destArgs0.NotifyBulkLoadRequest.RequestHeader.ExtParamList

mappingList(srcArgs0.extParamList,destMidVar0.ParameterInfo,listMapping0)

destMidVar.Language = srcArgs0.language

destMidVar.OperatorId = srcArgs0.operatorId

destMidVar.OperatorPwd = srcArgs0.operatorPwd

destMidVar.ProcessTime = srcArgs0.processTime

destMidVar.SessionId = srcArgs0.sessionId

destMidVar.TestFlag = srcArgs0.testFlag

destMidVar.TimeType = srcArgs0.timeType

destMidVar.TimeZoneID = srcArgs0.timeZoneId

destMidVar.TransactionId = srcArgs0.transactionId

destMidVar.Version = srcArgs0.version

def destMidVar1 = destArgs0.NotifyBulkLoadRequest.BulkLoadResult

destMidVar1.SuccessFileName = srcArgs1.successFileName

destMidVar1.TotalFailedAmt = srcArgs1.totalFailedAmt

destMidVar1.TotalSuccessfulAmt = srcArgs1.totalSuccessfulAmt

destMidVar1.FailFileName = srcArgs1.failFileName

destMidVar1.ResultCode = srcArgs1.resultCode
