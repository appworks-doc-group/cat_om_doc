def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcReturn.WorkOrderResultMsg.ResultHeader

mappingList(srcMidVar.AdditionalProperty,destReturn.simpleProperty,listMapping0)

destReturn.msgLanguageCode = srcMidVar.MsgLanguageCode

destReturn.resultCode = srcMidVar.ResultCode

destReturn.resultDesc = srcMidVar.ResultDesc

destReturn.version = srcMidVar.Version

destReturn._class = "com.huawei.ngcbs.cm.common.reverse.commnotify.io.WOCommonResult"
