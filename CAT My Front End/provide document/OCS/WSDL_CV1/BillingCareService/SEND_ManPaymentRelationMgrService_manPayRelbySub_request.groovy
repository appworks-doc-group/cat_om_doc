import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReqDocMsg = src.payload._args[0]

def destMsgHeader = dest.payload._args[0]
destMsgHeader._class = "com.huawei.ngcbs.cm.ocs33ws.core.bo.Ocs33MessageHeader"

def destReqData = dest.payload._args[1]
destReqData._class = "com.huawei.ngcbs.cm.ocs33ws.customer.manpayrelbysub.io.OCS33ManPayRelBySubRequest"

def srcReqHeaderDoc = srcReqDocMsg.ManPayRelbySubRequestMsg.RequestHeader

destMsgHeader.commandId = srcReqHeaderDoc.CommandId
destMsgHeader.version = srcReqHeaderDoc.Version
destMsgHeader.transactionId = srcReqHeaderDoc.TransactionId
destMsgHeader.sequenceId = srcReqHeaderDoc.SequenceId
destMsgHeader.requestType = srcReqHeaderDoc.RequestType
destMsgHeader.loginSystem = srcReqHeaderDoc.SessionEntity.Name
destMsgHeader.password = srcReqHeaderDoc.SessionEntity.Password
destMsgHeader.remoteAddress = srcReqHeaderDoc.SessionEntity.RemoteAddress
destMsgHeader.interFrom = srcReqHeaderDoc.InterFrom
destMsgHeader.interMedi = srcReqHeaderDoc.InterMedi
destMsgHeader.interMode = srcReqHeaderDoc.InterMode
destMsgHeader.visitArea = srcReqHeaderDoc.visitArea
destMsgHeader.currentCell = srcReqHeaderDoc.currentCell
destMsgHeader.additionInfo = srcReqHeaderDoc.additionInfo
destMsgHeader.thirdPartyId = srcReqHeaderDoc.ThirdPartyID
destMsgHeader.partnerId = srcReqHeaderDoc.PartnerID
destMsgHeader.operatorId = srcReqHeaderDoc.OperatorID
destMsgHeader.tradePartnerId = srcReqHeaderDoc.TradePartnerID
destMsgHeader.partnerOperId = srcReqHeaderDoc.PartnerOperID
destMsgHeader.belToAreaId = srcReqHeaderDoc.BelToAreaID
destMsgHeader.reserve2 = srcReqHeaderDoc.Reserve2
destMsgHeader.reserve3 = srcReqHeaderDoc.Reserve3
destMsgHeader.messageSeq = srcReqHeaderDoc.SerialNo
destMsgHeader.remark = srcReqHeaderDoc.Remark
destMsgHeader.msgLanguageCode = srcReqHeaderDoc.Language
destMsgHeader.performanceStatCmd = "ChangeRscRelation"

def srcReqDataDoc = srcReqDocMsg.ManPayRelbySubRequestMsg.ManPayRelbySubRequest
destReqData.subscriberNo = srcReqDataDoc.SubscriberNo
destReqData.operType = srcReqDataDoc.Opertype
destReqData.validMode = srcReqDataDoc.ValidMode.Mode
destReqData.effectiveDate = parseDate(srcReqDataDoc.ValidMode.EffectiveDate,Constant4Model.DATE_FORMAT)
destReqData.expireDate = parseDate(srcReqDataDoc.ValidMode.ExpireDate,Constant4Model.DATE_FORMAT)

def listPara = 
{
	src,dest  ->
	
	dest.paraType = src.ParaType
	dest.paraCode = src.ParaCode
	dest.paraOper = src.ParaOper	
	dest.paraValue = src.ParaValue
}

def listSimpleCondition = 
{
	src,dest  ->
	
	mappingList(src.Para, dest.paras, listPara)
}

def listPaymentSubsMapping = 
{
	src,dest  ->
	
	dest.operType = src.OperType
	dest.recID = src.RecID
	dest.priority = src.Priority	
	dest.serviceScene = src.ServiceScene
	dest.timeSchemaID = src.TimeSchemaID
	listSimpleCondition.call(src.SimpleCondition,dest.simpleCondition)
	dest.applyTime = parseDate(src.ApplyTime,Constant4Model.DATE_FORMAT)
	dest.expireTime = parseDate(src.ExpireTime,Constant4Model.DATE_FORMAT)
}

def listMapping1

listMapping1 =
		{
			src,dest  ->

				dest.quota = src.Quota

				dest.notifyType = src.NotifyType

				dest.actionRule = src.ActionRule

				dest.effDate = parseDate(src.StartTime, Constant4Model.DATE_FORMAT)

				dest.expDate = parseDate(src.EndTime, Constant4Model.DATE_FORMAT)

		}

def listPaymentRelationMapping = 
{
	src,dest  ->
	
	dest.accountCode = src.Accountcode
	dest.subscriberNo = src.SubscriberNo
	dest.operType = src.OperType
	dest.paymentRelKey = src.PaymentRelKey
	dest.ruleType = src.RuleType
	dest.quota = src.Quota
	dest.measureID = src.MeasureID
	dest.applyTime = parseDate(src.ApplyTime,Constant4Model.DATE_FORMAT)
	dest.expireTime = parseDate(src.ExpireTime,Constant4Model.DATE_FORMAT)
	dest.measureID = src.MeasureID
	dest.accountType = src.AccountType
	dest.shareSourceType = src.ShareSourceType
	dest.permillage = src.Permillage
	dest.priority = src.Priority
	mappingList(src.PaymentSubs, dest.paymentSubsList, listPaymentSubsMapping)

	mappingList(src.NotifyRule, dest.notifyRules, listMapping1)
}

mappingList(srcReqDataDoc.PaymentRelation, destReqData.paymentRelationList, listPaymentRelationMapping)
