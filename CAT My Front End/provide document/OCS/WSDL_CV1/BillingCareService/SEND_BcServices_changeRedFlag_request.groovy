import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("AccountService","changeRedFlag")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.account.changeredflag.io.ChangeRedFlagRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def destMidVar = destArgs1.acctAccessCode

def srcMidVar = srcArgs0.ChangeRedFlagRequestMsg.ChangeRedFlagRequest.ChangeObj.AcctAccessCode

destMidVar.accoutCode = srcMidVar.AccountCode

destMidVar.accoutKey = srcMidVar.AccountKey

destMidVar.payType = srcMidVar.PayType

destMidVar.primaryIdentity = srcMidVar.PrimaryIdentity

def destMidVar0 = destArgs1.custAccessCode

def srcMidVar0 = srcArgs0.ChangeRedFlagRequestMsg.ChangeRedFlagRequest.ChangeObj.CustAccessCode

destMidVar0.customerCode = srcMidVar0.CustomerCode

destMidVar0.customerKey = srcMidVar0.CustomerKey

destMidVar0.primaryIdentity = srcMidVar0.PrimaryIdentity

def destMidVar1 = destArgs1.subAccessCode

def srcMidVar1 = srcArgs0.ChangeRedFlagRequestMsg.ChangeRedFlagRequest.ChangeObj.SubAccessCode

destMidVar1.primaryIdentity = srcMidVar1.PrimaryIdentity

destMidVar1.subscriberKey = srcMidVar1.SubscriberKey

def srcMidVar2 = srcArgs0.ChangeRedFlagRequestMsg.ChangeRedFlagRequest

destArgs1.creditLimitRedFlag = srcMidVar2.CreditLimitRedFlag

destArgs1.dunningRedFlag = srcMidVar2.DunningRedFlag

def srcMidVar3 = srcArgs0.ChangeRedFlagRequestMsg.ChangeRedFlagRequest.EffectiveTime

destArgs1.expMode = srcMidVar3.Mode

destArgs1.effDate=parseDate(srcMidVar3.Time, Constant4Model.DATE_FORMAT)

destArgs1.expirationTime=parseDate(srcMidVar2.ExpirationTime, Constant4Model.DATE_FORMAT)

destArgs1.opType = srcMidVar2.OpType

def srcMidVar4 = srcArgs0.ChangeRedFlagRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar4.AccessMode

def srcMidVar5 = srcArgs0.ChangeRedFlagRequestMsg.RequestHeader.AccessSecurity

destArgs0.password = srcMidVar5.Password

destArgs0.remoteAddress = srcMidVar5.RemoteIP

mappingList(srcMidVar4.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.businessCode = srcMidVar4.BusinessCode

destArgs0.messageSeq = srcMidVar4.MessageSeq

destArgs0.msgLanguageCode = srcMidVar4.MsgLanguageCode

def srcMidVar6 = srcArgs0.ChangeRedFlagRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar6.ChannelID

destArgs0.operatorId = srcMidVar6.OperatorID

def srcMidVar7 = srcArgs0.ChangeRedFlagRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar7.BEID

destArgs0.brId = srcMidVar7.BRID

def srcMidVar8 = srcArgs0.ChangeRedFlagRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar8.TimeType

destArgs0.timeZoneId = srcMidVar8.TimeZoneID

destArgs0.loginSystem = srcMidVar5.LoginSystemCode

destArgs0.version = srcMidVar4.Version

destArgs1.effMode = srcMidVar3.Mode
