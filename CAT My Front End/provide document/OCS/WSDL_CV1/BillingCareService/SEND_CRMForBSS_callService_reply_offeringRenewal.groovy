def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.paraName = src.Name
	
	dest.paraValue = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.paraName = src.Name
	
	dest.paraValue = src.Value
	
}

def srcMidVar = srcReturn.OfferingRenewalResult.ResponseHeader.ExtParamList

mappingList(srcMidVar.ParameterInfo,destReturn.paramInfos,listMapping0)

def destMidVar = destReturn.reverseHeader

def srcMidVar0 = srcReturn.OfferingRenewalResult.ResponseHeader.RequestHeader

destMidVar.accessPwd = srcMidVar0.AccessPwd

destMidVar.accessUser = srcMidVar0.AccessUser

destMidVar.beId = srcMidVar0.BEId

destMidVar.channelId = srcMidVar0.ChannelId

def srcMidVar1 = srcReturn.OfferingRenewalResult.ResponseHeader.RequestHeader.ExtParamList

mappingList(srcMidVar1.ParameterInfo,destMidVar.extParamList,listMapping1)

destMidVar.language = srcMidVar0.Language

destMidVar.operatorId = srcMidVar0.OperatorId

destMidVar.operatorPwd = srcMidVar0.OperatorPwd

destMidVar.processTime = srcMidVar0.ProcessTime

destMidVar.sessionId = srcMidVar0.SessionId

destMidVar.testFlag = srcMidVar0.TestFlag

destMidVar.timeType = srcMidVar0.TimeType

destMidVar.timeZoneId = srcMidVar0.TimeZoneID

destMidVar.transactionId = srcMidVar0.TransactionId

destMidVar.version = srcMidVar0.Version

def srcMidVar2 = srcReturn.OfferingRenewalResult.ResponseHeader

destReturn.resultCode = srcMidVar2.RetCode

destReturn.resultDesc = srcMidVar2.RetMsg

destReturn._class = "com.huawei.ngcbs.cm.reverse.ws.ReverseResponseHeader"
