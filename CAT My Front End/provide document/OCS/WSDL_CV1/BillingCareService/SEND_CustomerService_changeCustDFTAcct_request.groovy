import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.customer.changecustdftacct.io.ChangeCustDFTAcctRequest"

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.value = src.Value
	
	dest.key = src.Code
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	def destMidVar = dest.changeCustDftAcctInfo
	
	def srcMidVar0 = src.NewDFTAcct
	
	destMidVar.acctKey = srcMidVar0.AcctKey
	
	destMidVar.opType = src.OpType
	
	def srcMidVar1 = src.EffectiveTime
	
	destMidVar.effMode = srcMidVar1.Mode
	
		destMidVar.effDate=parseDate(srcMidVar1.Time, Constant4Model.DATE_FORMAT)
	
	dest.payRelationKey = srcMidVar0.PayRelationKey
	
	mappingList(src.ControlProperty,dest.controlPropertys,listMapping5)
	
	listMapping3.call(src.CustAccessCode,dest.custAccessCode)
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	def srcMidVar2 = src.AccessSecurity
	
	dest.loginSystem = srcMidVar2.LoginSystemCode
	
	dest.password = srcMidVar2.Password
	
	dest.remoteAddress = srcMidVar2.RemoteIP
	
	mappingList(src.AdditionalProperty,dest.simpleProperty,listMapping6)
	
	dest.businessCode = src.BusinessCode
	
	dest.messageSeq = src.MessageSeq
	
	dest.msgLanguageCode = src.MsgLanguageCode
	
	def srcMidVar3 = src.OperatorInfo
	
	dest.channelId = srcMidVar3.ChannelID
	
	dest.operatorId = srcMidVar3.OperatorID
	
	def srcMidVar4 = src.OwnershipInfo
	
	dest.beId = srcMidVar4.BEID
	
	dest.brId = srcMidVar4.BRID
	
	def srcMidVar5 = src.TimeFormat
	
	dest.timeType = srcMidVar5.TimeType
	
	dest.timeZoneId = srcMidVar5.TimeZoneID
	
	dest.version = src.Version
	
	dest.interMode = src.AccessMode
	
}

def srcMidVar = srcArgs0.ChangeCustDFTAcctRequestMsg

listMapping2.call(srcMidVar.ChangeCustDFTAcctRequest,destArgs1)

listMapping4.call(srcMidVar.RequestHeader,destArgs0)
