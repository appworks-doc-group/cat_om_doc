import com.huawei.ngcbs.bm.common.common.Constant4Model;

dest.setServiceOperation("BMQueryService","queryTransferValidityLog")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.query.querytransfervaliditylog.io.QueryTransferValidityLogRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def srcMidVar6 = srcArgs0.QueryTransferValidityLogRequestMsg.QueryTransferValidityLogRequest

destArgs1.startTime = parseDate(srcMidVar6.StartTime,Constant4Model.DATE_FORMAT)

destArgs1.endTime = parseDate(srcMidVar6.EndTime,Constant4Model.DATE_FORMAT)

destArgs1.totalRowNum = srcMidVar6.TotalRowNum

destArgs1.beginRowNum = srcMidVar6.BeginRowNum

destArgs1.fetchRowNum = srcMidVar6.FetchRowNum

listMapping1.call(srcMidVar6.QueryObj.SubAccessCode,destArgs1.subAccessCode)
	
def srcMidVar2 = srcArgs0.QueryTransferValidityLogRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar2.AccessMode

def srcMidVar3 = srcMidVar2.AccessSecurity

destArgs0.loginSystem = srcMidVar3.LoginSystemCode

destArgs0.password = srcMidVar3.Password

destArgs0.remoteAddress = srcMidVar3.RemoteIP

destArgs0.businessCode = srcMidVar3.BusinessCode

destArgs0.messageSeq = srcMidVar2.MessageSeq

destArgs0.msgLanguageCode = srcMidVar2.MsgLanguageCode

def srcMidVar4 = srcMidVar2.OperatorInfo

destArgs0.channelId = srcMidVar4.ChannelID

destArgs0.operatorId = srcMidVar4.OperatorID

def srcMidVar5 = srcMidVar2.OwnershipInfo

destArgs0.beId = srcMidVar5.BEID

destArgs0.brId = srcMidVar5.BRID

def srcMidVar7 = srcMidVar2.TimeFormat

destArgs0.timeType = srcMidVar7.TimeType

destArgs0.timeZoneId = srcMidVar7.TimeZoneID

destArgs0.version = srcMidVar2.Version

mappingList(srcArgs0.QueryTransferValidityLogRequestMsg.RequestHeader.AdditionalProperty,destArgs0.simpleProperty,listMapping0)
