import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("ChangeMonitorRuleService","changeMonitorRule")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.iot.changemonitorrule.io.ChangeMonitorRuleRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.offeringId = src.OfferingID
	
	dest.offeringCode = src.OfferingCode
	
	dest.purchaseSeq = src.PurchaseSeq
}

def listMapping5

listMapping5 = 
{
    src,dest  ->
		
	dest.monitorTypeCode = src.MonitorTemplateType
	
	dest.unitType = src.UnitType
	
	dest.permillage = src.Permillage	

	def destMidVar7 = dest.measureInfo
	
	destMidVar7.measureId = src.MeasureID
	
	destMidVar7.monitorValue = src.MonitorValue
	
	def destMidVar8 = dest.currencyInfo
	
	destMidVar8.currencyID = src.CurrencyID
	
	destMidVar8.monitorValue = src.MonitorValue
	
	listMapping4.call(src.MonitorCond.OfferingKey,dest.offeringKey)
}

def listMapping6

listMapping6 = 
{
    src,dest  ->
		
	dest.optype = src.opType
}

def listMapping7

listMapping7 = 
{
    src,dest  ->
			
	dest.newMonitorValue = src.MonitorValue
	
	dest.newPermillage = src.NewPermillage
	
	dest.measureId = src.MeasureID
}


def srcMidVar = srcArgs0.ChangeMonitorRuleRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar.BEID

destArgs0.brId = srcMidVar.BRID

def srcMidVar0 = srcArgs0.ChangeMonitorRuleRequestMsg.RequestHeader

destArgs0.businessCode = srcMidVar0.BusinessCode

def srcMidVar1 = srcArgs0.ChangeMonitorRuleRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar1.ChannelID

destArgs0.interMode = srcMidVar0.AccessMode

def srcMidVar2 = srcArgs0.ChangeMonitorRuleRequestMsg.RequestHeader.AccessSecurity

destArgs0.loginSystem = srcMidVar2.LoginSystemCode

destArgs0.messageSeq = srcMidVar0.MessageSeq

destArgs0.msgLanguageCode = srcMidVar0.MsgLanguageCode

destArgs0.operatorId = srcMidVar1.OperatorID

destArgs0.password = srcMidVar2.Password

destArgs0.remoteAddress = srcMidVar2.RemoteIP

mappingList(srcMidVar0.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

def srcMidVar3 = srcArgs0.ChangeMonitorRuleRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar3.TimeType

destArgs0.timeZoneId = srcMidVar3.TimeZoneID

destArgs0.version = srcMidVar0.Version

destArgs1.monitorRuleId = srcArgs0.ChangeMonitorRuleRequestMsg.ChangeMonitorRuleRequest.MonitorRuleID

def srcMidVar4 = srcArgs0.ChangeMonitorRuleRequestMsg.ChangeMonitorRuleRequest.MonitorObj

listMapping1.call(srcMidVar4.CustAccessCode,destArgs1.custAccessCode)

listMapping2.call(srcMidVar4.SubAccessCode,destArgs1.subAccessCode)

listMapping3.call(srcMidVar4.SubGroupAccessCode,destArgs1.groupAccessCode)

def srcMidVar5 = srcArgs0.ChangeMonitorRuleRequestMsg.ChangeMonitorRuleRequest

listMapping5.call(srcMidVar5.AddMonitor,destArgs1.addMonitorRuleInfo)
listMapping6.call(srcMidVar5.DeactiveMonitor,destArgs1.deactiveMonitorRuleInfo)
listMapping7.call(srcMidVar5.ModifyMonitor,destArgs1.modifyMonitorRuleInfo)



