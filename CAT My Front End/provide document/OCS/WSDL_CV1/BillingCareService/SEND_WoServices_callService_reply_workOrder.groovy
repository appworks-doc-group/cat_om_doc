def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcReturn.WorkOrderResultMsg.ResultHeader

destReturn.resultCode = srcMidVar.ResultCode

destReturn.resultDesc = srcMidVar.ResultDesc

def destMidVar = destReturn.ResultHrader

destMidVar.msgLanguageCode = srcMidVar.MsgLanguageCode

destMidVar.version = srcMidVar.Version

mappingList(srcMidVar.AdditionalProperty,destMidVar.simpleProperty,listMapping0)

destReturn._class = "com.huawei.ngcbs.cm.common.ws.client.io.common.WorkOrderWSResult"
