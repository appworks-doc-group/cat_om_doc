def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.RcvWriteBackResponse.WriteBackResult.ResultMessage.MessageBody

destMidVar.ID = srcReturn.id

destMidVar.RetCode = srcReturn.retCode

destMidVar.RetDesc = srcReturn.retDesc

destMidVar.Serial = srcReturn.serialId

destMidVar.TranID = srcReturn.tranId
