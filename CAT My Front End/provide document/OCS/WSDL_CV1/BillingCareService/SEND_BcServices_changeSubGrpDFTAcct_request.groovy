import com.huawei.ngcbs.bm.common.common.Constant4Model


dest.setServiceOperation("BMGroupService","changeSubGrpDFTAcct")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.group.changesubgrpdftacct.io.ChangeSubGrpDFTAcctRequest"

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.businessCode = src.BusinessCode
	
	def srcMidVar0 = src.OperatorInfo
	
	dest.channelId = srcMidVar0.ChannelID
	
	def srcMidVar1 = src.AccessSecurity
	
	dest.loginSystem = srcMidVar1.LoginSystemCode
	
	dest.messageSeq = src.MessageSeq
	
	dest.msgLanguageCode = src.MsgLanguageCode
	
	dest.operatorId = srcMidVar0.OperatorID
	
	dest.password = srcMidVar1.Password
	
	def srcMidVar2 = src.TimeFormat
	
	dest.timeType = srcMidVar2.TimeType
	
	dest.timeZoneId = srcMidVar2.TimeZoneID
	
	dest.version = src.Version
	
	dest.remoteAddress = srcMidVar1.RemoteIP
	
	mappingList(src.AdditionalProperty,dest.simpleProperty,listMapping1)
	
	def srcMidVar3 = src.OwnershipInfo
	
	dest.beId = srcMidVar3.BEID
	
	dest.brId = srcMidVar3.BRID
	
	dest.interMode = src.AccessMode
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcArgs0.ChangeSubGrpDFTAcctRequestMsg

listMapping0.call(srcMidVar.RequestHeader,destArgs0)

def srcMidVar4 = srcArgs0.ChangeSubGrpDFTAcctRequestMsg.ChangeSubGrpDFTAcctRequest

listMapping2.call(srcMidVar4.SubGroupAccessCode,destArgs1.groupAccessCode)

def destMidVar = destArgs1.changeGroupDftAcctInfo

mappingList(srcMidVar4.ControlProperty,destMidVar.controlProperties,listMapping3)

destMidVar.opType = srcMidVar4.OpType

def srcMidVar5 = srcArgs0.ChangeSubGrpDFTAcctRequestMsg.ChangeSubGrpDFTAcctRequest.NewDFTAcct

destMidVar.acctKey = srcMidVar5.AcctKey

destMidVar.paymentRelationKey = srcMidVar5.PayRelationKey

def srcMidVar6 = srcArgs0.ChangeSubGrpDFTAcctRequestMsg.ChangeSubGrpDFTAcctRequest.EffectiveTime

destMidVar.effMode = srcMidVar6.Mode

destMidVar.effDate=parseDate(srcMidVar4.EffectiveTime.Time, Constant4Model.DATE_FORMAT)
