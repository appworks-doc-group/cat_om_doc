def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Id = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Name = src.name
	
	dest.Password = src.password
	
	dest.RemoteAddress = src.remoteAddress
	
}

def destMidVar = destArgs0.CreateGroupRequestMsg.CreateGroupRequest

destMidVar.EffectiveDate = srcArgs0.effDate

destMidVar.Group_Id = srcArgs0.groupId

def destMidVar0 = destArgs0.CreateGroupRequestMsg.CreateGroupRequest.GroupCustomer

destMidVar0.GroupName = srcArgs0.groupName

destMidVar0.GroupNumber = srcArgs0.groupNumber

destMidVar0.NoAnswerTime = srcArgs0.noAnswerTime

destMidVar0.ParentGroupNumber = srcArgs0.parentGrpNumber

destMidVar0.SpeedDialPrefix = srcArgs0.speedDialPrefix

destMidVar.ValidMode = srcArgs0.validMode

mappingList(srcArgs0.simplePropertyList,destMidVar0.SimpleProperty,listMapping0)

def destMidVar1 = destArgs0.CreateGroupRequestMsg.RequestHeader

def srcMidVar = srcArgs0.omRequestHeader

destMidVar1.BelToAreaID = srcMidVar.belToAreaId

destMidVar1.CommandId = srcMidVar.commandId

destMidVar1.currentCell = srcMidVar.currentCell

destMidVar1.InterFrom = srcMidVar.interFrom

destMidVar1.InterMedi = srcMidVar.interMedi

destMidVar1.InterMode = srcMidVar.interMode

destMidVar1.OperatorID = srcMidVar.operatorId

destMidVar1.PartnerID = srcMidVar.partnerId

destMidVar1.PartnerOperID = srcMidVar.partnerOperId

destMidVar1.Remark = srcMidVar.remark

destMidVar1.RequestType = srcMidVar.requestType

destMidVar1.SequenceId = srcMidVar.sequenceId

destMidVar1.SerialNo = srcMidVar.serialNo

listMapping1.call(srcMidVar.sessionEntity,destMidVar1.SessionEntity)

destMidVar1.TenantID = srcMidVar.tenantId

destMidVar1.ThirdPartyID = srcMidVar.thirdPartyId

destMidVar1.TradePartnerID = srcMidVar.tradePartnerId

destMidVar1.TransactionId = srcMidVar.transactionId

destMidVar1.Version = srcMidVar.version

destMidVar1.visitArea = srcMidVar.visitArea
