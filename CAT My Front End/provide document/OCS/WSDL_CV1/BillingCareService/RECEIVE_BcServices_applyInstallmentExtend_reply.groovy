import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

  dest.InitialAmount = src.initialAmount

	dest.Amount = src.amount	
		
	dest.CycleClass = src.cycleClass
	
	dest.CycleDueDate = formatDate(src.cycleDueDate, Constant4Model.DATE_FORMAT)
	
	dest.InstallmentInstID = src.installmentInstID
	
	dest.RealRepayDate = formatDate(src.realRepayDate, Constant4Model.DATE_FORMAT)
	
	dest.Status = src.status
	
	dest.CurrencyID = src.currencyID
	
	dest.CycleSequence = src.cycleSequence
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def srcMidVar = srcReturn.applyInstallmentInfo

def destMidVar = destReturn.ApplyInstallmentExtendResultMsg.ApplyInstallmentExtendResult.newInstallmentInfo

mappingList(srcMidVar.inatallmentDetailList,destMidVar.InatallmentDetail,listMapping0)

def destMidVar0 = destReturn.ApplyInstallmentExtendResultMsg.ResultHeader

def srcMidVar0 = srcReturn.resultHeader

destMidVar0.MsgLanguageCode = srcMidVar0.msgLanguageCode

destMidVar0.ResultCode = srcMidVar0.resultCode

destMidVar0.ResultDesc = srcMidVar0.resultDesc

destMidVar0.Version = srcMidVar0.version

destMidVar0.MessageSeq = srcMidVar0.messageSeq

mappingList(srcMidVar0.simpleProperty,destMidVar0.AdditionalProperty,listMapping1)
