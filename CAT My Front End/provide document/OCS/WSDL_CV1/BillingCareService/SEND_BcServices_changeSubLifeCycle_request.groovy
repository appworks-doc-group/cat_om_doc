dest.setServiceOperation("SubscriberService","changeSubLifeCycle")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.subscriber.changesublifecycle.io.ChangeSubLifeCycleRequest"

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	def srcMidVar0 = src.AccessSecurity
	
	dest.loginSystem = srcMidVar0.LoginSystemCode
	
	dest.password = srcMidVar0.Password
	
	dest.remoteAddress = srcMidVar0.RemoteIP
	
	mappingList(src.AdditionalProperty,dest.simpleProperty,listMapping1)
	
	dest.businessCode = src.BusinessCode
	
	dest.messageSeq = src.MessageSeq
	
	dest.msgLanguageCode = src.MsgLanguageCode
	
	def srcMidVar1 = src.OperatorInfo
	
	dest.channelId = srcMidVar1.ChannelID
	
	dest.operatorId = srcMidVar1.OperatorID
	
	def srcMidVar2 = src.OwnershipInfo
	
	dest.beId = srcMidVar2.BEID
	
	dest.brId = srcMidVar2.BRID
	
	dest.version = src.Version
	
	def srcMidVar3 = src.TimeFormat
	
	dest.timeType = srcMidVar3.TimeType
	
	dest.timeZoneId = srcMidVar3.TimeZoneID
	
	dest.interMode = src.AccessMode
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->
	
	dest.subscriberKey = src.SubscriberKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->
	
	dest.statusName = src.StatusName
	
	dest.statusExpireTime=parseDate(src.StatusExpireTime, "yyyyMMddHHmmss")
	
	dest.statusIndex = src.StatusIndex
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	  dest.code = src.Code
	
	  dest.value = src.Value
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	listMapping3.call(src.SubAccessCode,dest.subAccessCode)
	
	mappingList(src.AdditionalProperty,dest.simplePropertyList,listMapping5)
	
	mappingList(src.LifeCycleStatus,dest.lifeCycleStatusList,listMapping4)
	
}

def srcMidVar = srcArgs0.ChangeSubLifeCycleRequestMsg

listMapping0.call(srcMidVar.RequestHeader,destArgs0)

listMapping2.call(srcMidVar.ChangeSubLifeCycleRequest,destArgs1)

