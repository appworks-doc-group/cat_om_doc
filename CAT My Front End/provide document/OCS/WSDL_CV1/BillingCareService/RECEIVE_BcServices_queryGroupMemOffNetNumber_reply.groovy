import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.EffectiveTime=formatDate(src.effDate,Constant4Model.DATE_FORMAT)
	
	dest.ExpirationTime=formatDate(src.expDate,Constant4Model.DATE_FORMAT)
	
	dest.OffNetNumber = src.offNetNumber
	
	dest.OffNetNumberGroupID = src.offNetNumberGroupId
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destReturn.QueryGroupMemOffNetNumberResultMsg.QueryGroupMemOffNetNumberResult

mappingList(srcReturn.offNetNumbers,destMidVar.GroupMemOffNetNumber,listMapping0)

def destMidVar0 = destReturn.QueryGroupMemOffNetNumberResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar0.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar0.ResultCode = srcMidVar.resultCode

destMidVar0.ResultDesc = srcMidVar.resultDesc

destMidVar0.Version = srcMidVar.version

destMidVar0.MessageSeq = srcMidVar.messageSeq

mappingList(srcMidVar.simpleProperty,destMidVar0.AdditionalProperty,listMapping1)
