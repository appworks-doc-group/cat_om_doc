def srcReturn = src.payload._return

def destReturn = dest.payload._return

def destMidVar = destReturn.resultHeader

def srcMidVar = srcReturn.BlacklistQueryResultMsg.ResultHeader

destMidVar.resultCode = srcMidVar.ResultCode

destMidVar.resultDesc = srcMidVar.ResultDesc

destMidVar.version = srcMidVar.Version

destReturn._class = "com.huawei.ngcbs.cm.common.ws.client.io.uvc.QueryBlackListInfResult"

def srcMidResult = srcReturn.BlacklistQueryResultMsg.BlacklistQueryResult

def destMidVar1 = destReturn.blackListInfos[0]

destMidVar1.fraudTimes = srcMidResult.FraudTimes

destMidVar1.blackTime = srcMidResult.FraudDate

destMidVar1.subscriberNo = srcMidResult.RechargeObject

destMidVar1.fraudState = srcMidResult.AccountState