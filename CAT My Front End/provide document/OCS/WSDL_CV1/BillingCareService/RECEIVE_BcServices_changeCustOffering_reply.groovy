import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

	mappingList(src.simpleProperty,dest.AdditionalProperty,listMapping1)
	
	dest.Version = src.version
	
	dest.ResultDesc = src.resultDesc
	
	dest.ResultCode = src.resultCode
	
	dest.MsgLanguageCode = src.msgLanguageCode

	dest.MessageSeq = src.messageSeq
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.NewEffectiveTime=formatDate(src.effDate, Constant4Model.DATE_FORMAT)
	
	dest.NewExpirationTime=formatDate(src.expDate, Constant4Model.DATE_FORMAT)
	
	def destMidVar0 = dest.OfferingKey
	
	def srcMidVar = src.offeringKey
	
  srcMidVar._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
	
	destMidVar0.OfferingID = srcMidVar.oId
	
	destMidVar0.OfferingCode = srcMidVar.oCode
	
	destMidVar0.PurchaseSeq = srcMidVar.pSeq
	
}

def listMapping2

listMapping2 = 
{
    src,dest  ->

	mappingList(src.modifyOfferingInfos,dest.ModifyOffering,listMapping3)
	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	dest.SubPropCode = src.code
	
	dest.Value = src.value
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.EffectiveTime=formatDate(src.effTime, Constant4Model.DATE_FORMAT)
	
	dest.ExpirationTime=formatDate(src.expTime, Constant4Model.DATE_FORMAT)
	
	def srcMidVar4 = src.instPropertyInfo
	
	mappingList(srcMidVar4.subProps,dest.SubPropInst,listMapping7)
	
	dest.Value = srcMidVar4.value
	
	dest.PropCode = srcMidVar4.propCode
	
	dest.PropType = srcMidVar4.complexFlag
	
}

def listMapping5
listMapping5 = 
{
    src,dest  ->

	dest.EffectiveTime=formatDate(src.trialStartTime, Constant4Model.DATE_FORMAT)
	
	dest.ExpirationTime=formatDate(src.trialEndTime, Constant4Model.DATE_FORMAT)
	
	mappingList(src.offerInstPropList,dest.OfferingInstProperty,listMapping4)
	
	def destMidVar5 = dest.OfferingKey
	
	def srcMidVar5 = src.offeringKeyInfo
  srcMidVar5._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
    
	destMidVar5.OfferingID = srcMidVar5.oId
  destMidVar5.OfferingCode = srcMidVar5.oCode
	destMidVar5.PurchaseSeq = srcMidVar5.pSeq
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	mappingList(src.addOffering,dest.AddOffering,listMapping5)
	
}

def destMidVar = destReturn.ChangeCustOfferingResultMsg

listMapping0.call(srcReturn.resultHeader,destMidVar.ResultHeader)

listMapping2.call(srcReturn.changeCustOfferingResultBody,destMidVar.ChangeCustOfferingResult)

listMapping6.call(srcReturn.changeCustOfferingResultBody,destMidVar.ChangeCustOfferingResult)
