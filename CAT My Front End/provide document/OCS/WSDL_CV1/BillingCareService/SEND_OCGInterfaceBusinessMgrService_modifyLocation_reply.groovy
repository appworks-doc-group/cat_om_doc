def srcReturn = src.payload._return                                                          
                                                                 
def destReturn = dest.payload._return                            
                                                                 
def srcMidVar = srcReturn.ModifyLocationResultMsg.ResultHeader
                                                                 
def destMidVar = destReturn.resultHeader                           
                                                                 
destMidVar.resultCode = srcMidVar.ResultCode                     
                                                                 
destMidVar.resultDesc = srcMidVar.ResultDesc                     
                                                                 
destMidVar.version = srcMidVar.Version                           