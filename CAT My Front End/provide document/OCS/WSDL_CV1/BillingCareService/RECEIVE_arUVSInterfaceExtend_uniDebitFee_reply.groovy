def srcReturn = src.payload._return   

def destReturn = dest.payload._return    

def srcResultHeader = srcReturn.resultHeader

def destMidVar = destReturn.UniDebitFeeResponse.UniDebitFeeResult.ResultMessage.MessageHeader

destMidVar.CommandId = srcResultHeader.commandId
destMidVar.Version = srcResultHeader.version
destMidVar.TransactionId = srcResultHeader.transactionId
destMidVar.SequenceId = srcResultHeader.sequenceId
destMidVar.ResultCode = srcResultHeader.resultCode
destMidVar.ResultDesc = srcResultHeader.resultDesc
destMidVar.TenantId = srcResultHeader.tenantId
destMidVar.Language = srcResultHeader.language

def srcResultBody = srcReturn.resultBody

def destMessageBody = destReturn.UniDebitFeeResponse.UniDebitFeeResult.ResultMessage.MessageBody

destMessageBody.AfterBalance.Value = srcResultBody.afterBalance.value

destMessageBody.AfterBalance.MinMeasureId = srcResultBody.afterBalance.minMeasureId