def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.CallingNumber = src.callingNumber
	
	dest.SerialNo = src.serialNo
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def destMidVar = destReturn.QueryFphCallerResultMsg.QueryFphCallerResult

def srcMidVar = srcReturn.queryFPHCallerResponse

destMidVar.SubscriberNo = srcMidVar.subscriberNo

mappingList(srcMidVar.fphCallers,destMidVar.FphCaller,listMapping0)

def destMidVar0 = destReturn.QueryFphCallerResultMsg.ResultHeader

def srcMidVar0 = srcReturn.resultHeader

destMidVar0.MsgLanguageCode = srcMidVar0.msgLanguageCode

destMidVar0.ResultCode = srcMidVar0.resultCode

destMidVar0.ResultDesc = srcMidVar0.resultDesc

mappingList(srcMidVar0.simpleProperty,destMidVar0.AdditionalProperty,listMapping1)

destMidVar0.Version = srcMidVar0.version

destMidVar0.MessageSeq = srcMidVar0.messageSeq
