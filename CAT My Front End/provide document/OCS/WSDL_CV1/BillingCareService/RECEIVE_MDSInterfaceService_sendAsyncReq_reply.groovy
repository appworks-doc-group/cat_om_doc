def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.paraName = src.Name
	
	dest.paraValue = src.Value
	
}

def srcMidVar = srcReturn.SendAsyncReqResponse.AsyncRequestResult.ResultMessage.MessageBody[0]

destReturn.curStep = srcMidVar.CurStep

destReturn.id = srcMidVar.ID

destReturn.retCode = srcMidVar.RetCode

destReturn.retDesc = srcMidVar.RetDesc

def srcMidVar0 = srcReturn.SendAsyncReqResponse.AsyncRequestResult.ResultMessage.MessageBody[0].RetParaList

def destMidVar = destReturn.retParamList

mappingList(srcMidVar0.Para,destMidVar.retParamList,listMapping0)

destReturn.serialId = srcMidVar.Serial

destReturn.time = srcMidVar.Time

def destMidVar0 = destReturn.resultHeader

def srcMidVar1 = srcReturn.SendAsyncReqResponse.AsyncRequestResult.ResultMessage.MessageHeader

destMidVar0.debugFlag = srcMidVar1.DebugFlag

destMidVar0.sessId = srcMidVar1.SessionID

def destMidVar1 = destReturn.resultHeader.traceIds

def srcMidVar2 = srcReturn.SendAsyncReqResponse.AsyncRequestResult.ResultMessage.MessageHeader.TraceIDs

destMidVar1.traceIds = srcMidVar2.TraceID

destReturn._class = "com.huawei.ngcbs.cm.common.provision.ws.io.SyncProvisionResult"
