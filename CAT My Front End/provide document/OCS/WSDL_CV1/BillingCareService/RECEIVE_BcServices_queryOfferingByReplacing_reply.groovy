import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return


def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.OfferingID = src.offeringID
	
	dest.OfferingCode = src.offeringCode
	
	dest.OfferingName = src.offeringName
	
	dest.EffectiveDate = formatDate(src.effectiveDate, Constant4Model.DATE_FORMAT)
	
	dest.ExpireDate = formatDate(src.expireDate, Constant4Model.DATE_FORMAT)
}



def destMidVar = destReturn.QueryOfferingByReplacingResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

destMidVar.Version = srcMidVar.version

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.MessageSeq = srcMidVar.messageSeq

def destMidVar0 = destReturn.QueryOfferingByReplacingResultMsg.QueryOfferingByReplacingResult

mappingList(srcReturn.offeringInfoList,destMidVar0.OfferingInfo,listMapping0)
