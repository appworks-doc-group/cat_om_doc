import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	dest.FreeUnitType = src.freeUnitType
	
	dest.InitialAmount = src.initialAmount
	
	dest.CurrentAmount = src.currentAmount
	
	dest.UsedAmount = src.usedAmount
	
	dest.MeasureUnit = src.measureUnit
	
	dest.TodayUsedAmount = src.todayUsedAmount
	
}

def destMidVar = destReturn.QueryDataPackageUsageResultMsg.ResultHeader

def srcMidVar = srcReturn.resultHeader

destMidVar.MsgLanguageCode = srcMidVar.msgLanguageCode

addingList(srcMidVar.simpleProperty,destMidVar.AdditionalProperty,listMapping0)

destMidVar.Version = srcMidVar.version

destMidVar.ResultCode = srcMidVar.resultCode

destMidVar.ResultDesc = srcMidVar.resultDesc

destMidVar.MessageSeq = srcMidVar.messageSeq

def srcMidVar0 = srcReturn.queryDataPackageUsageResultInfo

def destMidVar0 = destReturn.QueryDataPackageUsageResultMsg.QueryDataPackageUsageResult

addingList(srcMidVar0.usageList,destMidVar0.UsageList,listMapping1)

destMidVar0.BillCycleOpenDate = formatDate(srcMidVar0.billCycleOpenDate, Constant4Model.DATE_FORMAT)

destMidVar0.BillCycleEndDate = formatDate(srcMidVar0.billCycleEndDate, Constant4Model.DATE_FORMAT)

destMidVar0.BillCycleID = srcMidVar0.billCycleId
