import com.huawei.ngcbs.bm.common.common.Constant4Model;

dest.setServiceOperation("BMQueryService","queryUsagePayLimit")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.query.queryusagepaylimit.io.QueryUsagePayLimitRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}
def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.groupKey = src.SubGroupKey
	
	dest.groupCode = src.SubGroupCode
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping_chgSubAccessCode

listMapping_chgSubAccessCode = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

 

def listMapping_Access

listMapping_Access = 
{
  src,dest  ->
  dest.usagePayLimitKey = src.UsagePayLimitKey
	  listMapping_chgSubAccessCode.call(src.ChgObj.SubAccessCode,dest.chgObj.subAccessCode)
	  listMapping3.call(src.PaidObj.CustAccessCode,dest.paidObj.custAccessCode)
	  listMapping4.call(src.PaidObj.SubAccessCode,dest.paidObj.subAccessCode)
	  listMapping2.call(src.PaidObj.SubGroupAccessCode,dest.paidObj.groupAccessCode)
}

def srcRequest = srcArgs0.QueryUsagePayLimitRequestMsg.QueryUsagePayLimitRequest
def destRequest = destArgs1


listMapping_Access.call(srcRequest,destRequest)

	
def srcMidVar2 = srcArgs0.QueryUsagePayLimitRequestMsg.RequestHeader

destArgs0.interMode = srcMidVar2.AccessMode

def srcMidVar3 = srcMidVar2.AccessSecurity

destArgs0.loginSystem = srcMidVar3.LoginSystemCode

destArgs0.password = srcMidVar3.Password

destArgs0.remoteAddress = srcMidVar3.RemoteIP

destArgs0.businessCode = srcMidVar3.BusinessCode

destArgs0.messageSeq = srcMidVar2.MessageSeq

destArgs0.msgLanguageCode = srcMidVar2.MsgLanguageCode

def srcMidVar4 = srcMidVar2.OperatorInfo

destArgs0.channelId = srcMidVar4.ChannelID

destArgs0.operatorId = srcMidVar4.OperatorID

def srcMidVar5 = srcMidVar2.OwnershipInfo

destArgs0.beId = srcMidVar5.BEID

destArgs0.brId = srcMidVar5.BRID

def srcMidVar7 = srcMidVar2.TimeFormat

destArgs0.timeType = srcMidVar7.TimeType

destArgs0.timeZoneId = srcMidVar7.TimeZoneID

destArgs0.version = srcMidVar2.Version