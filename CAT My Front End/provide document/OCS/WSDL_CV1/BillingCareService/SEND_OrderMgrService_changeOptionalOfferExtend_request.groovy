import com.huawei.ngcbs.bm.common.common.Constant4Model

dest.setServiceOperation("OCS33OrderMgrService","changeOptionalOfferExtend")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

def destArgs1 = dest.payload._args[1]

destArgs0._class = "com.huawei.ngcbs.cm.ocs33ws.core.bo.Ocs33MessageHeader"

destArgs1._class = "com.huawei.ngcbs.cm.ocs33ws.offer.chgopofr.io.OCS33ChangeOptionalOfferExtendRequest"

def destHandleObj = destArgs1.handleObj
def destArgs3 = destArgs1.optionalOfferOrders
def destofferOrderBundleRelations = destArgs1.offerOrderBundleRelations

def srcMessageHeader = srcArgs0.ChangeOptionalOfferExtendRequestMsg.RequestHeader
def srcMessageBody = srcArgs0.ChangeOptionalOfferExtendRequestMsg.ChangeOptionalOfferExtendRequest

def srcHandleObj = srcMessageBody.HandleObj
def srcOptionalOfferOrder = srcMessageBody.OptionalOfferOrder
def srcOfferOrderBundleRelation = srcMessageBody.OfferOrderBundleRelation

destArgs0.beId  = srcMessageHeader.TenantId
destArgs0.msgLanguageCode  = srcMessageHeader.Language
destArgs0.commandId = srcMessageHeader.CommandId
destArgs0.version = srcMessageHeader.Version
destArgs0.transactionId = srcMessageHeader.TransactionId
destArgs0.sequenceId = srcMessageHeader.SequenceId
destArgs0.requestType = srcMessageHeader.RequestType
destArgs0.loginSystem = srcMessageHeader.SessionEntity.Name
destArgs0.password = srcMessageHeader.SessionEntity.Password
destArgs0.remoteAddress = srcMessageHeader.SessionEntity.RemoteAddress
destArgs0.interFrom = srcMessageHeader.InterFrom
destArgs0.interMedi = srcMessageHeader.InterMedi
destArgs0.interMode = srcMessageHeader.InterMode
destArgs0.visitArea = srcMessageHeader.visitArea
destArgs0.currentCell = srcMessageHeader.currentCell
destArgs0.additionInfo = srcMessageHeader.additionInfo
destArgs0.thirdPartyId = srcMessageHeader.ThirdPartyID
destArgs0.partnerId = srcMessageHeader.PartnerID
destArgs0.operatorId = srcMessageHeader.OperatorID
destArgs0.tradePartnerId = srcMessageHeader.TradePartnerID
destArgs0.partnerOperId = srcMessageHeader.PartnerOperID
destArgs0.belToAreaId = srcMessageHeader.BelToAreaID
destArgs0.reserve2 = srcMessageHeader.Reserve2
destArgs0.reserve3 = srcMessageHeader.Reserve3
destArgs0.messageSeq = srcMessageHeader.SerialNo
destArgs0.remark = srcMessageHeader.Remark
destArgs0.performanceStatCmd = "ChangeOptionalOfferExtend"

def listMapping14
listMapping14 = 
{
    src,dest  ->
	dest.homeZoneID = src.HomeZoneID
	dest.effectiveDate = src.effectiveDate
	dest.expireDate = src.expireDate 
}

def listMapping13
listMapping13 = 
{
    src,dest  ->
	dest.homeZoneChange = src.HomeZoneChange
	dest.homeZonePromptMode = src.HomeZonePromptMode
	mappingList(src.HomeZoneList,dest.homeZoneLists,listMapping14)
}

def listMapping4
listMapping4 = 
{
    src,dest  ->
	dest.offerKey = src.OfferKey
	dest.extOfferCode = src.ExtOfferCode
	dest.extOfferOrderCode = src.ExtOfferOrderCode
}

def listMapping7
listMapping7 = 
{
    src,dest  ->
	dest.mode = src.Mode
	dest.validateFlag = src.ValidateFlag
	dest.effectiveDate = parseDate(src.EffectiveDate,Constant4Model.DATE_FORMAT)
	dest.expireDate = parseDate(src.ExpireDate,Constant4Model.DATE_FORMAT)
}


def listMapping10
listMapping10 = 
{
    src,dest  ->
	dest.familyNo1 = src.FamilyNo
	dest.phoneNoOrder = src.phoneNoOrder
	dest.subGroupType = src.subGroupType
	dest.effectiveDate = src.effectiveDate
	dest.expireDate = src.expireDate
}

def listMapping9
listMapping9 = 
{
    src,dest  ->
	mappingList(src.FamilyNoList,dest.familyNoLists,listMapping10)
}

def listMapping12
listMapping12 = 
{
    src,dest  ->
	dest.callScreenNo = src.callScreenNo
	dest.effectiveDate = src.effectiveDate
	dest.expireDate = src.expireDate
	dest.weekStart = src.weekStart
	dest.weekStop = src.weekStop
	dest.routeNumber = src.routeNumber
	dest.iRRouteFlag = src.IRRouteFlag
	dest.routingMethod = src.RoutingMethod
}

def listMapping11
listMapping11 = 
{
    src,dest  ->
	dest.callScreenType = src.CallScreenType
	mappingList(src.CallScreenNoInfoList,dest.callScreenNoInfoLists,listMapping12)
}

def listMapping8
listMapping8 = 
{
    src,dest  ->
	listMapping9.call(src.FamilyNo,dest.familyNo)
	listMapping11.call(src.CallScreen,dest.callScreen)
	listMapping13.call(src.HomeZone,dest.homeZone)
}
def listMapping0
listMapping0 = 
{
    src,dest  ->
	dest.id = src.Id
	dest.value = src.Value	
}

def listMapping16
listMapping16 = 
{
    src,dest  ->
	listMapping4.call(src.OfferOrderIdentify,dest.offerOrderIdentify)
	listMapping7.call(src.ValidMode,dest.validMode)
	mappingList(src.SpecialProperty,dest.specialPropertys,listMapping8)
	mappingList(src.SimpleProperty,dest.simplePropertys,listMapping0)
	dest.handleChargeFlag = src.HandleChargeFlag
	dest.operationType = src.OperationType
	dest.subscriberNo = src.SubscriberNo
	dest.accountCode = src.AccountCode
	dest.convergeOfferFlag = src.ConvergeOfferFlag
	dest.optionalOfferOrder = src.OptionalOfferOrder
}
mappingList(srcOptionalOfferOrder,destArgs3,listMapping16)

def listMapping18
listMapping18 = 
{
    src,dest  ->
	dest.offerKey = src.OfferKey
	dest.extOfferCode = src.ExtOfferCode	
	dest.extOfferOrderCode = src.ExtOfferOrderCode
}

def listMapping19
listMapping19 = 
{
    src,dest  ->
	dest.offerKey = src.OfferKey
	dest.extOfferCode = src.ExtOfferCode	
	dest.extOfferOrderCode = src.ExtOfferOrderCode
	dest.subscriberCode = src.SubscriberCode
	dest.subscriberNo = src.SubscriberNo
}
def listMapping17
listMapping17 = 
{
    src,dest  ->
	dest.operationType = src.OperationType
	listMapping18.call(src.BundleOfferOrderIdentity,dest.bundleOfferOrderIdentity)
	mappingList(src.OfferOrderIdentity,dest.offerOrderIdentitys,listMapping19)
	dest.bundleType = src.BundleType
	listMapping7.call(src.ValidMode,dest.validMode)
	mappingList(src.SimpleProperty,dest.simplePropertys,listMapping0)
	dest.accountCode = src.AccountCode	
}


def listMapping22
listMapping22 = 
{
    src,dest  ->
	dest.operationID = src.OperationID
	dest.subTradeType = src.SubTradeType
	dest.accessMode = src.AccessMode
	dest.chargeAmount = src.ChargeAmount
	dest.subscriberNo = src.SubscriberNo
	dest.accountCode = src.AccountCode
	listMapping4.call(src.OfferOrderIdentify,dest.offerOrderIdentify)
}
def listMapping21
listMapping21 = 
{
    src,dest  ->
	mappingList(src.OneTimeFee,dest.oneTimeFees,listMapping22)
}

def listMapping27
listMapping27 = 
{
    src,dest  ->
	dest.itemId = src.ItemId
	dest.amount = src.Amount
}

def listMapping26
listMapping26 = 
{
    src,dest  ->
	mappingList(src.ItemValue,dest.itemValues,listMapping27)
}

def listMapping25
listMapping25 = 
{
    src,dest  ->
	dest.acctId = src.AcctId
	dest.subId = src.SubId
	listMapping26.call(src.ItemList,dest.itemList)
}

def listMapping24
listMapping24 = 
{
    src,dest  ->
	mappingList(src.SubFeeValue,dest.subFeeValues,listMapping25)
}

def listMapping23
listMapping23 = 
{
    src,dest  ->
	listMapping24.call(src.SubFeeList,dest.subFeeList)
	dest.BusiType = src.BusiType
	dest.Bsno = src.Bsno
	dest.ChannelId = src.ChannelId
	dest.Remark = src.Remark
	dest.TradeTime = src.TradeTime
}

def listMapping31
listMapping31 = 
{
    src,dest  ->
	dest.transType = src.transType
	dest.productName = src.productName
	dest.orderQty = src.orderQty
	dest.backQty = src.backQty
	dest.salesQty = src.salesQty
	dest.unitPrice = src.unitPrice
	dest.charge = src.charge
	dest.discountAmt = src.discountAmt
	dest.transDate = src.transDate
	dest.salesDate = src.salesDate
	dest.feeItemCode = src.feeItemCode
	dest.billCycleId = src.billCycleId
	dest.taxInclusive = src.taxInclusive
	dest.glacode = src.glacode
	dest.waiveGlCode = src.waiveGlCode
	dest.rebateGlcode = src.rebateGlcode
	dest.rebateAmt = src.rebateAmt
	dest.resTypeId = src.resTypeId
	dest.resModelId = src.resModelId
	dest.resId = src.resId
	dest.resCode = src.resCode
	dest.resAmout = src.resAmout
	dest.refNo = src.refNo
	dest.feeType = src.feeType
	dest.productId = src.productId
	dest.spCpId = src.spCpId
	dest.serviceId = src.serviceId
	dest.prmFlag = src.prmFlag
	dest.serviceType = src.serviceType
	dest.serviceCategory = src.serviceCategory
	dest.reasonCode = src.reasonCode
	dest.feeSubType = src.feeSubType
	dest.invoiceType = src.invoiceType
	dest.remark = src.remark
}

def listMapping30
listMapping30 = 
{
    src,dest  ->
	mappingList(src.Fee2arRequestDetailValue,dest.fee2arRequestDetailValues,listMapping31)
}

def listMapping29
listMapping29 = 
{
    src,dest  ->
	dest.AcctId = src.AcctId
	dest.AcctCode = src.AcctCode
	dest.CustId = src.CustId
	dest.CustCode = src.CustCode
	dest.SubId = src.SubId
	dest.Msisdn = src.Msisdn
	dest.TeleType = src.TeleType
	dest.InvoiceDate = src.InvoiceDate
	dest.DueDate = parseDate(src.DueDate,Constant4Model.DATE_FORMAT)
	dest.BusiType = src.BusiType
	dest.Bsno = src.Bsno
	dest.isCustomer = src.isCustomer
	dest.ChannelId = src.ChannelId
	dest.Remark = src.Remark
	listMapping30.call(src.Fee2arRequestDetailList,dest.fee2arRequestDetailList)
}

def listMapping28
listMapping28 = 
{
    src,dest  ->
	mappingList(src.Fee2arRequestValue,dest.fee2arRequestValues,listMapping29)
}

def listMapping20
listMapping20 = 
{
    src,dest  ->
	listMapping21.call(src.ToCredit,dest.toCredit)
	listMapping23.call(src.ToBilling,dest.toBilling)
	listMapping28.call(src.ToAR,dest.toAR)
}

mappingList(srcOfferOrderBundleRelation,destofferOrderBundleRelations,listMapping17)
destArgs1.handleChargeFlag = srcMessageBody.HandleChargeFlag
listMapping20.call(srcMessageBody.OneTimeCharge,destArgs1.oneTimeCharge)

destHandleObj.subscriberNo = srcHandleObj.SubscriberNo
destHandleObj.accountCode = srcHandleObj.AccountCode
destHandleObj.customerCode = srcHandleObj.CustomerCode


