dest.setServiceOperation("BMGroupService","batchChangeMemberShortNo")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.group.batch.changemembershortno.io.BatchChangeMemberShortNoRequest"

def listMapping0

listMapping0 = 
{
    src,dest  ->

	
}

def srcMidVar = srcArgs0.BatchChangeMemberShortNoRequestMsg.BatchChangeMemberShortNoRequest

destArgs1.requestFileName = srcMidVar.FileName

def destMidVar = destArgs1.groupAccessCode

def srcMidVar0 = srcArgs0.BatchChangeMemberShortNoRequestMsg.BatchChangeMemberShortNoRequest.SubGroupAccessCode

destMidVar.groupCode = srcMidVar0.SubGroupCode

destMidVar.groupKey = srcMidVar0.SubGroupKey

def srcMidVar1 = srcArgs0.BatchChangeMemberShortNoRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar1.BEID

destArgs0.brId = srcMidVar1.BRID

def srcMidVar2 = srcArgs0.BatchChangeMemberShortNoRequestMsg.RequestHeader

destArgs0.businessCode = srcMidVar2.BusinessCode

def srcMidVar3 = srcArgs0.BatchChangeMemberShortNoRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar3.ChannelID

destArgs0.messageSeq = srcMidVar2.MessageSeq

destArgs0.msgLanguageCode = srcMidVar2.MsgLanguageCode

destArgs0.operatorId = srcMidVar3.OperatorID

def srcMidVar4 = srcArgs0.BatchChangeMemberShortNoRequestMsg.RequestHeader.AccessSecurity

destArgs0.password = srcMidVar4.Password

def destMidVar0 = destArgs0.simpleProperty[0]

def srcMidVar5 = srcArgs0.BatchChangeMemberShortNoRequestMsg.RequestHeader.AdditionalProperty[0]

destMidVar0.code = srcMidVar5.Code

destMidVar0.value = srcMidVar5.Value

def srcMidVar6 = srcArgs0.BatchChangeMemberShortNoRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar6.TimeType

destArgs0.timeZoneId = srcMidVar6.TimeZoneID

destArgs0.version = srcMidVar2.Version

destArgs0.loginSystem = srcMidVar4.LoginSystemCode

destArgs0.remoteAddress = srcMidVar4.RemoteIP

mappingList(srcMidVar2.AdditionalProperty,destArgs0.simpleProperty,listMapping0)

destArgs0.interMode = srcMidVar2.AccessMode
