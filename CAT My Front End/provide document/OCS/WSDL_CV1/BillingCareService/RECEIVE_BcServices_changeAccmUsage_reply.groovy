import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.Code = src.code
	
	dest.Value = src.value
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->
	
	dest.AccmTypeID = src.accmTypeID
	
	dest.AccmTypeCode = src.accmTypeCode
	
	dest.BeginDate = formatDate(src.beginDate, Constant4Model.DATE_FORMAT)
	
	dest.EndDate = formatDate(src.endDate, Constant4Model.DATE_FORMAT)
   
    dest.CurrAmount = src.currAmount
	
	dest.OldAmount = src.oldAmount
}

def srcMidVar0 = srcReturn.resultHeader

def destMidVar0 = destReturn.ChangeAccmUsageResultMsg.ResultHeader

destMidVar0.ResultDesc = srcMidVar0.resultDesc

destMidVar0.ResultCode = srcMidVar0.resultCode

destMidVar0.MsgLanguageCode = srcMidVar0.msgLanguageCode

destMidVar0.Version = srcMidVar0.version

destMidVar0.MessageSeq = srcMidVar0.messageSeq

mappingList(srcMidVar0.simpleProperty,destMidVar0.AdditionalProperty,listMapping0)

def destMidVar1 = destReturn.ChangeAccmUsageResultMsg.ChangeAccmUsageResult

mappingList(srcReturn.accmUsageList,destMidVar1.AccmUsageList,listMapping1)




