import com.huawei.ngcbs.bm.common.common.Constant4Model

def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping1

listMapping1 = { src,dest  ->
    
    dest.code = src.Code
    
    dest.value = src.Value
}

def listMapping2

listMapping2 = { src,dest  ->
    
    dest.balanceId = src.BalanceID
    
    dest.balanceType = src.BalanceType
    
    dest.balanceTypeName = src.BalanceTypeName
    
    dest.currencyId = src.CurrencyID
    
    dest.newBalanceAmt = src.NewBalanceAmt
    
    dest.oldBalanceAmt = src.OldBalanceAmt
}

def listMapping3

listMapping3 = { src,dest  ->
    
    dest.acctName = src.AcctName
    
    dest.acctno = src.AcctNo
    
    dest.acctType = src.AcctType
    
    dest.bankBranchCode = src.BankBranchCode
    
    dest.bankCode = src.BankCode
    
    dest.checkNo = src.CheckNo
    
    dest.creditCardType = src.CreditCardType
    
    dest.cvvNumber = src.CVVNumber
    
    dest.expDate=parseDate(src.ExpDate,Constant4Model.DATE_FORMAT)
}

def listMapping4

listMapping4 = { src,dest  ->
    
    dest.statusExpireTime = parseDate(src.StatusExpireTime,Constant4Model.DATE_FORMAT)
    
    dest.statusIndex = src.StatusIndex
    
    dest.statusName = src.StatusName
}

def listMapping5

listMapping5 = { src,dest  ->
    
    dest.statusExpireTime = parseDate(src.StatusExpireTime ,Constant4Model.DATE_FORMAT)
    
    dest.statusIndex = src.StatusIndex
    
    dest.statusName = src.StatusName
}

def listMapping6

listMapping6 = { src,dest  ->
    
    dest.balanceId = src.BalanceID
    
    dest.balanceType = src.BalanceType
    
    dest.balanceTypeName = src.BalanceTypeName
    
    dest.bonusAmt = src.BonusAmt
    
    dest.currencyId = src.CurrencyID
    
    dest.effectiveTime =  parseDate(src.EffectiveTime,Constant4Model.DATE_FORMAT)
    
    dest.expireTime =  parseDate(src.ExpireTime,Constant4Model.DATE_FORMAT)
}

def listMapping7

listMapping7 = { src,dest  ->
    
    dest.bonusAmt = src.BonusAmt
    
    dest.effectiveTime = parseDate(src.EffectiveTime,Constant4Model.DATE_FORMAT)
    
    dest.expireTime = parseDate(src.ExpireTime ,Constant4Model.DATE_FORMAT)
    
    dest.freeUnitId = src.FreeUnitID
    
    dest.freeUnitType = src.FreeUnitType
    
    dest.freeUnitTypeName = src.FreeUnitTypeName
    
    dest.measureUnit = src.MeasureUnit
    
    dest.measureUnitName = src.MeasureUnitName
}

def listMapping0

listMapping0 = { src,dest  ->
    
    dest.acctKey = src.AcctKey
    
    mappingList(src.AdditionalProperty,dest.additionalProperty,listMapping1)
    
    mappingList(src.BalanceChgInfo,dest.balanceChgInfoList,listMapping2)
    
    mappingList(src.BankInfo,dest.bankInfoList,listMapping3)
    
    def destMidVar0 = dest.cardInfo
    
    def srcMidVar0 = src.CardInfo
    
    destMidVar0.cardPinNumber = srcMidVar0.CardPinNumber
    
    destMidVar0.cardSequence = srcMidVar0.CardSequence
    
    dest.currencyId = src.CurrencyID
    
    dest.currencyRate = src.CurrencyRate
    
    dest.deptId = src.DeptID
    
    dest.extRechargeType = src.ExtRechargeType
    
    dest.extTransId = src.ExtTransID
    
    def destMidVar1 = dest.lifeCycleChgInfo
    
    def srcMidVar1 = src.LifeCycleChgInfo
    
    destMidVar1.chgValidity = srcMidVar1.ChgValidity
    
    mappingList(srcMidVar1.NewLifeCycleStatus,destMidVar1.newLifeCycleStatus,listMapping4)
    
    mappingList(srcMidVar1.OldLifeCycleStatus,destMidVar1.oldLifeCycleStatus,listMapping5)
    
    dest.operId = src.OperID
    
    dest.oriAmount = src.OriAmount
    
    dest.oriCurrencyId = src.OriCurrencyID
    
    dest.primaryIdentity = src.PrimaryIdentity
    
    dest.rechargeAmount = src.RechargeAmount
    
    def srcMidVar2 = src.RechargeBonus
    
    def destMidVar2 = dest.rechargeBonus
    
    mappingList(srcMidVar2.BalanceList,destMidVar2.balanceInfos,listMapping6)
    
    mappingList(srcMidVar2.FreeUnitItemList,destMidVar2.freeUnitItemList,listMapping7)
    
    dest.rechargeChannelId = src.RechargeChannelID
    
    dest.rechargePenalty = src.RechargePenalty
    
    dest.rechargeReason = src.RechargeReason
    
    dest.rechargeTax = src.RechargeTax
    
    dest.rechargeType = src.RechargeType
    
    dest.resultCode = src.ResultCode
    
    dest.reversalDeptId = src.ReversalDeptID
    
    dest.reversalFlag = src.ReversalFlag
    
    dest.reversalOpId = src.ReversalOpID
    
    dest.reversalReason = src.ReversalReason
    
    dest.reversalTime = parseDate(src.ReversalTime ,Constant4Model.DATE_FORMAT)
    
    dest.subKey = src.SubKey
    
    dest.tradeTime = parseDate(src.TradeTime,Constant4Model.DATE_FORMAT)
    
    dest.transId = src.TransID
}

def listMapping8

listMapping8 = { src,dest  ->
    
    dest.code = src.Code
    
    dest.value = src.Value
}

def destMidVar = destReturn.rechargeLogResultInfo

def srcMidVar = srcReturn.QueryRechargeLogResultMsg.QueryRechargeLogResult

destMidVar.beginRowNum = srcMidVar.BeginRowNum

destMidVar.fetchRowNum = srcMidVar.FetchRowNum

destMidVar.totalRowNum = srcMidVar.TotalRowNum

mappingList(srcMidVar.RechargeInfo,destMidVar.rechargeInfoList,listMapping0)

def srcMidVar3 = srcReturn.QueryRechargeLogResultMsg.ResultHeader

def destMidVar3 = destReturn.resultHeader

mappingList(srcMidVar3.AdditionalProperty,destMidVar3.simpleProperty,listMapping8)

destMidVar3.msgLanguageCode = srcMidVar3.MsgLanguageCode

destMidVar3.resultCode = srcMidVar3.ResultCode

destMidVar3.resultDesc = srcMidVar3.ResultDesc

destMidVar3.version = srcMidVar3.Version

destReturn._class = "com.huawei.ngcbs.cm.common.ws.client.io.ar.queryRechargeLog.io.QueryRechargeLogResult"
