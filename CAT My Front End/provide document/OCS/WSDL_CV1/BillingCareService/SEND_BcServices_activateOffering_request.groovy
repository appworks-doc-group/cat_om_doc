dest.setServiceOperation("BMOfferingService","activateOfferingInst")

def srcArgs0 = src.payload._args[0]

def destArgs0 = dest.payload._args[0]

destArgs0._class = "com.huawei.ngcbs.bm.common.common.MessageHeader"

def destArgs1 = dest.payload._args[1]

destArgs1._class = "com.huawei.ngcbs.cm.offering.activateofferinginst.io.ActivateOfferingInstRequest"

def listMapping2

listMapping2 = 
{
    src,dest  ->

	dest.oId = src.OfferingID
	
	dest.oCode = src.OfferingCode
	
	dest.pSeq = src.PurchaseSeq
	
}

def listMapping1

listMapping1 = 
{
    src,dest  ->

	listMapping2.call(src,dest)
	
}

def listMapping0

listMapping0 = 
{
    src,dest  ->

		dest.expirationTime=parseDate(src.ExpirationTime,"yyyyMMddHHmmss")
		
	def oKeyExt = dest.offeringKey
	
  oKeyExt._class = "com.huawei.ngcbs.cm.common.common.io.offering.OfferingKeyExtInfo"
	
	listMapping1.call(src.OfferingKey,oKeyExt)
	
		dest.trialEndTime=parseDate(src.TrialEndTime,"yyyyMMddHHmmss")
	
		dest.trialStartTime=parseDate(src.TrialStartTime,"yyyyMMddHHmmss")
	
}

def listMapping3

listMapping3 = 
{
    src,dest  ->

	dest.accoutCode = src.AccountCode
	
	dest.accoutKey = src.AccountKey
	
	dest.payType = src.PayType
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping4

listMapping4 = 
{
    src,dest  ->

	dest.customerCode = src.CustomerCode
	
	dest.customerKey = src.CustomerKey
	
	dest.primaryIdentity = src.PrimaryIdentity
	
}

def listMapping5

listMapping5 = 
{
    src,dest  ->

	dest.primaryIdentity = src.PrimaryIdentity
	
	dest.subscriberKey = src.SubscriberKey
	
}

def listMapping6

listMapping6 = 
{
    src,dest  ->

	dest.groupCode = src.SubGroupCode
	
	dest.groupKey = src.SubGroupKey
	
}

def listMapping9

listMapping9 = 
{
    src,dest  ->

	
}

def listMapping7

listMapping7 = 
{
    src,dest  ->

	listMapping9.call(src.OfferingOwner,dest.anyAccessCode)
	
}

def listMapping11

listMapping11 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def listMapping10

listMapping10 = 
{
    src,dest  ->

	mappingList(src.AdditionalProperty,dest.simpleProperty,listMapping11)
	
	def srcMidVar7 = src.AccessSecurity
	
	dest.remoteAddress = srcMidVar7.RemoteIP
	
	dest.interMode = src.AccessMode
	
}

def listMapping8

listMapping8 = 
{
    src,dest  ->

	def srcMidVar6 = src.AccessSecurity
	
	dest.password = srcMidVar6.Password
	
	dest.loginSystem = srcMidVar6.LoginSystemCode
	
	listMapping10.call(src,dest)
	
}


def srcMidVar = srcArgs0.ActivateOfferingRequestMsg.ActivateOfferingRequest

mappingList(srcMidVar.OfferingInst,destArgs1.activeOfferingInstInfoList,listMapping0)

def srcMidVar0 = srcArgs0.ActivateOfferingRequestMsg.ActivateOfferingRequest.OfferingOwner

def destMidVar = destArgs1.anyAccessCode

listMapping3.call(srcMidVar0.AcctAccessCode,destMidVar.acctAccessCode)

listMapping4.call(srcMidVar0.CustAccessCode,destMidVar.custAccessCode)

listMapping5.call(srcMidVar0.SubAccessCode,destMidVar.subAccessCode)

listMapping6.call(srcMidVar0.SubGroupAccessCode,destMidVar.groupAccessCode)

def srcMidVar1 = srcArgs0.ActivateOfferingRequestMsg

listMapping7.call(srcMidVar1.ActivateOfferingRequest,destArgs1)

def srcMidVar2 = srcArgs0.ActivateOfferingRequestMsg.RequestHeader.TimeFormat

destArgs0.timeType = srcMidVar2.TimeType

destArgs0.timeZoneId = srcMidVar2.TimeZoneID

def srcMidVar3 = srcArgs0.ActivateOfferingRequestMsg.RequestHeader

destArgs0.version = srcMidVar3.Version

def srcMidVar4 = srcArgs0.ActivateOfferingRequestMsg.RequestHeader.OperatorInfo

destArgs0.channelId = srcMidVar4.ChannelID

destArgs0.operatorId = srcMidVar4.OperatorID

def srcMidVar5 = srcArgs0.ActivateOfferingRequestMsg.RequestHeader.OwnershipInfo

destArgs0.beId = srcMidVar5.BEID

destArgs0.brId = srcMidVar5.BRID

destArgs0.msgLanguageCode = srcMidVar3.MsgLanguageCode

destArgs0.messageSeq = srcMidVar3.MessageSeq

destArgs0.businessCode = srcMidVar3.BusinessCode

listMapping8.call(srcMidVar1.RequestHeader,destArgs0)
