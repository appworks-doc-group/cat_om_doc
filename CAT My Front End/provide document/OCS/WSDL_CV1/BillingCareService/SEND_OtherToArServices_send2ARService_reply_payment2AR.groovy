def srcReturn = src.payload._return

def destReturn = dest.payload._return

def listMapping0

listMapping0 = 
{
    src,dest  ->

	dest.code = src.Code
	
	dest.value = src.Value
	
}

def srcMidVar = srcReturn.Payment2ARResultMsg.ResultHeader

def destMidVar = destReturn.resultHeader

mappingList(srcMidVar.AdditionalProperty,destMidVar.simpleProperty,listMapping0)

destMidVar.msgLanguageCode = srcMidVar.MsgLanguageCode

destMidVar.resultCode = srcMidVar.ResultCode

destMidVar.resultDesc = srcMidVar.ResultDesc

destMidVar.version = srcMidVar.Version

destReturn._class = "com.huawei.ngcbs.cm.common.ws.payment2AR.io.Payment2ARResult"
