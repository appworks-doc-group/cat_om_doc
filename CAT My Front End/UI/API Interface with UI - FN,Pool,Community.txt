﻿API Family Shared Bundle
- ค้นหากลุ่มกับ CA (CRM)
- ค้นหา BA ที่ผูกกับกลุ่ม (OCS)
- product catalog - query PO, SO
- API: [OCS] ถามดูว่ามีการนำ SO ไป query OCS product catalog เพื่อหา free unit ที่มากับ SO ได้เลยหรือไม่
- API: Create Group
  - group name
  - CA
  - BA
  - PO
  - SO
  - Members
  - (ถ้า OCS query free unit ได้) ส่ง manage quota ได้เลย
- API: Query Member List
- API: Add Member / Remove Member
  - กรณีที่มีการ Manager เกินจำนวนครั้งที่ฟรี หน้าบ้านจะมีส่ง NRC พร้อม ราคา มาให้เพื่อคิดค่าธรรมเนียม (เก็บ NRC ID ส่วน ราคาดึงจาก Product Catalog)
- API: Query Free Unit List and Amount by SO
- API: Query Resource Relation (Member Quota)
  - อยากได้ MSISDN, share type (fix, pool), free unit quota type, free unit quota amount
- API: Manage Quota (Resource Relation)
  - กรณีที่มีการ Manager เกินจำนวนครั้งที่ฟรี หน้าบ้านจะมีส่ง NRC พร้อม ราคา มาให้เพื่อคิดค่าธรรมเนียม (เก็บ NRC ID ส่วน ราคาดึงจาก Product Catalog)
  - Set MSISDN quota
    - ** UI อยากให้ set เป็น excel file และ upload ขึ้นไปทาง Eportal **


API FN
- API: Query MSISDN Info - เพิ่ม fnFlag ใน response ของ PO, SO
- API: Product Catalog - Query SO ว่ามี Group List อะไร
- API: Query Member List - ว่าอยู่กลุ่มไหน เบอร์อะไร
- API: Manager FN - Add/Del, SO, Group ID, Dest MSISDN เป็น order item (ลบก่อน และค่อยเพิ่ม กรณีที่เป็นการย้าย)
- [OCS] ถ้าเบอร์เดียวกัน สามารถ add หลาย group ได้ไหม


API Community
- ทุกอย่างอยากส่งเป็น file จาก UI
- [API] Set Pay Relation : by service, time, percentage - file
- [API] Product Catalog
  - List SO special Tarif (M)
  - List SO free unit (G)
- [API] ในการ query ดูว่า set pay relation, rsc relation, special tarif ราย MSIDN

*** file ทาง UI จะเอาไปวางไว้ที่ share path และส่งชื่อ file มาทาง order แทน ***